(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["splash-splash-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/splash/splash.page.html":
/*!*******************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/splash/splash.page.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content>\n\n\n   <div class=\"back\">\n   \n     <div class=\"logoSize\">\n          <img src=\"/assets/img/w.png\"  />\n      </div>\n     \n\n\n      <div class=\"pad\" id=\"move-down2\">\n        <h1 class=\"font-style2\">Quality Rides with Us</h1> \n        <h4 class=\"font-style\"> A better ride\n          experience for you.</h4>\n      </div>\n        <div class=\"move-down\">\n        <p class=\"pad\" style=\"text-align: center\">\n          <ion-button color=\"primary\" class=\"button_style\" (click)=\"login()\">\n            Get Started\n            <ion-icon color=\"light\" name=\"arrow-forward\" slot=\"end\"></ion-icon>\n          </ion-button>\n        </p>\n      </div>\n  \n     \n     \n  </div> \n</ion-content>\n"

/***/ }),

/***/ "./src/app/splash/splash.module.ts":
/*!*****************************************!*\
  !*** ./src/app/splash/splash.module.ts ***!
  \*****************************************/
/*! exports provided: SplashPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SplashPageModule", function() { return SplashPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _splash_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./splash.page */ "./src/app/splash/splash.page.ts");







var routes = [
    {
        path: '',
        component: _splash_page__WEBPACK_IMPORTED_MODULE_6__["SplashPage"]
    }
];
var SplashPageModule = /** @class */ (function () {
    function SplashPageModule() {
    }
    SplashPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_splash_page__WEBPACK_IMPORTED_MODULE_6__["SplashPage"]]
        })
    ], SplashPageModule);
    return SplashPageModule;
}());



/***/ }),

/***/ "./src/app/splash/splash.page.scss":
/*!*****************************************!*\
  !*** ./src/app/splash/splash.page.scss ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".back {\n  background-image: url(/assets/img/circle.png);\n  background-color: white;\n  height: 100%;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n  align-content: center;\n  text-align: center;\n}\n\n.pad {\n  text-align: center;\n}\n\n.font-style {\n  color: black;\n  font-size: 19px;\n  text-align: center;\n  font-weight: 100;\n  line-height: 1.5;\n  letter-spacing: 1px;\n}\n\n.font-style2 {\n  color: #010000;\n  font-size: 32px;\n  text-align: center;\n  font-weight: 900;\n  letter-spacing: 1px;\n}\n\n.move-down {\n  padding-top: 15%;\n}\n\n#move-down2 {\n  padding-top: 30%;\n}\n\n.button_style {\n  text-align: center;\n  border-radius: 60px;\n  height: 40px;\n  width: 90%;\n}\n\n.logoSize {\n  text-align: center;\n  width: 70%;\n  align-content: center;\n  margin-left: 20%;\n  padding-top: 5%;\n  margin-bottom: -20%;\n  position: relative;\n  -webkit-animation: spin 2s ease-in-out alternate infinite;\n  -webkit-animation-duration: 2s;\n          animation-duration: 2s;\n}\n\n#overlay {\n  position: fixed;\n  /* display: none; */\n  width: 100%;\n  height: 100%;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  background-color: #00000036;\n  z-index: 10;\n  cursor: pointer;\n}\n\n@-webkit-keyframes spin {\n  from {\n    left: 0px;\n    top: 0px;\n  }\n  to {\n    left: 0px;\n    top: 20px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYWhhL0lvbmljQXBwcy9DNFJpZGUtSU9TLTIwMjQvY2FsbDRyaWRlLXBhc3Nlbmdlci9zcmMvYXBwL3NwbGFzaC9zcGxhc2gucGFnZS5zY3NzIiwic3JjL2FwcC9zcGxhc2gvc3BsYXNoLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUF5Q0E7RUFDRSw2Q0FBQTtFQUNFLHVCQUFBO0VBQ0YsWUFBQTtFQUNBLDJCQUFBO0VBQ0EsNEJBQUE7RUFDQSxzQkFBQTtFQUNBLHFCQUFBO0VBQ0Esa0JBQUE7QUN4Q0Y7O0FEMkNBO0VBQ0Usa0JBQUE7QUN4Q0Y7O0FEMkNBO0VBQ0UsWUFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBRUEsZ0JBQUE7RUFDQSxtQkFBQTtBQ3pDRjs7QUQ0Q0E7RUFDRSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBQ3pDRjs7QUQ0Q0E7RUFDRSxnQkFBQTtBQ3pDRjs7QUQ0Q0E7RUFDRSxnQkFBQTtBQ3pDRjs7QUQyQ0E7RUFDRSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7QUN4Q0Y7O0FEMkNBO0VBQ0Usa0JBQUE7RUFDRSxVQUFBO0VBQ0EscUJBQUE7RUFDQyxnQkFBQTtFQUNELGVBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBRUcseURBQUE7RUFDRiw4QkFBQTtVQUFBLHNCQUFBO0FDekNMOztBRDRDQTtFQUNFLGVBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsTUFBQTtFQUNBLE9BQUE7RUFDQSxRQUFBO0VBQ0EsU0FBQTtFQUNBLDJCQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7QUN6Q0Y7O0FEOENBO0VBQ0c7SUFDRyxTQUFBO0lBQ0EsUUFBQTtFQzNDSjtFRDhDQztJQUNHLFNBQUE7SUFDQSxTQUFBO0VDNUNKO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9zcGxhc2gvc3BsYXNoLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIGlvbi1jb250ZW50IHtcbi8vICAgLS1iYWNrZ3JvdW5kOiAjZmJiOTFkO1xuLy8gfVxuLy8gLmNvbnRhaW5lciB7XG4vLyAgIC13ZWJraXQtYW5pbWF0aW9uOiByb3RhdGUtY2VudGVyIDFzIGVhc2UtaW4tb3V0IGJvdGg7XG4vLyAgIGFuaW1hdGlvbjogcm90YXRlLWNlbnRlciAxcyBlYXNlLWluLW91dCBib3RoO1xuLy8gICAvLyBwb3NpdGlvbjogcmVsYXRpdmU7XG4vLyAgIGltZyB7XG4vLyAgICAgaGVpZ2h0OiAxMDBweDtcbi8vICAgICBtYXJnaW4tdG9wOiA2MCU7XG4vLyAgIH1cbi8vIH1cblxuLy8gQC13ZWJraXQta2V5ZnJhbWVzIHJvdGF0ZS1jZW50ZXIge1xuLy8gICAwJSB7XG4vLyAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgwKTtcbi8vICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgwKTtcbi8vICAgfVxuLy8gICAxMDAlIHtcbi8vICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDM2MGRlZyk7XG4vLyAgICAgdHJhbnNmb3JtOiByb3RhdGUoMzYwZGVnKTtcbi8vICAgfVxuLy8gfVxuLy8gQGtleWZyYW1lcyByb3RhdGUtY2VudGVyIHtcbi8vICAgMCUge1xuLy8gICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMCk7XG4vLyAgICAgdHJhbnNmb3JtOiByb3RhdGUoMCk7XG4vLyAgIH1cbi8vICAgMTAwJSB7XG4vLyAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgzNjBkZWcpO1xuLy8gICAgIHRyYW5zZm9ybTogcm90YXRlKDM2MGRlZyk7XG4vLyAgIH1cbi8vIH1cbi8vIC52ZXJ0aWNhbC1jZW50ZXIge1xuLy8gICBtYXJnaW46IDA7XG4vLyAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbi8vICAgdG9wOiA1MCU7XG4vLyAgIC1tcy10cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XG4vLyAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNTAlKTtcbi8vIH1cblxuLmJhY2sge1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoL2Fzc2V0cy9pbWcvY2lyY2xlLnBuZyk7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gIGhlaWdodDogMTAwJTtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLnBhZCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmZvbnQtc3R5bGUge1xuICBjb2xvcjogcmdiKDAsIDAsIDApO1xuICBmb250LXNpemU6IDE5cHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC13ZWlnaHQ6IDEwMDtcbiAgLy8gbWFyZ2luLWxlZnQ6IDEwcHg7XG4gIGxpbmUtaGVpZ2h0OiAxLjU7XG4gIGxldHRlci1zcGFjaW5nOiAxcHg7XG59XG5cbi5mb250LXN0eWxlMiB7XG4gIGNvbG9yOiByZ2IoMSwgMCwgMCk7XG4gIGZvbnQtc2l6ZTogMzJweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXdlaWdodDogOTAwO1xuICBsZXR0ZXItc3BhY2luZzogMXB4O1xufVxuXG4ubW92ZS1kb3duIHtcbiAgcGFkZGluZy10b3A6IDE1JTtcbn1cblxuI21vdmUtZG93bjIge1xuICBwYWRkaW5nLXRvcDogMzAlO1xufVxuLmJ1dHRvbl9zdHlsZSB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgYm9yZGVyLXJhZGl1czogNjBweDtcbiAgaGVpZ2h0OiA0MHB4O1xuICB3aWR0aDogOTAlO1xufVxuXG4ubG9nb1NpemUge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgd2lkdGg6IDcwJTtcbiAgICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XG4gICAgIG1hcmdpbi1sZWZ0OiAyMCU7XG4gICAgcGFkZGluZy10b3A6IDUlO1xuICAgIG1hcmdpbi1ib3R0b206IC0yMCU7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgLy8gYW5pbWF0aW9uOiBzcGluIGluZmluaXRlIGxpbmVhciBib3RoO1xuICAgICAgIC13ZWJraXQtYW5pbWF0aW9uOnNwaW4gMnMgZWFzZS1pbi1vdXQgYWx0ZXJuYXRlIGluZmluaXRlO1xuICAgICBhbmltYXRpb24tZHVyYXRpb246IDIuMHM7IFxufVxuXG4jb3ZlcmxheSB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgLyogZGlzcGxheTogbm9uZTsgKi9cbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgdG9wOiAwO1xuICBsZWZ0OiAwO1xuICByaWdodDogMDtcbiAgYm90dG9tOiAwO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwMDAwMzY7XG4gIHotaW5kZXg6IDEwO1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG5cblxuXG5ALXdlYmtpdC1rZXlmcmFtZXMgc3BpbiAge1xuICAgZnJvbSB7XG4gICAgICBsZWZ0OjBweDtcbiAgICAgIHRvcDowcHg7XG4gICB9XG4gICBcbiAgIHRvIHtcbiAgICAgIGxlZnQ6MHB4O1xuICAgICAgdG9wOjIwcHg7XG4gICB9XG5cbn1cbiIsIi5iYWNrIHtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKC9hc3NldHMvaW1nL2NpcmNsZS5wbmcpO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgaGVpZ2h0OiAxMDAlO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4ucGFkIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uZm9udC1zdHlsZSB7XG4gIGNvbG9yOiBibGFjaztcbiAgZm9udC1zaXplOiAxOXB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtd2VpZ2h0OiAxMDA7XG4gIGxpbmUtaGVpZ2h0OiAxLjU7XG4gIGxldHRlci1zcGFjaW5nOiAxcHg7XG59XG5cbi5mb250LXN0eWxlMiB7XG4gIGNvbG9yOiAjMDEwMDAwO1xuICBmb250LXNpemU6IDMycHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC13ZWlnaHQ6IDkwMDtcbiAgbGV0dGVyLXNwYWNpbmc6IDFweDtcbn1cblxuLm1vdmUtZG93biB7XG4gIHBhZGRpbmctdG9wOiAxNSU7XG59XG5cbiNtb3ZlLWRvd24yIHtcbiAgcGFkZGluZy10b3A6IDMwJTtcbn1cblxuLmJ1dHRvbl9zdHlsZSB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgYm9yZGVyLXJhZGl1czogNjBweDtcbiAgaGVpZ2h0OiA0MHB4O1xuICB3aWR0aDogOTAlO1xufVxuXG4ubG9nb1NpemUge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHdpZHRoOiA3MCU7XG4gIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcbiAgbWFyZ2luLWxlZnQ6IDIwJTtcbiAgcGFkZGluZy10b3A6IDUlO1xuICBtYXJnaW4tYm90dG9tOiAtMjAlO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIC13ZWJraXQtYW5pbWF0aW9uOiBzcGluIDJzIGVhc2UtaW4tb3V0IGFsdGVybmF0ZSBpbmZpbml0ZTtcbiAgYW5pbWF0aW9uLWR1cmF0aW9uOiAycztcbn1cblxuI292ZXJsYXkge1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIC8qIGRpc3BsYXk6IG5vbmU7ICovXG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIHRvcDogMDtcbiAgbGVmdDogMDtcbiAgcmlnaHQ6IDA7XG4gIGJvdHRvbTogMDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwMDAwMDM2O1xuICB6LWluZGV4OiAxMDtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG5ALXdlYmtpdC1rZXlmcmFtZXMgc3BpbiB7XG4gIGZyb20ge1xuICAgIGxlZnQ6IDBweDtcbiAgICB0b3A6IDBweDtcbiAgfVxuICB0byB7XG4gICAgbGVmdDogMHB4O1xuICAgIHRvcDogMjBweDtcbiAgfVxufSJdfQ== */"

/***/ }),

/***/ "./src/app/splash/splash.page.ts":
/*!***************************************!*\
  !*** ./src/app/splash/splash.page.ts ***!
  \***************************************/
/*! exports provided: SplashPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SplashPage", function() { return SplashPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



var SplashPage = /** @class */ (function () {
    function SplashPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    SplashPage.prototype.ngOnInit = function () { };
    SplashPage.prototype.login = function () {
        this.navCtrl.navigateRoot("login");
    };
    SplashPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] }
    ]; };
    SplashPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-splash",
            template: __webpack_require__(/*! raw-loader!./splash.page.html */ "./node_modules/raw-loader/index.js!./src/app/splash/splash.page.html"),
            styles: [__webpack_require__(/*! ./splash.page.scss */ "./src/app/splash/splash.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]])
    ], SplashPage);
    return SplashPage;
}());



/***/ })

}]);
//# sourceMappingURL=splash-splash-module-es5.js.map