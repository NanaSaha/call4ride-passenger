(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-entrance-entrance-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/entrance/entrance.page.html":
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/entrance/entrance.page.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content class='ion-padding'>\n\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/entrance/entrance.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/entrance/entrance.module.ts ***!
  \***************************************************/
/*! exports provided: EntrancePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EntrancePageModule", function() { return EntrancePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _entrance_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./entrance.page */ "./src/app/pages/entrance/entrance.page.ts");







var routes = [
    {
        path: '',
        component: _entrance_page__WEBPACK_IMPORTED_MODULE_6__["EntrancePage"]
    }
];
var EntrancePageModule = /** @class */ (function () {
    function EntrancePageModule() {
    }
    EntrancePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_entrance_page__WEBPACK_IMPORTED_MODULE_6__["EntrancePage"]]
        })
    ], EntrancePageModule);
    return EntrancePageModule;
}());



/***/ }),

/***/ "./src/app/pages/entrance/entrance.page.scss":
/*!***************************************************!*\
  !*** ./src/app/pages/entrance/entrance.page.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2VudHJhbmNlL2VudHJhbmNlLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/pages/entrance/entrance.page.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/entrance/entrance.page.ts ***!
  \*************************************************/
/*! exports provided: EntrancePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EntrancePage", function() { return EntrancePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



var EntrancePage = /** @class */ (function () {
    function EntrancePage(navCtrl, load) {
        this.navCtrl = navCtrl;
        this.load = load;
    }
    EntrancePage.prototype.ngOnInit = function () {
        this.presentRouteLoader('');
    };
    EntrancePage.prototype.presentRouteLoader = function (message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading, myTimeout;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.load.create({
                            // tslint:disable-next-line: object-literal-shorthand
                            message: message
                        })];
                    case 1:
                        loading = _a.sent();
                        loading.present();
                        myTimeout = setTimeout(function () {
                            loading.dismiss();
                            _this.navCtrl.navigateRoot('signup');
                            clearTimeout(myTimeout);
                        }, 1000);
                        return [2 /*return*/];
                }
            });
        });
    };
    EntrancePage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] }
    ]; };
    EntrancePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-entrance',
            template: __webpack_require__(/*! raw-loader!./entrance.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/entrance/entrance.page.html"),
            styles: [__webpack_require__(/*! ./entrance.page.scss */ "./src/app/pages/entrance/entrance.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]])
    ], EntrancePage);
    return EntrancePage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-entrance-entrance-module-es5.js.map