(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-payment-payment-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/payment/payment.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/payment/payment.page.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--\n  Generated template for the PaymentPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header no-border>\n  <ion-toolbar style=\"margin-top: 20px\">\n    <ion-button (click)=\"goBack()\" ion-button color=\"primary\" fill='clear'>\n      <ion-icon style=\"font-size: 1.5em\" name=\"arrow-back\"></ion-icon> <span\n        style=\"margin-left:30px; font-size: 1.4em\">{{lp.translate()[0].payment2}}</span>\n    </ion-button>\n\n  </ion-toolbar>\n\n\n\n</ion-header>\n\n\n<ion-content class=\"yes-scroll\" padding>\n\n\n  <ion-list padding>\n    <ion-radio-group value=\"\">\n      <div *ngFor=\"let item of items\">\n        <ion-button expand='block' id='item' color=\"success\" size='large'>\n          <ion-grid>\n            <ion-row>\n              <ion-col>\n                <ion-icon color='light' name=\"card\" slot='start'>\n                </ion-icon>\n              </ion-col>\n              <ion-col>\n                <span> {{item.card.source.card.brand}} **** {{item.card.source.card.last4}} </span>\n              </ion-col>\n              <ion-col>\n                <ion-radio color='light' checked='{{item.checked}}'\n                  (ionSelect)=\"UseCard(item.card.source.id, item.key, item.customerID)\"></ion-radio>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n        </ion-button>\n      </div>\n\n      <ion-button expand='block' id='item' color=\"success\" size='large'>\n        <ion-grid>\n          <ion-row>\n            <ion-col>\n              <ion-icon color='light' name=\"cash\"></ion-icon>\n            </ion-col>\n            <ion-col>\n              <ion-label>\n                Use Cash\n              </ion-label>\n            </ion-col>\n            <ion-col>\n              <ion-radio color='light' checked='{{payWith}}' value=\"\" (ionSelect)=\"UseCash()\"></ion-radio>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n\n      </ion-button>\n    </ion-radio-group>\n  </ion-list>\n\n  <h1 style=\"text-align: center;\">Credits</h1>\n  <h1 *ngIf='credits' style=\"text-align: center;\">${{credits}}</h1>\n  <h1 *ngIf='!credits' style=\"text-align: center;\">$0.00</h1>\n\n  <ion-button expand='block' id='item' color=\"primary\" (click)='gotoCard()' size='large'>\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <ion-icon color='light' name=\"add\">\n          </ion-icon>\n        </ion-col>\n        <ion-col>\n          <ion-label [hidden]=\"ph.card != null\">\n          {{lp.translate()[0].addcard}}\n          </ion-label>\n        </ion-col>\n        <ion-col>\n\n          <ion-label [hidden]=\"ph.card == null\">\n            <ion-icon color='light' name=\"card\">\n            </ion-icon> {{cardnumber}}\n          </ion-label>\n        </ion-col>\n\n      </ion-row>\n    </ion-grid>\n\n\n  </ion-button>\n\n\n\n\n\n  <ion-label text-center>\n\n  </ion-label>\n  <ion-button *ngIf='set.refer' expand='block' id='item' color=\"newColor\" (click)=\"Promo()\" size='large'>\n    <ion-label>\n      <ion-icon color='nav-color' padding name=\"trophy\">\n      </ion-icon>\n      Enter A Referal Code.\n    </ion-label>\n  </ion-button>\n\n\n\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/payment/payment.module.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/payment/payment.module.ts ***!
  \*************************************************/
/*! exports provided: PaymentPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentPageModule", function() { return PaymentPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _payment_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./payment.page */ "./src/app/pages/payment/payment.page.ts");







const routes = [
    {
        path: '',
        component: _payment_page__WEBPACK_IMPORTED_MODULE_6__["PaymentPage"]
    }
];
let PaymentPageModule = class PaymentPageModule {
};
PaymentPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_payment_page__WEBPACK_IMPORTED_MODULE_6__["PaymentPage"]]
    })
], PaymentPageModule);



/***/ }),

/***/ "./src/app/pages/payment/payment.page.scss":
/*!*************************************************!*\
  !*** ./src/app/pages/payment/payment.page.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-header {\n  margin-top: 20px;\n}\n\n#item {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-flex: 1;\n          flex: 1 1 auto;\n  height: 90px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYWhhL0lvbmljQXBwcy9DNFJpZGUtSU9TLTIwMjQvY2FsbDRyaWRlLXBhc3Nlbmdlci9zcmMvYXBwL3BhZ2VzL3BheW1lbnQvcGF5bWVudC5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL3BheW1lbnQvcGF5bWVudC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxnQkFBQTtBQ0NGOztBRENBO0VBQ0Usb0JBQUE7RUFBQSxhQUFBO0VBQ0EsbUJBQUE7VUFBQSxjQUFBO0VBRUEsWUFBQTtBQ0NGIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvcGF5bWVudC9wYXltZW50LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1oZWFkZXIge1xyXG4gIG1hcmdpbi10b3A6IDIwcHg7XHJcbn1cclxuI2l0ZW0ge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleDogMSAxIGF1dG87XHJcblxyXG4gIGhlaWdodDogOTBweDtcclxufVxyXG5cclxuIiwiaW9uLWhlYWRlciB7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG59XG5cbiNpdGVtIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleDogMSAxIGF1dG87XG4gIGhlaWdodDogOTBweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/payment/payment.page.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/payment/payment.page.ts ***!
  \***********************************************/
/*! exports provided: PaymentPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentPage", function() { return PaymentPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_language_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/language.service */ "./src/app/services/language.service.ts");
/* harmony import */ var src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/settings.service */ "./src/app/services/settings.service.ts");
/* harmony import */ var src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/profile.service */ "./src/app/services/profile.service.ts");
/* harmony import */ var src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/pop-up.service */ "./src/app/services/pop-up.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");








let PaymentPage = class PaymentPage {
    constructor(ph, pop, set, lp, navCtrl, route) {
        this.ph = ph;
        this.pop = pop;
        this.set = set;
        this.lp = lp;
        this.navCtrl = navCtrl;
        this.route = route;
        this.price = this.route.snapshot.paramMap.get('price');
        this.NoKey = false;
        this.payWith = true;
    }
    ngOnInit() {
    }
    ionViewDidEnter() {
        this.ph.getUserProfile().on('value', userProfileSnapshot => {
            if (userProfileSnapshot.val().credits)
                this.credits = userProfileSnapshot.val().credits;
            if (userProfileSnapshot.val().payWith == 1)
                this.payWith = true;
            if (userProfileSnapshot.val().payWith == 2)
                this.payWith = false;
        });
        this.ph.getUserProfile().child('CreditCards').on('value', snapshot => {
            this.items = [];
            console.log(snapshot.val());
            snapshot.forEach(snap => {
                console.log(snap.val());
                if (snap.val().checked) {
                    this.Level_key = snap.key;
                    console.log(snap.key);
                }
                else {
                    this.NoKey = true;
                }
                this.items.push({
                    key: snap.key,
                    card: snap.val().card,
                    checked: snap.val().checked,
                    customerID: snap.val().customerID
                });
                console.log(snap.val());
                return false;
            });
        });
    }
    UseCard(id, k, customerID) {
        console.log(k, id, customerID);
        this.ph.UseCard(id, customerID);
        if (this.Level_key) {
            this.ph.DitchCard(this.Level_key).then(() => {
                this.ph.CheckCard(k);
                this.updatePayment(2);
            });
        }
        else {
            this.ph.CheckCard(k);
            this.updatePayment(2);
            this.NoKey = false;
        }
        this.payWith = false;
    }
    UseCash() {
        this.payWith = true;
        if (this.Level_key) {
            this.ph.DitchCard(this.Level_key).then(() => {
                this.updatePayment(1);
            });
        }
        else {
            this.NoKey = false;
            this.updatePayment(1);
        }
    }
    updatePayment(value) {
        this.ph.UpdatePaymentType(value);
        this.navCtrl.navigateBack('home');
    }
    gotoCard() {
        this.navCtrl.navigateRoot('card');
    }
    Promo() {
        this.navCtrl.navigateRoot('referalcode');
    }
    goBack() {
        this.navCtrl.navigateBack('home');
    }
};
PaymentPage.ctorParameters = () => [
    { type: src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_5__["ProfileService"] },
    { type: src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_6__["PopUpService"] },
    { type: src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_4__["SettingsService"] },
    { type: src_app_services_language_service__WEBPACK_IMPORTED_MODULE_3__["LanguageService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"] }
];
PaymentPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-payment',
        template: __webpack_require__(/*! raw-loader!./payment.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/payment/payment.page.html"),
        styles: [__webpack_require__(/*! ./payment.page.scss */ "./src/app/pages/payment/payment.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_5__["ProfileService"], src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_6__["PopUpService"], src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_4__["SettingsService"], src_app_services_language_service__WEBPACK_IMPORTED_MODULE_3__["LanguageService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"]])
], PaymentPage);



/***/ })

}]);
//# sourceMappingURL=pages-payment-payment-module-es2015.js.map