(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-payment-approval-payment-approval-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/payment-approval/payment-approval.page.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/payment-approval/payment-approval.page.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title>payment-approval</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/payment-approval/payment-approval.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/payment-approval/payment-approval.module.ts ***!
  \*******************************************************************/
/*! exports provided: PaymentApprovalPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentApprovalPageModule", function() { return PaymentApprovalPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _payment_approval_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./payment-approval.page */ "./src/app/pages/payment-approval/payment-approval.page.ts");







const routes = [
    {
        path: '',
        component: _payment_approval_page__WEBPACK_IMPORTED_MODULE_6__["PaymentApprovalPage"]
    }
];
let PaymentApprovalPageModule = class PaymentApprovalPageModule {
};
PaymentApprovalPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_payment_approval_page__WEBPACK_IMPORTED_MODULE_6__["PaymentApprovalPage"]]
    })
], PaymentApprovalPageModule);



/***/ }),

/***/ "./src/app/pages/payment-approval/payment-approval.page.scss":
/*!*******************************************************************!*\
  !*** ./src/app/pages/payment-approval/payment-approval.page.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".button {\n  height: 50px;\n}\n\n.butt {\n  display: inline-table;\n  height: auto;\n  overflow: hidden;\n}\n\n.price {\n  color: rgba(219, 205, 8, 0.91);\n  font-size: 1.67em;\n  padding-top: 14px;\n  padding-bottom: 14px;\n  border-radius: 12px;\n}\n\n.price ion-icon {\n  font-size: 0.8em;\n  padding: 5px;\n  color: rgba(219, 205, 8, 0.91);\n}\n\n.location {\n  width: auto;\n  padding-top: 8px;\n  padding-bottom: 8px;\n}\n\n.location p {\n  font-size: 1.3em;\n  height: auto;\n}\n\n.location ion-icon {\n  font-size: 0.8em;\n  padding: 5px;\n  color: orange;\n}\n\n.date {\n  color: orange;\n  font-size: 1.47em;\n  padding-top: 14px;\n  padding-bottom: 14px;\n  border-bottom: 1px solid #d8d8d8;\n}\n\n.date ion-icon {\n  font-size: 0.8em;\n  padding: 5px;\n  color: rgba(219, 205, 8, 0.91);\n}\n\n.destination {\n  width: auto;\n  padding-top: 8px;\n  padding-bottom: 8px;\n}\n\n.destination ion-icon {\n  font-size: 0.8em;\n  padding: 5px;\n  color: darkslateblue;\n}\n\n#envelope {\n  height: auto;\n  width: 6em;\n}\n\n.bars {\n  margin-top: 0%;\n  padding: 12px;\n}\n\n.bars .poiter {\n  z-index: 5;\n  margin-left: 1%;\n  background: rgba(240, 240, 240, 0.92);\n  border-left: 1.1px solid #d8d8d8;\n  border-right: 1.1px solid #d8d8d8;\n  border-top: 1px solid #d8d8d8;\n  border-bottom: 1px solid #d8d8d8;\n}\n\n.bars .bars-locate {\n  height: 50px;\n  width: 100%;\n  background: rgba(240, 240, 240, 0.92);\n  border-left: 1.1px solid #d8d8d8;\n  border-right: 1.1px solid #d8d8d8;\n  border-top: 1px solid #d8d8d8;\n  border-bottom: 1px solid #d8d8d8;\n  margin-left: 0%;\n  z-index: 3;\n  border-radius: 12px;\n  overflow: hidden;\n  line-height: 20px;\n  font-size: 1.2em;\n  text-align: center;\n}\n\n.bars .bars-locate ion-icon {\n  position: absolute;\n  font-size: 1em;\n  left: 2%;\n  color: #0a64eb;\n  padding: 5px;\n}\n\n.bars .bars-destinate {\n  height: 50px;\n  width: 100%;\n  background: rgba(240, 240, 240, 0.92);\n  margin: 3% 0 0 -50px;\n  margin-left: 0%;\n  z-index: 3;\n  border-left: 1.1px solid #d8d8d8;\n  border-right: 1.1px solid #d8d8d8;\n  border-top: 1px solid #d8d8d8;\n  border-bottom: 1px solid #d8d8d8;\n  overflow: hidden;\n  border-radius: 12px;\n  line-height: 20px;\n  font-size: 1.2em;\n  text-align: center;\n}\n\n.bars .bars-destinate ion-icon {\n  position: absolute;\n  font-size: 1em;\n  left: 2%;\n  padding: 5px;\n  color: rgba(219, 205, 8, 0.91);\n}\n\n.bars .bars-price {\n  height: 50px;\n  width: 100%;\n  background: #ffffff;\n  border-left: 1.1px solid #d8d8d8;\n  border-right: 1.1px solid #d8d8d8;\n  border-top: 1px solid #d8d8d8;\n  border-bottom: 1px solid #d8d8d8;\n  z-index: 3;\n  border-radius: 12px;\n  overflow: hidden;\n  line-height: 20px;\n  font-size: 1.2em;\n  text-align: center;\n}\n\n.bars .bars-price ion-icon {\n  position: absolute;\n  font-size: 1em;\n  left: 2%;\n  color: #0a64eb;\n  padding: 5px;\n}\n\n#position {\n  text-align: center;\n  padding-left: 17px;\n}\n\n#whereto {\n  text-align: center;\n  padding-left: 17px;\n}\n\n#stuff {\n  color: #f53d3d;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYWhhL0lvbmljQXBwcy9DNFJpZGUtSU9TLTIwMjQvY2FsbDRyaWRlLXBhc3Nlbmdlci9zcmMvYXBwL3BhZ2VzL3BheW1lbnQtYXBwcm92YWwvcGF5bWVudC1hcHByb3ZhbC5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL3BheW1lbnQtYXBwcm92YWwvcGF5bWVudC1hcHByb3ZhbC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxZQUFBO0FDQ0Y7O0FEQ0E7RUFDRSxxQkFBQTtFQUNBLFlBQUE7RUFFQSxnQkFBQTtBQ0NGOztBREVBO0VBQ0UsOEJBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0VBQ0Esb0JBQUE7RUFFQSxtQkFBQTtBQ0FGOztBRENFO0VBQ0UsZ0JBQUE7RUFDQSxZQUFBO0VBQ0EsOEJBQUE7QUNDSjs7QURHQTtFQUNFLFdBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0FDQUY7O0FEQ0U7RUFDRSxnQkFBQTtFQUNBLFlBQUE7QUNDSjs7QURFRTtFQUNFLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7QUNBSjs7QURJQTtFQUNFLGFBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0VBQ0Esb0JBQUE7RUFDQSxnQ0FBQTtBQ0RGOztBREdFO0VBQ0UsZ0JBQUE7RUFDQSxZQUFBO0VBQ0EsOEJBQUE7QUNESjs7QURLQTtFQUNFLFdBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0FDRkY7O0FER0U7RUFDRSxnQkFBQTtFQUNBLFlBQUE7RUFDQSxvQkFBQTtBQ0RKOztBREtBO0VBQ0UsWUFBQTtFQUNBLFVBQUE7QUNGRjs7QURLQTtFQUNFLGNBQUE7RUFDQSxhQUFBO0FDRkY7O0FESUU7RUFDRSxVQUFBO0VBQ0EsZUFBQTtFQUNBLHFDQUFBO0VBQ0EsZ0NBQUE7RUFDQSxpQ0FBQTtFQUNBLDZCQUFBO0VBQ0EsZ0NBQUE7QUNGSjs7QURLRTtFQUNFLFlBQUE7RUFDQSxXQUFBO0VBQ0EscUNBQUE7RUFDQSxnQ0FBQTtFQUNBLGlDQUFBO0VBQ0EsNkJBQUE7RUFDQSxnQ0FBQTtFQUNBLGVBQUE7RUFFQSxVQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBQ0pKOztBRE1JO0VBQ0Usa0JBQUE7RUFDQSxjQUFBO0VBQ0EsUUFBQTtFQUNBLGNBQUE7RUFDQSxZQUFBO0FDSk47O0FEUUU7RUFDRSxZQUFBO0VBQ0EsV0FBQTtFQUNBLHFDQUFBO0VBQ0Esb0JBQUE7RUFDQSxlQUFBO0VBQ0EsVUFBQTtFQUNBLGdDQUFBO0VBQ0EsaUNBQUE7RUFDQSw2QkFBQTtFQUNBLGdDQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBQ05KOztBRFFJO0VBQ0Usa0JBQUE7RUFDQSxjQUFBO0VBQ0EsUUFBQTtFQUNBLFlBQUE7RUFDQSw4QkFBQTtBQ05OOztBRFVFO0VBQ0UsWUFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtFQUNBLGdDQUFBO0VBQ0EsaUNBQUE7RUFDQSw2QkFBQTtFQUNBLGdDQUFBO0VBR0EsVUFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUNWSjs7QURZSTtFQUNFLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLFFBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtBQ1ZOOztBRGVBO0VBQ0Usa0JBQUE7RUFDQSxrQkFBQTtBQ1pGOztBRGVBO0VBQ0Usa0JBQUE7RUFDQSxrQkFBQTtBQ1pGOztBRGVBO0VBQ0UsY0FBQTtBQ1pGIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvcGF5bWVudC1hcHByb3ZhbC9wYXltZW50LWFwcHJvdmFsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5idXR0b24ge1xyXG4gIGhlaWdodDogNTBweDtcclxufVxyXG4uYnV0dCB7XHJcbiAgZGlzcGxheTogaW5saW5lLXRhYmxlO1xyXG4gIGhlaWdodDogYXV0bztcclxuXHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxufVxyXG5cclxuLnByaWNlIHtcclxuICBjb2xvcjogcmdiYSgyMTksIDIwNSwgOCwgMC45MSk7XHJcbiAgZm9udC1zaXplOiAxLjY3ZW07XHJcbiAgcGFkZGluZy10b3A6IDE0cHg7XHJcbiAgcGFkZGluZy1ib3R0b206IDE0cHg7XHJcblxyXG4gIGJvcmRlci1yYWRpdXM6IDEycHg7XHJcbiAgaW9uLWljb24ge1xyXG4gICAgZm9udC1zaXplOiAwLjhlbTtcclxuICAgIHBhZGRpbmc6IDVweDtcclxuICAgIGNvbG9yOiByZ2JhKDIxOSwgMjA1LCA4LCAwLjkxKTtcclxuICB9XHJcbn1cclxuXHJcbi5sb2NhdGlvbiB7XHJcbiAgd2lkdGg6IGF1dG87XHJcbiAgcGFkZGluZy10b3A6IDhweDtcclxuICBwYWRkaW5nLWJvdHRvbTogOHB4O1xyXG4gIHAge1xyXG4gICAgZm9udC1zaXplOiAxLjNlbTtcclxuICAgIGhlaWdodDogYXV0bztcclxuICB9XHJcblxyXG4gIGlvbi1pY29uIHtcclxuICAgIGZvbnQtc2l6ZTogMC44ZW07XHJcbiAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICBjb2xvcjogb3JhbmdlO1xyXG4gIH1cclxufVxyXG5cclxuLmRhdGUge1xyXG4gIGNvbG9yOiBvcmFuZ2U7XHJcbiAgZm9udC1zaXplOiAxLjQ3ZW07XHJcbiAgcGFkZGluZy10b3A6IDE0cHg7XHJcbiAgcGFkZGluZy1ib3R0b206IDE0cHg7XHJcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHJnYigyMTYsIDIxNiwgMjE2KTtcclxuXHJcbiAgaW9uLWljb24ge1xyXG4gICAgZm9udC1zaXplOiAwLjhlbTtcclxuICAgIHBhZGRpbmc6IDVweDtcclxuICAgIGNvbG9yOiByZ2JhKDIxOSwgMjA1LCA4LCAwLjkxKTtcclxuICB9XHJcbn1cclxuXHJcbi5kZXN0aW5hdGlvbiB7XHJcbiAgd2lkdGg6IGF1dG87XHJcbiAgcGFkZGluZy10b3A6IDhweDtcclxuICBwYWRkaW5nLWJvdHRvbTogOHB4O1xyXG4gIGlvbi1pY29uIHtcclxuICAgIGZvbnQtc2l6ZTogMC44ZW07XHJcbiAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICBjb2xvcjogZGFya3NsYXRlYmx1ZTtcclxuICB9XHJcbn1cclxuXHJcbiNlbnZlbG9wZSB7XHJcbiAgaGVpZ2h0OiBhdXRvO1xyXG4gIHdpZHRoOiA2ZW07XHJcbn1cclxuXHJcbi5iYXJzIHtcclxuICBtYXJnaW4tdG9wOiAwJTtcclxuICBwYWRkaW5nOiAxMnB4O1xyXG5cclxuICAucG9pdGVyIHtcclxuICAgIHotaW5kZXg6IDU7XHJcbiAgICBtYXJnaW4tbGVmdDogMSU7XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDI0MCwgMjQwLCAyNDAsIDAuOTIpO1xyXG4gICAgYm9yZGVyLWxlZnQ6IDEuMXB4IHNvbGlkIHJnYigyMTYsIDIxNiwgMjE2KTtcclxuICAgIGJvcmRlci1yaWdodDogMS4xcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xyXG4gICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkIHJnYigyMTYsIDIxNiwgMjE2KTtcclxuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNik7XHJcbiAgfVxyXG5cclxuICAuYmFycy1sb2NhdGUge1xyXG4gICAgaGVpZ2h0OiA1MHB4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDI0MCwgMjQwLCAyNDAsIDAuOTIpO1xyXG4gICAgYm9yZGVyLWxlZnQ6IDEuMXB4IHNvbGlkIHJnYigyMTYsIDIxNiwgMjE2KTtcclxuICAgIGJvcmRlci1yaWdodDogMS4xcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xyXG4gICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkIHJnYigyMTYsIDIxNiwgMjE2KTtcclxuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNik7XHJcbiAgICBtYXJnaW4tbGVmdDogMCU7XHJcblxyXG4gICAgei1pbmRleDogMztcclxuICAgIGJvcmRlci1yYWRpdXM6IDEycHg7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgbGluZS1oZWlnaHQ6IDIwcHg7XHJcbiAgICBmb250LXNpemU6IDEuMmVtO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG5cclxuICAgIGlvbi1pY29uIHtcclxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICBmb250LXNpemU6IDFlbTtcclxuICAgICAgbGVmdDogMiU7XHJcbiAgICAgIGNvbG9yOiByZ2IoMTAsIDEwMCwgMjM1KTtcclxuICAgICAgcGFkZGluZzogNXB4O1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLmJhcnMtZGVzdGluYXRlIHtcclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYmFja2dyb3VuZDogcmdiYSgyNDAsIDI0MCwgMjQwLCAwLjkyKTtcclxuICAgIG1hcmdpbjogMyUgMCAwIC01MHB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDAlO1xyXG4gICAgei1pbmRleDogMztcclxuICAgIGJvcmRlci1sZWZ0OiAxLjFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNik7XHJcbiAgICBib3JkZXItcmlnaHQ6IDEuMXB4IHNvbGlkIHJnYigyMTYsIDIxNiwgMjE2KTtcclxuICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNik7XHJcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDEycHg7XHJcbiAgICBsaW5lLWhlaWdodDogMjBweDtcclxuICAgIGZvbnQtc2l6ZTogMS4yZW07XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcblxyXG4gICAgaW9uLWljb24ge1xyXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgIGZvbnQtc2l6ZTogMWVtO1xyXG4gICAgICBsZWZ0OiAyJTtcclxuICAgICAgcGFkZGluZzogNXB4O1xyXG4gICAgICBjb2xvcjogcmdiYSgyMTksIDIwNSwgOCwgMC45MSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAuYmFycy1wcmljZSB7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGJhY2tncm91bmQ6ICNmZmZmZmY7XHJcbiAgICBib3JkZXItbGVmdDogMS4xcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xyXG4gICAgYm9yZGVyLXJpZ2h0OiAxLjFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNik7XHJcbiAgICBib3JkZXItdG9wOiAxcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHJnYigyMTYsIDIxNiwgMjE2KTtcclxuXHJcblxyXG4gICAgei1pbmRleDogMztcclxuICAgIGJvcmRlci1yYWRpdXM6IDEycHg7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgbGluZS1oZWlnaHQ6IDIwcHg7XHJcbiAgICBmb250LXNpemU6IDEuMmVtO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG5cclxuICAgIGlvbi1pY29uIHtcclxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICBmb250LXNpemU6IDFlbTtcclxuICAgICAgbGVmdDogMiU7XHJcbiAgICAgIGNvbG9yOiByZ2IoMTAsIDEwMCwgMjM1KTtcclxuICAgICAgcGFkZGluZzogNXB4O1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuI3Bvc2l0aW9uIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgcGFkZGluZy1sZWZ0OiAxN3B4O1xyXG59XHJcblxyXG4jd2hlcmV0byB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIHBhZGRpbmctbGVmdDogMTdweDtcclxufVxyXG5cclxuI3N0dWZmIHtcclxuICBjb2xvcjogI2Y1M2QzZDtcclxufVxyXG4iLCIuYnV0dG9uIHtcbiAgaGVpZ2h0OiA1MHB4O1xufVxuXG4uYnV0dCB7XG4gIGRpc3BsYXk6IGlubGluZS10YWJsZTtcbiAgaGVpZ2h0OiBhdXRvO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuXG4ucHJpY2Uge1xuICBjb2xvcjogcmdiYSgyMTksIDIwNSwgOCwgMC45MSk7XG4gIGZvbnQtc2l6ZTogMS42N2VtO1xuICBwYWRkaW5nLXRvcDogMTRweDtcbiAgcGFkZGluZy1ib3R0b206IDE0cHg7XG4gIGJvcmRlci1yYWRpdXM6IDEycHg7XG59XG4ucHJpY2UgaW9uLWljb24ge1xuICBmb250LXNpemU6IDAuOGVtO1xuICBwYWRkaW5nOiA1cHg7XG4gIGNvbG9yOiByZ2JhKDIxOSwgMjA1LCA4LCAwLjkxKTtcbn1cblxuLmxvY2F0aW9uIHtcbiAgd2lkdGg6IGF1dG87XG4gIHBhZGRpbmctdG9wOiA4cHg7XG4gIHBhZGRpbmctYm90dG9tOiA4cHg7XG59XG4ubG9jYXRpb24gcCB7XG4gIGZvbnQtc2l6ZTogMS4zZW07XG4gIGhlaWdodDogYXV0bztcbn1cbi5sb2NhdGlvbiBpb24taWNvbiB7XG4gIGZvbnQtc2l6ZTogMC44ZW07XG4gIHBhZGRpbmc6IDVweDtcbiAgY29sb3I6IG9yYW5nZTtcbn1cblxuLmRhdGUge1xuICBjb2xvcjogb3JhbmdlO1xuICBmb250LXNpemU6IDEuNDdlbTtcbiAgcGFkZGluZy10b3A6IDE0cHg7XG4gIHBhZGRpbmctYm90dG9tOiAxNHB4O1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2Q4ZDhkODtcbn1cbi5kYXRlIGlvbi1pY29uIHtcbiAgZm9udC1zaXplOiAwLjhlbTtcbiAgcGFkZGluZzogNXB4O1xuICBjb2xvcjogcmdiYSgyMTksIDIwNSwgOCwgMC45MSk7XG59XG5cbi5kZXN0aW5hdGlvbiB7XG4gIHdpZHRoOiBhdXRvO1xuICBwYWRkaW5nLXRvcDogOHB4O1xuICBwYWRkaW5nLWJvdHRvbTogOHB4O1xufVxuLmRlc3RpbmF0aW9uIGlvbi1pY29uIHtcbiAgZm9udC1zaXplOiAwLjhlbTtcbiAgcGFkZGluZzogNXB4O1xuICBjb2xvcjogZGFya3NsYXRlYmx1ZTtcbn1cblxuI2VudmVsb3BlIHtcbiAgaGVpZ2h0OiBhdXRvO1xuICB3aWR0aDogNmVtO1xufVxuXG4uYmFycyB7XG4gIG1hcmdpbi10b3A6IDAlO1xuICBwYWRkaW5nOiAxMnB4O1xufVxuLmJhcnMgLnBvaXRlciB7XG4gIHotaW5kZXg6IDU7XG4gIG1hcmdpbi1sZWZ0OiAxJTtcbiAgYmFja2dyb3VuZDogcmdiYSgyNDAsIDI0MCwgMjQwLCAwLjkyKTtcbiAgYm9yZGVyLWxlZnQ6IDEuMXB4IHNvbGlkICNkOGQ4ZDg7XG4gIGJvcmRlci1yaWdodDogMS4xcHggc29saWQgI2Q4ZDhkODtcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNkOGQ4ZDg7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZDhkOGQ4O1xufVxuLmJhcnMgLmJhcnMtbG9jYXRlIHtcbiAgaGVpZ2h0OiA1MHB4O1xuICB3aWR0aDogMTAwJTtcbiAgYmFja2dyb3VuZDogcmdiYSgyNDAsIDI0MCwgMjQwLCAwLjkyKTtcbiAgYm9yZGVyLWxlZnQ6IDEuMXB4IHNvbGlkICNkOGQ4ZDg7XG4gIGJvcmRlci1yaWdodDogMS4xcHggc29saWQgI2Q4ZDhkODtcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNkOGQ4ZDg7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZDhkOGQ4O1xuICBtYXJnaW4tbGVmdDogMCU7XG4gIHotaW5kZXg6IDM7XG4gIGJvcmRlci1yYWRpdXM6IDEycHg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICBmb250LXNpemU6IDEuMmVtO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uYmFycyAuYmFycy1sb2NhdGUgaW9uLWljb24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGZvbnQtc2l6ZTogMWVtO1xuICBsZWZ0OiAyJTtcbiAgY29sb3I6ICMwYTY0ZWI7XG4gIHBhZGRpbmc6IDVweDtcbn1cbi5iYXJzIC5iYXJzLWRlc3RpbmF0ZSB7XG4gIGhlaWdodDogNTBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIGJhY2tncm91bmQ6IHJnYmEoMjQwLCAyNDAsIDI0MCwgMC45Mik7XG4gIG1hcmdpbjogMyUgMCAwIC01MHB4O1xuICBtYXJnaW4tbGVmdDogMCU7XG4gIHotaW5kZXg6IDM7XG4gIGJvcmRlci1sZWZ0OiAxLjFweCBzb2xpZCAjZDhkOGQ4O1xuICBib3JkZXItcmlnaHQ6IDEuMXB4IHNvbGlkICNkOGQ4ZDg7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCAjZDhkOGQ4O1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2Q4ZDhkODtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcbiAgbGluZS1oZWlnaHQ6IDIwcHg7XG4gIGZvbnQtc2l6ZTogMS4yZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5iYXJzIC5iYXJzLWRlc3RpbmF0ZSBpb24taWNvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgZm9udC1zaXplOiAxZW07XG4gIGxlZnQ6IDIlO1xuICBwYWRkaW5nOiA1cHg7XG4gIGNvbG9yOiByZ2JhKDIxOSwgMjA1LCA4LCAwLjkxKTtcbn1cbi5iYXJzIC5iYXJzLXByaWNlIHtcbiAgaGVpZ2h0OiA1MHB4O1xuICB3aWR0aDogMTAwJTtcbiAgYmFja2dyb3VuZDogI2ZmZmZmZjtcbiAgYm9yZGVyLWxlZnQ6IDEuMXB4IHNvbGlkICNkOGQ4ZDg7XG4gIGJvcmRlci1yaWdodDogMS4xcHggc29saWQgI2Q4ZDhkODtcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNkOGQ4ZDg7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZDhkOGQ4O1xuICB6LWluZGV4OiAzO1xuICBib3JkZXItcmFkaXVzOiAxMnB4O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBsaW5lLWhlaWdodDogMjBweDtcbiAgZm9udC1zaXplOiAxLjJlbTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLmJhcnMgLmJhcnMtcHJpY2UgaW9uLWljb24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGZvbnQtc2l6ZTogMWVtO1xuICBsZWZ0OiAyJTtcbiAgY29sb3I6ICMwYTY0ZWI7XG4gIHBhZGRpbmc6IDVweDtcbn1cblxuI3Bvc2l0aW9uIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwYWRkaW5nLWxlZnQ6IDE3cHg7XG59XG5cbiN3aGVyZXRvIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwYWRkaW5nLWxlZnQ6IDE3cHg7XG59XG5cbiNzdHVmZiB7XG4gIGNvbG9yOiAjZjUzZDNkO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/payment-approval/payment-approval.page.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/payment-approval/payment-approval.page.ts ***!
  \*****************************************************************/
/*! exports provided: PaymentApprovalPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentApprovalPage", function() { return PaymentApprovalPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/pop-up.service */ "./src/app/services/pop-up.service.ts");
/* harmony import */ var src_app_services_language_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/language.service */ "./src/app/services/language.service.ts");
/* harmony import */ var src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/settings.service */ "./src/app/services/settings.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");







let PaymentApprovalPage = class PaymentApprovalPage {
    constructor(navCtrl, lp, settings, pop, actRoute, modalCtrl) {
        this.navCtrl = navCtrl;
        this.lp = lp;
        this.settings = settings;
        this.pop = pop;
        this.actRoute = actRoute;
        this.modalCtrl = modalCtrl;
        this.from = this.actRoute.snapshot.paramMap.get('from');
        this.to = this.actRoute.snapshot.paramMap.get('to');
        this.charge = this.actRoute.snapshot.paramMap.get('charge');
        this.info = this.actRoute.snapshot.paramMap.get('info');
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad payment-approval');
    }
    closeModal() {
        this.modalCtrl.dismiss();
    }
    ngOnInit() {
    }
};
PaymentApprovalPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: src_app_services_language_service__WEBPACK_IMPORTED_MODULE_4__["LanguageService"] },
    { type: src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_5__["SettingsService"] },
    { type: src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_3__["PopUpService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] }
];
PaymentApprovalPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-payment-approval',
        template: __webpack_require__(/*! raw-loader!./payment-approval.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/payment-approval/payment-approval.page.html"),
        styles: [__webpack_require__(/*! ./payment-approval.page.scss */ "./src/app/pages/payment-approval/payment-approval.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], src_app_services_language_service__WEBPACK_IMPORTED_MODULE_4__["LanguageService"], src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_5__["SettingsService"],
        src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_3__["PopUpService"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])
], PaymentApprovalPage);



/***/ })

}]);
//# sourceMappingURL=pages-payment-approval-payment-approval-module-es2015.js.map