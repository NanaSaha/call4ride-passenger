(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-cancelled-cancelled-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/cancelled/cancelled.page.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/cancelled/cancelled.page.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header no-border>\n  <ion-toolbar style=\"margin-top: 20px\">\n    <ion-button (click)=\"goBack()\" ion-button color=\"primary\" fill='clear'>\n      <ion-icon style=\"font-size: 1.5em\" name=\"arrow-back\"></ion-icon> <span\n        style=\"margin-left:30px; font-size: 1.4em\">Cancelled Rides</span>\n    </ion-button>\n\n  </ion-toolbar>\n\n</ion-header>\n\n\n<ion-content padding  class='yes-scroll'>\n  <div class='followed-items'>\n    <ion-list>\n      <div>\n        <ion-item class='hists' lines='none' *ngFor=\"let event of eventList\">\n          <ion-grid>\n            <ion-row>\n              <ion-col>\n                <ion-label  class='drive'><strong>{{event?.name}}</strong></ion-label>\n\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col>\n                <ion-label class='price'><strong>{{settings.appcurrency}}{{event?.price}}</strong></ion-label>\n\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col>\n                <ion-label class='date'><strong>{{event?.date}}</strong></ion-label>\n\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col>\n                <ion-label class='location'><strong>{{event?.location}}</strong></ion-label>\n\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col>\n                <ion-label class='destination'><strong>{{event?.destination}}</strong></ion-label>\n\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col>\n                <ion-label  class='drive'>Charge: <strong>{{event?.charge}}</strong></ion-label>\n\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col>\n                <ion-label  class='date'>Reason: <strong>{{event?.reason}}</strong></ion-label>\n\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n\n\n        </ion-item>\n      </div>\n    </ion-list>\n  </div>\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/cancelled/cancelled.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/cancelled/cancelled.module.ts ***!
  \*****************************************************/
/*! exports provided: CancelledPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CancelledPageModule", function() { return CancelledPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _cancelled_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./cancelled.page */ "./src/app/pages/cancelled/cancelled.page.ts");







var routes = [
    {
        path: '',
        component: _cancelled_page__WEBPACK_IMPORTED_MODULE_6__["CancelledPage"]
    }
];
var CancelledPageModule = /** @class */ (function () {
    function CancelledPageModule() {
    }
    CancelledPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_cancelled_page__WEBPACK_IMPORTED_MODULE_6__["CancelledPage"]]
        })
    ], CancelledPageModule);
    return CancelledPageModule;
}());



/***/ }),

/***/ "./src/app/pages/cancelled/cancelled.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/pages/cancelled/cancelled.page.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content .drive {\n  color: red;\n}\nion-content .price {\n  color: #8fff8f;\n}\nion-content .date {\n  color: #ffc8b4;\n}\nion-content .destination {\n  color: #75faff;\n}\nion-content .location {\n  color: whitesmoke;\n}\nion-content ion-item {\n  margin-top: 24px;\n  border-bottom: 1px solid #d8d8d8 !important;\n}\nion-content .hists {\n  border: 1px solid;\n  margin-top: 8px;\n  border-radius: 12px;\n  --background: blue;\n  color: light;\n}\nion-content .button {\n  color: blue;\n}\nion-content .button .icon {\n  color: blue;\n}\nion-content .button .icons {\n  color: #fdfdfd;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYWhhL0lvbmljQXBwcy9DNFJpZGUtSU9TLTIwMjQvY2FsbDRyaWRlLXBhc3Nlbmdlci9zcmMvYXBwL3BhZ2VzL2NhbmNlbGxlZC9jYW5jZWxsZWQucGFnZS5zY3NzIiwic3JjL2FwcC9wYWdlcy9jYW5jZWxsZWQvY2FuY2VsbGVkLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFHTTtFQUNJLFVBQUE7QUNGVjtBREtNO0VBQ0UsY0FBQTtBQ0hSO0FETU07RUFDRSxjQUFBO0FDSlI7QURPTTtFQUNFLGNBQUE7QUNMUjtBRFFNO0VBQ0UsaUJBQUE7QUNOUjtBRFVRO0VBQ0UsZ0JBQUE7RUFFQSwyQ0FBQTtBQ1RWO0FEYU07RUFDRSxpQkFBQTtFQUVBLGVBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtBQ1pSO0FEZU07RUFDRSxXQUFBO0FDYlI7QURjUTtFQUNFLFdBQUE7QUNaVjtBRGNRO0VBQ0UsY0FBQTtBQ1pWIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvY2FuY2VsbGVkL2NhbmNlbGxlZC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuICAgIGlvbi1jb250ZW50e1xyXG4gIFxyXG4gICAgICAuZHJpdmV7XHJcbiAgICAgICAgICBjb2xvcjogcmVkO1xyXG4gICAgICB9XHJcbiAgXHJcbiAgICAgIC5wcmljZXtcclxuICAgICAgICBjb2xvcjogcmdiKDE0MywgMjU1LCAxNDMpO1xyXG4gICAgICB9XHJcbiAgXHJcbiAgICAgIC5kYXRle1xyXG4gICAgICAgIGNvbG9yOiByZ2IoMjU1LCAyMDAsIDE4MCk7XHJcbiAgICAgIH1cclxuICBcclxuICAgICAgLmRlc3RpbmF0aW9ue1xyXG4gICAgICAgIGNvbG9yOiByZ2IoMTE3LCAyNTAsIDI1NSk7XHJcbiAgICAgIH1cclxuICBcclxuICAgICAgLmxvY2F0aW9ue1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZXNtb2tlO1xyXG4gICAgICB9XHJcbiAgXHJcbiAgICAgIFxyXG4gICAgICAgIGlvbi1pdGVte1xyXG4gICAgICAgICAgbWFyZ2luLXRvcDogMjRweDtcclxuICAgICAgICAgLy8gbWFyZ2luLWJvdHRvbTogMTVweDtcclxuICAgICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNikgIWltcG9ydGFudDtcclxuICAgICAgfVxyXG4gIFxyXG4gIFxyXG4gICAgICAuaGlzdHN7XHJcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgO1xyXG4gICAgICAgIFxyXG4gICAgICAgIG1hcmdpbi10b3A6IDhweDtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDogYmx1ZTtcclxuICAgICAgICBjb2xvcjogbGlnaHQ7XHJcbiAgICAgIH1cclxuICBcclxuICAgICAgLmJ1dHRvbntcclxuICAgICAgICBjb2xvcjogYmx1ZTtcclxuICAgICAgICAuaWNvbntcclxuICAgICAgICAgIGNvbG9yOiBibHVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICAuaWNvbnN7XHJcbiAgICAgICAgICBjb2xvcjogI2ZkZmRmZDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgIFxyXG4gICAgIFxyXG4gIH1cclxuICBcclxuICAiLCJpb24tY29udGVudCAuZHJpdmUge1xuICBjb2xvcjogcmVkO1xufVxuaW9uLWNvbnRlbnQgLnByaWNlIHtcbiAgY29sb3I6ICM4ZmZmOGY7XG59XG5pb24tY29udGVudCAuZGF0ZSB7XG4gIGNvbG9yOiAjZmZjOGI0O1xufVxuaW9uLWNvbnRlbnQgLmRlc3RpbmF0aW9uIHtcbiAgY29sb3I6ICM3NWZhZmY7XG59XG5pb24tY29udGVudCAubG9jYXRpb24ge1xuICBjb2xvcjogd2hpdGVzbW9rZTtcbn1cbmlvbi1jb250ZW50IGlvbi1pdGVtIHtcbiAgbWFyZ2luLXRvcDogMjRweDtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNkOGQ4ZDggIWltcG9ydGFudDtcbn1cbmlvbi1jb250ZW50IC5oaXN0cyB7XG4gIGJvcmRlcjogMXB4IHNvbGlkO1xuICBtYXJnaW4tdG9wOiA4cHg7XG4gIGJvcmRlci1yYWRpdXM6IDEycHg7XG4gIC0tYmFja2dyb3VuZDogYmx1ZTtcbiAgY29sb3I6IGxpZ2h0O1xufVxuaW9uLWNvbnRlbnQgLmJ1dHRvbiB7XG4gIGNvbG9yOiBibHVlO1xufVxuaW9uLWNvbnRlbnQgLmJ1dHRvbiAuaWNvbiB7XG4gIGNvbG9yOiBibHVlO1xufVxuaW9uLWNvbnRlbnQgLmJ1dHRvbiAuaWNvbnMge1xuICBjb2xvcjogI2ZkZmRmZDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/cancelled/cancelled.page.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/cancelled/cancelled.page.ts ***!
  \***************************************************/
/*! exports provided: CancelledPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CancelledPage", function() { return CancelledPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/profile.service */ "./src/app/services/profile.service.ts");
/* harmony import */ var src_app_services_language_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/language.service */ "./src/app/services/language.service.ts");
/* harmony import */ var src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/settings.service */ "./src/app/services/settings.service.ts");
/* harmony import */ var src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/pop-up.service */ "./src/app/services/pop-up.service.ts");
/* harmony import */ var src_app_services_event_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/event.service */ "./src/app/services/event.service.ts");








var CancelledPage = /** @class */ (function () {
    function CancelledPage(navCtrl, ph, alertCtrl, lp, settings, pop, load, eventProvider) {
        this.navCtrl = navCtrl;
        this.ph = ph;
        this.alertCtrl = alertCtrl;
        this.lp = lp;
        this.settings = settings;
        this.pop = pop;
        this.load = load;
        this.eventProvider = eventProvider;
    }
    CancelledPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.pop.presentLoader('').then(function () {
            _this.pop.hideLoader();
        });
        this.eventProvider.getCancelledList().on('value', function (snapshot) {
            _this.eventList = [];
            console.log(snapshot.val());
            snapshot.forEach(function (snap) {
                _this.eventList.push({
                    id: snap.key,
                    name: snap.val().name,
                    price: snap.val().price,
                    date: snap.val().date,
                    location: snap.val().location,
                    destination: snap.val().destination,
                    tip: snap.val().tip,
                    upvote: snap.val().upvote,
                    downvote: snap.val().downvote,
                    charge: snap.val().charge,
                    reason: snap.val().reason
                });
                _this.eventList.sort();
                _this.eventList.reverse();
                return false;
            });
        });
    };
    CancelledPage.prototype.goBack = function () {
        this.navCtrl.navigateRoot('history');
    };
    CancelledPage.prototype.goToEventDetail = function (eventId) {
        this.navCtrl.navigateRoot(['historydetail', { 'eventId': eventId }]);
    };
    CancelledPage.prototype.OpenCancelled = function () {
        this.navCtrl.navigateRoot('cancelled');
    };
    CancelledPage.prototype.TipMe = function (id) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            message: 'Enter Ammount in $',
                            inputs: [
                                {
                                    value: ''
                                },
                            ],
                            buttons: [
                                {
                                    text: this.lp.translate()[0].cancel,
                                },
                                {
                                    text: this.lp.translate()[0].accept,
                                    handler: function (data) {
                                        console.log(data[0]);
                                        _this.ph.updateHistoryTip(data[0], id).then(function () {
                                            _this.pop.showPimp('Driver has Been Tipped');
                                        });
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    CancelledPage.prototype.ngOnInit = function () {
    };
    CancelledPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
        { type: src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_3__["ProfileService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
        { type: src_app_services_language_service__WEBPACK_IMPORTED_MODULE_4__["LanguageService"] },
        { type: src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_5__["SettingsService"] },
        { type: src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_6__["PopUpService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
        { type: src_app_services_event_service__WEBPACK_IMPORTED_MODULE_7__["EventService"] }
    ]; };
    CancelledPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-cancelled',
            template: __webpack_require__(/*! raw-loader!./cancelled.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/cancelled/cancelled.page.html"),
            styles: [__webpack_require__(/*! ./cancelled.page.scss */ "./src/app/pages/cancelled/cancelled.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_3__["ProfileService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"], src_app_services_language_service__WEBPACK_IMPORTED_MODULE_4__["LanguageService"], src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_5__["SettingsService"], src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_6__["PopUpService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"], src_app_services_event_service__WEBPACK_IMPORTED_MODULE_7__["EventService"]])
    ], CancelledPage);
    return CancelledPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-cancelled-cancelled-module-es5.js.map