import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { Routes, RouterModule } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { UpdateUsersInfoPage } from "./update-users-info.page";

const routes: Routes = [
  {
    path: "",
    component: UpdateUsersInfoPage,
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
  ],
  declarations: [UpdateUsersInfoPage],
})
export class UpdateUsersInfoPageModule {}
