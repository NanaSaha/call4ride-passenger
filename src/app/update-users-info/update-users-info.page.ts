import { Component, OnInit } from "@angular/core";
import {
  Platform,
  LoadingController,
  NavController,
  AlertController,
  MenuController,
  ActionSheetController,
} from "@ionic/angular";

import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { EmailValidator } from "src/validators/email";
import { LanguageService } from "src/app/services/language.service";
import { SettingsService } from "src/app/services/settings.service";
import { ProfileService } from "src/app/services/profile.service";
import { NativeMapContainerService } from "src/app/services/native-map-container.service";
import { AuthService } from "src/app/services/auth.service";
import firebase from "firebase";
import { ActivatedRoute } from "@angular/router";
import { Camera, CameraOptions } from "@ionic-native/camera/ngx";
import { PopUpService } from "src/app/services/pop-up.service";

@Component({
  selector: "app-update-users-info",
  templateUrl: "./update-users-info.page.html",
  styleUrls: ["./update-users-info.page.scss"],
})
export class UpdateUsersInfoPage implements OnInit {
  public updateForm: FormGroup;
  public initState = false;
  minSelectabledate: any;
  maxSelectabledate: any;
  date: any;
  captureDataUrl: string;
  public userProfileRef: firebase.database.Reference;
  loading: Promise<HTMLIonLoadingElement>;
  from_phone;
  phonenumber;
  
  public signupForm: any;
  step: any = 1;
  signupVal;
  jsonBody;
  email;
  constructor(
    public navCtrl: NavController,
    public lp: LanguageService,
    public settings: SettingsService,
    public ntP: NativeMapContainerService,
    public platform: Platform,
    public menu: MenuController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public authProvider: AuthService,
    public ph: ProfileService,
    public formBuilder: FormBuilder,
    private route: ActivatedRoute,
    public actionSheetCtrl: ActionSheetController,
    public camera: Camera,
    public pop: PopUpService,
    public _form: FormBuilder,
  ) {
    this.date = new Date();
    this.minSelectabledate = this.formatDate(this.date);
    this.maxSelectabledate = this.formatDatemax(this.date);
    this.userProfileRef = firebase.database().ref("/userProfile");
    this.menu.enable(false);
    this.updateForm = formBuilder.group({
      first_name: ["", Validators.compose([Validators.required])],
      last_name: ["", Validators.compose([Validators.required])],
      phonenumber: ["", Validators.compose([Validators.required])],
    
    });

    this.route.queryParams.subscribe((params) => {
      this.from_phone = params["from_phone"];
      console.log("FROM PHONE", this.from_phone);
    });
    console.log("PROFILE ID::", this.ph.id);

    const unsubscribe = firebase.auth().onAuthStateChanged((user) => {
      this.ph
        .getUserProfil()
        .child(this.ph.id)
        .on("value", (userProfileSnapshot) => {
          console.log("USER::", user);
          this.email = user.email;
          console.log("USER PROFILE SNAPSHOT::", userProfileSnapshot.val());
          console.log("PROFILE ID::", this.ph.id);
        });
    });
  }

  ngOnInit() {}

  async updateUser() {
    if (!this.updateForm.valid) {
      console.log(this.updateForm.value);
    } else {
      let email = this.updateForm.value.email;

      let phonenumber = this.updateForm.value.phonenumber;

      let first_name = this.updateForm.value.first_name;
      let last_name = this.updateForm.value.last_name;
    
      console.log("FIRST NAME" + first_name);
      let currentYear = new Date().getFullYear();
      //let unique = "GH" + currentYear + "R" + Math.floor(Date.now() / 1000);
      let unique = "GH" + currentYear + "R" + Math.floor(1000 + Math.random() * 9000);

      console.log("UPDATE FORM VALUES", this.updateForm.value);
      console.log("User ID::", this.ph.id);
      console.log("UNIQGUE NUMBER-->", unique);

      this.userProfileRef.child(this.ph.id).child("userInfo").update({
        first_name: first_name,
        last_name: last_name,
        email: this.email,
        phonenumber: phonenumber,
        unique_number: unique,
      });

      this.navCtrl.navigateRoot("home");

    }
  }

  goToBack(): void {
    this.navCtrl.navigateRoot("login");
  }

  submit() {

    this.step = this.step + 1;
    console.log("STEP NOW IS", this.step)
  }

  prev() {
    this.step = this.step - 1;
    console.log("STEP NOW IS", this.step)
  }

  login() {

    this.navCtrl.navigateRoot("login");
  }

  formatDate(date) {
    var d = new Date(date),
      month = "" + (d.getMonth() + 1),
      day = "" + d.getDate(),
      year = d.getFullYear();

    // console.log("year" + year + "and day = " + day);

    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;

    console.log("year" + year + "and day = " + day);

    return [year, month, day].join("-");
  }

  formatDatemax(date) {
    var d = new Date(date),
      month = "" + d.getMonth(),
      day = "" + d.getDate(),
      year = d.getFullYear() + 1;

    console.log("year" + year + "and day = " + day);

    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;

    console.log("year" + year + "and day = " + day);
    return [year, month, day].join("-");
  }

  async choosePic_1() {
    const actionSheet = await this.actionSheetCtrl.create({
      header: "Choose From",
      buttons: [
        {
          text: "Camera",
          icon: "ios-camera",
          handler: () => {
            this.capture();
          },
        },
        {
          text: "File",
          icon: "ios-folder",
          handler: () => {
            this.captureFromFile();
          },
        },
        {
          text: "Cancel",
          icon: "close",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          },
        },
      ],
    });
    actionSheet.present();
  }

  capture() {
    const options: CameraOptions = {
      quality: 40,
      sourceType: this.camera.PictureSourceType.CAMERA,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
    };

    this.camera.getPicture(options).then(
      (imageData) => {
        this.captureDataUrl = "data:image/jpeg;base64," + imageData;
        this.processProfilePicture(this.captureDataUrl);
      },
      (err) => {
        // Handle error
      }
    );
  }

  captureFromFile() {
    const options: CameraOptions = {
      quality: 40,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
    };

    this.camera.getPicture(options).then((imageData) => {
      this.captureDataUrl = "data:image/jpeg;base64," + imageData;

      this.processProfilePicture(this.captureDataUrl);
    });
  }

  async processProfilePicture(captureData) {
    const storageRef = firebase.storage().ref();
    // Create a timestamp as filename
    const filename = Math.floor(Date.now() / 1000);
    const loading = await this.loadingCtrl.create();
    await loading.present();
    // Create a reference to 'images/todays-date.jpg'
    const imageRef = storageRef.child(`userPictures/${filename}.jpg`);

    imageRef
      .putString(captureData, firebase.storage.StringFormat.DATA_URL)
      .then((snapshot) => {
        imageRef
          .getDownloadURL()
          .then((url) => {
            console.log(url);
            this.ph
              .UpdatePhoto(url)
              .then((success) => {
                console.log(url);
                loading.dismiss();
                console.log("done");
                // this.navCtrl.navigateRoot("home");
              })
              .catch((error) => {
                this.pop.presentToast(
                  "Check Your Internet Connection and try again"
                );
              });
          })
          .catch((error) => {
            this.pop.presentToast(
              "Check Your Internet Connection and try again"
            );
          });
      })
      .catch((error) => {
        this.pop.presentToast("Check Your Internet Connection and try again");
      });
  }
}
