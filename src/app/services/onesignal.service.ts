import { Injectable } from "@angular/core";
import { OneSignal } from "@ionic-native/onesignal/ngx";
import { PopUpService } from "./pop-up.service";
import { LanguageService } from "./language.service";

@Injectable({
  providedIn: "root",
})
export class OnesignalService {
  constructor(
    public One: OneSignal,
    public pop: PopUpService,
    public lp: LanguageService
  ) {}

  sendTest(id) {
    const notificationObj: any = {
      include_player_ids: [id],
      contents: { en: "TESTING POST" },
    };

    this.One.postNotification(notificationObj).then(
      (good) => {
        console.log(good);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  sendPush(id) {
    const notificationObj: any = {
      include_player_ids: [id],
      contents: { en: this.lp.translate()[0].picknote },
    };

    this.One.postNotification(notificationObj).then(
      (good) => {
        console.log(good);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  sendPushForAddTrip(id) {
    const notificationObj: any = {
      include_player_ids: [id],
      contents: { en: this.lp.translate()[0].passenger_add_trip },
    };

    this.One.postNotification(notificationObj).then(
      (good) => {
        console.log(good);
      },
      (error) => {
        console.log(error);
      }
    );
  }


  

  sendArrived(id) {
    const notificationObj: any = {
      include_player_ids: [id],
      contents: { en: "Driver Has Started Trip." },
    };
    this.One.postNotification(notificationObj).then(
      (good) => {
        console.log(good);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  sendEnd(id) {
    const notificationObj: any = {
      include_player_ids: [id],
      contents: { en: "Your Trip Has Ended." },
    };
    this.One.postNotification(notificationObj).then(
      (good) => {
        console.log(good);
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
