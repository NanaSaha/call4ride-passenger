import { Injectable, NgZone } from "@angular/core";
declare var google: any;
declare var plugin: any;
declare var geofire: any;
import { interval } from "rxjs";
import {
    GoogleMap,
    GoogleMapOptions,
    GoogleMaps,
    GoogleMapsEvent,
    LatLng,
    ILatLng,
    LatLngBounds,
    Marker
} from "@ionic-native/google-maps";
import { AlertController, Platform, ToastController } from "@ionic/angular";
import { ProfileService } from "./profile.service";
import { SettingsService } from "./settings.service";
import { GeocoderService } from "./geocoder.service";
import { EventService } from "./event.service";
import { Geolocation, PositionError, Geoposition } from "@ionic-native/geolocation/ngx";
import { Storage } from "@ionic/storage";


@Injectable({
    providedIn: "root",
})
export class NativeMapContainerService {
    map: GoogleMap;
    public onLocationbarHide: boolean = true;
    public onDestinatiobarHide: boolean = true;
    public lat: any;
    public lng: any;
    public startPos: any;
    public client: any;
    public driver: any;
    public CARS: any = [];
    public pause: boolean = true;
    public shove: boolean = true;
    public speed: number = 50; // km/h
    // public marker: any;
    marker: Marker;
    public canMess: boolean = true;
    public cars: any = [];
    public car_location: any = [];
    public car_notificationIds: Array<any>;
    public delay: number = 100;
    public hasRequested: boolean = false;
    public isCarAvailable: boolean = false;
    public uid: any;
    public norideavailable: boolean = false;
    public canShowchoiceTab: boolean = false;
    public actual_Lat: any;
    public noGps: boolean = false;
    public actual_Lng: any;
    public isLocationChange: boolean = false;
    public onGpsEnabled: boolean = false;
    isNavigate: boolean = false;
    locations: Array<any>;
    location: any;
    public executiveStance: any = "none";
    public tricycleStance: any = "none";
    public standardStance: any = "none";
    timer1: any;
    public closeDrivers: any = [];
    public mapLoadComplete: boolean = false;
    public driverCarType: number = 0;
    choseCar: boolean = false;
    public toggleNav: boolean = true;
    public isClear: boolean = false;
    public onbar: boolean = false;
    public driver_Point: any;
    public classic: boolean = false;
    public smallcar: boolean = false;
    public pool: boolean = false;
    public onbar1: boolean = false;
    public onbar2: boolean = false;
    public onbar3: boolean = false;
    public canShow: boolean = true;
    public toggleBtn: boolean = false;
    public onPointerHide: boolean = false;
    public userPos: any;
    public stopMovingUserDestination: boolean = false;
    public stopMovingUsertoDriver: boolean = false;
    public selected_destination_bar: boolean = false;
    mapElement: HTMLElement;
    public pan: number = 0;
    NotifyTimes: number = -1;
    canCheck: boolean = true;
    public isDriverAvailable: boolean = false;
    public detectCarChange: any;
    public detectUserChange: any;
    user_Point: any;
    public does: boolean = true;
    public carMarker: any = [];
    public ClearDetection: boolean = false;
    public hasDone: boolean = false;
    public hasStart: boolean = false;
    public hasShown: boolean = true;
    public correctLocationShown: boolean = false;
    public D_lat: any;
    public hasShow: boolean = true;
    public car: any;
    public started: boolean = false;
    public showDone: boolean = false;
    public hasbooked: boolean = false;
    public driverPrice: any;
    public locationChange: boolean = true;
    public hasAdded: boolean = false;
    public hasCompleted: boolean = false;
    public isDestination: boolean = false;
    public CloseCars: any = [];
    public hidelocator: boolean = true;
    myTime: any;
    ready: boolean = false;
    public D_lng: any;
    carMarkers: Promise<any>;
    id: any[];
    nodriver: boolean = true;
    isBooking: boolean = false;
    markers: Array<any>;
    polyOptions: any;
    keyEntered: any;
    exited: any;
    moved: any;
    hasMoved: boolean;
    radius: number = 3000;
    geoFireInstance: any;
    pope: number;
    watchPositionSubscription: Geolocation;
    watch;
    isComingToUser: boolean = true;
    Line: any;
    flag: any;
    driver_lat: any;
    driver_lng: any;
    destination: LatLng;
    goto_passenger_watcher: number;
    route: any[];
    destination_route: any[];
    directionsService: any = new google.maps.DirectionsService();
    directionsDisplay = new google.maps.DirectionsRenderer();
    Line2: any;
    canDo: boolean = true;
    isDropped: boolean = false;
    totalDistance: void;

    eventList: any[];
    pop: any;
    frankdejong: any = false;
    driverRejected: boolean = true;
    hasPushed: boolean = false;
    allcar_array: any[];
    isDone: boolean = true;
    isWalk: boolean = false;

    isPickedUp: boolean;
    my_location: any;
    my_destination: any;
    routeNumber: any = 0;
    driverHere: boolean;
    intervalea: any;
    myLng: number;
    myLat: number;
    allmarkers: any;
    MyLat: number;
    MyLng: number;
    destination_tracker_loop: any;
    location_tracker_loop: any;
    should: any = true;
    flag_Lat: any;
    flag_Lng: any;
    StopLocUpdate: boolean;
    currentToll: number;
    isDestinationChange: any;
    route2: ILatLng[];
    route22: any = [];
    actualPrice: any = 0;
    myTolls: any[] = [];
    surge: any = 0;
    myVal: any;
    new_destination;
    origin;
    circle;
    yellow_markersArray = [];
    driver_markersArray = [];
    client_markersArray = [];
    flag_markersArray = [];
    car_markersArray = [];
    rider_markersArray = [];
    passenger;
    rider_lat;
    rider_lng;
    watch2;
    watch_to_pick;
    watch_to_drop;
    watch_restart;
    rider_bearing: any;
    driver_bearing: any;
    // this.marker: Marker;
    constructor(
        public eventProvider: EventService,
        public toastCtrl: ToastController,
        public alert: AlertController,
        public settings: SettingsService,
        public zone: NgZone,
        public myProf: ProfileService,
        public gcode: GeocoderService,
        public platform: Platform,
        private geo: Geolocation,
        public storage: Storage,
    ) {
        if (!this.platform.is("cordova")) {
            this.lat = 5.4966964;
            this.lng = 7.5297323;
        }


    }

 

    ///Start the cordova map
    loadMap() {

        

        let lat;
        let lng;
        let zoom;
        lat = 5.6027352;
        lng = -0.2095918;
        zoom = 19;

        let mapOptions: GoogleMapOptions = {
            camera: {
                target: {
                    lat: 5.614818,
                    lng: -0.205874,
                },
                zoom: zoom,
                tilt: 0,
            },
        };

        if (!this.platform.is("cordova")) {
            
            this.Geofiring();
            this.hasShown = true;
            this.lat = 40.65563;
            this.lng = -73.95025;
            this.gcode.locationName = "Devdex Software";
        }

        this.map = GoogleMaps.create("map", mapOptions);

        this.map.one(GoogleMapsEvent.MAP_READY).then(() => {          
           
            this.map.setMyLocationEnabled(true)
            this.map.setAllGesturesEnabled(true)
            this.map.setClickable(true)
            this.map.setCompassEnabled(true)

           
            this.hasStart = true;

            // this.directionsDisplay.setMap(this.map);



            this.watch2 = this.geo.watchPosition({ enableHighAccuracy: true, timeout: 10000 }).subscribe(position => {

                if ((position as Geoposition).coords != undefined) {
                    var geoposition = (position as Geoposition);
                

                  
                    this.location = geoposition;
                    this.correctLocationShown = true

                    this.lat = geoposition.coords.latitude,
                    this.lng = geoposition.coords.longitude;
                    
                    this.storage.set("cMap.lat", this.lat);
                    this.storage.set("cMap.lng", this.lng);

                  

                    this.gcode.Reverse_Geocode(
                        this.lat,
                        this.lng,
                        false
                    );
                    this.userPos = new google.maps.LatLng(
                        this.lat,
                        this.lng,
                    );
                    this.AnimateToLoc(this.lat, this.lng);



                } else {
                    var positionError = (position as PositionError);
                    // console.log('Error ' + positionError.code + ': ' + positionError.message);

                    // this.presentToast("Finding it difficult to get your current location. Please wait..");

                }
            });

        });
    }

    //check if gps is available by trying to getlocation info which automatically handles everything
    Restart() {

        this.isDropped = false;

        if (this.platform.is("cordova")) {

         


            this.map.getMyLocation().then(location => {
              

                this.lat = location.latLng.lat;
                this.lng = location.latLng.lng;
                this.storage.set("cMap.lat", this.lat);
                this.storage.set("cMap.lng", this.lng);

                this.AnimateToLoc(this.lat, this.lng);

                this.hasbooked = true
                this.gcode.Reverse_Geocode(this.lat, this.lng, false);
                this.userPos = new google.maps.LatLng(this.lat, this.lng);
                this.location = location;

                this.canShow = false;
                this.hasShown = true;

                this.hasRequested = false;

                this.isDriverAvailable = false;


            })

        } else {
            
            this.gcode.Reverse_Geocode(this.lat, this.lng, false);
            this.userPos = new google.maps.LatLng(this.lat, this.lng);
            this.location = location;

            this.canShow = false;

            this.hasShown = true;
            this.hasRequested = false;

            this.isDriverAvailable = false;
        }
    }

    //Start the map touch detection
    PumpControls() {
        this.map.on(GoogleMapsEvent.CAMERA_MOVE_START).subscribe((start) => {
            this.hidelocator = false;
            this.pause = false;
            this.hasMoved = true;
        });

        this.map.on(GoogleMapsEvent.CAMERA_MOVE_END).subscribe((start) => {
            this.pause = true;
            //Check if the user has already booked a ride
            if (!this.hasRequested) {
                if (this.canCheck && this.ready) {
                    this.canCheck = false;
                   
                }

                if (!this.platform.is("cordova")) {
                    this.lat = 4.883364;
                    this.lng = 7.025034;
                    
                } else {
                    if (this.isLocationChange) {
                        let center = this.map.getCameraPosition();
                        if (!this.StopLocUpdate) {
                            this.lat = center.target.lat;
                            this.lng = center.target.lng;

                            this.storage.set("cMap.lat", this.lat);
                            this.storage.set("cMap.lng", this.lng);
                        }
                        this.gcode.Reverse_Geocode(this.lat, this.lng, false);

                    }
                    if (this.isDestinationChange) {
                        let center = this.map.getCameraPosition();
                        let lat = center.target.lat;
                        let lng = center.target.lng;
                        this.gcode.Reverse_Geocode(lat, lng, false);

                    }
                }
            }
        });
    }

    ///Animate to user location
    AnimateToLoc(lat, lng) {
      

        if (!this.hasbooked) {
            this.map
                .moveCamera({
                    target: {
                        lat,
                        lng,
                    },
                    zoom: 19,
                    tilt: 0,
                    bearing: 0,
                })
                .then((distanceApart) => {
                    
                    this.lat = lat;
                    this.lng = lng;

                    this.storage.set("cMap.lat", this.lat);
                    this.storage.set("cMap.lng", this.lng);

                    this.canDo = true;

                    this.PumpControls();

                    this.Geofiring();
                    this.statistic();
                    this.canShow = false;

                    this.map.setClickable(true);


                    this.hasRequested = false;

                    this.isDriverAvailable = false;
                    const image_icon = {
                        url: "assets/icon/pin.png",

                        size: new google.maps.Size(40, 40),
                    };


                  

                    if (this.yellow_markersArray.length >= 1) {

                        this.marker.setPosition({
                            lat: lat,
                            lng: lng
                        });

                        this.hasShown = true;
                        this.mapLoadComplete = true;


                    }
                    else if (this.yellow_markersArray.length < 1) {
                       
                        this.map
                            .addMarker({      

                                icon: image_icon,
                                position: {
                                    lat,
                                    lng,
                                },

                                rotation: 0,
                            })
                            .then((marker) => {
                                this.marker = marker;
                                this.yellow_markersArray.push(this.marker);
                              
                                this.hasShown = true;
                                this.mapLoadComplete = true;
                               



                            });
                    }
                });

        }

        else {
            this.map
                .moveCamera({
                    target: {
                        lat,
                        lng,
                    },
                    zoom: 19,
                    tilt: 0,
                    bearing: 0,
                })
                .then((distanceApart) => {
                    
                    this.lat = lat;
                    this.lng = lng;

                    this.storage.set("cMap.lat", this.lat);
                    this.storage.set("cMap.lng", this.lng);

                    this.canShow = false;

                    this.map.setClickable(true);

                    this.hasRequested = false;

                    this.isDriverAvailable = false;
                    this.Geofiring();

                    const image_icon2 = {
                        url: "assets/icon/pin.png",

                        size: new google.maps.Size(40, 40),
                    };
                    

                    //CHeck if yellow marker exist
                    if (this.yellow_markersArray.length >= 1) {

                        this.marker.setPosition({
                            lat: lat,
                            lng: lng
                        });

                        let latlng = new google.maps.LatLng(lat, lng);


                    }
                    else if (this.yellow_markersArray.length < 1) {
                        this.map
                            .addMarker({
                               
                                icon: image_icon2,
                                position: {
                                    lat,
                                    lng,
                                },
                            })
                            .then((marker) => {
                                this.marker = marker;
                                this.yellow_markersArray.push(this.marker);
                                this.hasShown = true;
                                this.canDo = true;

                            });
                    }
                });
        }
        // });
    }


    statistic() {
        let myKey;

        this.nodriver = true;

        this.myProf.getAllDrivers().on("child_added", (driverSnapshot) => {
            
            this.Geofiring()

            if (!this.isBooking) {
            
                myKey = driverSnapshot.key;

                const driv_latitude = Number(driverSnapshot.val().driver_details[0]);
                const driv_longitude = Number(driverSnapshot.val().driver_details[1]);

                this.locations = [
                    [
                        driv_latitude,
                        driv_longitude,
                    ],
                ];

                var promises = this.locations.map((location, index) => {
                   
                    return this.geoFireInstance.set(myKey, location).then(() => { });
                });
            }
        });

        this.myProf.getAllDrivers().on("child_changed", (driverSnapshot) => {
            if (!this.isBooking) {
               
                myKey = driverSnapshot.key;

                const driv_latitude2 = Number(driverSnapshot.val().driver_details[0]);
                const driv_longitude2 = Number(driverSnapshot.val().driver_details[1]);

                // Specify the locations for each fish
                this.locations = [
                    [
                        driv_latitude2,
                        driv_longitude2,
                    ],
                ];

                var promises = this.locations.map((location, index) => {
                   
                    return this.geoFireInstance.set(myKey, location).then(() => { });
                });
            }
        });

        this.myProf.getAllDrivers().on("child_removed", (driverSnapshot) => {
            
            if (!this.isBooking) {
               
                if (driverSnapshot.val()) {
                    this.nodriver = false;
                } else {
                    this.nodriver = true;
                }
                this.myProf.geofireDrivers.child(driverSnapshot.key).remove();

                this.myProf.getAllDrivers().off("value");
            }
        });
    }



    Geofiring() {
       
        this.markers = [];
        this.car_notificationIds = [];
        this.car_notificationIds.length = 0;
        this.driverHere = false;
        this.allcar_array = [];
        this.locations = null;
        this.locations = [];
        var i;
        var a;
        this.geoFireInstance = new geofire.GeoFire(this.myProf.geofireDrivers);
        // Create a GeoQuery centered at fish2
        var geoQuery = this.geoFireInstance.query({
            center: [this.lat, this.lng],
            radius: this.settings.apart,
        });

        let p_Arry = [];
        let v_Arry = [];

        this.keyEntered = geoQuery.on("key_entered", (key, location, distance) => {
            
            this.isDone = true;
            if (!this.isBooking) {


                

                this.myProf
                    .getAllDrivers()
                    .child(key)
                    .once("value", (myShot) => {
                    

                        this.nodriver = false;
                        if (this.platform.is("cordova")) {
                            if (myShot.val()) {
                            
                                this.AddCar(myShot);
                            }

                        }

                        if (myShot.val() != null) {
                          
                            if (myShot.val().driver_details[0])
                                this.car_notificationIds.push([
                                    myShot.val().driver_details[0],
                                    myShot.val().driver_details[1],
                                    myShot.val().driver_details[2],
                                    myShot.key,
                                    myShot.val().driver_details[3],
                                    myShot.val().driver_details[4],
                                    myShot.val().driver_details[5],
                                    myShot.val().driver_details[6],
                                    myShot.val().driver_details[7],
                                    myShot.val().driver_details[8],

                                ]);
                           
                           

                            this.hasPushed = true;

                            this.car_notificationIds.reduce((acc, current) => {
                                const x = acc.find((item) => item[4] === current[4]);
                                if (!x) {
                                    this.allcar_array = acc.concat([current]);
                                   
                                    return acc.concat([current]);
                                } else {
                                  
                                    return acc;
                                }
                            }, []);

                        } else {
                         
                            this.myProf.geofireDrivers.child(key).remove();
                        }
                        
                    });
            }
         
        });

        this.exited = geoQuery.on("key_exited", (key, location, distance) => {
           
            if (!this.isBooking) {
                
                if (this.platform.is("cordova")) {
                    if (this.markers[key]) this.markers[key].remove();
                }

                for (i = 0; i < this.car_notificationIds.length; i++) {
                  
                    if (this.car_notificationIds[i][3] == key) {
                        this.car_notificationIds.splice(i, 1);
                        
                        return true;
                    } else {
                     
                    }
                }
               
            }
        });

        this.moved = geoQuery.on("key_moved", (key, location, distance) => {
           
        
            if (!this.isBooking) {
                this.myProf
                    .getAllDrivers()
                    .child(key)
                    .once("value", (myShot) => {
                       
                        if (this.platform.is("cordova")) {
                            if (this.markers[myShot.key]) {
                                this.markers[myShot.key].remove();
                                this.AddCar(myShot);
                            }
                        }

                        this.myProf.getAllDrivers().child(key).off("value");
                    });
            }
        });
    }

    async presentToast(message) {
        const toast = await this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: "top",
        });

        toast.onDidDismiss().then(() => {
           
        });

        await toast.present();
    }

    AddCar(data) {
      
        var uluru = {
            lat: data.val().driver_details[0],
            lng: data.val().driver_details[1],
        };

        if (this.car_markersArray.length >= 1) {
          this.car.setPosition({
            lat: data.val().driver_details[0],
            lng: data.val().driver_details[1]
          });
        }

        else if (this.car_markersArray.length < 1) {

        this.map
            .addMarker({
                title: "",
                icon: {
                    url: "assets/icon/car.png",
                    size: {
                        width: 70,
                        height: 45,
                    },
                },
                position: uluru,
                rotation: data.val().driver_details[8],
            })
            .then((f) => {
                this.markers[data.key] = f;
                this.car = f;
                this.car_markersArray.push(this.car);


            });
     }
    }

    //Show distance between driver and User in the map
    setMarkers(lat, lng, uid) {
        if (this.watch2) {
            this.watch2.unsubscribe();
        }

        

        this.driver_lat = lat;
        this.driver_lng = lng;
        this.uid = uid;
        this.destination = new LatLng(lat, lng);
        this.isPickedUp = true;


        let location = new LatLng(this.driver_lat, this.driver_lng);
       


        this.MyLat = this.lat;
        this.MyLng = this.lng;

        this.myProf
            .getUserAsClientInfo()
            .child(uid)
            .on("child_changed", (driverSnap) => {


                
                if (driverSnap.val()) {
                    
                    //DRIVER LOCATION COORDINATES
                    this.driver_lat = driverSnap.val().Driver_location[0];
                    this.driver_lng = driverSnap.val().Driver_location[1];
                    this.driver_bearing = driverSnap.val().Driver_bearing;


                    let location = new LatLng(this.driver_lat, this.driver_lng);

                    this.destination = new LatLng(
                        driverSnap.val().Driver_location[0],
                        driverSnap.val().Driver_location[1]
                    );

                  


                }


                //this.myProf.getUserAsClientInfo().child(uid).off("value");
                // });
            });

        this.WatchToPick();
    }

    WatchToPick() {

        if (this.watch2) {
            this.watch2.unsubscribe();
        }

        if (this.passenger) {
            this.passenger.remove();
            this.rider_markersArray = [];
        }

        if (this.client) {
            this.client.remove();
            this.client_markersArray = [];
        }

        if (this.driver) {
            this.driver.remove();
            this.driver_markersArray = [];
        }
        if (this.flag) {
            this.flag.remove();
            this.flag_markersArray = [];
        }

 

        let pope = true;

        this.watch_to_pick = this.geo.watchPosition({ enableHighAccuracy: true, timeout: 10000 }).subscribe(position => {
            if ((position as Geoposition).coords != undefined) {
                var geoposition = (position as Geoposition);
             
                this.location = geoposition;

                this.rider_lat = geoposition.coords.latitude,
                this.rider_lng = geoposition.coords.longitude;
                this.rider_bearing = geoposition.coords.heading

                this.route = [];

        
                if (this.isPickedUp) {

                    this.directionsService.route(
                        {
                            origin: new LatLng(
                                this.rider_lat,
                                this.rider_lng
                            ),
                            destination: this.destination,
                            travelMode: google.maps.TravelMode.DRIVING,
                            provideRouteAlternatives: true,
                        },
                        (res, status) => {
                            

                            if (this.isPickedUp) {
                                if (status == google.maps.DirectionsStatus.OK) {

                                    let gole;

                                    if (res.routes.length > 1) {
                                        gole = this.routeNumber;
                                    } else {
                                        gole = 0;
                                    }

                                    let point;
                                    if (res.routes[gole].overview_path) {
                                        for (
                                            let i = 0, len = res.routes[gole].overview_path.length;
                                            i < len;
                                            i++
                                        ) {
                                            point = res.routes[gole].overview_path[i];
                                            this.route[i] = {
                                                lat: Number(point.lat()),
                                                lng: Number(point.lng()),
                                            };

                                           
                                        }
                                    }

                               
                                    if (this.Line) {
                                        this.Line.remove();
                                    }
                                    if (this.Line2) {
                                        this.Line2.remove();
                                    }
                                

                                    this.AddPolyLine();

                                    this.MoveDriver(this.driver_lat, this.driver_lng, this.driver_bearing);
                                    this.showPassenger(this.rider_lat, this.rider_lng);


                                    this.myProf.UpdateUserLocation(
                                        this.rider_lat,
                                        this.rider_lng,
                                        this.uid,
                                        this.rider_bearing
                                    );


                                    // setTimeout(() => {
                                    // if (pope) {
                                    this.Measure(
                                        this.rider_lat,
                                        this.rider_lng,
                                        this.driver_lat,
                                        this.driver_lng,

                                    );

                                    // if (point)
                                    // this.map.animateCamera({
                                    //   target: point,
                                    //   duration: 500,
                                    // });


                                    //     pope = false;
                                    // }
                                    // }, 1000);

                                    // let path: ILatLng[] = [{ "lat": this.driver_lat, "lng": this.driver_lng }, { "lat": this.rider_lat, "lng": this.rider_lng }];
                                    // let latLngBounds2 = new LatLngBounds(path);

                                    // this.map.animateCamera({
                                    //   target: latLngBounds2,
                                    //   duration: 500,
                                    // });

                                    // var bounds = [{ "lat": this.driver_lat, "lng": this.driver_lng },{ "lat": this.rider_lat, "lng": this.rider_lng },];

                                    // this.map.moveCamera({
                                    //   'target': bounds,
                                    // });

                                }
                                else {
                                    
                                 
                                    if (this.Line) {
                                        this.Line.remove();
                                    }
                                    if (this.Line2) {
                                        this.Line2.remove();
                                    }
                                  
                                }
                            }

                            else {
                               
                            }

                        }

                    );
                  
                }

                else {
                    
                }

            }
            else {
              

            }

        });
        // });

    }

    MoveDriver(lat, lng,bearing) {

        if (this.driver_markersArray.length >= 1) {
            this.driver.setPosition({
                lat: lat,
                lng: lng
            });

            this.driver.setRotation(bearing);
        }

        else if (this.driver_markersArray.length < 1) {
            const myIcon = {
                url: "assets/icon/map-taxi.png",
                size: new google.maps.Size(40, 40),

            };

            this.map
                .addMarker({
                    title: "",
                    icon: myIcon,
                    position: {
                        lat: lat,
                        lng: lng,
                    },
                })
                .then((marker) => {
                    this.driver = marker;
                    this.driver_markersArray.push(this.driver);
                    
                });

        }
    }

    MoveClient(lat, lng) {

      

        // if (this.client) {
        //   this.client.remove();
        // }

        if (this.client_markersArray.length >= 1) {
            this.client.setPosition({
                lat: lat,
                lng: lng
            });
        }
        else if (this.client_markersArray.length < 1) {


            const riderIcon = {
                url: "assets/icon/rider.png",
                size: new google.maps.Size(40, 40),

            };

            this.map
                .addMarker({
                    title: "",
                    icon: riderIcon,
                    position: {
                        lat: lat,
                        lng: lng,
                    },
                })
                .then((markdfder) => {
                    this.client = markdfder;
                    this.client_markersArray.push(this.client);
                    
                });

        }
    }

    showPassenger(lat, lng) {

       
    
        if (this.rider_markersArray.length >= 1) {
            this.passenger.setPosition({
                lat: lat,
                lng: lng
            });
        }
        else if (this.rider_markersArray.length < 1) {


            const riderIcon = {
                url: "assets/icon/rider.png",
                size: new google.maps.Size(40, 40),

            };

            this.map
                .addMarker({
                    title: "",
                    icon: riderIcon,
                    position: {
                        lat: lat,
                        lng: lng,
                    },
                })
                .then((markdfder) => {
                    this.passenger = markdfder;
                    this.rider_markersArray.push(this.passenger);

                });

        }
    }

    MoveFlag(lat, lng) {
       
        if (this.flag_markersArray.length >= 1) {
            this.flag.setPosition({
                lat: lat,
                lng: lng
            });
        }
        else if (this.flag_markersArray.length < 1) {

            // if (this.flag) {
            //   this.flag.remove();
            // }

            const rflagIcon = {
                url: "assets/icon/flag2.png",
                size: new google.maps.Size(40, 40),

            };

            this.map
                .addMarker({
                    title: "",
                    icon: rflagIcon,
                    position: {
                        lat: lat,
                        lng: lng,
                    },
                })
                .then((markdfder) => {
                    this.flag = markdfder;
                    this.flag_markersArray.push(this.flag);
                   
                });
        }
    }

    //Show distance between driver and User in the map
    setMarkersDestination(lat, lng, uid) {
       
        this.destination = new LatLng(lat, lng);
        this.isPickedUp = false;
        this.isDropped = true;
        this.flag_Lat = lat;
        this.flag_Lng = lng;
        this.uid = uid;

        // this.route = [];
       

        this.myProf
            .getUserAsClientInfo()
            .child(uid)
            .on("child_changed", (driverSnap) => {
                

                if (driverSnap.val()) {
                    this.driver_lat = driverSnap.val().Driver_location[0];
                    this.driver_lng = driverSnap.val().Driver_location[1];
                    this.driver_bearing = driverSnap.val().Driver_bearing;

                    let location = new LatLng(this.driver_lat, this.driver_lng);
                   

                    this.destination = new LatLng(
                        driverSnap.val().Driver_location[0],
                        driverSnap.val().Driver_location[1]
                    );


                
                }



                //this.myProf.getUserAsClientInfo().child(uid).off("value");
                // });
            });

        this.WatchToDrop();

    }

    WatchToDrop() {

     
        if (this.watch_to_pick) {
            this.watch_to_pick.unsubscribe();
        }
        if (this.watch2) {
            this.watch2.unsubscribe();
        }

        if (this.passenger) {
            this.passenger.remove();
            this.rider_markersArray = [];
        }

        if (this.client) {
            this.client.remove();
            this.client_markersArray = [];
        }

        if (this.driver) {
            this.driver.remove();
            this.driver_markersArray = [];
        }
        if (this.flag) {
            this.flag.remove();
            this.flag_markersArray = [];
        }



        let pope = true;

        this.watch_to_drop = this.geo.watchPosition({ enableHighAccuracy: true, timeout: 10000 }).subscribe(position => {
            if ((position as Geoposition).coords != undefined) {
                var geoposition = (position as Geoposition);
            
                this.location = geoposition;

                this.rider_lat = geoposition.coords.latitude,
                this.rider_lng = geoposition.coords.longitude;
                this.rider_bearing = geoposition.coords.heading

                
                this.route22 = [];
                this.destination_route = [];

                // this.route = [];
                this.directionsService.route(
                    {
                        origin: new LatLng(
                            this.rider_lat,
                            this.rider_lng
                        ),
                        destination: this.destination,
                        travelMode: google.maps.TravelMode.DRIVING,
                        provideRouteAlternatives: true,
                    },
                    (res, status) => {
                       
                        if (this.hasCompleted == false) {

                            if (status == google.maps.DirectionsStatus.OK) {
                                //  this.directionsDisplay.setDirections(res);
                                let gole;

                                if (res.routes.length > 1) {
                                    gole = this.routeNumber;
                                } else {
                                    gole = 0;
                                }

                                let point;
                                if (res.routes[gole].overview_path) {
                                    for (
                                        let i = 0, len = res.routes[gole].overview_path.length;
                                        i < len;
                                        i++
                                    ) {
                                        point = res.routes[gole].overview_path[i];

                                        this.route22[i] = {
                                            lat: Number(point.lat()),
                                            lng: Number(point.lng()),
                                        };

                                        this.destination_route[i] = {
                                            lat: Number(point.lat()),
                                            lng: Number(point.lng()),
                                        };
                                    }
                                }

                              
                               
                                if (this.Line) {
                                    this.Line.remove();
                                }
                                if (this.Line2) {
                                    this.Line2.remove();
                                }
                              

                                this.AddPolyLineDestination();
                                this.MoveDriver(this.driver_lat, this.driver_lng, this.rider_bearing);
                                this.MoveFlag(this.flag_Lat, this.flag_Lng);


                                // setTimeout(() => {
                                //   if (pope) {
                                this.Measure(
                                    this.flag_Lat,
                                    this.flag_Lng,
                                    this.driver_lat,
                                    this.driver_lng

                                );

                             
                            }
                        } else {
                            
                         
                            if (this.Line) {
                                this.Line.remove();
                            }
                            if (this.Line2) {
                                this.Line2.remove();
                            }
                          


                        }
                    }
                );
              


            }
            else {
                

            }

        });

    }



    AddPolyLine() {
    

        this.map
            .addPolyline({
                color: "#fbb91d",
                visible: true,
                width: 7,
                points: this.route,
            })
            .then((ff) => {
                this.Line2 = ff;
            });
    }

    AddPolyLineDestination() {
        

        this.map
            .addPolyline({
                color: "#fbb91d",
                visible: true,
                width: 7,
                points: this.destination_route,
            })
            .then((ff) => {
                this.Line = ff;
            });
    }

  

    Measure(lat, lng, lat2, lng2) {
       
        let arrayOfLatLng = [new LatLng(lat, lng), new LatLng(lat2, lng2)];

        let bounds = new LatLngBounds(arrayOfLatLng);

        let center = bounds.getCenter();

        var mapElement = document.getElementById("map");

        var mapDimensions = {
            height: mapElement.offsetHeight,
            width: mapElement.offsetWidth,
        };


        // var zoom = this.getBoundsZoomLevel(bounds, mapDimensions);
        // console.log("ZOOM VAR:", zoom);
        // this.map
        //   .animateCamera({
        //     target: center,
        //     zoom: zoom,
        //     duration: 200,
        //   })
        //   .then(() => { });

        this.map
            .animateCamera({
                target: bounds,
                duration: 500,
            })
            .then(() => {
                this.isNavigate = true;
            });

        // this.map.moveCamera({
        //   'target': center,
        // });
    }

    getBoundsZoomLevel(bounds, mapDim) {
        // var WORLD_DIM = { height: 180 / 2.05, width: 160 / 2.05 };
        var WORLD_DIM = { height: 96 / 2.05, width: 96 / 2.05 };
        var ZOOM_MAX = 13;

        var ne = bounds.northeast;
        var sw = bounds.southwest;

        var latFraction = (this.latRad(ne.lat) - this.latRad(sw.lat)) / Math.PI;

        var lngDiff = ne.lng - sw.lng;
        var lngFraction =
            ((lngDiff < 0 ? lngDiff + 360 * 1.16 : lngDiff) / 360) * 1.16;

        var latZoom = this.zoom(mapDim.height, WORLD_DIM.height, latFraction);
        var lngZoom = this.zoom(mapDim.width, WORLD_DIM.width, lngFraction);

        return Math.min(latZoom, lngZoom, ZOOM_MAX);
    }

    latRad(lat) {
        var sin = Math.sin((lat * Math.PI) / 180);
        var radX2 = Math.log((1 + sin) / (1 - sin)) / 2;
        return Math.max(Math.min(radX2, Math.PI), -Math.PI) / 2;
    }

    zoom(mapPx, worldPx, fraction) {
        return Math.floor(Math.log(mapPx / worldPx / fraction) / Math.LN2);
    }

    ShowDestination(loc1, loc2) {


        if (this.watch_to_pick) {
            this.watch_to_pick.unsubscribe();
        }
        if (this.watch2) {
            this.watch2.unsubscribe();
        }

     

        let t_Mode = google.maps.TravelMode.DRIVING;

        this.origin = new LatLng(loc1[0], loc1[1])
        this.new_destination = new LatLng(loc2[0], loc2[1])

    
        this.destination_route = [];

        this.directionsService.route(
            {
                origin: new LatLng(loc1[0], loc1[1]),
                destination: new LatLng(loc2[0], loc2[1]),
                travelMode: t_Mode,
                provideRouteAlternatives: true,
            },
            (res, status) => {
                
                if (status == google.maps.DirectionsStatus.OK) {
                    
                   
                    let gole;
                   
                  
                    if (res.routes.length > 1) {
                        gole = this.routeNumber;
                    } else {
                        gole = 0;
                    }


                    let point;
                    if (res.routes[gole].overview_path) {
                        for (
                            let i = 0, len = res.routes[gole].overview_path.length;
                            i < len;
                            i++
                        ) {
                            point = res.routes[gole].overview_path[i];
                            
                            this.destination_route[i] = {
                                lat: Number(point.lat()),
                                lng: Number(point.lng()),
                            };
                        }
                    }

                    //Show Rider At Location
                    this.MoveClient(loc1[0], loc1[1]);

                    //Display flag at Destination
                    this.MoveFlag(loc2[0], loc2[1]);

                    this.AddPolyLineDestination();


                    this.Measure(loc1[0], loc1[1], loc2[0], loc2[1]);



                    // let path: ILatLng[] = [{ "lat": loc1[0], "lng": loc1[1] }, { "lat": loc2[0], "lng": loc2[1] }];
                    // let latLngBounds = new LatLngBounds(path);

                    // this.map.animateCamera({
                    //   target: latLngBounds,
                    //   duration: 500,
                    // });

                    // var bounds = [{ "lat": loc1[0], "lng": loc1[1] }, { "lat": loc2[0], "lng": loc2[1] }];

                    // this.map.moveCamera({
                    //   'target': bounds,
                    // });



                    // this.map.setCameraZoom(19);
                    //this.map.setCameraTarget(point);

                }
            }
        );
        // this.directionsDisplay.setMap(this.map);
    }
}
