import { Injectable } from "@angular/core";
import firebase from "firebase";
import { LanguageService } from "./language.service";

@Injectable({
  providedIn: "root",
})
export class SettingsService {
  public WebAdminProfile: any;
  public appName = "Rider";
  public car1 = "Standard";
  public car2 = "Taxi";
  public car3 = "Executive";
  public language = "en";
  public appCareer = "https://callforride.com";
  public appFaq = "https://callforride.com";
  public appLink = "https://callforride.com";
  public appinsta = "https://www.instagram.com/callforride/";
  public appFB = "https://www.facebook.com/call4rideghana/";
  public appTikTok = "http://tiktok.com/@call4ridegh";
  public appYoutube = "https://youtube.com/channel/UCXdQQx_eSKKV9xjDs7NO3Nw";
  public twitter = "https://twitter.com/callforride";
  public appTerms = "https://Call4Ride.net";
  //public apart = 3000; // 500 metres apart from driver
  public apart = 150; // 500 metres apart from driver
  public appCountryCode = "GH";
  public appStripeKey = "sk_live_Emol5oC7Z42JqsYTzC05y85e";
  public isStripe = false;
  public appcurrency: any = "₵";
  public appDashboard = "https://ajetaxi.firebaseapp.com//";
  public appPhone: any = "+233502903382";
  public companyMail: any = "call4ridegh@gmail.com";
  // tslint:disable-next-line: variable-name
  public current_ID = false;
  public mailGUrl: any = "sandbox28ca01fb2b374bb1b8aceb9d0a86895a.mailgun.org";
  public mailGKey: any = window.btoa(
    "api:key-60b9b5a8e7097e2fdcada552e4820db4"
  );
  public id: any;
  public refer = false;
  public wallet = false;
  public company = false;
  public pool = false;
  public schedule = false;
  public OnesignalAppID: any = "f6345b2b-3777-4631-8415-a3a178826e8d";
  public CloudID: any = "1003487763108";
  // tslint:disable-next-line: variable-name
  public support_email: any = "chndth@gmail.com";
  langauge: any;
  distanceDriving = 3000;

  constructor(public lang: LanguageService) {
    this.WebAdminProfile = firebase.database().ref(`DashboardSettings`);

    // this.getWebAdminProfile().on("value", (sShot) => {
    //   // tslint:disable-next-line: curly
    //   if (sShot.val().name) this.appName = sShot.val().name;
    //   // tslint:disable-next-line: curly
    //   if (sShot.val().website) this.appLink = sShot.val().website;
    //   // tslint:disable-next-line: curly
    //   if (sShot.val().faq) this.appFaq = sShot.val().faq;
    //   if (sShot.val().instagram) {
    //     this.appinsta = sShot.val().instagram;
    //   }
    //   if (sShot.val().distanceDriving) {
    //     this.distanceDriving = sShot.val().distanceDriving;
    //   }
    //   if (sShot.val().careers) {
    //     this.appCareer = sShot.val().careers;
    //   }
    //   if (sShot.val().facebook) {
    //     this.appFB = sShot.val().facebook;
    //   }
    //   if (sShot.val().apart) {
    //     this.apart = sShot.val().apart;
    //   }
    //   if (sShot.val().countrycode) {
    //     this.appCountryCode = sShot.val().countrycode;
    //   }
    //   if (sShot.val().currency) {
    //     this.appcurrency = sShot.val().currency;
    //   }
    //   if (sShot.val().phone) {
    //     this.appPhone = sShot.val().phone;
    //   }
    //   if (sShot.val().car_0) {
    //     this.car1 = sShot.val().car_0;
    //   }
    //   if (sShot.val().car_1) {
    //     this.car2 = sShot.val().car_1;
    //   }
    //   if (sShot.val().car_2) {
    //     this.car3 = sShot.val().car_2;
    //   }
    //   if (sShot.val().stripe) {
    //     this.isStripe = true;
    //   }
    //   if (sShot.val().apart) {
    //     this.apart = sShot.val().apart;
    //   }
    //   if (sShot.val().langauge) {
    //     console.log(this.langauge);
    //     this.langauge = sShot.val().langauge;
    //   }
    //   if (sShot.val().company) {
    //     this.company = sShot.val().company;
    //   }
    //   if (sShot.val().refer) {
    //     this.refer = sShot.val().refer;
    //   }
    //   if (sShot.val().wallet) {
    //     this.wallet = sShot.val().wallet;
    //   }
    //   if (sShot.val().pool) {
    //     this.pool = sShot.val().pool;
    //   }
    //   if (sShot.val().schedule) {
    //     this.schedule = sShot.val().schedule;
    //   }
    //   if (sShot.val().appID) {
    //     this.OnesignalAppID = sShot.val().appID;
    //   }
    //   if (sShot.val().CloudID) {
    //     this.CloudID = sShot.val().CloudID;
    //   }
    //   if (sShot.val().email) {
    //     this.companyMail = sShot.val().email;
    //   }
    //   if (sShot.val().mailgunUrl) {
    //     this.mailGUrl = sShot.val().mailgunUrl;
    //   }
    //   if (sShot.val().mailgunKey) {
    //     this.mailGKey = sShot.val().mailgunKey;
    //   }
    //   if (sShot.val().support_email) {
    //     this.support_email = sShot.val().support_email;
    //   }
    // });
  }

  getWebAdminProfile(): firebase.database.Reference {
    return this.WebAdminProfile;
  }
}
