import { Injectable } from "@angular/core";
import { LanguageService } from "./language.service";
import { NativeMapContainerService } from "./native-map-container.service";
import { GeocoderService } from "./geocoder.service";
import { PopUpService } from "./pop-up.service";
import { ModalController, Platform } from "@ionic/angular";
import { EventService } from "./event.service";
import { HttpClient } from "@angular/common/http";
import { ProfileService } from "./profile.service";
import { Storage } from "@ionic/storage";
declare var google;
@Injectable({
  providedIn: "root",
})
export class DirectionserviceService {
  public price: any;
  public new_price_for_add_stop: any;
  public time: any;
  public canDismiss: boolean = false;
  public name: string;
  public id: any;
  public highPrice: any;
  public locationName: any;
  public hasGottenTripDist: boolean = false;
  public calculateBtn: boolean = false;
  public pricePerKm: any = 4;
  public fare: any = 5;
  public time2: any;
  public destination_time: any;
  public rate: any = 2;
  public extra: any = 50;
  public fix: any = 65;
  public above: any = 110;
  public next: any = 55;
  public min: any = 3;
  public cons: any = 4;
  appPrice: any;
  public canUpdateDestination: boolean = false;
  public isDriver: boolean = false;
  destinationName: any;
  public service: any = new google.maps.DistanceMatrixService();
  public duration: any;
  percentage;
  status;
  rage: any = 0;
  miles: number;
  request: {
    origin: any;
    destination: any;
    travelMode: any;
    provideRouteAlternatives: boolean;
  };
  totalTolls: number;
  total: any;
  actualPrice: any = 0;
  distance2: any;
  tollPrice: any[];
  toll_2: any[];
  toll_3: any[];

  tollName: any[];
  toll_1: any = 0;
  toll_0: any = 0;
  mentoll: any[];
  duration2: number;
  isClose: boolean;
  timeToReach: any = "calculating...";
  arriving_time: any = "calculating...."

  constructor(
    private eProvider: EventService,
    public http: HttpClient,
    public lp: LanguageService,
    public modalCtrl: ModalController,
    public platform: Platform,
    public cMap: NativeMapContainerService,
    public gCode: GeocoderService,
    public popOp: PopUpService,
    public ph: ProfileService,
    public storage: Storage,
  ) {
    
  }


  calcRouteAriving(
    start,
    stop,
    isDriver,
    canUpdateDestination,
    destinationName,
    canshowLoads,
    percentage
  ) {


    let directionsService = new google.maps.DirectionsService();
    // console.log(start, stop);
    // console.log("PERCENTAGE IN CAC ROUTE: " + percentage);

    // console.log("LOCAION: " + this.gCode.locationName);
    // console.log("DESTINATION: " + destinationName);
    var request = {
      origin: start,
      destination: stop,
      travelMode: google.maps.TravelMode.DRIVING,
      provideRouteAlternatives: true,
    };

    directionsService.route(request, (response, status) => {
      this.callback3(response, status, canshowLoads, percentage);
    });

    this.isDriver = isDriver;
    this.canUpdateDestination = canUpdateDestination;
    this.destinationName = destinationName;
  }

  callback3(response, status, canshowLoads, percentage) {
    // See Parsing the Results for
    // the basics of a callback function.
  

    if (status == google.maps.DirectionsStatus.OK) {
      // loading.present();

      // this.checkPromoExist();

      let duration = Math.round(response.routes[0].legs[0].duration.value / 60);
      let distance = response.routes[0].legs[0].distance.value * 0.000621371192;
      this.duration = duration;
      this.miles = distance;

      this.storage.set("duration", this.duration);

      console.log("<<<<<<< DIRECTION SERVICE >>>>>>>");
      console.log("This is the duration " + duration);
      console.log("This is the distance " + distance);
      console.log("This is the fare " + this.fare);
      console.log("This is the pricePerKm " + this.pricePerKm);
      console.log("This is the percentage " + percentage);

      

      // this.ph.getPricing().limitToLast(1).once("value", (data) => {
      //   if (data.val()) {
      //     data.forEach((snap) => {

      //       this.fare = snap.val().base;
      //       this.pricePerKm = snap.val().pricePerKm;
      //       let surge = snap.val().surge;

      //       console.log("basefare is " + this.fare);
      //       console.log("pricePerKm is " + this.pricePerKm);
      //       console.log("surge is " + surge);


      //       if (percentage == undefined || percentage == "undefined") {
      //         this.price = Math.round(((this.fare + distance + duration + surge) * this.pricePerKm));
      //         console.log("Price when % is undefined " + this.price);

      //       } else {
      //         let old_price = (this.fare + distance + duration + surge) * this.pricePerKm;
      //         this.price = Math.round((old_price - percentage * old_price));
      //         console.log("Price when % exist " + this.price);


      //       }
      //     });
      //   } else {
      //     console.log("NO PRICING");
      //   }

      // })

      this.arriving_time = response.routes[0].legs[0].duration.text;
      this.storage.set("arriving_time", this.arriving_time);

     

      let rect = document.getElementById("header");
      if (this.isDriver && rect) {

        rect.innerText = this.lp.translate()[0].arrival + this.time;
        rect.style.fontSize = 1.8 + "em";
      }

      // this.timeToReach = response.routes[0].legs[0].duration.text;
      // console.log("TIME TO REACH IS::", this.timeToReach);


    } else {

      
      //this.cMap.norideavailable = true;
      // if (canshowLoads)
      // this.popOp.hideLoader();
    }
  }

  calcRoute(
    start,
    stop,
    isDriver,
    canUpdateDestination,
    destinationName,
    canshowLoads,
    percentage
  ) {
    // if (!this.platform.is("cordova")) {
    //   console.log("did this");
    //   start = new google.maps.LatLng(40.65563, -73.95025);
    //   this.gCode.locationName = "Testing Location";
    // }

    let directionsService = new google.maps.DirectionsService();

    if (this.calculateBtn) {
      //this.popOp.presentLoader('Measuring distance')
    }

    console.log("START",start,"STOP", stop);
    console.log("PERCENTAGE IN CAC ROUTE: " + percentage);

    console.log("LOCAION: " + this.gCode.locationName);
    console.log("DESTINATION: " + destinationName);
    var request = {
      origin: start,
      destination: stop,
      travelMode: google.maps.TravelMode.DRIVING,
      provideRouteAlternatives: true,
    };

    directionsService.route(request, (response, status) => {
      this.callback(response, status, canshowLoads, percentage);
    });

    this.isDriver = isDriver;
    this.canUpdateDestination = canUpdateDestination;
    this.destinationName = destinationName;
  }

  callback(response, status, canshowLoads, percentage) {
    // See Parsing the Results for
    // the basics of a callback function.
    

    if (status == google.maps.DirectionsStatus.OK) {
      // loading.present();

      // this.checkPromoExist();


      // const distance = route.legs[0].distance.value; // Distance in meters
      // const farePerMeter = 0.10; // Adjust this to your fare rate
      // const fare = (distance * farePerMeter) / 1000; // Convert meters to kilometers

      // return fare.toFixed(2)



      let duration = Math.round(response.routes[0].legs[0].duration.value / 60);
      // let distance = response.routes[0].legs[0].distance.value * 0.000621371192;
      let distance = response.routes[0].legs[0].distance.value;
      this.duration = duration;
      this.miles = distance;

      this.storage.set("duration", this.duration);
     
      console.log("This is the duration " + duration);
      console.log("This is the distance " + distance);
      console.log("This is the fare " + this.fare);
      console.log("This is the pricePerKm " + this.pricePerKm);
      console.log("This is the percentage " + percentage);

      
      this.ph.getPricing().limitToLast(1).once("value", (data) => {
        if (data.val()) {
          data.forEach((snap) => {

            this.fare = snap.val().base;
            this.pricePerKm = snap.val().pricePerKm;
            let surge = snap.val().surge;

            console.log("basefare is " + this.fare);
            console.log("pricePerKm is " + this.pricePerKm);
            console.log("surge is " + surge);
            console.log("This is the distance " + distance);


            if (percentage == undefined || percentage == "undefined") {

              // this.price = Math.round(((this.fare + distance + duration + surge) * this.pricePerKm));

              this.price = Math.round( (distance * this.fare * this.pricePerKm) / 1000) + surge;

              this.storage.set("price", this.price);
              console.log("Price when % is undefined " + this.price);

            }
            else {

              // let old_price = (this.fare + distance + duration + surge) * this.pricePerKm;

              let old_price = ((distance * this.fare * this.pricePerKm) / 1000) + surge;

              this.price = Math.round((old_price - percentage * old_price));
              
             
              this.storage.set("price", this.price);

              
              console.log("Price when % exist " + this.price);

            }
          });
        } else {
         
          if (percentage == undefined || percentage == "undefined") {
            // this.price = Math.round(((this.fare + distance + duration) * this.pricePerKm));

            this.price = Math.round((distance * this.fare * this.pricePerKm) / 1000);
            this.storage.set("price", this.price);
            console.log("Price when % is undefined " + this.price);

          } else {
            // let old_price = (this.fare + distance + duration) * this.pricePerKm;

            let old_price = (distance * this.fare * this.pricePerKm) / 1000

            this.price = Math.round((old_price - percentage * old_price));
            this.storage.set("price", this.price);
            console.log("Price when % exist " + this.price);

          }
        }

      })



      this.time = response.routes[0].legs[0].duration.text;
      console.log("FINAL PRICE:: " + this.price);
      console.log("HIGH PRICE:: " + this.highPrice);
      console.log("TIME :: " + this.time);

      let rect = document.getElementById("header");
      if (this.isDriver && rect) {
        rect.innerText = this.lp.translate()[0].arrival + this.time;
        rect.style.fontSize = 1.8 + "em";
      }

      this.timeToReach = response.routes[0].legs[0].duration.text;
      this.storage.set("timeToReach", this.timeToReach);
      console.log("TIME TO REACH IS::", this.timeToReach);

      console.log(
        response.routes[0].legs[0].distance.value * 0.000621371192,
        response.routes[0].legs[0].duration.value,
        response.routes[0].legs[0].duration.text
      );
    } else {

   
      //this.cMap.norideavailable = true;
      // if (canshowLoads)
      // this.popOp.hideLoader();
    }
  }

  calcDestRoute(
    name,
    start,
    stop,
    destinationName,
    id,
    canshowLoads,
    percentage
  ) {
 
    if (!this.platform.is("cordova")) {
      start = new google.maps.LatLng(40.65563, -73.95025);
      this.gCode.locationName = "Test Accra";
    }

    let directionsService = new google.maps.DirectionsService();


    var request = {
      origin: this.gCode.locationName,
      destination: destinationName,
      travelMode: google.maps.TravelMode.DRIVING,
      provideRouteAlternatives: true,
    };

    directionsService.route(request, (response, status) => {
      this.callback2(response, status, id, canshowLoads, this.percentage);
    });

  

    this.request = {
      origin: this.gCode.locationName,
      destination: destinationName,
      travelMode: google.maps.TravelMode.DRIVING,
      provideRouteAlternatives: true,
    };

    this.destinationName = destinationName;
    this.name = name;

    let time = new Date();

    var hh = time.getHours();
    var mm = time.getMinutes();
    var ss = time.getSeconds();
  }

  callback2(response, status, id, canshowLoads, percentage) {
    // See Parsing the Results for
    // the basics of a callback function.
    

    if (status == google.maps.DirectionsStatus.OK) {
      // this.checkPromoExist();

      let duration = Math.round(response.routes[0].legs[0].duration.value / 60);
      let distance = response.routes[0].legs[0].distance.value * 0.000621371192;
      this.duration2 = duration;
      this.miles = distance;
      this.distance2 = (
        response.routes[0].legs[0].distance.value * 0.000621371192
      ).toFixed(2);

       console.log("This is the duration " + this.duration2);
      console.log("This is the distance " + this.distance2);
      console.log("This is the fare " + this.fare);
      console.log("This is the pricePerKm " + this.pricePerKm);
      console.log("This is the percentage " + percentage);
      

      // this.ph.getPricing().limitToLast(1).once("value", (data) => {
      //   if (data.val()) {
      //     data.forEach((snap) => {

      //       this.fare = snap.val().base;
      //       this.pricePerKm = snap.val().pricePerKm;
      //       let surge = snap.val().surge;

      //       console.log("basefare is " + this.fare);
      //       console.log("pricePerKm is " + this.pricePerKm);
      //       console.log("surge is " + surge);


      //       if (percentage == undefined || percentage == "undefined") {
      //         this.price = Math.round(((this.fare + distance + duration + surge) * this.pricePerKm));
      //         console.log("Price when % is undefined " + this.price);


      //       } else {
      //         let old_price = (this.fare + distance + duration + surge) * this.pricePerKm;
      //         this.price = Math.round((old_price - percentage * old_price));
      //         console.log("Price when % exsit " + this.price);


      //       }
      //     });
      //   } else {
      //     console.log("NO PRICING");
      //     if (percentage == undefined || percentage == "undefined") {
      //       this.price = Math.round(((this.fare + distance + duration) * this.pricePerKm));
      //       console.log("Price when % is undefined " + this.price);

      //     } else {
      //       let old_price = (this.fare + distance + duration) * this.pricePerKm;
      //       this.price = Math.round((old_price - percentage * old_price));
      //       console.log("Price when % exust " + this.price);

      //     }
      //   }

      // })


      this.destination_time = response.routes[0].legs[0].duration.text;
      this.storage.set("destination_time", this.destination_time);
    

      let rect = document.getElementById("header");
      this.isDriver = false;
      if (rect) {
        rect.innerText = this.lp.translate()[0].arrival + this.time;
        rect.style.fontSize = 1.8 + "em";
      }
      //  loading.dismiss()
      // this.popOp.hideLoader();
     
    }
  }


  calcRouteForAddstop(
    start,
    stop,
    isDriver,
    canUpdateDestination,
    destinationName,
    canshowLoads,
    percentage
  ) {
 
    let directionsService = new google.maps.DirectionsService();

  

    var request = {
      origin: start,
      destination: stop,
      travelMode: google.maps.TravelMode.DRIVING,
      provideRouteAlternatives: true,
    };

    directionsService.route(request, (response, status) => {
      this.callbackForAddStop(response, status, canshowLoads, percentage);
    });

    this.isDriver = isDriver;
    this.canUpdateDestination = canUpdateDestination;
    this.destinationName = destinationName;
  }


  callbackForAddStop(response, status, canshowLoads, percentage) {
    
  

    if (status == google.maps.DirectionsStatus.OK) {
   

      let duration = Math.round(response.routes[0].legs[0].duration.value / 60);
      // let distance = response.routes[0].legs[0].distance.value * 0.000621371192;
      let distance = response.routes[0].legs[0].distance.value;
      this.duration = duration;
      this.miles = distance;

      this.storage.set("duration", this.duration);
      // console.log("<<<<<<< DIRECTION SERVICE >>>>>>>");
      // console.log("This is the duration " + duration);
      // console.log("This is the distance " + distance);
      // console.log("This is the fare " + this.fare);
      // console.log("This is the pricePerKm " + this.pricePerKm);
      // console.log("This is the percentage " + percentage);

      
      this.ph.getPricing().limitToLast(1).once("value", (data) => {
        if (data.val()) {
          data.forEach((snap) => {

            this.fare = snap.val().base;
            this.pricePerKm = snap.val().pricePerKm;
            let surge = snap.val().surge;

            // console.log("basefare is " + this.fare);
            // console.log("pricePerKm is " + this.pricePerKm);
            // console.log("surge is " + surge);


            if (percentage == undefined || percentage == "undefined") {


              this.new_price_for_add_stop = Math.round((distance * this.fare * this.pricePerKm) / 1000) + surge;

              this.storage.set("price", this.new_price_for_add_stop);

              // console.log("Price when % is undefined " + this.new_price_for_add_stop);

            }
            else {

              // let old_price = (this.fare + distance + duration + surge) * this.pricePerKm;

              let old_price = ((distance * this.fare * this.pricePerKm) / 1000) + surge;

              this.new_price_for_add_stop = Math.round((old_price - percentage * old_price));


              this.storage.set("price", this.new_price_for_add_stop);


              // console.log("new_price_for_add_stop when % exist " + this.new_price_for_add_stop);

            }
          });
        } else {
          
          if (percentage == undefined || percentage == "undefined") {
            // this.price = Math.round(((this.fare + distance + duration) * this.pricePerKm));

            this.new_price_for_add_stop = Math.round((distance * this.fare * this.pricePerKm) / 1000);
            this.storage.set("price", this.price);
            console.log("Price when % is undefined " + this.price);

          } else {
            // let old_price = (this.fare + distance + duration) * this.pricePerKm;

            let old_price = (distance * this.fare * this.pricePerKm) / 1000

            this.new_price_for_add_stop = Math.round((old_price - percentage * old_price));
            this.storage.set("price", this.price);
            console.log("new_price_for_add_stop when % exist " + this.new_price_for_add_stop);

          }
        }

      })



      this.time = response.routes[0].legs[0].duration.text;
      // console.log("FINAL PRICE:: " + this.price);
      // console.log("HIGH PRICE:: " + this.highPrice);
      // console.log("TIME :: " + this.time);

      let rect = document.getElementById("header");
      if (this.isDriver && rect) {
        rect.innerText = this.lp.translate()[0].arrival + this.time;
        rect.style.fontSize = 1.8 + "em";
      }

     
      
    } else {

     
    }
  }

  checkPromoExist() {
    let id = this.ph.id;
    this.ph
      .getAllUsedCodes()
      .orderByChild("rider_id")
      .equalTo(id)
      .once("value", (data) => {
       

        if (data.val()) {
          data.forEach((snap) => {
            this.status = snap.val().status;
            let perc = snap.val().percentage;
            // console.log("Status is " + this.status);
            // console.log("percen is " + perc);

            if (this.status == "activated") {
              this.percentage = perc / 100;
              // console.log(
              //   "The ACTIVATED percentage here is " + this.percentage
              // );
              return this.percentage;
            } else {
              
            }
          });
        } else {
         
        }
      });
  }
}
