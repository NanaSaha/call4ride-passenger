import { Injectable } from "@angular/core";
import {
  AlertController,
  ModalController,
  LoadingController,
  NavController,
  Platform,
} from "@ionic/angular";
import firebase from "firebase";
import { Storage } from "@ionic/storage";
import { ToastController } from "@ionic/angular";
import { ActivityService } from "./activity.service";
import { LanguageService } from "./language.service";
import { NativeMapContainerService } from "./native-map-container.service";
import { ProfileService } from "./profile.service";

@Injectable({
  providedIn: "root",
})
export class PopUpService {
  public onRequest = false;
  public mapComponent: any;
  public canDismiss = false;
  public calculateBtn = false;
  public price: number;
  public allowed = true;
  public uid: any;
  public hasCleared = false;
  public dismissLoader: any;
  public currency: any;
  toast: any;
  driverEnded = true;
  dismissLoader22: any;

  constructor(
    public act: ActivityService,
    public platform: Platform,
    public lp: LanguageService,
    private toastCtrl: ToastController,
    public storage: Storage,
    public cMap: NativeMapContainerService,
    public alert: AlertController,
    public ph: ProfileService,
    public load: LoadingController
  ) { }

  async showAlertNormal(title, subtitle, network) {
    const alert = await this.alert.create({
      header: title,
      subHeader: subtitle,
      buttons: [
        {
          text: this.lp.translate()[0].retrynew,
          role: "cancel",
          handler: () => {
            if (network) {
              this.clearAll(this.uid, true);
            }
          },
        },
      ],
      backdropDismiss: false,
    });
    await alert.present();
  }

  async showAlert(title, subtitle) {
    const alert = await this.alert.create({
      header: title,
      subHeader: subtitle,
      buttons: [
        {
          text: this.lp.translate()[0].accept,
          role: "cancel",
          handler: () => {
            this.cMap.map.setClickable(true);
          },
        },
      ],
      backdropDismiss: false,
    });
    await alert.present();
  }

  async presentSimpleLoader(message) {
    const loading = await this.load.create({
      // tslint:disable-next-line: object-literal-shorthand
      message: message,
    });

    await loading.present();
    console.log("RUnnin SImple Loader");
    const myInterval = setTimeout(() => {
      loading.dismiss();
      clearTimeout(myInterval);
    }, 500);
  }

  async SmartLoader(message) {
    const loading = await this.load.create({
      message: message,
    });

    await loading.present();
    console.log("Running Loader");
    const myInterval = setInterval(() => {
      loading.dismiss();
      clearInterval(myInterval);
    }, 500);
  }

  async presentToast(message) {
    const toast = await this.toastCtrl.create({
      // tslint:disable-next-line: object-literal-shorthand
      message: message,
      duration: 3000,
      position: "top",
    });
    toast.onDidDismiss();
    toast.present();
  }

  async showToast(message) {
    this.toast = await this.toastCtrl.create({
      // tslint:disable-next-line: object-literal-shorthand
      message: message,
      position: "top",
    });

    this.toast.present();
  }

  hideToast() {
    this.toast.dismiss();
  }

  async showPomp(title, message) {
    const alert = await this.alert.create({
      // tslint:disable-next-line: object-literal-shorthand
      header: title,
      subHeader: message,
      buttons: [
        {
          text: this.lp.translate()[0].accept,
          role: "cancel",
          handler: () => {
            this.clearAll(this.uid, true);
          },
        },
      ],
      backdropDismiss: false,
    });
    await alert.present();
  }

  async showPimp(title) {
    const alert = await this.alert.create({
      // tslint:disable-next-line: object-literal-shorthand
      header: title,
      buttons: [
        {
          text: "OK",
          role: "cancel",
          handler: () => { },
        },
      ],
      backdropDismiss: false,
    });
    await alert.present();
  }

  refactor() {
    this.cMap.onDestinatiobarHide = false;
    this.calculateBtn = false;
    document.getElementById("destination").innerHTML = "Set Destination";
  }

  async alertClosure(message) {
    const alert = await this.alert.create({
      header: message,
      buttons: [
        {
          text: "Exit",
          role: "cancel",
          handler: () => { },
        },
      ],
      backdropDismiss: true,
    });
    await alert.present();
  }

  clearAll(uid, can) {
    this.driverEnded = false;
    console.log(uid);
    // tslint:disable-next-line: prefer-const
    let customer = firebase.database().ref(`Customer/${uid}`);
    customer
      .remove()
      .then((success) => { })
      .catch((error) => { });
  }

  locatePosition(lat, lng) { }

  async presentRouteLoader(message) {
    const loading = await this.load.create({
      // tslint:disable-next-line: object-literal-shorthand
      message: message,
    });

    await loading.present();
    console.log("sdfggfsfsfsfs");
    const myInterval = setInterval(() => {
      if (this.canDismiss) {
        loading.dismiss();
        clearInterval(myInterval);
      }
    }, 1000);
  }

  async presentLoader(message) {
    this.dismissLoader = await this.load.create({
      message: message,
    });
    this.dismissLoader.present();
    console.log("Running Loader");
  }

  hideLoader() {
    this.dismissLoader.dismiss();
    console.log("LOADER DISMISSED");
  }
  async showLoader2(message) {
    this.dismissLoader22 = await this.load.create({
      message: message,
    });
    await this.dismissLoader22.present();
    console.log("SHOW  LOADER");
  }
  removeLoader2() {
    if (this.dismissLoader22) this.dismissLoader22.dismiss();
    //this.presentLoader = false;
    console.log("REMOVIN LOADER");
  }

  async showAlertComplex(title, message, accept, reject, iscancel) {
    const alert = await this.alert.create({
      header: title,
      // tslint:disable-next-line: object-literal-shorthand
      message: message,
      inputs: [
        {
          name: "long",
          label: this.lp.translate()[0].longpick,
          type: "checkbox",
          value: "true",
          checked: false,
        },
        {
          name: "incorrect",
          label: this.lp.translate()[0].incorrect,
          type: "checkbox",
          value: "false",
          checked: false,
        },
      ],
      buttons: [
        {
          text: this.lp.translate()[0].reject,
          role: "cancel",
          handler: () => {
            // tslint:disable-next-line: no-trailing-whitespace
          },
        },
        {
          text: this.lp.translate()[0].accept,
          handler: () => {
            if (iscancel) {
              this.clearAll(this.uid, true);
            }
          },
        },
      ],
      backdropDismiss: false,
    });
    await alert.present();
  }
}
