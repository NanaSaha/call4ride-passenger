import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
declare var google;

@Injectable({
  providedIn: 'root'
})
export class GeocoderService {

  public locationName: any;
  public lat: any;
  public lng: any;
  public destinationSetName: any;
  public locationChange = true;
  destinationChange: any;
  // tslint:disable-next-line: new-parens
  public geocoder: any = new google.maps.Geocoder;
  constructor(public platform: Platform) {

  }


  Geocode(address) {
    // tslint:disable-next-line: object-literal-key-quotes
    this.geocoder.geocode({ 'address': address }, (results, status) => {
      if (status === 'OK') {
        const position = results[0].geometry.location;
        this.lat = position.lat();
        this.lng = position.lng();
        console.log(this.lat);
      } else {
      }
    });
  }
 
  Reverse_Geocode(lat, lng, driverMode){
    
    let latlng = {lat: parseFloat(lat), lng: parseFloat(lng)};
    this.geocoder.geocode({'location': latlng}, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          if (!driverMode){
           
            if (this.locationChange){
              if (document.getElementById("location"))
              document.getElementById("location").innerText = results[0].formatted_address;
              this.locationName = results[0].formatted_address;
              console.log('location was chnaged')
            }
            if (this.destinationChange){
              if (document.getElementById("destination"))
              document.getElementById("destination").innerText = results[0].formatted_address;
              this.destinationSetName = results[0].formatted_address;
              console.log('destination was chnaged')
            }
          }else{
           console.log('This is not the driver mode')
         /// var driver_location = results[0].formatted_address;
          }

        } else {
         // window.alert('No results found');
        }
      } else {
       // window.alert('Geocoder failed due to: ' + status);
      }
    });

  }


  




  Simple_Geocode(lat, lng) {

    const latlng = { lat: parseFloat(lat), lng: parseFloat(lng) };
    let result;
    // tslint:disable-next-line: object-literal-key-quotes
    this.geocoder.geocode({ 'location': latlng }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          result = results[0].formatted_address;
        }
      }
    });
    return result;
  }

}
