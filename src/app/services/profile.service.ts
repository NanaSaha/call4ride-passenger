import { Injectable } from "@angular/core";
import firebase from "firebase";

@Injectable({
  providedIn: "root",
})
export class ProfileService {
  public getCode: any;
  public userProfile: any;
  public customer: any;
  public currentUser: firebase.User;
  public user: any;
  public phone: number;
  public WebAdminProfile: any;
  public ScheduledProfile: any;
  public promoProfile: any;
  public referalProfile: any;
  public referalProfile2: any;
  public drivers: any;
  public pools: any;
  public name: any;
  public CustomerOwnPropertyRef: any;
  public driver: any;
  public DriverProfile: any;
  public paymentType: any;
  public card: any;
  public email: any;
  public cvc: any;
  public uidProfile: firebase.database.Reference;
  public home: any;
  public work: any;
  public verificationID: any;
  public year: any;
  public month: any;
  public isHome: boolean = true;
  public pic: any;
  public id: any;
  public uid: any;
  public fare: any;
  public pricePerKm: any;
  public admin: any;
  public userID: any;
  public users: any;
  public login: boolean = false;
  public kit: boolean = false;
  public ratingText: any = "";
  public ratingValue: any = 0;
  public loadingState: boolean = false;
  public earnings: any;
  public IDProfile: any;
  public hasLoaded: boolean = false;
  public referal: any = "rty";
  public usedPromo: any;
  refEarning: any = 0;
  companyNews: any;
  public convrefEarning: any;
  userProfil: firebase.database.Reference;
  geofireDrivers: firebase.database.Reference;
  notify_ID: string;
  companyProfile: firebase.database.Reference;
  theKey: any;
  driverID: any;
  favoriteDriver: any;
  CancelledProfile: firebase.database.Reference;
  status: any = "Unverified";
  credits: number = 0;
  historyID: any;
  rating_positive: any;
  rating_negative: any;
  paymentToken: any;
  customerID: any;
  pricing: any;
  constructor() {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        // console.log(user)
        this.user = user;
        //console.log(this.user)
        this.id = this.user.uid;

        this.userProfil = firebase.database().ref(`userProfile/`);
        this.customer = firebase.database().ref(`Customer`);
        this.userProfile = firebase
          .database()
          .ref(`userProfile/${user.uid}/userInfo`);
        this.IDProfile = firebase.database().ref(`userProfile/`);
        this.WebAdminProfile = firebase.database().ref(`DashboardSettings`);
        this.CancelledProfile = firebase.database().ref(`Company`);
        this.DriverProfile = firebase
          .database()
          .ref(`driverProfile/${user.uid}`);
        this.ScheduledProfile = firebase.database().ref(`ScheduledRides`);
        this.uidProfile = firebase.database().ref(`SharingID/`);
        this.companyProfile = firebase.database().ref(`Company`);
        this.promoProfile = firebase.database().ref("SharingIDPromo");
        this.usedPromo = firebase.database().ref(`UsedPromo/${user.uid}`);

        this.pricing = firebase.database().ref(`pricing`);

        //  this.getCode = firebase.database().ref(`SharingIDPromo/`).orderByChild("code").equalTo("54ca2c11d1afc1612871624a");

        //       ref.child("studentList")
        //  .orderByChild("name")
        //  .equalTo("54ca2c11d1afc1612871624a")
        //  .on("child_added", function(snapshot) {
        //     console.log(snapshot.val());
        //   });

        this.referalProfile = firebase.database().ref(`userProfile/`);
        this.referalProfile2 = firebase.database().ref(`driverProfile/`);
        this.companyNews = firebase.database().ref(`News/User`);
        this.getDriverProfile().on("value", (userProfileSnapshot) => {
          this.driver = userProfileSnapshot.val();
        });

        this.users = firebase.database().ref(`userProfile`);
        this.drivers = firebase.database().ref(`Drivers/AllDrivers`);
        this.geofireDrivers = firebase.database().ref(`Drivers/Geofired`);
        this.pools = firebase.database().ref(`PoolRides`);
        this.CustomerOwnPropertyRef = firebase
          .database()
          .ref(`Customer/${user.uid}/client`);

        this.getUserProfil()
          .child(this.id)
          .on("child_added", (userProfileSnapshot) => {
            console.log(
              "SNAPSHOT ON GETUSERPROFIL::",
              userProfileSnapshot.val()
            );

            this.userID = userProfileSnapshot.val();
            this.home = userProfileSnapshot.val().Home;
            this.work = userProfileSnapshot.val().Work;
            this.favoriteDriver = userProfileSnapshot.val().favorite;
            this.phone = userProfileSnapshot.val().phonenumber
              ;
            this.pic = userProfileSnapshot.val().picture;
            this.verificationID = userProfileSnapshot.val().random;
            this.name = userProfileSnapshot.val().first_name;
            this.credits = userProfileSnapshot.val().credits;
            this.paymentType = userProfileSnapshot.val().payWith;
            this.paymentToken = userProfileSnapshot.val().payment_token;
            this.customerID = userProfileSnapshot.val().customerID;
            this.card = userProfileSnapshot.val().Card_Number;
            this.email = userProfileSnapshot.val().Card_email;
            this.cvc = userProfileSnapshot.val().Card_Cvc;
            this.year = userProfileSnapshot.val().Card_Year;
            this.month = userProfileSnapshot.val().Card_month;
            this.ratingText = userProfileSnapshot.val().ratingtext;
            this.ratingValue = userProfileSnapshot.val().rating;
            this.rating_positive = userProfileSnapshot.val().rating_positive;
            this.rating_negative = userProfileSnapshot.val().rating_negative;
            this.earnings = userProfileSnapshot.val().earnings;
            this.status = userProfileSnapshot.val().verified;
            if (userProfileSnapshot.val().referal) {
              this.referal = userProfileSnapshot.val().referal;
              let ref;
              if (userProfileSnapshot.val().referalID) {
                ref = userProfileSnapshot.val().referalID;
              } else {
                ref = "etter";
              }

              console.log(ref);
              this.convrefEarning = userProfileSnapshot.val().refEarning;
              if (ref.replace(/[^A-Z,0-9]/gi, "").length == 4) {
                this.getReferalProfile().on("value", (userProfileSnapshot) => {
                  if (userProfileSnapshot.val().refEarning)
                    this.refEarning = userProfileSnapshot.val().refEarning;
                });
              } else {
                this.getReferalProfile2().on("value", (userProfileSnapshot) => {
                  if (userProfileSnapshot.val().refEarning)
                    this.refEarning = userProfileSnapshot.val().refEarning;
                });
              }
            }
            console.log(this.referal);
          });
      }
    });
  }

  // createMyDocList(data, id): Promise<void> {
  //   return this.companyProfile
  //     .child("Rider")
  //     .child("documents")
  //     .child(`${id}`)
  //     .child(`${this.id}/`)
  //     .update({
  //       name: this.name,
  //       client: "Rider",
  //       data: data,
  //       comment: "none",
  //       expired: [2019, 6, 5],
  //       denied: "false",
  //       approved: "false",
  //     });
  // }

  // this.userProfileRef.child(newUser.user.uid).child("userInfo").set({
  //         email: email,
  //       });

  getUserProfile(): firebase.database.Reference {
    return this.userProfile;
  }

  getUserProfil(): firebase.database.Reference {
    return this.userProfil;
  }

  getyProfile(): firebase.database.Reference {
    return this.IDProfile;
  }

  getUserIDProfile(r): firebase.database.Reference {
    return this.IDProfile.child(r);
  }

  getReferalProfile(): firebase.database.Reference {
    return this.referalProfile.child(this.referal);
  }

  getReferalProfile2(): firebase.database.Reference {
    return this.referalProfile2.child(this.referal);
  }

  getDriverProfile(): firebase.database.Reference {
    return this.referalProfile2;
  }

  getNewsProfile(): firebase.database.Reference {
    return this.companyNews;
  }

  getWebAdminProfile(): firebase.database.Reference {
    return this.WebAdminProfile;
  }

  getCancelledProfile(): firebase.database.Reference {
    return this.CancelledProfile;
  }

  getScheduledProfile(id: any): firebase.database.Reference {
    return this.ScheduledProfile.child(id);
  }

  getUserAsClientInfo(): firebase.database.Reference {
    return this.customer;
  }

  getAllDrivers(): firebase.database.Reference {
    return this.drivers;
  }

  getAllUser(): firebase.database.Reference {
    return this.users;
  }

  getAllPool(): firebase.database.Reference {
    return this.pools;
  }

  getAllSharingID(code): firebase.database.Reference {
    return this.uidProfile.child(code);
  }

  getAllSharingPromoID(): firebase.database.Reference {
    return this.promoProfile;
  }

  getAllUsedCodes(): firebase.database.Reference {
    return this.usedPromo;
  }

  getPricing(): firebase.database.Reference {
    return this.pricing;
  }

  getSharingID(): firebase.database.Reference {
    return this.uidProfile;
  }
  getCompanyProfile(id: any): firebase.database.Reference {
    return this.companyProfile.child(`${id}/`);
  }

  getCompanies(): firebase.database.Reference {
    return this.companyProfile;
  }

  updateName(first_name: string): Promise<void> {
    return this.userProfile.update({
      name: first_name,
    });
  }

  updateRouteNumber(first_name: any): Promise<void> {
    return this.userProfile.update({
      routeNumber: first_name,
    });
  }

  RiderPromoSaved(rider_id, code, percentage, status): Promise<any> {
    return this.usedPromo.push({
      rider_id: rider_id,
      code: code,
      percentage: percentage,
      status: status,
    });
  }

  createDocList(data, id): firebase.database.Reference {
    return this.companyProfile
      .child("Rider")
      .child("documents")
      .child(`${id}/`)
      .push({
        data: data,
      });
  }

  createMyDocList(data, id): Promise<void> {
    return this.companyProfile
      .child("Rider")
      .child("documents")
      .child(`${id}`)
      .child(`${this.id}/`)
      .update({
        name: this.name,
        client: "Rider",
        data: data,
        comment: "none",
        expired: [2019, 6, 5],
        denied: "false",
        approved: "false",
      });
  }

  uploadDocFile(data, id): Promise<void> {
    return this.companyProfile
      .child("Rider")
      .child("documents")
      .child(`${id}`)
      .child(`${this.id}/`)
      .update({
        name: this.name,
        client: "Rider",
        data: data,
        comment: "none",
        expired: [2019, 6, 5],
        denied: "false",
        approved: "false",
      });
  }

  uploadDocFile2(data, id): Promise<void> {
    return this.companyProfile
      .child("Rider")
      .child("documents")
      .child(`${id}`)
      .child(`${this.id}/`)
      .update({
        data: data,
      });
  }

  ///Forr Refer for ride
  updatePromoID(id: string): Promise<void> {
    return this.promoProfile.child(id).update({
      id: [this.id],
    });
  }

  createPromo(code, percentage): Promise<void> {
    return this.promoProfile.push({
      code: code,
      percentage: percentage,
    });
  }

  updateRideID(id: string): Promise<void> {
    return this.userProfile
      .update({
        idForRide: id,
      })
      .then(() => {
        this.updatePromoID(id);
      });
  }

  ///Forr Refer for cash
  updateID(first_name: string): Promise<void> {
    return this.userProfile.update({
      id: first_name,
    });
  }

  M_updateID(id: any, name: any): Promise<void> {
    console.log(id, name);
    return this.userProfile.child("favorite").child(id).update({
      name: name,
    });
  }

  M_updateIDP(id: any, name: any): Promise<void> {
    console.log(id, name);
    return this.userProfile.child("rejected").child(id).update({
      name: name,
    });
  }

  updateGUID(first_name: string): Promise<void> {
    return this.uidProfile
      .child(first_name)
      .update({
        id: [this.id],
      })
      .then(() => {
        this.updateID(first_name);
      });
  }

  updateEmail(first_name: string): Promise<void> {
    return this.userProfile.update({
      email: first_name,
    });
  }

  UpdateNumber(first_name: string, number: number): Promise<any> {
    return this.userProfile.update({
      name: first_name,
      phone: number,
      rating: 0,
      ratingtext: "Not Yet Rated",
    });
  }

  UpdateNumbers(number: number): Promise<any> {
    return this.userProfile.update({
      phone: number,
    });
  }

  UpdateEmergencyNumbers(number: number): Promise<any> {
    return this.userProfile.update({
      emergencyNumber: number,
    });
  }

  UpdateUserLocation(lat: any, lng: any, id: any, bearing: any): Promise<any> {
    console.log("upadating in database");
    return firebase
      .database()
      .ref(`Customer/${id}/client`)
      .update({
        Client_location: [lat, lng],
        Client_Bearing: bearing
      });
  }

  UpdateEarnings(sum: any): Promise<any> {
    return this.userProfile.update({
      earnings: this.earnings + sum,
    });
  }

  UpdateRefEarnings(sum: any): Promise<any> {
    console.log(this.refEarning);
    return this.referalProfile.child(this.referal).update({
      refEarning: this.refEarning + sum,
    });
  }

  UpdateRefEarnings2(sum: any): Promise<any> {
    return this.referalProfile2.child(this.referal[0]).update({
      refEarning: this.refEarning + sum,
    });
  }

  UpdateHome(number: number): Promise<any> {
    return this.userProfile.update({
      Home: number,
    });
  }

  UpdateToll(number: any): Promise<any> {
    return this.userProfile.update({
      DeTol: number,
    });
  }

  UpdateBank(bank: any, account: any): Promise<any> {
    return this.userProfile.update({
      bank: bank,
      accountNumber: account,
    });
  }

  UpdateWork(number: number): Promise<any> {
    return this.userProfile.update({
      Work: number,
    });
  }

  UpdateCredits(number: number): Promise<any> {
    return this.userProfile.update({
      credits: number,
    });
  }

  UpdateDriverCredits(number: number, id: any): Promise<any> {
    return this.referalProfile2.child(id).child("userInfo").update({
      credits: number,
    });
  }

  UpdateReferal(number: number, id): Promise<any> {
    return this.userProfile.set({
      referal: number,
      referalID: id,
    });
  }

  updateDestination(number: number): Promise<any> {
    return this.userProfile.update({
      Work: number,
    });
  }

  createHistory(
    name: string,
    price: number,
    date: any,
    location: number,
    destination: number,
    realP: any,
    waitTimeCost: any,
  ): Promise<any> {
    return this.userProfile
      .child("/eventList")
      .push({
        user_name: this.name,
        name: name,
        price: price,
        date: date,
        location: location,
        destination: destination,
        realPrice: realP,
        waitTimeCost: waitTimeCost
      })
      .then((id) => {
        this.historyID = id;
        this.getUserProfile().on("value", (us) => {
          console.log(us.val(), id);
          if (us.val().Driver_id) {
            this.pushToHistory(us.val().Driver_id, us.val().Driver_key, id.key);
          }
        });
      });
  }

  updateHistory(id: any, upvote: any, downvote: any, tip: any): Promise<any> {
    return this.userProfile
      .child("/eventList")
      .child(id)
      .update({
        tip: tip,
        upvote: upvote,
        downvote: downvote,
      })
      .then((id) => {
        this.historyID = id;
      });
  }

  pushToHistory(id: any, key: any, histID: any): Promise<any> {
    return this.userProfile
      .child("/eventList")
      .child(histID)
      .update({
        driver_id: id,
        driver_key: key,
      })
      .then((id) => {
        this.historyID = id;
      });
  }

  updateHistoryTip(price: any, id: any): Promise<any> {
    return this.userProfile.child("/eventList").child(id).update({
      tip: price,
    });
  }

  updateDriiverTip(price: any, id: any, key): Promise<any> {
    return this.referalProfile2
      .child(id)
      .child("userInfo")
      .child("/eventList")
      .child(key)
      .update({
        tip: price,
      });
  }

  updateHistoryVoteUp(id: any, up: any): Promise<any> {
    return this.userProfile.child("/eventList").child(id).update({
      upvote: up,
    });
  }

  updateHistoryVoteUpSingle(id: any, up: any): Promise<any> {
    return this.userProfile.child("/eventList").child(id).update({
      upvote: up,
    });
  }

  updateHistoryVoteDown(id: any, down: any): Promise<any> {
    return this.userProfile.child("/eventList").child(id).update({
      downvote: down,
    });
  }

  updateHistoryVoteDownSingle(id: any, down: any): Promise<any> {
    return this.userProfile.child("/eventList").child(id).update({
      downvote: down,
    });
  }

  UpdatePhoto(pic: any): Promise<any> {
    return this.userProfile.update({
      picture: pic,
    });
  }

  UpdateGuid(pic: any): Promise<any> {
    return this.userProfile.update({
      GUID: [pic, this.id],
    });
  }

  UpdateEarning(price: any): Promise<any> {
    return this.userProfile.update({
      earnings: price,
    });
  }

  UpdateUserRating(rate: any, text: any): Promise<any> {
    return this.userProfile.update({
      rating: rate,
      ratingtext: text,
    });
  }

  PushRandomNumber(number: number): Promise<any> {
    return this.userProfile.update({
      random: number,
    });
  }

  Complain(value: any): Promise<any> {
    return firebase.database().ref(`DashboardSettings/user/complains`).push({
      complain: value,
      email: this.user.email,
    });
  }

  RateDriver(id: any, rScore: any, text: any, positive: any): Promise<any> {
    return firebase.database().ref(`driverProfile/${id}/userInfo`).update({
      rating: rScore,
      review: text,
    });
  }

  Tip(id: any, val: any): Promise<any> {
    return firebase.database().ref(`driverProfile/${id}/userInfo`).update({
      tip: val,
    });
  }

  PositiveRateDriver(id: any, value: any): Promise<any> {
    return firebase.database().ref(`driverProfile/${id}/userInfo`).update({
      rating_positive: value,
    });
  }

  NegativeRateDriver(id: any, value: any): Promise<any> {
    return firebase.database().ref(`driverProfile/${id}/userInfo`).update({
      rating_negative: value,
    });
  }

  Request(id: any): Promise<any> {
    return firebase
      .database()
      .ref(`driverProfile/${id}/userInfo/favorite`)
      .update({
        favoriteSeek: true,
        id: [this.id, this.name],
      })
      .then(() => { });
  }

  Rater(id: any, rate: any): Promise<any> {
    return firebase
      .database()
      .ref(`driverProfile/${id}/userInfo/rater`)
      .update({
        rating: true,
        rate: rate,
      })
      .then(() => { });
  }

  Tippen(id: any, est: any): Promise<any> {
    return firebase
      .database()
      .ref(`driverProfile/${id}/userInfo/tipper`)
      .update({
        tippen: true,
        tipped: est,
      })
      .then(() => { });
  }

  ApprovePickup(value: boolean, id: any): Promise<any> {
    return firebase.database().ref(`Customer/${id}/client`).update({
      Client_PickedUp: value,
    });
  }

  Cancelled(
    chat: any,
    name: string,
    date: any,
    location: any,
    destination: any,
    price: any,
    reason: any,
    charge: any,
    surcharge: any,
    realP: any,
    osc: any
  ): Promise<any> {
    return firebase.database().ref(`Cancelled/`).push({
      user_name: this.name,
      chat: chat,
      name: name,
      date: date,
      location: location,
      destination: destination,
      price: price,
      reason: reason,
      charge: charge,
      surcharge: surcharge,
      realPrice: realP,
      osc: osc,
    });
  }

  CancelledMe(
    name: string,
    date: any,
    location: any,
    destination: any,
    price: any,
    reason: any,
    charge: any,
    surcharge: any,
    realP: any,
    osc: any
  ): Promise<any> {
    return this.userProfile.child(`cancelled/`).push({
      user_name: this.name,
      name: name,
      date: date,
      location: location,
      destination: destination,
      price: price,
      reason: reason,
      charge: charge,
      surcharge: surcharge,
      realPrice: realP,
      osc: osc,
    });
  }

  CancelledMe2(
    id: any,
    name: string,
    date: any,
    location: any,
    destination: any,
    price: any,
    reason: any,
    charge: any,
    surcharge: any,
    realP: any,
    osc: any
  ): Promise<any> {
    return this.referalProfile2.child(id).child(`userInfo/cancelled/`).push({
      name: name,
      user_name: this.name,
      date: date,
      location: location,
      destination: destination,
      price: price,
      reason: reason,
      charge: charge,
      surcharge: surcharge,
      realPrice: realP,
      osc: osc,
    });
  }

  Completed(
    chat: any,
    id: any,
    name: string,
    date: any,
    location: any,
    destination: any,
    price: any,
    toll: any,
    date1: any,
    date2: any,
    date3: any,
    location2: any,
    destination2: any,
    surcharge: any,
    realP: any,
    osc: any,
    waitTimeCost: any
  ): Promise<any> {
    return firebase.database().ref(`Completed`).push({
      chat: chat,
      name: name,
      user_name: name,
      date: date,
      location: location,
      destination: destination,
      price: price,
      toll: toll,
      date1: date1,
      date2: date2,
      date3: date3,
      location2: location2,
      destination2: destination2,
      surcharge: surcharge,
      realPrice: realP,
      osc: osc,
      waitTimeCost: waitTimeCost
    });
  }

  ApproveDrop(value: boolean, id: any): Promise<any> {
    return firebase.database().ref(`Customer/${id}/client`).update({
      Client_Dropped: value,
    });
  }

  SendMessage(value: any, id: any): Promise<any> {
    return firebase.database().ref(`Customer/${id}/client/Chat`).push({
      Client_Message: value,
    });
  }

  SendMessage2(value: any, id: any): Promise<any> {
    return firebase.database().ref(`Customer/${id}/client/Chat`).push({
      Client_Audio: value,
    });
  }

  CanCharge(value: boolean, id: any): Promise<any> {
    return firebase.database().ref(`Customer/${id}/client`).update({
      Client_CanChargeCard: value,
    });
  }

  UpdatePaymentType(number: number): Promise<any> {
    return this.userProfile.update({
      payWith: number,
    });
  }

  UseCard(id: any, sd: any): Promise<any> {
    return this.userProfile.update({
      customerID: sd,
      source: id,
    });
  }

  AddPaymentCard(card: any, cust: any): Promise<any> {
    return this.userProfile.child("CreditCards").push({
      card: card,
      checked: false,
      customerID: cust,
    });
  }

  AddDestination(card: any): Promise<any> {
    return this.userProfile.child("Places").push({
      place: card,
    });
  }

  CheckCard(id: any): Promise<any> {
    return this.userProfile.child("CreditCards").child(id).update({
      checked: true,
    });
  }

  DitchCard(id: any): Promise<any> {
    return this.userProfile.child("CreditCards").child(id).update({
      checked: false,
    });
  }

  send_sms(recipient_number, message_body) {
    console.log("SMS HITT")
    const data = {
      recipient_number: recipient_number,
      message_body: message_body
    }
    const options = {
      method: 'POST',
      // headers: {
      //   'Content-Type': 'application/json',
      //   //'Access- Control-Allow-Origin': "*",

      // },
      body: JSON.stringify(data)
    }
    fetch(`https://api.ghingerhealth.com/sendsms`, options)
      .then(async response => {
        console.log("RESPONSE FORM SMS::", response)

      })
      .catch(e => {
        console.log("ERRO RESPONSE FORM SMS::", e)
      })
  }
}
