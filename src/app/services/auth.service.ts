import { Injectable, Injector } from "@angular/core";
import { LanguageService } from "./language.service";
import { PopUpService } from "./pop-up.service";
import { Platform, NavController, AlertController } from "@ionic/angular";
import firebase from "firebase";
import { Router } from '@angular/router';
import { ProfileService } from "src/app/services/profile.service";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  confirmationResult: firebase.auth.ConfirmationResult;
  public fireAuth: firebase.auth.Auth;
  public userProfileRef: firebase.database.Reference;
  private currentUser: firebase.User;

  constructor(
    public lp: LanguageService,
    protected injector: Injector,
    public pop: PopUpService,
    public platform: Platform,
    public router: Router,
    public alertCtrl: AlertController,
    public ph: ProfileService
  ) {
    this.fireAuth = firebase.auth();
    this.userProfileRef = firebase.database().ref("/userProfile");
    firebase
      .auth()
      .onAuthStateChanged((user: firebase.User) => (this.currentUser = user));
  }
  get navCtrl(): NavController {
    // tslint:disable-next-line: deprecation
    return this.injector.get(NavController);
  }



  loginUser(email: string, password: string): Promise<any> {
    return this.fireAuth.signInWithEmailAndPassword(email, password).then((result) => {
      console.log("SIGIN RESULT:::" + JSON.stringify(result));
      console.log("UID:::" + result.user.uid);


      const unsubscribe2 = firebase.auth().onAuthStateChanged((user) => {
        this.ph
          .getUserProfil()
          .child(this.ph.id)
          .on("value", (userProfileSnapshot) => {
            console.log("USER::", user);

            console.log("USER PROFILE SNAPSHOT::", userProfileSnapshot.val());
            console.log("FIRST NAME USER PROFILE SNAPSHOT::", userProfileSnapshot.val().first_name);
            console.log("PROFILE ID::", this.ph.id);



            if (user != null) {
              if (userProfileSnapshot.val() == null && userProfileSnapshot.val().first_name == undefined) {

                this.navCtrl.navigateRoot("update-users-info");
              }
              else {
                console.log("Im home");

                this.navCtrl.navigateRoot("home");


              }
            }
            else {
              this.navCtrl.navigateRoot("login");
            }
          });
      });

      //   const unsubscribe =  firebase.auth()
      //   .onAuthStateChanged((user) =>{

      //     this.ph.getUserProfile().child(this.ph.id).on("value", async (userProfileSnapshot) => {


      //   // this.ph.getUserProfile().on("value", (userProfileSnapshot) => {
      //     console.log("USER PROFILE:: ", userProfileSnapshot.val());
      //     const user_details = userProfileSnapshot.val();
      //     console.log("USER PROFILE user_details:: ", user_details);
      //     if (user_details == null) {

      //       this.navCtrl.navigateRoot("update-users-info");
      //             } else {
      //       console.log("Im home");

      //         this.navCtrl.navigateRoot("home");


      //     }
      //   });

      // });


    })
      .catch(async (error) => {
        console.log("ERROR SIGNING IN" + error.message);
        let alert = await this.alertCtrl.create({
          message: "User account cannot be found. Kindly register first",
          buttons: [
            {
              text: "Cancel",
              role: "cancel",
            },
          ],
        });
        alert.present();

      });;
  }

  signupUser(email: string, password: string): Promise<any> {
    return this.fireAuth
      .createUserWithEmailAndPassword(email, password)
      .then((newUser) => {
        this.userProfileRef.child(newUser.user.uid).child("userInfo").set({
          email,
        });

        this.navCtrl.navigateRoot("update-users-info");

        // this.router.navigate(["more-info"]);


      });
  }
  // signupUser(email: string, password: string): Promise<any> {
  //   return firebase
  //     .auth()
  //     .createUserWithEmailAndPassword(email, password)
  //     .then((newUser) => {
  //       console.log("NEW USER CREATED:: --", newUser);
  //       this.userProfileRef.child(newUser.user.uid).child("userInfo").set({
  //         email: email,
  //       });
  //     });
  // }

  public signInWithPhoneNumber(recaptchaVerifier, phoneNumber) {
    console.log("PHONE::", phoneNumber);
    console.log("recaptchaVerifier::", recaptchaVerifier);
    return new Promise<any>((resolve, reject) => {
      return firebase
        .auth()
        .signInWithPhoneNumber(phoneNumber, recaptchaVerifier)
        .then((confirmationResult) => {
          console.log("confirmationResult", confirmationResult);
          this.confirmationResult = confirmationResult;
          resolve(confirmationResult);
        })
        .catch((error) => {
          console.log(error);
          reject("SMS not sent");
        });
    });
  }
  public async enterVerificationCode(code) {
    return new Promise<any>((resolve, reject) => {
      this.confirmationResult
        .confirm(code)
        .then(async (result) => {
          console.log("RESULTS", result);
          const user = result.user;
          resolve(user);
        })
        .catch((error) => {
          console.log("ERROR IN VERIFICATION", error);
          reject(error.message);
        });
    });
  }

  resetPassword(email: string): Promise<void> {
    return firebase.auth().sendPasswordResetEmail(email);
  }

  logoutUser(): Promise<void> {
    this.userProfileRef
      .child(firebase.auth().currentUser.uid)
      .child("userInfo")
      .off();
    return firebase.auth().signOut();
  }

  get authenticated(): boolean {
    return this.currentUser !== null;
  }

  signOut(): void {
    firebase.auth().signOut();
  }

  displayName(): string {
    if (this.currentUser !== null) {
      return this.currentUser.displayName;
    } else {
      return "";
    }
  }
}
