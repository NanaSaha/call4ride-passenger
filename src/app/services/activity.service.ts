import { Injectable } from "@angular/core";
import firebase from "firebase";

@Injectable({
  providedIn: "root",
})
export class ActivityService {
  public userProfile: any;
  public CustomerOwnPropertyRef: any;
  public currentDriverRef: any;
  public PoolRef: any;
  public connectedRef: any;
  public user: any;
  constructor() {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
       
        this.user = user;
        this.currentDriverRef = firebase.database().ref(`Drivers/AllDrivers`);
        this.CustomerOwnPropertyRef = firebase.database().ref(`Customer`);
        this.PoolRef = firebase.database().ref(`Pools/${user.uid}`);
      }
    });
  }

  getCurrentDriver(id: any): firebase.database.Reference {
    return this.currentDriverRef.child(id);
  }

  getPoolActivityProfile(id: any): firebase.database.Reference {
    return this.CustomerOwnPropertyRef.child(`${id}/client`);
  }

  getActivityProfile(id: any): firebase.database.Reference {
   
    return this.CustomerOwnPropertyRef.child(`${id}/client`);
  }

  getActiveProfile(id: any): firebase.database.Reference {
    return firebase.database().ref(`Customer/${id}`);
  }

  getPoolProfile(): firebase.database.Reference {
    return this.PoolRef;
  }
  getActivProfile(): firebase.database.Reference {
    return firebase.database().ref(`Customer/`);
  }

  getConnectionState(): firebase.database.Reference {
    return this.connectedRef;
  }

  RequestPool(lat: number, lng: number, driverid: any): Promise<any> {
    return this.PoolRef.set({
      pool_details: [lat, lng, driverid],
    });
  }
}
