import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ProfileService } from './profile.service';
import { PopUpService } from './pop-up.service';
import { ActivityService } from './activity.service';
import { SettingsService } from './settings.service';
import { EventService } from './event.service';
import { LanguageService } from './language.service';

@Injectable({
  providedIn: 'root'
})
export class StripeService {

  constructor(public prof: ProfileService, public pop: PopUpService, public act: ActivityService,
    public lp: LanguageService, public settings: SettingsService, private http: HttpClient,
    public eProvider: EventService) {
    console.log('Hello StripeProvider Provider');
  }


  ChargeCard(amt, id) {
    // const data = 'stripetoken=' + this.prof.card + '&amount=' + amt;
    // const headers = new HttpHeaders();
    // headers.append('Conent-Type', 'application/x-www-form-urlencoded');
    // // tslint:disable-next-line: object-literal-shorthand
    // this.http.post('http://localhost:3333/processpay', data, { headers: headers }).subscribe((res) => {
    //   if (res) {
    //     this.pop.hideLoader();
    //   }
    //   this.act.getActivityProfile(id).update({
    //     Client_hasPaid: true,
    //   });
    // });
  }

}
