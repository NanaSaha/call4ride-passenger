import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { ActivityService } from './activity.service';
import { LanguageService } from './language.service';
import { EventService } from './event.service';
import { PopUpService } from './pop-up.service';
@Injectable({
  providedIn: 'root'
})
export class PaystackService {

  public isDone = false;
  public cardCharge = false;
  constructor(public act: ActivityService, public lp: LanguageService, public eProvider: EventService,
    public platform: Platform, public pop: PopUpService) {

  }

  UpdateCard(card: string, month: any, year: any, cvc: any, amount: any, email: any, driverPay: any) {
    this.eProvider.UpdateCard(card, month, year, cvc, amount, email, driverPay).then(success => {

    }).catch(error => { });
  }



  ChargeCard(card, month, cvc, year, amount, email, id) {
    this.pop.presentLoader(this.lp.translate()[0].proccessingcharge);

    this.platform.ready().then(() => {
      if (this.platform.is('cordova')) {

        // tslint:disable-next-line: no-angle-bracket-type-assertion
        (<any>window).window.PaystackPlugin.chargeCard(
          (resp) => {
            this.pop.hideLoader();
            this.act.getActivityProfile(id).update({
              Client_hasPaid: true,
            });
          },
          (resp) => {
            this.pop.hideLoader();
            this.act.getActivityProfile(id).update({
              Client_paidCash: true,
            });
          },
          {
            cardNumber: card,
            expiryMonth: month,
            expiryYear: year,
            // tslint:disable-next-line: object-literal-shorthand
            cvc: cvc,
            // tslint:disable-next-line: object-literal-shorthand
            email: email,
            amountInKobo: amount,
          });
      } else {

      }
    });


  }


}
