import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouteReuseStrategy } from "@angular/router";
import { HttpClientModule } from "@angular/common/http";

import { InAppBrowser } from "@ionic-native/in-app-browser/ngx";
import { IonicModule, IonicRouteStrategy } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { CallNumber } from "@ionic-native/call-number/ngx";
import { Camera } from "@ionic-native/camera/ngx";
import { OneSignal } from "@ionic-native/onesignal/ngx";
import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";
import { Vibration } from "@ionic-native/vibration/ngx";
import { HTTP } from "@ionic-native/http/ngx";
import { SocialSharing } from "@ionic-native/social-sharing/ngx";
import { IonicRatingModule } from "ionic4-rating";

import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { Firebase } from "@ionic-native/firebase/ngx";
import * as firebase from "firebase";
import { IonicStorageModule } from "@ionic/storage";
import { AutocompletePage } from "./pages/autocomplete/autocomplete.page";
import { DriverInfoPage } from "./pages/driver-info/driver-info.page";
import { TripInfoPage } from "./pages/trip-info/trip-info.page";
import { ChatPage } from "./pages/chat/chat.page";
import { AutocompletePageModule } from "./pages/autocomplete/autocomplete.module";
import { DriverInfoPageModule } from "./pages/driver-info/driver-info.module";
import { TripInfoPageModule } from "./pages/trip-info/trip-info.module";
import { ChatPageModule } from "./pages/chat/chat.module";
import { RatePage } from "./pages/rate/rate.page";
import { RatePageModule } from "./pages/rate/rate.module";
import { environment } from "src/environments/environment";
import { NgOtpInputModule } from "ng-otp-input";
import { IonicSelectableModule } from "ionic-selectable";
import { Geolocation } from "@ionic-native/geolocation/ngx";


// Import ionic-rating module



export const firebaseConfig = {
  // apiKey: "AIzaSyApjpwWIXFsAn6WWgauNG93bHdAY126eVw",
  // authDomain: "ghana-c4r.firebaseapp.com",
  // projectId: "ghana-c4r",
  // storageBucket: "ghana-c4r.appspot.com",
  // databaseURL: "https://ghana-c4r-default-rtdb.firebaseio.com",
  // messagingSenderId: "516821551729",
  // appId: "1:516821551729:web:04f1a30ca01b8acc9d29d0",
  // measurementId: "G-2Y04YP8P7Q"


  apiKey: "AIzaSyDtQGdYGuIQ7f-r9JqWBBj7q6RJJ9595nI",
  authDomain: "call4ride-2fe35.firebaseapp.com",
  projectId: "call4ride-2fe35",
  storageBucket: "call4ride-2fe35.appspot.com",
  databaseURL: "https://call4ride-2fe35-default-rtdb.firebaseio.com",
  messagingSenderId: "1092796986980",
  appId: "1:1092796986980:web:24d071186167016803d171",
  measurementId: "G-EPD48CQC9J"
};



firebase.initializeApp(firebaseConfig);

@NgModule({
  declarations: [AppComponent],
  entryComponents: [
    // AutocompletePage,
    // DriverInfoPage,
    // TripInfoPage,
    // ChatPage,
    // RatePage,
  ],
  imports: [
    IonicRatingModule,
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    IonicStorageModule.forRoot(),
    HttpClientModule,
    // AutocompletePageModule,
    // DriverInfoPageModule,
    // TripInfoPageModule,
    // ChatPageModule,
    // RatePageModule,
    NgOtpInputModule,
    IonicSelectableModule,
  ],
  providers: [
    StatusBar,
    Vibration,
    HTTP,
    SplashScreen,
    SocialSharing,
    Firebase,
    InAppBrowser,
    Camera,
    OneSignal,
    CallNumber,
    Geolocation,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
