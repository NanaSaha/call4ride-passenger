
import { Component, OnInit } from "@angular/core";
import {
  Platform,
  LoadingController,
  NavController,
  AlertController,
  MenuController,
  ActionSheetController,
} from "@ionic/angular";

import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { EmailValidator } from "src/validators/email";
import { LanguageService } from "src/app/services/language.service";
import { SettingsService } from "src/app/services/settings.service";
import { ProfileService } from "src/app/services/profile.service";
import { NativeMapContainerService } from "src/app/services/native-map-container.service";
import { AuthService } from "src/app/services/auth.service";
import firebase from "firebase";
import { ActivatedRoute } from "@angular/router";
import { Camera, CameraOptions } from "@ionic-native/camera/ngx";
import { PopUpService } from "src/app/services/pop-up.service";
import { NavParams, ModalController } from "@ionic/angular";


@Component({
  selector: 'app-someone',
  templateUrl: './someone.page.html',
  styleUrls: ['./someone.page.scss'],
})
export class SomeonePage implements OnInit {
  public updateForm: FormGroup;
  public initState = false;
  minSelectabledate: any;
  maxSelectabledate: any;
  date: any;
  captureDataUrl: string;
  public userProfileRef: firebase.database.Reference;
  loading: Promise<HTMLIonLoadingElement>;
  from_phone;
  phonenumber;
  public signupForm: any;
  step: any = 1;
  signupVal;
  jsonBody;
  email;
  constructor(
    public navCtrl: NavController,
    public lp: LanguageService,
    public settings: SettingsService,
    public ntP: NativeMapContainerService,
    public platform: Platform,
    public menu: MenuController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public authProvider: AuthService,
    public ph: ProfileService,
    public formBuilder: FormBuilder,
    private route: ActivatedRoute,
    public actionSheetCtrl: ActionSheetController,
    public camera: Camera,
    public pop: PopUpService,
    public _form: FormBuilder,
    public modal: ModalController,
  ) {

    this.date = new Date();
    // this.minSelectabledate = this.formatDate(this.date);
    // this.maxSelectabledate = this.formatDatemax(this.date);
    this.userProfileRef = firebase.database().ref("/userProfile");
    this.menu.enable(false);
    this.updateForm = formBuilder.group({
      first_name: ["", Validators.compose([Validators.required])],
      number: ["", Validators.compose([Validators.required])],
   

    });



  }

  ionViewWillEnter() {
    console.log("INSIDE RATTTTIINN *******");
  }

  goBack() {
    this.modal.dismiss(1);
  }


  async updateUser() {
    if (!this.updateForm.valid) {
      console.log(this.updateForm.value);
    } else {
      let number = this.updateForm.value.number;

      let first_name = this.updateForm.value.first_name;
    

      console.log("FIRST NAME" + first_name);
    

      console.log("UPDATE FORM VALUES", this.updateForm.value);
    
      this.modal.dismiss(this.updateForm.value);

    }
  }





 

  // logForm() {
  //   console.log(this.todo);
  //   if (this.rateNumber != null) {
  //     const value = this.data;
  //     console.log(this.rateNumber);
  //     this.prof
  //       .RateDriver(value, this.rateNumber, this.todo.description, true)
  //       .then((suc) => {
  //         // this.navCtrl.pop();
  //       });
  //     this.modal.dismiss(1);
  //   } else {
  //     this.pop.showPimp(this.lp.translate()[0].alertcar);
  //   }
  // }


  ngOnInit() { }
}

