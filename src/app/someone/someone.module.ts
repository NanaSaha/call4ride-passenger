// import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';
// import { FormsModule } from '@angular/forms';
// import { Routes, RouterModule } from '@angular/router';

// import { IonicModule } from '@ionic/angular';

// import { SomeonePage } from './someone.page';

// const routes: Routes = [
//   {
//     path: '',
//     component: SomeonePage
//   }
// ];

// @NgModule({
//   imports: [
//     CommonModule,
//     FormsModule,
//     IonicModule,
//     RouterModule.forChild(routes)
//   ],
//   declarations: [SomeonePage]
// })
// export class SomeonePageModule { }








import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";
import {  ReactiveFormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";
import {
  NgModule,
  CUSTOM_ELEMENTS_SCHEMA,
  NO_ERRORS_SCHEMA,
} from "@angular/core";
import { IonicRatingModule } from "ionic4-rating";

import { SomeonePage } from './someone.page';

const routes: Routes = [
  {
    path: "",
    component: SomeonePage,
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    IonicRatingModule,
    RouterModule.forChild(routes),
  ],
  exports: [
    // RatePage, //<----- this is if it is going to be used else where
  ],
  declarations: [
    // other components
    // SomeonePage,
  ],
  // declarations: [RatePage],
  // entryComponents: [RatePage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class SomeonePageModule { }














