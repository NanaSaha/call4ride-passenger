import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";
import {
  NgModule,
  CUSTOM_ELEMENTS_SCHEMA,
  NO_ERRORS_SCHEMA,
} from "@angular/core";
import { IonicRatingModule } from "ionic4-rating";

import { RatePage } from "./rate.page";

const routes: Routes = [
  {
    path: "",
    component: RatePage,
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IonicRatingModule,
    RouterModule.forChild(routes),
  ],
  exports: [
    // RatePage, //<----- this is if it is going to be used else where
  ],
  // declarations: [RatePage],
  // entryComponents: [RatePage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class RatePageModule {}
