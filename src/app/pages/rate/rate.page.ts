import { Component, OnInit, ViewChild, ElementRef, Input } from "@angular/core";
import { LanguageService } from "src/app/services/language.service";
import { PopUpService } from "src/app/services/pop-up.service";
import { NavController, NavParams, ModalController } from "@ionic/angular";
import { ProfileService } from "src/app/services/profile.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-rate",
  templateUrl: "./rate.page.html",
  styleUrls: ["./rate.page.scss"],
})
export class RatePage implements OnInit {
  @ViewChild("myInput", { static: true }) myInput: ElementRef;
  rate;
  @Input() eventId;
  @Input() positive_Rating;
  @Input() negative_Rating;
  // @Input() name;
  @Input() time;
  // @Input() price;
  @Input() m_ID;
  @Input() custom_ID;
  @Input() waitTimeCost;
  @Input() price;

  name: any = this.route.snapshot.paramMap.get("name");
 // price: any = this.route.snapshot.paramMap.get("price");
//  waitTimeCost: any = this.route.snapshot.paramMap.get("waitTimeCost");
  public rateNumber: any;
  todo = {
    description: " ",
  };
  data: any;
  total: any;
  constructor(
    public navCtrl: NavController,
    public lp: LanguageService,
    public pop: PopUpService,
    public actRoute: ActivatedRoute,
    public prof: ProfileService,
    public modal: ModalController,
    public route: ActivatedRoute,
    public navParams: NavParams
  ) {

   
    this.price = this.navParams.get('price');
    this.waitTimeCost = this.navParams.get('waitTimeCost');
    
    console.log("Driver Name:::", this.name);
    console.log("Ride Cost:::", this.price);
    console.log("Wait time Cost:::", this.waitTimeCost);

    this.total = this.waitTimeCost + this.price
    console.log("Total Cost will be :::", this.total)
    
    this.actRoute.queryParams.subscribe((params) => {
      if (params && params.special) {
        this.data = JSON.parse(params.special);
        console.log("RATTTTIINN DATA *******", this.data);
      }
    });
  }

  ionViewWillEnter() {
    console.log("INSIDE RATTTTIINN *******");
  }

  onRateChange($event) {
    this.rateNumber = $event;
    console.log("RATING NUMBER__-->>", this.rateNumber);
    this.pop.presentLoader("");

    setTimeout(() => {
      this.pop.hideLoader();
    }, 1000);
  }

  logForm() {
    console.log(this.todo);
    if (this.rateNumber != null) {
      const value = this.data;
      console.log(this.rateNumber);
      this.prof
        .RateDriver(value, this.rateNumber, this.todo.description, true)
        .then((suc) => {
          // this.navCtrl.pop();
        });
      this.modal.dismiss(1);
    } else {
      this.pop.showPimp(this.lp.translate()[0].alertcar);
    }
  }

  closeModal() {
    this.modal.dismiss(2);
  }

  acceptModal() {
    console.log("ACCEPTED REQUEST");
    this.modal.dismiss(1);
  }

  ngOnInit() {}
}
