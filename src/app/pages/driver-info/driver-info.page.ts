import { Component, OnInit, Input } from "@angular/core";
import { NavController, ModalController, AlertController } from "@ionic/angular";
import { ProfileService } from "src/app/services/profile.service";
import { LanguageService } from "src/app/services/language.service";
import { SettingsService } from "src/app/services/settings.service";
import { PopUpService } from "src/app/services/pop-up.service";
import { CallNumber } from "@ionic-native/call-number/ngx";

@Component({
  selector: "app-driver-info",
  templateUrl: "./driver-info.page.html",
  styleUrls: ["./driver-info.page.scss"],
})
export class DriverInfoPage implements OnInit {
  @Input() info;
  items: any[];
  currentCar: any;
  constructor(
    public navCtrl: NavController,
    public ph: ProfileService,
    public lp: LanguageService,
    public settings: SettingsService,
    public pop: PopUpService,
    public modal: ModalController,
    public callNumber: CallNumber,
     public alertCtl: AlertController
  ) {}

  ionViewDidLoad() {
    console.log("INSIDE DRIVER INFOR PAGE-- CHECK INF0::", this.info);
  }

  closeModal() {
    this.modal.dismiss();
  }

  onChange(e) {
    // this.modal.dismiss(e);
    this.presentAlertCheckbox(e)
  }

  CallDriver() {
    window.open("tel:" + this.info.Driver_number);
    //  window.open(`tel:this.info.Driver_number`, "_system");
    // this.callNumber
    //   .callNumber(this.info.Driver_number, true)
    //   .then((res) => console.log("Launched dialer!", res))
    //   .catch((err) => console.log("Error launching dialer", err));
  }

  ngOnInit() {
    console.log("INSIDE DRIVER INFOR PAGE-- CHECK INF0::", this.info);
  }


  async presentAlertCheckbox(e) {
    const alert = await this.alertCtl.create({
      header: 'Reason for cancellation ',
      inputs: [
        {
          name: 'checkbox1',
          type: 'radio',
          label: 'Driver is too far',
          value: 'pickup',
          checked: true
        },

        {
          name: 'checkbox2',
          type: 'radio',
          label: 'Driver is not coming',
          value: 'gps'
        },

        {
          name: 'checkbox3',
          type: 'checkbox',
          label: 'Wrong car appears',
          value: 'rider'
        },

        {
          name: 'checkbox4',
          type: 'radio',
          label: 'Wrong number plate',
          value: 'value4'
        },

        {
          name: 'checkbox5',
          type: 'radio',
          label: 'Others',
          value: 'others'
        },

      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: () => {
            console.log('Confirm Ok');
            this.modal.dismiss(e);
          }
        }
      ]
    });

    await alert.present();
  }
}
