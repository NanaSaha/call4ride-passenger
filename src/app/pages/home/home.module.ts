import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";
import { Routes, RouterModule } from "@angular/router";

import { AutocompletePage } from "../autocomplete/autocomplete.page";
import { AutocompletePageModule } from "../autocomplete/autocomplete.module";

import { DriverInfoPage } from "../driver-info/driver-info.page";
import { TripInfoPage } from "../trip-info/trip-info.page";
import { ChatPage } from "../chat/chat.page";

import { HomePage } from "./home.page";
import { RatePage } from "../rate/rate.page";
import { SomeonePage } from '../../someone/someone.page';
import { IonicRatingModule } from "ionic4-rating";
import { ReactiveFormsModule } from "@angular/forms";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IonicRatingModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: "",
        component: HomePage,
      },
    ]),
  ],
  declarations: [
    HomePage,
    AutocompletePage,
    DriverInfoPage,
    TripInfoPage,
    ChatPage,
    RatePage,
    SomeonePage
  ],
  entryComponents: [
    AutocompletePage,
    DriverInfoPage,
    TripInfoPage,
    ChatPage,
    RatePage,
    SomeonePage
  ],
})
export class HomePageModule {}
