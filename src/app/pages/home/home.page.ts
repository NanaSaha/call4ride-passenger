import { Component, OnInit, NgZone ,ChangeDetectorRef} from "@angular/core";
import {
  NavController,
  MenuController,
  ModalController,
  Platform,
  AlertController,
  LoadingController,
  ActionSheetController
} from "@ionic/angular";
import { CallNumber } from "@ionic-native/call-number/ngx";
import {
  LatLng,
  GoogleMaps,
  GoogleMapOptions,
  GoogleMap,
} from "@ionic-native/google-maps";
import * as firebase from "firebase/app";
import { Storage } from "@ionic/storage";
import { Vibration } from "@ionic-native/vibration/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { OneSignal } from "@ionic-native/onesignal/ngx";
import { timer } from "rxjs";
import { interval } from "rxjs";
import { LanguageService } from "src/app/services/language.service";
import { AuthService } from "src/app/services/auth.service";
import { ActivityService } from "src/app/services/activity.service";
import { SettingsService } from "src/app/services/settings.service";
import { NativeMapContainerService } from "src/app/services/native-map-container.service";
import { GeocoderService } from "src/app/services/geocoder.service";
import { DirectionserviceService } from "src/app/services/directionservice.service";
import { ProfileService } from "src/app/services/profile.service";
import { OnesignalService } from "src/app/services/onesignal.service";
import { PopUpService } from "src/app/services/pop-up.service";
import { EventService } from "src/app/services/event.service";
import { AutocompletePage } from "src/app/pages/autocomplete/autocomplete.page";
import { ChatPage } from "src/app/pages/chat/chat.page";
import { HttpClient } from "@angular/common/http";
import { DriverInfoPage } from "../driver-info/driver-info.page";
import { RatePage } from "src/app/pages/rate/rate.page";
import { SomeonePage } from '../../someone/someone.page';


import { TripInfoPage } from "../trip-info/trip-info.page";
import { Router, NavigationExtras } from "@angular/router";
import { Geolocation, PositionError, Geoposition } from "@ionic-native/geolocation/ngx";
import { SocialSharing } from "@ionic-native/social-sharing/ngx";


import { Subject } from "rxjs";
import { map, takeUntil, takeWhile } from "rxjs/operators";
// import { addSeconds, format } from "date-fns";
import Swal from 'sweetalert2';
//import Swal from 'sweetalert2/dist/sweetalert2.js';



declare var google;

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"],
})
export class HomePage implements OnInit {

  countdownDisplay?: string;
  starter$ = new Subject<void>();

  userProfile: any;
  notify_ID = "43cd6829-4651-4039-bbc3-aace7fbe7d72";
  showGps: boolean;
  toggleMore: boolean;
  type: string = "arrow-up";
  cardnumber: string;
  hidelocator: boolean;
  math: any = Math;
  lat: number;
  lng: number;
  picked: boolean;
  currentCar: any;
  startedNavigation: boolean = false;
  booked_for_someone: boolean = false;
  destination_lat: any;
  destination_lng: any;
  bookStarted: boolean;
  returningUser: boolean;
  uid: any;
  hideNews: boolean;
  newPrice: number;
  canCancel: boolean;
  driver_connected: boolean;
  refreshedTimes: any = 0;
  notification: boolean;
  carType: any;
  plate: any;
  name: any;
  seat: any;
  rating: any;
  review: any;
  picture: any;
  location: any;
  canShowBars: boolean = true;
  rideSelected: boolean = false;
  hasPaused: boolean = false
  hasResumed: boolean = false
  public geocoder: any = new google.maps.Geocoder();
  price: number;
  userDestName: any;
  number: any;
  driverLocationName: any;
  pickUpLocation: any;
  connect_change: boolean = true;
  myTimer: any;
  referal: any;
  referalID: any;
  actualPrice: any;
  highPrice: any;
  driverHighestDrivingDistance: any;
  driverFound: boolean = true;
  pickedup: boolean = true;
  droppedoff: boolean = true;
  hasPaid: boolean = true;
  isChecking: boolean = true;
  tracker: any;
  mapTracker: any;
  watchPositionSubscription: Geolocation;
  isDriverEnded: boolean = true;
  canSwoop: boolean = true;
  canEnd: boolean = true;
  destiny: any;
  dest: any = "Your Destination";
  currentID: any;
  rating_positive: any = 0;
  rating_negative: any = 0;
  cartype: any;
  isDone: boolean = true;
  isRoute: boolean = true;
  distance_1: any;
  distance_2: any;
  map2: any;
  tolls: any = 0;
  surcharges: any = 0;
  myData: any = [];
  myData3: any;
  myData2: any;
  hasTimed: boolean = true;
  cancelTimer: any;
  canIncur: boolean = false;
  items: any[];
  priceAfterAddStop: any;
  time: number;
  mileage: number;
  baseFare: any;
  exp: any;
  expo: any;
  up: any;
  down: any;
  myAlert2: any;
  bull: any;
  myAlert3: any;
  interval: any;
  //timeLeft: number = 120;
  timeLeft: number = 60;
  driver_ID: any;
  newterval: any;
  myID: any;
  date1: any;
  date2: string;
  location2: any;
  destination2: any;
  date3: string;
  newtrval: number;
  myTolls: any[] = [];
  drre: any;
  toller: any = [];
  tollName: any[];
  toll_1: any;
  toll_0: number;
  toll_3: any;
  toll_2: any;
  date: string;
  totTOLLS: number;
  allChat: any = [];
  unturned: boolean;
  randNum: any;
  paymentType: any;
  justHide: boolean = true;
  ranD: number;
  driverGlobalPercentSurcharge: any = 0;
  riderGlobalSurcharge: any = 0;
  driverGlobalFlatSurcharge: any = 0;
  riderGlobalPercentSurcharge: any = 0;
  riderGlobalFlatSurcharge: any = 0;
  riderZipcodeFlatSurcharge: any = 0;
  riderZipcodePercentSurcharge: any = 0;
  driverZipcodeFlatSurcharge: any = 0;
  driverZipcodePercentSurcharge: any = 0;
  mySurch: any = [];
  totalFlatFee: void;
  totalRiderSurcharge: any = 0;
  totalDriverSurcharge: any = 0;
  loc1: any[];
  loc2: any[];
  networkOK: any;
  toller3: any;
  toller2: any;
  AllTOLLs: any;
  gunshot: any;
  tollAMT: number;
  currentTOLLS: any;
  currentTOLLS_PRICES: any;
  r: number;
  g: number;
  result3: number = 0;
  check_tracker_loop: any;
  MilkTOlls: any[];
  result4: any;
  result5: any;
  tollTems: any[] = [];
  recentItems: any[];
  toll_1_value: any = 0;
  allSurcharges: any = 0;
  num: any;
  myg: any[] = [];
  canFit: boolean;
  realPrice: any;
  outofstatecharge: number;
  myPos: any;
  calPos: any;
  modal: HTMLIonModalElement;
  items2: any[];
  testVal;
  myAlert: any;
  driverPhone: any;
  detination_words: any;
  emergenc;
  first_name;
  selectedRadioGroup;
  yesorno: any;
  percentage;
  status;
  driver_pos;
  watch;
  driver_arrived;
  mapStyle: string;
  btnStyle: string;
  secs: any = 300;
  countingTime;
  pauseCost: any = 0;
  someoneName: any;
  someonePhoneNumber: any;
  newDestinationStop: any;
  selected_driver_uid: any;



  public setBorderColor: boolean = false;

  public hideDriverFound: boolean = false;

  isComplete: boolean = false;
  defaultMap: boolean = false;
  shortMap: boolean = false;


  test2: boolean = false;
  basefar: any;
  audioPlayer: HTMLAudioElement;
  constructor(
    public share: SocialSharing,
    public storage: Storage,
    public http: HttpClient,
    public lp: LanguageService,
    public authProvider: AuthService,
    public One: OneSignal,
    public act: ActivityService,
    public settings: SettingsService,
    public statusBar: StatusBar,
    public loadingCtrl: LoadingController,
    private vibration: Vibration,
    public alert: AlertController,
    public cMap: NativeMapContainerService,
    private call1: CallNumber,
    public myGcode: GeocoderService,
    public dProvider: DirectionserviceService,
    public platform: Platform,
    public OneSignal: OnesignalService,
    public modalCtrl: ModalController,
    public menu: MenuController,
    public pop: PopUpService,
    public ph: ProfileService,
    public navCtrl: NavController,
    public eventProvider: EventService,
    public router: Router,
    private zone: NgZone,
    public actionSheetController: ActionSheetController,
    private geo: Geolocation,
    private cdr: ChangeDetectorRef
     ) {
    this.platform.ready().then(() => {

      // this.keepScreenAlive();
      // this.backgroundMode.enable();

      this.cMap.loadMap();
      this.WaitForGeolocation();
     
      this.subscribeToOnesignal()

      this.allStorage()

    });

    this.subscribeToOnesignal()
   
    this.getPrice()
 

  }



  // keepScreenAlive() {

  
  //   this.insomnia.keepAwake()
  //     .then(
  //       () => console.log('KEEPING AWAKE success'),
  //       () => console.log('KEEPING AWAKE error')
  //     );
  // }

  completeItem() {
    this.defaultMap = !this.defaultMap;
    this.shortMap = !this.shortMap
    this.storage.set("shortMap", this.shortMap);
    this.storage.set("defaultMap", this.defaultMap);

    this.cdr.detectChanges();


  }



  ionViewDidEnter() {
    this.audioPlayer = new Audio('/assets/sounds/cancelalert.mp3');
    
    this.subscribeToOnesignal()
    this.allStorage()
  }

  allStorage() {

    this.storage.get("cMap.onbar3").then((value) => {
      if (value == null) {

      } else {
        this.cMap.onbar3 = value

      }


    })

    this.storage.get("defaultMap").then((value) => {
      if (value == null) {

      } else {
        this.defaultMap = value

      }


    })



    this.storage.get("driver_connected").then((value) => {
      if (value == null) {

      } else {
        this.driver_connected = value

      }


    })

    this.storage.get("hasPaused").then((value) => {
      if (value == null) {

      } else {
        this.hasPaused = value

      }


    })



    this.storage.get("destination_time").then((value) => {
      if (value == null) {

      } else {
        this.dProvider.destination_time = value

      }

    })
    this.storage.get("driver_arrived").then((value) => {
      if (value == null) {

      } else {
        this.driver_arrived = value

        // if (this.driver_arrived = true && !this.dProvider.destination_time) {
        //   this.startCountdown()
        // }
      }

    })
    this.storage.get("arriving_time").then((value) => {
      if (value == null) {

      } else {
        this.dProvider.arriving_time = value

      }

    })
    this.storage.get("carType").then((value) => {
      if (value == null) {

      } else {
        this.carType = value

      }

    })
    this.storage.get("plate").then((value) => {
      if (value == null) {

      } else {
        this.plate = value

      }

    })
    this.storage.get("name").then((value) => {
      if (value == null) {

      } else {
        this.name = value

      }

    })

    this.storage.get("cMap.lat").then((value) => {
      if (value == null) {

      } else {
        this.cMap.lat = value

      }

    })

    this.storage.get("cMap.lng").then((value) => {
      if (value == null) {

      } else {
        this.cMap.lng = value

      }

    })



    this.storage.get("detination_words").then((value) => {
      if (value == null) {

        console.log("DESTINATION NULL:::=>>", value)

      } else {
        this.detination_words = value

        console.log("DESTINATION VALUE:::=>>", this.detination_words)
      }

    })
    this.storage.get("dProvider.price").then((value) => {
      if (value == null) {

        console.log("dProvider.price:::=>>", value)

      } else {
        this.dProvider.price = value

        console.log("dProvider.price:::=>>", this.dProvider.price)

      }

    })

    this.storage.get("price").then((value) => {
      if (value == null) {
        console.log("dProvider.price:::=>>", value)
      } else {
        console.log("dProvider.price:::=>>", this.dProvider.price)
        this.dProvider.price = value

      }

    })

    this.storage.get("timeToReach").then((value) => {
      if (value == null) {

      } else {
        this.dProvider.timeToReach = value

      }

    })

    this.storage.get("duration").then((value) => {
      if (value == null) {

      } else {
        this.dProvider.duration = value

      }

    })




    this.storage.get("cMap.selected_destination_bar").then((value) => {
      if (value == null) {

      } else {
        this.cMap.selected_destination_bar = value

      }

    })





    this.storage.get("bull").then((value) => {
      if (value == null) {

      } else {
        this.bull = value

      }

    })

    this.storage.get("uid").then((value) => {
      if (value == null) {

      } else {
        this.uid = value

      }

    })

    this.storage.get("driver_number").then((value) => {
      if (value == null) {

      } else {
        this.number = value

      }

    })


    this.storage.get("rideSelected").then((value) => {
      if (value == null) {

      } else {
        this.rideSelected = value

      }

    })

  

    this.storage.get("returningUser").then((value) => {
      if (value == null) {

      } else {
        this.returningUser = value


        if (this.returningUser == true) {
          this.startListeningForResponse()
        }
      }


    })


    this.storage.get("connect_change").then((value) => {
      if (value == null) {

      } else {
        this.connect_change = value

      }

    })

    this.storage.get("StopLocUpdate").then((value) => {
      if (value == null) {
        
      } else {
        this.cMap.StopLocUpdate = value

      }

    })

    this.storage.get("cMap.hasCompleted").then((value) => {
      if (value == null) {

      } else {
        this.cMap.hasCompleted = value

      }

    })

    this.storage.get("cMap.isPickedUp").then((value) => {
      if (value == null) {

      } else {
        this.cMap.isPickedUp = value

      }

    })

    this.storage.get("shortMap").then((value) => {
      if (value == null) {

      } else {
        this.shortMap = value

      }

    })

    this.storage.get("demo").then((value) => {
      if (value == null) {

      } else {
        var demo
        demo = value

      }

    })


    this.storage.get("pickUpLocation").then((value) => {
      if (value == null) {

      } else {
       
        this.pickUpLocation = value

      }

    })




    this.storage.get("myGcode.locationName").then((value) => {
      if (value == null) {

      } else {

        this.myGcode.locationName = value

      }

    })

    this.storage.get("cMap.canShowchoiceTab").then((value) => {
      if (value == null) {

        console.log("canShowchoiceTab NULL ======>>>>>>>", value)
        console.log("canShowchoiceTab NULL 2 ======>>>>>>>", this.cMap.canShowchoiceTab)

      } else {

        this.cMap.canShowchoiceTab = value
        console.log("canShowchoiceTab VALUE ======>>>>>>>", this.cMap.canShowchoiceTab)

      }

    })

    


    this.storage.get("pop.onRequest").then((value) => {
      if (value == null) {

        console.log("POP REQUEST 1::",this.pop.onRequest )

      } else {
       
        this.pop.onRequest = value
        console.log("POP REQUEST 2::",this.pop.onRequest )

      }

    })

    this.storage.get("cMap.hasShown").then((value) => {
      if (value == null) {

      } else {

        this.cMap.hasShown = value

      }

    })

    this.storage.get("cMap.onLocationbarHide").then((value) => {
      if (value == null) {

      } else {

        this.cMap.onLocationbarHide = value

      }
    })


    this.storage.get("pop.hasCleared").then((value) => {

      if (value == null) {

      } else {

        this.pop.hasCleared = value

      }

    })

    this.storage.get("cMap.onbar2").then((value) => {

      if (value == null) {

      } else {

        this.cMap.onbar2 = value

      }

    })

    this.storage.get("picture").then((value) => {

      if (value == null) {

      } else {

        this.picture = value

      }

    }) 
  }

  // ionViewWillLeave() {
  //   this.storage.clear(); 
  // }



  removeStorage() {

    console.log("REMOVING ALL STORAGE:::")
    this.storage.clear();
    

    this.storage.remove("cMap.onbar3")

    this.storage.remove("defaultMap")


    this.storage.remove("priceAfterAddStop");
    this.storage.remove("driver_connected")
    this.storage.remove("hasPaused")

    this.storage.remove("destination_time")
    this.storage.remove("driver_arrived")
    this.storage.remove("arriving_time")
    this.storage.remove("carType")
    this.storage.remove("plate")
    this.storage.remove("name")

    this.storage.remove("cMap.lat")

    this.storage.remove("cMap.lng")

    this.storage.remove("detination_words")
    this.storage.remove("price")

    this.storage.remove("duration")

    this.storage.remove("cMap.selected_destination_bar")

    this.storage.remove("bull")

    this.storage.remove("uid")

    this.storage.remove("driver_number")


    this.storage.remove("rideSelected")



    this.storage.remove("returningUser")


    this.storage.remove("connect_change")

    this.storage.remove("StopLocUpdate")

    this.storage.remove("cMap.hasCompleted")

    this.storage.remove("cMap.isPickedUp")

    this.storage.remove("shortMap")

    this.storage.remove("demo")

    this.storage.remove("pickUpLocation")

 

    this.storage.remove("myGcode.locationName")

    this.storage.remove("cMap.canShowchoiceTab")
    this.storage.remove("pop.onRequest")

    this.storage.remove("cMap.hasShown")

    this.storage.remove("cMap.onLocationbarHide")

    this.storage.remove("pop.hasCleared")

    this.storage.remove("cMap.onbar2")

    this.storage.remove("picture")


  }






  subscribeToOnesignal() {
    if (!this.platform.is("cordova")) {
      this.notify_ID = "43cd6829-4651-4039-bbc3-aace7fbe7d72";
      
    }
    else if (this.platform.is("desktop")) {
      this.notify_ID = "43cd6829-4651-4039-bbc3-aace7fbe7d72";
     
    }

    else if (this.platform.is("mobileweb")) {
      this.notify_ID = "43cd6829-4651-4039-bbc3-aace7fbe7d72";
     
    }

    else {
      this.One.getIds().then((success) => {
        
        this.notify_ID = success.userId;

       
      });
    }
  }

  hidePanel() {
    
    this.hideDriverFound = this.hideDriverFound ? false : true;
    
  }

  toggle() {
    this.menu.enable(true);
    this.menu.open();
  }

  getIDD(id) {
  
  }

  dismiss_noride() {
 
    this.cMap.norideavailable = false
    this.cMap.selected_destination_bar = false;
    this.cMap.hasShown = true;
    this.pop.onRequest = false;

    this.storage.set("pop.onRequest", this.pop.onRequest);
    this.storage.set("cMap.hasShown", this.cMap.hasShown);
    this.storage.set("cMap.selected_destination_bar", this.cMap.selected_destination_bar);

    this.cMap.canShowchoiceTab = false;
    this.storage.set("cMap.canShowchoiceTab", this.cMap.canShowchoiceTab);
    this.storage.set("cMap.norideavailable", this.cMap.norideavailable);
   
    if (this.cMap.watch_to_drop) {
      this.cMap.watch_to_drop.unsubscribe();
    }
    if (this.cMap.watch2) {
      this.cMap.watch2.unsubscribe();
    }
    if (this.cMap.watch_to_pick) {
      this.cMap.watch_to_pick.unsubscribe();
    }

   /// this.removeStorage()
  
  }

  dismiss_widget() {
    this.CloseOnly();
    this.Close();
    this.PerformActionOnCancel();
    if (this.cMap.watch_to_drop) {
      this.cMap.watch_to_drop.unsubscribe();
    }
    if (this.cMap.watch2) {
      this.cMap.watch2.unsubscribe();
    }
    if (this.cMap.watch_to_pick) {
      this.cMap.watch_to_pick.unsubscribe();
    }

    this.removeStorage()
  
  }

  radioGroupChange(event) {
    
    this.selectedRadioGroup = event.detail;
    // document.getElementById(id).style.backgroundColor = "black";
    // document.getElementById(id).style.color = "white";
  }
  checkVal() {

    this.showUser(); 
  
  }
  

  call_phone() {
  

    // let customerPhone = this.emergenc;
    // window.open("tel:" + customerPhone);

    this.call1.callNumber(`${this.emergenc}`, true)
    .then(() => console.log('Phone call initiated'))
    .catch(() => console.error('Error initiating phone call'));
  }

  call_phone_other() {
    // window.open("tel:+233276113371");

    this.call1.callNumber("+233276113371", true)
    .then(() => console.log('Phone call initiated'))
    .catch(() => console.error('Error initiating phone call'));
   
  }

  ngOnInit() {
    this.audioPlayer = new Audio('/assets/sounds/cancelalert.mp3');
    
    this.allStorage()
    this.subscribeToOnesignal()
    this.checkPromoExist();
   
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
       
        this.lat = 5.7186233;
        this.lng = -0.0240626;
     

        this.WaitForGeolocation();
       

        if (this.ph.card != null) {
          var mainStr = this.ph.card || "2345678765445678",
            vis = mainStr.slice(-4),
            countNum = "";

          for (var i = mainStr.length - 4; i > 0; i--) {
            countNum += "*";
          }
          this.cardnumber = countNum + vis;
        }
      }
    });

    //GET USER PROFILE DETAILS
    this.ph.getUserProfile().on("value", (userProfileSnapshot) => {
     
      const phone = userProfileSnapshot.val().phonenumber;
      this.first_name = userProfileSnapshot.val().first_name;
      this.emergenc = userProfileSnapshot.val().emergencyNumber;
     
    });



  }




  remove(): void {
    this.authProvider.logoutUser().then(() => {
      this.navCtrl.navigateRoot("login-entrance");
    });
  }

  toggleMoreSection() {
    
    this.unturned = true;
    this.cMap.selected_destination_bar = true;
    this.canSwoop = false;

    this.storage.set("cMap.selected_destination_bar", this.cMap.selected_destination_bar);

    this.doNow();

  }

  doNow() {
    // this.pop.presentLoader("").then(() => {
    //   this.pop.hideLoader();
    // });
    timer(1000).subscribe(() => {
      // document.getElementById("destination").innerText =
      //   this.lp.translate()[0].dest;
      if (this.platform.is("cordova") && this.platform.is("mobileweb")) {
        this.cMap.map.setClickable(false);
      }
    
      this.cMap.onDestinatiobarHide = true;
      this.cMap.onLocationbarHide = true;
      this.cMap.showDone = false;
      this.hidelocator = true;

      this.storage.set("cMap.onLocationbarHide", this.cMap.onLocationbarHide);

    });
  }

  async showLoadRefresh() {
    const loading = await this.loadingCtrl.create({});
    await loading.present().then(() => {
      let myTimeout = setTimeout(() => {
        clearTimeout(myTimeout);
        loading.dismiss();
      }, 400);
    });
  }

  myVal: any;
  manTime: any;
  seconds: any = 1;
  minutes: any = 0;
  clock_minutes: any = 0;
  trop: number;
  hours: any = 0;

  startCountdown() {
    
    this.manTime = 0;

    this.myVal = interval(1000).subscribe(() => {

      var demo = document.getElementById("demo");
      this.storage.set("demo", demo);

      this.trop = new Date().getHours() - new Date().getHours();

      if (demo) {


        if (this.seconds) {
          this.seconds++;

          if (this.seconds >= 60) {
            this.minutes++;
            this.seconds = 1;
            console.log("Minutes:::", this.minutes);

          }
          if (this.minutes >= 5) {
            this.myVal.unsubscribe();
            console.log("Wait time Over:::", this.minutes);

            this.waitTimeAlert()

          }

          demo.innerHTML = this.minutes + "min : " +
            this.seconds + "secs ";

        }
      }
      this.cdr.detectChanges();
    });
  }



  ReturnHome() {

    this.removeStorage()

    this.cMap.selected_destination_bar = false;
    this.storage.set("cMap.selected_destination_bar", this.cMap.selected_destination_bar);

    if (this.platform.is("cordova") && this.platform.is("mobileweb")) {
      this.cMap.map.setClickable(true);
    }

    this.rideSelected = false;
    this.cMap.showDone = false;
    this.isRoute = true;
    this.cMap.StopLocUpdate = false;
    this.storage.set("StopLocUpdate", this.cMap.StopLocUpdate);
    this.storage.set("rideSelected", this.rideSelected);
    this.dProvider.isClose = true;
    this.allSurcharges = 0;
    this.toll_1_value = 0;
    this.surcharges = 0;
    this.myTolls = [];
    this.mySurch = [];
    this.realPrice = 0;
    this.driverGlobalPercentSurcharge = 0;
    this.riderGlobalSurcharge = 0;
    this.driverGlobalFlatSurcharge = 0;
    this.riderGlobalPercentSurcharge = 0;
    this.riderGlobalFlatSurcharge = 0;
    this.riderZipcodeFlatSurcharge = 0;
    this.riderZipcodePercentSurcharge = 0;
    this.driverZipcodeFlatSurcharge = 0;
    this.driverZipcodePercentSurcharge = 0;
    this.myg = [];
    this.actualPrice = 0;
    this.totalRiderSurcharge = 0;
    this.totalDriverSurcharge = 0;
    this.AllTOLLs = null;
    this.hidelocator = false;
    this.tollTems = [];
    this.canSwoop = true;
    this.cMap.shove = true;
    if (this.platform.is("cordova")) {
      this.cMap.map.clear().then(() => {
       
        this.cMap.yellow_markersArray = [];
        this.cMap.driver_markersArray = [];
        this.cMap.client_markersArray = [];
        this.cMap.flag_markersArray = [];
        this.cMap.car_markersArray = [];
        this.cMap.rider_markersArray = [];
        if (this.cMap.watch_to_drop) {
          this.cMap.watch_to_drop.unsubscribe();
        }
        if (this.cMap.watch2) {
          this.cMap.watch2.unsubscribe();
        }
        if (this.cMap.watch_to_pick) {
          this.cMap.watch_to_pick.unsubscribe();
        }
        
        this.cMap.Restart();

      })


    }

    setTimeout(() => {
      this.StartTracker();
    }, 2000);
    this.dProvider.isDriver = true;
    this.cMap.hasRequested = false;
    this.pop.onRequest = false;
    this.hidelocator = false;
    this.cMap.canShowchoiceTab = false;
    this.cMap.norideavailable = false;
    this.cMap.isLocationChange = false;
    this.cMap.isDestinationChange = false;
    this.myGcode.locationChange = false;
    this.myGcode.destinationChange = false;
    this.eventProvider
      .getUserProfile()
      .child("userInfo")
      .child("ActualPrice")
      .remove();
    
    this.storage.set("pop.onRequest", this.pop.onRequest);
    this.storage.set("cMap.canShowchoiceTab", this.cMap.canShowchoiceTab);
    this.storage.set("cMap.norideavailable", this.cMap.norideavailable);

    this.cdr.detectChanges();
  }

  StartTracker() {
    

    this.watch = this.geo.watchPosition({ enableHighAccuracy: true, timeout: 10000 }).subscribe(position => {
      if ((position as Geoposition).coords != undefined) {
        var geoposition = (position as Geoposition);
       

        this.cMap.Geofiring


        this.lat = geoposition.coords.latitude,
          this.lng = geoposition.coords.longitude;
       
        if (!this.cMap.StopLocUpdate) {
          this.cMap.lat = this.lat;
          this.cMap.lng = this.lng;

          this.cMap.gcode.Reverse_Geocode(this.lat, this.lng, false);
        }


      } else {
        var positionError = (position as PositionError);


      }
    });

  }

  WaitForGeolocation() {
    //A timer to detect if the location has been found.
    let location_tracker_loop = interval(3000).subscribe(() => {
      if (this.cMap.hasShown) {
        location_tracker_loop.unsubscribe();
        this.StartTracker();
        this.showGps = false;
        this.cMap.mapLoadComplete = true;
       
      }
    });
  }

  toggleMoreBtn() {
    //show or hide more button on connecting with driver.
    this.toggleMore = this.toggleMore ? false : true;
    if (this.toggleMore) {
      this.type = "arrow-down";
    } else {
      this.type = "arrow-up";
    }
  }

  GetRoute(location, destination) {
  
    if (!this.pickUpLocation) {
      this.navCtrl.navigateRoot([
        "route" +
        {
          price: this.actualPrice,
          surcharge: this.allSurcharges,
          location: this.myGcode.locationName,
          destination: this.myGcode.destinationSetName,
          toll: this.tollTems,
          osc: this.outofstatecharge,
          baseFare: this.baseFare,
          time: this.time,
          mileage: this.mileage,
        },
      ]);
    }
    else {
      this.navCtrl.navigateRoot([
        "route" +
        {
          price: this.actualPrice,
          surcharge: this.allSurcharges,
          location: this.pickUpLocation,
          destination: this.myGcode.destinationSetName,
          toll: this.tollTems,
          osc: this.outofstatecharge,
          baseFare: this.baseFare,
          time: this.time,
          mileage: this.mileage,
        },
      ]);

    }
  }

  async showSearch() {
    
    this.modal = await this.modalCtrl.create({
      component: AutocompletePage,
      cssClass: "my-custom-class",
      showBackdrop: true,
    });
    await this.modal.present();
    //   this.modalCtrl
    //     .create({
    //       component: AutocompletePage,
    //       cssClass: "my-custom-class",
    //       showBackdrop: true,
    //     })
    //     .then((element) => {
    //       element.present();
    //     });
  }


  showAddressModal(selectedBar) {



    this.showSearch().then(() => {
      this.modal.onDidDismiss().then((data) => {
        //Open the address modal on location bar click to change location

       


        if (selectedBar == 1 && data.data != null) {
          if (!this.startedNavigation) {

            if (!this.pickUpLocation) {
              document.getElementById("location").innerText = data.data;
            }
            
            this.pickUpLocation = data.data;
            this.myGcode.locationName = this.pickUpLocation
            let loction_words = data.data;

            this.storage.set("pickUpLocation", this.pickUpLocation);
            this.storage.set("myGcode.locationName", this.myGcode.locationName);
            

            this.pop.presentSimpleLoader("Searching....");
            this.cMap.StopLocUpdate = true;
            this.storage.set("StopLocUpdate", this.cMap.StopLocUpdate);

            if (!this.pickUpLocation) {
              this.myGcode.geocoder.geocode(
                { address: data.data },
                (results, status) => {
                  if (status === "OK") {

                    var position = results[0].geometry.location;
                    this.cMap.lat = position.lat();
                    this.cMap.lng = position.lng();


                    this.myPos = new LatLng(position.lat(), position.lng());
                   

                  }
                }
              );
            }
            else {
              this.myGcode.geocoder.geocode(
                { address: this.pickUpLocation },
                (results, status) => {
                  if (status === "OK") {

                    var position = results[0].geometry.location;
                    this.cMap.lat = position.lat();
                    this.cMap.lng = position.lng();


                    this.myPos = new LatLng(position.lat(), position.lng());
                  
                  }
                }
              );
            }

          }
        }

        //Open the address modal on destination bar click to change destination
        if (selectedBar == 2 && data.data != null) {

               if (!this.pickUpLocation) {
            this.myPos = new LatLng(this.lat, this.lng);
         
          }
          else {
          
          }
          this.cMap.canShowchoiceTab = true;
          this.storage.set("cMap.canShowchoiceTab", this.cMap.canShowchoiceTab);


          this.cMap.selected_destination_bar = false;
          this.storage.set("cMap.selected_destination_bar", this.cMap.selected_destination_bar);

          document.getElementById("destination").innerText = data.data;
          this.myGcode.destinationSetName = data.data;

          this.detination_words = data.data;
          this.storage.set("detination_words", this.detination_words);


          // this.showUser();


          this.pop.presentSimpleLoader("Searching....");
          ///After data input, check to see if user selected to add a destination or to calculate distance.
          this.myGcode.geocoder.geocode(
            { address: data.data },
            (results, status) => {

              if (status === "OK") {

                var position = results[0].geometry.location;


                this.calPos = new LatLng(position.lat(), position.lng());

               
                this.destination_lat = position.lat();
                this.destination_lng = position.lng();

                //CALCULATE ROUTE
                this.myGcode.destinationSetName = data.data;
                this.dProvider.calcRoute(
                  this.myPos,
                  this.calPos,
                  false,
                  true,
                  this.detination_words,
                  true,
                  this.percentage
                );
               
                this.picked = true;

                let h = interval(2000).subscribe(() => {
                  //If the estimated price has been calculated
                  let loc1 = [this.cMap.lat, this.cMap.lng];
                  let loc2 = [this.destination_lat, this.destination_lng];
                  this.loc1 = loc1;
                  this.loc2 = loc2;

             
                  if (this.dProvider.price != null) {

                    this.pickLocation(loc1, loc2, data.data);
                    h.unsubscribe();
                  
                  } else {
                    this.cMap.norideavailable = true
                    h.unsubscribe();
        
                    this.pop.onRequest = true;
                    this.storage.set("pop.onRequest", this.pop.onRequest);
                    this.storage.set("cMap.norideavailable", this.cMap.norideavailable);
                  
                  }
                });

              }
              else {
                this.cMap.norideavailable = true
                this.storage.set("cMap.norideavailable", this.cMap.norideavailable);

              }
            }, (error) => {

              this.cMap.norideavailable = true
              this.storage.set("cMap.norideavailable", this.cMap.norideavailable);
            }
          );
        }




        /// Endign Addding a stop
        if (selectedBar == 3 && data.data != null) {

     

          this.myGcode.geocoder.geocode(
            { address: this.detination_words },
            (results, status) => {
              if (status === "OK") {

                var position = results[0].geometry.location;
                this.cMap.lat = position.lat();
                this.cMap.lng = position.lng();


                this.myPos = new LatLng(position.lat(), position.lng());
               

              }
            }
          );

  
          this.cMap.canShowchoiceTab = true;
          this.cMap.selected_destination_bar = false;
          this.newDestinationStop = data.data;
          this.myGcode.destinationSetName = data.data;

          this.storage.set("cMap.selected_destination_bar", this.cMap.selected_destination_bar);
          this.storage.set("newDestinationStop", this.newDestinationStop);
          this.storage.set("cMap.canShowchoiceTab", this.cMap.canShowchoiceTab);

          ///After data input, check to see if user selected to add a destination or to calculate distance.
          this.myGcode.geocoder.geocode(
            { address: data.data },
            (results, status) => {

              if (status === "OK") {

                var position = results[0].geometry.location;


                this.calPos = new LatLng(position.lat(), position.lng());

                
                this.destination_lat = position.lat();
                this.destination_lng = position.lng();

                //CALCULATE ROUTE
                this.myGcode.destinationSetName = data.data;
                this.dProvider.calcRouteForAddstop(
                  this.myPos,
                  this.calPos,
                  false,
                  true,
                  this.newDestinationStop,
                  true,
                  this.percentage
                );
                
                this.picked = true;

                let h = interval(2000).subscribe(() => {
                  //If the estimated price has been calculated
                  let loc1 = [this.cMap.lat, this.cMap.lng];
                  let loc2 = [this.destination_lat, this.destination_lng];
                  this.loc1 = loc1;
                  this.loc2 = loc2;

                 

                  if (this.dProvider.new_price_for_add_stop != null) {

                    this.pickLocation(loc1, loc2, data.data);
                    h.unsubscribe();
                   
               

                    this.RequestForRideForAddStop()
                  } else {
                    this.cMap.norideavailable = true
                    h.unsubscribe();

                    this.pop.onRequest = true;
                    this.storage.set("pop.onRequest", this.pop.onRequest);
                    this.storage.set("cMap.norideavailable", this.cMap.norideavailable);
                  }
                });

              }
              else {
                this.cMap.norideavailable = true
                this.storage.set("cMap.norideavailable", this.cMap.norideavailable);

              }
            }, (error) => {

              this.cMap.norideavailable = true
              this.storage.set("cMap.norideavailable", this.cMap.norideavailable);
            }
          );
        }

        /// Endign Addding a stop
      });
    });
  }



  FaceShare() {
    let message =
      "Hi, i am going to " +
      this.loc2 +
      " from " + this.loc1;
    let subject = "My trip"
    let file = "https://www.google.com/maps/search/?api=1&query=" + this.loc2;
    let link = "https://www.google.com/maps/search/?api=1&query=" + this.loc2;
    this.share
      .share(message, subject, file, link)
      .then(() => { })
      .catch(() => { });
  }


  whatsappShare() {
    let message =
      "Hi, i am going to " +
      this.loc2 +
      "from " + this.loc1;
    let link = "https://www.google.com/maps/search/?api=1&query=" + this.loc2;
    this.share
      .shareViaWhatsApp(message, null, link)
      .then(() => {
        
      })
      .catch((err) => {
        
      });
  }


  async showUser() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Who will take this ride?',
      // subHeader: 'If you request a ride for someone else, enter their phone number.',
      buttons: [{
        text: 'Myself',
        role: 'destructive',
        icon: 'person',
        handler: () => {
          this.bookForSelf();
        }
      }, {
        text: 'Someone Else',
        icon: 'add',
        handler: () => {
          this.show_someone_form();
        }
      }]
    });
    await actionSheet.present();
  }



  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Share Trip',
      buttons: [{
        text: 'Share to Whatsapp',
        role: 'destructive',
        icon: 'logo-whatsapp',
        handler: () => {
          this.whatsappShare()
        }
      }, {
        text: 'Share to Others',
        icon: 'share',
        handler: () => {
          this.FaceShare()
        }
      }]
    });
    await actionSheet.present();
  }

  Surcharge(value) {
    // this.realPrice = (this.exp);
    this.actualPrice = this.exp + parseFloat(value);
   
  }

  checkPrice(data, data1, data2, car) {
  

    let currentPrice = this.dProvider.price;
    let g = [];

    this.cMap.canShowchoiceTab = true;
    this.storage.set("cMap.canShowchoiceTab", this.cMap.canShowchoiceTab);
    this.storage.set("dProvider.price",  this.dProvider.price)

    this.cMap.selected_destination_bar = false; 
    this.storage.set("cMap.selected_destination_bar", this.cMap.selected_destination_bar);

    this.pop.removeLoader2();
    
  
    this.cMap.car_notificationIds.forEach((element) => {
      
      this.cartype = element[4];
      this.driverPhone = element[5];
      
    });

    if (this.cMap.car_notificationIds.length == 0) {
      this.cMap.isBooking = false;
      this.Close();
      this.pop.showPimp("No Cars In This Region");
    } else {
      if (!data) {

       
        this.baseFare = this.cMap.car_notificationIds[0][9];
        this.time = this.cMap.car_notificationIds[0][9];
        this.mileage = this.cMap.car_notificationIds[0][9];

        this.exp = parseFloat(
          this.baseFare +
          this.dProvider.miles +
          this.dProvider.duration
        );

        // this.realPrice = parseFloat(this.exp.toFixed(2));
        this.realPrice = this.dProvider.price

        this.r = this.exp;
        this.g = this.exp;

        this.actualPrice =
          this.cMap.car_notificationIds[0][9] +
          this.dProvider.miles +
          this.dProvider.duration

      } else {

       
        this.actualPrice = 0

        for (
          let index = 0;
          index < this.cMap.car_notificationIds.length;
          index++
        ) {
          const element = this.cMap.car_notificationIds[index];
         

          if (element[2] == car[2]) {
            
            this.refreshedTimes = index;

           
          }
        }
      }
    }
  }

  get_state(address) {
    var parts = address.split(",");
    for (var x = parts.length - 1; x >= 0; x--) {
      var item = parts[x].trim();
      item = item.split(" ");
      if (item[0].length == 2) {
        return item[0].toLowerCase();
      }
    }
  }

  checkStateToStateSurch() {


    if (
      this.get_state(this.myGcode.locationName) !=
      this.get_state(this.myGcode.destinationSetName)
    ) {
      
      this.outofstatecharge = 20;
    } else {
      
      this.outofstatecharge = 0;
    }
  }


  pickLocation(loc1, loc2, data) {
   

    let pints = [];
    let cints = [];
    let fints = [];
    let ponts = [];
    let conts = [];
    let fonts = [];
    let pxnts = [];
    let rust = [];

    this.driverGlobalPercentSurcharge = 0;
    this.riderGlobalSurcharge = 0;
    this.driverGlobalFlatSurcharge = 0;
    this.riderGlobalPercentSurcharge = 0;
    this.riderGlobalFlatSurcharge = 0;
    this.riderZipcodeFlatSurcharge = 0;
    this.riderZipcodePercentSurcharge = 0;
    this.driverZipcodeFlatSurcharge = 0;
    this.driverZipcodePercentSurcharge = 0;

    this.mySurch = [];



    if (this.myGcode.destinationSetName != null) {
      this.pop.onRequest = true;
      this.storage.set("pop.onRequest", this.pop.onRequest);

      if (this.platform.is("cordova") && this.platform.is("mobileweb")) {
        this.cMap.map.setClickable(true);
      }


      let apart = (
        google.maps.geometry.spherical.computeDistanceBetween(
          new google.maps.LatLng(loc1[0], loc1[1]),
          new google.maps.LatLng(loc2[0], loc2[1])
        ) / 1000
      ).toFixed(2);

     

      if (parseFloat(apart) <= this.settings.distanceDriving) {
       

        if (this.cMap.car_notificationIds != undefined || this.cMap.car_notificationIds != null) {

          if (this.cMap.car_notificationIds.length != 0) {
            

            this.tollName = [];
            this.toll_1 = [];

            this.checkPrice(null, null, null, null);

            this.cMap.choseCar = false;
            this.cMap.showDone = false;
            this.hidelocator = true;
          
          }
          else {
          
            this.cMap.canShowchoiceTab = false;
            this.cMap.norideavailable = true;

            this.cMap.selected_destination_bar = false; //show menu bar
            this.cMap.choseCar = false;
            this.cMap.showDone = false;
            this.hidelocator = true;

            this.storage.set("cMap.selected_destination_bar", this.cMap.selected_destination_bar);
            this.storage.set("cMap.canShowchoiceTab", this.cMap.canShowchoiceTab);
            this.storage.set("cMap.norideavailable", this.cMap.norideavailable);

          }
        } else {
          
          this.cMap.canShowchoiceTab = false;
          this.cMap.norideavailable = true;

          this.cMap.selected_destination_bar = false; //show menu bar
          this.cMap.choseCar = false;
          this.cMap.showDone = false;
          this.hidelocator = true;

          this.storage.set("cMap.selected_destination_bar ", this.cMap.selected_destination_bar);
          this.storage.set("cMap.canShowchoiceTab", this.cMap.canShowchoiceTab);
          this.storage.set("cMap.norideavailable", this.cMap.norideavailable);

        }
      } else {
        this.pop.showPimp("Destination Too Far.");
        this.ReturnToSelect();
      }
    } else {
      //There is no destination. Tell user to add location
      this.cMap.onDestinatiobarHide = true;
      this.cMap.onLocationbarHide = true;
      this.cMap.showDone = false;

      this.storage.set("cMap.onLocationbarHide", this.cMap.onLocationbarHide);

    }
  }

  checkPayment() {
    if (this.ph.customerID) {
     
    } else {
      this.showPomp();
    }
  }

  async showPomp() {
    const alert = await this.alert.create({
      message: "Add Your Momo",
      subHeader: "For Quicker Checkout.",
      buttons: [
        {
          text: "Use Cash",
          role: "cancel",
          handler: () => { },
        },
        {
          text: "Use Momo",
          role: "cancel",
          handler: () => {
            this.navCtrl.navigateRoot("card");
          },
        },
      ],
      backdropDismiss: false,
    });
    await alert.present();
  }


  async waitTimeAlert() {
    const alert = await this.alert.create({
      message: "Driver is waiting....",
      subHeader: "Hello, Driver has been waiting for 5mins. Kindly call or meet driver",
      buttons: [
        {
          text: "Meet Driver",
          role: "cancel",
          handler: () => {

          },
        },
        {
          text: "Call Driver",
          handler: () => {
            this.callDriver()

          },
        },
      ],
      backdropDismiss: false,
    });
    await alert.present();
  }

  //This is the fuction for estimate btn.
  estimate() {
    this.navCtrl.navigateRoot([
      "estimate",
      {
        lat: this.cMap.lat,
        lng: this.cMap.lng,
      },
    ]);
    // this.platform.backButton.subscribe(() => this.navCtrl.pop());
  }

  popRoutes() {

    if (!this.pickUpLocation) {
      this.GetRoute(this.myGcode.locationName, this.myGcode.destinationSetName);

    }
    else {
      this.GetRoute(this.pickUpLocation, this.myGcode.destinationSetName);
    }

  }

  //goto payment page on cash or card click
  gotoPayment() {
    if (this.ph.card == null) {
      this.navCtrl.navigateRoot("payment");
      this.platform.backButton.subscribe(() => this.navCtrl.pop());
    }
  }



  ReturnToSelect() {

    this.cMap.selected_destination_bar = false;
    this.cMap.showDone = false;
    this.pop.onRequest = false;
    this.hidelocator = false;
    this.cMap.canShowchoiceTab = false;
    this.cMap.norideavailable = false;
    this.dProvider.price = null;
    this.cMap.hasRequested = false;
    this.cMap.hasShow = true;

    this.storage.set("pop.onRequest", this.pop.onRequest);
    this.storage.set("cMap.selected_destination_bar", this.cMap.selected_destination_bar);
    this.storage.set("cMap.canShowchoiceTab", this.cMap.canShowchoiceTab);
    this.storage.set("cMap.norideavailable", this.cMap.norideavailable);
    this.storage.set("dProvider.price",  this.dProvider.price)
  }



  Start() {
    this.RequestForRide();
    this.rideSelected = false;

    this.cMap.canShowchoiceTab = false;
    this.cMap.norideavailable = false;
    this.cMap.hasRequested = true;
    // this.watchPositionSubscription.clearWatch(this.mapTracker);
    this.watch.unsubscribe()
    this.pop.driverEnded = true;
    this.cMap.isBooking = true;

    this.cLearMapActivity();

    this.storage.set("cMap.canShowchoiceTab", this.cMap.canShowchoiceTab);
    this.storage.set("rideSelected", this.rideSelected);
    this.storage.set("cMap.norideavailable", this.cMap.norideavailable);
  }



  cLearMapActivity() {


    if (this.cMap.keyEntered) {
      this.cMap.keyEntered.cancel();
    }
    if (this.cMap.exited) {
      this.cMap.exited.cancel();
    }
    if (this.cMap.moved) {
      this.cMap.moved.cancel();
    }
    this.cMap.map.clear().then(() => {

      this.cMap.yellow_markersArray = [];
      this.cMap.driver_markersArray = [];
      this.cMap.client_markersArray = [];
      this.cMap.flag_markersArray = [];
      this.cMap.car_markersArray = [];
      this.cMap.rider_markersArray = [];


      if (this.watch) {
        this.watch.unsubscribe();
      }
      if (this.cMap.watch_to_drop) {
        this.cMap.watch_to_drop.unsubscribe();
      }
      if (this.cMap.watch2) {
        this.cMap.watch2.unsubscribe();
      }
      if (this.cMap.watch_to_pick) {
        this.cMap.watch_to_pick.unsubscribe();
      }
      // if (this.cMap.watch_restart) {
      //     this.cMap.watch_restart.unsubscribe();
      //     console.log("watch_restart UNSUBSCRIBED")
      // }

    })
  }

  ///The Book Now button onclick=>Create and push the users information to the database.
  RequestForRide() {
    //document.getElementById("bar4").style.display = 'none'
    this.bookStarted = true;
    this.hideFunctions();

    this.returningUser = true;
    this.storage.set("returningUser", this.returningUser);

    this.cMap.hasCompleted = false;
    this.storage.set("cMap.hasCompleted", this.cMap.hasCompleted);

    var image = this.ph.id.photoURL;
    var name = this.first_name;
    var pay = this.ph.paymentType;
    this.pop.calculateBtn = false;

   
    clearInterval(this.newterval);

    if (image == null) {
      if (this.ph.pic == null) {
        image =
          "https://cdn1.iconfinder.com/data/icons/instagram-ui-glyph/48/Sed-10-128.png";
      } else {
        image = this.ph.pic;
      }
    }

    if (name == null) {
      if (this.ph.name != null) {
        name = this.ph.name;
      } else {
        name = "User";
      }
    }

    if (pay == null) {
      pay = 1;
    }

    let ratingText = this.ph.ratingText;
    let ratingValue = this.ph.ratingValue;

    if (ratingText == null && ratingValue == null) {
      ratingText = this.lp.translate()[0].notrate;
      ratingValue = 0;
    }

    this.cMap.isBooking = true;
    if (this.cMap.car_notificationIds != null || this.cMap.car_notificationIds != undefined) {
     

    } else {
      
    }

    if (this.cMap.driverHere) {
      this.Close();
      this.cMap.isBooking = false;
      this.pop.showPimp("No drivers around.");
    }
    else {

      if (this.cMap.car_notificationIds.length <= this.refreshedTimes) {
      
        this.cMap.isBooking = false;
        this.Close();
      }

      else {

        if (!this.pickUpLocation) {
          this.createUserInformation(
            image,
            this.myGcode.locationName,
            pay,
            ratingValue,
            ratingText
          );

        } else {
          this.createUserInformation(
            image,
            this.pickUpLocation,
            pay,
            ratingValue,
            ratingText
          );

        }



      }
    }

    this.startTimer();

    this.startListeningForResponse();
  }





  RequestForRideForAddStop() {

    this.bookStarted = true;
    this.hideFunctions();

    this.returningUser = true;
    this.storage.set("returningUser", this.returningUser);

    this.cMap.hasCompleted = false;
    this.storage.set("cMap.hasCompleted", this.cMap.hasCompleted);

    var image = this.ph.id.photoURL;
    var name = this.first_name;
    var pay = this.ph.paymentType;
    this.pop.calculateBtn = false;

   
    clearInterval(this.newterval);

    if (image == null) {
      if (this.ph.pic == null) {
        image =
          "https://cdn1.iconfinder.com/data/icons/instagram-ui-glyph/48/Sed-10-128.png";
      } else {
        image = this.ph.pic;
      }
    }

    if (pay == null) {
      pay = 1;
    }

    let ratingText = this.ph.ratingText;
    let ratingValue = this.ph.ratingValue;

    if (ratingText == null && ratingValue == null) {
      ratingText = this.lp.translate()[0].notrate;
      ratingValue = 0;
    }

    this.cMap.isBooking = true;


    this.createUserInformationForAddStop(
          
            this.detination_words
      
          );
  }

  async OpenDriveInfo() {
   
    let obj = { info: this.bull };
    this.myAlert2 = await this.modalCtrl.create({
      component: DriverInfoPage,
      componentProps: obj,
    });
    await this.myAlert2.present();
    await this.myAlert2.onDidDismiss().then((data) => {
   
      if (data.data == null || data.data == undefined) {

      } else {
        this.onChange(data);

      }
    });
  }

  async OpenTripInfo() {
   
    let obj = {
      info: this.bull,
      accepted: this.date2,
      arrived: this.date,
      distance: this.dProvider.distance2,
      toll: this.myTolls,
    };
    this.myAlert3 = await this.modalCtrl.create({
      component: TripInfoPage,
      componentProps: obj,
    });
    await this.myAlert3.present();
    await this.myAlert3.onDidDismiss((data) => {
      
      if (data.data == null) {
       
      } else {
        this.onChange(data);
      
      }
    });
  }

  toggleTurn() {
    this.unturned = this.unturned ? false : true;
    if (this.unturned) {
      this.type = "arrow-down";
    } else {
      this.type = "arrow-up";
    }
  }

  createUserInformation(
    picture: any,
    locationName: any,
    payWith: any,
    ratingValue: any,
    ratingText: any
  ) {
    this.platform.backButton.subscribe(() =>
      this.pop.presentToast(this.lp.translate()[0].cantexit)
    );

    ////Here we check if there are cars available, if none we return
    if (this.cMap.car_notificationIds != null || this.cMap.car_notificationIds != undefined) {
      

      //This represents the drivers information for access to the driver node in the database
      let selected_driver =
        this.cMap.car_notificationIds[this.refreshedTimes][2];
      let selected_drivers_key =
        this.cMap.car_notificationIds[this.refreshedTimes][3];

     
      

      this.selected_driver_uid = selected_driver;
      this.pop.uid = selected_driver;
      this.dProvider.id = selected_driver;
      this.uid = selected_driver;
      this.storage.set("uid", this.uid);
      this.storage.set("dProvider.price",  this.dProvider.price)

      let dest = this.myGcode.destinationSetName;
      ////console.log('Started the shit here riht now')
      if (!this.platform.is("cordova")) {
        this.lat = 5.4966964;
        this.lng = 7.5297323;
      } else {
        this.lat = this.cMap.lat;
        this.lng = this.cMap.lng;
      }
      //Get access to the drivers database node, remove the current driver from the node and push, to avoid any other request on the same driver
      this.act
        .getCurrentDriver(selected_drivers_key)
        .remove()
        .then(() => {
          // alert(selected_drivers_key)
          // console.log(name, locationName, lat, selected_driver, selected_drivers_key, 'hereerdvdhjhfssfsfds');

          this.eventProvider
            .getUserProfile()
            .on("value", (userProfileSnapshot) => {
              this.cMap.routeNumber = userProfileSnapshot.val().routeNumber;
             
            });


          this.act
            .getActivityProfile(selected_driver)
            .set({
              Client_username: this.first_name || "unknown",
              Client_Deleted: false,
              Client_location: [this.lat, this.lng],
              Client_locationName: locationName || "Accra",
              Client_paymentForm: payWith || 1,
              Client_picture: picture || "no pic",
              Client_phoneNumber: this.ph.phone || 43543533,
              Client_ID: this.ph.id,
              Client_destinationName: dest,
              Client_price: this.dProvider.price,
              Client_CanChargeCard: false,
              Client_PickedUp: false,
              Client_Dropped: false,
              Client_HasRated: false,
              Client_Rating: ratingValue,
              Client_RatingText: ratingText,
              Client_Positive_rating: this.ph.rating_positive || 0,
              Client_Negative_rating: this.ph.rating_negative || 0,
              Client_hasPaid: false,
              Client_paidCash: false,
              Client_returnState: false,
              Client_ended: false,
              Client_Notif: this.notify_ID,
              Client_Arrived: false,
              Client_pold: this.cMap.routeNumber || 0,
              Client_status: this.ph.status || "Unverified",
              Client_toll: this.myTolls || "null",
              Client_token: this.ph.paymentToken || "0",
              Client_customer_id: this.ph.customerID || "no ID",
              Client_DriverSurharge: this.totalDriverSurcharge || 0,
              Client_Surcharges: this.mySurch || [],
              Client_realPrice: this.dProvider.price || 0,
              Client_OutOfStateCharge: this.outofstatecharge || "null",
              RideAccepted: false
            })
            .then(() => {
             

              this.CreatePushNotification(
                selected_driver, "old_stop"
              );

            });
        });
      // }
    }
  }




  createUserInformationForAddStop(

    locationName: any
  ) {
    this.platform.backButton.subscribe(() =>
      this.pop.presentToast(this.lp.translate()[0].cantexit)
    );

    ////Here we check if there are cars available, if none we return
    if (this.cMap.car_notificationIds != null || this.cMap.car_notificationIds != undefined) {


      this.pop.uid = this.selected_driver_uid,
        this.dProvider.id = this.selected_driver_uid,
        this.uid = this.selected_driver_uid,
      this.storage.set("uid", this.uid);

      let new_dest = this.myGcode.destinationSetName;
     

      if (!this.platform.is("cordova")) {
        this.lat = 5.4966964;
        this.lng = 7.5297323;
      } else {
        this.lat = this.cMap.lat;
        this.lng = this.cMap.lng;
      }   

      this.priceAfterAddStop = this.dProvider.price + this.dProvider.new_price_for_add_stop

      this.storage.set("priceAfterAddStop", this.priceAfterAddStop);
  
    
          this.act
            .getActivityProfile(this.selected_driver_uid)
            .update({
            
              Client_location: [this.lat, this.lng],
              Client_locationName: locationName || "Accra",
              Client_new_destinationName: new_dest,
              Client_price: this.priceAfterAddStop,
              Client_realPrice: this.priceAfterAddStop || 0,
              Client_AddStopPrice: this.priceAfterAddStop || 0
            })
            .then(() => {
             

              this.CreatePushNotification(
                this.selected_driver_uid, "new_stop"
              );

            });
      
    }
  }

  CloseLooper() {
    this.PerformActionOnCancel();
  }


  Close() {
    this.defaultMap = true; //FOr toggling
    this.shortMap = false; //FOr toggling
    this.rideSelected = false;
    this.currentCar = null;
    this.dProvider.destination_time = null
    this.dProvider.price = null
    this.pop.clearAll(this.uid, true);
    this.ReturnHome();
   
    this.cMap.hasCompleted = true;
    this.storage.set("cMap.hasCompleted", this.cMap.hasCompleted);
    this.storage.set("shortMap", this.shortMap);
    this.storage.set("defaultMap", this.defaultMap);
    this.storage.set("destination_time", this.dProvider.destination_time);
    this.storage.set("rideSelected", this.rideSelected);
    this.storage.set("dProvider.price",  this.dProvider.price)
    

    clearInterval(this.interval);
    this.cMap.StopLocUpdate = false;
    this.storage.set("StopLocUpdate", this.cMap.StopLocUpdate);
    if (this.myTimer) {
      this.myTimer.unsubscribe();
    }

    // this.cMap.car_notificationIds = null;
    this.pop.hasCleared = true;
    this.cMap.onbar2 = false;
    this.refreshedTimes = 0;
    
    this.PerformActionOnCancel();

    this.storage.set("pop.hasCleared", this.pop.hasCleared);
    this.storage.set("cMap.onbar2", this.cMap.onbar2);

    this.eventProvider.getChatList(this.uid).off("child_added");

    this.cMap.map.clear().then(() => {
      this.cMap.yellow_markersArray = [];
      this.cMap.driver_markersArray = [];
      this.cMap.client_markersArray = [];
      this.cMap.flag_markersArray = [];
      this.cMap.car_markersArray = [];
      this.cMap.rider_markersArray = [];

      if (this.watch) {
        this.watch.unsubscribe();
      }
      if (this.cMap.watch_to_drop) {
        this.cMap.watch_to_drop.unsubscribe();
      }
      if (this.cMap.watch2) {
        this.cMap.watch2.unsubscribe();
      }
      if (this.cMap.watch_to_pick) {
        this.cMap.watch_to_pick.unsubscribe();
      }
      // if (this.cMap.watch_restart) {
      //     this.cMap.watch_restart.unsubscribe();
      //     console.log("watch_restart UNSUBSCRIBED")
      // }


    })



    this.act.getActiveProfile(this.uid).off("child_changed");
    this.act.getActiveProfile(this.uid).off("child_removed");

    this.removeStorage()
    // this.platform.backButton.subscribe(() => {
    //   navigator["app"].exitApp();
    // });
  }

  CreatePushNotification(id,stop) {
   
  
    let current_id = id[2];
   

    if (this.platform.is("cordova")) {
      if (stop == "old_stop") {
        this.OneSignal.sendPush(id);
      }
      else {
        this.OneSignal.sendPushForAddTrip(id);
      }
      
     
      
    }
    else if (this.platform.is("desktop")) {
      this.pop.presentToast("New Passenger To Pickup. You Have Less");
     
    }
    else if (this.platform.is("mobileweb")) {
      this.pop.presentToast("New Passenger To Pickup. You Have Less");
     

    }

  

    this.myTimer = interval(1000000).subscribe(() => {
      if (!this.driver_connected) {
        //alert("No Driver Found Yet");
        this.refreshedTimes++;
        this.eventProvider.getChatList(this.uid).off("child_added");
        this.myTimer.unsubscribe();
        this.act.getActiveProfile(this.uid).off("child_changed");
        this.act.getActiveProfile(this.uid).off("child_removed");
        this.act.getActiveProfile(this.uid).off("value");
        this.pop.driverEnded = false;
        this.act
          .getActivityProfile(id)
          .remove()
          .then(() => {
            this.RequestForRide();

            // alert("Trying again, no driver found");
          });
      } else {
        this.cMap.marker = null;
        this.myTimer.unsubscribe();
        //  alert("Driver Found");
        this.refreshedTimes = -1;
        this.cMap.isBooking = true;
        this.pop.driverEnded = true;
        this.CloseLooper();
      }
    });

    this.cMap.ShowDestination(this.loc1, this.loc2);
    this.canCancel = true;
  }

  //call the driver now
  callDriver() {
   
    // window.open("tel:" +
    //   this.number);

    console.log('Phone c<><<><><> ISSS', this.number)

      this.call1.callNumber(`${this.number}`, true)
      .then(() => console.log('Phone call initiated'))
      .catch(() => console.error('Error initiating phone call'));
  }
  //Send a message to the driver

  async Send() {
    const modal = await this.modalCtrl.create({
      component: ChatPage,
      componentProps: { id: this.uid },
    });
    await modal.present();
    modal.onDidDismiss().then(() => {
      this.notification = false;
    });
  }



  pausingCountdown() {
  
    this.manTime = 0;

    this.myVal = interval(1000).subscribe(() => {
      var pause_time = document.getElementById("pause_time");

      if (pause_time) {
        if (this.seconds) {
          this.seconds++;

          if (this.seconds >= 60) {
            this.minutes++;
            this.clock_minutes++;
            this.seconds = 1;
            console.log("Minutes:::", this.minutes);

          }
          if (this.minutes >= 1) {
            this.pauseCost = this.pauseCost + 0.20
            console.log("Pause Cost iss:::", this.pauseCost);
            this.minutes = 0
           

          }

          pause_time.innerHTML = this.clock_minutes + "min : " +
            this.seconds + "secs ";

        }
      }
    });
  }

  startListeningForResponse() {
   

    this.platform.backButton.subscribe(() =>
      this.pop.presentToast(this.lp.translate()[0].cantexit)
    );

    if (this.returningUser) {
     
      this.act
        .getActiveProfile(this.uid)
        .on("child_added", (customerSnapshot) => {
        

          //  this.act.getActiveProfile(this.uid).once('value', customerSnapshot => {
          let activity = customerSnapshot.val();
          this.driver_ID = activity.Driver_ID;
        
          if (activity.Driver_number != null) {
          
            this.cMap.D_lat = customerSnapshot.val().Driver_location[0];
            this.cMap.D_lng = customerSnapshot.val().Driver_location[1];

            this.allChat = customerSnapshot.val().Chat;

            this.number = customerSnapshot.val().Driver_number;
            this.storage.set("driver_number", this.number);

            console.log('Phone c<><<><><> ISSS', this.number)


            if (this.myTimer) {
              this.myTimer.unsubscribe();
            }

            this.driver_connected = true;
            this.storage.set("driver_connected", this.driver_connected);
            this.PerformActionOnAdd(activity);
            this.bull = activity;
            this.storage.set("bull", this.bull);
          }

          this.cdr.detectChanges();
         
        });
    } else {
      
    }

    this.eventProvider.getChatList(this.uid).on("child_added", (snapshot) => {
      this.eventProvider.getChatList(this.uid).on("child_added", () => {
        if (snapshot.val().Driver_Message || snapshot.val().Driver_Audio) {
          this.notification = true;
          this.pop.presentToast("New Message From Driver");
          this.vibration.vibrate(3000);
          this.audioPlayer.play();
          this.eventProvider.getChatList(this.uid).off("child_added");
        }
      });
    });

    if (this.cMap.isWalk)
      this.act.getActivityProfile(this.uid).update({
        Client_pold: true,
      });

    this.act
      .getActiveProfile(this.uid)
      .once("child_changed", (customerSnapshot) => {
        let activity = customerSnapshot.val();

        this.driver_arrived = activity.Driver_Arrived
        this.storage.set("driver_arrived", this.driver_arrived);

        if (this.driver_arrived = true && !this.dProvider.destination_time) {
          this.startCountdown()
        }
        this.act.getActiveProfile(this.uid).off("value");
        this.cdr.detectChanges();
      })

    this.act
      .getActiveProfile(this.uid)
      .on("child_changed", (customerSnapshot) => {
        
        let activity = customerSnapshot.val();
        this.driver_ID = activity.Driver_ID;

     
        this.driver_arrived = activity.Driver_Arrived
        this.storage.set("driver_arrived", this.driver_arrived);
    


        if (activity.PauseTrip == true) {

          if (this.hasPaused == false) {

            this.hasPaused = true
            this.pausingCountdown()
            this.alerting("Trip Paused", "Driver has paused the ongoing trip\nPlease note there will be additional cost charged per minute whiles the trip is paused ", "warning")
            this.storage.set("hasPaused", this.hasPaused);

          }
          else {
            
          }

        }

        else if (activity.PauseTrip == false) {
          this.hasPaused = false
          this.storage.set("hasPaused", this.hasPaused);

          if (this.hasResumed == false) {
            this.hasResumed = true

            this.alerting("Trip Resumed", "Driver has resumed the trip \nEnjoy the ride", "success")

            this.myVal.unsubscribe();
            this.storage.set("hasPaused", this.hasPaused);
          }
          else {
            
          }
        }



        if (activity.Driver_location != null) {

          this.cMap.D_lat = customerSnapshot.val().Driver_location[0];
          this.cMap.D_lng = customerSnapshot.val().Driver_location[1];

          this.allChat = customerSnapshot.val().Chat;

          this.number = customerSnapshot.val().Driver_number;
          this.storage.set("driver_number", this.number);


          if (this.myTimer) this.myTimer.unsubscribe();
          this.driver_connected = true;
          this.storage.set("driver_connected", this.driver_connected);

          this.PerformActionOnAdd(activity);
          this.bull = activity;
        }

        this.act.getActiveProfile(this.uid).off("value");
        this.cdr.detectChanges();
     
      });

    this.act.getActiveProfile(this.uid).on("child_removed", () => {

        this.cMap.isPickedUp = false;
        this.cMap.hasCompleted = true;

        this.storage.set("cMap.isPickedUp", this.cMap.isPickedUp);
        this.storage.set("cMap.hasCompleted", this.cMap.hasCompleted);

        if (this.pop.driverEnded) {
          this.pop.presentToast("Driver has ended the trip.Search for another driver...");

          this.pop.hasCleared = true;
          this.storage.set("pop.hasCleared", this.pop.hasCleared);


          this.CloseOnly();
          
          this.PerformActionOnCancel();
          // this.RequestForRide();
          // this.Start()

          this.canEnd = false;
          this.cdr.detectChanges();
        } else {
          // this.pop.showPimp("Ride Cancelled...");
          this.onChange("closed")
         
        }

        this.act.getActiveProfile(this.uid).off("value");
        this.cdr.detectChanges();
      
    });
  }

  alerting(title, msg, icon) {
    
    Swal.fire({
      title: title,
      text: msg,
      icon: icon,
      heightAuto: false
    })

  }



  PerformActionOnCancel() {
  
    this.removeStorage()
    this.dProvider.destination_time = null;

    this.dProvider.new_price_for_add_stop = null;
    this.newDestinationStop = null;
    this.storage.set("newDestinationStop", this.newDestinationStop);

    this.hasPaused = false;
    this.rideSelected = false;
    this.startedNavigation = false;
    this.driver_connected = false;
    this.driver_arrived = false
    this.storage.set("driver_connected", this.driver_connected);
    this.storage.set("driver_arrived", this.driver_arrived);
    this.storage.set("rideSelected", this.rideSelected);
    this.surcharges = 0;
    this.canCancel = false;
    this.canIncur = false;
    this.isDone = true;
    clearInterval(this.interval);
    this.toll_1 = [];
    this.myTolls = [];
    this.toller = 0;
    this.eventProvider
      .getUserProfile()
      .child("userInfo")
      .child("ActualPrice")
      .remove();
    this.cMap.routeNumber = 0;
    //why clear drivers? Commenting it out for now to test
    //this.cMap.car_notificationIds.length = 0;
    this.cMap.norideavailable = false;
    this.storage.set("cMap.norideavailable", this.cMap.norideavailable);


    this.eventProvider.getUserProfile().update({
      routeNumber: 0,
    });
    this.cMap.isWalk = false;
    //  this.ph.getUserAsClientInfo().child(this.uid).child('client').off('child_changed')
    this.dProvider.isDriver = false;
    this.isChecking = false;

    this.cMap.map.clear().then(() => {

      this.cMap.yellow_markersArray = [];
      this.cMap.driver_markersArray = [];
      this.cMap.client_markersArray = [];
      this.cMap.flag_markersArray = [];
      this.cMap.car_markersArray = [];
      this.cMap.rider_markersArray = [];

      if (this.watch) {
        this.watch.unsubscribe();
      }
      if (this.cMap.watch_to_drop) {
        this.cMap.watch_to_drop.unsubscribe();
      }
      if (this.cMap.watch2) {
        this.cMap.watch2.unsubscribe();
      }
      if (this.cMap.watch_to_pick) {
        this.cMap.watch_to_pick.unsubscribe();
      }
      // if (this.cMap.watch_restart) {
      //     this.cMap.watch_restart.unsubscribe();
      //     console.log("watch_restart UNSUBSCRIBED")
      // }


    })
    if (this.platform.is("cordova")) {
      

      this.BeginTracker();
    } else {
      
      this.cMap.Restart();
      if (this.platform.is("cordova") && this.platform.is("mobileweb")) {
        this.cMap.map.setClickable(true);
      }
    }

    //this.cMap.element = this.mapComponent
    this.cMap.hasRequested = false;
    this.cMap.onbar3 = false;
    this.storage.set("cMap.onbar3", this.cMap.onbar3);
    this.cMap.toggleBtn = false;
    this.cMap.onPointerHide = false;
    this.cMap.isBooking = false;
    // this.cMap.onDestinatiobarHide = false;
    this.pop.allowed = true;
    //this.cMap.canMess = false;
    this.pop.canDismiss = true;
    this.hasTimed = true;

    this.cMap.isDestination = false;
    this.cMap.selected_destination_bar = false;
    this.cMap.isLocationChange = false;
    this.cMap.isDestinationChange = false;
    this.dProvider.price = null;
    this.droppedoff = true;
    this.pickedup = true;
    this.hasPaid = true;
    this.driverFound = true;
    this.cMap.showDone = false;
    this.pop.onRequest = false;
    this.canShowBars = true;
    this.toggleMore = true;
    this.cMap.isNavigate = false;

    this.connect_change = true;
    this.storage.set("connect_change", this.connect_change);
    this.storage.set("cMap.selected_destination_bar", this.cMap.selected_destination_bar);
    this.storage.set("dProvider.price",  this.dProvider.price)
    
    this.hidelocator = false;

    this.bookStarted = false;

    this.storage.set("pop.onRequest", this.pop.onRequest);

    this.cdr.detectChanges();
  }


  BeginTracker() {
   
    this.cMap.Restart();

    if (this.platform.is("cordova") && this.platform.is("mobileweb")) {
      this.cMap.map.setClickable(true);
    }
  }



  PerformActionOnAdd(activity) {
   
    if (this.connect_change) {

      this.Onconnect(activity);

      this.connect_change = false;
      this.storage.set("connect_change", this.connect_change);

    }

    this.cMap.driver_lat = activity.Driver_location[0];
    this.cMap.driver_lng = activity.Driver_location[1];


    this.driver_pos = new google.maps.LatLng(
      this.cMap.driver_lat,
      this.cMap.driver_lng
    );



    this.destiny = activity.Client_destinationName;


    if (activity.Driver_location[0] != null && !activity.Client_PickedUp) {

      let latlng = {
        lat: parseFloat(activity.Driver_location[0]),
        lng: parseFloat(activity.Driver_location[1]),
      };
      this.geocoder.geocode({ location: latlng }, (results, status) => {
        if (status === "OK") {
          if (results[0]) {


            let driverPos = new LatLng(activity.Driver_location[0], activity.Driver_location[1]);

            let userPos = new LatLng(activity.Client_location[0], activity.Client_location[1]);

           
            this.dProvider.calcRouteAriving(
              userPos,
              driverPos,
              true,
              false,
              results[0].formatted_address,
              false,
              this.percentage
            );

            this.hidelocator = true;
            this.driverFound = false;
          }
        }
      });
    }


    if (activity.Driver_location[0] != null && activity.Client_PickedUp) {
      let latlng = {
        lat: parseFloat(this.destination_lat),
        lng: parseFloat(this.destination_lng),
      };
      this.geocoder.geocode({ location: latlng }, (results, status) => {
        if (status === "OK") {
          if (results[0]) {
            let flagPos = new google.maps.LatLng(
              this.destination_lat,
              this.destination_lng
            );

            let userPos = new LatLng(activity.Client_location[0], activity.Client_location[1]);

            
            this.dProvider.calcDestRoute(
              true,
              userPos,
              flagPos,
              results[0].formatted_address,
              false,
              false,
              this.percentage
            );
          }
        }
      });
    }

    if (activity.Client_PickedUp && this.pickedup) {

      this.cMap.isDestination = true;
      // this.cancelTimer.unsubscribe();
      this.cMap.ClearDetection = true;
      this.canShowBars = false;
      this.toggleMore = false;

      let mydate = new Date();

      this.pickedup = false;
      var currentdate = new Date();
      var datetime =
        this.paddNumber(currentdate.getMonth() + 1, "00") +
        "-" +
        this.paddNumber(currentdate.getDate(), "00") +
        "-" +
        currentdate.getFullYear() +
        " @ " +
        this.paddNumber((currentdate.getHours() + 24) % 12 || 12, "00") +
        ":" +
        ("0" + currentdate.getMinutes()).slice(-2) +
        " " +
        (currentdate.getHours() > 11 ? "PM" : "AM");

      this.date2 = datetime;

      this.cMap.map.getMyLocation().then((location) => {

        this.myGcode.geocoder.geocode(
          { location: location.latLng },
          (results, status) => {
            if (status === "OK") {
              if (results[0]) {
               
                let dfo = interval(500).subscribe(() => {

                  if (this.dProvider.duration > 0) {
                    
                    let r = new Date(
                      mydate.getMinutes() - this.dProvider.duration
                    );

                    var now = new Date();
                    now.setMinutes(now.getMinutes() - this.dProvider.duration); // timestamp
                    now = new Date(now); // Date object
                   

                    var gh =
                      this.paddNumber(now.getMonth() + 1, "00") +
                      "-" +
                      this.paddNumber(now.getDate(), "00") +
                      "-" +
                      now.getFullYear() +
                      " @ " +
                      this.paddNumber((now.getHours() + 24) % 12 || 12, "00") +
                      ":" +
                      ("0" + now.getMinutes()).slice(-2) +
                      " " +
                      (now.getHours() > 11 ? "PM" : "AM");
                    this.date1 = gh;
                    dfo.unsubscribe();
                  } else {
                    let now = new Date(); // Date object

                    var gh =
                      this.paddNumber(now.getMonth(), "00") +
                      "-" +
                      this.paddNumber(now.getDate(), "00") +
                      "-" +
                      now.getFullYear() +
                      " @ " +
                      this.paddNumber((now.getHours() + 24) % 12 || 12, "00") +
                      ":" +
                      ("0" + now.getMinutes()).slice(-2) +
                      " " +
                      (now.getHours() > 11 ? "PM" : "AM");
                    this.date1 = gh;
                  }
                });

                this.location2 = results[0].formatted_address;
              }
            }
          }
        );
      });

      this.cMap.toggleBtn = false;

      let lat;
      let lng;
      this.type = "arrow-up";

      this.myGcode.Reverse_Geocode(this.cMap.lat, this.cMap.lng, false);

      this.pop.presentSimpleLoader("Trip Started....");


      if (this.platform.is("cordova")) {
        this.OneSignal.sendArrived(this.notify_ID);
      }
      else if (this.platform.is("desktop")) {
        this.pop.presentToast("Driver Has Started Trip");
       
      }
      else if (this.platform.is("mobileweb")) {
        this.pop.presentToast("Driver Has Started Trip");
       

      }

   
      this.cMap.isDropped = true;
      this.cMap.isPickedUp = false;

      this.storage.set("cMap.isPickedUp", this.cMap.isPickedUp);

      

      let setDetin = interval(500).subscribe(() => {

        this.geocoder.geocode(
          { address: activity.Client_destinationName },
          (results, status) => {
            

            if (status == "OK") {

              setDetin.unsubscribe()
              var position = results[0].geometry.location;
              lat = position.lat();
              lng = position.lng();

              if (this.platform.is("cordova")) {
                //console.log('destination');

                //document.getElementById("header").innerHTML = 'Estimated Arrival ' + this.dProvider.time2;
                let urPos = new google.maps.LatLng(lat, lng);
                //this.pop.presentLoader('')
                let uLOC = new google.maps.LatLng(this.cMap.lat, this.cMap.lng);

                this.driverFound = false;


                this.pop.SmartLoader("");
                this.cMap.setMarkersDestination(lat, lng, this.uid);


                this.pop.presentSimpleLoader("Ride Started....");

                if (this.watch) {
                  this.watch.unsubscribe();
                }

                if (this.cMap.watch2) {
                  this.cMap.watch2.unsubscribe();
                }
                if (this.cMap.watch_to_pick) {
                  this.cMap.watch_to_pick.unsubscribe();
                }

                this.dProvider.calcDestRoute(
                  this.name,
                  uLOC,
                  urPos,
                  activity.Client_destinationName,
                  this.uid,
                  true,
                  this.percentage
                );
              } else {
                if (this.watch) {
                  this.watch.unsubscribe();
                }

                if (this.cMap.watch2) {
                  this.cMap.watch2.unsubscribe();
                }
                if (this.cMap.watch_to_pick) {
                  this.cMap.watch_to_pick.unsubscribe();
                }

                this.cMap.setMarkersDestination(lat, lng, this.uid);
                this.pop.presentSimpleLoader("Ride Ongoing....");

                let urPos = new google.maps.LatLng(lat, lng);
               
                let uLOC = new google.maps.LatLng(this.cMap.lat, this.cMap.lng);
                this.dProvider.calcDestRoute(
                  this.name,
                  uLOC,
                  urPos,
                  activity.Client_destinationName,
                  this.uid,
                  true,
                  this.percentage
                );

              }

            } else {
              this.pop.presentSimpleLoader("eror getting location....");


            }


          }
        );

      });
    }

    if (activity.Client_Dropped && this.droppedoff) {


      this.cMap.hasCompleted = true;
      this.storage.set("cMap.hasCompleted", this.cMap.hasCompleted);

      this.pop.presentSimpleLoader(this.lp.translate()[0].waiting);
      this.ph.uid = this.uid;

     


      if (this.platform.is("cordova")) {
        this.OneSignal.sendEnd(this.notify_ID);
      }
      else if (this.platform.is("desktop")) {
        this.pop.presentToast("Your Trip Has Ended.");

      }
      else if (this.platform.is("mobileweb")) {
        this.pop.presentToast("Your Trip Has Ended.");


      }

      this.Charged(activity.Client_realPrice);

      this.droppedoff = false;

      let dfo = interval(500).subscribe(() => {
        if (this.dProvider.duration2) {
          var now = new Date();
          now.setMinutes(now.getMinutes() + this.dProvider.duration2); // timestamp
          now = new Date(now); // Date object
          

          var gh =
            this.paddNumber(currentdate.getMonth(), "00") +
            "-" +
            this.paddNumber(currentdate.getDate(), "00") +
            "-" +
            currentdate.getFullYear() +
            " @ " +
            this.paddNumber((currentdate.getHours() + 24) % 12 || 12, "00") +
            ":" +
            ("0" + currentdate.getMinutes()).slice(-2) +
            " " +
            (currentdate.getHours() > 11 ? "PM" : "AM");

          this.date = gh;
          dfo.unsubscribe();
        }
      });

      this.cMap.map.getMyLocation().then((location) => {
        this.geocoder.geocode(
          { location: location.latLng },
          (results, status) => {
            if (status === "OK") {
              if (results[0]) {


                this.destination2 = results[0].formatted_address;
                if (!this.pickUpLocation) {
                  this.ph.Completed(
                    activity.Chat || "",
                    this.myID,
                    this.name,
                    this.date,
                    this.myGcode.locationName,
                    destiny,
                    this.price,
                    this.totTOLLS || 0,
                    this.date1 || "",
                    this.date2 || "",
                    this.date3 || "",
                    this.location2 || "",
                    this.destination2 || "",
                    this.mySurch,
                    this.dProvider.price,
                    this.outofstatecharge || "",
                    this.pauseCost || 0,
                  );
                } else {
                  this.ph.Completed(
                    activity.Chat || "",
                    this.myID,
                    this.name,
                    this.date,
                    this.pickUpLocation,
                    destiny,
                    this.price,
                    this.totTOLLS || 0,
                    this.date1 || "",
                    this.date2 || "",
                    this.date3 || "",
                    this.location2 || "",
                    this.destination2 || "",
                    this.mySurch,
                    this.dProvider.price,
                    this.outofstatecharge || "",
                    this.pauseCost || 0,
                  );
                }


              }
            }
          }
        );
      });

      let destiny;

      destiny = activity.Client_destinationName;

      var currentdate = new Date();
      var datetime =
        this.paddNumber(currentdate.getMonth() + 1, "00") +
        "-" +
        this.paddNumber(currentdate.getDate(), "00") +
        "-" +
        currentdate.getFullYear() +
        " @ " +
        this.paddNumber((currentdate.getHours() + 24) % 12 || 12, "00") +
        ":" +
        ("0" + currentdate.getMinutes()).slice(-2) +
        " " +
        (currentdate.getHours() > 11 ? "PM" : "AM");

      this.date3 = datetime;
    }

    if (
      (activity.Client_hasPaid && this.hasPaid) ||
      (activity.Client_paidCash && this.hasPaid)
    ) {
    
      if (this.myTimer) this.myTimer.unsubscribe();

      this.act.getActiveProfile(this.uid).off("child_changed");
      this.act.getActiveProfile(this.uid).off("child_added");
      this.act.getActiveProfile(this.uid).off("child_removed");
      this.act.getActiveProfile(this.uid).off("value");
      this.ph.driverID = activity.Driver_ID;

      var datetime =
        this.paddNumber(currentdate.getMonth() + 1, "00") +
        "-" +
        this.paddNumber(currentdate.getDate(), "00") +
        "-" +
        currentdate.getFullYear() +
        " @ " +
        this.paddNumber((currentdate.getHours() + 24) % 12 || 12, "00") +
        ":" +
        ("0" + currentdate.getMinutes()).slice(-2) +
        " " +
        (currentdate.getHours() > 11 ? "PM" : "AM");
      let destiny;

      destiny = activity.Client_destinationName;

      this.hasPaid = false;

      if (this.isDone)


        if (!this.pickUpLocation) {
          this.ph
            .createHistory(
              this.name,
              this.price,
              datetime,
              this.myGcode.locationName,
              destiny,
              this.price,
              this.pauseCost || 0,
            )
            .then((id) => {

              this.isDone = false;
                 this.show_ratings(activity);


              if (this.canEnd) {
                this.CloseOnly();
                this.Close();

                this.PerformActionOnCancel();

                this.cMap.onbar3 = false;
                this.storage.set("cMap.onbar3", this.cMap.onbar3);


                this.canEnd = false;
              }
            });

        }
        else {
          this.ph
            .createHistory(
              this.name,
              this.price,
              datetime,
              this.pickUpLocation,
              destiny,
              this.price,
              this.pauseCost || 0,
            )
            .then((id) => {

              this.isDone = false;
              this.show_ratings(activity);


              if (this.canEnd) {
                this.CloseOnly();
                this.Close();

                this.PerformActionOnCancel();

                this.cMap.onbar3 = false;
                this.storage.set("cMap.onbar3", this.cMap.onbar3);


                this.canEnd = false;
              }
            });

        }
    }

    this.cdr.detectChanges();
  }


  CloseOnly() {
    this.defaultMap = true; //FOr toggling
    this.shortMap = false; //FOr toggling
    this.rideSelected = false;
    this.pop.hasCleared = true;
    this.cMap.onbar2 = false;
    this.driver_arrived = false
    this.refreshedTimes = 0;

    this.storage.set("shortMap", this.shortMap);
    this.storage.set("defaultMap", this.defaultMap);
    this.storage.set("driver_arrived", this.driver_arrived);
    this.storage.set("rideSelected", this.rideSelected);
    this.storage.set("pop.hasCleared", this.pop.hasCleared);
    this.storage.set("cMap.onbar2", this.cMap.onbar2);
    // this.platform.backButton.subscribe(() => {
    //   navigator["app"].exitApp();
    // });

    this.removeStorage()
  }


  async bookForSelf() {
    if (this.cMap.norideavailable == true) {
      
      this.pop.showPimp("Sorry, No driver available in your location. Please try again");

    }
    else {

      
      this.rideSelected = true;
      this.pop.hasCleared = false;
      this.cMap.onbar2 = false;

      this.storage.set("rideSelected", this.rideSelected);
      this.storage.set("pop.hasCleared", this.pop.hasCleared);
      this.storage.set("cMap.onbar2", this.cMap.onbar2);

    }
   }

  async show_someone_form() {
    this.myAlert = await this.modalCtrl.create({
      component: SomeonePage
    });
    await this.myAlert.present();
    await this.myAlert.onDidDismiss().then((data) => {

      
      if (data.data == 1) {
        

       }
      else {
        if (this.cMap.norideavailable == true) {
         
          this.pop.showPimp("Sorry, No driver available in your location. Please try again");
          
        }
        else {
         
          this.first_name = data.data.first_name
          this.ph.phone = data.data.number

          this.booked_for_someone = true

       
          this.rideSelected = true;
        this.pop.hasCleared = false;
          this.cMap.onbar2 = false;
          
          this.storage.set("rideSelected", this.rideSelected);
          this.storage.set("pop.hasCleared", this.pop.hasCleared);
          this.storage.set("cMap.onbar2", this.cMap.onbar2);
          
        }
        
      }

    });
    this.myAlert.present();
  }

  async show_ratings(activity) {
    
    this.removeStorage()

    let obj = {
      eventId: activity.Driver_ID,
      positive_Rating: activity.Driver_Positive_rating,
      negative_Rating: activity.Driver_Negative_rating,
      name: activity.Driver_name,
      time: this.dProvider.time,
      distance: this.dProvider.distance2,
      price: activity.Client_realPrice,
      m_ID: this.ph.historyID.key || "noID",
      custom_ID: this.ph.customerID || "noIDPresent",
      waitTimeCost: this.pauseCost || 0
    };
    this.myAlert = await this.modalCtrl.create({
      component: RatePage,
      componentProps: obj,
    });
    await this.myAlert.present();

    if (this.platform.is("cordova")) {
      this.vibration.vibrate(3000);
      this.audioPlayer.play();
    }
    await this.myAlert.onDidDismiss().then((data) => {
      this.dProvider.new_price_for_add_stop = ""
      this.newDestinationStop = ""
      this.storage.set("newDestinationStop", this.newDestinationStop);

      
      if (data.data == 1) {
        this.CloseOnly();
        this.Close();
        this.PerformActionOnCancel();
      
      } else {
        this.CloseOnly();
        this.Close();
        this.PerformActionOnCancel();
       
      }
    });
    this.myAlert.present();
  }

  ToggleChange_1() {


    if (!this.pickUpLocation) {
    
      document.getElementById("location").innerText = this.myGcode.locationName
    }
    else {
      this.pickUpLocation = ''
      this.storage.set("pickUpLocation", this.pickUpLocation);
      document.getElementById("location").innerText = this.myGcode.locationName
    }

  }

  paddNumber(number, paddingValue) {
    return String(paddingValue + number).slice(-paddingValue.length);
  }

  ToggleChange_2() {

  }

  DriverFound(
    location,
    plate,
    carType,
    name,
    seat,
    rating,
    picture,
    client_lat,
    client_lng,
    number
  ) {
    this.location = location;
    this.plate = plate;
    this.carType = carType;

    this.seat = seat;
    this.up = rating;
    this.number = number;
    this.picture = picture;
    this.name = name;
    this.cMap.lat = client_lat;
    this.cMap.lng = client_lng;

    this.storage.set("carType", this.carType);
    this.storage.set("plate", this.plate);
    this.storage.set("name", this.name);
    this.storage.set("driver_number", this.number);
    this.storage.set("picture", this.picture);

    console.log('Phone c<><<><><> ISSS', this.number)
 

    if (location) {
      this.cMap.D_lat = location[0];
      this.cMap.D_lng = location[1];
    } else {
      if (this.bookStarted)
        this.bookStarted = false;
      this.pop.presentToast("Couldnt find a driver at this time.");
    }

    //alert("Driver has been Found");

    if (this.platform.is("cordova")) {

      this.cMap.map.clear().then(() => {
        this.cMap.yellow_markersArray = [];
        this.cMap.driver_markersArray = [];
        this.cMap.client_markersArray = [];
        this.cMap.flag_markersArray = [];
        this.cMap.car_markersArray = [];
        this.cMap.rider_markersArray = [];
        if (this.watch) {
          this.watch.unsubscribe();
        }

        if (this.cMap.watch2) {
          this.cMap.watch2.unsubscribe();
        }
        if (this.cMap.watch_to_pick) {
          this.cMap.watch_to_pick.unsubscribe();
        }
        // if (this.cMap.watch_restart) {
        //     this.cMap.watch_restart.unsubscribe();
        //     console.log("watch_restart UNSUBSCRIBED")
        // }
        this.pop.SmartLoader("");
        this.cMap.setMarkers(location[0], location[1], this.uid);
        this.cMap.isPickedUp = true;
        this.storage.set("cMap.isPickedUp", this.cMap.isPickedUp);

        this.cdr.detectChanges();

      });
    }

    if (this.booked_for_someone == true) {
      this.booked_for_someone = false

     
      this.ph.send_sms(this.ph.phone, `A ride has been booked for you. Find Drivers Details:\nName: ${this.name}\nPhone: ${this.number}\nCar Plate: ${this.plate}`)

    
      
    }

   

  }

  startTimer() {
    
    this.interval = setInterval(() => {
      if (this.timeLeft > 0) {
        this.timeLeft--;
        console.log("TIMELEFT--.>>>" + this.timeLeft)
      } else {
        clearInterval(this.interval);
        if (this.cMap.onbar3) {
        
        } else {
          this.Close()
        }

        this.timeLeft = 60;
        console.log("TIMELEFT--.>>>" + this.timeLeft)
      }
    }, 1000);
  }



  onChange(e) {
    this.rideSelected = false;
    this.storage.set("rideSelected", this.rideSelected);
    this.Close();

    let yu = e;
    let charge = 0;
    this.eventProvider.getChatList(this.uid).off("child_added");

    this.act.getActiveProfile(this.uid).off("child_changed");
    this.act.getActiveProfile(this.uid).off("child_removed");

    var currentdate = new Date();

    var datetime =
      this.paddNumber(currentdate.getMonth() + 1, "00") +
      "-" +
      this.paddNumber(currentdate.getDate(), "00") +
      "-" +
      currentdate.getFullYear() +
      " @ " +
      this.paddNumber((currentdate.getHours() + 24) % 12 || 12, "00") +
      ":" +
      ("0" + currentdate.getMinutes()).slice(-2) +
      " " +
      (currentdate.getHours() > 11 ? "PM" : "AM");

    if (!this.pickUpLocation) {

      if (
        this.ph.name &&
        datetime &&
        this.myGcode.locationName &&
        this.myGcode.destinationSetName &&
        this.actualPrice &&
        yu
      )
        this.ph
          .Cancelled(
            this.allChat,
            this.ph.name,
            datetime,
            this.myGcode.locationName,
            this.myGcode.destinationSetName,
            this.actualPrice,
            yu,
            charge,
            this.mySurch,
            this.dProvider.price,
            this.outofstatecharge
          )
          .then(() => {
            this.ph
              .CancelledMe2(
                this.driver_ID,
                this.ph.name,
                datetime,
                this.myGcode.locationName,
                this.myGcode.destinationSetName,
                this.actualPrice,
                yu,
                charge,
                this.mySurch,
                this.dProvider.price,
                this.outofstatecharge
              )
              .then(() => { });
            this.ph
              .CancelledMe(
                this.ph.name,
                datetime,
                this.myGcode.locationName,
                this.myGcode.destinationSetName,
                this.actualPrice,
                yu,
                charge,
                this.mySurch,
                this.dProvider.price,
                this.outofstatecharge
              )
              .then(() => { });



            this.ph
              .getCancelledProfile()
              .child("Cancelled/documents")
              .on("value", (snapshot) => {
                this.items = [];
               

                snapshot.forEach((snap) => {
                 
                  if (snap.val().type == "Rider")
                    this.items.push({
                      key: snap.key,
                      text: snap.val().title,
                      type: snap.val().type,
                      status: snap.val().status,
                    });

                
                  return false;
                });
              });
            this.currentCar = null;
          });

          this.cdr.detectChanges();
    }

    else {
      if (
        this.ph.name &&
        datetime &&
        this.pickUpLocation &&
        this.myGcode.destinationSetName &&
        this.actualPrice &&
        yu
      )
        this.ph
          .Cancelled(
            this.allChat,
            this.ph.name,
            datetime,
            this.pickUpLocation,
            this.myGcode.destinationSetName,
            this.actualPrice,
            yu,
            charge,
            this.mySurch,
            this.dProvider.price,
            this.outofstatecharge
          )
          .then(() => {
            this.ph
              .CancelledMe2(
                this.driver_ID,
                this.ph.name,
                datetime,
                this.pickUpLocation,
                this.myGcode.destinationSetName,
                this.actualPrice,
                yu,
                charge,
                this.mySurch,
                this.dProvider.price,
                this.outofstatecharge
              )
              .then(() => { });
            this.ph
              .CancelledMe(
                this.ph.name,
                datetime,
                this.pickUpLocation,
                this.myGcode.destinationSetName,
                this.actualPrice,
                yu,
                charge,
                this.mySurch,
                this.dProvider.price,
                this.outofstatecharge
              )
              .then(() => { });



            this.ph
              .getCancelledProfile()
              .child("Cancelled/documents")
              .on("value", (snapshot) => {
                this.items = [];
              

                snapshot.forEach((snap) => {
                  
                  if (snap.val().type == "Rider")
                    this.items.push({
                      key: snap.key,
                      text: snap.val().title,
                      type: snap.val().type,
                      status: snap.val().status,
                    });

                  
                  return false;
                });
              });
            this.currentCar = null;
          });

          this.cdr.detectChanges();

    }

    this.pop.driverEnded = false;
    this.pop.hasCleared = true;

    this.storage.set("pop.hasCleared", this.pop.hasCleared);
    this.removeStorage()
  }


  hideFunctionsOnDriverFound() {
    this.cMap.onbar2 = false;
    this.cMap.onbar3 = true;
    this.pop.onRequest = true;
    this.storage.set("cMap.onbar3", this.cMap.onbar3);
    this.storage.set("pop.onRequest", this.pop.onRequest);
    this.shortMap = true //this is for the toggle to bring up the home widget
    this.cMap.selected_destination_bar = false; //show menu bar
    this.cMap.toggleBtn = true;
    this.cMap.onPointerHide = true;

    this.storage.set("shortMap", this.shortMap);
    this.storage.set("cMap.selected_destination_bar", this.cMap.selected_destination_bar);
    this.storage.set("cMap.onbar2", this.cMap.onbar2);

    this.watch.unsubscribe()
    // if (this.cMap.watch_to_drop) {
    //     this.cMap.watch_to_drop.unsubscribe();
    // }
    if (this.cMap.watch2) {
      this.cMap.watch2.unsubscribe();
    }
    if (this.cMap.watch) {
      this.cMap.watch.unsubscribe();
    }


    this.cdr.detectChanges();
    // if (this.cMap.watch_restart) {
    //     this.cMap.watch_restart.unsubscribe();
    //     console.log("watch_restart UNSUBSCRIBED")
    // }
  }

  hideFunctions() {
    this.hideNews = true;
    this.cMap.hasbooked = true;

   
    this.cMap.shove = false;
    ///hide and remove some properties on user request.
    this.cMap.onbar2 = true;

    this.cMap.selected_destination_bar = false;

    this.storage.set("cMap.selected_destination_bar", this.cMap.selected_destination_bar);
    this.storage.set("cMap.onbar2", this.cMap.onbar2);

    this.cMap.norideavailable = false;
    this.cMap.canShowchoiceTab = false;

    this.storage.set("cMap.canShowchoiceTab", this.cMap.canShowchoiceTab);
    this.storage.set("cMap.norideavailable", this.cMap.norideavailable);

    if (this.platform.is("cordova") || this.platform.is("mobileweb")) {
      this.cMap.map.setClickable(true);
    }

    // this.cMap.map.clear().then(() => {
    //   this.cMap.map.setCameraZoom(16);
    // });
    this.startedNavigation = true;
    this.pop.onRequest = true;
    this.storage.set("pop.onRequest", this.pop.onRequest);

    //  this.cMap.isCarAvailable = false;
    this.dProvider.calculateBtn = false;

    this.watch.unsubscribe()
    // if (this.cMap.watch_to_drop) {
    //     this.cMap.watch_to_drop.unsubscribe();
    // }
    if (this.cMap.watch2) {
      this.cMap.watch2.unsubscribe();
    }
    if (this.cMap.watch) {
      this.cMap.watch.unsubscribe();
    }
    // if (this.cMap.watch_restart) {
    //     this.cMap.watch_restart.unsubscribe();
    //     console.log("watch_restart UNSUBSCRIBED")
    // }

    // this.cMap.map.clear();
    this.cdr.detectChanges();
  }

  Onconnect(activity) {
   
    this.pop.presentSimpleLoader(this.lp.translate()[0].driverfound);
    // this.driver_ID = activity.driver_ID
    if (this.platform.is("cordova")) {
      this.vibration.vibrate(1000);
      this.audioPlayer.play();
    }

    this.hideFunctionsOnDriverFound();
    this.pop.uid = this.uid;
    if (this.platform.is("cordova") && this.platform.is("mobileweb")) {
      this.cMap.map.setClickable(true);
    }
    //  this.eventProvider.UpdateSate(true, this.uid);
    //console.log(this.uid)
    this.DriverFound(
      activity.Driver_location,
      activity.Driver_plate,
      activity.Driver_carType,
      activity.Driver_name,
      activity.Driver_seats,
      activity.Driver_Positive_rating,
      activity.Driver_picture,
      activity.Client_location[0],
      activity.Client_location[1],
      activity.Driver_number
    );

    
    // document.getElementById("destination").innerHTML =
    //   this.lp.translate()[0].dest;
    // this.storage.set(`currentUserId`, this.uid);
  }

  ShowPrevious() { }

  Charged(price) {
    
    if (this.platform.is("cordova") && this.platform.is("mobileweb")) {
      this.cMap.map.setClickable(false);
    }

    this.price = price; //|| activity.Pool_price;
   
    if (this.paymentType == 2) {
      this.act.getActivityProfile(this.uid).update({
        Client_hasPaid: true,
      });
    } else {
      this.act.getActivityProfile(this.uid).update({
        Client_paidCash: true,
      });
     
    }
    // })
  }

  getPrice() {
    this.ph.getPricing().limitToLast(1).once("value", (data) => {
      if (data.val()) {
        data.forEach((snap) => {

          let basefare = snap.val().base;
          let pricePerKm = snap.val().pricePerKm;
          let surge = snap.val().surge;

          this.basefar = basefare

          let cal_price = Math.round(((basefare) * pricePerKm));
          
        });
      } else {
        
      }

    })
  }

  checkPromoExist() {

    let id = this.ph.id;
    this.ph
      .getAllUsedCodes()
      .orderByChild("rider_id")
      .equalTo(id)
      .once("value", (data) => {
       

        if (data.val()) {
          data.forEach((snap) => {
            this.status = snap.val().status;
            let perc = snap.val().percentage;
            

            if (this.status == "activated") {
              this.percentage = perc / 100;
              
            } else {
             
            }
          });
        } else {
          
        }
      });
  }
}
