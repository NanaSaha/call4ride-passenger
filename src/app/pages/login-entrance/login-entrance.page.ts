import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, NavParams, MenuController } from '@ionic/angular';
import { SettingsService } from 'src/app/services/settings.service';

@Component({
  selector: 'app-login-entrance',
  templateUrl: './login-entrance.page.html',
  styleUrls: ['./login-entrance.page.scss'],
})
export class LoginEntrancePage implements OnInit {

  constructor(public navCtrl: NavController, public menu: MenuController, public settings: SettingsService,
    public load: LoadingController, ) {
      this.menu.enable(false);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StartupPage');

  }


  ngOnInit() {
  }

}
