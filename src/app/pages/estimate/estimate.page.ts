import { Component, OnInit } from '@angular/core';
declare var google;
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { Platform } from '@ionic/angular';
import { NavController, AlertController, ModalController, ActionSheetController, NavParams } from '@ionic/angular';
import { ProfileService } from 'src/app/services/profile.service';
import { LanguageService } from 'src/app/services/language.service';
import { DirectionserviceService } from 'src/app/services/directionservice.service';
import { NativeMapContainerService } from 'src/app/services/native-map-container.service';
import { GeocoderService } from 'src/app/services/geocoder.service';
import { PopUpService } from 'src/app/services/pop-up.service';
import { EventService } from 'src/app/services/event.service';
import { AutocompletePage } from '../autocomplete/autocomplete.page';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-estimate',
  templateUrl: './estimate.page.html',
  styleUrls: ['./estimate.page.scss'],
})
export class EstimatePage implements OnInit {

  public eventList: Array<any>;
  public location: any;
  public username: any;
  public destination: any;
  public userPos: any;
  public userDes: any;
  public lat: any;
  public currentYear: any;
  public lng: any;
  public dataTime: any;
  public scheduleInfo: any;
  public hasBooked = false;
  // tslint:disable-next-line: new-parens
  public geocoder: any = new google.maps.Geocoder;
  public locationName: any;
  userID: any;
  id: any;
  currentMonth: any;
  currentDay: any;
  public service: any = new google.maps.DistanceMatrixService();
  items: any;
  constructor(public navCtrl: NavController, public lp: LanguageService, public actRoute: ActivatedRoute,
    public alertCtrl: AlertController, public platform: Platform, public ph: ProfileService,
    public dProvider: DirectionserviceService, public cMap: NativeMapContainerService,
    public gCode: GeocoderService, public One: OneSignal, public pop: PopUpService,
    public eventProvider: EventService, private modalCtrl: ModalController, public urllocation: Location) {

    this.lat = this.actRoute.snapshot.paramMap.get('lat');
    this.lng = this.actRoute.snapshot.paramMap.get('lng');
    this.userPos = new google.maps.LatLng(this.lat, this.lng);
    const latlng = { lat: parseFloat(this.lat), lng: parseFloat(this.lng) };
    this.geocoder.geocode({ location: latlng }, (results, status) => {
      if (status === 'OK') {
        this.locationName = results[0].formatted_address;
        this.location = this.locationName;
      } else {
      }


    });
    this.ph.getWebAdminProfile().on('value', userProfileSnapshot => {
      const admin = userProfileSnapshot.val();
      this.dProvider.fare = admin.price;
      this.dProvider.pricePerKm = admin.perkm;
    });

  }


  Calculate() {
    if (this.userPos != null && this.userDes != null) {
      this.calcScheduleRoute(this.userPos, this.userDes, this.destination, this.location);
    } else {
      this.pop.showPimp('Please Add your Destination and Location');
    }

  }



  calcScheduleRoute(start, stop, destinationName, locationName) {
    this.pop.presentLoader('Processing....');

    this.service.getDistanceMatrix(
      {
        origins: [start, locationName],
        destinations: [destinationName, stop],
        travelMode: 'DRIVING',
        unitSystem: google.maps.UnitSystem.METRIC,
        avoidHighways: false,
        avoidTolls: false,
      }, (response, status) => {
        if (status === 'OK') {
          const fareTime = Math.floor(response.rows[0].elements[1].duration.value / 60) * 5.5;
          const price = Math.floor(response.rows[0].elements[1].distance.value / 1000)
            * this.dProvider.pricePerKm + this.dProvider.fare + fareTime;
          console.log(this.dProvider.pricePerKm, this.dProvider.fare);
          document.getElementById('cash').innerText = price;
          this.pop.hideLoader();
          console.log(price);
        }




      });


  }


  goBack() {
    this.urllocation.back();
  }






  async showAddressModal(selectedBar) {
    const modal = await this.modalCtrl.create({
      component: AutocompletePage,
      componentProps: ({ item: this.items })
    });
    modal.onDidDismiss()
      .then((data: any) => {
        // Open the address modal on location bar click to change location
        const item = data['data']
        console.log(data);
        if (selectedBar === 1 && item != null) {
          document.getElementById('position').innerText = item;
          this.location = item;
          this.gCode.geocoder.geocode({ address: item }, (results, status) => {
            if (status === 'OK') {
              const position = results[0].geometry.location;
              this.userPos = new google.maps.LatLng(position.lat(), position.lng());
              this.lat = position.lat();
              this.lng = position.lng();
            }
          });
        }
        // Open the address modal on destination bar click to change destination
        if (selectedBar === 2 && item != null) {
          document.getElementById('whereto').innerText = item;
          this.destination = item;
          /// After data input, check to see if user selected to add a destination or to calculate distance.
          this.gCode.geocoder.geocode({ address: item }, (results, status) => {
            if (status === 'OK') {
              const position = results[0].geometry.location;
              this.userDes = new google.maps.LatLng(position.lat(), position.lng());
              this.Calculate();
            }
          });

        }
      });
    modal.present();
  }





  ngOnInit() {
  }

}
