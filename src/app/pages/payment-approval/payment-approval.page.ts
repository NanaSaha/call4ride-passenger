import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController } from '@ionic/angular';
import { PopUpService } from 'src/app/services/pop-up.service';
import { LanguageService } from 'src/app/services/language.service';
import { SettingsService } from 'src/app/services/settings.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-payment-approval',
  templateUrl: './payment-approval.page.html',
  styleUrls: ['./payment-approval.page.scss'],
})
export class PaymentApprovalPage implements OnInit {

  from: string = this.actRoute.snapshot.paramMap.get('from');
  to: string = this.actRoute.snapshot.paramMap.get('to');
  charge: string = this.actRoute.snapshot.paramMap.get('charge');
  info: any = this.actRoute.snapshot.paramMap.get('info');
  constructor(public navCtrl: NavController, public lp: LanguageService, public settings: SettingsService,
    public pop: PopUpService, public actRoute: ActivatedRoute, public modalCtrl: ModalController) {


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad payment-approval');
  }

  closeModal() {
    this.modalCtrl.dismiss();
  }


  ngOnInit() {
  }

}
