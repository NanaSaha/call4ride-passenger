import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { CallNumber } from "@ionic-native/call-number/ngx";
import {
  NavController,
  AlertController,
  ModalController,
} from "@ionic/angular";
import { SettingsService } from "src/app/services/settings.service";
import { ProfileService } from "src/app/services/profile.service";
import { LanguageService } from "src/app/services/language.service";
import { EventService } from "src/app/services/event.service";
import { HttpClient } from "@angular/common/http";
import { Location } from "@angular/common";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-support",
  templateUrl: "./support.page.html",
  styleUrls: ["./support.page.scss"],
  // encapsulation: ViewEncapsulation.None,
})
export class SupportPage implements OnInit {
  id: any;
  public eventList: Array<any>;
  constructor(
    public navCtrl: NavController,
    public http: HttpClient,
    public call: CallNumber,
    public settings: SettingsService,
    private actRoute: ActivatedRoute,
    public alert: AlertController,
    public modal: ModalController,
    private location: Location,
    public eventProvider: EventService,
    public ph: ProfileService,
    public lp: LanguageService
  ) {
    this.actRoute.snapshot.paramMap.get("id");
  }

  ionViewDidEnter() {
    console.log("inregf");
    this.eventProvider
      .getSupportChatList(this.ph.id)
      .on("value", (snapshot) => {
        this.eventList = [];
        console.log("sjiy");
        snapshot.forEach((snap) => {
          this.eventList.push({
            id: snap.key,
            admin: snap.val().Admin_Message,
            user: snap.val().Client_Message,
          });
          console.log(this.eventList);
          return false;
        });
      });
  }

  closeChat() {
    this.modal.dismiss();
  }

  callNow() {
    this.call.callNumber(this.settings.appPhone, true);
  }

  async Send() {
    const alert = await this.alert.create({
      header: "message",
      inputs: [
        {
          name: "Your Message",
          placeholder: "Reply",
        },
      ],
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: (data) => {},
        },
        {
          text: "Send",
          handler: (data) => {
            this.send(
              "Support Message from user with ID " + this.ph.uid,
              data.Message,
              this.settings.support_email
            );
            this.eventProvider.Complain(data.Message, this.ph.id);
          },
        },
      ],
    });
    alert.present();
  }

  send(subject: string, message: string, email: string) {
    const url =
      "https://api.mailgun.net/v3/" + this.settings.mailGUrl + "/messages";
    const body = {
      // tslint:disable-next-line: object-literal-key-quotes
      from: email,
      // tslint:disable-next-line: object-literal-key-quotes
      to: this.settings.companyMail,
      // tslint:disable-next-line: object-literal-key-quotes
      subject: subject,
      // tslint:disable-next-line: object-literal-key-quotes
      html:
        "<html><body>First Name: " +
        this.settings.appName +
        "<br>" +
        "Last Name: " +
        this.settings.appLink +
        "<br>" +
        "Email: " +
        email +
        "<br>" +
        "Subject: " +
        subject +
        "<br><br>" +
        "Message: " +
        message +
        "</body></html>",
    };
    const headers = {
      // tslint:disable-next-line: object-literal-key-quotes
      Authorization: "Basic " + this.settings.mailGKey,
      "Content-Type": "application/x-www-form-urlencoded",
    };
  }
  goBack() {
    this.location.back();
  }

  ngOnInit() {}
}
