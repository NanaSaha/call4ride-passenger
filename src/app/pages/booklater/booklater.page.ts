import { Component, OnInit } from '@angular/core';
import { NavController, AlertController, ModalController, NavParams, Platform } from '@ionic/angular';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { SettingsService } from 'src/app/services/settings.service';
import { LanguageService } from 'src/app/services/language.service';
import { ProfileService } from 'src/app/services/profile.service';
import { DirectionserviceService } from 'src/app/services/directionservice.service';
import { GeocoderService } from 'src/app/services/geocoder.service';
import { NativeMapContainerService } from 'src/app/services/native-map-container.service';
import { EventService } from 'src/app/services/event.service';
import { PopUpService } from 'src/app/services/pop-up.service';
import { AutocompletePage } from '../autocomplete/autocomplete.page';
import { ActivatedRoute } from '@angular/router';
declare var google;

@Component({
  selector: 'app-booklater',
  templateUrl: './booklater.page.html',
  styleUrls: ['./booklater.page.scss'],
})
export class BooklaterPage implements OnInit {

  public eventList: Array<any>;
  public location: any;
  public username: any;
  public destination: any;
  public userPos: any;
  public userDes: any;
  public lat: any;
  public currentYear: any;
  public lng: any;
  public dataTime: any;
  public scheduleInfo: any;
  public hasBooked = false;
  // tslint:disable-next-line: new-parens
  public geocoder: any = new google.maps.Geocoder;
  public locationName: any;
  userID: any;
  id: any;
  currentMonth: any;
  currentDay: any;
  Lang: any;
  toppings = 'London Stansted Airport';
  public service: any = new google.maps.DistanceMatrixService();
  constructor(public navCtrl: NavController, public settings: SettingsService, public actRoute: ActivatedRoute,
    public lp: LanguageService, public alertCtrl: AlertController,
    public platform: Platform, public ph: ProfileService, public dProvider: DirectionserviceService,
    public cMap: NativeMapContainerService, public gCode: GeocoderService,
    public One: OneSignal, public pop: PopUpService, public eventProvider: EventService, private modalCtrl: ModalController) {
    this.Lang = this.lp.translate();
    this.settings.appCountryCode = 'UK';
    this.lat = this.actRoute.snapshot.paramMap.get('lat');
    this.lng = this.actRoute.snapshot.paramMap.get('lng');
    this.userPos = new google.maps.LatLng(this.lat, this.lng);
    const latlng = { lat: parseFloat(this.lat), lng: parseFloat(this.lng) };
    this.geocoder.geocode({ location: latlng }, (results, status) => {
      if (status === 'OK') {
        this.locationName = results[0].formatted_address;
        this.location = this.locationName;
      } else {
      }


    });

    this.currentYear = new Date().getFullYear();
    this.currentMonth = new Date().getUTCMonth() + 1;
    this.currentDay = new Date().getUTCDate();
    this.pop.presentLoader('');
    this.ph.getWebAdminProfile().on('value', userProfileSnapshot => {
      const admin = userProfileSnapshot.val();
      this.dProvider.fare = admin.price;
      this.dProvider.pricePerKm = admin.perkm;
      this.pop.hideLoader();
    });

    if (!this.platform.is('cordova')) {
      this.id = '3456789098765456733';
      this.ph.getScheduledProfile(this.id).on('value', userProfileSnapshot => {
        this.scheduleInfo = userProfileSnapshot.val();
        if (this.scheduleInfo != null) {
          const today = new Date();
          const future = new Date(this.scheduleInfo.Client_Date);
          console.log(future);
          this.dataTime = this.calcDate(today, future);
          console.log(this.dataTime);
        }
        this.ph.getScheduledProfile(this.id).off('value');
      });
    } else {
      this.One.getIds().then(id => {
        this.userID = id.userId;

        this.ph.getScheduledProfile(this.userID).on('value', userProfileSnapshot => {
          this.scheduleInfo = userProfileSnapshot.val();
          if (this.scheduleInfo != null) {
            const today = new Date();
            const future = new Date(this.scheduleInfo.Client_Date);
            console.log(future);
            this.dataTime = this.calcDate(today, future);
            console.log(this.dataTime);
          }
          this.ph.getScheduledProfile(this.id).off('value');
        });
      });

    }


  }

  ionViewDidEnter() {

    this.eventProvider.getScheduledList().on('value', snapshot => {
      this.eventList = [];
      snapshot.forEach(snap => {
        this.eventList.push({
          id: snap.key,
          date: snap.val().TimeandDate,
        });
        return false;
      });

      this.eventProvider.getScheduledList().off('value');
    });

  }




  calcDate(date1, date2) {

    const diff = Math.floor(date2.getTime() - date1.getTime());
    const day = 1000 * 60 * 60 * 24;

    const days = Math.floor(diff / day);
    const months = Math.floor(days / 31);


    const message = date2.toDateString();


    return message;
  }



  Chosen(e) {
    if (this.currentYear <= e.year) {
      console.log(this.userPos, this.userDes);
      if (this.userPos != null && this.userDes != null) {
        console.log(e);
        const date = [];
        const time = [];
        date.push(e.year, e.month, e.day);
        time.push(e.hour, e.minute);
        console.log(date);
        this.calcScheduleRoute(this.userPos, this.userDes, this.destination, this.toppings, date, time);
      } else {
        this.pop.showPimp(this.Lang[0].addDest);
      }
    } else {
      this.pop.showPimp(this.Lang[0].addTime);
      console.log(this.currentMonth, this.currentDay);
    }

  }



  calcScheduleRoute(start, stop, destinationName, locationName, date, time) {
    this.pop.presentLoader('');

    this.service.getDistanceMatrix(
      {
        origins: [start, locationName],
        destinations: [destinationName, stop],
        travelMode: 'DRIVING',
        unitSystem: google.maps.UnitSystem.METRIC,
        avoidHighways: false,
        avoidTolls: false,
      }, (response, status) => {
        if (status === 'OK') {
          console.log(this.dProvider.pricePerKm, this.dProvider.fare);
          let image = this.ph.user.photoURL;
          let name = this.ph.user.displayName;
          // tslint:disable-next-line: variable-name
          const edited_name = this.ph.name;
          let pay = this.ph.paymentType;
          this.pop.calculateBtn = false;

          if (image == null) {
            if (this.ph.pic == null) {
              image = 'https://cdn1.iconfinder.com/data/icons/instagram-ui-glyph/48/Sed-10-128.png';
            } else {
              image = this.ph.pic;
            }

          }

          if (name == null) {
            if (edited_name != null) {
              name = edited_name;
            } else {
              name = this.ph.user.email;
            }
          }

          if (pay == null) {
            pay = 1;
          }

          if (this.lat == null && this.lng == null) {
            this.lat = this.actRoute.snapshot.paramMap.get('lat');
            this.lng = this.actRoute.snapshot.paramMap.get('lng');
          }


          if (!this.platform.is('cordova')) {
            const id = '3456789098765456733';
            this.ph.getUserProfile().on('value', userProfileSnapshot => {
              let ratingText = userProfileSnapshot.val().ratingtext;
              let ratingValue = userProfileSnapshot.val().rating;
              if (ratingText == null && ratingValue == null) {
                ratingText = this.Lang[0].notrate;
                ratingValue = 0;
              }

              this.ph.getUserProfile().off('value');
              console.log(name, image, this.lat, this.lng, this.toppings, pay, this.destination,
                this.ph.phone, date, ratingText, ratingValue, time);
              this.eventProvider.CreateLondonBook(name, image, this.lat, this.lng, this.toppings, pay,
                this.destination, this.ph.phone, date, id, ratingText, ratingValue, time).then(s => {
                  this.pop.hideLoader();
                  this.pop.showPimp('Your ride has been scheduled successfully.');
                });

            });
          } else {
            this.One.getIds().then(id => {
              this.userID = id.userId;

              this.ph.getUserProfile().on('value', userProfileSnapshot => {
                let ratingText = userProfileSnapshot.val().ratingtext;
                let ratingValue = userProfileSnapshot.val().rating;
                if (ratingText == null && ratingValue == null) {
                  ratingText = this.Lang[0].notrate;
                  ratingValue = 0;
                }
                this.lat = this.actRoute.snapshot.paramMap.get('lat');
                this.lng = this.actRoute.snapshot.paramMap.get('lng');
                this.ph.getUserProfile().off('value');

                console.log(name, image, this.lat, this.lng, this.toppings, pay, this.destination, this.ph.phone, date);
                this.eventProvider.CreateLondonBook(name, image, this.lat, this.lng, this.toppings, pay,
                  this.destination, this.ph.phone, date, this.userID, ratingText, ratingValue, time).then(s => {
                    this.pop.hideLoader();
                    this.pop.showPimp(this.Lang[0].sucsch);
                  });

              });

            });

          }
        }

      });


  }




  async showAddressModal(selectedBar) {
    const modal = await this.modalCtrl.create({
      component: AutocompletePage
    });
    modal.onDidDismiss()
      .then((data: any) => {
        // Open the address modal on location bar click to change location
        console.log(data);
        if (selectedBar === 1 && data != null) {
          document.getElementById('position').innerText = data;
          this.gCode.geocoder.geocode({ address: data }, (results, status) => {
            if (status === 'OK') {
              const position = results[0].geometry.location;
              this.userPos = new google.maps.LatLng(position.lat(), position.lng());
              this.lat = position.lat();
              this.lng = position.lng();
            }
          });
        }
        // Open the address modal on destination bar click to change destination
        if (selectedBar === 2 && data != null) {
          document.getElementById('whereto').innerText = data;
          this.destination = data;
          /// After data input, check to see if user selected to add a destination or to calculate distance.
          this.gCode.geocoder.geocode({ address: data }, (results, status) => {
            if (status === 'OK') {
              const position = results[0].geometry.location;
              this.userDes = new google.maps.LatLng(position.lat(), position.lng());

            }
          });

        }
      });
    return await modal.present();
  }




  async CancelRide() {
    const alert = await this.alertCtrl.create({
      message: this.Lang[0].error,
      buttons: [
        {
          text: this.Lang[0].reject,
        },
        {
          text: this.Lang[0].accept,
          handler: data => {
            this.remove();
          }
        }
      ]
    });
    alert.present();
  }

  remove() {
    this.One.getIds().then(id => {
      this.userID = id.userId;
      this.ph.getScheduledProfile(this.userID).remove().then(s => {
        this.pop.showPimp(this.Lang[0].sucSchw);
        this.hasBooked = false;
      });
    });
  }



  ngOnInit() {
  }

}
