import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BooklaterPage } from './booklater.page';

describe('BooklaterPage', () => {
  let component: BooklaterPage;
  let fixture: ComponentFixture<BooklaterPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BooklaterPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BooklaterPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
