import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { LanguageService } from 'src/app/services/language.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

  constructor(public navCtrl: NavController, public storage: Storage, public lp: LanguageService,
    public location: Location) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad settings');
  }



  onSelectChange(selectedValue: any) {
    console.log('Selected', selectedValue);

    this.storage.set(`Language`, selectedValue).then(() => {
      console.log('saved id');
      this.lp.targetLanguageCode = selectedValue;
    });
  }
  goBack() {
    this.navCtrl.navigateRoot('');
  }
  ngOnInit() {
  }

}
