import { Component, OnInit } from '@angular/core';
import { NavController, AlertController, NavParams, ModalController } from '@ionic/angular';
import { EventService } from 'src/app/services/event.service';
import { ProfileService } from 'src/app/services/profile.service';
import { LanguageService } from 'src/app/services/language.service';
import { ActivatedRoute } from '@angular/router';
import { PopUpService } from "src/app/services/pop-up.service";

@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {

  id: any;
  data = { type: '', nickname: '', message: '' };
  public eventList: Array<any>;
  constructor(public navCtrl: NavController, public alert: AlertController,
    public eventProvider: EventService, public ph: ProfileService, public modalctrl: ModalController,
    public lp: LanguageService, public actRoute: ActivatedRoute, public pop: PopUpService,) {
    this.id = this.actRoute.snapshot.paramMap.get('id');
  }

  ionViewDidEnter() {
    console.log('inregf');
    this.eventProvider.getChatList(this.id).orderByValue().on('value', snapshot => {
      this.eventList = [];
      console.log('sjiy');
      snapshot.forEach(snap => {
        this.eventList.push({
          id: snap.key,
          driver: snap.val().Driver_Message,
          user: snap.val().Client_Message,
        });
        console.log(this.eventList);
        return false;
      });
    });
  }


  closeChat() {
    this.modalctrl.dismiss();
  }



  async Send() {
    console.log('MESSAGE', this.data.message);
    if (this.data.message == '') {
      this.pop.showPimp("Message is Empty so it cannot send")
    }
    else {
      this.ph.SendMessage(this.data.message, this.id);
      this.data.message = ''
    }
   
    // const alert = await this.alert.create({
    //   header: 'message',
    //   inputs: [
    //     {
    //       name: 'Message',
    //       placeholder: 'Reply'
    //     },
    //   ],
    //   buttons: [
    //     {
    //       text: 'Cancel',
    //       role: 'cancel',
    //       handler: data => {
    //       }
    //     },
    //     {
    //       text: 'Send',
    //       handler: data => {
    //         this.ph.SendMessage(data.Message, this.id);
    //       }
    //     }
    //   ]
    // });
    // return await alert.present();
  }


  ngOnInit() {
  }

}
