import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountkitPage } from './accountkit.page';

describe('AccountkitPage', () => {
  let component: AccountkitPage;
  let fixture: ComponentFixture<AccountkitPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountkitPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountkitPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
