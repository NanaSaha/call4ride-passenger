import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, Platform } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { ProfileService } from 'src/app/services/profile.service';
import { PopUpService } from 'src/app/services/pop-up.service';

@Component({
  selector: 'app-accountkit',
  templateUrl: './accountkit.page.html',
  styleUrls: ['./accountkit.page.scss'],
})
export class AccountkitPage implements OnInit {

  isNotCordova = false;

  constructor(public navCtrl: NavController, public authy: AuthService, public platform: Platform,
    public pop: PopUpService, public ph: ProfileService) {
  }

  ngOnInit() {
    console.log('ionViewDidLoad LoginPage');
    // call this function to initiate facebook account kit
    if (this.platform.is('cordova')) {
      this.login();
    } else {
      this.isNotCordova = true;
    }
  }

  login() {
    (window as any).AccountKitPlugin.loginWithPhoneNumber({
      useAccessToken: true,
      defaultCountryCode: 'US',
      facebookNotificationsEnabled: true
    }, (successdata) => {
      (window as any).AccountKitPlugin.getAccount((user) => {
        // push number to the database ones gotten
        this.pop.presentLoader('');
        this.ph.UpdateNumbers(user.phoneNumber).then(() => {
          this.navCtrl.navigateRoot('phone');
          this.pop.hideLoader();
        });
      });
    }, (err) => {
      this.login();
    });
  }








}
