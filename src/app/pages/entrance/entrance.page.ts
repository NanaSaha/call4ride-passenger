import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController } from '@ionic/angular';
@Component({
  selector: 'app-entrance',
  templateUrl: './entrance.page.html',
  styleUrls: ['./entrance.page.scss'],
})
export class EntrancePage implements OnInit {


  constructor(public navCtrl: NavController, public load: LoadingController, ) {
  }
  ngOnInit() {
    this.presentRouteLoader('');
  }


  async presentRouteLoader(message) {
    const loading = await this.load.create({
      // tslint:disable-next-line: object-literal-shorthand
      message: message
    });

    loading.present();

    const myTimeout = setTimeout(() => {
      loading.dismiss();
      this.navCtrl.navigateRoot('signup');
      clearTimeout(myTimeout);

    }, 1000);
  }


}
