import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController, NavController, Platform, NavParams } from '@ionic/angular';
import { FormBuilder } from '@angular/forms';
import { ProfileService } from 'src/app/services/profile.service';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { PopUpService } from 'src/app/services/pop-up.service';
import { LanguageService } from 'src/app/services/language.service';
import { SettingsService } from 'src/app/services/settings.service';
import { EventService } from 'src/app/services/event.service';

@Component({
  selector: 'app-referride',
  templateUrl: './referride.page.html',
  styleUrls: ['./referride.page.scss'],
})
export class ReferridePage implements OnInit {

  randomCode: any = 'Getting id...';
  message: string = null;
  file: string = null;
  link: string = null;
  subject: string = null;
  constructor(public ph: ProfileService, public share: SocialSharing,
    public lp: LanguageService, public pop: PopUpService, public eProvider: EventService,
    public platform: Platform, public nav: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,
    public alertCtrl: AlertController, public settings: SettingsService,
    public formBuilder: FormBuilder) {

    this.pop.presentLoader('Getting referal Earnings');
    this.ph.getUserProfile().on('value', userProfileSnapshot => {
      if (userProfileSnapshot.val()) {
        this.randomCode = userProfileSnapshot.val().idForRide;
      }
      this.pop.hideLoader();
    });

  }

  ionViewDidEnter() {
    this.message = 'Use ' + this.randomCode + ' as referal code to Register in  ' + this.settings.appName;

  }


  FaceShare() {
    this.share.share(this.message, this.subject, this.file, this.link).then(() => {

    }).catch(() => {

    });
  }


  ngOnInit() {
  }

}
