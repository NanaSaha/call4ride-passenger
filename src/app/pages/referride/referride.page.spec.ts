import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferridePage } from './referride.page';

describe('ReferridePage', () => {
  let component: ReferridePage;
  let fixture: ComponentFixture<ReferridePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReferridePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferridePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
