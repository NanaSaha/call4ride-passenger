import { Component, OnInit } from '@angular/core';
import {
  LoadingController,
  AlertController, NavController, Platform
} from '@ionic/angular';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { HttpClient, HttpHeaders, } from '@angular/common/http';

import { ProfileService } from 'src/app/services/profile.service';
import { SettingsService } from 'src/app/services/settings.service';
import { LanguageService } from 'src/app/services/language.service';
import { EventService } from 'src/app/services/event.service';
import { Observable } from 'rxjs';
import { Location } from '@angular/common';
import { PopUpService } from 'src/app/services/pop-up.service';
import { map } from 'rxjs/operators';
declare var Stripe;

@Component({
  selector: 'app-card',
  templateUrl: './card.page.html',
  styleUrls: ['./card.page.scss'],
})
export class CardPage implements OnInit {
  public cardpaymentForm: FormGroup;
  public data: any;
  info: any;
  paymentAmount: string = '3.33';
  currency: string = 'USD';
  currencyIcon: string = '$';
  stripe_key = 'pk_test_8tqD66FgCZq0DosnjKmXqdHe00aCCiUfTN';
  cardDetails: any = {};


  stripe = Stripe('pk_test_8tqD66FgCZq0DosnjKmXqdHe00aCCiUfTN');
  card: any;
  myCard: any = [];

  constructor(public prof: ProfileService, public pop: PopUpService, public http: HttpClient, public lp: LanguageService, public settings: SettingsService, public eProvider: EventService, public platform: Platform, public nav: NavController, public loadingCtrl: LoadingController,
    public alertCtrl: AlertController) {

  }



  ionViewDidLoad() {
    this.setupStripe();
  }


  setupStripe() {
    let elements = this.stripe.elements();
    var style = {
      base: {
        color: '#32325d',
        lineHeight: '24px',
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSmoothing: 'antialiased',
        fontSize: '16px',
        '::placeholder': {
          color: '#aab7c4'
        }
      },
      invalid: {
        color: '#fa755a',
        iconColor: '#fa755a'
      }
    };
    this.pop.SmartLoader('');

    this.card = elements.create('card', { style: style });

    this.card.mount('#card-element');

    this.card.addEventListener('change', event => {
      var displayError = document.getElementById('card-errors');
      if (event.error) {
        displayError.textContent = event.error.message;
      } else {
        displayError.textContent = '';
      }
    });



    var form = document.getElementById('payment-form');
    form.addEventListener('submit', event => {
      event.preventDefault();

      var ownerInfo = {
        owner: {
          name: this.prof.name,
          address: {
            line1: '',
            city: '',
            postal_code: '',
            country: '',
          },
          email: this.prof.email
        },

      }

      console.log(event)
      // this.stripe.createToken(this.card)
      this.stripe.createSource(this.card).then(result => {
        var rytytt = document.getElementById('card-element');
        console.log(rytytt);
        if (result.error) {
          var errorElement = document.getElementById('card-errors');
          errorElement.textContent = result.error.message;
        } else {
          this.pop.showPimp('Please Create Your Cloud Function link in card.page.ts line 117')
          this.pop.presentLoader('Processing...');
          this.http
            .post(
              '', { //Add your cloud fn url here.
              src: result.source.id,
              email: this.prof.user.email
            }).pipe(map((response: any) => response.json()))

            .subscribe(res => {
              // alert(JSON.stringify(res.id))
              this.prof.UpdatePaymentType(2).then(suc => {
                this.prof.AddPaymentCard(result, res.id).then(() => {

                  if (!this.prof.customerID)
                    this.prof.UseCard(result, res.id);

                  this.showPimp('Card Added Successfully.');
                  this.pop.hideLoader();
                });
              })
            });

        }
      });
    });
  }




 async showPimp(title) {
    const alert = await this.alertCtrl.create({
      message: title,
      buttons: [{
        text: this.lp.translate()[0].accept,
        role: 'cancel',
        handler: () => {
          this.nav.pop();
        }
      },],
      backdropDismiss: false
    });
  await  alert.present();
  }


  goBack() {
    this.nav.navigateRoot('payment')
  }
  ngOnInit() {

  }
}
