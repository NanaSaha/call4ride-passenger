import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { LanguageService } from 'src/app/services/language.service';
import { SettingsService } from 'src/app/services/settings.service';
import { ProfileService } from 'src/app/services/profile.service';
import { Location } from '@angular/common';
import { PopUpService } from 'src/app/services/pop-up.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.page.html',
  styleUrls: ['./payment.page.scss'],
})
export class PaymentPage implements OnInit {

  userProfile: any;
state: any;
public price: any =  this.route.snapshot.paramMap.get('price')

card: any;
paymentType: any;
cardnumber: any;
credits: any;
  items: any[];
  Level_key: string;
  NoKey: boolean = false;
  payWith: boolean = true;
  constructor(public ph: ProfileService,  public pop: PopUpService,  public set: SettingsService, public lp: LanguageService, public navCtrl: NavController,public route: ActivatedRoute) {
  }
ngOnInit(){

}
  ionViewDidEnter() {
    this.ph.getUserProfile().on('value', userProfileSnapshot => {
      if (userProfileSnapshot.val().credits)
      this.credits = userProfileSnapshot.val().credits

      if (userProfileSnapshot.val().payWith == 1)
      this.payWith = true;

      if (userProfileSnapshot.val().payWith == 2)
      this.payWith = false;

    })


    this.ph.getUserProfile().child('CreditCards').on('value', snapshot => {
      this.items = [];
      console.log(snapshot.val());

        snapshot.forEach( snap => {
          console.log(snap.val());
        
          if (snap.val().checked){
            this.Level_key = snap.key
            console.log(snap.key);
          }else{
            this.NoKey = true
          }

          this.items.push({
            key: snap.key,
            card: snap.val().card,
            checked: snap.val().checked,
            customerID: snap.val().customerID
          });
          console.log(snap.val());
          
          return false
        });
      });
  }


  UseCard(id, k, customerID){
    console.log(k, id, customerID);
    this.ph.UseCard(id, customerID)
    if (this.Level_key){
    this.ph.DitchCard(this.Level_key).then(()=>{
      this.ph.CheckCard(k)
      this.updatePayment(2)
    })
  }else{
    this.ph.CheckCard(k)
    this.updatePayment(2)
    this.NoKey = false;
  }

  this.payWith = false
  
  }

  UseCash(){
     this.payWith = true
    if (this.Level_key){
    this.ph.DitchCard(this.Level_key).then(()=>{
      this.updatePayment(1)
    })
  }else{
    this.NoKey = false;
    this.updatePayment(1)
  }
  
  }


  updatePayment(value){
    this.ph.UpdatePaymentType(value)
    this.navCtrl.navigateBack('home');
  }

  gotoCard(){
 
      this.navCtrl.navigateRoot('card')
    
  }

  Promo(){
    this.navCtrl.navigateRoot('referalcode')
  }
  goBack() {
    this.navCtrl.navigateBack('home');
  }
}
