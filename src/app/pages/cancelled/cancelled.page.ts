import { Component, OnInit } from '@angular/core';
import { AlertController, NavController, LoadingController } from '@ionic/angular';
import { ProfileService } from 'src/app/services/profile.service';
import { LanguageService } from 'src/app/services/language.service';
import { SettingsService } from 'src/app/services/settings.service';
import { PopUpService } from 'src/app/services/pop-up.service';
import { EventService } from 'src/app/services/event.service';

@Component({
  selector: 'app-cancelled',
  templateUrl: './cancelled.page.html',
  styleUrls: ['./cancelled.page.scss'],
})
export class CancelledPage implements OnInit {

  public eventList: Array<any>;

  constructor(public navCtrl: NavController, public ph: ProfileService, public alertCtrl: AlertController, public lp: LanguageService, public settings: SettingsService, public pop: PopUpService, public load: LoadingController, public eventProvider: EventService) { }

  ionViewDidEnter() {
    this.pop.presentLoader('').then(() => {
      this.pop.hideLoader()
    })
    this.eventProvider.getCancelledList().on('value', snapshot => {
      this.eventList = [];

      console.log(snapshot.val());
      snapshot.forEach(snap => {
        this.eventList.push({
          id: snap.key,
          name: snap.val().name,
          price: snap.val().price,
          date: snap.val().date,
          location: snap.val().location,
          destination: snap.val().destination,
          tip: snap.val().tip,
          upvote: snap.val().upvote,
          downvote: snap.val().downvote,
          charge: snap.val().charge,
          reason: snap.val().reason
        });
        this.eventList.sort();
        this.eventList.reverse();
        return false
      });
    });



  }
  goBack() {
    this.navCtrl.navigateRoot('history')
  }

  goToEventDetail(eventId) {
    this.navCtrl.navigateRoot(['historydetail', { 'eventId': eventId }]);
  }


  OpenCancelled() {
    this.navCtrl.navigateRoot('cancelled');
  }


  async TipMe(id) {
    const alert = await this.alertCtrl.create({
      message: 'Enter Ammount in $',
      inputs: [
        {

          value: ''
        },
      ],
      buttons: [
        {
          text: this.lp.translate()[0].cancel,
        },
        {
          text: this.lp.translate()[0].accept,
          handler: data => {
            console.log(data[0]);
            this.ph.updateHistoryTip(data[0], id).then(() => {
              this.pop.showPimp('Driver has Been Tipped');
            })

          }
        }
      ]
    });
    await alert.present();


  }




  ngOnInit() {
  }

}
