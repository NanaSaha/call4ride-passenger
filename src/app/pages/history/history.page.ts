import { Component, OnInit } from "@angular/core";
import {
  NavController,
  LoadingController,
  AlertController,
} from "@ionic/angular";
import { LanguageService } from "src/app/services/language.service";
import { PopUpService } from "src/app/services/pop-up.service";
import { EventService } from "src/app/services/event.service";
import { SettingsService } from "src/app/services/settings.service";
import { Location } from "@angular/common";
import { Router, NavigationExtras } from "@angular/router";
import { ProfileService } from "src/app/services/profile.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { map } from "rxjs/operators";

@Component({
  selector: "app-history",
  templateUrl: "./history.page.html",
  styleUrls: ["./history.page.scss"],
})
export class HistoryPage implements OnInit {
  public eventList: Array<any>;
  public math: any = Math;
  public float: any = parseFloat;
  custom_ID: any;
  surcharges: any;
  currentEvent: any;
  totalSurge: any;
  totalDriverSurge: number;
  flatDriver: any;
  percentDriver: any;
  actual: number;
  riderpaid: any;
  totemTIPS: any;
  surcharge: any;
  httpOptions = {
    headers: new HttpHeaders({ "Content-Type": "application/json" }),
  };
  constructor(
    public http: HttpClient,
    public navCtrl: NavController,
    public alert: AlertController,
    public ph: ProfileService,
    public lp: LanguageService,
    public settings: SettingsService,
    public pop: PopUpService,
    public load: LoadingController,
    public eventProvider: EventService
  ) { }
  ngOnInit() { }
  ionViewDidEnter() {
    let pinket;
    this.pop.presentLoader("").then(() => {
      this.pop.hideLoader();
    });
    this.eventProvider.getEventList().on("value", (snapshot) => {
      this.eventList = [];

      snapshot.forEach((snap) => {
        this.eventList.push({
          id: snap.key,
          name: snap.val().name,
          price: snap.val().price + snap.val().waitTimeCost,
          date: snap.val().date,
          location: snap.val().location,
          destination: snap.val().destination,
          tip:
            snap.val().tip -
            this.checkMe(snap.val().tip, snap.val().surcharge || []),
          upvote: snap.val().upvote || 0,
          downvote: snap.val().downvote || 0,
          toll: snap.val().tolls,
          surcharge: snap.val().surcharge,
          realPrice: snap.val().realPrice + snap.val().waitTimeCost,
          waitTimeCost: snap.val().waitTimeCost,
          osc: snap.val().osc,
          driver_id: snap.val().driver_id,
          driver_key: snap.val().driver_key,
        });

        this.eventList.sort();
        this.eventList.reverse();

        console.log("EVENT LISTTTS:::::::",this.eventList);
        console.log("EVENT LISTTTS:::::::", this.eventList.length);
        return false;
      });
      console.log("EVENT LISTTTS:::::::", this.eventList.length);
    });
  }

  checkMe(price, surcharge) {
    let c = [];
    let n = [];

    console.log(surcharge);

    surcharge.forEach((element) => {
      this.riderpaid = parseFloat(price).toFixed(2);

      //if driver
      if (element.owner == 0) {
        //if percent
        if (element.bone == 1) {
          let nb = element.price / 100;
          console.log(nb * this.riderpaid);
          let fo = nb * this.riderpaid;
          n.push(fo);
          const add2 = (a, b) => a + b;
          const result2 = n.reduce(add2);
          this.percentDriver = result2;
          console.log((Math.floor(element.price) / 100) * this.riderpaid);
        }
        //if flat fee
        if (element.bone == 0) {
          c.push(parseFloat(element.price));
          const add4 = (a, b) => a + b;
          const result4 = c.reduce(add4);
          this.flatDriver = result4;
          console.log(result4);
        }

        this.totalDriverSurge = this.flatDriver + this.percentDriver;
        console.log(this.totalDriverSurge, this.flatDriver, this.percentDriver);
      }
    });

    return this.totalDriverSurge;
  }

  goToEventDetail(eventId) {
    this.navCtrl.navigateRoot(["history-details", { eventId: eventId }]);
  }

  Approve(id, up, down) {
    this.ph.updateHistoryVoteUpSingle(id, up + 1);
  }
  disapprove(id, up, down) {
    this.ph.updateHistoryVoteDownSingle(id, down - 1);
  }

  OpenCancelled() {
    this.navCtrl.navigateRoot("cancelled");
  }

  async TipMe(id, driver_id, mykey) {
    const alert = await this.alert.create({
      message: "Enter Amount in $",
      inputs: [
        {
          value: "",
        },
      ],
      buttons: [
        {
          text: this.lp.translate()[0].cancel,
        },
        {
          text: this.lp.translate()[0].accept,
          handler: (data) => {
            console.log(data[0]);
            //this.pop.showLoader('')
            this.ph.updateDriiverTip(data[0], driver_id, mykey);
            this.ph.updateHistoryTip(data[0], id).then(() => {
              this.payWithStripe(parseFloat(data[0]).toFixed(2));
              this.pop.showPimp("Driver has Been Tipped");
              //this.pop.hideLoader();
            });
          },
        },
      ],
    });
    alert.present();
  }

  payWithStripe(amt) {
    //console.log(parseFloat(amt), this.custom_ID)
    this.pop.SmartLoader("Charging...");
    // this.http
    //   .post(
    //     "https://us-central1-ridefhv-61945.cloudfunctions.net/payWithStripe",
    //     {
    //       amount: amt * 100,
    //       currency: "usd",
    //       customer: this.ph.customerID,
    //     }
    //   )
    //   .pipe(map((response: any) => response.json()))
    //   .subscribe(
    //     (res) => {
    //       this.pop.hideLoader();
    //     },
    //     (error) => {
    //       this.showPimp(
    //         "Credit Card Error. Please select a valid credit card from your payments settings."
    //       );
    //       //this.pop.hideLoader();
    //     }
    //   );
  }

  async showPimp(title) {
    const alert = await this.alert.create({
      message: title,
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: () => { },
        },
        {
          text: "Go To Payments",
          role: "cancel",
          handler: () => {
            this.navCtrl.navigateRoot("");
          },
        },
      ],
      backdropDismiss: false,
    });
    await alert.present();
  }

  async goBack() {
    this.navCtrl.navigateRoot("home");
  }
}
