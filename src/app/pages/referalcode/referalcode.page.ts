import { Component, OnInit } from "@angular/core";
import { NavController } from "@ionic/angular";
import { LanguageService } from "src/app/services/language.service";
import { ProfileService } from "src/app/services/profile.service";
import { PopUpService } from "src/app/services/pop-up.service";
import { Location } from "@angular/common";
import firebase from "firebase";
import { Router } from "@angular/router";

@Component({
  selector: "app-referalcode",
  templateUrl: "./referalcode.page.html",
  styleUrls: ["./referalcode.page.scss"],
})
export class ReferalcodePage implements OnInit {
  userName: any;
  code: any;
  perc;
  public all_data: any;
  public items: any;
  verify_code: any;
  rider_id: any;
  status;
  applied: boolean = false;
  user_id;
  percentage;
  constructor(
    public router: Router,
    public navCtrl: NavController,
    public lp: LanguageService,
    public ph: ProfileService,
    private pop: PopUpService,
    public location: Location
  ) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad referalcode");
  }

  apply_code(code) {
    // Step 2 - Pass the mobile number for verific
    if (this.code != null) {
      // this.pop.presentLoader("");

      // this.AddCode(code);
      this.SearchForSharingIDs(code);
    } else {
      this.pop.presentToast("ENTER VALID DISCOUNT CODE");
    }
  }

  // AddCode(code): void {
  //   this.SearchForSharingIDs(code);
  // }

  SearchForSharingIDs(code) {
    console.log("PROMO ID " + code);
    this.ph
      .getAllSharingPromoID()
      .orderByChild("code")
      .equalTo(code)
      .on("value", (data) => {
        this.all_data = data;

        if (data.val()) {
          this.all_data.forEach((snap) => {
            let rider_id = this.ph.id;
            let code = snap.val().code;
            let percentage = snap.val().percentage;

            this.verify_promo(code, rider_id, percentage);
            //this.navCtrl.navigateRoot("referalcode");
            // this.router.navigateByUrl("referalcode");
            //return false;
          });

          console.log("VALID");
        } else {
          this.pop.presentToast("PROMO CODE NOT VALID");
          console.log("NOT VALID");
          this.router.navigateByUrl("referalcode");
          // this.navCtrl.navigateRoot("referalcode");
          //this.pop.hideLoader();
        }
        //this.pop.hideLoader();
      });

    // var ref = firebase.database().ref("SharingIDPromo");
    // ref
    //   .orderByChild("code")
    //   .equalTo(promoID)
    //   .on("value", function (snapshot) {
    //     // this.all_data = snapshot.val();
    //     console.log("SNAPSHOT DATA::" + snapshot.val());
    //     let all_data = snapshot.val();
    //     console.log("SNAPSHOT DATA 2::" + all_data);
    //     this.all_data = all_data;
    //     console.log("SNAPSHOT DATA 3::" + this.all_data);
    //   });
  }

  createDiscount() {
    console.log("CODE " + this.code, "PErcentage " + this.perc);
    this.ph.createPromo(this.code, this.perc);
  }

  verify_promo(code, id, perc) {
    console.log("RUNNINGH VERIFY CODE --->>>>");

    this.verify_code = code;
    this.rider_id = id;

    console.log("VERIFYPROMO CODE IS " + this.verify_code);
    console.log("RIDER ID IS " + this.rider_id);
    this.ph
      .getAllUsedCodes()
      .orderByChild("rider_id")
      .equalTo(id)
      .once("value", (data) => {
        console.log("VALUE FROM VERIFYIN CODE: " + JSON.stringify(data.val()));

        let arr = JSON.stringify(data.val());
        var index = arr.indexOf(code);

        console.log("index is : " + index);

        if (data.val()) {
          data.forEach((snap) => {
            this.user_id = this.ph.id;
            let oldcode = snap.val().code;
            this.percentage = snap.val().percentage;
            let status = snap.val().status;

            console.log(this.user_id, oldcode, this.percentage);

            console.log("Code is " + this.verify_code);
            console.log("oldcode is " + oldcode);
            console.log("Status is " + status);

            // if (this.verify_code == oldcode && status == "activated") {
            //   console.log("code used");
            //   this.pop.presentToast("DISCOUNT CODE APPLIED BUT NOT USED");

            //   this.router.navigateByUrl("referalcode");
            // } else if (this.verify_code == oldcode && status == "deactivated") {
            //   console.log("Same code but deactivated-- APPLY");
            //   this.pop.presentToast("A DISCOUNT APPLIED TO YOUR NEXT RIDE");

            //   this.router.navigateByUrl("referalcode");
            // } else if (this.verify_code != oldcode && status == "activated") {
            //   console.log(
            //     "old code not used but it still activated-- DONT APPLY"
            //   );
            //   this.pop.presentToast("OLD DISCOUNT CODE NOT USED ");

            //   this.router.navigateByUrl("referalcode");
            // } else if (this.verify_code != oldcode && status == "deactivated") {
            //   console.log("code not used hence save it");
            //   this.pop.presentToast("A DISCOUNT APPLIED TO YOUR NEXT RIDE");
            //   this.applied = true;
            //   console.log("--->>>> APPLIIIEEEDDD--->>>" + this.applied);

            // this.router.navigateByUrl("referalcode");
            //}
          });

          if (index == -1) {
            console.log("INDEX IS -1 CODE DOESNT EXIST");
            this.pop.presentToast("PROMO CODE APPLIED SUCCESSFULLY");
            this.ph.RiderPromoSaved(this.user_id, code, perc, "activated");
          } else {
            this.pop.presentToast("DISCOUNT CODE APPLIED ALREADY");
          }
        } else {
          console.log("code doesnt exist hence save it");

          this.pop.presentToast("PROMO CODE APPLIED SUCCESSFULLY");

          this.ph.RiderPromoSaved(id, code, perc, "activated");

          this.router.navigateByUrl("referalcode");
        }
      });
  }

  goBack() {
    this.location.back();
  }

  checkPromoExist() {
    let id = this.ph.id;
    this.ph
      .getAllUsedCodes()
      .orderByChild("rider_id")
      .equalTo(id)
      .once("value", (data) => {
        console.log("VALUE FROM VERIFYIN CODE: " + JSON.stringify(data.val()));

        if (data.val()) {
          data.forEach((snap) => {
            this.status = snap.val().status;
            let percen = snap.val().percentage;
            console.log("Status is " + this.status);
            console.log("percen is " + percen);

            if (this.status == "activated") {
              console.log("The percentage here is " + percen);
            } else {
              console.log("Deactivated The percentage here is " + percen);
            }
          });
        } else {
          console.log("NO PROMO CODE ACTIVATED YET");
        }
      });
  }

  ngOnInit() {
    this.checkPromoExist();
  }
}
