import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferalcodePage } from './referalcode.page';

describe('ReferalcodePage', () => {
  let component: ReferalcodePage;
  let fixture: ComponentFixture<ReferalcodePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReferalcodePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferalcodePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
