import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
// import { IonicPage } from '@ionic/angular';
import { LanguageService } from 'src/app/services/language.service';
import { SettingsService } from 'src/app/services/settings.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
})
export class AboutPage implements OnInit {

  Lang: any;
  constructor(public iab: InAppBrowser, public lp: LanguageService, public settings: SettingsService,
     public navCtrl: NavController, public location: Location) {
    this.Lang = this.lp.translate();
  }
  gotoSite() {
    this.iab.create(this.settings.appLink);
  }

  gotoSite2() {
    this.iab.create(this.settings.appCareer);
  }

  gotoSite3() {
    this.iab.create(this.settings.appFaq);
  }

  gotoSite4() {
    this.iab.create(this.settings.appLink);
  }


  gotoSite9() {
    this.iab.create(this.settings.appFB);
  }

  gotoSite10() {
    this.iab.create(this.settings.appinsta);
  }


  appTikTok() {
    this.iab.create(this.settings.appTikTok);
  }

  appTube() {
    this.iab.create(this.settings.appYoutube);
  }

  twitter() {
    this.iab.create(this.settings.twitter);
  }
  goBack() {
    this.navCtrl.navigateRoot('home');
  }
  ngOnInit() {
  }

}
