import { Component, OnInit } from "@angular/core";
import { NavController, NavParams } from "@ionic/angular";
import { LanguageService } from "src/app/services/language.service";
import { SettingsService } from "src/app/services/settings.service";
import { EventService } from "src/app/services/event.service";
declare var google;
import { Location } from "@angular/common";

import { ActivatedRoute, Router } from "@angular/router";
import { ProfileService } from "src/app/services/profile.service";

@Component({
  selector: "app-history-details",
  templateUrl: "./history-details.page.html",
  styleUrls: ["./history-details.page.scss"],
})
export class HistoryDetailsPage implements OnInit {
  public currentEvent: any = {};
  total: any = 0;
  tolls: any[] = [];
  tollName: any[];
  myop: any[];
  warary: any;
  element: any;
  element2: any;
  allTotals: any = 0;
  driverMade: any = 0;
  surcharges: any = [];
  totalSurge: number = 0;
  actual: number = 0;
  riderPaid: any = 0;
  totalRiderSurge: any = 0;
  totalDriverSurge: any = 0;
  percentRider: number = 0;
  flatRider: any = 0;
  percentDriver: number = 0;
  flatDriver: any = 0;

  riderPercents: any = [];
  driverPercents: any = [];
  math = Math;
  riderpaid: any = 0;
  osc: any;
  dateOfTip: any;
  totemTIPS: any;
  myTip: any;
  percentDrive: any;
  flatDrive: any;
  totalDriverSurg: any;
  currentPrie: any;
  waitTimeCost: any = 0;
  constructor(
    public navCtrl: NavController,
    public prof: ProfileService,
    public lp: LanguageService,
    public settings: SettingsService,
    public eventProvider: EventService,
    public route: ActivatedRoute
  ) {}

  ionViewDidEnter() {
    let g = [];
    let b = [];
    let k = [];
    let o = [];
    let c = [];
    let n = [];
    let j = [];
    let m = [];

    console.log(this.route.snapshot.paramMap.get("eventId"));

    this.eventProvider
      .getEventDetail(this.route.snapshot.paramMap.get("eventId"))
      .on("value", (eventSnapshot) => {
        this.currentEvent = eventSnapshot.val();
        this.currentEvent.id = eventSnapshot.key;
        console.log(this.currentEvent);

        this.tolls = this.currentEvent.tolls;

        this.osc = this.currentEvent.osc;

        this.dateOfTip = this.currentEvent.date;

        console.log(this.tolls);

        this.currentEvent.price;

        this.currentPrie = this.currentEvent.price;
        this.waitTimeCost = this.currentEvent.waitTimeCost;

        //this.riderpaid = parseFloat(this.currentEvent.price).toFixed(2);
        this.riderpaid = this.currentEvent.price + this.currentEvent.waitTimeCost;

        //NO SURCHAGES NOW
        //this.surcharges = this.currentEvent.surcharge;

        // if (this.currentEvent.surcharge)
        //   for (
        //     let index = 0;
        //     index < this.currentEvent.surcharge.length;
        //     index++
        //   ) {
        //     k.push(parseFloat(this.currentEvent.surcharge[index].price));
        //     const add = (a, b) => a + b;
        //     const result = k.reduce(add);
        //     this.totalSurge = result;
        //     this.actual = this.currentEvent.price - this.totalSurge;
        //     console.log(this.totalSurge);
        //     this.currentEvent.surcharge[index].price;
        //   }

        // this.surcharges.forEach((element) => {
        //   //if rider
        //   if (element.owner == 1) {
        //     //if percent
        //     if (element.bone == 1) {
        //       let fo = (element.price / 100) * this.currentEvent.realPrice;
        //       console.log(element.price);
        //       o.push(fo);
        //       const add1 = (a, b) => a + b;
        //       const result1 = o.reduce(add1);
        //       this.percentRider = result1;
        //       console.log(this.percentRider);
        //     }

        //     if (element.bone == 0) {
        //       g.push(parseFloat(element.price));
        //       const add = (a, b) => a + b;
        //       const result = g.reduce(add);
        //       this.flatRider = result;
        //       console.log(result);
        //       element.price;
        //     }

        //     this.totalRiderSurge = this.flatRider + this.percentRider;
        //     console.log(this.totalRiderSurge);
        //   }

        //   this.riderpaid = parseFloat(this.currentEvent.price).toFixed(2);

        //   //if driver
        //   if (element.owner == 0) {
        //     //if percent
        //     if (element.bone == 1) {
        //       let nb = element.price / 100;
        //       console.log(nb * this.currentEvent.tip);
        //       let fo = nb * this.currentEvent.tip;
        //       j.push(fo);
        //       const add2 = (a, b) => a + b;
        //       const result2 = j.reduce(add2);
        //       this.percentDrive = result2;
        //       console.log(
        //         (Math.floor(element.price) / 100) * this.currentEvent.tip
        //       );
        //     }
        //     //if flat fee
        //     if (element.bone == 0) {
        //       m.push(parseFloat(element.price));
        //       const add4 = (a, b) => a + b;
        //       const result4 = m.reduce(add4);
        //       this.flatDrive = result4;
        //       console.log(result4);
        //     }

        //     this.totalDriverSurg = this.flatDrive + this.percentDrive;
        //     //console.log(this.totalDriverSurge, this.flatDriver, this.percentDriver);
        //   }

        //   //if driver
        //   if (element.owner == 0) {
        //     //if percent
        //     if (element.bone == 1) {
        //       let nb = element.price / 100;
        //       console.log(nb * this.riderpaid);
        //       let fo = nb * this.riderpaid;
        //       n.push(fo);
        //       const add2 = (a, b) => a + b;
        //       const result2 = n.reduce(add2);
        //       this.percentDriver = result2;
        //       console.log((Math.floor(element.price) / 100) * this.riderpaid);
        //     }
        //     //if flat fee
        //     if (element.bone == 0) {
        //       c.push(parseFloat(element.price));
        //       const add4 = (a, b) => a + b;
        //       const result4 = c.reduce(add4);
        //       this.flatDriver = result4;
        //       console.log(result4);
        //     }

        //     this.totalDriverSurge = this.flatDriver + this.percentDriver;
        //     console.log(
        //       this.totalDriverSurge,
        //       this.flatDriver,
        //       this.percentDriver
        //     );
        //   }
        // });

        this.totemTIPS =
          (parseFloat(this.currentEvent.tip) - this.totalDriverSurg).toFixed(
            2
          ) || 0;

        if (!this.totemTIPS) {
          this.totemTIPS = 0;
        }

        // console.log(this.actual, g, this.currentEvent.surcharge[index].price);

        console.log(this.currentEvent.tolls);

        if (this.currentEvent.tolls) {
          for (let index = 0; index < this.currentEvent.tolls.length; index++) {
            b.push(this.currentEvent.tolls[index].tagCost);
            const add = (a, b) => a + b;
            const result = b.reduce(add);
            this.total = result;
            console.log(this.total, g);
          }
        }

        if (!this.currentEvent.tip) {
          this.currentEvent.tip = 0;
        }

        this.myTip = parseFloat(this.currentEvent.tip).toFixed(2);

        this.driverMade = this.riderpaid;
        this.GetRoute(
          this.currentEvent.location,
          this.currentEvent.destination
        );
      });
  }

  ngOnInit() {
    // this.GetRoute(this.currentEvent.location, this.currentEvent.destination);
  }

  GetRoute(location, destination) {
    var source, destination;
    var directionsService = new google.maps.DirectionsService();
    let directionsDisplay = new google.maps.DirectionsRenderer({
      draggable: true,
    });
    var mumbai = new google.maps.LatLng(18.975, 72.8258);
    var mapOptions = {
      zoom: 7,
      center: mumbai,
      disableDefaultUI: true,
    };
    let map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
    directionsDisplay.setMap(map);
    //directionsDisplay.setPanel(document.getElementById('dvPanel'));

    //*********DIRECTIONS AND ROUTE**********************//
    source = location;
    destination = destination;

    var request = {
      origin: source,
      destination: destination,
      travelMode: google.maps.TravelMode.DRIVING,
    };
    directionsService.route(request, (response, status) => {
      if (status == google.maps.DirectionsStatus.OK) {
        directionsDisplay.setDirections(response);
      }
    });

    //*********DISTANCE AND DURATION**********************//
    var service = new google.maps.DistanceMatrixService();
    service.getDistanceMatrix(
      {
        origins: [source],
        destinations: [destination],
        travelMode: google.maps.TravelMode.DRIVING,
        unitSystem: google.maps.UnitSystem.METRIC,
        avoidHighways: false,
        avoidTolls: false,
      },
      (response, status) => {
        if (
          status == google.maps.DistanceMatrixStatus.OK &&
          response.rows[0].elements[0].status != "ZERO_RESULTS"
        ) {
          // var dvDistance = document.getElementById("dvDistance");
          //  dvDistance.innerHTML = "";
          // dvDistance.innerHTML += "Distance: " + distance + "<br />";
          // dvDistance.innerHTML += "Duration:" + duration;
        } else {
          alert("Unable to find the distance via road.");
        }
      }
    );
  }

  async goBack() {
    this.navCtrl.navigateRoot("home");
  }
}
