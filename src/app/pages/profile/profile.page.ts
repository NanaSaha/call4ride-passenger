import { Component, OnInit } from "@angular/core";
import firebase from "firebase/app";
import {
  ActionSheetController,
  NavController,
  ModalController,
  AlertController,
} from "@ionic/angular";
import { ProfileService } from "src/app/services/profile.service";
import { AuthService } from "src/app/services/auth.service";
import { PopUpService } from "src/app/services/pop-up.service";
import { LanguageService } from "src/app/services/language.service";
import { AutocompletePage } from "../autocomplete/autocomplete.page";
import { Camera, CameraOptions } from "@ionic-native/camera/ngx";
import { Location } from "@angular/common";
import { Router } from '@angular/router';
import { Storage } from "@ionic/storage";


@Component({
  selector: "app-profile",
  templateUrl: "./profile.page.html",
  styleUrls: ["./profile.page.scss"],
})
export class ProfilePage implements OnInit {
  public userProfile: any;
  public birthDate: string;
  public phone: any;
  public work: any;
  public home: any;
  public username: any;
  public pic: any;
  public email: any;
  public captureDataUrl: any;
  unique;
  emergencyNumber;
  last_name;
  first_name;
  items: any;
  phoneNumber;
  picture;
  constructor(
    public navCtrl: NavController,
    public lp: LanguageService,
    public actionSheetCtrl: ActionSheetController,
    public modalCtrl: ModalController,
    private pop: PopUpService,
    private camera: Camera,
    public alertCtrl: AlertController,
    public ph: ProfileService,
    public authProvider: AuthService,
    public location: Location,
    public router: Router,
    public storage: Storage,
  ) {
    ph.isHome = false;
  }

  ionViewDidEnter() {
    this.ph.getUserProfile().on("value", (userProfileSnapshot) => {
      console.log("userProfileSnapshot::", userProfileSnapshot.val());
      this.userProfile = userProfileSnapshot.val();

      this.first_name = userProfileSnapshot.val().first_name;
      this.last_name = userProfileSnapshot.val().last_name;
      this.email = userProfileSnapshot.val().email;
      this.emergencyNumber = userProfileSnapshot.val().emergencyNumber;
      this.phoneNumber = userProfileSnapshot.val().phonenumber;
      this.unique = userProfileSnapshot.val().unique_number;
      this.picture = userProfileSnapshot.val().picture;
      console.log("EMERGENC::", this.emergencyNumber);
      console.log("PHONE::", this.phoneNumber);
    });
  }

  remove(): void {
    this.authProvider.logoutUser().then(() => {
      this.navCtrl.navigateRoot("login-entrance");
    });
  }

  async choosePic() {
    const actionSheet = await this.actionSheetCtrl.create({
      header: this.lp.translate()[0].choosefrom,
      buttons: [
        {
          text: this.lp.translate()[0].camera,
          icon: "ios-camera",
          handler: () => {
            this.changePic();
          },
        },
        {
          text: this.lp.translate()[0].file,
          icon: "ios-folder",
          handler: () => {
            this.changePicFromFile();
          },
        },
        {
          text: this.lp.translate()[0].cancel,
          icon: "close",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          },
        },
      ],
    });
    actionSheet.present();
  }

  changePic() {
    const cameraOptions: CameraOptions = {
      quality: 10,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
    };

    this.camera.getPicture(cameraOptions).then((imageData) => {
      this.captureDataUrl = "data:image/jpeg;base64," + imageData;

      this.processProfilePicture(this.captureDataUrl);
    });
  }

  changePicFromFile() {
    const cameraOptions: CameraOptions = {
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      quality: 10,
      encodingType: this.camera.EncodingType.PNG,
    };

    this.camera.getPicture(cameraOptions).then((imageData) => {
      this.captureDataUrl = "data:image/jpeg;base64," + imageData;

      this.processProfilePicture(this.captureDataUrl);
    });
  }

  processProfilePicture(captureData) {
    const storageRef = firebase.storage().ref();
    // Create a timestamp as filename
    const filename = Math.floor(Date.now() / 1000);
    this.pop.presentLoader("Processing image..");
    // Create a reference to 'images/todays-date.jpg'
    const imageRef = storageRef.child(`userPictures/${filename}.jpg`);

    imageRef
      .putString(captureData, firebase.storage.StringFormat.DATA_URL)
      .then((snapshot) => {
        imageRef
          .getDownloadURL()
          .then((url) => {
            this.ph
              .UpdatePhoto(url)
              .then((success) => {
                this.pop.hideLoader();

                this.pop.presentToast(this.lp.translate()[0].pictureset);
              })
              .catch((error) => {
                alert(error);
              });
          })
          .catch((error) => {
            alert(error);
          });
      })
      .catch((error) => {
        alert(error);
      });
  }

  async updateNumber() {
    const alert = await this.alertCtrl.create({
      message: this.lp.translate()[0].phone,
      inputs: [
        {
          value: this.phone,
        },
      ],
      buttons: [
        {
          text: this.lp.translate()[0].cancel,
        },
        {
          text: this.lp.translate()[0].accept,
          handler: (data) => {
            console.log(data);
            this.ph.UpdateNumbers(data);
          },
        },
      ],
    });
    alert.present();
  }

  async updateEmergencyNumber() {
    const alert = await this.alertCtrl.create({
      message: "Enter an emergency number",
      inputs: [
        {
          value: this.emergencyNumber,
        },
      ],
      buttons: [
        {
          text: this.lp.translate()[0].cancel,
        },
        {
          text: this.lp.translate()[0].accept,
          handler: (data) => {
            console.log("EMERGENCY NUMBER DATA" + data);
            this.ph.UpdateEmergencyNumbers(data);
          },
        },
      ],
    });
    alert.present();
  }

  async updateName() {
    const alert = await this.alertCtrl.create({
      message: this.lp.translate()[0].name,
      inputs: [
        {
          value: this.userProfile.updateName,
        },
      ],
      buttons: [
        {
          text: this.lp.translate()[0].cancel,
        },
        {
          text: this.lp.translate()[0].accept,
          handler: (data) => {
            console.log(data);
            this.ph.updateName(data);
          },
        },
      ],
    });
    alert.present();
  }

  async updateEmail() {
    const alert = await this.alertCtrl.create({
      message: this.lp.translate()[0].email,
      inputs: [
        {
          value: this.ph.userID.email,
        },
      ],
      buttons: [
        {
          text: this.lp.translate()[0].cancel,
        },
        {
          text: this.lp.translate()[0].accept,
          handler: (data) => {
            console.log(data);
            this.ph.updateEmail(data);
          },
        },
      ],
    });
    alert.present();
  }

  async updateHome() {
    const modal = await this.modalCtrl.create({
      component: AutocompletePage,
      componentProps: { item: this.items },
    });
    modal.onDidDismiss().then((data: any) => {
      const item = data["data"];
      this.home = item;
      if (data != null) {
        this.ph.UpdateHome(item);
        this.ph.home = true;
      }
    });

    modal.present();
  }

  async updateWork() {
    const modal = await this.modalCtrl.create({
      component: AutocompletePage,
      componentProps: { item: this.items },
    });
    modal.onDidDismiss().then((data: any) => {
      const item = data["data"];
      this.home = item;
      if (data != null) {
        this.ph.UpdateWork(item);
        this.ph.work = true;
      }
    });
    modal.present();
  }

  async logOut() {
    const alert = await this.alertCtrl.create({
      message: this.lp.translate()[0].error,
      buttons: [
        {
          text: this.lp.translate()[0].cancel,
        },
        {
          text: this.lp.translate()[0].accept,
          handler: () => {
            
            this.navCtrl.navigateRoot("login");
            this.authProvider.logoutUser
            this.authProvider.signOut();
          },
        },
      ],
    });
    alert.present();
  }
  goBack() {
    this.navCtrl.navigateRoot("home");
  }


  async deleteAccount() {
    const alert = await this.alertCtrl.create({
      message: "Do you really want to delete your account ? ",

      buttons: [
        {
          text: "No",
        },
        {
          text: "Yes",
          handler: (data) => {
            console.log("EMERGENCY NUMBER DATA" + data);
            // this.authProvider.signOut();
            // this.navCtrl.navigateRoot("login");
            this.router.navigateByUrl('/login');
            this.deleteAlert()
          },
        },
      ],
    });
    alert.present();


  }

  async deleteAlert() {
    const alert = await this.alertCtrl.create({
      message: "We will delete your account in the next 24 hours. If you want to rescind your decision, send us an email: info@upwheels.com",

      buttons: [
        {
          text: "Okay",
        },
      ],
    });
    alert.present();

  }

  ngOnInit() { }


  async clearCache() {
    const alert = await this.alertCtrl.create({
      message: "Do you really want to clear your cache ? ",

      buttons: [
        {
          text: "No",
        },
        {
          text: "Yes",
          handler: (data) => {
            console.log("EMERGENCY NUMBER DATA" + data);
            this.storage.clear();
            this.authProvider.signOut();
            this.router.navigateByUrl('/login');
          
          },
        },
      ],
    });
    alert.present();


  }
}
