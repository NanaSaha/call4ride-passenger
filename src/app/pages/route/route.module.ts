

import { RoutePage } from './route.page';


import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";
import { Routes, RouterModule } from "@angular/router";

import { AutocompletePage } from "../autocomplete/autocomplete.page";
import { AutocompletePageModule } from "../autocomplete/autocomplete.module";

import { DriverInfoPage } from "../driver-info/driver-info.page";
import { TripInfoPage } from "../trip-info/trip-info.page";
import { ChatPage } from "../chat/chat.page";

import { RatePage } from "../rate/rate.page";
import { IonicRatingModule } from "ionic4-rating";

const routes: Routes = [
  {
    path: '',
    component: RoutePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IonicRatingModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    RoutePage,

  ],

})
export class RoutePageModule { }





// import { NgModule } from "@angular/core";
// import { CommonModule } from "@angular/common";
// import { FormsModule } from "@angular/forms";
// import { IonicModule } from "@ionic/angular";
// import { Routes, RouterModule } from "@angular/router";

// import { AutocompletePage } from "../autocomplete/autocomplete.page";
// import { AutocompletePageModule } from "../autocomplete/autocomplete.module";

// import { DriverInfoPage } from "../driver-info/driver-info.page";
// import { TripInfoPage } from "../trip-info/trip-info.page";
// import { ChatPage } from "../chat/chat.page";

// import { HomePage } from "./home.page";
// import { RatePage } from "../rate/rate.page";
// import { IonicRatingModule } from "ionic4-rating";

// @NgModule({
//   imports: [
//     CommonModule,
//     FormsModule,
//     IonicModule,
//     IonicRatingModule,
//     RouterModule.forChild([
//       {
//         path: "",
//         component: HomePage,
//       },
//     ]),
//   ],
//   declarations: [
//     HomePage,
//     AutocompletePage,
//     DriverInfoPage,
//     TripInfoPage,
//     ChatPage,
//     RatePage,
//   ],
//   entryComponents: [
//     AutocompletePage,
//     DriverInfoPage,
//     TripInfoPage,
//     ChatPage,
//     RatePage,
//   ],
// })
// export class HomePageModule { }

