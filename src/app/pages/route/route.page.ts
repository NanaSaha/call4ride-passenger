import { Component, OnInit, NgZone } from "@angular/core";
import {
  NavController,
  MenuController,
  ModalController,
  Platform,
  AlertController,
  LoadingController,
} from "@ionic/angular";
import { CallNumber } from "@ionic-native/call-number/ngx";


import {
  GoogleMap,
  GoogleMapOptions,
  GoogleMaps,
  GoogleMapsEvent,
  LatLng,
  ILatLng,
  LatLngBounds,
} from "@ionic-native/google-maps";
import * as firebase from "firebase/app";
import { Storage } from "@ionic/storage";
import { Vibration } from "@ionic-native/vibration/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { OneSignal } from "@ionic-native/onesignal/ngx";
import { timer } from "rxjs";
import { interval } from "rxjs";
import { LanguageService } from "src/app/services/language.service";
import { AuthService } from "src/app/services/auth.service";
import { ActivityService } from "src/app/services/activity.service";
import { SettingsService } from "src/app/services/settings.service";
import { NativeMapContainerService } from "src/app/services/native-map-container.service";
import { GeocoderService } from "src/app/services/geocoder.service";
import { DirectionserviceService } from "src/app/services/directionservice.service";
import { ProfileService } from "src/app/services/profile.service";
import { OnesignalService } from "src/app/services/onesignal.service";
import { PopUpService } from "src/app/services/pop-up.service";
import { EventService } from "src/app/services/event.service";

import { HttpClient } from "@angular/common/http";

import { Router, NavigationExtras } from "@angular/router";

declare var google;

@Component({
  selector: 'app-route',
  templateUrl: './route.page.html',
  styleUrls: ['./route.page.scss'],
})
export class RoutePage implements OnInit {
  userProfile: any;
  notify_ID: string;
  showGps: boolean;
  toggleMore: boolean;
  type: string = "arrow-up";
  cardnumber: string;
  hidelocator: boolean;
  math: any = Math;
  lat: number;
  lng: number;
  picked: boolean;
  currentCar: any;
  startedNavigation: boolean = false;
  destination_lat: any;
  destination_lng: any;
  bookStarted: boolean;
  returningUser: boolean;
  uid: any;
  hideNews: boolean;
  newPrice: number;
  canCancel: boolean;
  driver_connected: boolean;
  refreshedTimes: any = 0;
  notification: boolean;
  carType: any;
  plate: any;
  name: any;
  seat: any;
  rating: any;
  review: any;
  picture: any;
  location: any;
  canShowBars: boolean = true;
  rideSelected: boolean = false;
  public geocoder: any = new google.maps.Geocoder();
  price: number;
  userDestName: any;
  number: any;
  driverLocationName: any;
  connect_change: boolean = true;
  myTimer: any;
  referal: any;
  referalID: any;
  actualPrice: any;
  highPrice: any;
  driverHighestDrivingDistance: any;
  driverFound: boolean = true;
  pickedup: boolean = true;
  droppedoff: boolean = true;
  hasPaid: boolean = true;
  isChecking: boolean = true;
  tracker: any;
  mapTracker: any;
  watchPositionSubscription: Geolocation;
  isDriverEnded: boolean = true;
  canSwoop: boolean = true;
  canEnd: boolean = true;
  destiny: any;
  dest: any = "Your Destination";
  currentID: any;
  rating_positive: any = 0;
  rating_negative: any = 0;
  cartype: any;
  isDone: boolean = true;
  isRoute: boolean = true;
  distance_1: any;
  distance_2: any;
  map2: any;
  tolls: any = 0;
  surcharges: any = 0;
  myData: any = [];
  myData3: any;
  myData2: any;
  hasTimed: boolean = true;
  cancelTimer: any;
  canIncur: boolean = false;
  items: any[];
  time: number;
  mileage: number;
  baseFare: any;
  exp: any;
  expo: any;
  up: any;
  down: any;
  myAlert2: any;
  bull: any;
  myAlert3: any;
  interval: any;
  //timeLeft: number = 120;
  timeLeft: number = 20;
  driver_ID: any;
  newterval: any;
  myID: any;
  date1: any;
  date2: string;
  location2: any;
  destination2: any;
  date3: string;
  newtrval: number;
  myTolls: any[] = [];
  drre: any;
  toller: any = [];
  tollName: any[];
  toll_1: any;
  toll_0: number;
  toll_3: any;
  toll_2: any;
  date: string;
  totTOLLS: number;
  allChat: any = [];
  unturned: boolean;
  randNum: any;
  paymentType: any;
  justHide: boolean = true;
  ranD: number;
  driverGlobalPercentSurcharge: any = 0;
  riderGlobalSurcharge: any = 0;
  driverGlobalFlatSurcharge: any = 0;
  riderGlobalPercentSurcharge: any = 0;
  riderGlobalFlatSurcharge: any = 0;
  riderZipcodeFlatSurcharge: any = 0;
  riderZipcodePercentSurcharge: any = 0;
  driverZipcodeFlatSurcharge: any = 0;
  driverZipcodePercentSurcharge: any = 0;
  mySurch: any = [];
  totalFlatFee: void;
  totalRiderSurcharge: any = 0;
  totalDriverSurcharge: any = 0;
  loc1: any[];
  loc2: any[];
  networkOK: any;
  toller3: any;
  toller2: any;
  AllTOLLs: any;
  gunshot: any;
  tollAMT: number;
  currentTOLLS: any;
  currentTOLLS_PRICES: any;
  r: number;
  g: number;
  result3: number = 0;
  check_tracker_loop: any;
  MilkTOlls: any[];
  result4: any;
  result5: any;
  tollTems: any[] = [];
  recentItems: any[];
  toll_1_value: any = 0;
  allSurcharges: any = 0;
  num: any;
  myg: any[] = [];
  canFit: boolean;
  realPrice: any;
  outofstatecharge: number;
  myPos: any;
  calPos: any;
  modal: HTMLIonModalElement;
  items2: any[];
  testVal;
  myAlert: any;
  driverPhone: any;
  detination_words: any;
  emergenc;
  first_name;
  selectedRadioGroup;
  yesorno: any;
  percentage;
  status;
  map: GoogleMap;
  myVal;
  public marker: any;

  constructor(
    public storage: Storage,
    public http: HttpClient,
    public lp: LanguageService,
    public authProvider: AuthService,
    public One: OneSignal,
    public act: ActivityService,
    public settings: SettingsService,
    public statusBar: StatusBar,
    public loadingCtrl: LoadingController,
    private vibration: Vibration,
    public alert: AlertController,
    public cMap: NativeMapContainerService,
    private call: CallNumber,
    public myGcode: GeocoderService,
    public dProvider: DirectionserviceService,
    public platform: Platform,
    public OneSignal: OnesignalService,
    public modalCtrl: ModalController,
    public menu: MenuController,
    public pop: PopUpService,
    public ph: ProfileService,
    public navCtrl: NavController,
    public eventProvider: EventService,
    public router: Router,
    private zone: NgZone // public callNumber: CallNumber
  ) {
    this.platform.ready().then(() => {
      console.log("I AM IN HOMEPAGE");
      this.WaitForGeolocation();

    });
  }



  ngOnInit() {
    console.log("I AM IN HOMEPAGE NGONIT");
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        console.log("USER IN HOME PAGE:::" + user);

        //this.cMap.loadMap();
        //this.testloadMap();
        //  this.WaitForGeolocation();

      }
    });

  }


  // ionViewDidEnter() {
  //   console.log("Will ENTER");
  //   this.defaultMap()
  // }

  StartTracker() {
    console.log("start tracking..........");
    this.watchPositionSubscription = navigator.geolocation;
    this.mapTracker = this.watchPositionSubscription.watchPosition(
      (position) => {
        if (this.cMap.marker && this.canSwoop) {
          console.log("position callback" + position);
          this.lat = position.coords.latitude;
          this.lng = position.coords.longitude;
          console.log("Updating Your Location ....");

          if (!this.cMap.StopLocUpdate) {
            this.cMap.lat = this.lat;
            this.cMap.lng = this.lng;

            this.cMap.gcode.Reverse_Geocode(this.lat, this.lng, false);
          }

          console.log(this.lat, "LAtitude", this.lng);
          // reset route points

          if (this.cMap.marker)
            this.cMap.marker.setPosition(new LatLng(this.lat, this.lng));

          this.cMap.map
            .animateCamera({
              target: new LatLng(this.lat, this.lng),
              zoom: 15,
              tilt: 0,
              bearing: 0,
              duration: 1000,
            })
            .then(() => { });
        } else {
          console.log("position callback 2" + position);
          this.watchPositionSubscription.clearWatch(this.mapTracker);
        }
      },
      (error) => {
        console.log(error);
      },
      {
        enableHighAccuracy: true,
      }
    );
  }

  WaitForGeolocation() {

    let show_actual_location = interval(3000).subscribe(() => {
      console.log("COORECT LOCATION status" + this.cMap.correctLocationShown);
      if (this.cMap.correctLocationShown == true) {
        console.log("GOTTEN COORECT LOCATION ");
        show_actual_location.unsubscribe();

      }
      else {
        console.log("No COORECT LOCATION -->> KEEP RUNNIN DEFAUL LOCATION");
        this.defaultMap()
        show_actual_location.unsubscribe();
      }
    })


    //A timer to detect if the location has been found.
    let location_tracker_loop = interval(3000).subscribe(() => {
      if (this.cMap.hasShown) {
        location_tracker_loop.unsubscribe();
        this.StartTracker();
        this.showGps = false;
        this.cMap.mapLoadComplete = true;
        console.log("Wait For GEOLOACTION");
      }
    });
  }


  toggle() {
    this.menu.enable(true);
    this.menu.open();
  }



  defaultMap() {

    let lat;
    let lng;
    let zoom;
    lat = 5.614818;
    lng = -0.205874;
    zoom = 14;

    let mapOptions: GoogleMapOptions = {
      camera: {
        target: {
          lat: 5.614818,
          lng: -0.205874,
        },
        zoom: zoom,
        tilt: 0,
      },
    };

    this.map = GoogleMaps.create("map", mapOptions);
    console.log("Default Map is Created");


    console.log({
      lat,
      lng,
    });

    this.lat = lat;
    this.lng = lng;

    const image_icon2 = {
      url: "assets/icon/pin.png",
      size: new google.maps.Size(40, 40),
    };

    // new google.maps.Marker({
    //   map: this.map,
    //   position: {
    //     lat,
    //     lng,
    //   },
    //   icon: image_icon2,
    //   duration: 1000,
    //   easing: "easeOutExpo",
    // });
    console.log('Maps Shown -- ' + JSON.stringify(this.map));

    this.map
      .addMarker({
        title: "pinIcon",
        icon: image_icon2,
        position: {
          lat,
          lng,
        },
      })
      .then((marker) => {
        this.marker = marker;
        console.log("MARKER ADDDED----");

        this.myVal = interval(4000).subscribe(() => {
          this.map
            .addCircle({
              center: {
                lat,
                lng,
              },
              radius: 200,
              strokeColor: "#000000",
              strokeWidth: 2,
              visible: true,
              fillColor: "#737373",
              fillOpacity: 0.2,
            })
            .then((circle) => {

              setTimeout(() => {
                circle.setRadius(100);

              }, 500);
            });
        });
      });


  }


  testloadMap() {

    let lat;
    let lng;
    let zoom;
    lat = 5.614818;
    lng = -0.205874;
    zoom = 14;

    let mapOptions: GoogleMapOptions = {
      camera: {
        target: {
          lat: 5.614818,
          lng: -0.205874,
        },
        zoom: zoom,
        tilt: 0,
      },
    };

    this.map = GoogleMaps.create("map", mapOptions);
    console.log("Map is Created");
    this.map.one(GoogleMapsEvent.MAP_READY).then(() => {
      console.log('Map is ready ROUTES!');

      this.map.getMyLocation({ enableHighAccuracy: true }).then((location) => {

        console.log('GET CURRENT LOCATION ROUTES----!');
        console.log(location.latLng);

        this.cMap.correctLocationShown = true

        this.map
          .moveCamera({
            target: location.latLng,
            zoom: 16,
            tilt: 0,
            bearing: 0,
          })
          .then((distanceApart) => {
            console.log("MOVIN CAMERA");
            this.lat = location.latLng.lat;
            this.lng = location.latLng.lng;

            this.map.setClickable(true);

            const image_icon2 = {
              url: "assets/icon/pin.png",

              size: new google.maps.Size(40, 40),
            };

            this.map
              .addMarker({
                title: "pinIcon",
                icon: image_icon2,
                position: location.latLng,
              })
              .then((marker) => {
                this.marker = marker;

                this.myVal = interval(4000).subscribe(() => {
                  this.map
                    .addCircle({
                      center: location.latLng,
                      radius: 200,
                      strokeColor: "#fbb91d",
                      strokeWidth: 2,
                      visible: true,
                      fillColor: "#737373",
                      fillOpacity: 0.2,
                    })
                    .then((circle) => {

                      setTimeout(() => {
                        circle.setRadius(100);

                      }, 500);
                    });
                });
              });
          });


      }).catch((error) => {
        console.log(error);
        console.log("COUDLNT GET LOCATION-->> ....");

      });
    });
  }


}

