import { Component, OnInit, NgZone } from "@angular/core";
import { NavController } from "@ionic/angular";
import { PopUpService } from "src/app/services/pop-up.service";
import { LanguageService } from "src/app/services/language.service";
import { SettingsService } from "src/app/services/settings.service";
import { ProfileService } from "src/app/services/profile.service";
import { ModalController } from "@ionic/angular";
declare let google;
import { Location } from "@angular/common";

@Component({
  selector: "app-autocomplete",
  templateUrl: "./autocomplete.page.html",
  styleUrls: ["./autocomplete.page.scss"],
})
export class AutocompletePage implements OnInit {
  autocompleteItems;
  autocomplete;
  home: any;
  work: any;
  Lang: any;
  service = new google.maps.places.AutocompleteService();

  constructor(
    public pop: PopUpService,
    public lp: LanguageService,
    public settings: SettingsService,
    private navCtrl: NavController,
    private ph: ProfileService,
    private zone: NgZone,
    public modalController: ModalController,
    public location: Location
  ) {
    this.Lang = this.lp.translate();
    this.autocompleteItems = [];
    // listen for home and work button

    this.autocomplete = {
      query: "",
    };

    console.log("AUTOCOMPETE CALLED -----");
  }

  gotoSetting() {
    this.navCtrl.navigateForward("profile");
  }

  chooseHome() {
    if (this.home == null) {
      this.dismiss();
      this.pop.presentToast(this.lp.translate()[0].home);
    } else {
      this.modalController.dismiss(this.home);
    }
  }

  chooseWork() {
    if (this.work == null) {
      this.dismiss();
      this.pop.presentToast(this.lp.translate()[0].home);
    } else {
      this.modalController.dismiss(this.work);
    }
  }

  dismiss() {
    this.modalController.dismiss();
  }

  async chooseItem(item: any) {
    await this.modalController.dismiss(item);
    this.ph.isHome = true;
  }

  updateSearch() {
    this.ph.isHome = false;
    if (this.autocomplete.query === "") {
      this.autocompleteItems = [];
      return;
    }
    const me = this;
    this.service.getPlacePredictions(
      {
        input: this.autocomplete.query,
        componentRestrictions: { country: ["GH","US"] },
      },
      (predictions, status) => {
        me.autocompleteItems = [];
        console.log(predictions, status);
        me.zone.run(() => {
          if (predictions != null) {
            predictions.forEach((prediction) => {
              me.autocompleteItems.push(prediction.description);
            });
          }
        });
      }
    );
  }
  goBack() {
    this.modalController.dismiss();
  }
  ngOnInit() {
    console.log("AUTOCOMPETE CALLED INIT -----");
  }
}
