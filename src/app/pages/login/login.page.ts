import { Component, OnInit, NgZone, ViewChild, ViewEncapsulation } from "@angular/core";
import {
  Platform,
  LoadingController,
  NavController,
  AlertController,
  MenuController,
} from "@ionic/angular";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { EmailValidator } from "src/validators/email";
import { LanguageService } from "src/app/services/language.service";
import { SettingsService } from "src/app/services/settings.service";
import { ProfileService } from "src/app/services/profile.service";
import { NativeMapContainerService } from "src/app/services/native-map-container.service";
import { AuthService } from "src/app/services/auth.service";
import { environment } from "src/environments/environment";
import firebase from "firebase/app";
import { NavigationExtras } from "@angular/router";
import { IonicSelectableComponent } from "ionic-selectable";
import { PopUpService } from "src/app/services/pop-up.service";

class CountryJson2 {
  public name: string;
  public dial_code: string;
}
@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"],
  // encapsulation: ViewEncapsulation.None,
})
export class LoginPage implements OnInit {
  overall_list: string = "signin";
  public loginForm: FormGroup;
  public signupForm: FormGroup;
  public initState = false;
  phoneNum;
  // ports: Port[];
  port: CountryJson2[];
  loading: Promise<HTMLIonLoadingElement>;
  CountryJson = environment.CountryJson;
  OTP: string = "";
  Code: any;
  PhoneNo: any;
  port2: any;
  CountryCode: any;
  showOTPInput: boolean = false;
  showPhone: boolean = true;
  showEmail: boolean = false;
  showButton: boolean = false;
  // OTPmessage: string =
  //   "An OTP is sent to your number. You should receive it in 15 s";
  // recaptchaVerifier: firebase.auth.RecaptchaVerifier;
  confirmationResult: any;
  showSignup: boolean = false;
  showLogin: boolean = true;

  otp: string;
  showOtpComponent = false;
  @ViewChild("ngOtpInput", { static: false }) ngOtpInput: any;
  config = {
    allowNumbersOnly: true,
    length: 6,
    isPasswordInput: false,
    disableAutoFocus: false,
    placeholder: "*",
    inputStyles: {
      width: "50px",
      height: "50px",
    },
  };
  constructor(
    public pop: PopUpService,
    public navCtrl: NavController,
    public lp: LanguageService,
    public settings: SettingsService,
    public ntP: NativeMapContainerService,
    public platform: Platform,
    public menu: MenuController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public authProvider: AuthService,
    public ph: ProfileService,
    public formBuilder: FormBuilder,
    private ngZone: NgZone,
  ) {
    // console.log("JSON COMING", this.CountryJson);
    // console.log("DIAL_CODE", this.CountryJson[72].dial_code);

    // this.port2 = "+233";
    // console.log("PORT is", this.port2);
    //this.port2 = this.CountryJson[72].dial_code;
    //console.log("this.port2", this.port2);
    // tslint:disable-next-line: deprecation
    this.menu.enable(false);
    this.loginForm = formBuilder.group({
      email: [
        "",
        Validators.compose([Validators.required, EmailValidator.isValid]),
      ],
      password: [
        "",
        Validators.compose([Validators.minLength(6), Validators.required]),
      ],
    });

    this.signupForm = formBuilder.group({
      email: [
        "",
        Validators.compose([Validators.required, EmailValidator.isValid]),
      ],
      password: [
        "",
        Validators.compose([Validators.minLength(6), Validators.required]),
      ],
    });
    if (this.initState) {
      this.platform.backButton.subscribe(() => {
        navigator["app"].exitApp();
      });
    } else {
      this.platform.backButton.subscribe(() => {
        this.initState = false;
      });
    }
  }

  segmentChanged(ev: any) {
    console.log("Segment changed", ev);
  }

  showPhoneLogin() {
    this.showPhone = true;
    this.showEmail = false;
  }

  showEmailLogin() {
    this.showEmail = true;
    this.showPhone = false;
  }

  // async ionViewDidEnter() {
  //   this.recaptchaVerifier = new firebase.auth.RecaptchaVerifier(
  //     "sign-in-button",
  //     {
  //       size: "invisible",
  //       callback: (response) => { },
  //       "expired-callback": () => { },
  //     }
  //   );
  // }
  // ionViewDidLoad() {
  //   this.recaptchaVerifier = new firebase.auth.RecaptchaVerifier(
  //     "sign-in-button",
  //     {
  //       size: "invisible",
  //       callback: (response) => { },
  //       "expired-callback": () => { },
  //     }
  //   );
  // }

  // countryCodeChange($event) {
  //   console.log("EVENT", $event);
  //   this.CountryCode = $event.detail.value;
  // }

  countryCodeChange(event: {
    component: IonicSelectableComponent;
    value: any;
  }) {
    this.CountryCode = event.value;
    console.log("CountryCode", this.CountryCode.dial_code);
  }

  // Button event after the nmber is entered and button is clicked
  // signinWithPhoneNumber($event) {
  //   // this.navCtrl.navigateRoot("home");
  //   this.CountryCode = "+233"
  //   console.log("country", $event, this.recaptchaVerifier);
  //   console.log("PhoneNo", this.PhoneNo, this.CountryCode);

  //   this.showLoadRefresh().then(() => {
  //     if (this.PhoneNo && this.CountryCode) {
  //       this.authProvider
  //         .signInWithPhoneNumber(
  //           this.recaptchaVerifier,
  //           this.CountryCode + this.PhoneNo
  //         )
  //         .then((success) => {
  //           this.showOtpComponent = true;
  //           this.showPhone = false;
  //           this.showEmail = false;
  //           this.showButton = true;
  //         });
  //     } else {
  //       this.pop.presentToast("Error! SMS couldnt be sent. Please try again");
  //     }
  //   });
  // }

  // async showSuccess() {
  //   const alert = await this.alertCtrl.create({
  //     header: "OTP Successful",
  //     buttons: [
  //       {
  //         text: "Ok",
  //         handler: (res) => {
  //           alert.dismiss();
  //         },
  //       },
  //     ],
  //   });
  //   alert.present();
  // }
  // async OtpVerification() {
  //   const alert = await this.alertCtrl.create({
  //     header: "Enter OTP",
  //     backdropDismiss: false,
  //     inputs: [
  //       {
  //         name: "otp",
  //         type: "text",
  //         placeholder: "Enter your otp",
  //       },
  //     ],
  //     buttons: [
  //       {
  //         text: "Enter",
  //         handler: (res) => {
  //           this.authProvider
  //             .enterVerificationCode(res.otp)
  //             .then((userData) => {
  //               // this.showSuccess();
  //               console.log(userData);
  //             });
  //         },
  //       },
  //     ],
  //   });
  //   await alert.present();
  // }

  // // OTP Code
  // onOtpChange(otp) {
  //   this.otp = otp;
  //   // When all 6 digits are filled, trigger OTP validation method
  //   if (otp.length == 6) {
  //     this.validateOtp(otp);
  //   }
  // }

  // async validateOtp(otp) {
  //   // await this.loadingCtrl.dismiss().then(() => {
  //   this.authProvider.enterVerificationCode(otp).then(async (userData) => {
  //     console.log(userData);
  //     // await this.loadingCtrl.dismiss().then(() => {
  //     this.ph.getUserProfile().on("value", (userProfileSnapshot) => {
  //       console.log("USER PROFILE:: ", userProfileSnapshot.val());
  //       const user_details = userProfileSnapshot.val();
  //       console.log("USER PROFILE user_details:: ", user_details);
  //       if (user_details == null) {
  //         let navigationExtras: NavigationExtras = {
  //           queryParams: {
  //             from_phone: "yes",
  //           },
  //         };
  //         // this.navCtrl.navigateRoot("update-users-info");
  //         this.navCtrl.navigateRoot(["update-users-info"], navigationExtras);
  //       } else {
  //         console.log("Im home");
  //         this.ngZone.run(() => {
  //           this.navCtrl.navigateRoot("home");
  //         });

  //       }
  //     });
  //     // this.showSuccess();
  //     //});
  //     // const loading = await this.loadingCtrl.create();
  //     // await loading.present();
  //   });
  //   // const loading = await this.loadingCtrl.create();
  //   // await loading.present();
  // }

  // checkForGPS() { }

  // async loginUser() {
  //   if (!this.loginForm.valid) {
  //     console.log(this.loginForm.value);
  //   } else {
  //     this.authProvider
  //       .loginUser(this.loginForm.value.email, this.loginForm.value.password)
  //       .then(
  //         async (authData) => {
  //           await this.loadingCtrl.dismiss().then(() => {
  //             this.ph.getUserProfile().on("value", (userProfileSnapshot) => {
  //               console.log("USER PROFILE:: ", userProfileSnapshot.val());
  //               const phone = userProfileSnapshot.val().phone;
  //               console.log("USER PROFILE phone:: ", phone);
  //               if (phone == null) {
                 
  //                 this.navCtrl.navigateRoot("update-users-info");
  //               } else {
  //                 console.log("Im home");
  //                 this.ngZone.run(() => {
  //                   this.navCtrl.navigateRoot("home");
  //                 });
  //               }
  //             });
  //           });
  //         },
  //         async (error) => {
  //           this.loadingCtrl.dismiss().then(async () => {
  //             const alert = await this.alertCtrl.create({
  //               message: error.message,
  //               buttons: [
  //                 {
  //                   text: this.lp.translate()[0].accept,
  //                   role: "cancel",
  //                 },
  //               ],
  //             });
  //             alert.present();
  //           });
  //         }
  //       );
  //     const loading = await this.loadingCtrl.create();
  //     await loading.present();
  //   }
  // }

  // async signupUser() {
  //   if (!this.signupForm.valid) {
  //     console.log(this.signupForm.value);
  //   } else {
  //     this.authProvider
  //       .signupUser(this.signupForm.value.email, this.signupForm.value.password)
  //       .then(
  //         async () => {
  //           await this.loadingCtrl.dismiss().then(() => {
  //             if (this.ph.phone == null) {
  //               // this.navCtrl.navigateRoot("phone");
  //               this.navCtrl.navigateRoot("update-users-info");
  //             } else {
  //               this.navCtrl.navigateRoot("");
  //             }
  //           });
  //         },
  //         async (error) => {
  //           this.loadingCtrl.dismiss().then(async () => {
  //             const alert = await this.alertCtrl.create({
  //               message: error.message,
  //               buttons: [
  //                 {
  //                   text: this.lp.translate()[0].accept,
  //                   role: "cancel",
  //                 },
  //               ],
  //             });
  //             alert.present();
  //           });
  //         }
  //       );
  //     const loading = await this.loadingCtrl.create();
  //     await loading.present();
  //   }
  // }



  async onSubmit() {
    if (!this.loginForm.valid) {
      console.log(this.loginForm.value);
      const alert = await this.alertCtrl.create({
        message: "Please enter valid email or password",
        buttons: [
          {
            text: "Cancel",
            role: "cancel",
          },
        ],
      });
      alert.present();
    } else {
      this.authProvider
        .loginUser(this.loginForm.value.email, this.loginForm.value.password)
        .then(
          () => {
            this.loadingCtrl.dismiss().then(() => {
              console.log("ABOUT TO TRUN HOOMMMe--->>")


            });
          },
          (error) => {
            this.loadingCtrl.dismiss().then(async () => {
              const alert = await this.alertCtrl.create({
                message: error.message,
                buttons: [
                  {
                    text: "Cancel",
                    role: "cancel",
                  },
                ],
              });
              alert.present();
            });
          }
        );
      const loading = await this.loadingCtrl.create();
      loading.present();
    }
  }


  async signupUser() {
    if (!this.signupForm.valid) {
      console.log(this.signupForm.value);
      const alert = await this.alertCtrl.create({
        message: "Please enter valid email or password",
        buttons: [
          {
            text: "Cancel",
            role: "cancel",
          },
        ],
      });
      alert.present();
    } else {
      this.authProvider
        .signupUser(this.signupForm.value.email, this.signupForm.value.password)
        .then(
          () => {
            this.loadingCtrl.dismiss().then(() => {
              console.log("ABOUT TO TRUN SIGNUP MORE--->>")
              // this.navCtrl.navigateForward("more-info");

            });
          },
          (error) => {
            this.loadingCtrl.dismiss().then(async () => {
              const alert = await this.alertCtrl.create({
                message: error.message,
                buttons: [
                  {
                    text: "Cancel",
                    role: "cancel",
                  },
                ],
              });
              alert.present();
            });
          }
        );
      const loading = await this.loadingCtrl.create();
      loading.present();
    }
  }

  signuphere() {
    this.showSignup = true;
    this.showLogin = false;
  }

  loginhere() {
    this.showSignup = false;
    this.showLogin = true;
  }

  goToBack(): void {
    this.navCtrl.navigateRoot("login-entrance");
  }

  goToSignup(): void {
    this.navCtrl.navigateRoot("entrance");
  }

  goToResetPassword(): void {
    this.navCtrl.navigateRoot("reset-password");
  }

  async showLoadRefresh() {
    const loading = await this.loadingCtrl.create({});
    await loading.present().then(() => {
      let myTimeout = setTimeout(() => {
        clearTimeout(myTimeout);
        loading.dismiss();
      }, 400);
    });
  }

  ngOnInit() { }
}
