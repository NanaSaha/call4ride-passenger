import { Component, ViewChild, NgZone } from "@angular/core";
import {
  Platform,
  ModalController,
  LoadingController,
  NavController,
  AlertController,
  ToastController
} from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { OneSignal } from "@ionic-native/onesignal/ngx";
import { SettingsService } from "./services/settings.service";
import { AuthService } from "./services/auth.service";
import { ProfileService } from "./services/profile.service";
import { LanguageService } from "./services/language.service";
import { NativeMapContainerService } from "./services/native-map-container.service";
import firebase from "firebase";
import { PopUpService } from "./services/pop-up.service";
import { Router } from "@angular/router";
import { Geolocation, PositionError, Geoposition } from "@ionic-native/geolocation/ngx";

@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
  styleUrls: ["app.component.scss"],
})
export class AppComponent {
  public user: any;
  watchPositionSubscription: Geolocation;
  mapTracker: any;

  // for the purpose of effective lazy loading of pages make your rootPage directed at homepage.
  public rootPage: any;
  showSplash = true;
  public userProfile: any;
  phone: any;

  public appPages = [
    {
      title: "Home",
      url: "/home",
      icon: "ios-home",
    },
    {
      title: "Trip History",
      url: "/history",
      icon: "ios-clock",
    },
    {
      title: "Promo Code",
      url: "/referalcode",
      icon: "pricetags",
    },
    {
      title: "Referal Code",
      url: "/promo",
      icon: "ribbon",
    },

    // {
    //   title: "Payment",
    //   url: "/payment",
    //   icon: "ios-card",
    // },

    {
      title: "Support",
      url: "/support",
      icon: "ios-chatbubbles",
    },
    // {
    //   title: "Language",
    //   url: "/settings",
    //   icon: "ios-settings",
    // },


    {
      title: "Call4Ride Info",
      url: "/about",
      icon: "information-circle-outline",
    },
    // {
    //   title: "Home 2",
    //   url: "/route",
    //   icon: "map",
    // },
  ];
  hasHome: any;
  connect: any;
  first_name;
  last_name;
  picture;
  phonenum;
  unique;
  watch;
  email

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    public pop: PopUpService,
    private navCtrl: NavController,
    public statusBar: StatusBar,
    public zone: NgZone,
    public set: SettingsService,
    public cMap: NativeMapContainerService,
    public lp: LanguageService,
    public modalCtrl: ModalController,
    public loadingCtrl: LoadingController,
    private oneSignal: OneSignal,
    public profileServ: ProfileService,
    public alertCtrl: AlertController,
    public auth: AuthService,
    public router: Router,
    private toastCtrl: ToastController,
    private geo: Geolocation,
   
  ) {
    this.initializeApp();
  }

  initializeApp() {
    ///initialize onesignal notification here

    this.platform.ready().then(() => {
      // this.foregroundService.start('Call4Ride Rider', 'Requesting A Ride', 'ic_launcher');

      // this.powerManagement.dim().then(
      //   res => console.log('Wakelock acquired'),
      //   () => {
      //     console.log('Failed to acquire wakelock');
      //   }
      // );

      // this.powerManagement.acquire().then(
      //   res => console.log('Wakelock acquired'),
      //   () => {
      //     console.log('Failed to acquire wakelock');
      //   }
      // );

      // this.powerManagement.setReleaseOnPause(false).then(
      //   res => console.log('setReleaseOnPause successfully'),
      //   () => {
      //     console.log('Failed to set');
      //   }
      // ); 

      
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.allowLocation();
     
      this.startUp();
     

      if (this.platform.is("cordova")) {
        this.setupPush();
        this.getNotificationPlayerIds()
          .then((ids) => {
            })
          .catch((e) => {
           
          });
      }

      // this.keepScreenAlive();
      // this.backgroundMode.enable();
    });
  }

  setupPush() {
    // I recommend to put these into your environment.ts
    this.oneSignal.startInit(
      "8113f114-14de-4ee6-8ab2-4ec3cfb5a90a",
      "516821551729"
    );

    
    this.oneSignal.inFocusDisplaying(
      this.oneSignal.OSInFocusDisplayOption.None
    );

    // Notifcation was received in general
    this.oneSignal.handleNotificationReceived().subscribe((data) => {
    
      let msg = data.payload.body;
      let title = data.payload.title;
      let additionalData = data.payload.additionalData;
      this.showAlert(title, msg, additionalData.task);
    });

    // Notification was really clicked/opened
    this.oneSignal.handleNotificationOpened().subscribe((data) => {
     
      // Just a note that the data is a different place here!
      let additionalData = data.notification.payload.additionalData;

      this.showAlert(
        "Notification opened",
        "You already read this before",
        additionalData.task
      );
    });

    this.oneSignal.endInit();
  }


  // keepScreenAlive() {

    
  //   this.insomnia.keepAwake()
  //     .then(
  //       () => console.log('KEEPING AWAKE success'),
  //       () => console.log('KEEPING AWAKE error')
  //     );
  // }

  getNotificationPlayerIds() {
    return new Promise((resolve, reject) => {
      if (this.platform.is("cordova")) {
        this.oneSignal
          .getIds()
          .then((ids) => {
            
            resolve(ids);
          })
          .catch((e) => {
            reject(e);
          });
      }
    });
  }

  startUp() {
     const unsubscribe = firebase.auth().onAuthStateChanged((user) => {
      if (!user) {
        // if this is not a user then show entrance scene and hide status bar
        this.navCtrl.navigateRoot("splash");
       
        this.profileServ.login = true;
        unsubscribe();
      } else {
       
        unsubscribe();
        // Check If the connection is okay or bad.
        this.profileServ
          .getUserProfil()
          .child(this.profileServ.id)
          .on("value", (userProfileSnapshot) => {
         

           
            if (userProfileSnapshot.val() == null) {
              this.auth.logoutUser().then(() => {
               
                this.navCtrl.navigateRoot("login");
              });
            }

            this.profileServ
              .getUserProfil()
              .child(this.profileServ.id)
              .on("child_added", (userProfileSnapshot) => {
                

                this.first_name = userProfileSnapshot.val().first_name;
                this.email = userProfileSnapshot.val().email;
                this.phone = userProfileSnapshot.val().phonenumber;
                
            

            
                if (this.phone) {
                  if (this.first_name != null || this.first_name != undefined) {
                    this.navCtrl.navigateForward("home");

                  }
                  else {

                    this.navCtrl.navigateRoot("update-users-info");
                  }

                }
                else {
                  this.navCtrl.navigateRoot("update-users-info");
                }



              });
            this.profileServ
              .getUserProfil()
              .child(this.profileServ.id)
              .off("value");
          });
      }
    });
  }

  menuOpened() {
    this.profileServ
      .getUserProfil()
      .child(this.profileServ.id)
      .on("child_added", (userProfileSnapshot) => {
       

        this.first_name = userProfileSnapshot.val().first_name;
        this.email = userProfileSnapshot.val().email;
        this.unique = userProfileSnapshot.val().unique_number;
       


     });
  }

  menuClosed() { }


  async showAlert(title, msg, task) {
    const alert = await this.alertCtrl.create({
      header: title,
      subHeader: msg,
      buttons: [
        {
          text: `Action: ${task}`,
          handler: () => {
            // E.g: Navigate to a specific screen
          },
        },
      ],
    });
    alert.present();
  }

  goProfile() {
    this.navCtrl.navigateRoot("profile");
  }


  allowLocation() {
   

    this.watch = this.geo.watchPosition({ enableHighAccuracy: true, timeout: 10000 }).subscribe(position => {
      if ((position as Geoposition).coords != undefined) {
        var geoposition = (position as Geoposition);
       


      } else {
        var positionError = (position as PositionError);
      


      }
    });
  }

  async presentToast(message) {
    const toast = await this.toastCtrl.create({
      message,
      duration: 3000,
      position: "bottom",
    });

    toast.onDidDismiss();

    toast.present();
  }
}
