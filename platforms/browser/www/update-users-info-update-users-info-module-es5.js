(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["update-users-info-update-users-info-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/update-users-info/update-users-info.page.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/update-users-info/update-users-info.page.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<ion-content >\n  <div class=\"background\">\n  <img src=\"/assets/img/logo.jpg\" class=\"logo_position\" />\n  <h4 id=\"textLogin\">Complete this registration to ride with Call4Ride</h4>\n\n  <div class=\"container\">\n    <ul class=\"progressbar\">\n      <li [class.active]=\"step == 1\">Personal Info</li>\n\n      <li [class.active]=\"step == 2\">Other Info</li>\n   \n    </ul>\n  </div>\n  <form [formGroup]=\"updateForm\" class=\"padd\">\n    <div *ngIf=\"step == 1\">\n      <ion-item>\n        <ion-label position=\"floating\" style=\"color: #fbb91d !important\">First Name</ion-label>\n        <ion-input\n          type=\"text\"\n          formControlName=\"first_name\"\n          name=\"first_name\"\n          [class.invalid]=\"!updateForm.controls.first_name.valid && (updateForm.controls.first_name.dirty )\"\n        ></ion-input>\n      </ion-item>\n      <p\n        *ngIf=\"!updateForm.controls.first_name.valid  && (updateForm.controls.first_name.dirty )\"\n      >\n        <b style=\"color: rgb(255, 0, 0)\">Please enter first name.</b>\n      </p>\n      <br />\n\n      <ion-item>\n        <ion-label position=\"floating\" style=\"color: #fbb91d !important\">Last Name</ion-label>\n        <ion-input\n          type=\"text\"\n          formControlName=\"last_name\"\n          name=\"last_name\"\n          [class.invalid]=\"!updateForm.controls.last_name.valid && (updateForm.controls.last_name.dirty )\"\n        ></ion-input>\n      </ion-item>\n      <p\n        *ngIf=\"!updateForm.controls.last_name.valid  && (updateForm.controls.last_name.dirty )\"\n      >\n        <b style=\"color: rgb(255, 0, 0)\">Please enter Last Name.</b>\n      </p>\n      <br />\n\n    \n  <br />\n           <!-- <ion-item>\n        <ion-label position=\"floating\" style=\"color: #fbb91d !important\">Email</ion-label>\n        <ion-input\n          type=\"text\"\n          formControlName=\"email\"\n          name=\"email\"\n          \n        ></ion-input>\n      </ion-item> -->\n\n      <ion-item>\n        <ion-label position=\"floating\" style=\"color: #fbb91d !important\"\n          >Phone Number</ion-label\n        >\n        <ion-input\n          type=\"number\"\n            placeholder=\"Enter phone number eg. 0276XXXXXX\"\n          [(ngModel)]=\"phonenumber\"\n          formControlName=\"phonenumber\"\n          [class.invalid]=\"!updateForm.controls.phonenumber.valid && (updateForm.controls.phonenumber.dirty )\"\n        ></ion-input>\n      </ion-item>\n      <p\n        *ngIf=\"!updateForm.controls.phonenumber.valid  && (updateForm.controls.phonenumber.dirty)\"\n      >\n        <b style=\"color: rgb(255, 0, 0)\">Please enter your phone number</b>\n      </p>\n   \n      <br />\n\n  \n\n \n    </div>\n\n    <div *ngIf=\"step == 2\">\n       <div\n    style=\"\n      display: flex;\n      flex-direction: column;\n      justify-content: center;\n      text-align: center;\n    \"\n  >\n    <div (click)=\"choosePic_1()\">\n      <ion-avatar\n        style=\"text-align: center; display: inline-flex !important\"\n        *ngIf=\"captureDataUrl\"\n      >\n        <img id=\"my-pic\" [src]=\"captureDataUrl\" *ngIf=\"captureDataUrl\" />\n      </ion-avatar>\n      <ion-avatar\n        style=\"\n          text-align: center;\n          display: inline-flex;\n          width: 100px;\n          height: 100px !important;\n        \"\n        *ngIf=\"!captureDataUrl\"\n      >\n        <img\n          id=\"my-pic\"\n          *ngIf=\"!captureDataUrl\"\n          src=\"assets/svgs/user-edit-solid.svg\"\n          class=\"img_size\"\n        />\n      </ion-avatar>\n    </div>\n\n  </div>\n         \n    \n\n      <br />\n    </div>\n\n\n    <p class=\"pad\">\n      <ion-row>\n        <ion-col col-6 *ngIf=\"step != 1\">\n       \n\n                <ion-button\n        size=\"large\"\n        color=\"dark\"\n        id=\"mybutton\"\n        expand=\"block\"\n        (click)=\"prev()\"\n      >\n        PREVIOUS <ion-icon name=\"arrow-back\"></ion-icon></ion-button\n      >\n        </ion-col>\n\n        <ion-col col-6 *ngIf=\"step != 2\">\n\n           <ion-button\n        size=\"large\"\n        color=\"primary\"\n        id=\"mybutton\"\n        expand=\"block\"\n        (click)=\"submit()\"\n      >\n        NEXT <ion-icon name=\"arrow-forward\"></ion-icon></ion-button\n      >\n        \n        </ion-col>\n\n        <ion-col col-6 *ngIf=\"step == 2\">\n        \n\n                  <ion-button\n                   icon-end\n              [disabled]=\"!this.updateForm.valid\"\n        size=\"large\"\n        color=\"primary\"\n        id=\"mybutton\"\n        expand=\"block\"\n        (click)=\"updateUser()\"\n      >\n        COMPLETE <ion-icon name=\"checkmark-circle\"></ion-icon></ion-button\n      >\n        </ion-col>\n      </ion-row>\n\n      <br />\n      <br />\n    </p>\n\n    <p>\n      <b style=\"color: rgb(0, 0, 0)\">\n        <p style=\"color: rgb(0, 0, 0)\">\n          Already have an account ?\n          <a tappable (click)=\"login()\" style=\"color: #fbb91d\">Log In</a>\n        </p>\n      </b>\n    </p>\n\n    <p>\n      <b style=\"color: rgb(0, 0, 0)\">\n        <p style=\"color: rgb(0, 0, 0)\">\n          By clicking the button to signup, you're agreeing to our\n          <a tappable style=\"color: #fbb91d\">terms of service</a>\n        </p>\n      </b>\n    </p>\n  </form>\n  </div>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/update-users-info/update-users-info.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/update-users-info/update-users-info.module.ts ***!
  \***************************************************************/
/*! exports provided: UpdateUsersInfoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateUsersInfoPageModule", function() { return UpdateUsersInfoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _update_users_info_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./update-users-info.page */ "./src/app/update-users-info/update-users-info.page.ts");







var routes = [
    {
        path: "",
        component: _update_users_info_page__WEBPACK_IMPORTED_MODULE_6__["UpdateUsersInfoPage"],
    },
];
var UpdateUsersInfoPageModule = /** @class */ (function () {
    function UpdateUsersInfoPageModule() {
    }
    UpdateUsersInfoPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
            ],
            declarations: [_update_users_info_page__WEBPACK_IMPORTED_MODULE_6__["UpdateUsersInfoPage"]],
        })
    ], UpdateUsersInfoPageModule);
    return UpdateUsersInfoPageModule;
}());



/***/ }),

/***/ "./src/app/update-users-info/update-users-info.page.scss":
/*!***************************************************************!*\
  !*** ./src/app/update-users-info/update-users-info.page.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".padd {\n  padding-top: 30%;\n}\n\n#iconeye {\n  color: white;\n}\n\nion-icon {\n  color: #f1f3ee !important;\n}\n\n.pad {\n  text-align: center;\n}\n\n.button.customBtn {\n  width: 100%;\n  text-align: center;\n}\n\n#textLogin {\n  text-align: center;\n  padding-top: 5%;\n  font-weight: 700;\n}\n\n.logo_position {\n  padding-top: 5%;\n  width: 30%;\n}\n\n.button-clear-md-dark {\n  background-color: #171717 !important;\n}\n\n.item-md {\n  height: 100%;\n  background-color: transparent !important;\n}\n\n.item-md.item-block .item-inner {\n  padding-right: 8px;\n  border-bottom: 1px solid #000 !important;\n}\n\n.button-inner {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n  flex-flow: row nowrap;\n  flex-shrink: 0;\n  -webkit-box-align: center;\n  align-items: center;\n  -webkit-box-pack: center;\n  justify-content: center;\n}\n\nion-select {\n  display: -webkit-box;\n  display: flex;\n  overflow: hidden;\n  max-width: 100% !important;\n}\n\n.select-text {\n  overflow: visible !important;\n  -webkit-box-flex: 1;\n  flex: 1;\n  min-width: 21px;\n  font-size: inherit;\n  /* text-overflow: inherit; */\n  white-space: nowrap;\n  /* margin-right: 100%; */\n}\n\n.select-md {\n  padding: 13px 52px 13px 16px !important;\n  color: black !important;\n  /* margin-right: 100%; */\n}\n\n.select-md .select-placeholder {\n  color: black !important;\n  z-index: 122 !important;\n}\n\n.datetime-md .datetime-placeholder {\n  color: black !important;\n  z-index: 122 !important;\n}\n\n.label-md,\n.item-datetime .label-md {\n  color: black !important;\n  z-index: 122 !important;\n}\n\n.label-md {\n  margin: 12px 129px 19px 0px !important;\n}\n\n.background {\n  background-image: url(\"/assets/img/circle.png\");\n  height: 100%;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n  align-content: center;\n  text-align: center;\n}\n\nion-card.card {\n  box-shadow: none;\n  background: none;\n  border-radius: 5px;\n  padding-left: 10px;\n}\n\nion-icon {\n  color: #789527;\n}\n\n@media (min-height: 320px) and (max-height: 600px) and (orientation: portrait) and (-webkit-min-device-pixel-ratio: 2) {\n  .row {\n    padding-top: 0%;\n    text-align: center;\n  }\n}\n\n@media (max-height: 640px) {\n  .row {\n    padding-top: 0%;\n    text-align: center;\n  }\n\n  .card {\n    margin: 0 0px;\n    width: calc(110% - 0px);\n    padding: 14px;\n    box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);\n    border-radius: 8px;\n    text-align: center;\n  }\n}\n\n@media (min-height: 360px) and (orientation: portrait) {\n  .row {\n    padding-top: 0%;\n    text-align: center;\n  }\n\n  .card {\n    margin: 0 0px;\n    width: calc(110% - 0px);\n    padding: 14px;\n    box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);\n    border-radius: 8px;\n    text-align: center;\n  }\n}\n\n@media (min-height: 320px) and (orientation: portrait) {\n  .row {\n    padding-top: 0%;\n    text-align: center;\n  }\n\n  .card {\n    margin: 0 0px;\n    width: calc(110% - 0px);\n    padding: 14px;\n    box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);\n    border-radius: 8px;\n    text-align: center;\n  }\n}\n\n@media (min-height: 480px) and (orientation: portrait) {\n  .row {\n    padding-top: 0%;\n    text-align: center;\n  }\n\n  .card {\n    margin: 0 0px;\n    width: calc(110% - 0px);\n    padding: 14px;\n    box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);\n    border-radius: 8px;\n    text-align: center;\n  }\n}\n\n@media (min-height: 540px) {\n  .row {\n    padding-top: 0%;\n    text-align: center;\n  }\n\n  .card {\n    margin: 0 0px;\n    width: calc(105% - 0px);\n    padding: 14px;\n    box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);\n    border-radius: 8px;\n    text-align: center;\n  }\n}\n\n@media (min-height: 540px) and (min-width: 320px) {\n  .row {\n    padding-top: 0%;\n    text-align: center;\n  }\n\n  .card {\n    margin: 0 0px;\n    width: calc(107% - 0px);\n    padding: 14px;\n    box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);\n    border-radius: 8px;\n    text-align: center;\n  }\n}\n\n@media (max-height: 779px) and (min-width: 203px) {\n  .row {\n    padding-top: 0%;\n    text-align: center;\n  }\n\n  .card {\n    margin: 0 0px;\n    width: calc(115% - 0px);\n    padding: 14px;\n    box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);\n    border-radius: 8px;\n    text-align: center;\n  }\n}\n\n@media (max-height: 1050px) and (min-width: 203px) {\n  .row {\n    padding-top: 0%;\n    text-align: center;\n  }\n\n  .card {\n    margin: 0 0px;\n    width: calc(100% - 0px);\n    padding: 14px;\n    box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);\n    border-radius: 8px;\n    text-align: center;\n  }\n}\n\n@media (max-height: 831px) and (min-width: 203px) {\n  .row {\n    padding-top: 0%;\n    text-align: center;\n  }\n\n  .card {\n    margin: 0 0px;\n    width: calc(110% - 0px);\n    padding: 14px;\n    box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);\n    border-radius: 8px;\n    text-align: center;\n  }\n}\n\n@media (max-height: 752px) and (min-width: 203px) {\n  .padd {\n    padding-top: 30%;\n  }\n}\n\n@media (max-height: 600px) {\n  .row {\n    padding-top: 7%;\n    text-align: center;\n  }\n\n  .card {\n    margin: 0 0px;\n    width: calc(100% - 0px);\n    height: calc(97% - 0px);\n    padding: 14px;\n    box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);\n    border-radius: 6px;\n    text-align: center;\n  }\n}\n\n@media (min-width: 411px) {\n  .logo_position {\n    padding-top: 5%;\n    width: 20%;\n  }\n}\n\n.container {\n  width: 100%;\n}\n\n.progressbar {\n  counter-reset: step;\n  margin-right: -30%;\n}\n\n.progressbar li {\n  list-style-type: none;\n  width: 33%;\n  float: left;\n  font-size: 12px;\n  position: relative;\n  text-align: center;\n  text-transform: uppercase;\n  color: #7d7d7d;\n}\n\n.progressbar li:before {\n  width: 30px;\n  height: 30px;\n  content: counter(step);\n  counter-increment: step;\n  line-height: 30px;\n  border: 2px solid #7d7d7d;\n  display: block;\n  text-align: center;\n  margin: 0 auto 10px auto;\n  border-radius: 50%;\n  background-color: white;\n}\n\n.progressbar li:after {\n  width: 70%;\n  height: 2px;\n  content: \"\";\n  position: absolute;\n  background-color: #7d7d7d;\n  top: 15px;\n  left: -35%;\n  z-index: 0;\n}\n\n.progressbar li:first-child:after {\n  content: none;\n}\n\n.progressbar li.active {\n  color: #fbb91d;\n}\n\n.progressbar li.active:before {\n  border-color: #fbb91d;\n}\n\n.progressbar li.active + li:after {\n  background-color: #fbb91d;\n}\n\n.border1 {\n  background: #efefef;\n  border-radius: 4px;\n  font-size: 1.2rem;\n  line-height: 1.3;\n  margin: 0 auto 40px;\n  max-width: 400px;\n  padding: 15px;\n  position: relative;\n  margin-top: 40%;\n  width: 174%;\n  margin-left: -3%;\n}\n\n.border1 p {\n  margin: 0 0 10px;\n}\n\n.border1 p:last-of-type {\n  margin-bottom: 0;\n}\n\n.border1::after {\n  border-right: 20px solid transparent;\n  border-bottom: 20px solid #efefef;\n  top: -20px;\n  content: \"\";\n  position: absolute;\n  right: 59px;\n}\n\n.border2 {\n  background: #efefef;\n  border-radius: 4px;\n  font-size: 1.2rem;\n  line-height: 1.3;\n  margin: 0 auto 40px;\n  max-width: 400px;\n  padding: 15px;\n  position: relative;\n  margin-top: 40%;\n  width: 174%;\n  margin-left: -75%;\n}\n\n.border2 p {\n  margin: 0 0 10px;\n}\n\n.border2 p:last-of-type {\n  margin-bottom: 0;\n}\n\n.border2::after {\n  border-right: 20px solid transparent;\n  border-bottom: 20px solid #efefef;\n  top: -20px;\n  content: \"\";\n  position: absolute;\n  right: 26px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYWhhL0lvbmljQXBwcy9DNFJpZGUtSU9TLTIwMjQvY2FsbDRyaWRlLXBhc3Nlbmdlci9zcmMvYXBwL3VwZGF0ZS11c2Vycy1pbmZvL3VwZGF0ZS11c2Vycy1pbmZvLnBhZ2Uuc2NzcyIsInNyYy9hcHAvdXBkYXRlLXVzZXJzLWluZm8vdXBkYXRlLXVzZXJzLWluZm8ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQXlEQTtFQUNNLGdCQUFBO0FDeEROOztBRDBERTtFQUNFLFlBQUE7QUN2REo7O0FEMERFO0VBQ0UseUJBQUE7QUN2REo7O0FEeURFO0VBQ0Usa0JBQUE7QUN0REo7O0FEeURFO0VBQ0UsV0FBQTtFQUNBLGtCQUFBO0FDdERKOztBRHlERTtFQUNFLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FDdERKOztBRHlERTtFQUNFLGVBQUE7RUFFQSxVQUFBO0FDdkRKOztBRDBERTtFQUNFLG9DQUFBO0FDdkRKOztBRDJERTtFQUVFLFlBQUE7RUFFQSx3Q0FBQTtBQzFESjs7QURnRUU7RUFDRSxrQkFBQTtFQUNBLHdDQUFBO0FDN0RKOztBRGdFRTtFQUNFLG9CQUFBO0VBR0EsYUFBQTtFQUNBLDhCQUFBO0VBQ0EsNkJBQUE7RUFHQSxxQkFBQTtFQUdBLGNBQUE7RUFDQSx5QkFBQTtFQUdBLG1CQUFBO0VBQ0Esd0JBQUE7RUFHQSx1QkFBQTtBQzdESjs7QURpRUU7RUFDRSxvQkFBQTtFQUdBLGFBQUE7RUFDQSxnQkFBQTtFQUNBLDBCQUFBO0FDOURKOztBRGlFRTtFQUNFLDRCQUFBO0VBQ0EsbUJBQUE7RUFHQSxPQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsNEJBQUE7RUFDQSxtQkFBQTtFQUNBLHdCQUFBO0FDOURKOztBRGlFRTtFQUNFLHVDQUFBO0VBQ0EsdUJBQUE7RUFDQSx3QkFBQTtBQzlESjs7QURpRUU7RUFDRSx1QkFBQTtFQUNBLHVCQUFBO0FDOURKOztBRGlFRTtFQUNFLHVCQUFBO0VBQ0EsdUJBQUE7QUM5REo7O0FEaUVFOztFQUVFLHVCQUFBO0VBQ0EsdUJBQUE7QUM5REo7O0FEaUVFO0VBQ0Usc0NBQUE7QUM5REo7O0FEbUVFO0VBQ0UsK0NBQUE7RUFFQSxZQUFBO0VBQ0EsMkJBQUE7RUFDQSw0QkFBQTtFQUNBLHNCQUFBO0VBQ0EscUJBQUE7RUFDQSxrQkFBQTtBQ2pFSjs7QURvRUU7RUFDRSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQ2pFSjs7QURvRUU7RUFDRSxjQUFBO0FDakVKOztBRHNFRTtFQUNFO0lBQ0UsZUFBQTtJQUNBLGtCQUFBO0VDbkVKO0FBQ0Y7O0FEc0VFO0VBQ0U7SUFDRSxlQUFBO0lBQ0Esa0JBQUE7RUNwRUo7O0VEdUVFO0lBQ0UsYUFBQTtJQUNBLHVCQUFBO0lBQ0EsYUFBQTtJQUNBLDBDQUFBO0lBQ0Esa0JBQUE7SUFDQSxrQkFBQTtFQ3BFSjtBQUNGOztBRHVFRTtFQUNFO0lBQ0UsZUFBQTtJQUNBLGtCQUFBO0VDckVKOztFRHdFRTtJQUNFLGFBQUE7SUFDQSx1QkFBQTtJQUNBLGFBQUE7SUFDQSwwQ0FBQTtJQUNBLGtCQUFBO0lBQ0Esa0JBQUE7RUNyRUo7QUFDRjs7QUR3RUU7RUFDRTtJQUNFLGVBQUE7SUFDQSxrQkFBQTtFQ3RFSjs7RUR5RUU7SUFDRSxhQUFBO0lBQ0EsdUJBQUE7SUFDQSxhQUFBO0lBQ0EsMENBQUE7SUFDQSxrQkFBQTtJQUNBLGtCQUFBO0VDdEVKO0FBQ0Y7O0FEeUVFO0VBQ0U7SUFDRSxlQUFBO0lBQ0Esa0JBQUE7RUN2RUo7O0VEMEVFO0lBQ0UsYUFBQTtJQUNBLHVCQUFBO0lBQ0EsYUFBQTtJQUNBLDBDQUFBO0lBQ0Esa0JBQUE7SUFDQSxrQkFBQTtFQ3ZFSjtBQUNGOztBRDBFRTtFQUNFO0lBQ0UsZUFBQTtJQUNBLGtCQUFBO0VDeEVKOztFRDJFRTtJQUNFLGFBQUE7SUFDQSx1QkFBQTtJQUNBLGFBQUE7SUFDQSwwQ0FBQTtJQUNBLGtCQUFBO0lBQ0Esa0JBQUE7RUN4RUo7QUFDRjs7QUQyRUU7RUFDRTtJQUNFLGVBQUE7SUFDQSxrQkFBQTtFQ3pFSjs7RUQ0RUU7SUFDRSxhQUFBO0lBQ0EsdUJBQUE7SUFDQSxhQUFBO0lBQ0EsMENBQUE7SUFDQSxrQkFBQTtJQUNBLGtCQUFBO0VDekVKO0FBQ0Y7O0FENEVFO0VBQ0U7SUFDRSxlQUFBO0lBQ0Esa0JBQUE7RUMxRUo7O0VENkVFO0lBQ0UsYUFBQTtJQUNBLHVCQUFBO0lBQ0EsYUFBQTtJQUNBLDBDQUFBO0lBQ0Esa0JBQUE7SUFDQSxrQkFBQTtFQzFFSjtBQUNGOztBRDZFRTtFQUNFO0lBQ0UsZUFBQTtJQUNBLGtCQUFBO0VDM0VKOztFRDhFRTtJQUNFLGFBQUE7SUFDQSx1QkFBQTtJQUNBLGFBQUE7SUFDQSwwQ0FBQTtJQUNBLGtCQUFBO0lBQ0Esa0JBQUE7RUMzRUo7QUFDRjs7QUQ4RUU7RUFDRTtJQUNFLGVBQUE7SUFDQSxrQkFBQTtFQzVFSjs7RUQrRUU7SUFDRSxhQUFBO0lBQ0EsdUJBQUE7SUFDQSxhQUFBO0lBQ0EsMENBQUE7SUFDQSxrQkFBQTtJQUNBLGtCQUFBO0VDNUVKO0FBQ0Y7O0FEK0VFO0VBQ0U7SUFDRSxnQkFBQTtFQzdFSjtBQUNGOztBRGdGRTtFQUNFO0lBQ0UsZUFBQTtJQUNBLGtCQUFBO0VDOUVKOztFRGlGRTtJQUNFLGFBQUE7SUFDQSx1QkFBQTtJQUNBLHVCQUFBO0lBQ0EsYUFBQTtJQUNBLDBDQUFBO0lBQ0Esa0JBQUE7SUFDQSxrQkFBQTtFQzlFSjtBQUNGOztBRGlGRTtFQUNFO0lBQ0UsZUFBQTtJQUVBLFVBQUE7RUNoRko7QUFDRjs7QURvRkU7RUFDRSxXQUFBO0FDbEZKOztBRHFGRTtFQUNFLG1CQUFBO0VBQ0Esa0JBQUE7QUNsRko7O0FEb0ZFO0VBQ0UscUJBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSxjQUFBO0FDakZKOztBRG1GRTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLGlCQUFBO0VBQ0EseUJBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSx3QkFBQTtFQUNBLGtCQUFBO0VBQ0EsdUJBQUE7QUNoRko7O0FEbUZFO0VBQ0UsVUFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSx5QkFBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0VBQ0EsVUFBQTtBQ2hGSjs7QURrRkU7RUFDRSxhQUFBO0FDL0VKOztBRGlGRTtFQUNFLGNBQUE7QUM5RUo7O0FEZ0ZFO0VBQ0UscUJBQUE7QUM3RUo7O0FEK0VFO0VBQ0UseUJBQUE7QUM1RUo7O0FEK0VFO0VBQ0UsbUJBQUE7RUFFQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtBQzVFSjs7QUQrRUU7RUFDRSxnQkFBQTtBQzVFSjs7QUQ4RUU7RUFDRSxnQkFBQTtBQzNFSjs7QUQ4RUU7RUFDRSxvQ0FBQTtFQUNBLGlDQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7QUMzRUo7O0FEOEVFO0VBQ0UsbUJBQUE7RUFFQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtBQzNFSjs7QUQ4RUU7RUFDRSxnQkFBQTtBQzNFSjs7QUQ2RUU7RUFDRSxnQkFBQTtBQzFFSjs7QUQ2RUU7RUFDRSxvQ0FBQTtFQUNBLGlDQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7QUMxRUoiLCJmaWxlIjoic3JjL2FwcC91cGRhdGUtdXNlcnMtaW5mby91cGRhdGUtdXNlcnMtaW5mby5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBpb24tY29udGVudCB7XG4vLyAgIGlvbi1pbnB1dCB7XG4vLyAgICAgLS1wYWRkaW5nLXRvcDogMjBweDtcbi8vICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuLy8gICAgIGJvcmRlcjogMXB4IHNvbGlkICNlOWU5ZTk7XG4vLyAgICAgYmFja2dyb3VuZDogI2U5ZTllOTtcbi8vICAgfVxuXG4vLyAgIGlvbi1kYXRldGltZSB7XG4vLyAgICAgYm9yZGVyLXJhZGl1czogMTBweDtcbi8vICAgICBib3JkZXI6IDFweCBzb2xpZCAjZTllOWU5O1xuLy8gICAgIGJhY2tncm91bmQ6ICNlOWU5ZTk7XG4vLyAgIH1cblxuLy8gICBpb24tc2VsZWN0IHtcbi8vICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuLy8gICAgIGJvcmRlcjogMXB4IHNvbGlkICNlOWU5ZTk7XG4vLyAgICAgYmFja2dyb3VuZDogI2U5ZTllOTtcbi8vICAgICBoZWlnaHQ6IDUwcHg7XG4vLyAgIH1cblxuLy8gICBpb24tbGFiZWwge1xuLy8gICAgIGZvbnQtc2l6ZTogMjBweDtcbi8vICAgfVxuLy8gICBpb24taXRlbSB7XG4vLyAgICAgcGFkZGluZy10b3A6IDBweDtcbi8vICAgfVxuLy8gICAucCB7XG4vLyAgICAgcGFkZGluZy10b3A6IDMwcHg7XG4vLyAgIH1cbi8vICAgLnAxIHtcbi8vICAgICBwYWRkaW5nLXRvcDogMjBweDtcbi8vICAgfVxuLy8gICBpb24taXRlbS1kaXZpZGVyIHtcbi8vICAgICAtLXBhZGRpbmctYm90dG9tOiAtMTBweDtcbi8vICAgfVxuXG4vLyAgIC5uYXRpdmUtaW5wdXQuc2MtaW9uLWlucHV0LW1kIHtcbi8vICAgICBtYXJnaW4tbGVmdDogMjBweCAhaW1wb3J0YW50O1xuLy8gICB9XG5cbi8vICAgLmxhYmVsLWZsb2F0aW5nLnNjLWlvbi1sYWJlbC1tZC1oLFxuLy8gICAubGFiZWwtc3RhY2tlZC5zYy1pb24tbGFiZWwtbWQtaCB7XG4vLyAgICAgbWFyZ2luLWJvdHRvbTogMTVweDtcbi8vICAgfVxuXG4vLyAgIGlvbi1pdGVtIHtcbi8vICAgICAtLWJvcmRlci1jb2xvcjogdmFyKC0taW9uLWNvbG9yLWRhbmdlciwgI2ZmZmZmZik7XG4vLyAgICAgLS1ib3JkZXItY29sb3I6IHdoaXRlOyAvLyBkZWZhdWx0IHVuZGVybGluZSBjb2xvclxuLy8gICAgIC0taGlnaGxpZ2h0LWNvbG9yLWludmFsaWQ6IHJnYigyNTUsIDI1NSwgMjU1KTsgLy8gaW52YWxpZCB1bmRlcmxpbmUgY29sb3Jcbi8vICAgICAtLWhpZ2hsaWdodC1jb2xvci12YWxpZDogcmdiKDI1NSwgMjU1LCAyNTUpOyAvLyB2YWxpZCB1bmRlcmxpbmUgY29sb3Jcbi8vICAgfVxuLy8gfVxuXG5cbi8vL05FVyBTSUdOVVAgQ1NTXG5cbi5wYWRkIHtcbiAgICAgIHBhZGRpbmctdG9wOiAzMCU7XG4gICAgfVxuICAjaWNvbmV5ZSB7XG4gICAgY29sb3I6IHdoaXRlO1xuICB9XG5cbiAgaW9uLWljb24ge1xuICAgIGNvbG9yOiAjZjFmM2VlICFpbXBvcnRhbnQ7XG4gIH1cbiAgLnBhZCB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB9XG5cbiAgLmJ1dHRvbi5jdXN0b21CdG4ge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgfVxuXG4gICN0ZXh0TG9naW4ge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBwYWRkaW5nLXRvcDogNSU7XG4gICAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgfVxuXG4gIC5sb2dvX3Bvc2l0aW9uIHtcbiAgICBwYWRkaW5nLXRvcDogNSU7XG4gICAgLy8gaGVpZ2h0OiAxNSU7XG4gICAgd2lkdGg6IDMwJTtcbiAgfVxuXG4gIC5idXR0b24tY2xlYXItbWQtZGFyayB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzE3MTcxNyAhaW1wb3J0YW50O1xuICB9XG5cblxuICAuaXRlbS1tZCB7XG4gICBcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgLy8gYm9yZGVyLXJhZGl1czogNnB4ICFpbXBvcnRhbnQ7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQgIWltcG9ydGFudDtcbiAgICAvLyBtYXJnaW4tbGVmdDogNCUgIWltcG9ydGFudDtcbiAgfVxuXG4gXG5cbiAgLml0ZW0tbWQuaXRlbS1ibG9jayAuaXRlbS1pbm5lciB7XG4gICAgcGFkZGluZy1yaWdodDogOHB4O1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjMDAwICFpbXBvcnRhbnQ7XG4gIH1cblxuICAuYnV0dG9uLWlubmVyIHtcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcbiAgICBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICAtd2Via2l0LWJveC1vcmllbnQ6IGhvcml6b250YWw7XG4gICAgLXdlYmtpdC1ib3gtZGlyZWN0aW9uOiBub3JtYWw7XG4gICAgLXdlYmtpdC1mbGV4LWZsb3c6IHJvdyBub3dyYXA7XG4gICAgLW1zLWZsZXgtZmxvdzogcm93IG5vd3JhcDtcbiAgICBmbGV4LWZsb3c6IHJvdyBub3dyYXA7XG4gICAgLXdlYmtpdC1mbGV4LXNocmluazogMDtcbiAgICAtbXMtZmxleC1uZWdhdGl2ZTogMDtcbiAgICBmbGV4LXNocmluazogMDtcbiAgICAtd2Via2l0LWJveC1hbGlnbjogY2VudGVyO1xuICAgIC13ZWJraXQtYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgLXdlYmtpdC1ib3gtcGFjazogY2VudGVyO1xuICAgIC13ZWJraXQtanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIFxuICB9XG5cbiAgaW9uLXNlbGVjdCB7XG4gICAgZGlzcGxheTogLXdlYmtpdC1ib3g7XG4gICAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICBtYXgtd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgfVxuXG4gIC5zZWxlY3QtdGV4dCB7XG4gICAgb3ZlcmZsb3c6IHZpc2libGUgIWltcG9ydGFudDtcbiAgICAtd2Via2l0LWJveC1mbGV4OiAxO1xuICAgIC13ZWJraXQtZmxleDogMTtcbiAgICAtbXMtZmxleDogMTtcbiAgICBmbGV4OiAxO1xuICAgIG1pbi13aWR0aDogMjFweDtcbiAgICBmb250LXNpemU6IGluaGVyaXQ7XG4gICAgLyogdGV4dC1vdmVyZmxvdzogaW5oZXJpdDsgKi9cbiAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICAgIC8qIG1hcmdpbi1yaWdodDogMTAwJTsgKi9cbiAgfVxuXG4gIC5zZWxlY3QtbWQge1xuICAgIHBhZGRpbmc6IDEzcHggNTJweCAxM3B4IDE2cHggIWltcG9ydGFudDtcbiAgICBjb2xvcjogYmxhY2sgIWltcG9ydGFudDtcbiAgICAvKiBtYXJnaW4tcmlnaHQ6IDEwMCU7ICovXG4gIH1cblxuICAuc2VsZWN0LW1kIC5zZWxlY3QtcGxhY2Vob2xkZXIge1xuICAgIGNvbG9yOiBibGFjayAhaW1wb3J0YW50O1xuICAgIHotaW5kZXg6IDEyMiAhaW1wb3J0YW50O1xuICB9XG5cbiAgLmRhdGV0aW1lLW1kIC5kYXRldGltZS1wbGFjZWhvbGRlciB7XG4gICAgY29sb3I6IGJsYWNrICFpbXBvcnRhbnQ7XG4gICAgei1pbmRleDogMTIyICFpbXBvcnRhbnQ7XG4gIH1cblxuICAubGFiZWwtbWQsXG4gIC5pdGVtLWRhdGV0aW1lIC5sYWJlbC1tZCB7XG4gICAgY29sb3I6IGJsYWNrICFpbXBvcnRhbnQ7XG4gICAgei1pbmRleDogMTIyICFpbXBvcnRhbnQ7XG4gIH1cblxuICAubGFiZWwtbWQge1xuICAgIG1hcmdpbjogMTJweCAxMjlweCAxOXB4IDBweCAhaW1wb3J0YW50O1xuICB9XG5cbiBcblxuICAuYmFja2dyb3VuZCB7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiL2Fzc2V0cy9pbWcvY2lyY2xlLnBuZ1wiKTtcblxuICAgIGhlaWdodDogMTAwJTtcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIH1cblxuICBpb24tY2FyZC5jYXJkIHtcbiAgICBib3gtc2hhZG93OiBub25lO1xuICAgIGJhY2tncm91bmQ6IG5vbmU7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIHBhZGRpbmctbGVmdDogMTBweDtcbiAgfVxuXG4gIGlvbi1pY29uIHtcbiAgICBjb2xvcjogIzc4OTUyNztcbiAgfVxuXG5cblxuICBAbWVkaWEgKG1pbi1oZWlnaHQ6IDMyMHB4KSBhbmQgKG1heC1oZWlnaHQ6IDYwMHB4KSBhbmQgKG9yaWVudGF0aW9uOiBwb3J0cmFpdCkgYW5kICgtd2Via2l0LW1pbi1kZXZpY2UtcGl4ZWwtcmF0aW86IDIpIHtcbiAgICAucm93IHtcbiAgICAgIHBhZGRpbmctdG9wOiAwJTtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB9XG4gIH1cblxuICBAbWVkaWEgKG1heC1oZWlnaHQ6IDY0MHB4KSB7XG4gICAgLnJvdyB7XG4gICAgICBwYWRkaW5nLXRvcDogMCU7XG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgfVxuXG4gICAgLmNhcmQge1xuICAgICAgbWFyZ2luOiAwIDBweDtcbiAgICAgIHdpZHRoOiBjYWxjKDExMCUgLSAwcHgpO1xuICAgICAgcGFkZGluZzogMTRweDtcbiAgICAgIGJveC1zaGFkb3c6IDAgMnB4IDRweCAwIHJnYmEoMCwgMCwgMCwgMC4xKTtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB9XG4gIH1cblxuICBAbWVkaWEgKG1pbi1oZWlnaHQ6IDM2MHB4KSBhbmQgKG9yaWVudGF0aW9uOiBwb3J0cmFpdCkge1xuICAgIC5yb3cge1xuICAgICAgcGFkZGluZy10b3A6IDAlO1xuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIH1cblxuICAgIC5jYXJkIHtcbiAgICAgIG1hcmdpbjogMCAwcHg7XG4gICAgICB3aWR0aDogY2FsYygxMTAlIC0gMHB4KTtcbiAgICAgIHBhZGRpbmc6IDE0cHg7XG4gICAgICBib3gtc2hhZG93OiAwIDJweCA0cHggMCByZ2JhKDAsIDAsIDAsIDAuMSk7XG4gICAgICBib3JkZXItcmFkaXVzOiA4cHg7XG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgfVxuICB9XG5cbiAgQG1lZGlhIChtaW4taGVpZ2h0OiAzMjBweCkgYW5kIChvcmllbnRhdGlvbjogcG9ydHJhaXQpIHtcbiAgICAucm93IHtcbiAgICAgIHBhZGRpbmctdG9wOiAwJTtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB9XG5cbiAgICAuY2FyZCB7XG4gICAgICBtYXJnaW46IDAgMHB4O1xuICAgICAgd2lkdGg6IGNhbGMoMTEwJSAtIDBweCk7XG4gICAgICBwYWRkaW5nOiAxNHB4O1xuICAgICAgYm94LXNoYWRvdzogMCAycHggNHB4IDAgcmdiYSgwLCAwLCAwLCAwLjEpO1xuICAgICAgYm9yZGVyLXJhZGl1czogOHB4O1xuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIH1cbiAgfVxuXG4gIEBtZWRpYSAobWluLWhlaWdodDogNDgwcHgpIGFuZCAob3JpZW50YXRpb246IHBvcnRyYWl0KSB7XG4gICAgLnJvdyB7XG4gICAgICBwYWRkaW5nLXRvcDogMCU7XG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgfVxuXG4gICAgLmNhcmQge1xuICAgICAgbWFyZ2luOiAwIDBweDtcbiAgICAgIHdpZHRoOiBjYWxjKDExMCUgLSAwcHgpO1xuICAgICAgcGFkZGluZzogMTRweDtcbiAgICAgIGJveC1zaGFkb3c6IDAgMnB4IDRweCAwIHJnYmEoMCwgMCwgMCwgMC4xKTtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB9XG4gIH1cblxuICBAbWVkaWEgKG1pbi1oZWlnaHQ6IDU0MHB4KSB7XG4gICAgLnJvdyB7XG4gICAgICBwYWRkaW5nLXRvcDogMCU7XG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgfVxuXG4gICAgLmNhcmQge1xuICAgICAgbWFyZ2luOiAwIDBweDtcbiAgICAgIHdpZHRoOiBjYWxjKDEwNSUgLSAwcHgpO1xuICAgICAgcGFkZGluZzogMTRweDtcbiAgICAgIGJveC1zaGFkb3c6IDAgMnB4IDRweCAwIHJnYmEoMCwgMCwgMCwgMC4xKTtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB9XG4gIH1cblxuICBAbWVkaWEgKG1pbi1oZWlnaHQ6IDU0MHB4KSBhbmQgKG1pbi13aWR0aDogMzIwcHgpIHtcbiAgICAucm93IHtcbiAgICAgIHBhZGRpbmctdG9wOiAwJTtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB9XG5cbiAgICAuY2FyZCB7XG4gICAgICBtYXJnaW46IDAgMHB4O1xuICAgICAgd2lkdGg6IGNhbGMoMTA3JSAtIDBweCk7XG4gICAgICBwYWRkaW5nOiAxNHB4O1xuICAgICAgYm94LXNoYWRvdzogMCAycHggNHB4IDAgcmdiYSgwLCAwLCAwLCAwLjEpO1xuICAgICAgYm9yZGVyLXJhZGl1czogOHB4O1xuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIH1cbiAgfVxuXG4gIEBtZWRpYSAobWF4LWhlaWdodDogNzc5cHgpIGFuZCAobWluLXdpZHRoOiAyMDNweCkge1xuICAgIC5yb3cge1xuICAgICAgcGFkZGluZy10b3A6IDAlO1xuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIH1cblxuICAgIC5jYXJkIHtcbiAgICAgIG1hcmdpbjogMCAwcHg7XG4gICAgICB3aWR0aDogY2FsYygxMTUlIC0gMHB4KTtcbiAgICAgIHBhZGRpbmc6IDE0cHg7XG4gICAgICBib3gtc2hhZG93OiAwIDJweCA0cHggMCByZ2JhKDAsIDAsIDAsIDAuMSk7XG4gICAgICBib3JkZXItcmFkaXVzOiA4cHg7XG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgfVxuICB9XG5cbiAgQG1lZGlhIChtYXgtaGVpZ2h0OiAxMDUwcHgpIGFuZCAobWluLXdpZHRoOiAyMDNweCkge1xuICAgIC5yb3cge1xuICAgICAgcGFkZGluZy10b3A6IDAlO1xuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIH1cblxuICAgIC5jYXJkIHtcbiAgICAgIG1hcmdpbjogMCAwcHg7XG4gICAgICB3aWR0aDogY2FsYygxMDAlIC0gMHB4KTtcbiAgICAgIHBhZGRpbmc6IDE0cHg7XG4gICAgICBib3gtc2hhZG93OiAwIDJweCA0cHggMCByZ2JhKDAsIDAsIDAsIDAuMSk7XG4gICAgICBib3JkZXItcmFkaXVzOiA4cHg7XG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgfVxuICB9XG5cbiAgQG1lZGlhIChtYXgtaGVpZ2h0OiA4MzFweCkgYW5kIChtaW4td2lkdGg6IDIwM3B4KSB7XG4gICAgLnJvdyB7XG4gICAgICBwYWRkaW5nLXRvcDogMCU7XG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgfVxuXG4gICAgLmNhcmQge1xuICAgICAgbWFyZ2luOiAwIDBweDtcbiAgICAgIHdpZHRoOiBjYWxjKDExMCUgLSAwcHgpO1xuICAgICAgcGFkZGluZzogMTRweDtcbiAgICAgIGJveC1zaGFkb3c6IDAgMnB4IDRweCAwIHJnYmEoMCwgMCwgMCwgMC4xKTtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB9XG4gIH1cblxuICBAbWVkaWEgKG1heC1oZWlnaHQ6IDc1MnB4KSBhbmQgKG1pbi13aWR0aDogMjAzcHgpIHtcbiAgICAucGFkZCB7XG4gICAgICBwYWRkaW5nLXRvcDogMzAlO1xuICAgIH1cbiAgfVxuXG4gIEBtZWRpYSAobWF4LWhlaWdodDogNjAwcHgpIHtcbiAgICAucm93IHtcbiAgICAgIHBhZGRpbmctdG9wOiA3JTtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB9XG5cbiAgICAuY2FyZCB7XG4gICAgICBtYXJnaW46IDAgMHB4O1xuICAgICAgd2lkdGg6IGNhbGMoMTAwJSAtIDBweCk7XG4gICAgICBoZWlnaHQ6IGNhbGMoOTclIC0gMHB4KTtcbiAgICAgIHBhZGRpbmc6IDE0cHg7XG4gICAgICBib3gtc2hhZG93OiAwIDJweCA0cHggMCByZ2JhKDAsIDAsIDAsIDAuMSk7XG4gICAgICBib3JkZXItcmFkaXVzOiA2cHg7XG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgfVxuICB9XG5cbiAgQG1lZGlhIChtaW4td2lkdGg6IDQxMXB4KSB7XG4gICAgLmxvZ29fcG9zaXRpb24ge1xuICAgICAgcGFkZGluZy10b3A6IDUlO1xuICAgICAgLy8gaGVpZ2h0OiAyMCU7XG4gICAgICB3aWR0aDogMjAlO1xuICAgIH1cbiAgfVxuXG4gIC8vICAgUFJPR1JFU1MgQkFSXG4gIC5jb250YWluZXIge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIC8vIG1hcmdpbjogMTAwcHggYXV0bztcbiAgfVxuICAucHJvZ3Jlc3NiYXIge1xuICAgIGNvdW50ZXItcmVzZXQ6IHN0ZXA7XG4gICAgbWFyZ2luLXJpZ2h0OiAtMzAlO1xuICB9XG4gIC5wcm9ncmVzc2JhciBsaSB7XG4gICAgbGlzdC1zdHlsZS10eXBlOiBub25lO1xuICAgIHdpZHRoOiAzMyU7XG4gICAgZmxvYXQ6IGxlZnQ7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICBjb2xvcjogIzdkN2Q3ZDtcbiAgfVxuICAucHJvZ3Jlc3NiYXIgbGk6YmVmb3JlIHtcbiAgICB3aWR0aDogMzBweDtcbiAgICBoZWlnaHQ6IDMwcHg7XG4gICAgY29udGVudDogY291bnRlcihzdGVwKTtcbiAgICBjb3VudGVyLWluY3JlbWVudDogc3RlcDtcbiAgICBsaW5lLWhlaWdodDogMzBweDtcbiAgICBib3JkZXI6IDJweCBzb2xpZCAjN2Q3ZDdkO1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBtYXJnaW46IDAgYXV0byAxMHB4IGF1dG87XG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICB9XG5cbiAgLnByb2dyZXNzYmFyIGxpOmFmdGVyIHtcbiAgICB3aWR0aDogNzAlO1xuICAgIGhlaWdodDogMnB4O1xuICAgIGNvbnRlbnQ6IFwiXCI7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICM3ZDdkN2Q7XG4gICAgdG9wOiAxNXB4O1xuICAgIGxlZnQ6IC0zNSU7XG4gICAgei1pbmRleDogMDtcbiAgfVxuICAucHJvZ3Jlc3NiYXIgbGk6Zmlyc3QtY2hpbGQ6YWZ0ZXIge1xuICAgIGNvbnRlbnQ6IG5vbmU7XG4gIH1cbiAgLnByb2dyZXNzYmFyIGxpLmFjdGl2ZSB7XG4gICAgY29sb3I6ICNmYmI5MWQ7XG4gIH1cbiAgLnByb2dyZXNzYmFyIGxpLmFjdGl2ZTpiZWZvcmUge1xuICAgIGJvcmRlci1jb2xvcjogI2ZiYjkxZDtcbiAgfVxuICAucHJvZ3Jlc3NiYXIgbGkuYWN0aXZlICsgbGk6YWZ0ZXIge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmYmI5MWQ7XG4gIH1cblxuICAuYm9yZGVyMSB7XG4gICAgYmFja2dyb3VuZDogI2VmZWZlZjtcbiAgICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDRweDtcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gICAgZm9udC1zaXplOiAxLjJyZW07XG4gICAgbGluZS1oZWlnaHQ6IDEuMztcbiAgICBtYXJnaW46IDAgYXV0byA0MHB4O1xuICAgIG1heC13aWR0aDogNDAwcHg7XG4gICAgcGFkZGluZzogMTVweDtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgbWFyZ2luLXRvcDogNDAlO1xuICAgIHdpZHRoOiAxNzQlO1xuICAgIG1hcmdpbi1sZWZ0OiAtMyU7XG4gIH1cblxuICAuYm9yZGVyMSBwIHtcbiAgICBtYXJnaW46IDAgMCAxMHB4O1xuICB9XG4gIC5ib3JkZXIxIHA6bGFzdC1vZi10eXBlIHtcbiAgICBtYXJnaW4tYm90dG9tOiAwO1xuICB9XG5cbiAgLmJvcmRlcjE6OmFmdGVyIHtcbiAgICBib3JkZXItcmlnaHQ6IDIwcHggc29saWQgdHJhbnNwYXJlbnQ7XG4gICAgYm9yZGVyLWJvdHRvbTogMjBweCBzb2xpZCAjZWZlZmVmO1xuICAgIHRvcDogLTIwcHg7XG4gICAgY29udGVudDogXCJcIjtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgcmlnaHQ6IDU5cHg7XG4gIH1cblxuICAuYm9yZGVyMiB7XG4gICAgYmFja2dyb3VuZDogI2VmZWZlZjtcbiAgICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDRweDtcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gICAgZm9udC1zaXplOiAxLjJyZW07XG4gICAgbGluZS1oZWlnaHQ6IDEuMztcbiAgICBtYXJnaW46IDAgYXV0byA0MHB4O1xuICAgIG1heC13aWR0aDogNDAwcHg7XG4gICAgcGFkZGluZzogMTVweDtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgbWFyZ2luLXRvcDogNDAlO1xuICAgIHdpZHRoOiAxNzQlO1xuICAgIG1hcmdpbi1sZWZ0OiAtNzUlO1xuICB9XG5cbiAgLmJvcmRlcjIgcCB7XG4gICAgbWFyZ2luOiAwIDAgMTBweDtcbiAgfVxuICAuYm9yZGVyMiBwOmxhc3Qtb2YtdHlwZSB7XG4gICAgbWFyZ2luLWJvdHRvbTogMDtcbiAgfVxuXG4gIC5ib3JkZXIyOjphZnRlciB7XG4gICAgYm9yZGVyLXJpZ2h0OiAyMHB4IHNvbGlkIHRyYW5zcGFyZW50O1xuICAgIGJvcmRlci1ib3R0b206IDIwcHggc29saWQgI2VmZWZlZjtcbiAgICB0b3A6IC0yMHB4O1xuICAgIGNvbnRlbnQ6IFwiXCI7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHJpZ2h0OiAyNnB4O1xuICB9XG5cblxuIiwiLnBhZGQge1xuICBwYWRkaW5nLXRvcDogMzAlO1xufVxuXG4jaWNvbmV5ZSB7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuaW9uLWljb24ge1xuICBjb2xvcjogI2YxZjNlZSAhaW1wb3J0YW50O1xufVxuXG4ucGFkIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uYnV0dG9uLmN1c3RvbUJ0biB7XG4gIHdpZHRoOiAxMDAlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbiN0ZXh0TG9naW4ge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHBhZGRpbmctdG9wOiA1JTtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbn1cblxuLmxvZ29fcG9zaXRpb24ge1xuICBwYWRkaW5nLXRvcDogNSU7XG4gIHdpZHRoOiAzMCU7XG59XG5cbi5idXR0b24tY2xlYXItbWQtZGFyayB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMxNzE3MTcgIWltcG9ydGFudDtcbn1cblxuLml0ZW0tbWQge1xuICBoZWlnaHQ6IDEwMCU7XG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50ICFpbXBvcnRhbnQ7XG59XG5cbi5pdGVtLW1kLml0ZW0tYmxvY2sgLml0ZW0taW5uZXIge1xuICBwYWRkaW5nLXJpZ2h0OiA4cHg7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjMDAwICFpbXBvcnRhbnQ7XG59XG5cbi5idXR0b24taW5uZXIge1xuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcbiAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcbiAgZGlzcGxheTogZmxleDtcbiAgLXdlYmtpdC1ib3gtb3JpZW50OiBob3Jpem9udGFsO1xuICAtd2Via2l0LWJveC1kaXJlY3Rpb246IG5vcm1hbDtcbiAgLXdlYmtpdC1mbGV4LWZsb3c6IHJvdyBub3dyYXA7XG4gIC1tcy1mbGV4LWZsb3c6IHJvdyBub3dyYXA7XG4gIGZsZXgtZmxvdzogcm93IG5vd3JhcDtcbiAgLXdlYmtpdC1mbGV4LXNocmluazogMDtcbiAgLW1zLWZsZXgtbmVnYXRpdmU6IDA7XG4gIGZsZXgtc2hyaW5rOiAwO1xuICAtd2Via2l0LWJveC1hbGlnbjogY2VudGVyO1xuICAtd2Via2l0LWFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcbiAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuaW9uLXNlbGVjdCB7XG4gIGRpc3BsYXk6IC13ZWJraXQtYm94O1xuICBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xuICBkaXNwbGF5OiBmbGV4O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBtYXgtd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbn1cblxuLnNlbGVjdC10ZXh0IHtcbiAgb3ZlcmZsb3c6IHZpc2libGUgIWltcG9ydGFudDtcbiAgLXdlYmtpdC1ib3gtZmxleDogMTtcbiAgLXdlYmtpdC1mbGV4OiAxO1xuICAtbXMtZmxleDogMTtcbiAgZmxleDogMTtcbiAgbWluLXdpZHRoOiAyMXB4O1xuICBmb250LXNpemU6IGluaGVyaXQ7XG4gIC8qIHRleHQtb3ZlcmZsb3c6IGluaGVyaXQ7ICovXG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIC8qIG1hcmdpbi1yaWdodDogMTAwJTsgKi9cbn1cblxuLnNlbGVjdC1tZCB7XG4gIHBhZGRpbmc6IDEzcHggNTJweCAxM3B4IDE2cHggIWltcG9ydGFudDtcbiAgY29sb3I6IGJsYWNrICFpbXBvcnRhbnQ7XG4gIC8qIG1hcmdpbi1yaWdodDogMTAwJTsgKi9cbn1cblxuLnNlbGVjdC1tZCAuc2VsZWN0LXBsYWNlaG9sZGVyIHtcbiAgY29sb3I6IGJsYWNrICFpbXBvcnRhbnQ7XG4gIHotaW5kZXg6IDEyMiAhaW1wb3J0YW50O1xufVxuXG4uZGF0ZXRpbWUtbWQgLmRhdGV0aW1lLXBsYWNlaG9sZGVyIHtcbiAgY29sb3I6IGJsYWNrICFpbXBvcnRhbnQ7XG4gIHotaW5kZXg6IDEyMiAhaW1wb3J0YW50O1xufVxuXG4ubGFiZWwtbWQsXG4uaXRlbS1kYXRldGltZSAubGFiZWwtbWQge1xuICBjb2xvcjogYmxhY2sgIWltcG9ydGFudDtcbiAgei1pbmRleDogMTIyICFpbXBvcnRhbnQ7XG59XG5cbi5sYWJlbC1tZCB7XG4gIG1hcmdpbjogMTJweCAxMjlweCAxOXB4IDBweCAhaW1wb3J0YW50O1xufVxuXG4uYmFja2dyb3VuZCB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi9hc3NldHMvaW1nL2NpcmNsZS5wbmdcIik7XG4gIGhlaWdodDogMTAwJTtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuaW9uLWNhcmQuY2FyZCB7XG4gIGJveC1zaGFkb3c6IG5vbmU7XG4gIGJhY2tncm91bmQ6IG5vbmU7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgcGFkZGluZy1sZWZ0OiAxMHB4O1xufVxuXG5pb24taWNvbiB7XG4gIGNvbG9yOiAjNzg5NTI3O1xufVxuXG5AbWVkaWEgKG1pbi1oZWlnaHQ6IDMyMHB4KSBhbmQgKG1heC1oZWlnaHQ6IDYwMHB4KSBhbmQgKG9yaWVudGF0aW9uOiBwb3J0cmFpdCkgYW5kICgtd2Via2l0LW1pbi1kZXZpY2UtcGl4ZWwtcmF0aW86IDIpIHtcbiAgLnJvdyB7XG4gICAgcGFkZGluZy10b3A6IDAlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgfVxufVxuQG1lZGlhIChtYXgtaGVpZ2h0OiA2NDBweCkge1xuICAucm93IHtcbiAgICBwYWRkaW5nLXRvcDogMCU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB9XG5cbiAgLmNhcmQge1xuICAgIG1hcmdpbjogMCAwcHg7XG4gICAgd2lkdGg6IGNhbGMoMTEwJSAtIDBweCk7XG4gICAgcGFkZGluZzogMTRweDtcbiAgICBib3gtc2hhZG93OiAwIDJweCA0cHggMCByZ2JhKDAsIDAsIDAsIDAuMSk7XG4gICAgYm9yZGVyLXJhZGl1czogOHB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgfVxufVxuQG1lZGlhIChtaW4taGVpZ2h0OiAzNjBweCkgYW5kIChvcmllbnRhdGlvbjogcG9ydHJhaXQpIHtcbiAgLnJvdyB7XG4gICAgcGFkZGluZy10b3A6IDAlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgfVxuXG4gIC5jYXJkIHtcbiAgICBtYXJnaW46IDAgMHB4O1xuICAgIHdpZHRoOiBjYWxjKDExMCUgLSAwcHgpO1xuICAgIHBhZGRpbmc6IDE0cHg7XG4gICAgYm94LXNoYWRvdzogMCAycHggNHB4IDAgcmdiYSgwLCAwLCAwLCAwLjEpO1xuICAgIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIH1cbn1cbkBtZWRpYSAobWluLWhlaWdodDogMzIwcHgpIGFuZCAob3JpZW50YXRpb246IHBvcnRyYWl0KSB7XG4gIC5yb3cge1xuICAgIHBhZGRpbmctdG9wOiAwJTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIH1cblxuICAuY2FyZCB7XG4gICAgbWFyZ2luOiAwIDBweDtcbiAgICB3aWR0aDogY2FsYygxMTAlIC0gMHB4KTtcbiAgICBwYWRkaW5nOiAxNHB4O1xuICAgIGJveC1zaGFkb3c6IDAgMnB4IDRweCAwIHJnYmEoMCwgMCwgMCwgMC4xKTtcbiAgICBib3JkZXItcmFkaXVzOiA4cHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB9XG59XG5AbWVkaWEgKG1pbi1oZWlnaHQ6IDQ4MHB4KSBhbmQgKG9yaWVudGF0aW9uOiBwb3J0cmFpdCkge1xuICAucm93IHtcbiAgICBwYWRkaW5nLXRvcDogMCU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB9XG5cbiAgLmNhcmQge1xuICAgIG1hcmdpbjogMCAwcHg7XG4gICAgd2lkdGg6IGNhbGMoMTEwJSAtIDBweCk7XG4gICAgcGFkZGluZzogMTRweDtcbiAgICBib3gtc2hhZG93OiAwIDJweCA0cHggMCByZ2JhKDAsIDAsIDAsIDAuMSk7XG4gICAgYm9yZGVyLXJhZGl1czogOHB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgfVxufVxuQG1lZGlhIChtaW4taGVpZ2h0OiA1NDBweCkge1xuICAucm93IHtcbiAgICBwYWRkaW5nLXRvcDogMCU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB9XG5cbiAgLmNhcmQge1xuICAgIG1hcmdpbjogMCAwcHg7XG4gICAgd2lkdGg6IGNhbGMoMTA1JSAtIDBweCk7XG4gICAgcGFkZGluZzogMTRweDtcbiAgICBib3gtc2hhZG93OiAwIDJweCA0cHggMCByZ2JhKDAsIDAsIDAsIDAuMSk7XG4gICAgYm9yZGVyLXJhZGl1czogOHB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgfVxufVxuQG1lZGlhIChtaW4taGVpZ2h0OiA1NDBweCkgYW5kIChtaW4td2lkdGg6IDMyMHB4KSB7XG4gIC5yb3cge1xuICAgIHBhZGRpbmctdG9wOiAwJTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIH1cblxuICAuY2FyZCB7XG4gICAgbWFyZ2luOiAwIDBweDtcbiAgICB3aWR0aDogY2FsYygxMDclIC0gMHB4KTtcbiAgICBwYWRkaW5nOiAxNHB4O1xuICAgIGJveC1zaGFkb3c6IDAgMnB4IDRweCAwIHJnYmEoMCwgMCwgMCwgMC4xKTtcbiAgICBib3JkZXItcmFkaXVzOiA4cHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB9XG59XG5AbWVkaWEgKG1heC1oZWlnaHQ6IDc3OXB4KSBhbmQgKG1pbi13aWR0aDogMjAzcHgpIHtcbiAgLnJvdyB7XG4gICAgcGFkZGluZy10b3A6IDAlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgfVxuXG4gIC5jYXJkIHtcbiAgICBtYXJnaW46IDAgMHB4O1xuICAgIHdpZHRoOiBjYWxjKDExNSUgLSAwcHgpO1xuICAgIHBhZGRpbmc6IDE0cHg7XG4gICAgYm94LXNoYWRvdzogMCAycHggNHB4IDAgcmdiYSgwLCAwLCAwLCAwLjEpO1xuICAgIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIH1cbn1cbkBtZWRpYSAobWF4LWhlaWdodDogMTA1MHB4KSBhbmQgKG1pbi13aWR0aDogMjAzcHgpIHtcbiAgLnJvdyB7XG4gICAgcGFkZGluZy10b3A6IDAlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgfVxuXG4gIC5jYXJkIHtcbiAgICBtYXJnaW46IDAgMHB4O1xuICAgIHdpZHRoOiBjYWxjKDEwMCUgLSAwcHgpO1xuICAgIHBhZGRpbmc6IDE0cHg7XG4gICAgYm94LXNoYWRvdzogMCAycHggNHB4IDAgcmdiYSgwLCAwLCAwLCAwLjEpO1xuICAgIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIH1cbn1cbkBtZWRpYSAobWF4LWhlaWdodDogODMxcHgpIGFuZCAobWluLXdpZHRoOiAyMDNweCkge1xuICAucm93IHtcbiAgICBwYWRkaW5nLXRvcDogMCU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB9XG5cbiAgLmNhcmQge1xuICAgIG1hcmdpbjogMCAwcHg7XG4gICAgd2lkdGg6IGNhbGMoMTEwJSAtIDBweCk7XG4gICAgcGFkZGluZzogMTRweDtcbiAgICBib3gtc2hhZG93OiAwIDJweCA0cHggMCByZ2JhKDAsIDAsIDAsIDAuMSk7XG4gICAgYm9yZGVyLXJhZGl1czogOHB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgfVxufVxuQG1lZGlhIChtYXgtaGVpZ2h0OiA3NTJweCkgYW5kIChtaW4td2lkdGg6IDIwM3B4KSB7XG4gIC5wYWRkIHtcbiAgICBwYWRkaW5nLXRvcDogMzAlO1xuICB9XG59XG5AbWVkaWEgKG1heC1oZWlnaHQ6IDYwMHB4KSB7XG4gIC5yb3cge1xuICAgIHBhZGRpbmctdG9wOiA3JTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIH1cblxuICAuY2FyZCB7XG4gICAgbWFyZ2luOiAwIDBweDtcbiAgICB3aWR0aDogY2FsYygxMDAlIC0gMHB4KTtcbiAgICBoZWlnaHQ6IGNhbGMoOTclIC0gMHB4KTtcbiAgICBwYWRkaW5nOiAxNHB4O1xuICAgIGJveC1zaGFkb3c6IDAgMnB4IDRweCAwIHJnYmEoMCwgMCwgMCwgMC4xKTtcbiAgICBib3JkZXItcmFkaXVzOiA2cHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogNDExcHgpIHtcbiAgLmxvZ29fcG9zaXRpb24ge1xuICAgIHBhZGRpbmctdG9wOiA1JTtcbiAgICB3aWR0aDogMjAlO1xuICB9XG59XG4uY29udGFpbmVyIHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5wcm9ncmVzc2JhciB7XG4gIGNvdW50ZXItcmVzZXQ6IHN0ZXA7XG4gIG1hcmdpbi1yaWdodDogLTMwJTtcbn1cblxuLnByb2dyZXNzYmFyIGxpIHtcbiAgbGlzdC1zdHlsZS10eXBlOiBub25lO1xuICB3aWR0aDogMzMlO1xuICBmbG9hdDogbGVmdDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgY29sb3I6ICM3ZDdkN2Q7XG59XG5cbi5wcm9ncmVzc2JhciBsaTpiZWZvcmUge1xuICB3aWR0aDogMzBweDtcbiAgaGVpZ2h0OiAzMHB4O1xuICBjb250ZW50OiBjb3VudGVyKHN0ZXApO1xuICBjb3VudGVyLWluY3JlbWVudDogc3RlcDtcbiAgbGluZS1oZWlnaHQ6IDMwcHg7XG4gIGJvcmRlcjogMnB4IHNvbGlkICM3ZDdkN2Q7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbjogMCBhdXRvIDEwcHggYXV0bztcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbn1cblxuLnByb2dyZXNzYmFyIGxpOmFmdGVyIHtcbiAgd2lkdGg6IDcwJTtcbiAgaGVpZ2h0OiAycHg7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzdkN2Q3ZDtcbiAgdG9wOiAxNXB4O1xuICBsZWZ0OiAtMzUlO1xuICB6LWluZGV4OiAwO1xufVxuXG4ucHJvZ3Jlc3NiYXIgbGk6Zmlyc3QtY2hpbGQ6YWZ0ZXIge1xuICBjb250ZW50OiBub25lO1xufVxuXG4ucHJvZ3Jlc3NiYXIgbGkuYWN0aXZlIHtcbiAgY29sb3I6ICNmYmI5MWQ7XG59XG5cbi5wcm9ncmVzc2JhciBsaS5hY3RpdmU6YmVmb3JlIHtcbiAgYm9yZGVyLWNvbG9yOiAjZmJiOTFkO1xufVxuXG4ucHJvZ3Jlc3NiYXIgbGkuYWN0aXZlICsgbGk6YWZ0ZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmJiOTFkO1xufVxuXG4uYm9yZGVyMSB7XG4gIGJhY2tncm91bmQ6ICNlZmVmZWY7XG4gIC13ZWJraXQtYm9yZGVyLXJhZGl1czogNHB4O1xuICBib3JkZXItcmFkaXVzOiA0cHg7XG4gIGZvbnQtc2l6ZTogMS4ycmVtO1xuICBsaW5lLWhlaWdodDogMS4zO1xuICBtYXJnaW46IDAgYXV0byA0MHB4O1xuICBtYXgtd2lkdGg6IDQwMHB4O1xuICBwYWRkaW5nOiAxNXB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG1hcmdpbi10b3A6IDQwJTtcbiAgd2lkdGg6IDE3NCU7XG4gIG1hcmdpbi1sZWZ0OiAtMyU7XG59XG5cbi5ib3JkZXIxIHAge1xuICBtYXJnaW46IDAgMCAxMHB4O1xufVxuXG4uYm9yZGVyMSBwOmxhc3Qtb2YtdHlwZSB7XG4gIG1hcmdpbi1ib3R0b206IDA7XG59XG5cbi5ib3JkZXIxOjphZnRlciB7XG4gIGJvcmRlci1yaWdodDogMjBweCBzb2xpZCB0cmFuc3BhcmVudDtcbiAgYm9yZGVyLWJvdHRvbTogMjBweCBzb2xpZCAjZWZlZmVmO1xuICB0b3A6IC0yMHB4O1xuICBjb250ZW50OiBcIlwiO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiA1OXB4O1xufVxuXG4uYm9yZGVyMiB7XG4gIGJhY2tncm91bmQ6ICNlZmVmZWY7XG4gIC13ZWJraXQtYm9yZGVyLXJhZGl1czogNHB4O1xuICBib3JkZXItcmFkaXVzOiA0cHg7XG4gIGZvbnQtc2l6ZTogMS4ycmVtO1xuICBsaW5lLWhlaWdodDogMS4zO1xuICBtYXJnaW46IDAgYXV0byA0MHB4O1xuICBtYXgtd2lkdGg6IDQwMHB4O1xuICBwYWRkaW5nOiAxNXB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG1hcmdpbi10b3A6IDQwJTtcbiAgd2lkdGg6IDE3NCU7XG4gIG1hcmdpbi1sZWZ0OiAtNzUlO1xufVxuXG4uYm9yZGVyMiBwIHtcbiAgbWFyZ2luOiAwIDAgMTBweDtcbn1cblxuLmJvcmRlcjIgcDpsYXN0LW9mLXR5cGUge1xuICBtYXJnaW4tYm90dG9tOiAwO1xufVxuXG4uYm9yZGVyMjo6YWZ0ZXIge1xuICBib3JkZXItcmlnaHQ6IDIwcHggc29saWQgdHJhbnNwYXJlbnQ7XG4gIGJvcmRlci1ib3R0b206IDIwcHggc29saWQgI2VmZWZlZjtcbiAgdG9wOiAtMjBweDtcbiAgY29udGVudDogXCJcIjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogMjZweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/update-users-info/update-users-info.page.ts":
/*!*************************************************************!*\
  !*** ./src/app/update-users-info/update-users-info.page.ts ***!
  \*************************************************************/
/*! exports provided: UpdateUsersInfoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateUsersInfoPage", function() { return UpdateUsersInfoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var src_app_services_language_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/language.service */ "./src/app/services/language.service.ts");
/* harmony import */ var src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/settings.service */ "./src/app/services/settings.service.ts");
/* harmony import */ var src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/profile.service */ "./src/app/services/profile.service.ts");
/* harmony import */ var src_app_services_native_map_container_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/native-map-container.service */ "./src/app/services/native-map-container.service.ts");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! src/app/services/pop-up.service */ "./src/app/services/pop-up.service.ts");













var UpdateUsersInfoPage = /** @class */ (function () {
    function UpdateUsersInfoPage(navCtrl, lp, settings, ntP, platform, menu, loadingCtrl, alertCtrl, authProvider, ph, formBuilder, route, actionSheetCtrl, camera, pop, _form) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.lp = lp;
        this.settings = settings;
        this.ntP = ntP;
        this.platform = platform;
        this.menu = menu;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.authProvider = authProvider;
        this.ph = ph;
        this.formBuilder = formBuilder;
        this.route = route;
        this.actionSheetCtrl = actionSheetCtrl;
        this.camera = camera;
        this.pop = pop;
        this._form = _form;
        this.initState = false;
        this.step = 1;
        this.date = new Date();
        this.minSelectabledate = this.formatDate(this.date);
        this.maxSelectabledate = this.formatDatemax(this.date);
        this.userProfileRef = firebase__WEBPACK_IMPORTED_MODULE_9___default.a.database().ref("/userProfile");
        this.menu.enable(false);
        this.updateForm = formBuilder.group({
            first_name: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required])],
            last_name: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required])],
            phonenumber: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required])],
        });
        this.route.queryParams.subscribe(function (params) {
            _this.from_phone = params["from_phone"];
            console.log("FROM PHONE", _this.from_phone);
        });
        console.log("PROFILE ID::", this.ph.id);
        var unsubscribe = firebase__WEBPACK_IMPORTED_MODULE_9___default.a.auth().onAuthStateChanged(function (user) {
            _this.ph
                .getUserProfil()
                .child(_this.ph.id)
                .on("value", function (userProfileSnapshot) {
                console.log("USER::", user);
                _this.email = user.email;
                console.log("USER PROFILE SNAPSHOT::", userProfileSnapshot.val());
                console.log("PROFILE ID::", _this.ph.id);
            });
        });
    }
    UpdateUsersInfoPage.prototype.ngOnInit = function () { };
    UpdateUsersInfoPage.prototype.updateUser = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var email, phonenumber, first_name, last_name, currentYear, unique;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                if (!this.updateForm.valid) {
                    console.log(this.updateForm.value);
                }
                else {
                    email = this.updateForm.value.email;
                    phonenumber = this.updateForm.value.phonenumber;
                    first_name = this.updateForm.value.first_name;
                    last_name = this.updateForm.value.last_name;
                    console.log("FIRST NAME" + first_name);
                    currentYear = new Date().getFullYear();
                    unique = "GH" + currentYear + "R" + Math.floor(1000 + Math.random() * 9000);
                    console.log("UPDATE FORM VALUES", this.updateForm.value);
                    console.log("User ID::", this.ph.id);
                    console.log("UNIQGUE NUMBER-->", unique);
                    this.userProfileRef.child(this.ph.id).child("userInfo").update({
                        first_name: first_name,
                        last_name: last_name,
                        email: this.email,
                        phonenumber: phonenumber,
                        unique_number: unique,
                    });
                    this.navCtrl.navigateRoot("home");
                }
                return [2 /*return*/];
            });
        });
    };
    UpdateUsersInfoPage.prototype.goToBack = function () {
        this.navCtrl.navigateRoot("login");
    };
    UpdateUsersInfoPage.prototype.submit = function () {
        this.step = this.step + 1;
        console.log("STEP NOW IS", this.step);
    };
    UpdateUsersInfoPage.prototype.prev = function () {
        this.step = this.step - 1;
        console.log("STEP NOW IS", this.step);
    };
    UpdateUsersInfoPage.prototype.login = function () {
        this.navCtrl.navigateRoot("login");
    };
    UpdateUsersInfoPage.prototype.formatDate = function (date) {
        var d = new Date(date), month = "" + (d.getMonth() + 1), day = "" + d.getDate(), year = d.getFullYear();
        // console.log("year" + year + "and day = " + day);
        if (month.length < 2)
            month = "0" + month;
        if (day.length < 2)
            day = "0" + day;
        console.log("year" + year + "and day = " + day);
        return [year, month, day].join("-");
    };
    UpdateUsersInfoPage.prototype.formatDatemax = function (date) {
        var d = new Date(date), month = "" + d.getMonth(), day = "" + d.getDate(), year = d.getFullYear() + 1;
        console.log("year" + year + "and day = " + day);
        if (month.length < 2)
            month = "0" + month;
        if (day.length < 2)
            day = "0" + day;
        console.log("year" + year + "and day = " + day);
        return [year, month, day].join("-");
    };
    UpdateUsersInfoPage.prototype.choosePic_1 = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var actionSheet;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.actionSheetCtrl.create({
                            header: "Choose From",
                            buttons: [
                                {
                                    text: "Camera",
                                    icon: "ios-camera",
                                    handler: function () {
                                        _this.capture();
                                    },
                                },
                                {
                                    text: "File",
                                    icon: "ios-folder",
                                    handler: function () {
                                        _this.captureFromFile();
                                    },
                                },
                                {
                                    text: "Cancel",
                                    icon: "close",
                                    role: "cancel",
                                    handler: function () {
                                        console.log("Cancel clicked");
                                    },
                                },
                            ],
                        })];
                    case 1:
                        actionSheet = _a.sent();
                        actionSheet.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    UpdateUsersInfoPage.prototype.capture = function () {
        var _this = this;
        var options = {
            quality: 40,
            sourceType: this.camera.PictureSourceType.CAMERA,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
        };
        this.camera.getPicture(options).then(function (imageData) {
            _this.captureDataUrl = "data:image/jpeg;base64," + imageData;
            _this.processProfilePicture(_this.captureDataUrl);
        }, function (err) {
            // Handle error
        });
    };
    UpdateUsersInfoPage.prototype.captureFromFile = function () {
        var _this = this;
        var options = {
            quality: 40,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
        };
        this.camera.getPicture(options).then(function (imageData) {
            _this.captureDataUrl = "data:image/jpeg;base64," + imageData;
            _this.processProfilePicture(_this.captureDataUrl);
        });
    };
    UpdateUsersInfoPage.prototype.processProfilePicture = function (captureData) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var storageRef, filename, loading, imageRef;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        storageRef = firebase__WEBPACK_IMPORTED_MODULE_9___default.a.storage().ref();
                        filename = Math.floor(Date.now() / 1000);
                        return [4 /*yield*/, this.loadingCtrl.create()];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        imageRef = storageRef.child("userPictures/" + filename + ".jpg");
                        imageRef
                            .putString(captureData, firebase__WEBPACK_IMPORTED_MODULE_9___default.a.storage.StringFormat.DATA_URL)
                            .then(function (snapshot) {
                            imageRef
                                .getDownloadURL()
                                .then(function (url) {
                                console.log(url);
                                _this.ph
                                    .UpdatePhoto(url)
                                    .then(function (success) {
                                    console.log(url);
                                    loading.dismiss();
                                    console.log("done");
                                    // this.navCtrl.navigateRoot("home");
                                })
                                    .catch(function (error) {
                                    _this.pop.presentToast("Check Your Internet Connection and try again");
                                });
                            })
                                .catch(function (error) {
                                _this.pop.presentToast("Check Your Internet Connection and try again");
                            });
                        })
                            .catch(function (error) {
                            _this.pop.presentToast("Check Your Internet Connection and try again");
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    UpdateUsersInfoPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
        { type: src_app_services_language_service__WEBPACK_IMPORTED_MODULE_4__["LanguageService"] },
        { type: src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_5__["SettingsService"] },
        { type: src_app_services_native_map_container_service__WEBPACK_IMPORTED_MODULE_7__["NativeMapContainerService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
        { type: src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_8__["AuthService"] },
        { type: src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_6__["ProfileService"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_10__["ActivatedRoute"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"] },
        { type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_11__["Camera"] },
        { type: src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_12__["PopUpService"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] }
    ]; };
    UpdateUsersInfoPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-update-users-info",
            template: __webpack_require__(/*! raw-loader!./update-users-info.page.html */ "./node_modules/raw-loader/index.js!./src/app/update-users-info/update-users-info.page.html"),
            styles: [__webpack_require__(/*! ./update-users-info.page.scss */ "./src/app/update-users-info/update-users-info.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
            src_app_services_language_service__WEBPACK_IMPORTED_MODULE_4__["LanguageService"],
            src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_5__["SettingsService"],
            src_app_services_native_map_container_service__WEBPACK_IMPORTED_MODULE_7__["NativeMapContainerService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_8__["AuthService"],
            src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_6__["ProfileService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_10__["ActivatedRoute"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"],
            _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_11__["Camera"],
            src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_12__["PopUpService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"]])
    ], UpdateUsersInfoPage);
    return UpdateUsersInfoPage;
}());



/***/ })

}]);
//# sourceMappingURL=update-users-info-update-users-info-module-es5.js.map