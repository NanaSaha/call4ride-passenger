(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-phone-phone-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/phone/phone.page.html":
/*!***********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/phone/phone.page.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--\n  Generated template for the AccountkitPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n<ion-content padding>\n  <div class=\"o_section\" *ngIf=\"!requestCode\">\n    <ion-grid>\n      <ion-row>\n        <ion-col size=\"2\">\n          <ion-fab-button\n            style=\"width: 40px; height: 35px\"\n            class=\"padding\"\n            color=\"Menubutton\"\n            color=\"primary\"\n            expand=\"block\"\n            (click)=\"Country()\"\n          >\n            +233\n          </ion-fab-button>\n        </ion-col>\n        <ion-col size=\"10\">\n          <ion-input\n            class=\"input\"\n            type=\"text\"\n            placeholder=\"2321XXXXXXXXX\"\n            [(ngModel)]=\"userName\"\n          ></ion-input>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n    <ion-button\n      class=\"padding\"\n      color=\"primary\"\n      id=\"mybutton\"\n      expand=\"block\"\n      (click)=\"AuthPhone(userName)\"\n      >SUBMIT</ion-button\n    >\n  </div>\n\n  <div class=\"o_section\" *ngIf=\"requestCode\">\n    <ion-icon\n      style=\"text-align: center\"\n      class=\"stack\"\n      class=\"padding\"\n      color=\"primary\"\n      position=\"stacked\"\n      >Enter Verification Code</ion-icon\n    >\n    <ion-input\n      class=\"input\"\n      type=\"text\"\n      placeholder=\"00000\"\n      [(ngModel)]=\"userName\"\n    ></ion-input>\n\n    <ion-button\n      class=\"padding\"\n      color=\"gery\"\n      id=\"mybutton\"\n      expand=\"block\"\n      (click)=\"AuthCode(userName)\"\n      >VERIFY</ion-button\n    >\n\n    <div text-center><span id=\"countdown\"></span> sec!</div>\n  </div>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/pages/phone/phone.module.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/phone/phone.module.ts ***!
  \*********************************************/
/*! exports provided: PhonePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PhonePageModule", function() { return PhonePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _phone_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./phone.page */ "./src/app/pages/phone/phone.page.ts");







const routes = [
    {
        path: '',
        component: _phone_page__WEBPACK_IMPORTED_MODULE_6__["PhonePage"]
    }
];
let PhonePageModule = class PhonePageModule {
};
PhonePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_phone_page__WEBPACK_IMPORTED_MODULE_6__["PhonePage"]]
    })
], PhonePageModule);



/***/ }),

/***/ "./src/app/pages/phone/phone.page.scss":
/*!*********************************************!*\
  !*** ./src/app/pages/phone/phone.page.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  background-size: cover;\n  background-position: center;\n}\nion-content form {\n  margin-bottom: 26px;\n  height: 300px auto;\n  background: white;\n  padding: 10px;\n  border-radius: 30px;\n  margin-top: 20px;\n  border: 1px solid rgba(212, 212, 212, 0.93);\n}\nion-content p {\n  font-size: 0.8em;\n  color: #d2d2d2;\n}\nion-content ion-label {\n  text-align: center;\n}\nion-content .o_section {\n  position: fixed;\n  background: #f7f7f7;\n  height: 150px;\n  top: 70%;\n  width: 90%;\n  padding: 4px;\n  border-radius: 30px;\n  left: 5%;\n  border: 1px solid rgba(212, 212, 212, 0.93);\n  -webkit-animation-direction: alternate;\n          animation-direction: alternate;\n  -webkit-animation-name: bump;\n          animation-name: bump;\n  -webkit-animation-duration: 0.8s;\n          animation-duration: 0.8s;\n  z-index: 12;\n}\n@-webkit-keyframes bump {\n  0% {\n    top: 100%;\n  }\n  100% {\n    top: 70%;\n  }\n}\n@keyframes bump {\n  0% {\n    top: 100%;\n  }\n  100% {\n    top: 70%;\n  }\n}\nion-content ion-input {\n  padding: 5px;\n}\nion-content .invalid {\n  border: 1px solid #ff6153;\n}\nion-content .error-message .item-inner {\n  border-bottom: 0 !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYWhhL0lvbmljQXBwcy9DNFJpZGUtSU9TLTIwMjQvY2FsbDRyaWRlLXBhc3Nlbmdlci9zcmMvYXBwL3BhZ2VzL3Bob25lL3Bob25lLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvcGhvbmUvcGhvbmUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRUUsc0JBQUE7RUFDQSwyQkFBQTtBQ0FGO0FEQ0U7RUFDRSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLDJDQUFBO0FDQ0o7QURDRTtFQUNFLGdCQUFBO0VBQ0EsY0FBQTtBQ0NKO0FEQ0U7RUFDRSxrQkFBQTtBQ0NKO0FEQ0U7RUFDRSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsUUFBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxRQUFBO0VBQ0EsMkNBQUE7RUFDQSxzQ0FBQTtVQUFBLDhCQUFBO0VBQ0EsNEJBQUE7VUFBQSxvQkFBQTtFQUNBLGdDQUFBO1VBQUEsd0JBQUE7RUFDQSxXQUFBO0FDQ0o7QURDRTtFQUNFO0lBQ0UsU0FBQTtFQ0NKO0VERUU7SUFDRSxRQUFBO0VDQUo7QUFDRjtBRFBFO0VBQ0U7SUFDRSxTQUFBO0VDQ0o7RURFRTtJQUNFLFFBQUE7RUNBSjtBQUNGO0FERUU7RUFDRSxZQUFBO0FDQUo7QURFRTtFQUNFLHlCQUFBO0FDQUo7QURFRTtFQUNFLDJCQUFBO0FDQUoiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9waG9uZS9waG9uZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XHJcbiAgLy8gLS1iYWNrZ3JvdW5kOiB1cmwoXCJzcmMvYXNzZXRzL2ltZy9hLXNwbGFzaC0xLnBuZ1wiKSBuby1yZXBlYXQgY2VudGVyIC8gY292ZXI7XHJcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XHJcbiAgZm9ybSB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAyNnB4O1xyXG4gICAgaGVpZ2h0OiAzMDBweCBhdXRvO1xyXG4gICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMzBweDtcclxuICAgIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpO1xyXG4gIH1cclxuICBwIHtcclxuICAgIGZvbnQtc2l6ZTogMC44ZW07XHJcbiAgICBjb2xvcjogI2QyZDJkMjtcclxuICB9XHJcbiAgaW9uLWxhYmVsIHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB9XHJcbiAgLm9fc2VjdGlvbiB7XHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICBiYWNrZ3JvdW5kOiAjZjdmN2Y3O1xyXG4gICAgaGVpZ2h0OiAxNTBweDtcclxuICAgIHRvcDogNzAlO1xyXG4gICAgd2lkdGg6IDkwJTtcclxuICAgIHBhZGRpbmc6IDRweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDMwcHg7XHJcbiAgICBsZWZ0OiA1JTtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkIHJnYmEoMjEyLCAyMTIsIDIxMiwgMC45Myk7XHJcbiAgICBhbmltYXRpb24tZGlyZWN0aW9uOiBhbHRlcm5hdGU7XHJcbiAgICBhbmltYXRpb24tbmFtZTogYnVtcDtcclxuICAgIGFuaW1hdGlvbi1kdXJhdGlvbjogMC44cztcclxuICAgIHotaW5kZXg6IDEyO1xyXG4gIH1cclxuICBAa2V5ZnJhbWVzIGJ1bXAge1xyXG4gICAgMCUge1xyXG4gICAgICB0b3A6IDEwMCU7XHJcbiAgICB9XHJcbiAgICAvLyA1MCUge3RvcDogOTAlO31cclxuICAgIDEwMCUge1xyXG4gICAgICB0b3A6IDcwJTtcclxuICAgIH1cclxuICB9XHJcbiAgaW9uLWlucHV0IHtcclxuICAgIHBhZGRpbmc6IDVweDtcclxuICB9XHJcbiAgLmludmFsaWQge1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2ZmNjE1MztcclxuICB9XHJcbiAgLmVycm9yLW1lc3NhZ2UgLml0ZW0taW5uZXIge1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMCAhaW1wb3J0YW50O1xyXG4gIH1cclxufVxyXG4iLCJpb24tY29udGVudCB7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbn1cbmlvbi1jb250ZW50IGZvcm0ge1xuICBtYXJnaW4tYm90dG9tOiAyNnB4O1xuICBoZWlnaHQ6IDMwMHB4IGF1dG87XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBwYWRkaW5nOiAxMHB4O1xuICBib3JkZXItcmFkaXVzOiAzMHB4O1xuICBtYXJnaW4tdG9wOiAyMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpO1xufVxuaW9uLWNvbnRlbnQgcCB7XG4gIGZvbnQtc2l6ZTogMC44ZW07XG4gIGNvbG9yOiAjZDJkMmQyO1xufVxuaW9uLWNvbnRlbnQgaW9uLWxhYmVsIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuaW9uLWNvbnRlbnQgLm9fc2VjdGlvbiB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgYmFja2dyb3VuZDogI2Y3ZjdmNztcbiAgaGVpZ2h0OiAxNTBweDtcbiAgdG9wOiA3MCU7XG4gIHdpZHRoOiA5MCU7XG4gIHBhZGRpbmc6IDRweDtcbiAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgbGVmdDogNSU7XG4gIGJvcmRlcjogMXB4IHNvbGlkIHJnYmEoMjEyLCAyMTIsIDIxMiwgMC45Myk7XG4gIGFuaW1hdGlvbi1kaXJlY3Rpb246IGFsdGVybmF0ZTtcbiAgYW5pbWF0aW9uLW5hbWU6IGJ1bXA7XG4gIGFuaW1hdGlvbi1kdXJhdGlvbjogMC44cztcbiAgei1pbmRleDogMTI7XG59XG5Aa2V5ZnJhbWVzIGJ1bXAge1xuICAwJSB7XG4gICAgdG9wOiAxMDAlO1xuICB9XG4gIDEwMCUge1xuICAgIHRvcDogNzAlO1xuICB9XG59XG5pb24tY29udGVudCBpb24taW5wdXQge1xuICBwYWRkaW5nOiA1cHg7XG59XG5pb24tY29udGVudCAuaW52YWxpZCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNmZjYxNTM7XG59XG5pb24tY29udGVudCAuZXJyb3ItbWVzc2FnZSAuaXRlbS1pbm5lciB7XG4gIGJvcmRlci1ib3R0b206IDAgIWltcG9ydGFudDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/phone/phone.page.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/phone/phone.page.ts ***!
  \*******************************************/
/*! exports provided: PhonePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PhonePage", function() { return PhonePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/settings.service */ "./src/app/services/settings.service.ts");
/* harmony import */ var src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/profile.service */ "./src/app/services/profile.service.ts");
/* harmony import */ var src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/pop-up.service */ "./src/app/services/pop-up.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");








let PhonePage = class PhonePage {
    constructor(navCtrl, settings, http, platform, pop, ph, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.settings = settings;
        this.http = http;
        this.platform = platform;
        this.pop = pop;
        this.ph = ph;
        this.loadingCtrl = loadingCtrl;
        this.isNotCordova = false;
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpHeaders"]({ "Content-Type": "application/json" }),
        };
    }
    AuthPhone(phone) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            if (phone) {
                const loading = yield this.loadingCtrl.create({
                    spinner: null,
                    duration: 3000,
                    message: "Please wait...",
                    translucent: true,
                    cssClass: "custom-class custom-loading",
                });
                yield loading.present();
                this.PhoneNumber = phone;
                this.http
                    .post("https://us-central1-ridefhv-61945.cloudfunctions.net/Verify", {
                    number: "+233" + phone,
                    appName: this.settings.appName,
                })
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])((response) => response.json()))
                    .subscribe((res) => {
                    this.requestCode = res.request_id;
                    loading.dismiss();
                }, (error) => {
                    this.pop.showPimp("Wrong Number");
                });
                this.http
                    .post("https://us-central1-ridefhv-61945.cloudfunctions.net/Verify", {
                    number: "+233" + phone,
                    appName: this.settings.appName,
                })
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])((response) => response.json()))
                    .subscribe((res) => {
                    this.userName = "";
                }, (error) => {
                    this.pop.showPimp("Wrong Number");
                    //this.pop.hideLoader();
                });
                this.http
                    .post("https://us-central1-ridefhv-61945.cloudfunctions.net/Verify", {
                    number: "+233" + phone,
                    appName: this.settings.appName,
                })
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])((response) => response.json()))
                    .subscribe((res) => {
                    this.startTimer();
                }, (error) => {
                    this.pop.showPimp("Wrong Number");
                    // this.pop.hideLoader();
                });
            }
        });
    }
    AuthCode(code) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            if (code) {
                const loading = yield this.loadingCtrl.create({
                    spinner: null,
                    duration: 3000,
                    message: "Please wait...",
                    translucent: true,
                    cssClass: "custom-class custom-loading",
                });
                yield loading.present();
                this.http
                    .post("https://us-central1-ridefhv-61945.cloudfunctions.net/Verify", {
                    reqID: this.requestCode,
                    code: code,
                })
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])((response) => response.json()))
                    .subscribe((res) => {
                    if (res.status == 0) {
                        this.ph.UpdateNumber(this.userName, this.PhoneNumber).then(() => {
                            this.navCtrl.navigateRoot("more-info");
                            loading.dismiss();
                        });
                    }
                }, (error) => {
                    this.pop.showPimp("Couldnt get code");
                    this.requestCode = false;
                    this.userName = "";
                });
            }
        });
    }
    startTimer() {
        this.timeleft = 50;
        var downloadTimer = setInterval(() => {
            if (this.timeleft <= 0) {
                clearInterval(downloadTimer);
                this.userName = "";
                document.getElementById("countdown").innerHTML = "Finished";
                this.requestCode = false;
            }
            else {
                console.log(this.timeleft);
                document.getElementById("countdown").innerHTML =
                    "Try Again In " + this.timeleft;
            }
            this.timeleft -= 1;
        }, 1000);
    }
    ngOnInit() { }
};
PhonePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_3__["SettingsService"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
    { type: src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_5__["PopUpService"] },
    { type: src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_4__["ProfileService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] }
];
PhonePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-phone",
        template: __webpack_require__(/*! raw-loader!./phone.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/phone/phone.page.html"),
        styles: [__webpack_require__(/*! ./phone.page.scss */ "./src/app/pages/phone/phone.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
        src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_3__["SettingsService"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"],
        src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_5__["PopUpService"],
        src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_4__["ProfileService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]])
], PhonePage);



/***/ })

}]);
//# sourceMappingURL=pages-phone-phone-module-es2015.js.map