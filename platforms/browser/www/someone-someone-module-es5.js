(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["someone-someone-module"],{

/***/ "./src/app/someone/someone.module.ts":
/*!*******************************************!*\
  !*** ./src/app/someone/someone.module.ts ***!
  \*******************************************/
/*! exports provided: SomeonePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SomeonePageModule", function() { return SomeonePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ionic4_rating__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ionic4-rating */ "./node_modules/ionic4-rating/dist/index.js");
/* harmony import */ var _someone_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./someone.page */ "./src/app/someone/someone.page.ts");
// import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';
// import { FormsModule } from '@angular/forms';
// import { Routes, RouterModule } from '@angular/router';

// import { IonicModule } from '@ionic/angular';
// import { SomeonePage } from './someone.page';
// const routes: Routes = [
//   {
//     path: '',
//     component: SomeonePage
//   }
// ];
// @NgModule({
//   imports: [
//     CommonModule,
//     FormsModule,
//     IonicModule,
//     RouterModule.forChild(routes)
//   ],
//   declarations: [SomeonePage]
// })
// export class SomeonePageModule { }








var routes = [
    {
        path: "",
        component: _someone_page__WEBPACK_IMPORTED_MODULE_7__["SomeonePage"],
    },
];
var SomeonePageModule = /** @class */ (function () {
    function SomeonePageModule() {
    }
    SomeonePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_5__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                ionic4_rating__WEBPACK_IMPORTED_MODULE_6__["IonicRatingModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
            ],
            exports: [
            // RatePage, //<----- this is if it is going to be used else where
            ],
            declarations: [
            // other components
            // SomeonePage,
            ],
            // declarations: [RatePage],
            // entryComponents: [RatePage],
            schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_5__["CUSTOM_ELEMENTS_SCHEMA"], _angular_core__WEBPACK_IMPORTED_MODULE_5__["NO_ERRORS_SCHEMA"]],
        })
    ], SomeonePageModule);
    return SomeonePageModule;
}());



/***/ })

}]);
//# sourceMappingURL=someone-someone-module-es5.js.map