(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-documents-documents-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/documents/documents.page.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/documents/documents.page.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header no-border>\n  <ion-toolbar style=\"margin-top: 20px\">\n    <ion-button size=\"large\" fill=\"clear\" (click)=\"goBack()\">\n      <ion-icon name=\"arrow-back\"></ion-icon>\n      <span style=\"padding-left:30px; font-size: 1.0em\"> DOCUMENTS </span>\n    </ion-button>\n\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding text-center class='yes-scroll'>\n  <div class='followed-items'>\n    <ion-list text-center>\n      <div text-center *ngFor=\"let event of eventList\">\n        <ion-item class='driver' (click)='GotoItem(event)'>\n          <h2 style=\"font-size: 1.2em\">{{event.title}}</h2>\n\n          <ion-buttons slot=\"end\">\n            <ion-icon name=\"ios-arrow-forward\"></ion-icon>\n          </ion-buttons>\n\n          <!-- <div *ngFor='let eve of eventLists'> -->\n          <h2 color='danger' *ngIf='days < 0'><strong>{{event.expired}}</strong></h2>\n          <h2 color='danger' *ngIf='days > 0'><strong></strong>Expired</h2>\n\n          <ion-buttons slot=\"end\">\n            <ion-icon *ngIf='days > 0' color='danger' name=\"alert\"></ion-icon>\n          </ion-buttons>\n\n          <div *ngIf='event.permission'>\n            <h3 *ngIf='event.permission == 0'>Optional</h3>\n            <h3 *ngIf='event.permission == 1'></h3>\n          </div>\n\n          <!-- </div> -->\n\n\n\n        </ion-item>\n      </div>\n    </ion-list>\n\n  </div>\n</ion-content>\n\n\n<ion-footer>\n\n</ion-footer>"

/***/ }),

/***/ "./src/app/pages/documents/documents.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/documents/documents.module.ts ***!
  \*****************************************************/
/*! exports provided: DocumentsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DocumentsPageModule", function() { return DocumentsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _documents_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./documents.page */ "./src/app/pages/documents/documents.page.ts");







const routes = [
    {
        path: '',
        component: _documents_page__WEBPACK_IMPORTED_MODULE_6__["DocumentsPage"]
    }
];
let DocumentsPageModule = class DocumentsPageModule {
};
DocumentsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_documents_page__WEBPACK_IMPORTED_MODULE_6__["DocumentsPage"]]
    })
], DocumentsPageModule);



/***/ }),

/***/ "./src/app/pages/documents/documents.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/pages/documents/documents.page.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content h1 {\n  background: rgba(219, 205, 8, 0.91);\n  font-size: 1.67em;\n  padding-top: 7px;\n  width: auto;\n  padding-bottom: 7px;\n}\nion-content .followed-items .driver {\n  margin-top: 5px;\n  margin-bottom: 5px;\n  border-radius: 5px;\n  padding: 10px;\n  width: 100%;\n}\nion-content .followed-items .driver p {\n  font-size: 16px;\n  font-size: 4vw;\n  color: white;\n}\nion-content .followed-items .user {\n  margin-top: 5px;\n  margin-bottom: 5px;\n  border-radius: 30px;\n  border: 1px solid #d8d8d8;\n  padding: 10px;\n  float: left;\n  width: 80%;\n}\nion-content .followed-items .user p {\n  font-size: 16px;\n  font-size: 4vw;\n  color: white;\n}\nion-content .topped-items ion-item {\n  margin-top: 0px;\n  margin-bottom: 6px;\n}\nion-content .topped-items h2 {\n  color: orange;\n  font-size: 1.27em;\n  padding: 2px;\n}\nion-content .topped-items ion-label {\n  color: #0099ff !important;\n  font-size: 1em;\n  padding: 2px;\n}\n#container_1 {\n  margin-top: 2%;\n  height: auto;\n  width: 100%;\n  padding: 25px;\n  border-radius: 4px;\n  color: white;\n  text-align: center;\n  background: #45caff;\n  overflow: hidden;\n}\n#container_1 ion-icon {\n  color: white;\n}\n#container_1 h2 {\n  font-size: 1em;\n  height: auto;\n}\n#container_1 ion-icon {\n  margin: 5px;\n}\n#container_1 .profile-pic {\n  width: 15%;\n  height: 18%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYWhhL0lvbmljQXBwcy9DNFJpZGUtSU9TLTIwMjQvY2FsbDRyaWRlLXBhc3Nlbmdlci9zcmMvYXBwL3BhZ2VzL2RvY3VtZW50cy9kb2N1bWVudHMucGFnZS5zY3NzIiwic3JjL2FwcC9wYWdlcy9kb2N1bWVudHMvZG9jdW1lbnRzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDRTtFQUNFLG1DQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtBQ0FKO0FESUk7RUFDRSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUVBLGFBQUE7RUFFQSxXQUFBO0FDSk47QURNTTtFQUNFLGVBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtBQ0pSO0FEUUk7RUFDRSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBRUEsYUFBQTtFQUNBLFdBQUE7RUFDQSxVQUFBO0FDUE47QURTTTtFQUNFLGVBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtBQ1BSO0FEYUk7RUFDRSxlQUFBO0VBQ0Esa0JBQUE7QUNYTjtBRGFJO0VBQ0UsYUFBQTtFQUNBLGlCQUFBO0VBQ0EsWUFBQTtBQ1hOO0FEY0k7RUFDRSx5QkFBQTtFQUNBLGNBQUE7RUFDQSxZQUFBO0FDWk47QURpQkE7RUFDRSxjQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUVBLGdCQUFBO0FDZkY7QURnQkU7RUFDRSxZQUFBO0FDZEo7QURnQkU7RUFDRSxjQUFBO0VBQ0EsWUFBQTtBQ2RKO0FEaUJFO0VBQ0UsV0FBQTtBQ2ZKO0FEa0JFO0VBQ0UsVUFBQTtFQUNBLFdBQUE7QUNoQkoiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9kb2N1bWVudHMvZG9jdW1lbnRzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50IHtcclxuICBoMSB7XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDIxOSwgMjA1LCA4LCAwLjkxKTtcclxuICAgIGZvbnQtc2l6ZTogMS42N2VtO1xyXG4gICAgcGFkZGluZy10b3A6IDdweDtcclxuICAgIHdpZHRoOiBhdXRvO1xyXG4gICAgcGFkZGluZy1ib3R0b206IDdweDtcclxuICB9XHJcblxyXG4gIC5mb2xsb3dlZC1pdGVtcyB7XHJcbiAgICAuZHJpdmVyIHtcclxuICAgICAgbWFyZ2luLXRvcDogNXB4O1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiA1cHg7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuXHJcbiAgICAgIHBhZGRpbmc6IDEwcHg7XHJcblxyXG4gICAgICB3aWR0aDogMTAwJTtcclxuXHJcbiAgICAgIHAge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTZweDtcclxuICAgICAgICBmb250LXNpemU6IDR2dztcclxuICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAudXNlciB7XHJcbiAgICAgIG1hcmdpbi10b3A6IDVweDtcclxuICAgICAgbWFyZ2luLWJvdHRvbTogNXB4O1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xyXG4gICAgICBib3JkZXI6IDFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNik7XHJcblxyXG4gICAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgICBmbG9hdDogbGVmdDtcclxuICAgICAgd2lkdGg6IDgwJTtcclxuXHJcbiAgICAgIHAge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTZweDtcclxuICAgICAgICBmb250LXNpemU6IDR2dztcclxuICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIC50b3BwZWQtaXRlbXMge1xyXG4gICAgaW9uLWl0ZW0ge1xyXG4gICAgICBtYXJnaW4tdG9wOiAwcHg7XHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDZweDtcclxuICAgIH1cclxuICAgIGgyIHtcclxuICAgICAgY29sb3I6IG9yYW5nZTtcclxuICAgICAgZm9udC1zaXplOiAxLjI3ZW07XHJcbiAgICAgIHBhZGRpbmc6IDJweDtcclxuICAgIH1cclxuXHJcbiAgICBpb24tbGFiZWwge1xyXG4gICAgICBjb2xvcjogcmdiKDAsIDE1MywgMjU1KSAhaW1wb3J0YW50O1xyXG4gICAgICBmb250LXNpemU6IDFlbTtcclxuICAgICAgcGFkZGluZzogMnB4O1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuI2NvbnRhaW5lcl8xIHtcclxuICBtYXJnaW4tdG9wOiAyJTtcclxuICBoZWlnaHQ6IGF1dG87XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgcGFkZGluZzogMjVweDtcclxuICBib3JkZXItcmFkaXVzOiA0cHg7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBiYWNrZ3JvdW5kOiByZ2IoNjksIDIwMiwgMjU1KTtcclxuXHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICBpb24taWNvbiB7XHJcbiAgICBjb2xvcjogcmdiKDI1NSwgMjU1LCAyNTUpO1xyXG4gIH1cclxuICBoMiB7XHJcbiAgICBmb250LXNpemU6IDFlbTtcclxuICAgIGhlaWdodDogYXV0bztcclxuICB9XHJcblxyXG4gIGlvbi1pY29uIHtcclxuICAgIG1hcmdpbjogNXB4O1xyXG4gIH1cclxuXHJcbiAgLnByb2ZpbGUtcGljIHtcclxuICAgIHdpZHRoOiAxNSU7XHJcbiAgICBoZWlnaHQ6IDE4JTtcclxuICB9XHJcbn1cclxuIiwiaW9uLWNvbnRlbnQgaDEge1xuICBiYWNrZ3JvdW5kOiByZ2JhKDIxOSwgMjA1LCA4LCAwLjkxKTtcbiAgZm9udC1zaXplOiAxLjY3ZW07XG4gIHBhZGRpbmctdG9wOiA3cHg7XG4gIHdpZHRoOiBhdXRvO1xuICBwYWRkaW5nLWJvdHRvbTogN3B4O1xufVxuaW9uLWNvbnRlbnQgLmZvbGxvd2VkLWl0ZW1zIC5kcml2ZXIge1xuICBtYXJnaW4tdG9wOiA1cHg7XG4gIG1hcmdpbi1ib3R0b206IDVweDtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBwYWRkaW5nOiAxMHB4O1xuICB3aWR0aDogMTAwJTtcbn1cbmlvbi1jb250ZW50IC5mb2xsb3dlZC1pdGVtcyAuZHJpdmVyIHAge1xuICBmb250LXNpemU6IDE2cHg7XG4gIGZvbnQtc2l6ZTogNHZ3O1xuICBjb2xvcjogd2hpdGU7XG59XG5pb24tY29udGVudCAuZm9sbG93ZWQtaXRlbXMgLnVzZXIge1xuICBtYXJnaW4tdG9wOiA1cHg7XG4gIG1hcmdpbi1ib3R0b206IDVweDtcbiAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2Q4ZDhkODtcbiAgcGFkZGluZzogMTBweDtcbiAgZmxvYXQ6IGxlZnQ7XG4gIHdpZHRoOiA4MCU7XG59XG5pb24tY29udGVudCAuZm9sbG93ZWQtaXRlbXMgLnVzZXIgcCB7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgZm9udC1zaXplOiA0dnc7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cbmlvbi1jb250ZW50IC50b3BwZWQtaXRlbXMgaW9uLWl0ZW0ge1xuICBtYXJnaW4tdG9wOiAwcHg7XG4gIG1hcmdpbi1ib3R0b206IDZweDtcbn1cbmlvbi1jb250ZW50IC50b3BwZWQtaXRlbXMgaDIge1xuICBjb2xvcjogb3JhbmdlO1xuICBmb250LXNpemU6IDEuMjdlbTtcbiAgcGFkZGluZzogMnB4O1xufVxuaW9uLWNvbnRlbnQgLnRvcHBlZC1pdGVtcyBpb24tbGFiZWwge1xuICBjb2xvcjogIzAwOTlmZiAhaW1wb3J0YW50O1xuICBmb250LXNpemU6IDFlbTtcbiAgcGFkZGluZzogMnB4O1xufVxuXG4jY29udGFpbmVyXzEge1xuICBtYXJnaW4tdG9wOiAyJTtcbiAgaGVpZ2h0OiBhdXRvO1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogMjVweDtcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xuICBjb2xvcjogd2hpdGU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgYmFja2dyb3VuZDogIzQ1Y2FmZjtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cbiNjb250YWluZXJfMSBpb24taWNvbiB7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cbiNjb250YWluZXJfMSBoMiB7XG4gIGZvbnQtc2l6ZTogMWVtO1xuICBoZWlnaHQ6IGF1dG87XG59XG4jY29udGFpbmVyXzEgaW9uLWljb24ge1xuICBtYXJnaW46IDVweDtcbn1cbiNjb250YWluZXJfMSAucHJvZmlsZS1waWMge1xuICB3aWR0aDogMTUlO1xuICBoZWlnaHQ6IDE4JTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/documents/documents.page.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/documents/documents.page.ts ***!
  \***************************************************/
/*! exports provided: DocumentsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DocumentsPage", function() { return DocumentsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var src_app_services_language_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/language.service */ "./src/app/services/language.service.ts");
/* harmony import */ var src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/pop-up.service */ "./src/app/services/pop-up.service.ts");
/* harmony import */ var src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/settings.service */ "./src/app/services/settings.service.ts");
/* harmony import */ var src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/profile.service */ "./src/app/services/profile.service.ts");
/* harmony import */ var src_app_services_event_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/event.service */ "./src/app/services/event.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");











let DocumentsPage = class DocumentsPage {
    constructor(navCtrl, camera, lp, settings, pop, load, profile, alert, location, eventProvider, router) {
        this.navCtrl = navCtrl;
        this.camera = camera;
        this.lp = lp;
        this.settings = settings;
        this.pop = pop;
        this.load = load;
        this.profile = profile;
        this.alert = alert;
        this.location = location;
        this.eventProvider = eventProvider;
        this.router = router;
    }
    ionViewDidEnter() {
        this.eventList = [];
        this.eventLists = [];
        this.profile.getCompanies().child('Rider/documents').once('value', snapshot => {
            snapshot.forEach(snap => {
                if (snap.child(this.profile.id).val()) {
                    const today = new Date();
                    const expire = new Date(snap.child(this.profile.id).val().expired);
                    const day = this.days;
                    console.log(day);
                    console.log(this.calcDate(today, expire));
                    this.eventList.push({
                        id: snap.key,
                        permission: snap.val().permission,
                        filetype: snap.val().filetype,
                        title: snap.val().title,
                        approved: snap.child(this.profile.id).val().approved,
                        client: snap.child(this.profile.id).val().client,
                        comment: snap.child(this.profile.id).val().comment,
                        data: snap.child(this.profile.id).val().data,
                        denied: snap.child(this.profile.id).val().denied,
                        expired: this.calcDate(today, expire),
                        expiredate: day
                    });
                }
                else {
                    this.eventList.push({
                        id: snap.key,
                        permission: snap.val().permission,
                        filetype: snap.val().filetype,
                        title: snap.val().title,
                    });
                }
                return false;
            });
            this.profile.getCompanies().child('Rider/documents').off('value');
        });
    }
    calcDate(date1, date2) {
        const diff = Math.floor(date2.getTime() - date1.getTime());
        const day = 1000 * 60 * 60 * 24;
        const days = Math.floor(diff / day);
        const months = Math.floor(days / 31);
        const years = Math.floor(months / 12);
        console.log(days);
        this.days = days;
        let message = ' Expires in ';
        message += days + ' days ';
        if (months !== 0) {
            message += months + ' months ';
            message += years + ' years \n';
        }
        return message;
    }
    presentRouteLoader(message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.load.create({
                // tslint:disable-next-line: object-literal-shorthand
                message: message
            });
            loading.present();
            const myInterval = setInterval(() => {
                if (this.eventList != null) {
                    loading.dismiss();
                    clearInterval(myInterval);
                }
            }, 1000);
        });
    }
    GotoItem(h) {
        console.log(h);
        this.router.navigate(['documentdetail'], { queryParams: h });
    }
    goBack() {
        this.navCtrl.navigateRoot('home');
    }
    ngOnInit() {
    }
};
DocumentsPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_3__["Camera"] },
    { type: src_app_services_language_service__WEBPACK_IMPORTED_MODULE_4__["LanguageService"] },
    { type: src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_6__["SettingsService"] },
    { type: src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_5__["PopUpService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_7__["ProfileService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_9__["Location"] },
    { type: src_app_services_event_service__WEBPACK_IMPORTED_MODULE_8__["EventService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_10__["Router"] }
];
DocumentsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-documents',
        template: __webpack_require__(/*! raw-loader!./documents.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/documents/documents.page.html"),
        styles: [__webpack_require__(/*! ./documents.page.scss */ "./src/app/pages/documents/documents.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_3__["Camera"], src_app_services_language_service__WEBPACK_IMPORTED_MODULE_4__["LanguageService"],
        src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_6__["SettingsService"], src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_5__["PopUpService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
        src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_7__["ProfileService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"], _angular_common__WEBPACK_IMPORTED_MODULE_9__["Location"],
        src_app_services_event_service__WEBPACK_IMPORTED_MODULE_8__["EventService"], _angular_router__WEBPACK_IMPORTED_MODULE_10__["Router"]])
], DocumentsPage);



/***/ })

}]);
//# sourceMappingURL=pages-documents-documents-module-es2015.js.map