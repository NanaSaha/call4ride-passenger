(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-schedule-schedule-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/schedule/schedule.page.html":
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/schedule/schedule.page.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header no-border>\n  <ion-toolbar style=\"margin-top: 20px\">\n    <ion-button size=\"large\" fill=\"clear\" (click)=\"goBack()\">\n      <ion-icon name=\"arrow-back\"></ion-icon>\n      <span style=\"padding-left:30px; font-size: 1.0em\">{{Lang[0].rideSch}}</span>\n    </ion-button>\n\n  </ion-toolbar>\n</ion-header>\n<ion-content padding class=\"no-scroll\">\n  <!-- These are the location bar and destination bar -->\n\n  <div *ngIf='scheduleInfo == null'>\n    <div text-center class=\"whiteFlap\">\n      <div class=\"bars\">\n        <p>{{Lang[0].from}}</p>\n\n        <!-- location bar -->\n        <ion-button lines=\"none\" detail=\"false\" class=\"bars-locate\" (click)=\"showAddressModal(1)\">\n          <ion-icon color='deep' name=\"locate\" slot=\"icon-start\"></ion-icon>\n          <div id=\"position\">{{locationName}}</div>\n        </ion-button>\n        <p>{{Lang[0].to}}</p>\n        <!-- desination bar -->\n        <ion-button lines=\"none\" detail=\"false\" class=\"bars-destinate\" (click)=\"showAddressModal(2)\">\n          <ion-icon color='deep' name=\"flag\" slot=\"icon-start\"></ion-icon>\n          <div id=\"whereto\">{{Lang[0].dest}}</div>\n        </ion-button>\n\n\n      </div>\n\n\n\n\n\n      <div class='top-items'>\n        <ion-item lines=\"none\">\n          <ion-label text-center>{{Lang[0].date}}</ion-label>\n          <ion-datetime displayFormat=\"MMM DD, YYYY HH:mm\" [(ngModel)]=\"myDate\" (ionChange)='Chosen($event)' min='2018'\n            max=\"2020-10-31\"></ion-datetime>\n        </ion-item>\n      </div>\n    </div>\n  </div>\n\n\n\n\n  <div *ngIf='scheduleInfo != null' class='followed-items'>\n    <ion-list text-center>\n      <ion-item>\n        <h2 class='ride'><strong>\n            <ion-icon color='deep' name=\"timer\"></ion-icon>{{Lang[0].rideSchu}}\n          </strong></h2>\n        <h2 class='date'><strong>\n            <ion-icon color='deep' name=\"calendar\"></ion-icon>{{dataTime}}\n          </strong></h2>\n        <h2 class='date'><strong>\n            <ion-icon color='primary' name=\"clock\"></ion-icon>@{{scheduleInfo.Client_Time}}\n          </strong></h2>\n        <h2 class='location'><strong>\n            <ion-icon color='deep' name=\"locate\"></ion-icon>{{scheduleInfo.Client_locationName}}\n          </strong></h2>\n        <h2 class='destination'><strong>\n            <ion-icon color='deep' name=\"navigate\"></ion-icon>{{scheduleInfo.Client_destinationName}}\n          </strong></h2>\n\n        <ion-button detail-none id='button' color=\"danger\" no-lines text-center icon-start ion-item\n          (click)=\"CancelRide()\">\n          {{Lang[0].clear}}\n        </ion-button>\n      </ion-item>\n    </ion-list>\n  </div>\n\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/schedule/schedule.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/schedule/schedule.module.ts ***!
  \***************************************************/
/*! exports provided: SchedulePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SchedulePageModule", function() { return SchedulePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _schedule_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./schedule.page */ "./src/app/pages/schedule/schedule.page.ts");







var routes = [
    {
        path: '',
        component: _schedule_page__WEBPACK_IMPORTED_MODULE_6__["SchedulePage"]
    }
];
var SchedulePageModule = /** @class */ (function () {
    function SchedulePageModule() {
    }
    SchedulePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_schedule_page__WEBPACK_IMPORTED_MODULE_6__["SchedulePage"]]
        })
    ], SchedulePageModule);
    return SchedulePageModule;
}());



/***/ }),

/***/ "./src/app/pages/schedule/schedule.page.scss":
/*!***************************************************!*\
  !*** ./src/app/pages/schedule/schedule.page.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".top-items {\n  border-radius: 12px;\n  margin-top: 20%;\n}\n.top-items ion-item {\n  margin-top: 10px;\n  margin-bottom: 10px;\n  border-radius: 12px;\n  background: #0a64eb;\n}\n.top-items ion-item ion-label {\n  color: white !important;\n  text-align: center;\n  margin-left: 10%;\n}\n#envelope {\n  height: auto;\n  width: 4em;\n}\n.ride {\n  color: rgba(219, 205, 8, 0.91);\n  font-size: 1.17em;\n  padding-top: 14px;\n  padding-bottom: 14px;\n  border-radius: 12px;\n}\n.ride ion-icon {\n  font-size: 0.8em;\n  padding: 12px;\n  color: rgba(219, 205, 8, 0.91);\n}\n.price {\n  color: rgba(219, 205, 8, 0.91);\n  font-size: 1.67em;\n  padding-top: 14px;\n  padding-bottom: 14px;\n  border-radius: 12px;\n}\n.price ion-icon {\n  font-size: 0.8em;\n  padding: 5px;\n  color: rgba(219, 205, 8, 0.91);\n}\n.date {\n  color: orange;\n  font-size: 1.47em;\n  padding-top: 14px;\n  padding-bottom: 14px;\n  border-bottom: 1px solid #d8d8d8;\n}\n.date ion-icon {\n  font-size: 0.8em;\n  padding: 5px;\n  color: rgba(219, 205, 8, 0.91);\n}\n.time {\n  color: #2c88f1;\n  font-size: 1.17em;\n  padding-top: 14px;\n  padding-bottom: 14px;\n}\n.time ion-icon {\n  font-size: 0.8em;\n  padding: 5px;\n  color: rgba(219, 205, 8, 0.91);\n}\n.location {\n  width: auto;\n  padding-top: 8px;\n  padding-bottom: 8px;\n}\n.location p {\n  font-size: 1.3em;\n  height: auto;\n}\n.location ion-icon {\n  font-size: 0.8em;\n  padding: 5px;\n  color: orange;\n}\n.destination {\n  width: auto;\n  padding-top: 8px;\n  padding-bottom: 8px;\n}\n.destination ion-icon {\n  font-size: 0.8em;\n  padding: 5px;\n  color: darkslateblue;\n}\n.followed-items {\n  margin-top: 10%;\n}\n.followed-items ion-item {\n  margin-top: 10px;\n  margin-bottom: 10px;\n  border-radius: 12px;\n  border: 1px solid #d8d8d8;\n}\n.bars {\n  margin-top: 0%;\n  padding: 12px;\n}\n.bars .poiter {\n  z-index: 5;\n  margin-left: 1%;\n  background: rgba(240, 240, 240, 0.92);\n  border-left: 1.1px solid #d8d8d8;\n  border-right: 1.1px solid #d8d8d8;\n  border-top: 1px solid #d8d8d8;\n  border-bottom: 1px solid #d8d8d8;\n}\n.bars .bars-locate {\n  height: 50px;\n  width: 100%;\n  background: rgba(240, 240, 240, 0.92);\n  border-left: 1.1px solid #d8d8d8;\n  border-right: 1.1px solid #d8d8d8;\n  border-top: 1px solid #d8d8d8;\n  border-bottom: 1px solid #d8d8d8;\n  margin-left: 0%;\n  z-index: 3;\n  border-radius: 12px;\n  overflow: hidden;\n  line-height: 20px;\n  font-size: 1.2em;\n  text-align: center;\n}\n.bars .bars-locate ion-icon {\n  position: absolute;\n  font-size: 1em;\n  left: 2%;\n  color: #0a64eb;\n  padding: 5px;\n}\n.bars .bars-destinate {\n  height: 50px;\n  width: 100%;\n  background: rgba(240, 240, 240, 0.92);\n  margin: 3% 0 0 -50px;\n  margin-left: 0%;\n  z-index: 3;\n  border-left: 1.1px solid #d8d8d8;\n  border-right: 1.1px solid #d8d8d8;\n  border-top: 1px solid #d8d8d8;\n  border-bottom: 1px solid #d8d8d8;\n  overflow: hidden;\n  border-radius: 12px;\n  line-height: 20px;\n  font-size: 1.2em;\n  text-align: center;\n}\n.bars .bars-destinate ion-icon {\n  position: absolute;\n  font-size: 1em;\n  left: 2%;\n  padding: 5px;\n  color: rgba(219, 205, 8, 0.91);\n}\n.bars .bars-price {\n  height: 50px;\n  width: 100%;\n  background: #ffffff;\n  border-left: 1.1px solid #d8d8d8;\n  border-right: 1.1px solid #d8d8d8;\n  border-top: 1px solid #d8d8d8;\n  border-bottom: 1px solid #d8d8d8;\n  z-index: 3;\n  border-radius: 12px;\n  overflow: hidden;\n  line-height: 20px;\n  font-size: 1.2em;\n  text-align: center;\n}\n.bars .bars-price ion-icon {\n  position: absolute;\n  font-size: 1em;\n  left: 2%;\n  color: #0a64eb;\n  padding: 5px;\n}\n#position {\n  text-align: center;\n  padding-left: 17px;\n}\n#whereto {\n  text-align: center;\n  padding-left: 17px;\n}\n#cash {\n  text-align: center;\n  padding-left: 17px;\n}\n#button {\n  border-radius: 12px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYWhhL0lvbmljQXBwcy9DNFJpZGUtSU9TLTIwMjQvY2FsbDRyaWRlLXBhc3Nlbmdlci9zcmMvYXBwL3BhZ2VzL3NjaGVkdWxlL3NjaGVkdWxlLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvc2NoZWR1bGUvc2NoZWR1bGUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsbUJBQUE7RUFDQSxlQUFBO0FDQ0Y7QURBRTtFQUNFLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0FDRUo7QURESTtFQUNFLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQ0dOO0FERUE7RUFDRSxZQUFBO0VBQ0EsVUFBQTtBQ0NGO0FERUE7RUFDRSw4QkFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxvQkFBQTtFQUVBLG1CQUFBO0FDQUY7QURDRTtFQUNFLGdCQUFBO0VBQ0EsYUFBQTtFQUNBLDhCQUFBO0FDQ0o7QURHQTtFQUNFLDhCQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0VBRUEsbUJBQUE7QUNERjtBREVFO0VBQ0UsZ0JBQUE7RUFDQSxZQUFBO0VBQ0EsOEJBQUE7QUNBSjtBRElBO0VBQ0UsYUFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxvQkFBQTtFQUNBLGdDQUFBO0FDREY7QURHRTtFQUNFLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLDhCQUFBO0FDREo7QURLQTtFQUNFLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0VBQ0Esb0JBQUE7QUNGRjtBRElFO0VBQ0UsZ0JBQUE7RUFDQSxZQUFBO0VBQ0EsOEJBQUE7QUNGSjtBRE1BO0VBQ0UsV0FBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7QUNIRjtBRElFO0VBQ0UsZ0JBQUE7RUFDQSxZQUFBO0FDRko7QURLRTtFQUNFLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7QUNISjtBRE9BO0VBQ0UsV0FBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7QUNKRjtBREtFO0VBQ0UsZ0JBQUE7RUFDQSxZQUFBO0VBQ0Esb0JBQUE7QUNISjtBRE9BO0VBQ0UsZUFBQTtBQ0pGO0FETUU7RUFDRSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSx5QkFBQTtBQ0pKO0FEUUE7RUFDRSxjQUFBO0VBQ0EsYUFBQTtBQ0xGO0FET0U7RUFDRSxVQUFBO0VBQ0EsZUFBQTtFQUNBLHFDQUFBO0VBQ0EsZ0NBQUE7RUFDQSxpQ0FBQTtFQUNBLDZCQUFBO0VBQ0EsZ0NBQUE7QUNMSjtBRFFFO0VBQ0UsWUFBQTtFQUNBLFdBQUE7RUFDQSxxQ0FBQTtFQUNBLGdDQUFBO0VBQ0EsaUNBQUE7RUFDQSw2QkFBQTtFQUNBLGdDQUFBO0VBQ0EsZUFBQTtFQUVBLFVBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FDUEo7QURTSTtFQUNFLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLFFBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtBQ1BOO0FEV0U7RUFDRSxZQUFBO0VBQ0EsV0FBQTtFQUNBLHFDQUFBO0VBQ0Esb0JBQUE7RUFDQSxlQUFBO0VBQ0EsVUFBQTtFQUNBLGdDQUFBO0VBQ0EsaUNBQUE7RUFDQSw2QkFBQTtFQUNBLGdDQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBQ1RKO0FEV0k7RUFDRSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxRQUFBO0VBQ0EsWUFBQTtFQUNBLDhCQUFBO0FDVE47QURhRTtFQUNFLFlBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQ0FBQTtFQUNBLGlDQUFBO0VBQ0EsNkJBQUE7RUFDQSxnQ0FBQTtFQUVBLFVBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FDWko7QURjSTtFQUNFLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLFFBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtBQ1pOO0FEaUJBO0VBQ0Usa0JBQUE7RUFDQSxrQkFBQTtBQ2RGO0FEaUJBO0VBQ0Usa0JBQUE7RUFDQSxrQkFBQTtBQ2RGO0FEaUJBO0VBQ0Usa0JBQUE7RUFDQSxrQkFBQTtBQ2RGO0FEaUJBO0VBQ0UsbUJBQUE7QUNkRiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3NjaGVkdWxlL3NjaGVkdWxlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi50b3AtaXRlbXMge1xyXG4gIGJvcmRlci1yYWRpdXM6IDEycHg7XHJcbiAgbWFyZ2luLXRvcDogMjAlO1xyXG4gIGlvbi1pdGVtIHtcclxuICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTJweDtcclxuICAgIGJhY2tncm91bmQ6IHJnYigxMCwgMTAwLCAyMzUpO1xyXG4gICAgaW9uLWxhYmVsIHtcclxuICAgICAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XHJcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgbWFyZ2luLWxlZnQ6IDEwJTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbiNlbnZlbG9wZSB7XHJcbiAgaGVpZ2h0OiBhdXRvO1xyXG4gIHdpZHRoOiA0ZW07XHJcbn1cclxuXHJcbi5yaWRlIHtcclxuICBjb2xvcjogcmdiYSgyMTksIDIwNSwgOCwgMC45MSk7XHJcbiAgZm9udC1zaXplOiAxLjE3ZW07XHJcbiAgcGFkZGluZy10b3A6IDE0cHg7XHJcbiAgcGFkZGluZy1ib3R0b206IDE0cHg7XHJcblxyXG4gIGJvcmRlci1yYWRpdXM6IDEycHg7XHJcbiAgaW9uLWljb24ge1xyXG4gICAgZm9udC1zaXplOiAwLjhlbTtcclxuICAgIHBhZGRpbmc6IDEycHg7XHJcbiAgICBjb2xvcjogcmdiYSgyMTksIDIwNSwgOCwgMC45MSk7XHJcbiAgfVxyXG59XHJcblxyXG4ucHJpY2Uge1xyXG4gIGNvbG9yOiByZ2JhKDIxOSwgMjA1LCA4LCAwLjkxKTtcclxuICBmb250LXNpemU6IDEuNjdlbTtcclxuICBwYWRkaW5nLXRvcDogMTRweDtcclxuICBwYWRkaW5nLWJvdHRvbTogMTRweDtcclxuXHJcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcclxuICBpb24taWNvbiB7XHJcbiAgICBmb250LXNpemU6IDAuOGVtO1xyXG4gICAgcGFkZGluZzogNXB4O1xyXG4gICAgY29sb3I6IHJnYmEoMjE5LCAyMDUsIDgsIDAuOTEpO1xyXG4gIH1cclxufVxyXG5cclxuLmRhdGUge1xyXG4gIGNvbG9yOiBvcmFuZ2U7XHJcbiAgZm9udC1zaXplOiAxLjQ3ZW07XHJcbiAgcGFkZGluZy10b3A6IDE0cHg7XHJcbiAgcGFkZGluZy1ib3R0b206IDE0cHg7XHJcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHJnYigyMTYsIDIxNiwgMjE2KTtcclxuXHJcbiAgaW9uLWljb24ge1xyXG4gICAgZm9udC1zaXplOiAwLjhlbTtcclxuICAgIHBhZGRpbmc6IDVweDtcclxuICAgIGNvbG9yOiByZ2JhKDIxOSwgMjA1LCA4LCAwLjkxKTtcclxuICB9XHJcbn1cclxuXHJcbi50aW1lIHtcclxuICBjb2xvcjogcmdiKDQ0LCAxMzYsIDI0MSk7XHJcbiAgZm9udC1zaXplOiAxLjE3ZW07XHJcbiAgcGFkZGluZy10b3A6IDE0cHg7XHJcbiAgcGFkZGluZy1ib3R0b206IDE0cHg7XHJcblxyXG4gIGlvbi1pY29uIHtcclxuICAgIGZvbnQtc2l6ZTogMC44ZW07XHJcbiAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICBjb2xvcjogcmdiYSgyMTksIDIwNSwgOCwgMC45MSk7XHJcbiAgfVxyXG59XHJcblxyXG4ubG9jYXRpb24ge1xyXG4gIHdpZHRoOiBhdXRvO1xyXG4gIHBhZGRpbmctdG9wOiA4cHg7XHJcbiAgcGFkZGluZy1ib3R0b206IDhweDtcclxuICBwIHtcclxuICAgIGZvbnQtc2l6ZTogMS4zZW07XHJcbiAgICBoZWlnaHQ6IGF1dG87XHJcbiAgfVxyXG5cclxuICBpb24taWNvbiB7XHJcbiAgICBmb250LXNpemU6IDAuOGVtO1xyXG4gICAgcGFkZGluZzogNXB4O1xyXG4gICAgY29sb3I6IG9yYW5nZTtcclxuICB9XHJcbn1cclxuXHJcbi5kZXN0aW5hdGlvbiB7XHJcbiAgd2lkdGg6IGF1dG87XHJcbiAgcGFkZGluZy10b3A6IDhweDtcclxuICBwYWRkaW5nLWJvdHRvbTogOHB4O1xyXG4gIGlvbi1pY29uIHtcclxuICAgIGZvbnQtc2l6ZTogMC44ZW07XHJcbiAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICBjb2xvcjogZGFya3NsYXRlYmx1ZTtcclxuICB9XHJcbn1cclxuXHJcbi5mb2xsb3dlZC1pdGVtcyB7XHJcbiAgbWFyZ2luLXRvcDogMTAlO1xyXG5cclxuICBpb24taXRlbSB7XHJcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDEycHg7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNik7XHJcbiAgfVxyXG59XHJcblxyXG4uYmFycyB7XHJcbiAgbWFyZ2luLXRvcDogMCU7XHJcbiAgcGFkZGluZzogMTJweDtcclxuXHJcbiAgLnBvaXRlciB7XHJcbiAgICB6LWluZGV4OiA1O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDElO1xyXG4gICAgYmFja2dyb3VuZDogcmdiYSgyNDAsIDI0MCwgMjQwLCAwLjkyKTtcclxuICAgIGJvcmRlci1sZWZ0OiAxLjFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNik7XHJcbiAgICBib3JkZXItcmlnaHQ6IDEuMXB4IHNvbGlkIHJnYigyMTYsIDIxNiwgMjE2KTtcclxuICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNik7XHJcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xyXG4gIH1cclxuXHJcbiAgLmJhcnMtbG9jYXRlIHtcclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYmFja2dyb3VuZDogcmdiYSgyNDAsIDI0MCwgMjQwLCAwLjkyKTtcclxuICAgIGJvcmRlci1sZWZ0OiAxLjFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNik7XHJcbiAgICBib3JkZXItcmlnaHQ6IDEuMXB4IHNvbGlkIHJnYigyMTYsIDIxNiwgMjE2KTtcclxuICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNik7XHJcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDAlO1xyXG5cclxuICAgIHotaW5kZXg6IDM7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xyXG4gICAgZm9udC1zaXplOiAxLjJlbTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHJcbiAgICBpb24taWNvbiB7XHJcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgZm9udC1zaXplOiAxZW07XHJcbiAgICAgIGxlZnQ6IDIlO1xyXG4gICAgICBjb2xvcjogcmdiKDEwLCAxMDAsIDIzNSk7XHJcbiAgICAgIHBhZGRpbmc6IDVweDtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC5iYXJzLWRlc3RpbmF0ZSB7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGJhY2tncm91bmQ6IHJnYmEoMjQwLCAyNDAsIDI0MCwgMC45Mik7XHJcbiAgICBtYXJnaW46IDMlIDAgMCAtNTBweDtcclxuICAgIG1hcmdpbi1sZWZ0OiAwJTtcclxuICAgIHotaW5kZXg6IDM7XHJcbiAgICBib3JkZXItbGVmdDogMS4xcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xyXG4gICAgYm9yZGVyLXJpZ2h0OiAxLjFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNik7XHJcbiAgICBib3JkZXItdG9wOiAxcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHJnYigyMTYsIDIxNiwgMjE2KTtcclxuICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDIwcHg7XHJcbiAgICBmb250LXNpemU6IDEuMmVtO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG5cclxuICAgIGlvbi1pY29uIHtcclxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICBmb250LXNpemU6IDFlbTtcclxuICAgICAgbGVmdDogMiU7XHJcbiAgICAgIHBhZGRpbmc6IDVweDtcclxuICAgICAgY29sb3I6IHJnYmEoMjE5LCAyMDUsIDgsIDAuOTEpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLmJhcnMtcHJpY2Uge1xyXG4gICAgaGVpZ2h0OiA1MHB4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kOiAjZmZmZmZmO1xyXG4gICAgYm9yZGVyLWxlZnQ6IDEuMXB4IHNvbGlkIHJnYigyMTYsIDIxNiwgMjE2KTtcclxuICAgIGJvcmRlci1yaWdodDogMS4xcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xyXG4gICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkIHJnYigyMTYsIDIxNiwgMjE2KTtcclxuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNik7XHJcblxyXG4gICAgei1pbmRleDogMztcclxuICAgIGJvcmRlci1yYWRpdXM6IDEycHg7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgbGluZS1oZWlnaHQ6IDIwcHg7XHJcbiAgICBmb250LXNpemU6IDEuMmVtO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG5cclxuICAgIGlvbi1pY29uIHtcclxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICBmb250LXNpemU6IDFlbTtcclxuICAgICAgbGVmdDogMiU7XHJcbiAgICAgIGNvbG9yOiByZ2IoMTAsIDEwMCwgMjM1KTtcclxuICAgICAgcGFkZGluZzogNXB4O1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuI3Bvc2l0aW9uIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgcGFkZGluZy1sZWZ0OiAxN3B4O1xyXG59XHJcblxyXG4jd2hlcmV0byB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIHBhZGRpbmctbGVmdDogMTdweDtcclxufVxyXG5cclxuI2Nhc2gge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBwYWRkaW5nLWxlZnQ6IDE3cHg7XHJcbn1cclxuXHJcbiNidXR0b24ge1xyXG4gIGJvcmRlci1yYWRpdXM6IDEycHg7XHJcbn1cclxuIiwiLnRvcC1pdGVtcyB7XG4gIGJvcmRlci1yYWRpdXM6IDEycHg7XG4gIG1hcmdpbi10b3A6IDIwJTtcbn1cbi50b3AtaXRlbXMgaW9uLWl0ZW0ge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICBib3JkZXItcmFkaXVzOiAxMnB4O1xuICBiYWNrZ3JvdW5kOiAjMGE2NGViO1xufVxuLnRvcC1pdGVtcyBpb24taXRlbSBpb24tbGFiZWwge1xuICBjb2xvcjogd2hpdGUgIWltcG9ydGFudDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tbGVmdDogMTAlO1xufVxuXG4jZW52ZWxvcGUge1xuICBoZWlnaHQ6IGF1dG87XG4gIHdpZHRoOiA0ZW07XG59XG5cbi5yaWRlIHtcbiAgY29sb3I6IHJnYmEoMjE5LCAyMDUsIDgsIDAuOTEpO1xuICBmb250LXNpemU6IDEuMTdlbTtcbiAgcGFkZGluZy10b3A6IDE0cHg7XG4gIHBhZGRpbmctYm90dG9tOiAxNHB4O1xuICBib3JkZXItcmFkaXVzOiAxMnB4O1xufVxuLnJpZGUgaW9uLWljb24ge1xuICBmb250LXNpemU6IDAuOGVtO1xuICBwYWRkaW5nOiAxMnB4O1xuICBjb2xvcjogcmdiYSgyMTksIDIwNSwgOCwgMC45MSk7XG59XG5cbi5wcmljZSB7XG4gIGNvbG9yOiByZ2JhKDIxOSwgMjA1LCA4LCAwLjkxKTtcbiAgZm9udC1zaXplOiAxLjY3ZW07XG4gIHBhZGRpbmctdG9wOiAxNHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMTRweDtcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcbn1cbi5wcmljZSBpb24taWNvbiB7XG4gIGZvbnQtc2l6ZTogMC44ZW07XG4gIHBhZGRpbmc6IDVweDtcbiAgY29sb3I6IHJnYmEoMjE5LCAyMDUsIDgsIDAuOTEpO1xufVxuXG4uZGF0ZSB7XG4gIGNvbG9yOiBvcmFuZ2U7XG4gIGZvbnQtc2l6ZTogMS40N2VtO1xuICBwYWRkaW5nLXRvcDogMTRweDtcbiAgcGFkZGluZy1ib3R0b206IDE0cHg7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZDhkOGQ4O1xufVxuLmRhdGUgaW9uLWljb24ge1xuICBmb250LXNpemU6IDAuOGVtO1xuICBwYWRkaW5nOiA1cHg7XG4gIGNvbG9yOiByZ2JhKDIxOSwgMjA1LCA4LCAwLjkxKTtcbn1cblxuLnRpbWUge1xuICBjb2xvcjogIzJjODhmMTtcbiAgZm9udC1zaXplOiAxLjE3ZW07XG4gIHBhZGRpbmctdG9wOiAxNHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMTRweDtcbn1cbi50aW1lIGlvbi1pY29uIHtcbiAgZm9udC1zaXplOiAwLjhlbTtcbiAgcGFkZGluZzogNXB4O1xuICBjb2xvcjogcmdiYSgyMTksIDIwNSwgOCwgMC45MSk7XG59XG5cbi5sb2NhdGlvbiB7XG4gIHdpZHRoOiBhdXRvO1xuICBwYWRkaW5nLXRvcDogOHB4O1xuICBwYWRkaW5nLWJvdHRvbTogOHB4O1xufVxuLmxvY2F0aW9uIHAge1xuICBmb250LXNpemU6IDEuM2VtO1xuICBoZWlnaHQ6IGF1dG87XG59XG4ubG9jYXRpb24gaW9uLWljb24ge1xuICBmb250LXNpemU6IDAuOGVtO1xuICBwYWRkaW5nOiA1cHg7XG4gIGNvbG9yOiBvcmFuZ2U7XG59XG5cbi5kZXN0aW5hdGlvbiB7XG4gIHdpZHRoOiBhdXRvO1xuICBwYWRkaW5nLXRvcDogOHB4O1xuICBwYWRkaW5nLWJvdHRvbTogOHB4O1xufVxuLmRlc3RpbmF0aW9uIGlvbi1pY29uIHtcbiAgZm9udC1zaXplOiAwLjhlbTtcbiAgcGFkZGluZzogNXB4O1xuICBjb2xvcjogZGFya3NsYXRlYmx1ZTtcbn1cblxuLmZvbGxvd2VkLWl0ZW1zIHtcbiAgbWFyZ2luLXRvcDogMTAlO1xufVxuLmZvbGxvd2VkLWl0ZW1zIGlvbi1pdGVtIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2Q4ZDhkODtcbn1cblxuLmJhcnMge1xuICBtYXJnaW4tdG9wOiAwJTtcbiAgcGFkZGluZzogMTJweDtcbn1cbi5iYXJzIC5wb2l0ZXIge1xuICB6LWluZGV4OiA1O1xuICBtYXJnaW4tbGVmdDogMSU7XG4gIGJhY2tncm91bmQ6IHJnYmEoMjQwLCAyNDAsIDI0MCwgMC45Mik7XG4gIGJvcmRlci1sZWZ0OiAxLjFweCBzb2xpZCAjZDhkOGQ4O1xuICBib3JkZXItcmlnaHQ6IDEuMXB4IHNvbGlkICNkOGQ4ZDg7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCAjZDhkOGQ4O1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2Q4ZDhkODtcbn1cbi5iYXJzIC5iYXJzLWxvY2F0ZSB7XG4gIGhlaWdodDogNTBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIGJhY2tncm91bmQ6IHJnYmEoMjQwLCAyNDAsIDI0MCwgMC45Mik7XG4gIGJvcmRlci1sZWZ0OiAxLjFweCBzb2xpZCAjZDhkOGQ4O1xuICBib3JkZXItcmlnaHQ6IDEuMXB4IHNvbGlkICNkOGQ4ZDg7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCAjZDhkOGQ4O1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2Q4ZDhkODtcbiAgbWFyZ2luLWxlZnQ6IDAlO1xuICB6LWluZGV4OiAzO1xuICBib3JkZXItcmFkaXVzOiAxMnB4O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBsaW5lLWhlaWdodDogMjBweDtcbiAgZm9udC1zaXplOiAxLjJlbTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLmJhcnMgLmJhcnMtbG9jYXRlIGlvbi1pY29uIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBmb250LXNpemU6IDFlbTtcbiAgbGVmdDogMiU7XG4gIGNvbG9yOiAjMGE2NGViO1xuICBwYWRkaW5nOiA1cHg7XG59XG4uYmFycyAuYmFycy1kZXN0aW5hdGUge1xuICBoZWlnaHQ6IDUwcHg7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kOiByZ2JhKDI0MCwgMjQwLCAyNDAsIDAuOTIpO1xuICBtYXJnaW46IDMlIDAgMCAtNTBweDtcbiAgbWFyZ2luLWxlZnQ6IDAlO1xuICB6LWluZGV4OiAzO1xuICBib3JkZXItbGVmdDogMS4xcHggc29saWQgI2Q4ZDhkODtcbiAgYm9yZGVyLXJpZ2h0OiAxLjFweCBzb2xpZCAjZDhkOGQ4O1xuICBib3JkZXItdG9wOiAxcHggc29saWQgI2Q4ZDhkODtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNkOGQ4ZDg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIGJvcmRlci1yYWRpdXM6IDEycHg7XG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICBmb250LXNpemU6IDEuMmVtO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uYmFycyAuYmFycy1kZXN0aW5hdGUgaW9uLWljb24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGZvbnQtc2l6ZTogMWVtO1xuICBsZWZ0OiAyJTtcbiAgcGFkZGluZzogNXB4O1xuICBjb2xvcjogcmdiYSgyMTksIDIwNSwgOCwgMC45MSk7XG59XG4uYmFycyAuYmFycy1wcmljZSB7XG4gIGhlaWdodDogNTBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIGJhY2tncm91bmQ6ICNmZmZmZmY7XG4gIGJvcmRlci1sZWZ0OiAxLjFweCBzb2xpZCAjZDhkOGQ4O1xuICBib3JkZXItcmlnaHQ6IDEuMXB4IHNvbGlkICNkOGQ4ZDg7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCAjZDhkOGQ4O1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2Q4ZDhkODtcbiAgei1pbmRleDogMztcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgbGluZS1oZWlnaHQ6IDIwcHg7XG4gIGZvbnQtc2l6ZTogMS4yZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5iYXJzIC5iYXJzLXByaWNlIGlvbi1pY29uIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBmb250LXNpemU6IDFlbTtcbiAgbGVmdDogMiU7XG4gIGNvbG9yOiAjMGE2NGViO1xuICBwYWRkaW5nOiA1cHg7XG59XG5cbiNwb3NpdGlvbiB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcGFkZGluZy1sZWZ0OiAxN3B4O1xufVxuXG4jd2hlcmV0byB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcGFkZGluZy1sZWZ0OiAxN3B4O1xufVxuXG4jY2FzaCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcGFkZGluZy1sZWZ0OiAxN3B4O1xufVxuXG4jYnV0dG9uIHtcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/schedule/schedule.page.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/schedule/schedule.page.ts ***!
  \*************************************************/
/*! exports provided: SchedulePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SchedulePage", function() { return SchedulePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/onesignal/ngx */ "./node_modules/@ionic-native/onesignal/ngx/index.js");
/* harmony import */ var src_app_services_language_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/language.service */ "./src/app/services/language.service.ts");
/* harmony import */ var src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/profile.service */ "./src/app/services/profile.service.ts");
/* harmony import */ var src_app_services_directionservice_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/directionservice.service */ "./src/app/services/directionservice.service.ts");
/* harmony import */ var src_app_services_native_map_container_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/native-map-container.service */ "./src/app/services/native-map-container.service.ts");
/* harmony import */ var src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/pop-up.service */ "./src/app/services/pop-up.service.ts");
/* harmony import */ var src_app_services_geocoder_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/services/geocoder.service */ "./src/app/services/geocoder.service.ts");
/* harmony import */ var src_app_services_event_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/app/services/event.service */ "./src/app/services/event.service.ts");
/* harmony import */ var _autocomplete_autocomplete_page__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../autocomplete/autocomplete.page */ "./src/app/pages/autocomplete/autocomplete.page.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");














var SchedulePage = /** @class */ (function () {
    function SchedulePage(navCtrl, actRoute, lp, alertCtrl, platform, ph, urllocation, dProvider, cMap, gCode, One, pop, eventProvider, modalCtrl) {
        this.navCtrl = navCtrl;
        this.actRoute = actRoute;
        this.lp = lp;
        this.alertCtrl = alertCtrl;
        this.platform = platform;
        this.ph = ph;
        this.urllocation = urllocation;
        this.dProvider = dProvider;
        this.cMap = cMap;
        this.gCode = gCode;
        this.One = One;
        this.pop = pop;
        this.eventProvider = eventProvider;
        this.modalCtrl = modalCtrl;
        this.hasBooked = false;
        // tslint:disable-next-line: new-parens
        this.geocoder = new google.maps.Geocoder;
        this.service = new google.maps.DistanceMatrixService();
        this.Lang = this.lp.translate();
    }
    SchedulePage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.pop.presentLoader('');
        this.eventProvider.getScheduledList().on('value', function (snapshot) {
            _this.eventList = [];
            _this.pop.hideLoader();
            console.log('loader hidden');
            snapshot.forEach(function (snap) {
                _this.eventList.push({
                    id: snap.key,
                    date: snap.val().TimeandDate,
                });
                return false;
            });
        });
        if (this.actRoute.snapshot.paramMap.get('destination') != null) {
            console.log(this.actRoute.snapshot.paramMap.get('destination'));
            this.gCode.geocoder.geocode({ address: this.actRoute.snapshot.paramMap.get('destination') }, function (results, status) {
                if (status === 'OK') {
                    var position = results[0].geometry.location;
                    _this.userDes = new google.maps.LatLng(position.lat(), position.lng());
                }
            });
            document.getElementById('whereto').innerText = this.actRoute.snapshot.paramMap.get('destination');
            this.destination = this.actRoute.snapshot.paramMap.get('destination');
        }
        this.lat = this.actRoute.snapshot.paramMap.get('lat');
        this.lng = this.actRoute.snapshot.paramMap.get('lng');
        console.log(this.lat, this.lng);
        this.userPos = new google.maps.LatLng(this.lat, this.lng);
        var latlng = { lat: parseFloat(this.lat), lng: parseFloat(this.lng) };
        this.geocoder.geocode({ location: latlng }, function (results, status) {
            if (status === 'OK') {
                _this.locationName = results[0].formatted_address;
                _this.location = _this.locationName;
            }
            else {
            }
        });
        this.currentYear = new Date().getFullYear();
        this.currentMonth = new Date().getUTCMonth() + 1;
        this.currentDay = new Date().getUTCDate();
        this.ph.getWebAdminProfile().on('value', function (userProfileSnapshot) {
            var admin = userProfileSnapshot.val();
            _this.dProvider.fare = admin.price;
            _this.dProvider.pricePerKm = admin.perkm;
        });
        if (!this.platform.is('cordova')) {
            this.id = '43cd6829-4651-4039-bbc3-aace7fbe7d72';
            this.ph.getScheduledProfile(this.id).on('value', function (userProfileSnapshot) {
                _this.scheduleInfo = userProfileSnapshot.val();
                if (_this.scheduleInfo != null) {
                    var today = new Date();
                    var future = new Date(_this.scheduleInfo.Client_Date);
                    console.log(future);
                    _this.dataTime = _this.calcDate(today, future);
                    console.log(_this.dataTime);
                }
            });
        }
        else {
            this.One.getIds().then(function (id) {
                _this.userID = id.userId;
                _this.ph.getScheduledProfile(_this.userID).on('value', function (userProfileSnapshot) {
                    _this.scheduleInfo = userProfileSnapshot.val();
                    if (_this.scheduleInfo != null) {
                        var today = new Date();
                        var future = new Date(_this.scheduleInfo.Client_Date);
                        console.log(future);
                        _this.dataTime = _this.calcDate(today, future);
                        console.log(_this.dataTime);
                    }
                });
            });
        }
    };
    SchedulePage.prototype.calcDate = function (date1, date2) {
        var diff = Math.floor(date2.getTime() - date1.getTime());
        var day = 1000 * 60 * 60 * 24;
        var days = Math.floor(diff / day);
        var months = Math.floor(days / 31);
        var message = date2.toDateString();
        return message;
    };
    SchedulePage.prototype.Chosen = function (e) {
        if (this.currentYear <= e.year) {
            console.log(this.userPos, this.userDes);
            if (this.userPos != null && this.userDes != null) {
                console.log(e);
                var date = [];
                var time = [];
                date.push(e.year, e.month, e.day);
                time.push(e.hour, e.minute);
                console.log(date);
                this.calcScheduleRoute(this.userPos, this.userDes, this.destination, this.location, date, time);
            }
            else {
                this.pop.showPimp(this.Lang[0].addDest);
            }
        }
        else {
            this.pop.showPimp(this.Lang[0].addTime);
            console.log(this.currentMonth, this.currentDay);
        }
    };
    SchedulePage.prototype.calcScheduleRoute = function (start, stop, destinationName, locationName, date, time) {
        var _this = this;
        this.pop.presentLoader('');
        this.service.getDistanceMatrix({
            origins: [start, locationName],
            destinations: [destinationName, stop],
            travelMode: 'DRIVING',
            unitSystem: google.maps.UnitSystem.METRIC,
            avoidHighways: false,
            avoidTolls: false,
        }, function (response, status) {
            if (status === 'OK') {
                var fareTime = Math.floor(response.rows[0].elements[1].duration.value / 60) * 5.5;
                var price_1 = Math.floor(response.rows[0].elements[1].distance.value / 1000)
                    * _this.dProvider.pricePerKm + _this.dProvider.fare + fareTime;
                console.log(_this.dProvider.pricePerKm, _this.dProvider.fare);
                var image_1 = _this.ph.user.photoURL;
                var name_1 = _this.ph.user.displayName;
                // tslint:disable-next-line: variable-name
                var edited_name = _this.ph.name;
                var pay_1 = _this.ph.paymentType;
                _this.pop.calculateBtn = false;
                if (image_1 == null) {
                    if (_this.ph.pic == null) {
                        image_1 = 'https://cdn1.iconfinder.com/data/icons/instagram-ui-glyph/48/Sed-10-128.png';
                    }
                    else {
                        image_1 = _this.ph.pic;
                    }
                }
                if (name_1 == null) {
                    if (edited_name != null) {
                        name_1 = edited_name;
                    }
                    else {
                        name_1 = _this.ph.user.email;
                    }
                }
                if (pay_1 == null) {
                    pay_1 = 1;
                }
                if (_this.lat == null && _this.lng == null) {
                    _this.lat = _this.actRoute.snapshot.paramMap.get('lat');
                    _this.lng = _this.actRoute.snapshot.paramMap.get('lng');
                }
                if (!_this.platform.is('cordova')) {
                    var id_1 = '43cd6829-4651-4039-bbc3-aace7fbe7d72';
                    _this.ph.getUserProfile().on('value', function (userProfileSnapshot) {
                        var ratingText = userProfileSnapshot.val().ratingtext;
                        var ratingValue = userProfileSnapshot.val().rating;
                        if (ratingText == null && ratingValue == null) {
                            ratingText = _this.Lang[0].notrate;
                            ratingValue = 0;
                        }
                        _this.ph.getUserProfile().off('value');
                        console.log(price_1, name_1, image_1, _this.lat, _this.lng, _this.location, pay_1, _this.destination, _this.ph.phone, date, ratingText, ratingValue, time);
                        _this.eventProvider.CreateSchedule(price_1, name_1, image_1, _this.lat, _this.lng, _this.location, pay_1, _this.destination, _this.ph.phone, date, id_1, ratingText, ratingValue, time).then(function (s) {
                            _this.pop.hideLoader();
                            _this.pop.showPimp('Your ride has been scheduled successfully.');
                            _this.pop.SmartLoader('');
                            _this.ph.getScheduledProfile(_this.id).off('value');
                        });
                    });
                }
                else {
                    _this.One.getIds().then(function (id) {
                        _this.userID = id.userId;
                        _this.ph.getUserProfile().on('value', function (userProfileSnapshot) {
                            var ratingText = userProfileSnapshot.val().ratingtext;
                            var ratingValue = userProfileSnapshot.val().rating;
                            if (ratingText == null && ratingValue == null) {
                                ratingText = _this.Lang[0].notrate;
                                ratingValue = 0;
                            }
                            _this.lat = _this.actRoute.snapshot.paramMap.get('lat');
                            _this.lng = _this.actRoute.snapshot.paramMap.get('lng');
                            _this.ph.getUserProfile().off('value');
                            console.log(price_1, name_1, image_1, _this.lat, _this.lng, _this.location, pay_1, _this.destination, _this.ph.phone, date);
                            _this.eventProvider.CreateSchedule(price_1, name_1, image_1, _this.lat, _this.lng, _this.location, pay_1, _this.destination, _this.ph.phone, date, _this.userID, ratingText, ratingValue, time).then(function (s) {
                                _this.pop.hideLoader();
                                _this.pop.showPimp(_this.Lang[0].sucsch);
                                _this.pop.SmartLoader('');
                                _this.ph.getScheduledProfile(_this.id).off('value');
                            });
                        });
                    });
                }
            }
        });
    };
    SchedulePage.prototype.showAddressModal = function (selectedBar) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalCtrl.create({
                            component: _autocomplete_autocomplete_page__WEBPACK_IMPORTED_MODULE_11__["AutocompletePage"]
                        })];
                    case 1:
                        modal = _a.sent();
                        modal.onDidDismiss()
                            .then(function (data) {
                            // Open the address modal on location bar click to change location
                            console.log(data);
                            if (selectedBar === 1 && data != null) {
                                document.getElementById('position').innerText = data;
                                _this.location = data;
                                _this.gCode.geocoder.geocode({ address: data }, function (results, status) {
                                    if (status === 'OK') {
                                        var position = results[0].geometry.location;
                                        _this.userPos = new google.maps.LatLng(position.lat(), position.lng());
                                        _this.lat = position.lat();
                                        _this.lng = position.lng();
                                    }
                                });
                            }
                            // Open the address modal on destination bar click to change destination
                            if (selectedBar === 2 && data != null) {
                                document.getElementById('whereto').innerText = data;
                                _this.destination = data;
                                /// After data input, check to see if user selected to add a destination or to calculate distance.
                                _this.gCode.geocoder.geocode({ address: data }, function (results, status) {
                                    if (status === 'OK') {
                                        var position = results[0].geometry.location;
                                        _this.userDes = new google.maps.LatLng(position.lat(), position.lng());
                                    }
                                });
                            }
                        });
                        modal.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    SchedulePage.prototype.CancelRide = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            message: this.Lang[0].error,
                            buttons: [
                                {
                                    text: this.Lang[0].reject,
                                },
                                {
                                    text: this.Lang[0].accept,
                                    handler: function (data) {
                                        _this.remove();
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        alert.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    SchedulePage.prototype.remove = function () {
        var _this = this;
        this.One.getIds().then(function (id) {
            _this.userID = id.userId;
            _this.ph.getScheduledProfile(_this.userID).remove().then(function (s) {
                _this.pop.showPimp(_this.Lang[0].sucSchw);
                _this.hasBooked = false;
            });
        });
    };
    SchedulePage.prototype.goBack = function () {
        this.urllocation.back();
    };
    SchedulePage.prototype.ngOnInit = function () {
    };
    SchedulePage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_13__["ActivatedRoute"] },
        { type: src_app_services_language_service__WEBPACK_IMPORTED_MODULE_4__["LanguageService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
        { type: src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_5__["ProfileService"] },
        { type: _angular_common__WEBPACK_IMPORTED_MODULE_12__["Location"] },
        { type: src_app_services_directionservice_service__WEBPACK_IMPORTED_MODULE_6__["DirectionserviceService"] },
        { type: src_app_services_native_map_container_service__WEBPACK_IMPORTED_MODULE_7__["NativeMapContainerService"] },
        { type: src_app_services_geocoder_service__WEBPACK_IMPORTED_MODULE_9__["GeocoderService"] },
        { type: _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_3__["OneSignal"] },
        { type: src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_8__["PopUpService"] },
        { type: src_app_services_event_service__WEBPACK_IMPORTED_MODULE_10__["EventService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] }
    ]; };
    SchedulePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-schedule',
            template: __webpack_require__(/*! raw-loader!./schedule.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/schedule/schedule.page.html"),
            styles: [__webpack_require__(/*! ./schedule.page.scss */ "./src/app/pages/schedule/schedule.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _angular_router__WEBPACK_IMPORTED_MODULE_13__["ActivatedRoute"],
            src_app_services_language_service__WEBPACK_IMPORTED_MODULE_4__["LanguageService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"], src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_5__["ProfileService"], _angular_common__WEBPACK_IMPORTED_MODULE_12__["Location"],
            src_app_services_directionservice_service__WEBPACK_IMPORTED_MODULE_6__["DirectionserviceService"], src_app_services_native_map_container_service__WEBPACK_IMPORTED_MODULE_7__["NativeMapContainerService"],
            src_app_services_geocoder_service__WEBPACK_IMPORTED_MODULE_9__["GeocoderService"], _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_3__["OneSignal"], src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_8__["PopUpService"],
            src_app_services_event_service__WEBPACK_IMPORTED_MODULE_10__["EventService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])
    ], SchedulePage);
    return SchedulePage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-schedule-schedule-module-es5.js.map