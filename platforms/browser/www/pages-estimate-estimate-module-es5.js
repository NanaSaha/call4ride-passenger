(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-estimate-estimate-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/estimate/estimate.page.html":
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/estimate/estimate.page.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header no-border>\n  <ion-toolbar style=\"margin-top: 20px\">\n    <ion-button size=\"large\" fill=\"clear\" (click)=\"goBack()\">\n      <ion-icon name=\"arrow-back\"></ion-icon>\n      <span style=\"padding-left:30px; font-size: 1.0em\"> {{lp.translate()[0].calc}}</span>\n    </ion-button>\n\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding class='yes-scroll'>\n  <!-- These are the location bar and destination bar -->\n\n  <div text-center class=\"whiteFlap\">\n    <div class=\"bars\">\n      <p>{{lp.translate()[0].from}}</p>\n\n      <!-- location bar -->\n      <ion-button lines=\"none\" detail=\"false\" class=\"bars-locate\" (click)=\"showAddressModal(1)\">\n        <ion-icon color='deep' name=\"locate\" slot=\"start\"></ion-icon>\n        <div id=\"position\">{{locationName}}</div>\n      </ion-button>\n      <p>{{lp.translate()[0].to}}</p>\n      <!-- desination bar -->\n      <ion-button lines=\"none\" detail=\"false\" class=\"bars-destinate\" (click)=\"showAddressModal(2)\">\n        <ion-icon color='deep' name=\"flag\" slot=\"start\"></ion-icon>\n        <div id=\"whereto\">{{lp.translate()[0].dest}}</div>\n      </ion-button>\n\n      <p>{{lp.translate()[0].price}}</p>\n      <ion-button lines=\"none\" detail=\"false\" class=\"bars-price\">\n        <ion-icon color='deep' name=\"cash\" slot=\"start\"></ion-icon>\n        <div id=\"cash\">{{lp.translate()[0].priceappear}}</div>\n      </ion-button>\n\n      <p>{{lp.translate()[0].pvc}}</p>\n    </div>\n  </div>\n\n\n\n\n\n\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/estimate/estimate.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/estimate/estimate.module.ts ***!
  \***************************************************/
/*! exports provided: EstimatePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EstimatePageModule", function() { return EstimatePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _estimate_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./estimate.page */ "./src/app/pages/estimate/estimate.page.ts");







var routes = [
    {
        path: '',
        component: _estimate_page__WEBPACK_IMPORTED_MODULE_6__["EstimatePage"]
    }
];
var EstimatePageModule = /** @class */ (function () {
    function EstimatePageModule() {
    }
    EstimatePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_estimate_page__WEBPACK_IMPORTED_MODULE_6__["EstimatePage"]]
        })
    ], EstimatePageModule);
    return EstimatePageModule;
}());



/***/ }),

/***/ "./src/app/pages/estimate/estimate.page.scss":
/*!***************************************************!*\
  !*** ./src/app/pages/estimate/estimate.page.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".top-items {\n  border-radius: 12px;\n  margin-top: 20%;\n}\n.top-items ion-item {\n  margin-top: 10px;\n  margin-bottom: 10px;\n  border-radius: 12px;\n  background: #0a64eb;\n}\n.top-items ion-item ion-label {\n  color: white !important;\n  text-align: center;\n  margin-left: 10%;\n}\n#envelope {\n  height: auto;\n  width: 4em;\n}\n.ride {\n  color: rgba(219, 205, 8, 0.91);\n  font-size: 1.17em;\n  padding-top: 14px;\n  padding-bottom: 14px;\n  border-radius: 12px;\n}\n.ride ion-icon {\n  font-size: 0.8em;\n  padding: 12px;\n  color: rgba(219, 205, 8, 0.91);\n}\n.price {\n  color: rgba(219, 205, 8, 0.91);\n  font-size: 1.67em;\n  padding-top: 14px;\n  padding-bottom: 14px;\n  border-radius: 12px;\n}\n.price ion-icon {\n  font-size: 0.8em;\n  padding: 5px;\n  color: rgba(219, 205, 8, 0.91);\n}\n.date {\n  color: orange;\n  font-size: 1.47em;\n  padding-top: 14px;\n  padding-bottom: 14px;\n  border-bottom: 1px solid #d8d8d8;\n}\n.date ion-icon {\n  font-size: 0.8em;\n  padding: 5px;\n  color: rgba(219, 205, 8, 0.91);\n}\n.time {\n  color: #2c88f1;\n  font-size: 1.17em;\n  padding-top: 14px;\n  padding-bottom: 14px;\n}\n.time ion-icon {\n  font-size: 0.8em;\n  padding: 5px;\n  color: rgba(219, 205, 8, 0.91);\n}\n.location {\n  width: auto;\n  padding-top: 8px;\n  padding-bottom: 8px;\n}\n.location p {\n  font-size: 1.3em;\n  height: auto;\n}\n.location ion-icon {\n  font-size: 0.8em;\n  padding: 5px;\n  color: orange;\n}\n.destination {\n  width: auto;\n  padding-top: 8px;\n  padding-bottom: 8px;\n}\n.destination ion-icon {\n  font-size: 0.8em;\n  padding: 5px;\n  color: darkslateblue;\n}\n.followed-items {\n  margin-top: 10%;\n}\n.followed-items ion-item {\n  margin-top: 10px;\n  margin-bottom: 10px;\n  border-radius: 12px;\n  border: 1px solid #d8d8d8;\n}\n.bars {\n  margin-top: 0%;\n  padding: 12px;\n}\n.bars .poiter {\n  z-index: 5;\n  margin-left: 1%;\n  background: rgba(240, 240, 240, 0.92);\n  border-left: 1.1px solid #d8d8d8;\n  border-right: 1.1px solid #d8d8d8;\n  border-top: 1px solid #d8d8d8;\n  border-bottom: 1px solid #d8d8d8;\n}\n.bars .bars-locate {\n  height: 50px;\n  width: 100%;\n  background: rgba(240, 240, 240, 0.92);\n  border-left: 1.1px solid #d8d8d8;\n  border-right: 1.1px solid #d8d8d8;\n  border-top: 1px solid #d8d8d8;\n  border-bottom: 1px solid #d8d8d8;\n  margin-left: 0%;\n  z-index: 3;\n  border-radius: 12px;\n  overflow: hidden;\n  line-height: 20px;\n  font-size: 1.2em;\n  text-align: center;\n}\n.bars .bars-locate ion-icon {\n  position: absolute;\n  font-size: 1em;\n  left: 2%;\n  color: #0a64eb;\n  padding: 5px;\n}\n.bars .bars-destinate {\n  height: 50px;\n  width: 100%;\n  background: rgba(240, 240, 240, 0.92);\n  margin: 3% 0 0 -50px;\n  margin-left: 0%;\n  z-index: 3;\n  border-left: 1.1px solid #d8d8d8;\n  border-right: 1.1px solid #d8d8d8;\n  border-top: 1px solid #d8d8d8;\n  border-bottom: 1px solid #d8d8d8;\n  overflow: hidden;\n  border-radius: 12px;\n  line-height: 20px;\n  font-size: 1.2em;\n  text-align: center;\n}\n.bars .bars-destinate ion-icon {\n  position: absolute;\n  font-size: 1em;\n  left: 2%;\n  padding: 5px;\n  color: rgba(219, 205, 8, 0.91);\n}\n.bars .bars-price {\n  height: 50px;\n  width: 100%;\n  background: #ffffff;\n  border-left: 1.1px solid #d8d8d8;\n  border-right: 1.1px solid #d8d8d8;\n  border-top: 1px solid #d8d8d8;\n  border-bottom: 1px solid #d8d8d8;\n  z-index: 3;\n  border-radius: 12px;\n  overflow: hidden;\n  line-height: 20px;\n  font-size: 1.2em;\n  text-align: center;\n}\n.bars .bars-price ion-icon {\n  position: absolute;\n  font-size: 1em;\n  left: 2%;\n  color: #0a64eb;\n  padding: 5px;\n}\n#position {\n  text-align: center;\n  padding-left: 17px;\n}\n#whereto {\n  text-align: center;\n  padding-left: 17px;\n}\n#cash {\n  text-align: center;\n  padding-left: 17px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYWhhL0lvbmljQXBwcy9DNFJpZGUtSU9TLTIwMjQvY2FsbDRyaWRlLXBhc3Nlbmdlci9zcmMvYXBwL3BhZ2VzL2VzdGltYXRlL2VzdGltYXRlLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvZXN0aW1hdGUvZXN0aW1hdGUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsbUJBQUE7RUFDQSxlQUFBO0FDQ0Y7QURBRTtFQUNFLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0FDRUo7QURESTtFQUNFLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQ0dOO0FERUE7RUFDRSxZQUFBO0VBQ0EsVUFBQTtBQ0NGO0FERUE7RUFDRSw4QkFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxvQkFBQTtFQUVBLG1CQUFBO0FDQUY7QURDRTtFQUNFLGdCQUFBO0VBQ0EsYUFBQTtFQUNBLDhCQUFBO0FDQ0o7QURHQTtFQUNFLDhCQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0VBRUEsbUJBQUE7QUNERjtBREVFO0VBQ0UsZ0JBQUE7RUFDQSxZQUFBO0VBQ0EsOEJBQUE7QUNBSjtBRElBO0VBQ0UsYUFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxvQkFBQTtFQUNBLGdDQUFBO0FDREY7QURHRTtFQUNFLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLDhCQUFBO0FDREo7QURLQTtFQUNFLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0VBQ0Esb0JBQUE7QUNGRjtBREdFO0VBQ0UsZ0JBQUE7RUFDQSxZQUFBO0VBQ0EsOEJBQUE7QUNESjtBREtBO0VBQ0UsV0FBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7QUNGRjtBREdFO0VBQ0UsZ0JBQUE7RUFDQSxZQUFBO0FDREo7QURJRTtFQUNFLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7QUNGSjtBRE1BO0VBQ0UsV0FBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7QUNIRjtBRElFO0VBQ0UsZ0JBQUE7RUFDQSxZQUFBO0VBQ0Esb0JBQUE7QUNGSjtBRE1BO0VBQ0UsZUFBQTtBQ0hGO0FES0U7RUFDRSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSx5QkFBQTtBQ0hKO0FET0E7RUFDRSxjQUFBO0VBQ0EsYUFBQTtBQ0pGO0FETUU7RUFDRSxVQUFBO0VBQ0EsZUFBQTtFQUNBLHFDQUFBO0VBQ0EsZ0NBQUE7RUFDQSxpQ0FBQTtFQUNBLDZCQUFBO0VBQ0EsZ0NBQUE7QUNKSjtBRE9FO0VBQ0UsWUFBQTtFQUNBLFdBQUE7RUFDQSxxQ0FBQTtFQUNBLGdDQUFBO0VBQ0EsaUNBQUE7RUFDQSw2QkFBQTtFQUNBLGdDQUFBO0VBQ0EsZUFBQTtFQUVBLFVBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FDTko7QURRSTtFQUNFLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLFFBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtBQ05OO0FEVUU7RUFDRSxZQUFBO0VBQ0EsV0FBQTtFQUNBLHFDQUFBO0VBQ0Esb0JBQUE7RUFDQSxlQUFBO0VBQ0EsVUFBQTtFQUNBLGdDQUFBO0VBQ0EsaUNBQUE7RUFDQSw2QkFBQTtFQUNBLGdDQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBQ1JKO0FEVUk7RUFDRSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxRQUFBO0VBQ0EsWUFBQTtFQUNBLDhCQUFBO0FDUk47QURZRTtFQUNFLFlBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQ0FBQTtFQUNBLGlDQUFBO0VBQ0EsNkJBQUE7RUFDQSxnQ0FBQTtFQUdBLFVBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FDWko7QURjSTtFQUNFLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLFFBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtBQ1pOO0FEaUJBO0VBQ0Usa0JBQUE7RUFDQSxrQkFBQTtBQ2RGO0FEaUJBO0VBQ0Usa0JBQUE7RUFDQSxrQkFBQTtBQ2RGO0FEaUJBO0VBQ0Usa0JBQUE7RUFDQSxrQkFBQTtBQ2RGIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvZXN0aW1hdGUvZXN0aW1hdGUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRvcC1pdGVtcyB7XHJcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcclxuICBtYXJnaW4tdG9wOiAyMCU7XHJcbiAgaW9uLWl0ZW0ge1xyXG4gICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG4gICAgYmFja2dyb3VuZDogcmdiKDEwLCAxMDAsIDIzNSk7XHJcbiAgICBpb24tbGFiZWwge1xyXG4gICAgICBjb2xvcjogd2hpdGUgIWltcG9ydGFudDtcclxuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICBtYXJnaW4tbGVmdDogMTAlO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuI2VudmVsb3BlIHtcclxuICBoZWlnaHQ6IGF1dG87XHJcbiAgd2lkdGg6IDRlbTtcclxufVxyXG5cclxuLnJpZGUge1xyXG4gIGNvbG9yOiByZ2JhKDIxOSwgMjA1LCA4LCAwLjkxKTtcclxuICBmb250LXNpemU6IDEuMTdlbTtcclxuICBwYWRkaW5nLXRvcDogMTRweDtcclxuICBwYWRkaW5nLWJvdHRvbTogMTRweDtcclxuXHJcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcclxuICBpb24taWNvbiB7XHJcbiAgICBmb250LXNpemU6IDAuOGVtO1xyXG4gICAgcGFkZGluZzogMTJweDtcclxuICAgIGNvbG9yOiByZ2JhKDIxOSwgMjA1LCA4LCAwLjkxKTtcclxuICB9XHJcbn1cclxuXHJcbi5wcmljZSB7XHJcbiAgY29sb3I6IHJnYmEoMjE5LCAyMDUsIDgsIDAuOTEpO1xyXG4gIGZvbnQtc2l6ZTogMS42N2VtO1xyXG4gIHBhZGRpbmctdG9wOiAxNHB4O1xyXG4gIHBhZGRpbmctYm90dG9tOiAxNHB4O1xyXG5cclxuICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG4gIGlvbi1pY29uIHtcclxuICAgIGZvbnQtc2l6ZTogMC44ZW07XHJcbiAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICBjb2xvcjogcmdiYSgyMTksIDIwNSwgOCwgMC45MSk7XHJcbiAgfVxyXG59XHJcblxyXG4uZGF0ZSB7XHJcbiAgY29sb3I6IG9yYW5nZTtcclxuICBmb250LXNpemU6IDEuNDdlbTtcclxuICBwYWRkaW5nLXRvcDogMTRweDtcclxuICBwYWRkaW5nLWJvdHRvbTogMTRweDtcclxuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xyXG5cclxuICBpb24taWNvbiB7XHJcbiAgICBmb250LXNpemU6IDAuOGVtO1xyXG4gICAgcGFkZGluZzogNXB4O1xyXG4gICAgY29sb3I6IHJnYmEoMjE5LCAyMDUsIDgsIDAuOTEpO1xyXG4gIH1cclxufVxyXG5cclxuLnRpbWUge1xyXG4gIGNvbG9yOiByZ2IoNDQsIDEzNiwgMjQxKTtcclxuICBmb250LXNpemU6IDEuMTdlbTtcclxuICBwYWRkaW5nLXRvcDogMTRweDtcclxuICBwYWRkaW5nLWJvdHRvbTogMTRweDtcclxuICBpb24taWNvbiB7XHJcbiAgICBmb250LXNpemU6IDAuOGVtO1xyXG4gICAgcGFkZGluZzogNXB4O1xyXG4gICAgY29sb3I6IHJnYmEoMjE5LCAyMDUsIDgsIDAuOTEpO1xyXG4gIH1cclxufVxyXG5cclxuLmxvY2F0aW9uIHtcclxuICB3aWR0aDogYXV0bztcclxuICBwYWRkaW5nLXRvcDogOHB4O1xyXG4gIHBhZGRpbmctYm90dG9tOiA4cHg7XHJcbiAgcCB7XHJcbiAgICBmb250LXNpemU6IDEuM2VtO1xyXG4gICAgaGVpZ2h0OiBhdXRvO1xyXG4gIH1cclxuXHJcbiAgaW9uLWljb24ge1xyXG4gICAgZm9udC1zaXplOiAwLjhlbTtcclxuICAgIHBhZGRpbmc6IDVweDtcclxuICAgIGNvbG9yOiBvcmFuZ2U7XHJcbiAgfVxyXG59XHJcblxyXG4uZGVzdGluYXRpb24ge1xyXG4gIHdpZHRoOiBhdXRvO1xyXG4gIHBhZGRpbmctdG9wOiA4cHg7XHJcbiAgcGFkZGluZy1ib3R0b206IDhweDtcclxuICBpb24taWNvbiB7XHJcbiAgICBmb250LXNpemU6IDAuOGVtO1xyXG4gICAgcGFkZGluZzogNXB4O1xyXG4gICAgY29sb3I6IGRhcmtzbGF0ZWJsdWU7XHJcbiAgfVxyXG59XHJcblxyXG4uZm9sbG93ZWQtaXRlbXMge1xyXG4gIG1hcmdpbi10b3A6IDEwJTtcclxuXHJcbiAgaW9uLWl0ZW0ge1xyXG4gICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xyXG4gIH1cclxufVxyXG5cclxuLmJhcnMge1xyXG4gIG1hcmdpbi10b3A6IDAlO1xyXG4gIHBhZGRpbmc6IDEycHg7XHJcblxyXG4gIC5wb2l0ZXIge1xyXG4gICAgei1pbmRleDogNTtcclxuICAgIG1hcmdpbi1sZWZ0OiAxJTtcclxuICAgIGJhY2tncm91bmQ6IHJnYmEoMjQwLCAyNDAsIDI0MCwgMC45Mik7XHJcbiAgICBib3JkZXItbGVmdDogMS4xcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xyXG4gICAgYm9yZGVyLXJpZ2h0OiAxLjFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNik7XHJcbiAgICBib3JkZXItdG9wOiAxcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHJnYigyMTYsIDIxNiwgMjE2KTtcclxuICB9XHJcblxyXG4gIC5iYXJzLWxvY2F0ZSB7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGJhY2tncm91bmQ6IHJnYmEoMjQwLCAyNDAsIDI0MCwgMC45Mik7XHJcbiAgICBib3JkZXItbGVmdDogMS4xcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xyXG4gICAgYm9yZGVyLXJpZ2h0OiAxLjFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNik7XHJcbiAgICBib3JkZXItdG9wOiAxcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHJnYigyMTYsIDIxNiwgMjE2KTtcclxuICAgIG1hcmdpbi1sZWZ0OiAwJTtcclxuXHJcbiAgICB6LWluZGV4OiAzO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTJweDtcclxuICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICBsaW5lLWhlaWdodDogMjBweDtcclxuICAgIGZvbnQtc2l6ZTogMS4yZW07XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcblxyXG4gICAgaW9uLWljb24ge1xyXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgIGZvbnQtc2l6ZTogMWVtO1xyXG4gICAgICBsZWZ0OiAyJTtcclxuICAgICAgY29sb3I6IHJnYigxMCwgMTAwLCAyMzUpO1xyXG4gICAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAuYmFycy1kZXN0aW5hdGUge1xyXG4gICAgaGVpZ2h0OiA1MHB4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDI0MCwgMjQwLCAyNDAsIDAuOTIpO1xyXG4gICAgbWFyZ2luOiAzJSAwIDAgLTUwcHg7XHJcbiAgICBtYXJnaW4tbGVmdDogMCU7XHJcbiAgICB6LWluZGV4OiAzO1xyXG4gICAgYm9yZGVyLWxlZnQ6IDEuMXB4IHNvbGlkIHJnYigyMTYsIDIxNiwgMjE2KTtcclxuICAgIGJvcmRlci1yaWdodDogMS4xcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xyXG4gICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkIHJnYigyMTYsIDIxNiwgMjE2KTtcclxuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNik7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTJweDtcclxuICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xyXG4gICAgZm9udC1zaXplOiAxLjJlbTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHJcbiAgICBpb24taWNvbiB7XHJcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgZm9udC1zaXplOiAxZW07XHJcbiAgICAgIGxlZnQ6IDIlO1xyXG4gICAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICAgIGNvbG9yOiByZ2JhKDIxOSwgMjA1LCA4LCAwLjkxKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC5iYXJzLXByaWNlIHtcclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYmFja2dyb3VuZDogI2ZmZmZmZjtcclxuICAgIGJvcmRlci1sZWZ0OiAxLjFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNik7XHJcbiAgICBib3JkZXItcmlnaHQ6IDEuMXB4IHNvbGlkIHJnYigyMTYsIDIxNiwgMjE2KTtcclxuICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNik7XHJcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xyXG5cclxuXHJcbiAgICB6LWluZGV4OiAzO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTJweDtcclxuICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICBsaW5lLWhlaWdodDogMjBweDtcclxuICAgIGZvbnQtc2l6ZTogMS4yZW07XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcblxyXG4gICAgaW9uLWljb24ge1xyXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgIGZvbnQtc2l6ZTogMWVtO1xyXG4gICAgICBsZWZ0OiAyJTtcclxuICAgICAgY29sb3I6IHJnYigxMCwgMTAwLCAyMzUpO1xyXG4gICAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG4jcG9zaXRpb24ge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBwYWRkaW5nLWxlZnQ6IDE3cHg7XHJcbn1cclxuXHJcbiN3aGVyZXRvIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgcGFkZGluZy1sZWZ0OiAxN3B4O1xyXG59XHJcblxyXG4jY2FzaCB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIHBhZGRpbmctbGVmdDogMTdweDtcclxufVxyXG4iLCIudG9wLWl0ZW1zIHtcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcbiAgbWFyZ2luLXRvcDogMjAlO1xufVxuLnRvcC1pdGVtcyBpb24taXRlbSB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDEycHg7XG4gIGJhY2tncm91bmQ6ICMwYTY0ZWI7XG59XG4udG9wLWl0ZW1zIGlvbi1pdGVtIGlvbi1sYWJlbCB7XG4gIGNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi1sZWZ0OiAxMCU7XG59XG5cbiNlbnZlbG9wZSB7XG4gIGhlaWdodDogYXV0bztcbiAgd2lkdGg6IDRlbTtcbn1cblxuLnJpZGUge1xuICBjb2xvcjogcmdiYSgyMTksIDIwNSwgOCwgMC45MSk7XG4gIGZvbnQtc2l6ZTogMS4xN2VtO1xuICBwYWRkaW5nLXRvcDogMTRweDtcbiAgcGFkZGluZy1ib3R0b206IDE0cHg7XG4gIGJvcmRlci1yYWRpdXM6IDEycHg7XG59XG4ucmlkZSBpb24taWNvbiB7XG4gIGZvbnQtc2l6ZTogMC44ZW07XG4gIHBhZGRpbmc6IDEycHg7XG4gIGNvbG9yOiByZ2JhKDIxOSwgMjA1LCA4LCAwLjkxKTtcbn1cblxuLnByaWNlIHtcbiAgY29sb3I6IHJnYmEoMjE5LCAyMDUsIDgsIDAuOTEpO1xuICBmb250LXNpemU6IDEuNjdlbTtcbiAgcGFkZGluZy10b3A6IDE0cHg7XG4gIHBhZGRpbmctYm90dG9tOiAxNHB4O1xuICBib3JkZXItcmFkaXVzOiAxMnB4O1xufVxuLnByaWNlIGlvbi1pY29uIHtcbiAgZm9udC1zaXplOiAwLjhlbTtcbiAgcGFkZGluZzogNXB4O1xuICBjb2xvcjogcmdiYSgyMTksIDIwNSwgOCwgMC45MSk7XG59XG5cbi5kYXRlIHtcbiAgY29sb3I6IG9yYW5nZTtcbiAgZm9udC1zaXplOiAxLjQ3ZW07XG4gIHBhZGRpbmctdG9wOiAxNHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMTRweDtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNkOGQ4ZDg7XG59XG4uZGF0ZSBpb24taWNvbiB7XG4gIGZvbnQtc2l6ZTogMC44ZW07XG4gIHBhZGRpbmc6IDVweDtcbiAgY29sb3I6IHJnYmEoMjE5LCAyMDUsIDgsIDAuOTEpO1xufVxuXG4udGltZSB7XG4gIGNvbG9yOiAjMmM4OGYxO1xuICBmb250LXNpemU6IDEuMTdlbTtcbiAgcGFkZGluZy10b3A6IDE0cHg7XG4gIHBhZGRpbmctYm90dG9tOiAxNHB4O1xufVxuLnRpbWUgaW9uLWljb24ge1xuICBmb250LXNpemU6IDAuOGVtO1xuICBwYWRkaW5nOiA1cHg7XG4gIGNvbG9yOiByZ2JhKDIxOSwgMjA1LCA4LCAwLjkxKTtcbn1cblxuLmxvY2F0aW9uIHtcbiAgd2lkdGg6IGF1dG87XG4gIHBhZGRpbmctdG9wOiA4cHg7XG4gIHBhZGRpbmctYm90dG9tOiA4cHg7XG59XG4ubG9jYXRpb24gcCB7XG4gIGZvbnQtc2l6ZTogMS4zZW07XG4gIGhlaWdodDogYXV0bztcbn1cbi5sb2NhdGlvbiBpb24taWNvbiB7XG4gIGZvbnQtc2l6ZTogMC44ZW07XG4gIHBhZGRpbmc6IDVweDtcbiAgY29sb3I6IG9yYW5nZTtcbn1cblxuLmRlc3RpbmF0aW9uIHtcbiAgd2lkdGg6IGF1dG87XG4gIHBhZGRpbmctdG9wOiA4cHg7XG4gIHBhZGRpbmctYm90dG9tOiA4cHg7XG59XG4uZGVzdGluYXRpb24gaW9uLWljb24ge1xuICBmb250LXNpemU6IDAuOGVtO1xuICBwYWRkaW5nOiA1cHg7XG4gIGNvbG9yOiBkYXJrc2xhdGVibHVlO1xufVxuXG4uZm9sbG93ZWQtaXRlbXMge1xuICBtYXJnaW4tdG9wOiAxMCU7XG59XG4uZm9sbG93ZWQtaXRlbXMgaW9uLWl0ZW0ge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICBib3JkZXItcmFkaXVzOiAxMnB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjZDhkOGQ4O1xufVxuXG4uYmFycyB7XG4gIG1hcmdpbi10b3A6IDAlO1xuICBwYWRkaW5nOiAxMnB4O1xufVxuLmJhcnMgLnBvaXRlciB7XG4gIHotaW5kZXg6IDU7XG4gIG1hcmdpbi1sZWZ0OiAxJTtcbiAgYmFja2dyb3VuZDogcmdiYSgyNDAsIDI0MCwgMjQwLCAwLjkyKTtcbiAgYm9yZGVyLWxlZnQ6IDEuMXB4IHNvbGlkICNkOGQ4ZDg7XG4gIGJvcmRlci1yaWdodDogMS4xcHggc29saWQgI2Q4ZDhkODtcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNkOGQ4ZDg7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZDhkOGQ4O1xufVxuLmJhcnMgLmJhcnMtbG9jYXRlIHtcbiAgaGVpZ2h0OiA1MHB4O1xuICB3aWR0aDogMTAwJTtcbiAgYmFja2dyb3VuZDogcmdiYSgyNDAsIDI0MCwgMjQwLCAwLjkyKTtcbiAgYm9yZGVyLWxlZnQ6IDEuMXB4IHNvbGlkICNkOGQ4ZDg7XG4gIGJvcmRlci1yaWdodDogMS4xcHggc29saWQgI2Q4ZDhkODtcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNkOGQ4ZDg7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZDhkOGQ4O1xuICBtYXJnaW4tbGVmdDogMCU7XG4gIHotaW5kZXg6IDM7XG4gIGJvcmRlci1yYWRpdXM6IDEycHg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICBmb250LXNpemU6IDEuMmVtO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uYmFycyAuYmFycy1sb2NhdGUgaW9uLWljb24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGZvbnQtc2l6ZTogMWVtO1xuICBsZWZ0OiAyJTtcbiAgY29sb3I6ICMwYTY0ZWI7XG4gIHBhZGRpbmc6IDVweDtcbn1cbi5iYXJzIC5iYXJzLWRlc3RpbmF0ZSB7XG4gIGhlaWdodDogNTBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIGJhY2tncm91bmQ6IHJnYmEoMjQwLCAyNDAsIDI0MCwgMC45Mik7XG4gIG1hcmdpbjogMyUgMCAwIC01MHB4O1xuICBtYXJnaW4tbGVmdDogMCU7XG4gIHotaW5kZXg6IDM7XG4gIGJvcmRlci1sZWZ0OiAxLjFweCBzb2xpZCAjZDhkOGQ4O1xuICBib3JkZXItcmlnaHQ6IDEuMXB4IHNvbGlkICNkOGQ4ZDg7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCAjZDhkOGQ4O1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2Q4ZDhkODtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcbiAgbGluZS1oZWlnaHQ6IDIwcHg7XG4gIGZvbnQtc2l6ZTogMS4yZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5iYXJzIC5iYXJzLWRlc3RpbmF0ZSBpb24taWNvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgZm9udC1zaXplOiAxZW07XG4gIGxlZnQ6IDIlO1xuICBwYWRkaW5nOiA1cHg7XG4gIGNvbG9yOiByZ2JhKDIxOSwgMjA1LCA4LCAwLjkxKTtcbn1cbi5iYXJzIC5iYXJzLXByaWNlIHtcbiAgaGVpZ2h0OiA1MHB4O1xuICB3aWR0aDogMTAwJTtcbiAgYmFja2dyb3VuZDogI2ZmZmZmZjtcbiAgYm9yZGVyLWxlZnQ6IDEuMXB4IHNvbGlkICNkOGQ4ZDg7XG4gIGJvcmRlci1yaWdodDogMS4xcHggc29saWQgI2Q4ZDhkODtcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNkOGQ4ZDg7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZDhkOGQ4O1xuICB6LWluZGV4OiAzO1xuICBib3JkZXItcmFkaXVzOiAxMnB4O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBsaW5lLWhlaWdodDogMjBweDtcbiAgZm9udC1zaXplOiAxLjJlbTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLmJhcnMgLmJhcnMtcHJpY2UgaW9uLWljb24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGZvbnQtc2l6ZTogMWVtO1xuICBsZWZ0OiAyJTtcbiAgY29sb3I6ICMwYTY0ZWI7XG4gIHBhZGRpbmc6IDVweDtcbn1cblxuI3Bvc2l0aW9uIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwYWRkaW5nLWxlZnQ6IDE3cHg7XG59XG5cbiN3aGVyZXRvIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwYWRkaW5nLWxlZnQ6IDE3cHg7XG59XG5cbiNjYXNoIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwYWRkaW5nLWxlZnQ6IDE3cHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/estimate/estimate.page.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/estimate/estimate.page.ts ***!
  \*************************************************/
/*! exports provided: EstimatePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EstimatePage", function() { return EstimatePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/onesignal/ngx */ "./node_modules/@ionic-native/onesignal/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/profile.service */ "./src/app/services/profile.service.ts");
/* harmony import */ var src_app_services_language_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/language.service */ "./src/app/services/language.service.ts");
/* harmony import */ var src_app_services_directionservice_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/directionservice.service */ "./src/app/services/directionservice.service.ts");
/* harmony import */ var src_app_services_native_map_container_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/native-map-container.service */ "./src/app/services/native-map-container.service.ts");
/* harmony import */ var src_app_services_geocoder_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/geocoder.service */ "./src/app/services/geocoder.service.ts");
/* harmony import */ var src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/services/pop-up.service */ "./src/app/services/pop-up.service.ts");
/* harmony import */ var src_app_services_event_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/app/services/event.service */ "./src/app/services/event.service.ts");
/* harmony import */ var _autocomplete_autocomplete_page__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../autocomplete/autocomplete.page */ "./src/app/pages/autocomplete/autocomplete.page.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");















var EstimatePage = /** @class */ (function () {
    function EstimatePage(navCtrl, lp, actRoute, alertCtrl, platform, ph, dProvider, cMap, gCode, One, pop, eventProvider, modalCtrl, urllocation) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.lp = lp;
        this.actRoute = actRoute;
        this.alertCtrl = alertCtrl;
        this.platform = platform;
        this.ph = ph;
        this.dProvider = dProvider;
        this.cMap = cMap;
        this.gCode = gCode;
        this.One = One;
        this.pop = pop;
        this.eventProvider = eventProvider;
        this.modalCtrl = modalCtrl;
        this.urllocation = urllocation;
        this.hasBooked = false;
        // tslint:disable-next-line: new-parens
        this.geocoder = new google.maps.Geocoder;
        this.service = new google.maps.DistanceMatrixService();
        this.lat = this.actRoute.snapshot.paramMap.get('lat');
        this.lng = this.actRoute.snapshot.paramMap.get('lng');
        this.userPos = new google.maps.LatLng(this.lat, this.lng);
        var latlng = { lat: parseFloat(this.lat), lng: parseFloat(this.lng) };
        this.geocoder.geocode({ location: latlng }, function (results, status) {
            if (status === 'OK') {
                _this.locationName = results[0].formatted_address;
                _this.location = _this.locationName;
            }
            else {
            }
        });
        this.ph.getWebAdminProfile().on('value', function (userProfileSnapshot) {
            var admin = userProfileSnapshot.val();
            _this.dProvider.fare = admin.price;
            _this.dProvider.pricePerKm = admin.perkm;
        });
    }
    EstimatePage.prototype.Calculate = function () {
        if (this.userPos != null && this.userDes != null) {
            this.calcScheduleRoute(this.userPos, this.userDes, this.destination, this.location);
        }
        else {
            this.pop.showPimp('Please Add your Destination and Location');
        }
    };
    EstimatePage.prototype.calcScheduleRoute = function (start, stop, destinationName, locationName) {
        var _this = this;
        this.pop.presentLoader('Processing....');
        this.service.getDistanceMatrix({
            origins: [start, locationName],
            destinations: [destinationName, stop],
            travelMode: 'DRIVING',
            unitSystem: google.maps.UnitSystem.METRIC,
            avoidHighways: false,
            avoidTolls: false,
        }, function (response, status) {
            if (status === 'OK') {
                var fareTime = Math.floor(response.rows[0].elements[1].duration.value / 60) * 5.5;
                var price = Math.floor(response.rows[0].elements[1].distance.value / 1000)
                    * _this.dProvider.pricePerKm + _this.dProvider.fare + fareTime;
                console.log(_this.dProvider.pricePerKm, _this.dProvider.fare);
                document.getElementById('cash').innerText = price;
                _this.pop.hideLoader();
                console.log(price);
            }
        });
    };
    EstimatePage.prototype.goBack = function () {
        this.urllocation.back();
    };
    EstimatePage.prototype.showAddressModal = function (selectedBar) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalCtrl.create({
                            component: _autocomplete_autocomplete_page__WEBPACK_IMPORTED_MODULE_11__["AutocompletePage"],
                            componentProps: ({ item: this.items })
                        })];
                    case 1:
                        modal = _a.sent();
                        modal.onDidDismiss()
                            .then(function (data) {
                            // Open the address modal on location bar click to change location
                            var item = data['data'];
                            console.log(data);
                            if (selectedBar === 1 && item != null) {
                                document.getElementById('position').innerText = item;
                                _this.location = item;
                                _this.gCode.geocoder.geocode({ address: item }, function (results, status) {
                                    if (status === 'OK') {
                                        var position = results[0].geometry.location;
                                        _this.userPos = new google.maps.LatLng(position.lat(), position.lng());
                                        _this.lat = position.lat();
                                        _this.lng = position.lng();
                                    }
                                });
                            }
                            // Open the address modal on destination bar click to change destination
                            if (selectedBar === 2 && item != null) {
                                document.getElementById('whereto').innerText = item;
                                _this.destination = item;
                                /// After data input, check to see if user selected to add a destination or to calculate distance.
                                _this.gCode.geocoder.geocode({ address: item }, function (results, status) {
                                    if (status === 'OK') {
                                        var position = results[0].geometry.location;
                                        _this.userDes = new google.maps.LatLng(position.lat(), position.lng());
                                        _this.Calculate();
                                    }
                                });
                            }
                        });
                        modal.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    EstimatePage.prototype.ngOnInit = function () {
    };
    EstimatePage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
        { type: src_app_services_language_service__WEBPACK_IMPORTED_MODULE_5__["LanguageService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_13__["ActivatedRoute"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"] },
        { type: src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_4__["ProfileService"] },
        { type: src_app_services_directionservice_service__WEBPACK_IMPORTED_MODULE_6__["DirectionserviceService"] },
        { type: src_app_services_native_map_container_service__WEBPACK_IMPORTED_MODULE_7__["NativeMapContainerService"] },
        { type: src_app_services_geocoder_service__WEBPACK_IMPORTED_MODULE_8__["GeocoderService"] },
        { type: _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_2__["OneSignal"] },
        { type: src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_9__["PopUpService"] },
        { type: src_app_services_event_service__WEBPACK_IMPORTED_MODULE_10__["EventService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] },
        { type: _angular_common__WEBPACK_IMPORTED_MODULE_12__["Location"] }
    ]; };
    EstimatePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-estimate',
            template: __webpack_require__(/*! raw-loader!./estimate.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/estimate/estimate.page.html"),
            styles: [__webpack_require__(/*! ./estimate.page.scss */ "./src/app/pages/estimate/estimate.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"], src_app_services_language_service__WEBPACK_IMPORTED_MODULE_5__["LanguageService"], _angular_router__WEBPACK_IMPORTED_MODULE_13__["ActivatedRoute"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"], src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_4__["ProfileService"],
            src_app_services_directionservice_service__WEBPACK_IMPORTED_MODULE_6__["DirectionserviceService"], src_app_services_native_map_container_service__WEBPACK_IMPORTED_MODULE_7__["NativeMapContainerService"],
            src_app_services_geocoder_service__WEBPACK_IMPORTED_MODULE_8__["GeocoderService"], _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_2__["OneSignal"], src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_9__["PopUpService"],
            src_app_services_event_service__WEBPACK_IMPORTED_MODULE_10__["EventService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"], _angular_common__WEBPACK_IMPORTED_MODULE_12__["Location"]])
    ], EstimatePage);
    return EstimatePage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-estimate-estimate-module-es5.js.map