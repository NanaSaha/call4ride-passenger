(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-referride-referride-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/referride/referride.page.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/referride/referride.page.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header no-border>\n\n\n  <ion-title>Your Ride Referal Code</ion-title>\n\n\n</ion-header>\n\n<ion-content class=\"no-scroll\" padding>\n\n  <div text-center class=\"whiteFlap\">\n\n    <div class='bars'>\n\n      <ion-button no-lines detail-none ion-item icon-start class=\"bars-price\">\n        <ion-icon color='primary' name=\"key\"></ion-icon>\n        <div id=\"cash\">{{randomCode}}</div>\n      </ion-button>\n    </div>\n\n    <p>Share app with others. using this referal code. To get a free ride.</p>\n    <!-- <ion-grid>\n          <ion-row>\n            <ion-col> -->\n    <ion-button [ngStyle]=\"{'margin-top': 20 + 'px'}\" color=\"primary\" expand=\"block\" (click)=\"FaceShare()\">\n      <ion-icon color='nav-color' name=\"share\" slot=\" icon-only \"> Share </ion-icon>\n    </ion-button>\n    <!-- </ion-col>\n            <ion-col>\n                <button icon-only [ngStyle]=\"{'margin-top': 20 + 'px'}\" color=\"gery\" block (click)=\"WhatsappShare()\">\n                    <ion-icon color='nav-color' name=\"logo-whatsapp\"></ion-icon>\n               </button>         \n            </ion-col>\n            <ion-col>\n                <button icon-only [ngStyle]=\"{'margin-top': 20 + 'px'}\" color=\"newColor\" block (click)=\"InstaShare()\">\n                    <ion-icon color='nav-color' name=\"logo-instagram\"></ion-icon>\n               </button>         \n            </ion-col>\n          </ion-row>\n        </ion-grid> -->\n\n  </div>\n\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/referride/referride.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/referride/referride.module.ts ***!
  \*****************************************************/
/*! exports provided: ReferridePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReferridePageModule", function() { return ReferridePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _referride_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./referride.page */ "./src/app/pages/referride/referride.page.ts");







const routes = [
    {
        path: '',
        component: _referride_page__WEBPACK_IMPORTED_MODULE_6__["ReferridePage"]
    }
];
let ReferridePageModule = class ReferridePageModule {
};
ReferridePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_referride_page__WEBPACK_IMPORTED_MODULE_6__["ReferridePage"]]
    })
], ReferridePageModule);



/***/ }),

/***/ "./src/app/pages/referride/referride.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/pages/referride/referride.page.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "form {\n  margin-bottom: 26px;\n  height: 200px auto;\n  background: white;\n  padding: 15px;\n  border-radius: 12px;\n  margin-top: 20px;\n  -webkit-animation-direction: alternate;\n          animation-direction: alternate;\n  -webkit-animation-name: wiki;\n          animation-name: wiki;\n  -webkit-animation-duration: 0.3s;\n          animation-duration: 0.3s;\n  -webkit-animation-iteration-count: 1;\n          animation-iteration-count: 1;\n  border: 1px solid #d8d8d8;\n}\nform button {\n  margin-top: 20px !important;\n}\nform .button {\n  border: 1px solid #0a64eb;\n  padding: 10px;\n  background: #0a64eb;\n  color: #eeeeee;\n}\nion-label {\n  text-align: center;\n}\nion-input {\n  padding: 6px;\n  font-size: 1.3em;\n}\n.invalid {\n  border: 1px solid #ff6153;\n}\n.error-message .item-inner {\n  border-bottom: 0 !important;\n}\n.button {\n  border-radius: 12px;\n  height: 50px;\n}\n.butt {\n  display: inline-table;\n  height: 50px;\n  width: 90%;\n}\n#envelope {\n  height: auto;\n  width: 6em;\n}\n.bars {\n  margin-top: 0%;\n  padding: 12px;\n}\n.bars .bars-price {\n  height: 50px;\n  width: 100%;\n  background: #ffffff;\n  border-left: 1.1px solid #d8d8d8;\n  border-right: 1.1px solid #d8d8d8;\n  border-top: 1px solid #d8d8d8;\n  border-bottom: 1px solid #d8d8d8;\n  z-index: 3;\n  border-radius: 12px;\n  overflow: hidden;\n  line-height: 20px;\n  font-size: 1.2em;\n  text-align: center;\n}\n.bars .bars-price ion-icon {\n  position: absolute;\n  font-size: 1em;\n  left: 2%;\n  color: #0a64eb;\n  padding: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYWhhL0lvbmljQXBwcy9DNFJpZGUtSU9TLTIwMjQvY2FsbDRyaWRlLXBhc3Nlbmdlci9zcmMvYXBwL3BhZ2VzL3JlZmVycmlkZS9yZWZlcnJpZGUucGFnZS5zY3NzIiwic3JjL2FwcC9wYWdlcy9yZWZlcnJpZGUvcmVmZXJyaWRlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0Esc0NBQUE7VUFBQSw4QkFBQTtFQUNBLDRCQUFBO1VBQUEsb0JBQUE7RUFDQSxnQ0FBQTtVQUFBLHdCQUFBO0VBQ0Esb0NBQUE7VUFBQSw0QkFBQTtFQUNBLHlCQUFBO0FDQ0Y7QURBRTtFQUNFLDJCQUFBO0FDRUo7QURBRTtFQUNFLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtBQ0VKO0FERUE7RUFDRSxrQkFBQTtBQ0NGO0FERUE7RUFDRSxZQUFBO0VBQ0EsZ0JBQUE7QUNDRjtBREVBO0VBQ0UseUJBQUE7QUNDRjtBREVBO0VBQ0UsMkJBQUE7QUNDRjtBREVBO0VBQ0UsbUJBQUE7RUFDQSxZQUFBO0FDQ0Y7QURDQTtFQUNFLHFCQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7QUNFRjtBRENBO0VBQ0UsWUFBQTtFQUNBLFVBQUE7QUNFRjtBRENBO0VBQ0UsY0FBQTtFQUNBLGFBQUE7QUNFRjtBREFFO0VBQ0UsWUFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtFQUNBLGdDQUFBO0VBQ0EsaUNBQUE7RUFDQSw2QkFBQTtFQUNBLGdDQUFBO0VBR0EsVUFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUNBSjtBREVJO0VBQ0Usa0JBQUE7RUFDQSxjQUFBO0VBQ0EsUUFBQTtFQUNBLGNBQUE7RUFDQSxZQUFBO0FDQU4iLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9yZWZlcnJpZGUvcmVmZXJyaWRlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImZvcm0ge1xyXG4gIG1hcmdpbi1ib3R0b206IDI2cHg7XHJcbiAgaGVpZ2h0OiAyMDBweCBhdXRvO1xyXG4gIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gIHBhZGRpbmc6IDE1cHg7XHJcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcclxuICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gIGFuaW1hdGlvbi1kaXJlY3Rpb246IGFsdGVybmF0ZTtcclxuICBhbmltYXRpb24tbmFtZTogd2lraTtcclxuICBhbmltYXRpb24tZHVyYXRpb246IDAuM3M7XHJcbiAgYW5pbWF0aW9uLWl0ZXJhdGlvbi1jb3VudDogMTtcclxuICBib3JkZXI6IDFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNik7XHJcbiAgYnV0dG9uIHtcclxuICAgIG1hcmdpbi10b3A6IDIwcHggIWltcG9ydGFudDtcclxuICB9XHJcbiAgLmJ1dHRvbiB7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCByZ2IoMTAsIDEwMCwgMjM1KTtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2IoMTAsIDEwMCwgMjM1KTtcclxuICAgIGNvbG9yOiAjZWVlZWVlO1xyXG4gIH1cclxufVxyXG5cclxuaW9uLWxhYmVsIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbmlvbi1pbnB1dCB7XHJcbiAgcGFkZGluZzogNnB4O1xyXG4gIGZvbnQtc2l6ZTogMS4zZW07XHJcbn1cclxuXHJcbi5pbnZhbGlkIHtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjZmY2MTUzO1xyXG59XHJcblxyXG4uZXJyb3ItbWVzc2FnZSAuaXRlbS1pbm5lciB7XHJcbiAgYm9yZGVyLWJvdHRvbTogMCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uYnV0dG9uIHtcclxuICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG4gIGhlaWdodDogNTBweDtcclxufVxyXG4uYnV0dCB7XHJcbiAgZGlzcGxheTogaW5saW5lLXRhYmxlO1xyXG4gIGhlaWdodDogNTBweDtcclxuICB3aWR0aDogOTAlO1xyXG59XHJcblxyXG4jZW52ZWxvcGUge1xyXG4gIGhlaWdodDogYXV0bztcclxuICB3aWR0aDogNmVtO1xyXG59XHJcblxyXG4uYmFycyB7XHJcbiAgbWFyZ2luLXRvcDogMCU7XHJcbiAgcGFkZGluZzogMTJweDtcclxuXHJcbiAgLmJhcnMtcHJpY2Uge1xyXG4gICAgaGVpZ2h0OiA1MHB4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kOiAjZmZmZmZmO1xyXG4gICAgYm9yZGVyLWxlZnQ6IDEuMXB4IHNvbGlkIHJnYigyMTYsIDIxNiwgMjE2KTtcclxuICAgIGJvcmRlci1yaWdodDogMS4xcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xyXG4gICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkIHJnYigyMTYsIDIxNiwgMjE2KTtcclxuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNik7XHJcbiAgIFxyXG5cclxuICAgIHotaW5kZXg6IDM7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xyXG4gICAgZm9udC1zaXplOiAxLjJlbTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHJcbiAgICBpb24taWNvbiB7XHJcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgZm9udC1zaXplOiAxZW07XHJcbiAgICAgIGxlZnQ6IDIlO1xyXG4gICAgICBjb2xvcjogcmdiKDEwLCAxMDAsIDIzNSk7XHJcbiAgICAgIHBhZGRpbmc6IDVweDtcclxuICAgIH1cclxuICB9XHJcbn1cclxuIiwiZm9ybSB7XG4gIG1hcmdpbi1ib3R0b206IDI2cHg7XG4gIGhlaWdodDogMjAwcHggYXV0bztcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHBhZGRpbmc6IDE1cHg7XG4gIGJvcmRlci1yYWRpdXM6IDEycHg7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG4gIGFuaW1hdGlvbi1kaXJlY3Rpb246IGFsdGVybmF0ZTtcbiAgYW5pbWF0aW9uLW5hbWU6IHdpa2k7XG4gIGFuaW1hdGlvbi1kdXJhdGlvbjogMC4zcztcbiAgYW5pbWF0aW9uLWl0ZXJhdGlvbi1jb3VudDogMTtcbiAgYm9yZGVyOiAxcHggc29saWQgI2Q4ZDhkODtcbn1cbmZvcm0gYnV0dG9uIHtcbiAgbWFyZ2luLXRvcDogMjBweCAhaW1wb3J0YW50O1xufVxuZm9ybSAuYnV0dG9uIHtcbiAgYm9yZGVyOiAxcHggc29saWQgIzBhNjRlYjtcbiAgcGFkZGluZzogMTBweDtcbiAgYmFja2dyb3VuZDogIzBhNjRlYjtcbiAgY29sb3I6ICNlZWVlZWU7XG59XG5cbmlvbi1sYWJlbCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuaW9uLWlucHV0IHtcbiAgcGFkZGluZzogNnB4O1xuICBmb250LXNpemU6IDEuM2VtO1xufVxuXG4uaW52YWxpZCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNmZjYxNTM7XG59XG5cbi5lcnJvci1tZXNzYWdlIC5pdGVtLWlubmVyIHtcbiAgYm9yZGVyLWJvdHRvbTogMCAhaW1wb3J0YW50O1xufVxuXG4uYnV0dG9uIHtcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcbiAgaGVpZ2h0OiA1MHB4O1xufVxuXG4uYnV0dCB7XG4gIGRpc3BsYXk6IGlubGluZS10YWJsZTtcbiAgaGVpZ2h0OiA1MHB4O1xuICB3aWR0aDogOTAlO1xufVxuXG4jZW52ZWxvcGUge1xuICBoZWlnaHQ6IGF1dG87XG4gIHdpZHRoOiA2ZW07XG59XG5cbi5iYXJzIHtcbiAgbWFyZ2luLXRvcDogMCU7XG4gIHBhZGRpbmc6IDEycHg7XG59XG4uYmFycyAuYmFycy1wcmljZSB7XG4gIGhlaWdodDogNTBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIGJhY2tncm91bmQ6ICNmZmZmZmY7XG4gIGJvcmRlci1sZWZ0OiAxLjFweCBzb2xpZCAjZDhkOGQ4O1xuICBib3JkZXItcmlnaHQ6IDEuMXB4IHNvbGlkICNkOGQ4ZDg7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCAjZDhkOGQ4O1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2Q4ZDhkODtcbiAgei1pbmRleDogMztcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgbGluZS1oZWlnaHQ6IDIwcHg7XG4gIGZvbnQtc2l6ZTogMS4yZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5iYXJzIC5iYXJzLXByaWNlIGlvbi1pY29uIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBmb250LXNpemU6IDFlbTtcbiAgbGVmdDogMiU7XG4gIGNvbG9yOiAjMGE2NGViO1xuICBwYWRkaW5nOiA1cHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/referride/referride.page.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/referride/referride.page.ts ***!
  \***************************************************/
/*! exports provided: ReferridePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReferridePage", function() { return ReferridePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/profile.service */ "./src/app/services/profile.service.ts");
/* harmony import */ var _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/social-sharing/ngx */ "./node_modules/@ionic-native/social-sharing/ngx/index.js");
/* harmony import */ var src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/pop-up.service */ "./src/app/services/pop-up.service.ts");
/* harmony import */ var src_app_services_language_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/language.service */ "./src/app/services/language.service.ts");
/* harmony import */ var src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/settings.service */ "./src/app/services/settings.service.ts");
/* harmony import */ var src_app_services_event_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/services/event.service */ "./src/app/services/event.service.ts");










let ReferridePage = class ReferridePage {
    constructor(ph, share, lp, pop, eProvider, platform, nav, navParams, loadingCtrl, alertCtrl, settings, formBuilder) {
        this.ph = ph;
        this.share = share;
        this.lp = lp;
        this.pop = pop;
        this.eProvider = eProvider;
        this.platform = platform;
        this.nav = nav;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.settings = settings;
        this.formBuilder = formBuilder;
        this.randomCode = 'Getting id...';
        this.message = null;
        this.file = null;
        this.link = null;
        this.subject = null;
        this.pop.presentLoader('Getting referal Earnings');
        this.ph.getUserProfile().on('value', userProfileSnapshot => {
            if (userProfileSnapshot.val()) {
                this.randomCode = userProfileSnapshot.val().idForRide;
            }
            this.pop.hideLoader();
        });
    }
    ionViewDidEnter() {
        this.message = 'Use ' + this.randomCode + ' as referal code to Register in  ' + this.settings.appName;
    }
    FaceShare() {
        this.share.share(this.message, this.subject, this.file, this.link).then(() => {
        }).catch(() => {
        });
    }
    ngOnInit() {
    }
};
ReferridePage.ctorParameters = () => [
    { type: src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_4__["ProfileService"] },
    { type: _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_5__["SocialSharing"] },
    { type: src_app_services_language_service__WEBPACK_IMPORTED_MODULE_7__["LanguageService"] },
    { type: src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_6__["PopUpService"] },
    { type: src_app_services_event_service__WEBPACK_IMPORTED_MODULE_9__["EventService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_8__["SettingsService"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] }
];
ReferridePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-referride',
        template: __webpack_require__(/*! raw-loader!./referride.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/referride/referride.page.html"),
        styles: [__webpack_require__(/*! ./referride.page.scss */ "./src/app/pages/referride/referride.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_4__["ProfileService"], _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_5__["SocialSharing"],
        src_app_services_language_service__WEBPACK_IMPORTED_MODULE_7__["LanguageService"], src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_6__["PopUpService"], src_app_services_event_service__WEBPACK_IMPORTED_MODULE_9__["EventService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"], src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_8__["SettingsService"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"]])
], ReferridePage);



/***/ })

}]);
//# sourceMappingURL=pages-referride-referride-module-es2015.js.map