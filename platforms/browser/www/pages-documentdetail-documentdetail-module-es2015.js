(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-documentdetail-documentdetail-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/documentdetail/documentdetail.page.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/documentdetail/documentdetail.page.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header no-border>\n  <ion-toolbar style='margin-top: 25px;'>\n    <ion-button (click)=\"goBack()\" ion-button color=\"primary\" fill=\"clear\">\n      <ion-icon style=\"font-size: 1.5em\" name=\"arrow-back\"></ion-icon> <span\n        style=\"margin-left:30px; font-size: 1.4em\">DETAILS</span>\n    </ion-button>\n  </ion-toolbar>\n\n\n</ion-header>\n\n<ion-content class=\"scroll\">\n  <ion-row padding>\n  </ion-row>\n  <div>\n    <div text-center>\n      <h3 class='name'>{{data.title}}</h3>\n      <h1 class='name'>Comment :{{data.comment}}</h1>\n      <hr>\n    </div>\n\n    <div>\n\n\n      <ion-button padding *ngIf='data.filetype == 0' class='button' expand=\"block\" (click)='UploadFile(data.id)'>\n        Add File\n      </ion-button>\n\n\n      <ion-button padding *ngIf='data.filetype == 1' class='button' expand=\"block\" (click)='AddFile(data.id, true)'>\n        Add Info\n      </ion-button>\n\n    </div>\n\n    <div text-center>\n      <h1 class='name'> Current File :{{data.comment}}</h1>\n    </div>\n\n\n    <div *ngIf=\"data.data\">\n      <img id=\"my-pic\" [src]=\"data.data\" />\n    </div>\n\n    <div *ngIf=\"!data.data\">\n      <img id=\"my-pic\" [src]=\"captureDataUrl\" *ngIf=\"captureDataUrl\" />\n\n    </div>\n\n\n\n  </div>\n\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/documentdetail/documentdetail.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/documentdetail/documentdetail.module.ts ***!
  \***************************************************************/
/*! exports provided: DocumentdetailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DocumentdetailPageModule", function() { return DocumentdetailPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _documentdetail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./documentdetail.page */ "./src/app/pages/documentdetail/documentdetail.page.ts");







const routes = [
    {
        path: '',
        component: _documentdetail_page__WEBPACK_IMPORTED_MODULE_6__["DocumentdetailPage"]
    }
];
let DocumentdetailPageModule = class DocumentdetailPageModule {
};
DocumentdetailPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_documentdetail_page__WEBPACK_IMPORTED_MODULE_6__["DocumentdetailPage"]]
    })
], DocumentdetailPageModule);



/***/ }),

/***/ "./src/app/pages/documentdetail/documentdetail.page.scss":
/*!***************************************************************!*\
  !*** ./src/app/pages/documentdetail/documentdetail.page.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-card-header {\n  text-align: center;\n}\n\n.drive {\n  color: #0a64eb;\n  font-family: \"Montserrat\";\n}\n\n.drop-zone {\n  background-color: #f6f6f6;\n  border: dotted 3px #dedddd;\n  height: 30vh;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n          justify-content: center;\n  margin: 20px 0;\n}\n\n.selfie {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n          flex-direction: column;\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n\n#my-pic {\n  width: 90% !important;\n  height: 45% !important;\n  border-radius: 12px;\n  border: 1px solid #eeeeee;\n  vertical-align: center;\n  margin: 5%;\n}\n\n.file-input-container {\n  text-align: right;\n}\n\n.file-input-container input[type=file] {\n  display: none;\n}\n\n.file-input-container label {\n  border: 1px solid #ccc;\n  padding: 6px 12px;\n  cursor: pointer;\n}\n\n.nv-file-over {\n  border: dotted 3px red;\n}\n\n.price {\n  color: green;\n}\n\n.date {\n  color: orangered;\n}\n\n.destination {\n  color: cadetblue;\n}\n\nh3 {\n  font-size: 4em;\n}\n\nion-item {\n  margin-top: 24px;\n  border-bottom: 1px solid #d8d8d8 !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYWhhL0lvbmljQXBwcy9DNFJpZGUtSU9TLTIwMjQvY2FsbDRyaWRlLXBhc3Nlbmdlci9zcmMvYXBwL3BhZ2VzL2RvY3VtZW50ZGV0YWlsL2RvY3VtZW50ZGV0YWlsLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvZG9jdW1lbnRkZXRhaWwvZG9jdW1lbnRkZXRhaWwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usa0JBQUE7QUNDRjs7QURFQTtFQUNFLGNBQUE7RUFDQSx5QkFBQTtBQ0NGOztBREVBO0VBQ0UseUJBQUE7RUFDQSwwQkFBQTtFQUNBLFlBQUE7RUFDQSxvQkFBQTtFQUFBLGFBQUE7RUFDQSx5QkFBQTtVQUFBLG1CQUFBO0VBQ0Esd0JBQUE7VUFBQSx1QkFBQTtFQUNBLGNBQUE7QUNDRjs7QURFQTtFQUNFLG9CQUFBO0VBQUEsYUFBQTtFQUNBLDRCQUFBO0VBQUEsNkJBQUE7VUFBQSxzQkFBQTtFQUNBLHdCQUFBO1VBQUEsdUJBQUE7QUNDRjs7QURFQTtFQUNFLHFCQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0Esc0JBQUE7RUFDQSxVQUFBO0FDQ0Y7O0FERUE7RUFDRSxpQkFBQTtBQ0NGOztBRENFO0VBQ0UsYUFBQTtBQ0NKOztBREVFO0VBQ0Usc0JBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7QUNBSjs7QURJQTtFQUNFLHNCQUFBO0FDREY7O0FESUE7RUFDRSxZQUFBO0FDREY7O0FESUE7RUFDRSxnQkFBQTtBQ0RGOztBRElBO0VBQ0UsZ0JBQUE7QUNERjs7QURJQTtFQUNFLGNBQUE7QUNERjs7QURJQTtFQUNFLGdCQUFBO0VBQ0EsMkNBQUE7QUNERiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2RvY3VtZW50ZGV0YWlsL2RvY3VtZW50ZGV0YWlsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jYXJkLWhlYWRlciB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG4uZHJpdmUge1xyXG4gIGNvbG9yOiByZ2IoMTAsIDEwMCwgMjM1KTtcclxuICBmb250LWZhbWlseTogXCJNb250c2VycmF0XCI7XHJcbn1cclxuXHJcbi5kcm9wLXpvbmUge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmNmY2ZjY7XHJcbiAgYm9yZGVyOiBkb3R0ZWQgM3B4ICNkZWRkZGQ7XHJcbiAgaGVpZ2h0OiAzMHZoO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBtYXJnaW46IDIwcHggMDtcclxufVxyXG5cclxuLnNlbGZpZSB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG59XHJcblxyXG4jbXktcGljIHtcclxuICB3aWR0aDogOTAlICFpbXBvcnRhbnQ7XHJcbiAgaGVpZ2h0OiA0NSUgIWltcG9ydGFudDtcclxuICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICNlZWVlZWU7XHJcbiAgdmVydGljYWwtYWxpZ246IGNlbnRlcjtcclxuICBtYXJnaW46IDUlO1xyXG59XHJcblxyXG4uZmlsZS1pbnB1dC1jb250YWluZXIge1xyXG4gIHRleHQtYWxpZ246IHJpZ2h0O1xyXG5cclxuICBpbnB1dFt0eXBlPVwiZmlsZVwiXSB7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG4gIH1cclxuXHJcbiAgbGFiZWwge1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgIHBhZGRpbmc6IDZweCAxMnB4O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gIH1cclxufVxyXG5cclxuLm52LWZpbGUtb3ZlciB7XHJcbiAgYm9yZGVyOiBkb3R0ZWQgM3B4IHJlZDtcclxufVxyXG5cclxuLnByaWNlIHtcclxuICBjb2xvcjogZ3JlZW47XHJcbn1cclxuXHJcbi5kYXRlIHtcclxuICBjb2xvcjogb3JhbmdlcmVkO1xyXG59XHJcblxyXG4uZGVzdGluYXRpb24ge1xyXG4gIGNvbG9yOiBjYWRldGJsdWU7XHJcbn1cclxuXHJcbmgzIHtcclxuICBmb250LXNpemU6IDRlbTtcclxufVxyXG5cclxuaW9uLWl0ZW0ge1xyXG4gIG1hcmdpbi10b3A6IDI0cHg7XHJcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHJnYigyMTYsIDIxNiwgMjE2KSAhaW1wb3J0YW50O1xyXG59XHJcbiIsImlvbi1jYXJkLWhlYWRlciB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmRyaXZlIHtcbiAgY29sb3I6ICMwYTY0ZWI7XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXRcIjtcbn1cblxuLmRyb3Atem9uZSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmNmY2ZjY7XG4gIGJvcmRlcjogZG90dGVkIDNweCAjZGVkZGRkO1xuICBoZWlnaHQ6IDMwdmg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBtYXJnaW46IDIwcHggMDtcbn1cblxuLnNlbGZpZSB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG4jbXktcGljIHtcbiAgd2lkdGg6IDkwJSAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IDQ1JSAhaW1wb3J0YW50O1xuICBib3JkZXItcmFkaXVzOiAxMnB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjZWVlZWVlO1xuICB2ZXJ0aWNhbC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW46IDUlO1xufVxuXG4uZmlsZS1pbnB1dC1jb250YWluZXIge1xuICB0ZXh0LWFsaWduOiByaWdodDtcbn1cbi5maWxlLWlucHV0LWNvbnRhaW5lciBpbnB1dFt0eXBlPWZpbGVdIHtcbiAgZGlzcGxheTogbm9uZTtcbn1cbi5maWxlLWlucHV0LWNvbnRhaW5lciBsYWJlbCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIHBhZGRpbmc6IDZweCAxMnB4O1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG5cbi5udi1maWxlLW92ZXIge1xuICBib3JkZXI6IGRvdHRlZCAzcHggcmVkO1xufVxuXG4ucHJpY2Uge1xuICBjb2xvcjogZ3JlZW47XG59XG5cbi5kYXRlIHtcbiAgY29sb3I6IG9yYW5nZXJlZDtcbn1cblxuLmRlc3RpbmF0aW9uIHtcbiAgY29sb3I6IGNhZGV0Ymx1ZTtcbn1cblxuaDMge1xuICBmb250LXNpemU6IDRlbTtcbn1cblxuaW9uLWl0ZW0ge1xuICBtYXJnaW4tdG9wOiAyNHB4O1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2Q4ZDhkOCAhaW1wb3J0YW50O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/documentdetail/documentdetail.page.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/documentdetail/documentdetail.page.ts ***!
  \*************************************************************/
/*! exports provided: DocumentdetailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DocumentdetailPage", function() { return DocumentdetailPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.cjs.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_language_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/language.service */ "./src/app/services/language.service.ts");
/* harmony import */ var src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/settings.service */ "./src/app/services/settings.service.ts");
/* harmony import */ var src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/pop-up.service */ "./src/app/services/pop-up.service.ts");
/* harmony import */ var src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/profile.service */ "./src/app/services/profile.service.ts");
/* harmony import */ var src_app_services_event_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/event.service */ "./src/app/services/event.service.ts");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");











let DocumentdetailPage = class DocumentdetailPage {
    constructor(navCtrl, actionSheetCtrl, actRoute, camera, lp, settings, pop, load, profile, alert, eventProvider) {
        this.navCtrl = navCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.actRoute = actRoute;
        this.camera = camera;
        this.lp = lp;
        this.settings = settings;
        this.pop = pop;
        this.load = load;
        this.profile = profile;
        this.alert = alert;
        this.eventProvider = eventProvider;
        this.currentEvent = {};
        this.actRoute.queryParams.subscribe((res) => {
            console.log(res);
            this.data = res;
        });
        console.log(this.data);
    }
    ionViewDidEnter() {
    }
    UploadFile(eventId) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const actionSheet = yield this.actionSheetCtrl.create({
                header: 'Upload From',
                buttons: [
                    {
                        text: 'Camera',
                        icon: 'ios-camera',
                        handler: () => {
                            this.capture(eventId);
                        }
                    }, {
                        text: 'File',
                        icon: 'ios-folder',
                        handler: () => {
                            this.captureFromFile(eventId);
                        }
                    }, {
                        text: 'Close',
                        icon: 'close',
                        role: 'cancel',
                        handler: () => {
                            console.log('Cancel clicked');
                        }
                    }
                ]
            });
            yield actionSheet.present();
        });
    }
    AddFile(eventId, state) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            console.log(this.profile.theKey);
            const alert = yield this.alert.create({
                header: 'Entry Name',
                inputs: [
                    {
                        value: ''
                    },
                ],
                buttons: [
                    {
                        text: 'Cancel',
                    },
                    {
                        text: 'Okay',
                        handler: data => {
                            console.log(data, eventId);
                            if (this.data.approved) {
                                this.profile.createDocList(data, eventId);
                            }
                            else {
                                this.profile.createMyDocList(data, eventId);
                            }
                        }
                    }
                ],
                backdropDismiss: false
            });
            alert.present();
        });
    }
    capture(id) {
        const cameraOptions = {
            quality: 4,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
        };
        this.camera.getPicture(cameraOptions).then((imageData) => {
            this.captureDataUrl = 'data:image/jpeg;base64,' + imageData;
            this.processProfilePicture(this.captureDataUrl, id);
        }, (err) => {
            // Handle error
        });
    }
    captureFromFile(id) {
        const cameraOptions = {
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            destinationType: this.camera.DestinationType.DATA_URL,
            quality: 20,
            encodingType: this.camera.EncodingType.PNG,
        };
        this.camera.getPicture(cameraOptions).then((imageData) => {
            this.captureDataUrl = 'data:image/jpeg;base64,' + imageData;
            this.processProfilePicture(this.captureDataUrl, id);
        });
    }
    processProfilePicture(captureData, id) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const storageRef = firebase_app__WEBPACK_IMPORTED_MODULE_2__["storage"]().ref();
            // Create a timestamp as filename
            const filename = Math.floor(Date.now() / 1000);
            const loading = yield this.load.create({});
            yield loading.present();
            // Create a reference to 'images/todays-date.jpg'
            const imageRef = storageRef.child(`myPics/${filename}.jpg`);
            imageRef.putString(captureData, firebase_app__WEBPACK_IMPORTED_MODULE_2__["storage"].StringFormat.DATA_URL).then((snapshot) => {
                imageRef.getDownloadURL().then(url => {
                    console.log(url);
                    console.log(url);
                    this.load.dismiss();
                    console.log('done');
                    if (this.data.approved) {
                        this.profile.uploadDocFile2(url, id);
                        this.pop.hideLoader();
                    }
                    else {
                        this.pop.hideLoader();
                        this.profile.uploadDocFile(url, id);
                    }
                }).catch(error => { alert('error couldnt do what you asked'); });
            }).catch(error => { alert('error couldnt do what you asked'); });
        });
    }
    goBack() {
        this.navCtrl.navigateRoot('documents');
    }
    ngOnInit() {
    }
};
DocumentdetailPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ActionSheetController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_10__["ActivatedRoute"] },
    { type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_9__["Camera"] },
    { type: src_app_services_language_service__WEBPACK_IMPORTED_MODULE_4__["LanguageService"] },
    { type: src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_5__["SettingsService"] },
    { type: src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_6__["PopUpService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
    { type: src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_7__["ProfileService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
    { type: src_app_services_event_service__WEBPACK_IMPORTED_MODULE_8__["EventService"] }
];
DocumentdetailPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-documentdetail',
        template: __webpack_require__(/*! raw-loader!./documentdetail.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/documentdetail/documentdetail.page.html"),
        styles: [__webpack_require__(/*! ./documentdetail.page.scss */ "./src/app/pages/documentdetail/documentdetail.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ActionSheetController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_10__["ActivatedRoute"], _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_9__["Camera"], src_app_services_language_service__WEBPACK_IMPORTED_MODULE_4__["LanguageService"],
        src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_5__["SettingsService"], src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_6__["PopUpService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"], src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_7__["ProfileService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"], src_app_services_event_service__WEBPACK_IMPORTED_MODULE_8__["EventService"]])
], DocumentdetailPage);



/***/ })

}]);
//# sourceMappingURL=pages-documentdetail-documentdetail-module-es2015.js.map