(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-booklater-booklater-module~pages-estimate-estimate-module~pages-home-home-module~pages~7d935c43"],{

/***/ "./src/app/services/directionservice.service.ts":
/*!******************************************************!*\
  !*** ./src/app/services/directionservice.service.ts ***!
  \******************************************************/
/*! exports provided: DirectionserviceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DirectionserviceService", function() { return DirectionserviceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _language_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./language.service */ "./src/app/services/language.service.ts");
/* harmony import */ var _native_map_container_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./native-map-container.service */ "./src/app/services/native-map-container.service.ts");
/* harmony import */ var _geocoder_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./geocoder.service */ "./src/app/services/geocoder.service.ts");
/* harmony import */ var _pop_up_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./pop-up.service */ "./src/app/services/pop-up.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _event_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./event.service */ "./src/app/services/event.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _profile_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./profile.service */ "./src/app/services/profile.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");











var DirectionserviceService = /** @class */ (function () {
    function DirectionserviceService(eProvider, http, lp, modalCtrl, platform, cMap, gCode, popOp, ph, storage) {
        this.eProvider = eProvider;
        this.http = http;
        this.lp = lp;
        this.modalCtrl = modalCtrl;
        this.platform = platform;
        this.cMap = cMap;
        this.gCode = gCode;
        this.popOp = popOp;
        this.ph = ph;
        this.storage = storage;
        this.canDismiss = false;
        this.hasGottenTripDist = false;
        this.calculateBtn = false;
        this.pricePerKm = 4;
        this.fare = 5;
        this.rate = 2;
        this.extra = 50;
        this.fix = 65;
        this.above = 110;
        this.next = 55;
        this.min = 3;
        this.cons = 4;
        this.canUpdateDestination = false;
        this.isDriver = false;
        this.service = new google.maps.DistanceMatrixService();
        this.rage = 0;
        this.actualPrice = 0;
        this.toll_1 = 0;
        this.toll_0 = 0;
        this.timeToReach = "calculating...";
        this.arriving_time = "calculating....";
    }
    DirectionserviceService.prototype.calcRouteAriving = function (start, stop, isDriver, canUpdateDestination, destinationName, canshowLoads, percentage) {
        var _this = this;
        var directionsService = new google.maps.DirectionsService();
        // console.log(start, stop);
        // console.log("PERCENTAGE IN CAC ROUTE: " + percentage);
        // console.log("LOCAION: " + this.gCode.locationName);
        // console.log("DESTINATION: " + destinationName);
        var request = {
            origin: start,
            destination: stop,
            travelMode: google.maps.TravelMode.DRIVING,
            provideRouteAlternatives: true,
        };
        directionsService.route(request, function (response, status) {
            _this.callback3(response, status, canshowLoads, percentage);
        });
        this.isDriver = isDriver;
        this.canUpdateDestination = canUpdateDestination;
        this.destinationName = destinationName;
    };
    DirectionserviceService.prototype.callback3 = function (response, status, canshowLoads, percentage) {
        // See Parsing the Results for
        // the basics of a callback function.
        if (status == google.maps.DirectionsStatus.OK) {
            // loading.present();
            // this.checkPromoExist();
            var duration = Math.round(response.routes[0].legs[0].duration.value / 60);
            var distance = response.routes[0].legs[0].distance.value * 0.000621371192;
            this.duration = duration;
            this.miles = distance;
            this.storage.set("duration", this.duration);
            console.log("<<<<<<< DIRECTION SERVICE >>>>>>>");
            console.log("This is the duration " + duration);
            console.log("This is the distance " + distance);
            console.log("This is the fare " + this.fare);
            console.log("This is the pricePerKm " + this.pricePerKm);
            console.log("This is the percentage " + percentage);
            // this.ph.getPricing().limitToLast(1).once("value", (data) => {
            //   if (data.val()) {
            //     data.forEach((snap) => {
            //       this.fare = snap.val().base;
            //       this.pricePerKm = snap.val().pricePerKm;
            //       let surge = snap.val().surge;
            //       console.log("basefare is " + this.fare);
            //       console.log("pricePerKm is " + this.pricePerKm);
            //       console.log("surge is " + surge);
            //       if (percentage == undefined || percentage == "undefined") {
            //         this.price = Math.round(((this.fare + distance + duration + surge) * this.pricePerKm));
            //         console.log("Price when % is undefined " + this.price);
            //       } else {
            //         let old_price = (this.fare + distance + duration + surge) * this.pricePerKm;
            //         this.price = Math.round((old_price - percentage * old_price));
            //         console.log("Price when % exist " + this.price);
            //       }
            //     });
            //   } else {
            //     console.log("NO PRICING");
            //   }
            // })
            this.arriving_time = response.routes[0].legs[0].duration.text;
            this.storage.set("arriving_time", this.arriving_time);
            var rect = document.getElementById("header");
            if (this.isDriver && rect) {
                rect.innerText = this.lp.translate()[0].arrival + this.time;
                rect.style.fontSize = 1.8 + "em";
            }
            // this.timeToReach = response.routes[0].legs[0].duration.text;
            // console.log("TIME TO REACH IS::", this.timeToReach);
        }
        else {
            //this.cMap.norideavailable = true;
            // if (canshowLoads)
            // this.popOp.hideLoader();
        }
    };
    DirectionserviceService.prototype.calcRoute = function (start, stop, isDriver, canUpdateDestination, destinationName, canshowLoads, percentage) {
        // if (!this.platform.is("cordova")) {
        //   console.log("did this");
        //   start = new google.maps.LatLng(40.65563, -73.95025);
        //   this.gCode.locationName = "Testing Location";
        // }
        var _this = this;
        var directionsService = new google.maps.DirectionsService();
        if (this.calculateBtn) {
            //this.popOp.presentLoader('Measuring distance')
        }
        console.log("START", start, "STOP", stop);
        console.log("PERCENTAGE IN CAC ROUTE: " + percentage);
        console.log("LOCAION: " + this.gCode.locationName);
        console.log("DESTINATION: " + destinationName);
        var request = {
            origin: start,
            destination: stop,
            travelMode: google.maps.TravelMode.DRIVING,
            provideRouteAlternatives: true,
        };
        directionsService.route(request, function (response, status) {
            _this.callback(response, status, canshowLoads, percentage);
        });
        this.isDriver = isDriver;
        this.canUpdateDestination = canUpdateDestination;
        this.destinationName = destinationName;
    };
    DirectionserviceService.prototype.callback = function (response, status, canshowLoads, percentage) {
        // See Parsing the Results for
        // the basics of a callback function.
        var _this = this;
        if (status == google.maps.DirectionsStatus.OK) {
            // loading.present();
            // this.checkPromoExist();
            // const distance = route.legs[0].distance.value; // Distance in meters
            // const farePerMeter = 0.10; // Adjust this to your fare rate
            // const fare = (distance * farePerMeter) / 1000; // Convert meters to kilometers
            // return fare.toFixed(2)
            var duration = Math.round(response.routes[0].legs[0].duration.value / 60);
            // let distance = response.routes[0].legs[0].distance.value * 0.000621371192;
            var distance_1 = response.routes[0].legs[0].distance.value;
            this.duration = duration;
            this.miles = distance_1;
            this.storage.set("duration", this.duration);
            console.log("This is the duration " + duration);
            console.log("This is the distance " + distance_1);
            console.log("This is the fare " + this.fare);
            console.log("This is the pricePerKm " + this.pricePerKm);
            console.log("This is the percentage " + percentage);
            this.ph.getPricing().limitToLast(1).once("value", function (data) {
                if (data.val()) {
                    data.forEach(function (snap) {
                        _this.fare = snap.val().base;
                        _this.pricePerKm = snap.val().pricePerKm;
                        var surge = snap.val().surge;
                        console.log("basefare is " + _this.fare);
                        console.log("pricePerKm is " + _this.pricePerKm);
                        console.log("surge is " + surge);
                        console.log("This is the distance " + distance_1);
                        if (percentage == undefined || percentage == "undefined") {
                            // this.price = Math.round(((this.fare + distance + duration + surge) * this.pricePerKm));
                            _this.price = Math.round((distance_1 * _this.fare * _this.pricePerKm) / 1000) + surge;
                            _this.storage.set("price", _this.price);
                            console.log("Price when % is undefined " + _this.price);
                        }
                        else {
                            // let old_price = (this.fare + distance + duration + surge) * this.pricePerKm;
                            var old_price = ((distance_1 * _this.fare * _this.pricePerKm) / 1000) + surge;
                            _this.price = Math.round((old_price - percentage * old_price));
                            _this.storage.set("price", _this.price);
                            console.log("Price when % exist " + _this.price);
                        }
                    });
                }
                else {
                    if (percentage == undefined || percentage == "undefined") {
                        // this.price = Math.round(((this.fare + distance + duration) * this.pricePerKm));
                        _this.price = Math.round((distance_1 * _this.fare * _this.pricePerKm) / 1000);
                        _this.storage.set("price", _this.price);
                        console.log("Price when % is undefined " + _this.price);
                    }
                    else {
                        // let old_price = (this.fare + distance + duration) * this.pricePerKm;
                        var old_price = (distance_1 * _this.fare * _this.pricePerKm) / 1000;
                        _this.price = Math.round((old_price - percentage * old_price));
                        _this.storage.set("price", _this.price);
                        console.log("Price when % exist " + _this.price);
                    }
                }
            });
            this.time = response.routes[0].legs[0].duration.text;
            console.log("FINAL PRICE:: " + this.price);
            console.log("HIGH PRICE:: " + this.highPrice);
            console.log("TIME :: " + this.time);
            var rect = document.getElementById("header");
            if (this.isDriver && rect) {
                rect.innerText = this.lp.translate()[0].arrival + this.time;
                rect.style.fontSize = 1.8 + "em";
            }
            this.timeToReach = response.routes[0].legs[0].duration.text;
            this.storage.set("timeToReach", this.timeToReach);
            console.log("TIME TO REACH IS::", this.timeToReach);
            console.log(response.routes[0].legs[0].distance.value * 0.000621371192, response.routes[0].legs[0].duration.value, response.routes[0].legs[0].duration.text);
        }
        else {
            //this.cMap.norideavailable = true;
            // if (canshowLoads)
            // this.popOp.hideLoader();
        }
    };
    DirectionserviceService.prototype.calcDestRoute = function (name, start, stop, destinationName, id, canshowLoads, percentage) {
        var _this = this;
        if (!this.platform.is("cordova")) {
            start = new google.maps.LatLng(40.65563, -73.95025);
            this.gCode.locationName = "Test Accra";
        }
        var directionsService = new google.maps.DirectionsService();
        var request = {
            origin: this.gCode.locationName,
            destination: destinationName,
            travelMode: google.maps.TravelMode.DRIVING,
            provideRouteAlternatives: true,
        };
        directionsService.route(request, function (response, status) {
            _this.callback2(response, status, id, canshowLoads, _this.percentage);
        });
        this.request = {
            origin: this.gCode.locationName,
            destination: destinationName,
            travelMode: google.maps.TravelMode.DRIVING,
            provideRouteAlternatives: true,
        };
        this.destinationName = destinationName;
        this.name = name;
        var time = new Date();
        var hh = time.getHours();
        var mm = time.getMinutes();
        var ss = time.getSeconds();
    };
    DirectionserviceService.prototype.callback2 = function (response, status, id, canshowLoads, percentage) {
        // See Parsing the Results for
        // the basics of a callback function.
        if (status == google.maps.DirectionsStatus.OK) {
            // this.checkPromoExist();
            var duration = Math.round(response.routes[0].legs[0].duration.value / 60);
            var distance = response.routes[0].legs[0].distance.value * 0.000621371192;
            this.duration2 = duration;
            this.miles = distance;
            this.distance2 = (response.routes[0].legs[0].distance.value * 0.000621371192).toFixed(2);
            console.log("This is the duration " + this.duration2);
            console.log("This is the distance " + this.distance2);
            console.log("This is the fare " + this.fare);
            console.log("This is the pricePerKm " + this.pricePerKm);
            console.log("This is the percentage " + percentage);
            // this.ph.getPricing().limitToLast(1).once("value", (data) => {
            //   if (data.val()) {
            //     data.forEach((snap) => {
            //       this.fare = snap.val().base;
            //       this.pricePerKm = snap.val().pricePerKm;
            //       let surge = snap.val().surge;
            //       console.log("basefare is " + this.fare);
            //       console.log("pricePerKm is " + this.pricePerKm);
            //       console.log("surge is " + surge);
            //       if (percentage == undefined || percentage == "undefined") {
            //         this.price = Math.round(((this.fare + distance + duration + surge) * this.pricePerKm));
            //         console.log("Price when % is undefined " + this.price);
            //       } else {
            //         let old_price = (this.fare + distance + duration + surge) * this.pricePerKm;
            //         this.price = Math.round((old_price - percentage * old_price));
            //         console.log("Price when % exsit " + this.price);
            //       }
            //     });
            //   } else {
            //     console.log("NO PRICING");
            //     if (percentage == undefined || percentage == "undefined") {
            //       this.price = Math.round(((this.fare + distance + duration) * this.pricePerKm));
            //       console.log("Price when % is undefined " + this.price);
            //     } else {
            //       let old_price = (this.fare + distance + duration) * this.pricePerKm;
            //       this.price = Math.round((old_price - percentage * old_price));
            //       console.log("Price when % exust " + this.price);
            //     }
            //   }
            // })
            this.destination_time = response.routes[0].legs[0].duration.text;
            this.storage.set("destination_time", this.destination_time);
            var rect = document.getElementById("header");
            this.isDriver = false;
            if (rect) {
                rect.innerText = this.lp.translate()[0].arrival + this.time;
                rect.style.fontSize = 1.8 + "em";
            }
            //  loading.dismiss()
            // this.popOp.hideLoader();
        }
    };
    DirectionserviceService.prototype.calcRouteForAddstop = function (start, stop, isDriver, canUpdateDestination, destinationName, canshowLoads, percentage) {
        var _this = this;
        var directionsService = new google.maps.DirectionsService();
        var request = {
            origin: start,
            destination: stop,
            travelMode: google.maps.TravelMode.DRIVING,
            provideRouteAlternatives: true,
        };
        directionsService.route(request, function (response, status) {
            _this.callbackForAddStop(response, status, canshowLoads, percentage);
        });
        this.isDriver = isDriver;
        this.canUpdateDestination = canUpdateDestination;
        this.destinationName = destinationName;
    };
    DirectionserviceService.prototype.callbackForAddStop = function (response, status, canshowLoads, percentage) {
        var _this = this;
        if (status == google.maps.DirectionsStatus.OK) {
            var duration = Math.round(response.routes[0].legs[0].duration.value / 60);
            // let distance = response.routes[0].legs[0].distance.value * 0.000621371192;
            var distance_2 = response.routes[0].legs[0].distance.value;
            this.duration = duration;
            this.miles = distance_2;
            this.storage.set("duration", this.duration);
            // console.log("<<<<<<< DIRECTION SERVICE >>>>>>>");
            // console.log("This is the duration " + duration);
            // console.log("This is the distance " + distance);
            // console.log("This is the fare " + this.fare);
            // console.log("This is the pricePerKm " + this.pricePerKm);
            // console.log("This is the percentage " + percentage);
            this.ph.getPricing().limitToLast(1).once("value", function (data) {
                if (data.val()) {
                    data.forEach(function (snap) {
                        _this.fare = snap.val().base;
                        _this.pricePerKm = snap.val().pricePerKm;
                        var surge = snap.val().surge;
                        // console.log("basefare is " + this.fare);
                        // console.log("pricePerKm is " + this.pricePerKm);
                        // console.log("surge is " + surge);
                        if (percentage == undefined || percentage == "undefined") {
                            _this.new_price_for_add_stop = Math.round((distance_2 * _this.fare * _this.pricePerKm) / 1000) + surge;
                            _this.storage.set("price", _this.new_price_for_add_stop);
                            // console.log("Price when % is undefined " + this.new_price_for_add_stop);
                        }
                        else {
                            // let old_price = (this.fare + distance + duration + surge) * this.pricePerKm;
                            var old_price = ((distance_2 * _this.fare * _this.pricePerKm) / 1000) + surge;
                            _this.new_price_for_add_stop = Math.round((old_price - percentage * old_price));
                            _this.storage.set("price", _this.new_price_for_add_stop);
                            // console.log("new_price_for_add_stop when % exist " + this.new_price_for_add_stop);
                        }
                    });
                }
                else {
                    if (percentage == undefined || percentage == "undefined") {
                        // this.price = Math.round(((this.fare + distance + duration) * this.pricePerKm));
                        _this.new_price_for_add_stop = Math.round((distance_2 * _this.fare * _this.pricePerKm) / 1000);
                        _this.storage.set("price", _this.price);
                        console.log("Price when % is undefined " + _this.price);
                    }
                    else {
                        // let old_price = (this.fare + distance + duration) * this.pricePerKm;
                        var old_price = (distance_2 * _this.fare * _this.pricePerKm) / 1000;
                        _this.new_price_for_add_stop = Math.round((old_price - percentage * old_price));
                        _this.storage.set("price", _this.price);
                        console.log("new_price_for_add_stop when % exist " + _this.new_price_for_add_stop);
                    }
                }
            });
            this.time = response.routes[0].legs[0].duration.text;
            // console.log("FINAL PRICE:: " + this.price);
            // console.log("HIGH PRICE:: " + this.highPrice);
            // console.log("TIME :: " + this.time);
            var rect = document.getElementById("header");
            if (this.isDriver && rect) {
                rect.innerText = this.lp.translate()[0].arrival + this.time;
                rect.style.fontSize = 1.8 + "em";
            }
        }
        else {
        }
    };
    DirectionserviceService.prototype.checkPromoExist = function () {
        var _this = this;
        var id = this.ph.id;
        this.ph
            .getAllUsedCodes()
            .orderByChild("rider_id")
            .equalTo(id)
            .once("value", function (data) {
            if (data.val()) {
                data.forEach(function (snap) {
                    _this.status = snap.val().status;
                    var perc = snap.val().percentage;
                    // console.log("Status is " + this.status);
                    // console.log("percen is " + perc);
                    if (_this.status == "activated") {
                        _this.percentage = perc / 100;
                        // console.log(
                        //   "The ACTIVATED percentage here is " + this.percentage
                        // );
                        return _this.percentage;
                    }
                    else {
                    }
                });
            }
            else {
            }
        });
    };
    DirectionserviceService.ctorParameters = function () { return [
        { type: _event_service__WEBPACK_IMPORTED_MODULE_7__["EventService"] },
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpClient"] },
        { type: _language_service__WEBPACK_IMPORTED_MODULE_2__["LanguageService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ModalController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["Platform"] },
        { type: _native_map_container_service__WEBPACK_IMPORTED_MODULE_3__["NativeMapContainerService"] },
        { type: _geocoder_service__WEBPACK_IMPORTED_MODULE_4__["GeocoderService"] },
        { type: _pop_up_service__WEBPACK_IMPORTED_MODULE_5__["PopUpService"] },
        { type: _profile_service__WEBPACK_IMPORTED_MODULE_9__["ProfileService"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_10__["Storage"] }
    ]; };
    DirectionserviceService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root",
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_event_service__WEBPACK_IMPORTED_MODULE_7__["EventService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpClient"],
            _language_service__WEBPACK_IMPORTED_MODULE_2__["LanguageService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ModalController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["Platform"],
            _native_map_container_service__WEBPACK_IMPORTED_MODULE_3__["NativeMapContainerService"],
            _geocoder_service__WEBPACK_IMPORTED_MODULE_4__["GeocoderService"],
            _pop_up_service__WEBPACK_IMPORTED_MODULE_5__["PopUpService"],
            _profile_service__WEBPACK_IMPORTED_MODULE_9__["ProfileService"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_10__["Storage"]])
    ], DirectionserviceService);
    return DirectionserviceService;
}());



/***/ })

}]);
//# sourceMappingURL=default~pages-booklater-booklater-module~pages-estimate-estimate-module~pages-home-home-module~pages~7d935c43-es5.js.map