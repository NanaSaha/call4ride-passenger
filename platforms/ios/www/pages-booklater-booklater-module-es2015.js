(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-booklater-booklater-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/booklater/booklater.page.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/booklater/booklater.page.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header no-border>\n\n  <ion-title>Book a Car in London</ion-title>\n\n</ion-header>\n\n\n<ion-content padding class=\"no-scroll\">\n  <!-- These are the location bar and destination bar -->\n\n  <div *ngIf='scheduleInfo == null'>\n    <div text-center class=\"whiteFlap\">\n      <div class=\"bars\">\n        <p>{{Lang[0].from}}</p>\n\n\n        <!-- location bar -->\n        <!-- <p padding >Add a Phone Number to continue. </p> -->\n\n        <div class='form'>\n          <ion-item>\n            <ion-label class='stack' padding color='primary' stacked>Choose Your Airport</ion-label>\n            <ion-select [(ngModel)]=\"toppings\" multiple=\"false\">\n              <ion-select-option data-countryCode=\"AR\" value=\"London City Airport\">London City Airport\n              </ion-select-option>\n              <ion-select-option data-countryCode=\"AU\" value=\"London Gatwick Airport\">London Gatwick Airport\n              </ion-select-option>\n              <ion-select-option data-countryCode=\"AT\" value=\"London Heathrow Airport\">London Heathrow Airport\n              </ion-select-option>\n              <ion-select-option data-countryCode=\"AZ\" value=\"London Luton Airport\">London Luton Airport\n              </ion-select-option>\n              <ion-select-option data-countryCode=\"BS\" value=\"London Stansted Airport\">London Stansted Airport\n              </ion-select-option>\n            </ion-select>\n          </ion-item>\n\n        </div>\n\n        <p>{{Lang[0].to}}</p>\n        <!-- desination bar -->\n        <ion-button lines=\"none\" detail=\"false\" class=\"bars-destinate\" (click)=\"showAddressModal(2)\">\n          <ion-icon color='deep' name=\"flag\" slot=\"start\"></ion-icon>\n          <div id=\"whereto\">{{Lang[0].dest}}</div>\n        </ion-button>\n      </div>\n\n\n\n\n\n      <div class='top-items'>\n        <ion-item lines=\"none\">\n          <ion-label text-center>{{Lang[0].date}}</ion-label>\n          <ion-datetime displayFormat=\"MMM DD, YYYY HH:mm\" [(ngModel)]=\"myDate\" (ionChange)='Chosen($event)' min='2018'\n            max=\"2020-10-31\"></ion-datetime>\n        </ion-item>\n      </div>\n    </div>\n  </div>\n\n\n\n\n  <div *ngIf='scheduleInfo != null' class='followed-items'>\n    <ion-list text-center>\n      <ion-item>\n        <h2 class='ride'><strong>\n            <ion-icon color='deep' name=\"timer\"></ion-icon>A London Ride Booked\n          </strong></h2>\n        <h2 class='date'><strong>\n            <ion-icon color='deep' name=\"calendar\"></ion-icon>{{dataTime}}\n          </strong></h2>\n        <h2 class='date'><strong>\n            <ion-icon color='primary' name=\"clock\"></ion-icon>@{{scheduleInfo.Client_Time}}\n          </strong></h2>\n        <h2 class='location'><strong>\n            <ion-icon color='deep' name=\"locate\"></ion-icon>{{scheduleInfo.Client_locationName}}\n          </strong></h2>\n        <h2 class='destination'><strong>\n            <ion-icon color='deep' name=\"navigate\"></ion-icon>{{scheduleInfo.Client_destinationName}}\n          </strong></h2>\n\n        <button detail=\"false\" id='button' color=\"danger\" lines=\"none\" text-center (click)=\"CancelRide()\">\n          {{Lang[0].clear}}\n        </button>\n      </ion-item>\n    </ion-list>\n  </div>\n\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/booklater/booklater.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/booklater/booklater.module.ts ***!
  \*****************************************************/
/*! exports provided: BooklaterPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BooklaterPageModule", function() { return BooklaterPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _booklater_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./booklater.page */ "./src/app/pages/booklater/booklater.page.ts");







const routes = [
    {
        path: '',
        component: _booklater_page__WEBPACK_IMPORTED_MODULE_6__["BooklaterPage"]
    }
];
let BooklaterPageModule = class BooklaterPageModule {
};
BooklaterPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_booklater_page__WEBPACK_IMPORTED_MODULE_6__["BooklaterPage"]]
    })
], BooklaterPageModule);



/***/ }),

/***/ "./src/app/pages/booklater/booklater.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/pages/booklater/booklater.page.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".form {\n  margin-bottom: 26px;\n  height: 200px auto;\n  background: white;\n  padding: 10px;\n  border-radius: 12px;\n  margin-top: 20px;\n}\n.form button {\n  margin-top: 20px !important;\n  border-radius: 12px;\n}\n.form .stack {\n  text-align: center;\n  font-size: 1em;\n}\n.form .star {\n  z-index: 3;\n  top: 14.5%;\n  margin-top: 30%;\n}\n.form .star input {\n  width: 40%;\n}\n.top-items {\n  border-radius: 12px;\n  margin-top: 20%;\n}\n.top-items ion-item {\n  margin-top: 10px;\n  margin-bottom: 10px;\n  border-radius: 12px;\n  background: #0a64eb;\n}\n.top-items ion-item ion-label {\n  color: white !important;\n  text-align: center;\n  margin-left: 10%;\n}\n#envelope {\n  height: auto;\n  width: 4em;\n}\n.ride {\n  color: rgba(219, 205, 8, 0.91);\n  font-size: 1.17em;\n  padding-top: 14px;\n  padding-bottom: 14px;\n  border-radius: 12px;\n}\n.ride ion-icon {\n  font-size: 0.8em;\n  padding: 12px;\n  color: rgba(219, 205, 8, 0.91);\n}\n.price {\n  color: rgba(219, 205, 8, 0.91);\n  font-size: 1.67em;\n  padding-top: 14px;\n  padding-bottom: 14px;\n  border-radius: 12px;\n}\n.price ion-icon {\n  font-size: 0.8em;\n  padding: 5px;\n  color: rgba(219, 205, 8, 0.91);\n}\n.date {\n  color: orange;\n  font-size: 1.47em;\n  padding-top: 14px;\n  padding-bottom: 14px;\n  border-bottom: 1px solid #d8d8d8;\n  border-radius: 12px;\n}\n.date ion-icon {\n  font-size: 0.8em;\n  padding: 5px;\n  color: rgba(219, 205, 8, 0.91);\n}\n.time {\n  color: #2c88f1;\n  font-size: 1.17em;\n  padding-top: 14px;\n  padding-bottom: 14px;\n  border-radius: 12px;\n}\n.time ion-icon {\n  font-size: 0.8em;\n  padding: 5px;\n  color: rgba(219, 205, 8, 0.91);\n}\n.location {\n  width: auto;\n  padding-top: 8px;\n  padding-bottom: 8px;\n}\n.location p {\n  font-size: 1.3em;\n  height: auto;\n}\n.location ion-icon {\n  font-size: 0.8em;\n  padding: 5px;\n  color: orange;\n}\n.destination {\n  width: auto;\n  padding-top: 8px;\n  padding-bottom: 8px;\n}\n.destination ion-icon {\n  font-size: 0.8em;\n  padding: 5px;\n  color: darkslateblue;\n}\n.followed-items {\n  margin-top: 10%;\n}\n.followed-items ion-item {\n  margin-top: 10px;\n  margin-bottom: 10px;\n  border-radius: 12px;\n  border: 1px solid #d8d8d8;\n}\n.bars {\n  margin-top: 0%;\n  padding: 12px;\n}\n.bars .poiter {\n  z-index: 5;\n  margin-left: 1%;\n  background: rgba(240, 240, 240, 0.92);\n  border-left: 1.1px solid #d8d8d8;\n  border-right: 1.1px solid #d8d8d8;\n  border-top: 1px solid #d8d8d8;\n  border-bottom: 1px solid #d8d8d8;\n}\n.bars .bars-locate {\n  height: 50px;\n  width: 100%;\n  background: rgba(240, 240, 240, 0.92);\n  border-left: 1.1px solid #d8d8d8;\n  border-right: 1.1px solid #d8d8d8;\n  border-top: 1px solid #d8d8d8;\n  border-bottom: 1px solid #d8d8d8;\n  margin-left: 0%;\n  z-index: 3;\n  border-radius: 12px;\n  overflow: hidden;\n  line-height: 20px;\n  font-size: 1.2em;\n  text-align: center;\n}\n.bars .bars-locate ion-icon {\n  position: absolute;\n  font-size: 1em;\n  left: 2%;\n  color: #0a64eb;\n  padding: 5px;\n}\n.bars .bars-destinate {\n  height: 50px;\n  width: 100%;\n  background: rgba(240, 240, 240, 0.92);\n  margin: 3% 0 0 -50px;\n  margin-left: 0%;\n  z-index: 3;\n  border-left: 1.1px solid #d8d8d8;\n  border-right: 1.1px solid #d8d8d8;\n  border-top: 1px solid #d8d8d8;\n  border-bottom: 1px solid #d8d8d8;\n  overflow: hidden;\n  border-radius: 12px;\n  line-height: 20px;\n  font-size: 1.2em;\n  text-align: center;\n}\n.bars .bars-destinate ion-icon {\n  position: absolute;\n  font-size: 1em;\n  left: 2%;\n  padding: 5px;\n  color: rgba(219, 205, 8, 0.91);\n}\n.bars .bars-price {\n  height: 50px;\n  width: 100%;\n  background: #ffffff;\n  border-left: 1.1px solid #d8d8d8;\n  border-right: 1.1px solid #d8d8d8;\n  border-top: 1px solid #d8d8d8;\n  border-bottom: 1px solid #d8d8d8;\n  z-index: 3;\n  border-radius: 12px;\n  overflow: hidden;\n  line-height: 20px;\n  font-size: 1.2em;\n  text-align: center;\n}\n.bars .bars-price ion-icon {\n  position: absolute;\n  font-size: 1em;\n  left: 2%;\n  color: #0a64eb;\n  padding: 5px;\n}\n#position {\n  text-align: center;\n  padding-left: 17px;\n}\n#whereto {\n  text-align: center;\n  padding-left: 17px;\n}\n#cash {\n  text-align: center;\n  padding-left: 17px;\n}\n#button {\n  border-radius: 12px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYWhhL0lvbmljQXBwcy9DNFJpZGUtSU9TLTIwMjQvY2FsbDRyaWRlLXBhc3Nlbmdlci9zcmMvYXBwL3BhZ2VzL2Jvb2tsYXRlci9ib29rbGF0ZXIucGFnZS5zY3NzIiwic3JjL2FwcC9wYWdlcy9ib29rbGF0ZXIvYm9va2xhdGVyLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0FDQ0Y7QURBRTtFQUNFLDJCQUFBO0VBQ0EsbUJBQUE7QUNFSjtBREFFO0VBQ0Usa0JBQUE7RUFDQSxjQUFBO0FDRUo7QURBRTtFQUNFLFVBQUE7RUFDQSxVQUFBO0VBQ0EsZUFBQTtBQ0VKO0FEREk7RUFDRSxVQUFBO0FDR047QURDQTtFQUNFLG1CQUFBO0VBQ0EsZUFBQTtBQ0VGO0FEREU7RUFDRSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtBQ0dKO0FERkk7RUFDRSx1QkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUNJTjtBRENBO0VBQ0UsWUFBQTtFQUNBLFVBQUE7QUNFRjtBRENBO0VBQ0UsOEJBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0VBQ0Esb0JBQUE7RUFFQSxtQkFBQTtBQ0NGO0FEQUU7RUFDRSxnQkFBQTtFQUNBLGFBQUE7RUFDQSw4QkFBQTtBQ0VKO0FERUE7RUFDRSw4QkFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxvQkFBQTtFQUVBLG1CQUFBO0FDQUY7QURDRTtFQUNFLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLDhCQUFBO0FDQ0o7QURHQTtFQUNFLGFBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0VBQ0Esb0JBQUE7RUFDQSxnQ0FBQTtFQUNBLG1CQUFBO0FDQUY7QURDRTtFQUNFLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLDhCQUFBO0FDQ0o7QURHQTtFQUNFLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0VBQ0Esb0JBQUE7RUFDQSxtQkFBQTtBQ0FGO0FEQ0U7RUFDRSxnQkFBQTtFQUNBLFlBQUE7RUFDQSw4QkFBQTtBQ0NKO0FER0E7RUFDRSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBQ0FGO0FEQ0U7RUFDRSxnQkFBQTtFQUNBLFlBQUE7QUNDSjtBREVFO0VBQ0UsZ0JBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtBQ0FKO0FESUE7RUFDRSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBQ0RGO0FERUU7RUFDRSxnQkFBQTtFQUNBLFlBQUE7RUFDQSxvQkFBQTtBQ0FKO0FESUE7RUFDRSxlQUFBO0FDREY7QURHRTtFQUNFLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0FDREo7QURLQTtFQUNFLGNBQUE7RUFDQSxhQUFBO0FDRkY7QURJRTtFQUNFLFVBQUE7RUFDQSxlQUFBO0VBQ0EscUNBQUE7RUFDQSxnQ0FBQTtFQUNBLGlDQUFBO0VBQ0EsNkJBQUE7RUFDQSxnQ0FBQTtBQ0ZKO0FES0U7RUFDRSxZQUFBO0VBQ0EsV0FBQTtFQUNBLHFDQUFBO0VBQ0EsZ0NBQUE7RUFDQSxpQ0FBQTtFQUNBLDZCQUFBO0VBQ0EsZ0NBQUE7RUFDQSxlQUFBO0VBRUEsVUFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUNKSjtBRE1JO0VBQ0Usa0JBQUE7RUFDQSxjQUFBO0VBQ0EsUUFBQTtFQUNBLGNBQUE7RUFDQSxZQUFBO0FDSk47QURRRTtFQUNFLFlBQUE7RUFDQSxXQUFBO0VBQ0EscUNBQUE7RUFDQSxvQkFBQTtFQUNBLGVBQUE7RUFDQSxVQUFBO0VBQ0EsZ0NBQUE7RUFDQSxpQ0FBQTtFQUNBLDZCQUFBO0VBQ0EsZ0NBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FDTko7QURRSTtFQUNFLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLFFBQUE7RUFDQSxZQUFBO0VBQ0EsOEJBQUE7QUNOTjtBRFVFO0VBQ0UsWUFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtFQUNBLGdDQUFBO0VBQ0EsaUNBQUE7RUFDQSw2QkFBQTtFQUNBLGdDQUFBO0VBRUEsVUFBQTtFQUVBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUNWSjtBRFlJO0VBQ0Usa0JBQUE7RUFDQSxjQUFBO0VBQ0EsUUFBQTtFQUNBLGNBQUE7RUFDQSxZQUFBO0FDVk47QURlQTtFQUNFLGtCQUFBO0VBQ0Esa0JBQUE7QUNaRjtBRGVBO0VBQ0Usa0JBQUE7RUFDQSxrQkFBQTtBQ1pGO0FEZUE7RUFDRSxrQkFBQTtFQUNBLGtCQUFBO0FDWkY7QURlQTtFQUNFLG1CQUFBO0FDWkYiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9ib29rbGF0ZXIvYm9va2xhdGVyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5mb3JtIHtcclxuICBtYXJnaW4tYm90dG9tOiAyNnB4O1xyXG4gIGhlaWdodDogMjAwcHggYXV0bztcclxuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICBwYWRkaW5nOiAxMHB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDEycHg7XHJcbiAgbWFyZ2luLXRvcDogMjBweDtcclxuICBidXR0b24ge1xyXG4gICAgbWFyZ2luLXRvcDogMjBweCAhaW1wb3J0YW50O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTJweDtcclxuICB9XHJcbiAgLnN0YWNrIHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGZvbnQtc2l6ZTogMWVtO1xyXG4gIH1cclxuICAuc3RhciB7XHJcbiAgICB6LWluZGV4OiAzO1xyXG4gICAgdG9wOiAxNC41JTtcclxuICAgIG1hcmdpbi10b3A6IDMwJTtcclxuICAgIGlucHV0IHtcclxuICAgICAgd2lkdGg6IDQwJTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuLnRvcC1pdGVtcyB7XHJcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcclxuICBtYXJnaW4tdG9wOiAyMCU7XHJcbiAgaW9uLWl0ZW0ge1xyXG4gICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG4gICAgYmFja2dyb3VuZDogcmdiKDEwLCAxMDAsIDIzNSk7XHJcbiAgICBpb24tbGFiZWwge1xyXG4gICAgICBjb2xvcjogd2hpdGUgIWltcG9ydGFudDtcclxuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICBtYXJnaW4tbGVmdDogMTAlO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuI2VudmVsb3BlIHtcclxuICBoZWlnaHQ6IGF1dG87XHJcbiAgd2lkdGg6IDRlbTtcclxufVxyXG5cclxuLnJpZGUge1xyXG4gIGNvbG9yOiByZ2JhKDIxOSwgMjA1LCA4LCAwLjkxKTtcclxuICBmb250LXNpemU6IDEuMTdlbTtcclxuICBwYWRkaW5nLXRvcDogMTRweDtcclxuICBwYWRkaW5nLWJvdHRvbTogMTRweDtcclxuXHJcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcclxuICBpb24taWNvbiB7XHJcbiAgICBmb250LXNpemU6IDAuOGVtO1xyXG4gICAgcGFkZGluZzogMTJweDtcclxuICAgIGNvbG9yOiByZ2JhKDIxOSwgMjA1LCA4LCAwLjkxKTtcclxuICB9XHJcbn1cclxuXHJcbi5wcmljZSB7XHJcbiAgY29sb3I6IHJnYmEoMjE5LCAyMDUsIDgsIDAuOTEpO1xyXG4gIGZvbnQtc2l6ZTogMS42N2VtO1xyXG4gIHBhZGRpbmctdG9wOiAxNHB4O1xyXG4gIHBhZGRpbmctYm90dG9tOiAxNHB4O1xyXG5cclxuICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG4gIGlvbi1pY29uIHtcclxuICAgIGZvbnQtc2l6ZTogMC44ZW07XHJcbiAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICBjb2xvcjogcmdiYSgyMTksIDIwNSwgOCwgMC45MSk7XHJcbiAgfVxyXG59XHJcblxyXG4uZGF0ZSB7XHJcbiAgY29sb3I6IG9yYW5nZTtcclxuICBmb250LXNpemU6IDEuNDdlbTtcclxuICBwYWRkaW5nLXRvcDogMTRweDtcclxuICBwYWRkaW5nLWJvdHRvbTogMTRweDtcclxuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xyXG4gIGJvcmRlci1yYWRpdXM6IDEycHg7XHJcbiAgaW9uLWljb24ge1xyXG4gICAgZm9udC1zaXplOiAwLjhlbTtcclxuICAgIHBhZGRpbmc6IDVweDtcclxuICAgIGNvbG9yOiByZ2JhKDIxOSwgMjA1LCA4LCAwLjkxKTtcclxuICB9XHJcbn1cclxuXHJcbi50aW1lIHtcclxuICBjb2xvcjogcmdiKDQ0LCAxMzYsIDI0MSk7XHJcbiAgZm9udC1zaXplOiAxLjE3ZW07XHJcbiAgcGFkZGluZy10b3A6IDE0cHg7XHJcbiAgcGFkZGluZy1ib3R0b206IDE0cHg7XHJcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcclxuICBpb24taWNvbiB7XHJcbiAgICBmb250LXNpemU6IDAuOGVtO1xyXG4gICAgcGFkZGluZzogNXB4O1xyXG4gICAgY29sb3I6IHJnYmEoMjE5LCAyMDUsIDgsIDAuOTEpO1xyXG4gIH1cclxufVxyXG5cclxuLmxvY2F0aW9uIHtcclxuICB3aWR0aDogYXV0bztcclxuICBwYWRkaW5nLXRvcDogOHB4O1xyXG4gIHBhZGRpbmctYm90dG9tOiA4cHg7XHJcbiAgcCB7XHJcbiAgICBmb250LXNpemU6IDEuM2VtO1xyXG4gICAgaGVpZ2h0OiBhdXRvO1xyXG4gIH1cclxuXHJcbiAgaW9uLWljb24ge1xyXG4gICAgZm9udC1zaXplOiAwLjhlbTtcclxuICAgIHBhZGRpbmc6IDVweDtcclxuICAgIGNvbG9yOiBvcmFuZ2U7XHJcbiAgfVxyXG59XHJcblxyXG4uZGVzdGluYXRpb24ge1xyXG4gIHdpZHRoOiBhdXRvO1xyXG4gIHBhZGRpbmctdG9wOiA4cHg7XHJcbiAgcGFkZGluZy1ib3R0b206IDhweDtcclxuICBpb24taWNvbiB7XHJcbiAgICBmb250LXNpemU6IDAuOGVtO1xyXG4gICAgcGFkZGluZzogNXB4O1xyXG4gICAgY29sb3I6IGRhcmtzbGF0ZWJsdWU7XHJcbiAgfVxyXG59XHJcblxyXG4uZm9sbG93ZWQtaXRlbXMge1xyXG4gIG1hcmdpbi10b3A6IDEwJTtcclxuXHJcbiAgaW9uLWl0ZW0ge1xyXG4gICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xyXG4gIH1cclxufVxyXG5cclxuLmJhcnMge1xyXG4gIG1hcmdpbi10b3A6IDAlO1xyXG4gIHBhZGRpbmc6IDEycHg7XHJcblxyXG4gIC5wb2l0ZXIge1xyXG4gICAgei1pbmRleDogNTtcclxuICAgIG1hcmdpbi1sZWZ0OiAxJTtcclxuICAgIGJhY2tncm91bmQ6IHJnYmEoMjQwLCAyNDAsIDI0MCwgMC45Mik7XHJcbiAgICBib3JkZXItbGVmdDogMS4xcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xyXG4gICAgYm9yZGVyLXJpZ2h0OiAxLjFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNik7XHJcbiAgICBib3JkZXItdG9wOiAxcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHJnYigyMTYsIDIxNiwgMjE2KTtcclxuICB9XHJcblxyXG4gIC5iYXJzLWxvY2F0ZSB7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGJhY2tncm91bmQ6IHJnYmEoMjQwLCAyNDAsIDI0MCwgMC45Mik7XHJcbiAgICBib3JkZXItbGVmdDogMS4xcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xyXG4gICAgYm9yZGVyLXJpZ2h0OiAxLjFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNik7XHJcbiAgICBib3JkZXItdG9wOiAxcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHJnYigyMTYsIDIxNiwgMjE2KTtcclxuICAgIG1hcmdpbi1sZWZ0OiAwJTtcclxuXHJcbiAgICB6LWluZGV4OiAzO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTJweDtcclxuICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICBsaW5lLWhlaWdodDogMjBweDtcclxuICAgIGZvbnQtc2l6ZTogMS4yZW07XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcblxyXG4gICAgaW9uLWljb24ge1xyXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgIGZvbnQtc2l6ZTogMWVtO1xyXG4gICAgICBsZWZ0OiAyJTtcclxuICAgICAgY29sb3I6IHJnYigxMCwgMTAwLCAyMzUpO1xyXG4gICAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAuYmFycy1kZXN0aW5hdGUge1xyXG4gICAgaGVpZ2h0OiA1MHB4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDI0MCwgMjQwLCAyNDAsIDAuOTIpO1xyXG4gICAgbWFyZ2luOiAzJSAwIDAgLTUwcHg7XHJcbiAgICBtYXJnaW4tbGVmdDogMCU7XHJcbiAgICB6LWluZGV4OiAzO1xyXG4gICAgYm9yZGVyLWxlZnQ6IDEuMXB4IHNvbGlkIHJnYigyMTYsIDIxNiwgMjE2KTtcclxuICAgIGJvcmRlci1yaWdodDogMS4xcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xyXG4gICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkIHJnYigyMTYsIDIxNiwgMjE2KTtcclxuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNik7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTJweDtcclxuICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xyXG4gICAgZm9udC1zaXplOiAxLjJlbTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHJcbiAgICBpb24taWNvbiB7XHJcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgZm9udC1zaXplOiAxZW07XHJcbiAgICAgIGxlZnQ6IDIlO1xyXG4gICAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICAgIGNvbG9yOiByZ2JhKDIxOSwgMjA1LCA4LCAwLjkxKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC5iYXJzLXByaWNlIHtcclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYmFja2dyb3VuZDogI2ZmZmZmZjtcclxuICAgIGJvcmRlci1sZWZ0OiAxLjFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNik7XHJcbiAgICBib3JkZXItcmlnaHQ6IDEuMXB4IHNvbGlkIHJnYigyMTYsIDIxNiwgMjE2KTtcclxuICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNik7XHJcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xyXG5cclxuICAgIHotaW5kZXg6IDM7XHJcblxyXG4gICAgYm9yZGVyLXJhZGl1czogMTJweDtcclxuICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICBsaW5lLWhlaWdodDogMjBweDtcclxuICAgIGZvbnQtc2l6ZTogMS4yZW07XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcblxyXG4gICAgaW9uLWljb24ge1xyXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgIGZvbnQtc2l6ZTogMWVtO1xyXG4gICAgICBsZWZ0OiAyJTtcclxuICAgICAgY29sb3I6IHJnYigxMCwgMTAwLCAyMzUpO1xyXG4gICAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG4jcG9zaXRpb24ge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBwYWRkaW5nLWxlZnQ6IDE3cHg7XHJcbn1cclxuXHJcbiN3aGVyZXRvIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgcGFkZGluZy1sZWZ0OiAxN3B4O1xyXG59XHJcblxyXG4jY2FzaCB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIHBhZGRpbmctbGVmdDogMTdweDtcclxufVxyXG5cclxuI2J1dHRvbiB7XHJcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcclxufVxyXG4iLCIuZm9ybSB7XG4gIG1hcmdpbi1ib3R0b206IDI2cHg7XG4gIGhlaWdodDogMjAwcHggYXV0bztcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDEycHg7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG59XG4uZm9ybSBidXR0b24ge1xuICBtYXJnaW4tdG9wOiAyMHB4ICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1yYWRpdXM6IDEycHg7XG59XG4uZm9ybSAuc3RhY2sge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMWVtO1xufVxuLmZvcm0gLnN0YXIge1xuICB6LWluZGV4OiAzO1xuICB0b3A6IDE0LjUlO1xuICBtYXJnaW4tdG9wOiAzMCU7XG59XG4uZm9ybSAuc3RhciBpbnB1dCB7XG4gIHdpZHRoOiA0MCU7XG59XG5cbi50b3AtaXRlbXMge1xuICBib3JkZXItcmFkaXVzOiAxMnB4O1xuICBtYXJnaW4tdG9wOiAyMCU7XG59XG4udG9wLWl0ZW1zIGlvbi1pdGVtIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcbiAgYmFja2dyb3VuZDogIzBhNjRlYjtcbn1cbi50b3AtaXRlbXMgaW9uLWl0ZW0gaW9uLWxhYmVsIHtcbiAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLWxlZnQ6IDEwJTtcbn1cblxuI2VudmVsb3BlIHtcbiAgaGVpZ2h0OiBhdXRvO1xuICB3aWR0aDogNGVtO1xufVxuXG4ucmlkZSB7XG4gIGNvbG9yOiByZ2JhKDIxOSwgMjA1LCA4LCAwLjkxKTtcbiAgZm9udC1zaXplOiAxLjE3ZW07XG4gIHBhZGRpbmctdG9wOiAxNHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMTRweDtcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcbn1cbi5yaWRlIGlvbi1pY29uIHtcbiAgZm9udC1zaXplOiAwLjhlbTtcbiAgcGFkZGluZzogMTJweDtcbiAgY29sb3I6IHJnYmEoMjE5LCAyMDUsIDgsIDAuOTEpO1xufVxuXG4ucHJpY2Uge1xuICBjb2xvcjogcmdiYSgyMTksIDIwNSwgOCwgMC45MSk7XG4gIGZvbnQtc2l6ZTogMS42N2VtO1xuICBwYWRkaW5nLXRvcDogMTRweDtcbiAgcGFkZGluZy1ib3R0b206IDE0cHg7XG4gIGJvcmRlci1yYWRpdXM6IDEycHg7XG59XG4ucHJpY2UgaW9uLWljb24ge1xuICBmb250LXNpemU6IDAuOGVtO1xuICBwYWRkaW5nOiA1cHg7XG4gIGNvbG9yOiByZ2JhKDIxOSwgMjA1LCA4LCAwLjkxKTtcbn1cblxuLmRhdGUge1xuICBjb2xvcjogb3JhbmdlO1xuICBmb250LXNpemU6IDEuNDdlbTtcbiAgcGFkZGluZy10b3A6IDE0cHg7XG4gIHBhZGRpbmctYm90dG9tOiAxNHB4O1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2Q4ZDhkODtcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcbn1cbi5kYXRlIGlvbi1pY29uIHtcbiAgZm9udC1zaXplOiAwLjhlbTtcbiAgcGFkZGluZzogNXB4O1xuICBjb2xvcjogcmdiYSgyMTksIDIwNSwgOCwgMC45MSk7XG59XG5cbi50aW1lIHtcbiAgY29sb3I6ICMyYzg4ZjE7XG4gIGZvbnQtc2l6ZTogMS4xN2VtO1xuICBwYWRkaW5nLXRvcDogMTRweDtcbiAgcGFkZGluZy1ib3R0b206IDE0cHg7XG4gIGJvcmRlci1yYWRpdXM6IDEycHg7XG59XG4udGltZSBpb24taWNvbiB7XG4gIGZvbnQtc2l6ZTogMC44ZW07XG4gIHBhZGRpbmc6IDVweDtcbiAgY29sb3I6IHJnYmEoMjE5LCAyMDUsIDgsIDAuOTEpO1xufVxuXG4ubG9jYXRpb24ge1xuICB3aWR0aDogYXV0bztcbiAgcGFkZGluZy10b3A6IDhweDtcbiAgcGFkZGluZy1ib3R0b206IDhweDtcbn1cbi5sb2NhdGlvbiBwIHtcbiAgZm9udC1zaXplOiAxLjNlbTtcbiAgaGVpZ2h0OiBhdXRvO1xufVxuLmxvY2F0aW9uIGlvbi1pY29uIHtcbiAgZm9udC1zaXplOiAwLjhlbTtcbiAgcGFkZGluZzogNXB4O1xuICBjb2xvcjogb3JhbmdlO1xufVxuXG4uZGVzdGluYXRpb24ge1xuICB3aWR0aDogYXV0bztcbiAgcGFkZGluZy10b3A6IDhweDtcbiAgcGFkZGluZy1ib3R0b206IDhweDtcbn1cbi5kZXN0aW5hdGlvbiBpb24taWNvbiB7XG4gIGZvbnQtc2l6ZTogMC44ZW07XG4gIHBhZGRpbmc6IDVweDtcbiAgY29sb3I6IGRhcmtzbGF0ZWJsdWU7XG59XG5cbi5mb2xsb3dlZC1pdGVtcyB7XG4gIG1hcmdpbi10b3A6IDEwJTtcbn1cbi5mb2xsb3dlZC1pdGVtcyBpb24taXRlbSB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDEycHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNkOGQ4ZDg7XG59XG5cbi5iYXJzIHtcbiAgbWFyZ2luLXRvcDogMCU7XG4gIHBhZGRpbmc6IDEycHg7XG59XG4uYmFycyAucG9pdGVyIHtcbiAgei1pbmRleDogNTtcbiAgbWFyZ2luLWxlZnQ6IDElO1xuICBiYWNrZ3JvdW5kOiByZ2JhKDI0MCwgMjQwLCAyNDAsIDAuOTIpO1xuICBib3JkZXItbGVmdDogMS4xcHggc29saWQgI2Q4ZDhkODtcbiAgYm9yZGVyLXJpZ2h0OiAxLjFweCBzb2xpZCAjZDhkOGQ4O1xuICBib3JkZXItdG9wOiAxcHggc29saWQgI2Q4ZDhkODtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNkOGQ4ZDg7XG59XG4uYmFycyAuYmFycy1sb2NhdGUge1xuICBoZWlnaHQ6IDUwcHg7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kOiByZ2JhKDI0MCwgMjQwLCAyNDAsIDAuOTIpO1xuICBib3JkZXItbGVmdDogMS4xcHggc29saWQgI2Q4ZDhkODtcbiAgYm9yZGVyLXJpZ2h0OiAxLjFweCBzb2xpZCAjZDhkOGQ4O1xuICBib3JkZXItdG9wOiAxcHggc29saWQgI2Q4ZDhkODtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNkOGQ4ZDg7XG4gIG1hcmdpbi1sZWZ0OiAwJTtcbiAgei1pbmRleDogMztcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgbGluZS1oZWlnaHQ6IDIwcHg7XG4gIGZvbnQtc2l6ZTogMS4yZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5iYXJzIC5iYXJzLWxvY2F0ZSBpb24taWNvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgZm9udC1zaXplOiAxZW07XG4gIGxlZnQ6IDIlO1xuICBjb2xvcjogIzBhNjRlYjtcbiAgcGFkZGluZzogNXB4O1xufVxuLmJhcnMgLmJhcnMtZGVzdGluYXRlIHtcbiAgaGVpZ2h0OiA1MHB4O1xuICB3aWR0aDogMTAwJTtcbiAgYmFja2dyb3VuZDogcmdiYSgyNDAsIDI0MCwgMjQwLCAwLjkyKTtcbiAgbWFyZ2luOiAzJSAwIDAgLTUwcHg7XG4gIG1hcmdpbi1sZWZ0OiAwJTtcbiAgei1pbmRleDogMztcbiAgYm9yZGVyLWxlZnQ6IDEuMXB4IHNvbGlkICNkOGQ4ZDg7XG4gIGJvcmRlci1yaWdodDogMS4xcHggc29saWQgI2Q4ZDhkODtcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNkOGQ4ZDg7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZDhkOGQ4O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBib3JkZXItcmFkaXVzOiAxMnB4O1xuICBsaW5lLWhlaWdodDogMjBweDtcbiAgZm9udC1zaXplOiAxLjJlbTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLmJhcnMgLmJhcnMtZGVzdGluYXRlIGlvbi1pY29uIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBmb250LXNpemU6IDFlbTtcbiAgbGVmdDogMiU7XG4gIHBhZGRpbmc6IDVweDtcbiAgY29sb3I6IHJnYmEoMjE5LCAyMDUsIDgsIDAuOTEpO1xufVxuLmJhcnMgLmJhcnMtcHJpY2Uge1xuICBoZWlnaHQ6IDUwcHg7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kOiAjZmZmZmZmO1xuICBib3JkZXItbGVmdDogMS4xcHggc29saWQgI2Q4ZDhkODtcbiAgYm9yZGVyLXJpZ2h0OiAxLjFweCBzb2xpZCAjZDhkOGQ4O1xuICBib3JkZXItdG9wOiAxcHggc29saWQgI2Q4ZDhkODtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNkOGQ4ZDg7XG4gIHotaW5kZXg6IDM7XG4gIGJvcmRlci1yYWRpdXM6IDEycHg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICBmb250LXNpemU6IDEuMmVtO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uYmFycyAuYmFycy1wcmljZSBpb24taWNvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgZm9udC1zaXplOiAxZW07XG4gIGxlZnQ6IDIlO1xuICBjb2xvcjogIzBhNjRlYjtcbiAgcGFkZGluZzogNXB4O1xufVxuXG4jcG9zaXRpb24ge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHBhZGRpbmctbGVmdDogMTdweDtcbn1cblxuI3doZXJldG8ge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHBhZGRpbmctbGVmdDogMTdweDtcbn1cblxuI2Nhc2gge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHBhZGRpbmctbGVmdDogMTdweDtcbn1cblxuI2J1dHRvbiB7XG4gIGJvcmRlci1yYWRpdXM6IDEycHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/booklater/booklater.page.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/booklater/booklater.page.ts ***!
  \***************************************************/
/*! exports provided: BooklaterPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BooklaterPage", function() { return BooklaterPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/onesignal/ngx */ "./node_modules/@ionic-native/onesignal/ngx/index.js");
/* harmony import */ var src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/settings.service */ "./src/app/services/settings.service.ts");
/* harmony import */ var src_app_services_language_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/language.service */ "./src/app/services/language.service.ts");
/* harmony import */ var src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/profile.service */ "./src/app/services/profile.service.ts");
/* harmony import */ var src_app_services_directionservice_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/directionservice.service */ "./src/app/services/directionservice.service.ts");
/* harmony import */ var src_app_services_geocoder_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/geocoder.service */ "./src/app/services/geocoder.service.ts");
/* harmony import */ var src_app_services_native_map_container_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/services/native-map-container.service */ "./src/app/services/native-map-container.service.ts");
/* harmony import */ var src_app_services_event_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/app/services/event.service */ "./src/app/services/event.service.ts");
/* harmony import */ var src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! src/app/services/pop-up.service */ "./src/app/services/pop-up.service.ts");
/* harmony import */ var _autocomplete_autocomplete_page__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../autocomplete/autocomplete.page */ "./src/app/pages/autocomplete/autocomplete.page.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");














let BooklaterPage = class BooklaterPage {
    constructor(navCtrl, settings, actRoute, lp, alertCtrl, platform, ph, dProvider, cMap, gCode, One, pop, eventProvider, modalCtrl) {
        this.navCtrl = navCtrl;
        this.settings = settings;
        this.actRoute = actRoute;
        this.lp = lp;
        this.alertCtrl = alertCtrl;
        this.platform = platform;
        this.ph = ph;
        this.dProvider = dProvider;
        this.cMap = cMap;
        this.gCode = gCode;
        this.One = One;
        this.pop = pop;
        this.eventProvider = eventProvider;
        this.modalCtrl = modalCtrl;
        this.hasBooked = false;
        // tslint:disable-next-line: new-parens
        this.geocoder = new google.maps.Geocoder;
        this.toppings = 'London Stansted Airport';
        this.service = new google.maps.DistanceMatrixService();
        this.Lang = this.lp.translate();
        this.settings.appCountryCode = 'UK';
        this.lat = this.actRoute.snapshot.paramMap.get('lat');
        this.lng = this.actRoute.snapshot.paramMap.get('lng');
        this.userPos = new google.maps.LatLng(this.lat, this.lng);
        const latlng = { lat: parseFloat(this.lat), lng: parseFloat(this.lng) };
        this.geocoder.geocode({ location: latlng }, (results, status) => {
            if (status === 'OK') {
                this.locationName = results[0].formatted_address;
                this.location = this.locationName;
            }
            else {
            }
        });
        this.currentYear = new Date().getFullYear();
        this.currentMonth = new Date().getUTCMonth() + 1;
        this.currentDay = new Date().getUTCDate();
        this.pop.presentLoader('');
        this.ph.getWebAdminProfile().on('value', userProfileSnapshot => {
            const admin = userProfileSnapshot.val();
            this.dProvider.fare = admin.price;
            this.dProvider.pricePerKm = admin.perkm;
            this.pop.hideLoader();
        });
        if (!this.platform.is('cordova')) {
            this.id = '3456789098765456733';
            this.ph.getScheduledProfile(this.id).on('value', userProfileSnapshot => {
                this.scheduleInfo = userProfileSnapshot.val();
                if (this.scheduleInfo != null) {
                    const today = new Date();
                    const future = new Date(this.scheduleInfo.Client_Date);
                    console.log(future);
                    this.dataTime = this.calcDate(today, future);
                    console.log(this.dataTime);
                }
                this.ph.getScheduledProfile(this.id).off('value');
            });
        }
        else {
            this.One.getIds().then(id => {
                this.userID = id.userId;
                this.ph.getScheduledProfile(this.userID).on('value', userProfileSnapshot => {
                    this.scheduleInfo = userProfileSnapshot.val();
                    if (this.scheduleInfo != null) {
                        const today = new Date();
                        const future = new Date(this.scheduleInfo.Client_Date);
                        console.log(future);
                        this.dataTime = this.calcDate(today, future);
                        console.log(this.dataTime);
                    }
                    this.ph.getScheduledProfile(this.id).off('value');
                });
            });
        }
    }
    ionViewDidEnter() {
        this.eventProvider.getScheduledList().on('value', snapshot => {
            this.eventList = [];
            snapshot.forEach(snap => {
                this.eventList.push({
                    id: snap.key,
                    date: snap.val().TimeandDate,
                });
                return false;
            });
            this.eventProvider.getScheduledList().off('value');
        });
    }
    calcDate(date1, date2) {
        const diff = Math.floor(date2.getTime() - date1.getTime());
        const day = 1000 * 60 * 60 * 24;
        const days = Math.floor(diff / day);
        const months = Math.floor(days / 31);
        const message = date2.toDateString();
        return message;
    }
    Chosen(e) {
        if (this.currentYear <= e.year) {
            console.log(this.userPos, this.userDes);
            if (this.userPos != null && this.userDes != null) {
                console.log(e);
                const date = [];
                const time = [];
                date.push(e.year, e.month, e.day);
                time.push(e.hour, e.minute);
                console.log(date);
                this.calcScheduleRoute(this.userPos, this.userDes, this.destination, this.toppings, date, time);
            }
            else {
                this.pop.showPimp(this.Lang[0].addDest);
            }
        }
        else {
            this.pop.showPimp(this.Lang[0].addTime);
            console.log(this.currentMonth, this.currentDay);
        }
    }
    calcScheduleRoute(start, stop, destinationName, locationName, date, time) {
        this.pop.presentLoader('');
        this.service.getDistanceMatrix({
            origins: [start, locationName],
            destinations: [destinationName, stop],
            travelMode: 'DRIVING',
            unitSystem: google.maps.UnitSystem.METRIC,
            avoidHighways: false,
            avoidTolls: false,
        }, (response, status) => {
            if (status === 'OK') {
                console.log(this.dProvider.pricePerKm, this.dProvider.fare);
                let image = this.ph.user.photoURL;
                let name = this.ph.user.displayName;
                // tslint:disable-next-line: variable-name
                const edited_name = this.ph.name;
                let pay = this.ph.paymentType;
                this.pop.calculateBtn = false;
                if (image == null) {
                    if (this.ph.pic == null) {
                        image = 'https://cdn1.iconfinder.com/data/icons/instagram-ui-glyph/48/Sed-10-128.png';
                    }
                    else {
                        image = this.ph.pic;
                    }
                }
                if (name == null) {
                    if (edited_name != null) {
                        name = edited_name;
                    }
                    else {
                        name = this.ph.user.email;
                    }
                }
                if (pay == null) {
                    pay = 1;
                }
                if (this.lat == null && this.lng == null) {
                    this.lat = this.actRoute.snapshot.paramMap.get('lat');
                    this.lng = this.actRoute.snapshot.paramMap.get('lng');
                }
                if (!this.platform.is('cordova')) {
                    const id = '3456789098765456733';
                    this.ph.getUserProfile().on('value', userProfileSnapshot => {
                        let ratingText = userProfileSnapshot.val().ratingtext;
                        let ratingValue = userProfileSnapshot.val().rating;
                        if (ratingText == null && ratingValue == null) {
                            ratingText = this.Lang[0].notrate;
                            ratingValue = 0;
                        }
                        this.ph.getUserProfile().off('value');
                        console.log(name, image, this.lat, this.lng, this.toppings, pay, this.destination, this.ph.phone, date, ratingText, ratingValue, time);
                        this.eventProvider.CreateLondonBook(name, image, this.lat, this.lng, this.toppings, pay, this.destination, this.ph.phone, date, id, ratingText, ratingValue, time).then(s => {
                            this.pop.hideLoader();
                            this.pop.showPimp('Your ride has been scheduled successfully.');
                        });
                    });
                }
                else {
                    this.One.getIds().then(id => {
                        this.userID = id.userId;
                        this.ph.getUserProfile().on('value', userProfileSnapshot => {
                            let ratingText = userProfileSnapshot.val().ratingtext;
                            let ratingValue = userProfileSnapshot.val().rating;
                            if (ratingText == null && ratingValue == null) {
                                ratingText = this.Lang[0].notrate;
                                ratingValue = 0;
                            }
                            this.lat = this.actRoute.snapshot.paramMap.get('lat');
                            this.lng = this.actRoute.snapshot.paramMap.get('lng');
                            this.ph.getUserProfile().off('value');
                            console.log(name, image, this.lat, this.lng, this.toppings, pay, this.destination, this.ph.phone, date);
                            this.eventProvider.CreateLondonBook(name, image, this.lat, this.lng, this.toppings, pay, this.destination, this.ph.phone, date, this.userID, ratingText, ratingValue, time).then(s => {
                                this.pop.hideLoader();
                                this.pop.showPimp(this.Lang[0].sucsch);
                            });
                        });
                    });
                }
            }
        });
    }
    showAddressModal(selectedBar) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const modal = yield this.modalCtrl.create({
                component: _autocomplete_autocomplete_page__WEBPACK_IMPORTED_MODULE_12__["AutocompletePage"]
            });
            modal.onDidDismiss()
                .then((data) => {
                // Open the address modal on location bar click to change location
                console.log(data);
                if (selectedBar === 1 && data != null) {
                    document.getElementById('position').innerText = data;
                    this.gCode.geocoder.geocode({ address: data }, (results, status) => {
                        if (status === 'OK') {
                            const position = results[0].geometry.location;
                            this.userPos = new google.maps.LatLng(position.lat(), position.lng());
                            this.lat = position.lat();
                            this.lng = position.lng();
                        }
                    });
                }
                // Open the address modal on destination bar click to change destination
                if (selectedBar === 2 && data != null) {
                    document.getElementById('whereto').innerText = data;
                    this.destination = data;
                    /// After data input, check to see if user selected to add a destination or to calculate distance.
                    this.gCode.geocoder.geocode({ address: data }, (results, status) => {
                        if (status === 'OK') {
                            const position = results[0].geometry.location;
                            this.userDes = new google.maps.LatLng(position.lat(), position.lng());
                        }
                    });
                }
            });
            return yield modal.present();
        });
    }
    CancelRide() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertCtrl.create({
                message: this.Lang[0].error,
                buttons: [
                    {
                        text: this.Lang[0].reject,
                    },
                    {
                        text: this.Lang[0].accept,
                        handler: data => {
                            this.remove();
                        }
                    }
                ]
            });
            alert.present();
        });
    }
    remove() {
        this.One.getIds().then(id => {
            this.userID = id.userId;
            this.ph.getScheduledProfile(this.userID).remove().then(s => {
                this.pop.showPimp(this.Lang[0].sucSchw);
                this.hasBooked = false;
            });
        });
    }
    ngOnInit() {
    }
};
BooklaterPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_4__["SettingsService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_13__["ActivatedRoute"] },
    { type: src_app_services_language_service__WEBPACK_IMPORTED_MODULE_5__["LanguageService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
    { type: src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_6__["ProfileService"] },
    { type: src_app_services_directionservice_service__WEBPACK_IMPORTED_MODULE_7__["DirectionserviceService"] },
    { type: src_app_services_native_map_container_service__WEBPACK_IMPORTED_MODULE_9__["NativeMapContainerService"] },
    { type: src_app_services_geocoder_service__WEBPACK_IMPORTED_MODULE_8__["GeocoderService"] },
    { type: _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_3__["OneSignal"] },
    { type: src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_11__["PopUpService"] },
    { type: src_app_services_event_service__WEBPACK_IMPORTED_MODULE_10__["EventService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] }
];
BooklaterPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-booklater',
        template: __webpack_require__(/*! raw-loader!./booklater.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/booklater/booklater.page.html"),
        styles: [__webpack_require__(/*! ./booklater.page.scss */ "./src/app/pages/booklater/booklater.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_4__["SettingsService"], _angular_router__WEBPACK_IMPORTED_MODULE_13__["ActivatedRoute"],
        src_app_services_language_service__WEBPACK_IMPORTED_MODULE_5__["LanguageService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"], src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_6__["ProfileService"], src_app_services_directionservice_service__WEBPACK_IMPORTED_MODULE_7__["DirectionserviceService"],
        src_app_services_native_map_container_service__WEBPACK_IMPORTED_MODULE_9__["NativeMapContainerService"], src_app_services_geocoder_service__WEBPACK_IMPORTED_MODULE_8__["GeocoderService"],
        _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_3__["OneSignal"], src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_11__["PopUpService"], src_app_services_event_service__WEBPACK_IMPORTED_MODULE_10__["EventService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])
], BooklaterPage);



/***/ })

}]);
//# sourceMappingURL=pages-booklater-booklater-module-es2015.js.map