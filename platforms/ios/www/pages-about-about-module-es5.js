(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-about-about-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/about/about.page.html":
/*!***********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/about/about.page.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header no-border>\n  <ion-toolbar style=\"margin-top: 20px\">\n    <ion-button size=\"large\" fill=\"clear\" (click)=\"goBack()\">\n      <ion-icon name=\"arrow-back\"></ion-icon>\n      <span style=\"padding-left: 30px; font-size: 1em\">\n        {{this.Lang[0].about}}</span\n      >\n    </ion-button>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"no-scroll\" class=\"ion-padding\">\n  <div class=\"cont\">\n    <ion-button\n      style=\"color: rgb(10, 100, 235)\"\n      expand=\"block\"\n      (click)=\"gotoSite()\"\n    >\n      <span style=\"color: white\">\n        {{settings.appName}} {{Lang[0].website}}\n      </span>\n    </ion-button>\n\n    <br />\n    <ion-button style=\"color: #1d93ca\" expand=\"block\" (click)=\"gotoSite9()\">\n      <span style=\"color: white\"> Facebook </span>\n    </ion-button>\n\n    <ion-button color=\"danger\" expand=\"block\" (click)=\"gotoSite10()\">\n      Instagram <strong>@{{settings.appName}}</strong>\n    </ion-button>\n     <ion-button color=\"danger\" expand=\"block\" (click)=\"twitter()\">\n      Twitter <strong>@{{settings.appName}}</strong>\n    </ion-button>\n\n\n   <ion-button size=\"large\" color=\"danger\" expand=\"block\" (click)=\"appTikTok()\">\n    Tiktok \n  </ion-button>\n   <ion-button size=\"large\" color=\"danger\" expand=\"block\" (click)=\"appTube()\">\n    Youtube \n  </ion-button>\n  </div>\n\n  \n</ion-content>\n"

/***/ }),

/***/ "./src/app/pages/about/about.module.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/about/about.module.ts ***!
  \*********************************************/
/*! exports provided: AboutPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutPageModule", function() { return AboutPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _about_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./about.page */ "./src/app/pages/about/about.page.ts");







var routes = [
    {
        path: '',
        component: _about_page__WEBPACK_IMPORTED_MODULE_6__["AboutPage"]
    }
];
var AboutPageModule = /** @class */ (function () {
    function AboutPageModule() {
    }
    AboutPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_about_page__WEBPACK_IMPORTED_MODULE_6__["AboutPage"]]
        })
    ], AboutPageModule);
    return AboutPageModule;
}());



/***/ }),

/***/ "./src/app/pages/about/about.page.scss":
/*!*********************************************!*\
  !*** ./src/app/pages/about/about.page.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content ion-button {\n  border-radius: 12px;\n  height: 70px;\n}\nion-content ion-button {\n  margin-top: 60%;\n  height: 50px;\n  border-radius: 12px;\n}\nion-button {\n  box-shadow: none;\n  border-radius: 0px;\n  height: 54px;\n  font-size: 1.2em;\n}\nion-button {\n  box-shadow: none;\n  border-radius: 0px;\n  height: 54px;\n  font-size: 1.2em;\n}\n.cont ion-button {\n  border-radius: 12px;\n  height: 50px;\n  margin: 0.8%;\n  --ion-color-light: #eeeeee;\n  -webkit-animation-direction: alternate;\n          animation-direction: alternate;\n  -webkit-animation-name: wiki;\n          animation-name: wiki;\n  -webkit-animation-duration: 0.3s;\n          animation-duration: 0.3s;\n  -webkit-animation-iteration-count: 1;\n          animation-iteration-count: 1;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYWhhL0lvbmljQXBwcy9DNFJpZGUtSU9TLTIwMjQvY2FsbDRyaWRlLXBhc3Nlbmdlci9zcmMvYXBwL3BhZ2VzL2Fib3V0L2Fib3V0LnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvYWJvdXQvYWJvdXQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNFO0VBQ0UsbUJBQUE7RUFDQSxZQUFBO0FDQUo7QURFRTtFQUNFLGVBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7QUNBSjtBRElBO0VBQ0UsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtBQ0RGO0FER0E7RUFDRSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0FDQUY7QURJRTtFQUNFLG1CQUFBO0VBRUEsWUFBQTtFQUNBLFlBQUE7RUFDQSwwQkFBQTtFQUNBLHNDQUFBO1VBQUEsOEJBQUE7RUFDQSw0QkFBQTtVQUFBLG9CQUFBO0VBQ0EsZ0NBQUE7VUFBQSx3QkFBQTtFQUNBLG9DQUFBO1VBQUEsNEJBQUE7QUNGSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2Fib3V0L2Fib3V0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50IHtcclxuICBpb24tYnV0dG9uIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDEycHg7XHJcbiAgICBoZWlnaHQ6IDcwcHg7XHJcbiAgfVxyXG4gIGlvbi1idXR0b24ge1xyXG4gICAgbWFyZ2luLXRvcDogNjAlO1xyXG4gICAgaGVpZ2h0OiA1MHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTJweDtcclxuICB9XHJcbn1cclxuXHJcbmlvbi1idXR0b24ge1xyXG4gIGJveC1zaGFkb3c6IG5vbmU7XHJcbiAgYm9yZGVyLXJhZGl1czogMHB4O1xyXG4gIGhlaWdodDogNTRweDtcclxuICBmb250LXNpemU6IDEuMmVtO1xyXG59XHJcbmlvbi1idXR0b24ge1xyXG4gIGJveC1zaGFkb3c6IG5vbmU7XHJcbiAgYm9yZGVyLXJhZGl1czogMHB4O1xyXG4gIGhlaWdodDogNTRweDtcclxuICBmb250LXNpemU6IDEuMmVtO1xyXG59XHJcblxyXG4uY29udCB7XHJcbiAgaW9uLWJ1dHRvbiB7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG5cclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIG1hcmdpbjogMC44JTtcclxuICAgIC0taW9uLWNvbG9yLWxpZ2h0OiAjZWVlZWVlO1xyXG4gICAgYW5pbWF0aW9uLWRpcmVjdGlvbjogYWx0ZXJuYXRlO1xyXG4gICAgYW5pbWF0aW9uLW5hbWU6IHdpa2k7XHJcbiAgICBhbmltYXRpb24tZHVyYXRpb246IDAuM3M7XHJcbiAgICBhbmltYXRpb24taXRlcmF0aW9uLWNvdW50OiAxO1xyXG4gIH1cclxufVxyXG4iLCJpb24tY29udGVudCBpb24tYnV0dG9uIHtcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcbiAgaGVpZ2h0OiA3MHB4O1xufVxuaW9uLWNvbnRlbnQgaW9uLWJ1dHRvbiB7XG4gIG1hcmdpbi10b3A6IDYwJTtcbiAgaGVpZ2h0OiA1MHB4O1xuICBib3JkZXItcmFkaXVzOiAxMnB4O1xufVxuXG5pb24tYnV0dG9uIHtcbiAgYm94LXNoYWRvdzogbm9uZTtcbiAgYm9yZGVyLXJhZGl1czogMHB4O1xuICBoZWlnaHQ6IDU0cHg7XG4gIGZvbnQtc2l6ZTogMS4yZW07XG59XG5cbmlvbi1idXR0b24ge1xuICBib3gtc2hhZG93OiBub25lO1xuICBib3JkZXItcmFkaXVzOiAwcHg7XG4gIGhlaWdodDogNTRweDtcbiAgZm9udC1zaXplOiAxLjJlbTtcbn1cblxuLmNvbnQgaW9uLWJ1dHRvbiB7XG4gIGJvcmRlci1yYWRpdXM6IDEycHg7XG4gIGhlaWdodDogNTBweDtcbiAgbWFyZ2luOiAwLjglO1xuICAtLWlvbi1jb2xvci1saWdodDogI2VlZWVlZTtcbiAgYW5pbWF0aW9uLWRpcmVjdGlvbjogYWx0ZXJuYXRlO1xuICBhbmltYXRpb24tbmFtZTogd2lraTtcbiAgYW5pbWF0aW9uLWR1cmF0aW9uOiAwLjNzO1xuICBhbmltYXRpb24taXRlcmF0aW9uLWNvdW50OiAxO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/about/about.page.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/about/about.page.ts ***!
  \*******************************************/
/*! exports provided: AboutPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutPage", function() { return AboutPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/in-app-browser/ngx */ "./node_modules/@ionic-native/in-app-browser/ngx/index.js");
/* harmony import */ var src_app_services_language_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/language.service */ "./src/app/services/language.service.ts");
/* harmony import */ var src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/settings.service */ "./src/app/services/settings.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");




// import { IonicPage } from '@ionic/angular';



var AboutPage = /** @class */ (function () {
    function AboutPage(iab, lp, settings, navCtrl, location) {
        this.iab = iab;
        this.lp = lp;
        this.settings = settings;
        this.navCtrl = navCtrl;
        this.location = location;
        this.Lang = this.lp.translate();
    }
    AboutPage.prototype.gotoSite = function () {
        this.iab.create(this.settings.appLink);
    };
    AboutPage.prototype.gotoSite2 = function () {
        this.iab.create(this.settings.appCareer);
    };
    AboutPage.prototype.gotoSite3 = function () {
        this.iab.create(this.settings.appFaq);
    };
    AboutPage.prototype.gotoSite4 = function () {
        this.iab.create(this.settings.appLink);
    };
    AboutPage.prototype.gotoSite9 = function () {
        this.iab.create(this.settings.appFB);
    };
    AboutPage.prototype.gotoSite10 = function () {
        this.iab.create(this.settings.appinsta);
    };
    AboutPage.prototype.appTikTok = function () {
        this.iab.create(this.settings.appTikTok);
    };
    AboutPage.prototype.appTube = function () {
        this.iab.create(this.settings.appYoutube);
    };
    AboutPage.prototype.twitter = function () {
        this.iab.create(this.settings.twitter);
    };
    AboutPage.prototype.goBack = function () {
        this.navCtrl.navigateRoot('home');
    };
    AboutPage.prototype.ngOnInit = function () {
    };
    AboutPage.ctorParameters = function () { return [
        { type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_3__["InAppBrowser"] },
        { type: src_app_services_language_service__WEBPACK_IMPORTED_MODULE_4__["LanguageService"] },
        { type: src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_5__["SettingsService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
        { type: _angular_common__WEBPACK_IMPORTED_MODULE_6__["Location"] }
    ]; };
    AboutPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-about',
            template: __webpack_require__(/*! raw-loader!./about.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/about/about.page.html"),
            styles: [__webpack_require__(/*! ./about.page.scss */ "./src/app/pages/about/about.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_3__["InAppBrowser"], src_app_services_language_service__WEBPACK_IMPORTED_MODULE_4__["LanguageService"], src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_5__["SettingsService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["Location"]])
    ], AboutPage);
    return AboutPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-about-about-module-es5.js.map