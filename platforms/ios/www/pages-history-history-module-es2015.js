(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-history-history-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/history/history.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/history/history.page.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header no-border>\n  <ion-toolbar style=\"margin-top: 20px\">\n    <ion-button (click)=\"goBack()\" ion-button color=\"primary\" fill=\"clear\">\n      <ion-icon style=\"font-size: 1.5em\" name=\"arrow-back\"></ion-icon>\n      <span style=\"margin-left: 30px; font-size: 1.4em\">Trips</span>\n    </ion-button>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding class=\"yes-scroll\">\n  <div class=\"followed-items\">\n    <ion-list>\n      <div class=\"test\" *ngIf=\"eventList == undefined || eventList.length < 1 \">\n        <ion-item style=\"text-align: center;\">\n          <h4 style=\"text-align: center;\">\n            *No trip history here<span style='font-size:10px;'></span>\n          </h4>\n        </ion-item>\n      </div>\n\n      <div>\n        <ion-item class=\"ion-item\" lines=\"none\" *ngFor=\"let event of eventList\">\n          <ion-grid>\n            <ion-row>\n              <ion-col>\n                <ion-label class=\"drive\"\n                  ><strong>{{event?.name}}</strong></ion-label\n                >\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col>\n                <ion-label class=\"price\"\n                  ><strong\n                    >{{settings.appcurrency}}{{(float(event?.price) +\n                    float(event?.tip || 0)).toFixed(2)}}</strong\n                  ></ion-label\n                >\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col>\n                <ion-label class=\"date\"\n                  ><strong>{{event?.date}}</strong></ion-label\n                >\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col>\n                <ion-label class=\"location\"\n                  ><strong>{{event?.location}}</strong></ion-label\n                >\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col>\n                <ion-label class=\"destination\"\n                  ><strong>{{event?.destination}}</strong></ion-label\n                >\n              </ion-col>\n            </ion-row>\n\n            <ion-button\n              class=\"ion-button\"\n              size=\"large\"\n              color=\"primary\"\n              shape=\"round\"\n              expand=\"block\"\n              (click)=\"goToEventDetail(event.id)\"\n            >\n              <ion-icon\n                color=\"light\"\n                name=\"arrow-round-forward\"\n                slot=\"icon-only\"\n              ></ion-icon>\n            </ion-button>\n          </ion-grid>\n        </ion-item>\n      </div>\n    </ion-list>\n  </div>\n</ion-content>\n\n<!-- <ion-footer padding>\n    <ion-button color=\"primary\" size='large' shape='round' expand='block' (click)=\"OpenCancelled()\">\n        View Cancelled Trips\n    </ion-button>\n</ion-footer> -->\n"

/***/ }),

/***/ "./src/app/pages/history/history.module.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/history/history.module.ts ***!
  \*************************************************/
/*! exports provided: HistoryPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistoryPageModule", function() { return HistoryPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _history_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./history.page */ "./src/app/pages/history/history.page.ts");







const routes = [
    {
        path: '',
        component: _history_page__WEBPACK_IMPORTED_MODULE_6__["HistoryPage"]
    }
];
let HistoryPageModule = class HistoryPageModule {
};
HistoryPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_history_page__WEBPACK_IMPORTED_MODULE_6__["HistoryPage"]]
    })
], HistoryPageModule);



/***/ }),

/***/ "./src/app/pages/history/history.page.scss":
/*!*************************************************!*\
  !*** ./src/app/pages/history/history.page.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content .drive {\n  color: #0a64eb;\n  font-family: \"Montserrat\";\n}\nion-content .price {\n  color: green;\n}\nion-content .date {\n  color: orangered;\n}\nion-content .destination {\n  color: cadetblue;\n}\nion-content ion-item {\n  margin-top: 24px;\n  border-bottom: 1px solid #d8d8d8 !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYWhhL0lvbmljQXBwcy9DNFJpZGUtSU9TLTIwMjQvY2FsbDRyaWRlLXBhc3Nlbmdlci9zcmMvYXBwL3BhZ2VzL2hpc3RvcnkvaGlzdG9yeS5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2hpc3RvcnkvaGlzdG9yeS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0U7RUFDRSxjQUFBO0VBQ0EseUJBQUE7QUNBSjtBREdFO0VBQ0UsWUFBQTtBQ0RKO0FESUU7RUFDRSxnQkFBQTtBQ0ZKO0FES0U7RUFDRSxnQkFBQTtBQ0hKO0FETUU7RUFDRSxnQkFBQTtFQUNBLDJDQUFBO0FDSkoiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9oaXN0b3J5L2hpc3RvcnkucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge1xyXG4gIC5kcml2ZSB7XHJcbiAgICBjb2xvcjogcmdiKDEwLCAxMDAsIDIzNSk7XHJcbiAgICBmb250LWZhbWlseTogXCJNb250c2VycmF0XCI7XHJcbiAgfVxyXG5cclxuICAucHJpY2Uge1xyXG4gICAgY29sb3I6IGdyZWVuO1xyXG4gIH1cclxuXHJcbiAgLmRhdGUge1xyXG4gICAgY29sb3I6IG9yYW5nZXJlZDtcclxuICB9XHJcblxyXG4gIC5kZXN0aW5hdGlvbiB7XHJcbiAgICBjb2xvcjogY2FkZXRibHVlO1xyXG4gIH1cclxuXHJcbiAgaW9uLWl0ZW0ge1xyXG4gICAgbWFyZ2luLXRvcDogMjRweDtcclxuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNikgIWltcG9ydGFudDtcclxuICB9XHJcbn1cclxuIiwiaW9uLWNvbnRlbnQgLmRyaXZlIHtcbiAgY29sb3I6ICMwYTY0ZWI7XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXRcIjtcbn1cbmlvbi1jb250ZW50IC5wcmljZSB7XG4gIGNvbG9yOiBncmVlbjtcbn1cbmlvbi1jb250ZW50IC5kYXRlIHtcbiAgY29sb3I6IG9yYW5nZXJlZDtcbn1cbmlvbi1jb250ZW50IC5kZXN0aW5hdGlvbiB7XG4gIGNvbG9yOiBjYWRldGJsdWU7XG59XG5pb24tY29udGVudCBpb24taXRlbSB7XG4gIG1hcmdpbi10b3A6IDI0cHg7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZDhkOGQ4ICFpbXBvcnRhbnQ7XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/history/history.page.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/history/history.page.ts ***!
  \***********************************************/
/*! exports provided: HistoryPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistoryPage", function() { return HistoryPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_language_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/language.service */ "./src/app/services/language.service.ts");
/* harmony import */ var src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/pop-up.service */ "./src/app/services/pop-up.service.ts");
/* harmony import */ var src_app_services_event_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/event.service */ "./src/app/services/event.service.ts");
/* harmony import */ var src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/settings.service */ "./src/app/services/settings.service.ts");
/* harmony import */ var src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/profile.service */ "./src/app/services/profile.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");









let HistoryPage = class HistoryPage {
    constructor(http, navCtrl, alert, ph, lp, settings, pop, load, eventProvider) {
        this.http = http;
        this.navCtrl = navCtrl;
        this.alert = alert;
        this.ph = ph;
        this.lp = lp;
        this.settings = settings;
        this.pop = pop;
        this.load = load;
        this.eventProvider = eventProvider;
        this.math = Math;
        this.float = parseFloat;
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpHeaders"]({ "Content-Type": "application/json" }),
        };
    }
    ngOnInit() { }
    ionViewDidEnter() {
        let pinket;
        this.pop.presentLoader("").then(() => {
            this.pop.hideLoader();
        });
        this.eventProvider.getEventList().on("value", (snapshot) => {
            this.eventList = [];
            snapshot.forEach((snap) => {
                this.eventList.push({
                    id: snap.key,
                    name: snap.val().name,
                    price: snap.val().price + snap.val().waitTimeCost,
                    date: snap.val().date,
                    location: snap.val().location,
                    destination: snap.val().destination,
                    tip: snap.val().tip -
                        this.checkMe(snap.val().tip, snap.val().surcharge || []),
                    upvote: snap.val().upvote || 0,
                    downvote: snap.val().downvote || 0,
                    toll: snap.val().tolls,
                    surcharge: snap.val().surcharge,
                    realPrice: snap.val().realPrice + snap.val().waitTimeCost,
                    waitTimeCost: snap.val().waitTimeCost,
                    osc: snap.val().osc,
                    driver_id: snap.val().driver_id,
                    driver_key: snap.val().driver_key,
                });
                this.eventList.sort();
                this.eventList.reverse();
                console.log("EVENT LISTTTS:::::::", this.eventList);
                console.log("EVENT LISTTTS:::::::", this.eventList.length);
                return false;
            });
            console.log("EVENT LISTTTS:::::::", this.eventList.length);
        });
    }
    checkMe(price, surcharge) {
        let c = [];
        let n = [];
        console.log(surcharge);
        surcharge.forEach((element) => {
            this.riderpaid = parseFloat(price).toFixed(2);
            //if driver
            if (element.owner == 0) {
                //if percent
                if (element.bone == 1) {
                    let nb = element.price / 100;
                    console.log(nb * this.riderpaid);
                    let fo = nb * this.riderpaid;
                    n.push(fo);
                    const add2 = (a, b) => a + b;
                    const result2 = n.reduce(add2);
                    this.percentDriver = result2;
                    console.log((Math.floor(element.price) / 100) * this.riderpaid);
                }
                //if flat fee
                if (element.bone == 0) {
                    c.push(parseFloat(element.price));
                    const add4 = (a, b) => a + b;
                    const result4 = c.reduce(add4);
                    this.flatDriver = result4;
                    console.log(result4);
                }
                this.totalDriverSurge = this.flatDriver + this.percentDriver;
                console.log(this.totalDriverSurge, this.flatDriver, this.percentDriver);
            }
        });
        return this.totalDriverSurge;
    }
    goToEventDetail(eventId) {
        this.navCtrl.navigateRoot(["history-details", { eventId: eventId }]);
    }
    Approve(id, up, down) {
        this.ph.updateHistoryVoteUpSingle(id, up + 1);
    }
    disapprove(id, up, down) {
        this.ph.updateHistoryVoteDownSingle(id, down - 1);
    }
    OpenCancelled() {
        this.navCtrl.navigateRoot("cancelled");
    }
    TipMe(id, driver_id, mykey) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alert.create({
                message: "Enter Amount in $",
                inputs: [
                    {
                        value: "",
                    },
                ],
                buttons: [
                    {
                        text: this.lp.translate()[0].cancel,
                    },
                    {
                        text: this.lp.translate()[0].accept,
                        handler: (data) => {
                            console.log(data[0]);
                            //this.pop.showLoader('')
                            this.ph.updateDriiverTip(data[0], driver_id, mykey);
                            this.ph.updateHistoryTip(data[0], id).then(() => {
                                this.payWithStripe(parseFloat(data[0]).toFixed(2));
                                this.pop.showPimp("Driver has Been Tipped");
                                //this.pop.hideLoader();
                            });
                        },
                    },
                ],
            });
            alert.present();
        });
    }
    payWithStripe(amt) {
        //console.log(parseFloat(amt), this.custom_ID)
        this.pop.SmartLoader("Charging...");
        // this.http
        //   .post(
        //     "https://us-central1-ridefhv-61945.cloudfunctions.net/payWithStripe",
        //     {
        //       amount: amt * 100,
        //       currency: "usd",
        //       customer: this.ph.customerID,
        //     }
        //   )
        //   .pipe(map((response: any) => response.json()))
        //   .subscribe(
        //     (res) => {
        //       this.pop.hideLoader();
        //     },
        //     (error) => {
        //       this.showPimp(
        //         "Credit Card Error. Please select a valid credit card from your payments settings."
        //       );
        //       //this.pop.hideLoader();
        //     }
        //   );
    }
    showPimp(title) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alert.create({
                message: title,
                buttons: [
                    {
                        text: "Cancel",
                        role: "cancel",
                        handler: () => { },
                    },
                    {
                        text: "Go To Payments",
                        role: "cancel",
                        handler: () => {
                            this.navCtrl.navigateRoot("");
                        },
                    },
                ],
                backdropDismiss: false,
            });
            yield alert.present();
        });
    }
    goBack() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.navCtrl.navigateRoot("home");
        });
    }
};
HistoryPage.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpClient"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_7__["ProfileService"] },
    { type: src_app_services_language_service__WEBPACK_IMPORTED_MODULE_3__["LanguageService"] },
    { type: src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_6__["SettingsService"] },
    { type: src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_4__["PopUpService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: src_app_services_event_service__WEBPACK_IMPORTED_MODULE_5__["EventService"] }
];
HistoryPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-history",
        template: __webpack_require__(/*! raw-loader!./history.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/history/history.page.html"),
        styles: [__webpack_require__(/*! ./history.page.scss */ "./src/app/pages/history/history.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpClient"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
        src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_7__["ProfileService"],
        src_app_services_language_service__WEBPACK_IMPORTED_MODULE_3__["LanguageService"],
        src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_6__["SettingsService"],
        src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_4__["PopUpService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
        src_app_services_event_service__WEBPACK_IMPORTED_MODULE_5__["EventService"]])
], HistoryPage);



/***/ })

}]);
//# sourceMappingURL=pages-history-history-module-es2015.js.map