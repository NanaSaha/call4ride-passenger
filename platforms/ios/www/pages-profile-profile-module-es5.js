(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-profile-profile-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/profile/profile.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/profile/profile.page.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <ion-content class=\"background_whole2\" padding>\n  <ion-header no-border>\n    <ion-button size=\"large\" (click)=\"goBack()\">\n      <ion-icon name=\"arrow-back\"></ion-icon>\n    </ion-button>\n  </ion-header>\n\n  <div class=\"centerDiv\" *ngIf=\"ph.userID.picture\" (click)=\"choosePic()\">\n    <ion-avatar style=\"text-align: center; display: inline-flex !important\">\n      <img class=\"profile-pic\" [src]=\"ph.userID.picture\" />\n    </ion-avatar>\n  </div>\n\n  <div class=\"centerDiv\" *ngIf=\"!ph.userID.picture\" (click)=\"choosePic()\">\n    <ion-avatar style=\"text-align: center; display: inline-flex !important\">\n      <img class=\"profile-pic\" src=\"assets/svgs/user-edit-solid.svg\" />\n    </ion-avatar>\n  </div>\n  <div *ngIf=\"first_name\">\n    <h1 style=\"text-align: center\">{{first_name}} {{last_name}}</h1>\n  </div>\n  <div *ngIf=\"!first_name\">\n    <h1 style=\"text-align: center\">Call4Ride RIDER</h1>\n  </div>\n\n  <ion-item (click)=\"updateEmail()\">\n    <ion-icon name=\"ios-mail\" style=\"padding-right: 20px\"></ion-icon>\n    <h4 style=\"text-align: center\" *ngIf=\"!ph.userID.email\">test@email.com</h4>\n    <h4 style=\"text-align: center\" *ngIf=\"ph.userID.email\">\n      {{ph.userID.email}}\n    </h4>\n\n    <ion-icon name=\"arrow-dropright\" slot=\"end\"></ion-icon>\n  </ion-item>\n\n  <ion-item (click)=\"updateName()\">\n    <ion-icon name=\"ios-contact\" style=\"padding-right: 20px\"></ion-icon>\n    <h4 style=\"text-align: center\" *ngIf=\"!ph.userID?.first_name\">\n      Call4Ride Rider\n    </h4>\n    <h4 style=\"text-align: center\" *ngIf=\"ph.userID?.first_name\">\n      {{ph.userID.first_name}}\n    </h4>\n    <ion-icon name=\"arrow-dropright\" slot=\"end\"></ion-icon>\n  </ion-item>\n\n  <ion-item (click)=\"updateNumber()\">\n    <ion-icon name=\"ios-call\" style=\"padding-right: 20px\"></ion-icon>\n    <h4 style=\"text-align: center\" *ngIf=\"phone\">{{phone}}</h4>\n    <h4 style=\"text-align: center\" *ngIf=\"!phone\">027XXXXXXX</h4>\n    <ion-icon name=\"arrow-dropright\" slot=\"end\"></ion-icon>\n  </ion-item>\n\n  <ion-item (click)=\"updateEmergencyNumber()\" *ngIf=\"emergencyNumber\">\n    <ion-icon name=\"ios-call\" style=\"padding-right: 20px\"></ion-icon>\n    <h4 style=\"text-align: center\" *ngIf=\"emergencyNumber\">\n      Emergency:: <span style=\"color: red\"> {{emergencyNumber}}</span>\n    </h4>\n\n    <ion-icon name=\"arrow-dropright\" slot=\"end\"></ion-icon>\n  </ion-item>\n\n  <ion-item *ngIf=\"!emergencyNumber\">\n    <h4 style=\"text-align: center\" *ngIf=\"!emergencyNumber\">\n      <ion-button (click)=\"updateEmergencyNumber()\">\n        Add Emergency Number<ion-icon name=\"add\" slot=\"end\"></ion-icon\n      ></ion-button>\n    </h4>\n  </ion-item>\n\n  <div class=\"centerDiv2\">\n    <ion-button (click)=\"logOut()\">\n      Sign Out <ion-icon name=\"power\" slot=\"end\"></ion-icon\n    ></ion-button>\n  </div>\n</ion-content> -->\n\n<ion-content class=\"background_whole\" padding>\n  <ion-header no-border>\n    <ion-toolbar>\n      <ion-button size=\"large\" (click)=\"goBack()\">\n        <ion-icon name=\"arrow-back\"></ion-icon>\n        <ion-title>PROFILE</ion-title>\n      </ion-button>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-grid\n    style=\"background-color: beige; border-radius: 12px; margin-top: 10%\"\n  >\n    <ion-row>\n      <ion-col size=\"8\">\n        <div class=\"content-wrap\" *ngIf=\"first_name\">\n          <span class=\"bookPrice\">{{first_name}} {{last_name}}</span>\n          <br />\n          <span class=\"bookTitle\" style=\"color: red\">{{unique}}</span>\n        </div>\n        <div class=\"content-wrap\" *ngIf=\"!first_name\">\n          <span class=\"bookPrice\">Call4Ride RIDER</span>\n        </div>\n      </ion-col>\n\n      <ion-col size=\"4\">\n        <div class=\"content-wrap2\" *ngIf=\"picture\">\n          <ion-avatar class=\"avtr\">\n            <img class=\"profile-pic\" [src]=\"picture\" />\n          </ion-avatar>\n        </div>\n\n        <div class=\"content-wrap2\" *ngIf=\"!picture\">\n          <ion-avatar class=\"avtr\">\n            <img class=\"profile-pic\" src=\"assets/svgs/user-edit-solid.svg\" />\n          </ion-avatar>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <ion-item (click)=\"updateEmail()\">\n    <ion-icon name=\"ios-mail\" style=\"padding-right: 20px\"></ion-icon>\n    <h4 style=\"text-align: center\" *ngIf=\"!email\">Update Your Email</h4>\n    <h4 style=\"text-align: center\" *ngIf=\"email\">{{email}}</h4>\n  </ion-item>\n\n  <ion-item>\n    <ion-icon name=\"ios-call\" style=\"padding-right: 20px\"></ion-icon>\n    <h4 style=\"text-align: center\" *ngIf=\"phoneNumber\">{{phoneNumber}}</h4>\n    <h4 style=\"text-align: center\" *ngIf=\"!phoneNumber\">\n      Update Your Phone number\n    </h4>\n  </ion-item>\n\n  <ion-item (click)=\"updateEmergencyNumber()\" *ngIf=\"emergencyNumber\">\n    <ion-icon name=\"ios-call\" style=\"padding-right: 20px\"></ion-icon>\n    <h4 style=\"text-align: center\" *ngIf=\"emergencyNumber\">\n      Emergency:: <span style=\"color: red\"> {{emergencyNumber}}</span>\n    </h4>\n  </ion-item>\n\n  <div *ngIf=\"!emergencyNumber\" class=\"centerDiv\">\n    <h4 style=\"text-align: center\" *ngIf=\"!emergencyNumber\">\n      <ion-button (click)=\"updateEmergencyNumber()\" color=\"danger\">\n        Add Emergency Number<ion-icon name=\"add\" slot=\"end\"></ion-icon\n      ></ion-button>\n    </h4>\n  </div>\n\n  <div class=\"centerDiv2\">\n    <ion-button (click)=\"logOut()\">\n      Sign Out <ion-icon name=\"power\" slot=\"end\"></ion-icon\n    ></ion-button>\n  </div>\n</ion-content>\n\n<ion-footer>\n  <div class=\"centerDiv2\">\n    <ion-button (click)=\"deleteAccount()\" color=\"dark\">\n      Delete Account <ion-icon name=\"trash\" slot=\"end\"></ion-icon></ion-button>\n  </div>\n\n  <div class=\"centerDiv2\">\n    <ion-button (click)=\"clearCache()\" color=\"danger\">\n      Clear Cache <ion-icon name=\"trash\" slot=\"end\"></ion-icon></ion-button>\n  </div>\n</ion-footer>\n"

/***/ }),

/***/ "./src/app/pages/profile/profile.module.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/profile/profile.module.ts ***!
  \*************************************************/
/*! exports provided: ProfilePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePageModule", function() { return ProfilePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _profile_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./profile.page */ "./src/app/pages/profile/profile.page.ts");







var routes = [
    {
        path: '',
        component: _profile_page__WEBPACK_IMPORTED_MODULE_6__["ProfilePage"]
    }
];
var ProfilePageModule = /** @class */ (function () {
    function ProfilePageModule() {
    }
    ProfilePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_profile_page__WEBPACK_IMPORTED_MODULE_6__["ProfilePage"]]
        })
    ], ProfilePageModule);
    return ProfilePageModule;
}());



/***/ }),

/***/ "./src/app/pages/profile/profile.page.scss":
/*!*************************************************!*\
  !*** ./src/app/pages/profile/profile.page.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-item ion-label {\n  margin: 0 !important;\n}\n\nion-list {\n  padding: 0;\n  border-radius: 12px;\n}\n\nion-list-header {\n  background-color: #ececec;\n  margin: 0;\n  padding: 0;\n}\n\n.centerDiv {\n  position: relative;\n  text-align: center;\n}\n\n.centerDiv2 {\n  position: relative;\n  text-align: center;\n}\n\n.background_whole {\n  z-index: 0 !important;\n}\n\n.bookPrice {\n  text-align: center;\n  font-size: 35px;\n  font-weight: 800;\n}\n\n.bookTitle {\n  text-align: center;\n}\n\n.content-wrap,\n.img-wrapper {\n  display: inline-block;\n  margin-left: 40px;\n  margin-top: 10px;\n}\n\n.content-wrap2 {\n  display: inline-block;\n  margin-left: 0px;\n  width: 90px;\n  height: 90px;\n}\n\n.avtr {\n  width: 90px;\n  height: 90px;\n  text-align: center;\n  display: -webkit-inline-box !important;\n  display: inline-flex !important;\n}\n\nion-datetime {\n  padding-left: 3px !important;\n}\n\n.item-inner {\n  border: none !important;\n  padding: 0;\n}\n\n.popover-content {\n  min-height: 0 !important;\n  max-height: 88px !important;\n}\n\n.profile-popover {\n  margin-top: -1px !important;\n}\n\n.placeholder-profile {\n  color: #cccccc;\n}\n\n.dob-label {\n  color: #000000 !important;\n  padding: 10px !important;\n  max-width: 50% !important;\n}\n\n.bar_ridetype {\n  height: auto;\n  width: 91%;\n  background: white;\n  position: absolute;\n  margin-left: 0%;\n  padding: 0px;\n  top: 40%;\n  z-index: 1;\n  border: 1px solid #090a0a;\n  -webkit-animation-direction: alternate;\n          animation-direction: alternate;\n  -webkit-animation-name: wiki;\n          animation-name: wiki;\n  -webkit-animation-duration: 0.8s;\n          animation-duration: 0.8s;\n  -webkit-animation-iteration-count: 1;\n          animation-iteration-count: 1;\n}\n\n.bar_ridetype .btn_cancel ion-icon {\n  font-size: 2em;\n  top: 10px;\n}\n\n.bar_ridetype ion-button {\n  border: 1.3px solid black;\n  height: auto;\n  width: auto;\n  text-align: center;\n  border-radius: 12px;\n}\n\n.bar_ridetype .gutton {\n  bottom: -7.5px;\n  border-radius: 12px;\n  width: 96%;\n  margin-left: 2%;\n}\n\n.bar_ridetype .text {\n  padding-bottom: 0px;\n  font-size: 1.4em;\n}\n\n.bar_ridetype .text ion-spinner {\n  top: 10px;\n}\n\n.top_bar {\n  height: 200px;\n  border-radius: 12px;\n  width: 100%;\n  box-shadow: none;\n  padding: 0px;\n  background: black;\n  color: #fdfdfd;\n  font-weight: bold;\n  font-style: bold;\n  font-size: 1.3em;\n  border-top: 0px;\n}\n\n.top_bar ion-icon {\n  color: white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYWhhL0lvbmljQXBwcy9DNFJpZGUtSU9TLTIwMjQvY2FsbDRyaWRlLXBhc3Nlbmdlci9zcmMvYXBwL3BhZ2VzL3Byb2ZpbGUvcHJvZmlsZS5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL3Byb2ZpbGUvcHJvZmlsZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxvQkFBQTtBQ0NGOztBREVBO0VBQ0UsVUFBQTtFQUNBLG1CQUFBO0FDQ0Y7O0FERUE7RUFDRSx5QkFBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0FDQ0Y7O0FERUE7RUFDRSxrQkFBQTtFQUNBLGtCQUFBO0FDQ0Y7O0FERUE7RUFDRSxrQkFBQTtFQUNBLGtCQUFBO0FDQ0Y7O0FER0E7RUFDRSxxQkFBQTtBQ0FGOztBRElBO0VBQ0Usa0JBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUNERjs7QURJQTtFQUNFLGtCQUFBO0FDREY7O0FESUE7O0VBRUUscUJBQUE7RUFFQSxpQkFBQTtFQUNBLGdCQUFBO0FDRkY7O0FES0E7RUFDRSxxQkFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUNGRjs7QURLQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxzQ0FBQTtFQUFBLCtCQUFBO0FDRkY7O0FENkJBO0VBQ0UsNEJBQUE7QUMxQkY7O0FENkJBO0VBQ0UsdUJBQUE7RUFDQSxVQUFBO0FDMUJGOztBRDZCQTtFQUNFLHdCQUFBO0VBQ0EsMkJBQUE7QUMxQkY7O0FENkJBO0VBQ0UsMkJBQUE7QUMxQkY7O0FENkJBO0VBQ0UsY0FBQTtBQzFCRjs7QUQ2QkE7RUFDRSx5QkFBQTtFQUNBLHdCQUFBO0VBQ0EseUJBQUE7QUMxQkY7O0FENkJBO0VBQ0UsWUFBQTtFQUNBLFVBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7RUFDQSxRQUFBO0VBQ0EsVUFBQTtFQUNBLHlCQUFBO0VBQ0Esc0NBQUE7VUFBQSw4QkFBQTtFQUNBLDRCQUFBO1VBQUEsb0JBQUE7RUFDQSxnQ0FBQTtVQUFBLHdCQUFBO0VBQ0Esb0NBQUE7VUFBQSw0QkFBQTtBQzFCRjs7QUQ2Qkk7RUFDRSxjQUFBO0VBQ0EsU0FBQTtBQzNCTjs7QUQrQkU7RUFDRSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFFQSxtQkFBQTtBQzlCSjs7QURpQ0U7RUFDRSxjQUFBO0VBQ0EsbUJBQUE7RUFDQSxVQUFBO0VBQ0EsZUFBQTtBQy9CSjs7QURrQ0U7RUFDRSxtQkFBQTtFQUNBLGdCQUFBO0FDaENKOztBRGtDSTtFQUNFLFNBQUE7QUNoQ047O0FEb0NBO0VBQ0UsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7QUNqQ0Y7O0FEbUNFO0VBQ0UsWUFBQTtBQ2pDSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3Byb2ZpbGUvcHJvZmlsZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24taXRlbSBpb24tbGFiZWwge1xyXG4gIG1hcmdpbjogMCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5pb24tbGlzdCB7XHJcbiAgcGFkZGluZzogMDtcclxuICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG59XHJcblxyXG5pb24tbGlzdC1oZWFkZXIge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNlY2VjZWM7XHJcbiAgbWFyZ2luOiAwO1xyXG4gIHBhZGRpbmc6IDA7XHJcbn1cclxuXHJcbi5jZW50ZXJEaXYge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5jZW50ZXJEaXYyIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIC8vIG1hcmdpbi10b3A6IDIwJTtcclxufVxyXG5cclxuLmJhY2tncm91bmRfd2hvbGUge1xyXG4gIHotaW5kZXg6IDAgIWltcG9ydGFudDtcclxuICAvLyAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgzNjBkZWcsICNmZmYgNTAlLCAjZmJiOTFkIDUwJSkgIWltcG9ydGFudDtcclxufVxyXG5cclxuLmJvb2tQcmljZSB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGZvbnQtc2l6ZTogMzVweDtcclxuICBmb250LXdlaWdodDogODAwO1xyXG59XHJcblxyXG4uYm9va1RpdGxlIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5jb250ZW50LXdyYXAsXHJcbi5pbWctd3JhcHBlciB7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cclxuICBtYXJnaW4tbGVmdDogNDBweDtcclxuICBtYXJnaW4tdG9wOiAxMHB4O1xyXG59XHJcblxyXG4uY29udGVudC13cmFwMiB7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIG1hcmdpbi1sZWZ0OiAwcHg7XHJcbiAgd2lkdGg6IDkwcHg7XHJcbiAgaGVpZ2h0OiA5MHB4O1xyXG59XHJcblxyXG4uYXZ0ciB7XHJcbiAgd2lkdGg6IDkwcHg7XHJcbiAgaGVpZ2h0OiA5MHB4O1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBkaXNwbGF5OiBpbmxpbmUtZmxleCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uY29udCB7XHJcbiAgLy8gaW9uLWl0ZW0ge1xyXG4gIC8vICAgYm9yZGVyLXJhZGl1czogMTJweDtcclxuICAvLyAgIGJveC1zaGFkb3c6IDBweCAxcHggMHB4IDAuNXB4IHJnYmEoMywgMywgMywgMC4yNTMpO1xyXG4gIC8vICAgaGVpZ2h0OiA1MHB4O1xyXG4gIC8vICAgbWFyZ2luLXRvcDogMyU7XHJcbiAgLy8gICBjb2xvcjogcmdiKDMsIDMsIDMpO1xyXG4gIC8vICAgYW5pbWF0aW9uLWRpcmVjdGlvbjogYWx0ZXJuYXRlO1xyXG4gIC8vICAgYW5pbWF0aW9uLW5hbWU6IHdpa2k7XHJcbiAgLy8gICBhbmltYXRpb24tZHVyYXRpb246IDAuM3M7XHJcbiAgLy8gICBhbmltYXRpb24taXRlcmF0aW9uLWNvdW50OiAxO1xyXG4gIC8vIH1cclxufVxyXG5cclxuLnByb2ZpbGUtcGljIHtcclxuICAvLyB3aWR0aDogNmVtICFpbXBvcnRhbnQ7XHJcbiAgLy8gaGVpZ2h0OiA2ZW0gIWltcG9ydGFudDtcclxufVxyXG5cclxuLy8gaW9uLWl0ZW0ge1xyXG4vLyAgIHBhZGRpbmc6IDAgIWltcG9ydGFudDtcclxuLy8gICBib3JkZXI6IDFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNik7XHJcbi8vIH1cclxuXHJcbmlvbi1kYXRldGltZSB7XHJcbiAgcGFkZGluZy1sZWZ0OiAzcHggIWltcG9ydGFudDtcclxufVxyXG5cclxuLml0ZW0taW5uZXIge1xyXG4gIGJvcmRlcjogbm9uZSAhaW1wb3J0YW50O1xyXG4gIHBhZGRpbmc6IDA7XHJcbn1cclxuXHJcbi5wb3BvdmVyLWNvbnRlbnQge1xyXG4gIG1pbi1oZWlnaHQ6IDAgIWltcG9ydGFudDtcclxuICBtYXgtaGVpZ2h0OiA4OHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5wcm9maWxlLXBvcG92ZXIge1xyXG4gIG1hcmdpbi10b3A6IC0xcHggIWltcG9ydGFudDtcclxufVxyXG5cclxuLnBsYWNlaG9sZGVyLXByb2ZpbGUge1xyXG4gIGNvbG9yOiAjY2NjY2NjO1xyXG59XHJcblxyXG4uZG9iLWxhYmVsIHtcclxuICBjb2xvcjogIzAwMDAwMCAhaW1wb3J0YW50O1xyXG4gIHBhZGRpbmc6IDEwcHggIWltcG9ydGFudDtcclxuICBtYXgtd2lkdGg6IDUwJSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uYmFyX3JpZGV0eXBlIHtcclxuICBoZWlnaHQ6IGF1dG87XHJcbiAgd2lkdGg6IDkxJTtcclxuICBiYWNrZ3JvdW5kOiByZ2IoMjU1LCAyNTUsIDI1NSk7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIG1hcmdpbi1sZWZ0OiAwJTtcclxuICBwYWRkaW5nOiAwcHg7XHJcbiAgdG9wOiA0MCU7XHJcbiAgei1pbmRleDogMTtcclxuICBib3JkZXI6IDFweCBzb2xpZCByZ2IoOSwgMTAsIDEwKTtcclxuICBhbmltYXRpb24tZGlyZWN0aW9uOiBhbHRlcm5hdGU7XHJcbiAgYW5pbWF0aW9uLW5hbWU6IHdpa2k7XHJcbiAgYW5pbWF0aW9uLWR1cmF0aW9uOiAwLjhzO1xyXG4gIGFuaW1hdGlvbi1pdGVyYXRpb24tY291bnQ6IDE7XHJcblxyXG4gIC5idG5fY2FuY2VsIHtcclxuICAgIGlvbi1pY29uIHtcclxuICAgICAgZm9udC1zaXplOiAyZW07XHJcbiAgICAgIHRvcDogMTBweDtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGlvbi1idXR0b24ge1xyXG4gICAgYm9yZGVyOiAxLjNweCBzb2xpZCByZ2IoMCwgMCwgMCk7XHJcbiAgICBoZWlnaHQ6IGF1dG87XHJcbiAgICB3aWR0aDogYXV0bztcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHJcbiAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG4gIH1cclxuXHJcbiAgLmd1dHRvbiB7XHJcbiAgICBib3R0b206IC03LjVweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDEycHg7XHJcbiAgICB3aWR0aDogOTYlO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDIlO1xyXG4gIH1cclxuXHJcbiAgLnRleHQge1xyXG4gICAgcGFkZGluZy1ib3R0b206IDBweDtcclxuICAgIGZvbnQtc2l6ZTogMS40ZW07XHJcblxyXG4gICAgaW9uLXNwaW5uZXIge1xyXG4gICAgICB0b3A6IDEwcHg7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbi50b3BfYmFyIHtcclxuICBoZWlnaHQ6IDIwMHB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDEycHg7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgYm94LXNoYWRvdzogbm9uZTtcclxuICBwYWRkaW5nOiAwcHg7XHJcbiAgYmFja2dyb3VuZDogcmdiKDAsIDAsIDApO1xyXG4gIGNvbG9yOiAjZmRmZGZkO1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gIGZvbnQtc3R5bGU6IGJvbGQ7XHJcbiAgZm9udC1zaXplOiAxLjNlbTtcclxuICBib3JkZXItdG9wOiAwcHg7XHJcblxyXG4gIGlvbi1pY29uIHtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICB9XHJcbn1cclxuIiwiaW9uLWl0ZW0gaW9uLWxhYmVsIHtcbiAgbWFyZ2luOiAwICFpbXBvcnRhbnQ7XG59XG5cbmlvbi1saXN0IHtcbiAgcGFkZGluZzogMDtcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcbn1cblxuaW9uLWxpc3QtaGVhZGVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2VjZWNlYztcbiAgbWFyZ2luOiAwO1xuICBwYWRkaW5nOiAwO1xufVxuXG4uY2VudGVyRGl2IHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5jZW50ZXJEaXYyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5iYWNrZ3JvdW5kX3dob2xlIHtcbiAgei1pbmRleDogMCAhaW1wb3J0YW50O1xufVxuXG4uYm9va1ByaWNlIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDM1cHg7XG4gIGZvbnQtd2VpZ2h0OiA4MDA7XG59XG5cbi5ib29rVGl0bGUge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5jb250ZW50LXdyYXAsXG4uaW1nLXdyYXBwZXIge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIG1hcmdpbi1sZWZ0OiA0MHB4O1xuICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuXG4uY29udGVudC13cmFwMiB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgbWFyZ2luLWxlZnQ6IDBweDtcbiAgd2lkdGg6IDkwcHg7XG4gIGhlaWdodDogOTBweDtcbn1cblxuLmF2dHIge1xuICB3aWR0aDogOTBweDtcbiAgaGVpZ2h0OiA5MHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGRpc3BsYXk6IGlubGluZS1mbGV4ICFpbXBvcnRhbnQ7XG59XG5cbmlvbi1kYXRldGltZSB7XG4gIHBhZGRpbmctbGVmdDogM3B4ICFpbXBvcnRhbnQ7XG59XG5cbi5pdGVtLWlubmVyIHtcbiAgYm9yZGVyOiBub25lICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmc6IDA7XG59XG5cbi5wb3BvdmVyLWNvbnRlbnQge1xuICBtaW4taGVpZ2h0OiAwICFpbXBvcnRhbnQ7XG4gIG1heC1oZWlnaHQ6IDg4cHggIWltcG9ydGFudDtcbn1cblxuLnByb2ZpbGUtcG9wb3ZlciB7XG4gIG1hcmdpbi10b3A6IC0xcHggIWltcG9ydGFudDtcbn1cblxuLnBsYWNlaG9sZGVyLXByb2ZpbGUge1xuICBjb2xvcjogI2NjY2NjYztcbn1cblxuLmRvYi1sYWJlbCB7XG4gIGNvbG9yOiAjMDAwMDAwICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmc6IDEwcHggIWltcG9ydGFudDtcbiAgbWF4LXdpZHRoOiA1MCUgIWltcG9ydGFudDtcbn1cblxuLmJhcl9yaWRldHlwZSB7XG4gIGhlaWdodDogYXV0bztcbiAgd2lkdGg6IDkxJTtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbWFyZ2luLWxlZnQ6IDAlO1xuICBwYWRkaW5nOiAwcHg7XG4gIHRvcDogNDAlO1xuICB6LWluZGV4OiAxO1xuICBib3JkZXI6IDFweCBzb2xpZCAjMDkwYTBhO1xuICBhbmltYXRpb24tZGlyZWN0aW9uOiBhbHRlcm5hdGU7XG4gIGFuaW1hdGlvbi1uYW1lOiB3aWtpO1xuICBhbmltYXRpb24tZHVyYXRpb246IDAuOHM7XG4gIGFuaW1hdGlvbi1pdGVyYXRpb24tY291bnQ6IDE7XG59XG4uYmFyX3JpZGV0eXBlIC5idG5fY2FuY2VsIGlvbi1pY29uIHtcbiAgZm9udC1zaXplOiAyZW07XG4gIHRvcDogMTBweDtcbn1cbi5iYXJfcmlkZXR5cGUgaW9uLWJ1dHRvbiB7XG4gIGJvcmRlcjogMS4zcHggc29saWQgYmxhY2s7XG4gIGhlaWdodDogYXV0bztcbiAgd2lkdGg6IGF1dG87XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcbn1cbi5iYXJfcmlkZXR5cGUgLmd1dHRvbiB7XG4gIGJvdHRvbTogLTcuNXB4O1xuICBib3JkZXItcmFkaXVzOiAxMnB4O1xuICB3aWR0aDogOTYlO1xuICBtYXJnaW4tbGVmdDogMiU7XG59XG4uYmFyX3JpZGV0eXBlIC50ZXh0IHtcbiAgcGFkZGluZy1ib3R0b206IDBweDtcbiAgZm9udC1zaXplOiAxLjRlbTtcbn1cbi5iYXJfcmlkZXR5cGUgLnRleHQgaW9uLXNwaW5uZXIge1xuICB0b3A6IDEwcHg7XG59XG5cbi50b3BfYmFyIHtcbiAgaGVpZ2h0OiAyMDBweDtcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcbiAgd2lkdGg6IDEwMCU7XG4gIGJveC1zaGFkb3c6IG5vbmU7XG4gIHBhZGRpbmc6IDBweDtcbiAgYmFja2dyb3VuZDogYmxhY2s7XG4gIGNvbG9yOiAjZmRmZGZkO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zdHlsZTogYm9sZDtcbiAgZm9udC1zaXplOiAxLjNlbTtcbiAgYm9yZGVyLXRvcDogMHB4O1xufVxuLnRvcF9iYXIgaW9uLWljb24ge1xuICBjb2xvcjogd2hpdGU7XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/profile/profile.page.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/profile/profile.page.ts ***!
  \***********************************************/
/*! exports provided: ProfilePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePage", function() { return ProfilePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.cjs.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/profile.service */ "./src/app/services/profile.service.ts");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/pop-up.service */ "./src/app/services/pop-up.service.ts");
/* harmony import */ var src_app_services_language_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/language.service */ "./src/app/services/language.service.ts");
/* harmony import */ var _autocomplete_autocomplete_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../autocomplete/autocomplete.page */ "./src/app/pages/autocomplete/autocomplete.page.ts");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");













var ProfilePage = /** @class */ (function () {
    function ProfilePage(navCtrl, lp, actionSheetCtrl, modalCtrl, pop, camera, alertCtrl, ph, authProvider, location, router, storage) {
        this.navCtrl = navCtrl;
        this.lp = lp;
        this.actionSheetCtrl = actionSheetCtrl;
        this.modalCtrl = modalCtrl;
        this.pop = pop;
        this.camera = camera;
        this.alertCtrl = alertCtrl;
        this.ph = ph;
        this.authProvider = authProvider;
        this.location = location;
        this.router = router;
        this.storage = storage;
        ph.isHome = false;
    }
    ProfilePage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.ph.getUserProfile().on("value", function (userProfileSnapshot) {
            console.log("userProfileSnapshot::", userProfileSnapshot.val());
            _this.userProfile = userProfileSnapshot.val();
            _this.first_name = userProfileSnapshot.val().first_name;
            _this.last_name = userProfileSnapshot.val().last_name;
            _this.email = userProfileSnapshot.val().email;
            _this.emergencyNumber = userProfileSnapshot.val().emergencyNumber;
            _this.phoneNumber = userProfileSnapshot.val().phonenumber;
            _this.unique = userProfileSnapshot.val().unique_number;
            _this.picture = userProfileSnapshot.val().picture;
            console.log("EMERGENC::", _this.emergencyNumber);
            console.log("PHONE::", _this.phoneNumber);
        });
    };
    ProfilePage.prototype.remove = function () {
        var _this = this;
        this.authProvider.logoutUser().then(function () {
            _this.navCtrl.navigateRoot("login-entrance");
        });
    };
    ProfilePage.prototype.choosePic = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var actionSheet;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.actionSheetCtrl.create({
                            header: this.lp.translate()[0].choosefrom,
                            buttons: [
                                {
                                    text: this.lp.translate()[0].camera,
                                    icon: "ios-camera",
                                    handler: function () {
                                        _this.changePic();
                                    },
                                },
                                {
                                    text: this.lp.translate()[0].file,
                                    icon: "ios-folder",
                                    handler: function () {
                                        _this.changePicFromFile();
                                    },
                                },
                                {
                                    text: this.lp.translate()[0].cancel,
                                    icon: "close",
                                    role: "cancel",
                                    handler: function () {
                                        console.log("Cancel clicked");
                                    },
                                },
                            ],
                        })];
                    case 1:
                        actionSheet = _a.sent();
                        actionSheet.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    ProfilePage.prototype.changePic = function () {
        var _this = this;
        var cameraOptions = {
            quality: 10,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
        };
        this.camera.getPicture(cameraOptions).then(function (imageData) {
            _this.captureDataUrl = "data:image/jpeg;base64," + imageData;
            _this.processProfilePicture(_this.captureDataUrl);
        });
    };
    ProfilePage.prototype.changePicFromFile = function () {
        var _this = this;
        var cameraOptions = {
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            destinationType: this.camera.DestinationType.DATA_URL,
            quality: 10,
            encodingType: this.camera.EncodingType.PNG,
        };
        this.camera.getPicture(cameraOptions).then(function (imageData) {
            _this.captureDataUrl = "data:image/jpeg;base64," + imageData;
            _this.processProfilePicture(_this.captureDataUrl);
        });
    };
    ProfilePage.prototype.processProfilePicture = function (captureData) {
        var _this = this;
        var storageRef = firebase_app__WEBPACK_IMPORTED_MODULE_2___default.a.storage().ref();
        // Create a timestamp as filename
        var filename = Math.floor(Date.now() / 1000);
        this.pop.presentLoader("Processing image..");
        // Create a reference to 'images/todays-date.jpg'
        var imageRef = storageRef.child("userPictures/" + filename + ".jpg");
        imageRef
            .putString(captureData, firebase_app__WEBPACK_IMPORTED_MODULE_2___default.a.storage.StringFormat.DATA_URL)
            .then(function (snapshot) {
            imageRef
                .getDownloadURL()
                .then(function (url) {
                _this.ph
                    .UpdatePhoto(url)
                    .then(function (success) {
                    _this.pop.hideLoader();
                    _this.pop.presentToast(_this.lp.translate()[0].pictureset);
                })
                    .catch(function (error) {
                    alert(error);
                });
            })
                .catch(function (error) {
                alert(error);
            });
        })
            .catch(function (error) {
            alert(error);
        });
    };
    ProfilePage.prototype.updateNumber = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            message: this.lp.translate()[0].phone,
                            inputs: [
                                {
                                    value: this.phone,
                                },
                            ],
                            buttons: [
                                {
                                    text: this.lp.translate()[0].cancel,
                                },
                                {
                                    text: this.lp.translate()[0].accept,
                                    handler: function (data) {
                                        console.log(data);
                                        _this.ph.UpdateNumbers(data);
                                    },
                                },
                            ],
                        })];
                    case 1:
                        alert = _a.sent();
                        alert.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    ProfilePage.prototype.updateEmergencyNumber = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            message: "Enter an emergency number",
                            inputs: [
                                {
                                    value: this.emergencyNumber,
                                },
                            ],
                            buttons: [
                                {
                                    text: this.lp.translate()[0].cancel,
                                },
                                {
                                    text: this.lp.translate()[0].accept,
                                    handler: function (data) {
                                        console.log("EMERGENCY NUMBER DATA" + data);
                                        _this.ph.UpdateEmergencyNumbers(data);
                                    },
                                },
                            ],
                        })];
                    case 1:
                        alert = _a.sent();
                        alert.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    ProfilePage.prototype.updateName = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            message: this.lp.translate()[0].name,
                            inputs: [
                                {
                                    value: this.userProfile.updateName,
                                },
                            ],
                            buttons: [
                                {
                                    text: this.lp.translate()[0].cancel,
                                },
                                {
                                    text: this.lp.translate()[0].accept,
                                    handler: function (data) {
                                        console.log(data);
                                        _this.ph.updateName(data);
                                    },
                                },
                            ],
                        })];
                    case 1:
                        alert = _a.sent();
                        alert.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    ProfilePage.prototype.updateEmail = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            message: this.lp.translate()[0].email,
                            inputs: [
                                {
                                    value: this.ph.userID.email,
                                },
                            ],
                            buttons: [
                                {
                                    text: this.lp.translate()[0].cancel,
                                },
                                {
                                    text: this.lp.translate()[0].accept,
                                    handler: function (data) {
                                        console.log(data);
                                        _this.ph.updateEmail(data);
                                    },
                                },
                            ],
                        })];
                    case 1:
                        alert = _a.sent();
                        alert.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    ProfilePage.prototype.updateHome = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalCtrl.create({
                            component: _autocomplete_autocomplete_page__WEBPACK_IMPORTED_MODULE_8__["AutocompletePage"],
                            componentProps: { item: this.items },
                        })];
                    case 1:
                        modal = _a.sent();
                        modal.onDidDismiss().then(function (data) {
                            var item = data["data"];
                            _this.home = item;
                            if (data != null) {
                                _this.ph.UpdateHome(item);
                                _this.ph.home = true;
                            }
                        });
                        modal.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    ProfilePage.prototype.updateWork = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalCtrl.create({
                            component: _autocomplete_autocomplete_page__WEBPACK_IMPORTED_MODULE_8__["AutocompletePage"],
                            componentProps: { item: this.items },
                        })];
                    case 1:
                        modal = _a.sent();
                        modal.onDidDismiss().then(function (data) {
                            var item = data["data"];
                            _this.home = item;
                            if (data != null) {
                                _this.ph.UpdateWork(item);
                                _this.ph.work = true;
                            }
                        });
                        modal.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    ProfilePage.prototype.logOut = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            message: this.lp.translate()[0].error,
                            buttons: [
                                {
                                    text: this.lp.translate()[0].cancel,
                                },
                                {
                                    text: this.lp.translate()[0].accept,
                                    handler: function () {
                                        _this.navCtrl.navigateRoot("login");
                                        _this.authProvider.logoutUser;
                                        _this.authProvider.signOut();
                                    },
                                },
                            ],
                        })];
                    case 1:
                        alert = _a.sent();
                        alert.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    ProfilePage.prototype.goBack = function () {
        this.navCtrl.navigateRoot("home");
    };
    ProfilePage.prototype.deleteAccount = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            message: "Do you really want to delete your account ? ",
                            buttons: [
                                {
                                    text: "No",
                                },
                                {
                                    text: "Yes",
                                    handler: function (data) {
                                        console.log("EMERGENCY NUMBER DATA" + data);
                                        // this.authProvider.signOut();
                                        // this.navCtrl.navigateRoot("login");
                                        _this.router.navigateByUrl('/login');
                                        _this.deleteAlert();
                                    },
                                },
                            ],
                        })];
                    case 1:
                        alert = _a.sent();
                        alert.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    ProfilePage.prototype.deleteAlert = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            message: "We will delete your account in the next 24 hours. If you want to rescind your decision, send us an email: info@upwheels.com",
                            buttons: [
                                {
                                    text: "Okay",
                                },
                            ],
                        })];
                    case 1:
                        alert = _a.sent();
                        alert.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    ProfilePage.prototype.ngOnInit = function () { };
    ProfilePage.prototype.clearCache = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            message: "Do you really want to clear your cache ? ",
                            buttons: [
                                {
                                    text: "No",
                                },
                                {
                                    text: "Yes",
                                    handler: function (data) {
                                        console.log("EMERGENCY NUMBER DATA" + data);
                                        _this.storage.clear();
                                        _this.authProvider.signOut();
                                        _this.router.navigateByUrl('/login');
                                    },
                                },
                            ],
                        })];
                    case 1:
                        alert = _a.sent();
                        alert.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    ProfilePage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
        { type: src_app_services_language_service__WEBPACK_IMPORTED_MODULE_7__["LanguageService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ActionSheetController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] },
        { type: src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_6__["PopUpService"] },
        { type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_9__["Camera"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
        { type: src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_4__["ProfileService"] },
        { type: src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"] },
        { type: _angular_common__WEBPACK_IMPORTED_MODULE_10__["Location"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_11__["Router"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_12__["Storage"] }
    ]; };
    ProfilePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-profile",
            template: __webpack_require__(/*! raw-loader!./profile.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/profile/profile.page.html"),
            styles: [__webpack_require__(/*! ./profile.page.scss */ "./src/app/pages/profile/profile.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"],
            src_app_services_language_service__WEBPACK_IMPORTED_MODULE_7__["LanguageService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ActionSheetController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"],
            src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_6__["PopUpService"],
            _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_9__["Camera"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
            src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_4__["ProfileService"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"],
            _angular_common__WEBPACK_IMPORTED_MODULE_10__["Location"],
            _angular_router__WEBPACK_IMPORTED_MODULE_11__["Router"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_12__["Storage"]])
    ], ProfilePage);
    return ProfilePage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-profile-profile-module-es5.js.map