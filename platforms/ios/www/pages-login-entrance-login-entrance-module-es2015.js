(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-login-entrance-login-entrance-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/login-entrance/login-entrance.page.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/login-entrance/login-entrance.page.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content class='ion-padding'>\n    <div class='o_section'>\n\n        <ion-grid>\n            <ion-row>\n                <ion-col>\n                    <ion-button color='primary' color='light' fill=\"clear\" expand=\"block\" routerLink=\"/login\">\n                        LOGIN\n                    </ion-button>\n                </ion-col>\n                <ion-col>\n                    <ion-button color='primary' color='light' fill=\"clear\" expand=\"block\" routerLink=\"/signup\">\n                        REGISTER\n                    </ion-button>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n    </div>\n\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/login-entrance/login-entrance.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/login-entrance/login-entrance.module.ts ***!
  \***************************************************************/
/*! exports provided: LoginEntrancePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginEntrancePageModule", function() { return LoginEntrancePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _login_entrance_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login-entrance.page */ "./src/app/pages/login-entrance/login-entrance.page.ts");







const routes = [
    {
        path: '',
        component: _login_entrance_page__WEBPACK_IMPORTED_MODULE_6__["LoginEntrancePage"]
    }
];
let LoginEntrancePageModule = class LoginEntrancePageModule {
};
LoginEntrancePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_login_entrance_page__WEBPACK_IMPORTED_MODULE_6__["LoginEntrancePage"]]
    })
], LoginEntrancePageModule);



/***/ }),

/***/ "./src/app/pages/login-entrance/login-entrance.page.scss":
/*!***************************************************************!*\
  !*** ./src/app/pages/login-entrance/login-entrance.page.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  background-size: cover;\n  background-position: center;\n}\nion-content .o_section {\n  position: fixed;\n  top: 80%;\n  width: 100%;\n  left: 0%;\n}\nion-content ion-button {\n  background: #181817;\n  height: 50px;\n  color: #0c0c0c;\n  -webkit-animation-direction: alternate;\n          animation-direction: alternate;\n  -webkit-animation-name: wiki;\n          animation-name: wiki;\n  -webkit-animation-duration: 0.3s;\n          animation-duration: 0.3s;\n  -webkit-animation-iteration-count: 1;\n          animation-iteration-count: 1;\n  border-radius: 12px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYWhhL0lvbmljQXBwcy9DNFJpZGUtSU9TLTIwMjQvY2FsbDRyaWRlLXBhc3Nlbmdlci9zcmMvYXBwL3BhZ2VzL2xvZ2luLWVudHJhbmNlL2xvZ2luLWVudHJhbmNlLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvbG9naW4tZW50cmFuY2UvbG9naW4tZW50cmFuY2UucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRUUsc0JBQUE7RUFDQSwyQkFBQTtBQ0FGO0FEVUU7RUFDRSxlQUFBO0VBRUEsUUFBQTtFQUNBLFdBQUE7RUFFQSxRQUFBO0FDVko7QURZRTtFQUNFLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7RUFDQSxzQ0FBQTtVQUFBLDhCQUFBO0VBQ0EsNEJBQUE7VUFBQSxvQkFBQTtFQUNBLGdDQUFBO1VBQUEsd0JBQUE7RUFDQSxvQ0FBQTtVQUFBLDRCQUFBO0VBQ0EsbUJBQUE7QUNWSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2xvZ2luLWVudHJhbmNlL2xvZ2luLWVudHJhbmNlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50IHtcclxuICAvLyAtLWJhY2tncm91bmQ6IHVybChcInNyYy9hc3NldHMvaW1nL21lbnUucG5nXCIpIG5vLXJlcGVhdCBjZW50ZXIgLyBjb3ZlcjtcclxuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcclxuICAvLyBAa2V5ZnJhbWVzIGJ1bXAge1xyXG4gIC8vICAgMCUge1xyXG4gIC8vICAgICB0b3A6IDEwMCU7XHJcbiAgLy8gICB9XHJcbiAgLy8gICAvLyA1MCUge3RvcDogOTAlO31cclxuICAvLyAgIDEwMCUge1xyXG4gIC8vICAgICB0b3A6IDgwJTtcclxuICAvLyAgIH1cclxuICAvLyB9XHJcbiAgLm9fc2VjdGlvbiB7XHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICAvLyAgYmFja2dyb3VuZDogY29sb3IoJGNvbG9ycywgcHJpbWFyeSwgYmFzZSk7XHJcbiAgICB0b3A6IDgwJTtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgLy8gICBib3JkZXItcmFkaXVzOiAkYnV0dG9uUmFkaXVzO1xyXG4gICAgbGVmdDogMCU7XHJcbiAgfVxyXG4gIGlvbi1idXR0b24ge1xyXG4gICAgYmFja2dyb3VuZDogIzE4MTgxNztcclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIGNvbG9yOiAjMGMwYzBjO1xyXG4gICAgYW5pbWF0aW9uLWRpcmVjdGlvbjogYWx0ZXJuYXRlO1xyXG4gICAgYW5pbWF0aW9uLW5hbWU6IHdpa2k7XHJcbiAgICBhbmltYXRpb24tZHVyYXRpb246IDAuM3M7XHJcbiAgICBhbmltYXRpb24taXRlcmF0aW9uLWNvdW50OiAxO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTJweDtcclxuICB9XHJcbn1cclxuIiwiaW9uLWNvbnRlbnQge1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG59XG5pb24tY29udGVudCAub19zZWN0aW9uIHtcbiAgcG9zaXRpb246IGZpeGVkO1xuICB0b3A6IDgwJTtcbiAgd2lkdGg6IDEwMCU7XG4gIGxlZnQ6IDAlO1xufVxuaW9uLWNvbnRlbnQgaW9uLWJ1dHRvbiB7XG4gIGJhY2tncm91bmQ6ICMxODE4MTc7XG4gIGhlaWdodDogNTBweDtcbiAgY29sb3I6ICMwYzBjMGM7XG4gIGFuaW1hdGlvbi1kaXJlY3Rpb246IGFsdGVybmF0ZTtcbiAgYW5pbWF0aW9uLW5hbWU6IHdpa2k7XG4gIGFuaW1hdGlvbi1kdXJhdGlvbjogMC4zcztcbiAgYW5pbWF0aW9uLWl0ZXJhdGlvbi1jb3VudDogMTtcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/login-entrance/login-entrance.page.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/login-entrance/login-entrance.page.ts ***!
  \*************************************************************/
/*! exports provided: LoginEntrancePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginEntrancePage", function() { return LoginEntrancePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/settings.service */ "./src/app/services/settings.service.ts");




let LoginEntrancePage = class LoginEntrancePage {
    constructor(navCtrl, menu, settings, load) {
        this.navCtrl = navCtrl;
        this.menu = menu;
        this.settings = settings;
        this.load = load;
        this.menu.enable(false);
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad StartupPage');
    }
    ngOnInit() {
    }
};
LoginEntrancePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"] },
    { type: src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_3__["SettingsService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] }
];
LoginEntrancePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login-entrance',
        template: __webpack_require__(/*! raw-loader!./login-entrance.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/login-entrance/login-entrance.page.html"),
        styles: [__webpack_require__(/*! ./login-entrance.page.scss */ "./src/app/pages/login-entrance/login-entrance.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"], src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_3__["SettingsService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]])
], LoginEntrancePage);



/***/ })

}]);
//# sourceMappingURL=pages-login-entrance-login-entrance-module-es2015.js.map