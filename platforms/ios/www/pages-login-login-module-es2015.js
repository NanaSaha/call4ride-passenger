(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-login-login-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/login/login.page.html":
/*!***********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/login/login.page.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<ion-content>\n  <div class=\"imgBack\">\n    <div id=\"overlay\">\n\n\n    </div>\n\n    <div class=\"backText\">\n      <h3 class=\"smallText\">Welcome<span id=\"verySmall\">Rider</span> </h3>\n      <h1 class=\"BigText\" *ngIf=\"showLogin\">LOGIN<span id=\"verySmall\"></span></h1>\n      <h1 class=\"BigText\" *ngIf=\"showSignup\">SIGNUP<span id=\"verySmall\"></span></h1>\n    </div>\n\n\n    <div overflow-scroll=\"true\" class=\"centerLogin\">\n\n      <br />\n\n      <ion-grid class=\"phoneAuthGrid\" *ngIf=\"showLogin\">\n        <form [formGroup]=\"loginForm\">\n          <ion-row>\n\n            <ion-col size=\"12\">\n              <ion-input clearInput type=\"text\" placeholder=\"Enter Your Email\" formControlName=\"email\"\n                class=\"input ion-padding-horizontal\"></ion-input>\n            </ion-col> <br>\n            <ion-col size=\"12\">\n              <ion-input clearInput type=\"password\" placeholder=\"Password\" formControlName=\"password\"\n                class=\"input ion-padding-horizontal\" clear-input=\"true\"></ion-input>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col (click)=\"onSubmit()\">\n              <ion-button expand=\"block\" class=\"btn-transition\">\n                Login</ion-button>\n            </ion-col>\n          </ion-row>\n          <p class=\"insideGrid\">\n            Dont have an account ? Sign up\n            <span style=\"color: red\" (click)=\"signuphere()\">here</span>\n          </p>\n        </form>\n\n      </ion-grid>\n\n      <ion-grid class=\"phoneAuthGrid\" *ngIf=\"showSignup\">\n        <form [formGroup]=\"signupForm\" (ngSubmit)=\"signupUser()\">\n          <h4 style=\"text-align: center\">\n            SIGN UP\n\n          </h4>\n          <ion-row>\n\n            <ion-col size=\"12\">\n              <ion-input clearInput type=\"text\" placeholder=\"Your Email\" formControlName=\"email\"\n                class=\"input ion-padding-horizontal\"></ion-input>\n            </ion-col> <br>\n            <ion-col size=\"12\">\n              <ion-input clearInput type=\"password\" placeholder=\"Password\" formControlName=\"password\"\n                class=\"input ion-padding-horizontal\" clear-input=\"true\"></ion-input>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col>\n              <ion-button expand=\"block\" (click)=\"signupUser()\" class=\"btn-transition\">\n                Signup</ion-button>\n            </ion-col>\n          </ion-row>\n          <p class=\"insideGrid\">\n            Already have an account? Login\n            <span style=\"color: red\" (click)=\"loginhere()\">here</span>\n          </p>\n        </form>\n\n      </ion-grid>\n\n    </div>\n\n  </div>\n  <!-- <p class=\"centerText\">\n    If you are creating a new account,\n    <span style=\"color: red\">Terms & Conditions</span> and\n    <span style=\"color: red\">Privacy Policy</span>\n    will apply.\n  </p> -->\n</ion-content>\n\n\n<!-- <ion-content scroll=\"true\" overflow-scroll=\"true\">\n  <div class=\"imgBack\">\n    <div id=\"overlay\">\n    \n      <div class=\"backText\">\n        <h3 class=\"smallText\">Welcome</h3>\n        <h1 class=\"BigText\">RIDER<span id=\"verySmall\"></span></h1>\n      </div>\n    </div>\n  </div>\n\n  <div overflow-scroll=\"true\" class=\"centerLogin\">\n    <br />\n  \n    <div id=\"sign-in-button\"></div>\n\n    <ion-grid class=\"phoneAuthGrid\" *ngIf=\"showPhone\">\n      <ion-row>\n        <ion-col size=\"4\">\n          <ion-item class=\"countryCode\">\n            <div><img src=\"/assets/flags/gha.svg\" class=\"flag\" /> +233</div>\n           \n          </ion-item>\n        </ion-col>\n        <ion-col size=\"8\">\n          <ion-input\n            clearInput\n            type=\"number\"\n            placeholder=\"Your Phone Number\"\n            [(ngModel)]=\"PhoneNo\"\n            class=\"input ion-padding-horizontal\"\n            clear-input=\"true\"\n          ></ion-input>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-button\n            expand=\"block\"\n            (click)=\"signinWithPhoneNumber($event)\"\n            class=\"btn-transition\"\n          >\n            Sign In With Phone Number</ion-button\n          >\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n    <div *ngIf=\"showOtpComponent\" class=\"alignCen\">\n      <h2 style=\"text-align: center\">Enter Your OTP Code Here To Login</h2>\n      <ng-otp-input\n        #ngOtpInput\n        (onInputChange)=\"onOtpChange($event)\"\n        [config]=\"config\"\n        style=\"text-align: center\"\n      ></ng-otp-input>\n      <br />\n      <ion-button class=\"btn-transition\" style=\"text-align: center\">\n        Resend Code\n      </ion-button>\n    </div>\n    \n  </div>\n\n\n  <p class=\"centerText\">\n    If you are creating a new account,\n    <span style=\"color: red\">Terms & Conditions</span> and\n    <span style=\"color: red\">Privacy Policy</span>\n    will apply.\n  </p>\n</ion-content>\n -->\n"

/***/ }),

/***/ "./src/app/pages/login/login.module.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.module.ts ***!
  \*********************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login.page */ "./src/app/pages/login/login.page.ts");
/* harmony import */ var ng_otp_input__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ng-otp-input */ "./node_modules/ng-otp-input/fesm2015/ng-otp-input.js");
/* harmony import */ var ionic_selectable__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ionic-selectable */ "./node_modules/ionic-selectable/esm2015/ionic-selectable.min.js");









const routes = [
    {
        path: "",
        component: _login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"],
    },
];
let LoginPageModule = class LoginPageModule {
};
LoginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            ng_otp_input__WEBPACK_IMPORTED_MODULE_7__["NgOtpInputModule"],
            ionic_selectable__WEBPACK_IMPORTED_MODULE_8__["IonicSelectableModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
        ],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]],
    })
], LoginPageModule);



/***/ }),

/***/ "./src/app/pages/login/login.page.scss":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.page.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bk-color {\n  position: relative;\n  text-align: center;\n}\n\n.countryCode {\n  border: 1px solid gainsboro;\n  border-radius: 10px;\n}\n\n.imgBack {\n  background-image: url(/assets/img/login1.jpg);\n  height: 50%;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n  align-content: center;\n  text-align: center;\n}\n\n.BigText {\n  font-size: 50px;\n  font-weight: 900;\n  letter-spacing: -0.1px;\n  line-height: 30px;\n  margin-bottom: 2px;\n  margin-top: 0.5rem;\n  color: white;\n}\n\n#verySmall {\n  font-size: 24px;\n  font-weight: 900;\n  letter-spacing: -0.1px;\n  line-height: 30px;\n  margin-bottom: 2px;\n  margin-top: 0.5rem;\n  color: white;\n}\n\n.smallText {\n  font-size: 24px;\n  font-weight: 100;\n  letter-spacing: -0.1px;\n  line-height: 30px;\n  margin-bottom: 2px;\n  margin-top: 0.5rem;\n  color: white;\n}\n\n.backText {\n  line-height: 30px;\n  text-align: left;\n  margin-left: 5%;\n  padding-top: 65%;\n  position: relative;\n}\n\n@media (min-width: 320px) {\n  .backText {\n    line-height: 30px;\n    text-align: left;\n    margin-left: 5%;\n    padding-top: 50%;\n    position: relative;\n  }\n}\n\n@media (min-width: 360px) {\n  .backText {\n    line-height: 30px;\n    text-align: left;\n    margin-left: 5%;\n    padding-top: 50%;\n    position: relative;\n  }\n\n  .smallText {\n    font-size: 25px;\n    font-weight: 100;\n    letter-spacing: -0.1px;\n    line-height: 45px;\n    margin-bottom: 2px;\n    margin-top: 0.5rem;\n    color: white;\n  }\n}\n\n@media (min-height: 812px) and (min-width: 375px) {\n  .backText {\n    line-height: 30px;\n    text-align: left;\n    margin-left: 5%;\n    padding-top: 58%;\n    position: relative;\n  }\n\n  .smallText {\n    font-size: 25px;\n    font-weight: 100;\n    letter-spacing: -0.1px;\n    line-height: 45px;\n    margin-bottom: 2px;\n    margin-top: 0.5rem;\n    color: white;\n  }\n\n  #overlay {\n    position: fixed;\n    /* display: none; */\n    width: 100%;\n    height: 43% !important;\n    top: 0;\n    left: 0;\n    right: 0;\n    bottom: 0;\n    background-color: #000000a8;\n    z-index: 0;\n    cursor: pointer;\n  }\n}\n\n@media (max-height: 667px) and (min-width: 375px) {\n  .backText {\n    line-height: 30px;\n    text-align: left;\n    margin-left: 5%;\n    padding-top: 58%;\n    position: relative;\n  }\n\n  .smallText {\n    font-size: 25px;\n    font-weight: 100;\n    letter-spacing: -0.1px;\n    line-height: 45px;\n    margin-bottom: 2px;\n    margin-top: 0.5rem;\n    color: white;\n  }\n\n  #overlay {\n    position: fixed;\n    width: 100%;\n    height: 50% !important;\n    top: 0;\n    left: 0;\n    right: 0;\n    bottom: 0;\n    background-color: #000000a8;\n    z-index: 0;\n    cursor: pointer;\n  }\n}\n\n@media (min-width: 390px) {\n  .centerLogin {\n    margin-top: 8% !important;\n    background-color: white;\n    z-index: 999999;\n  }\n\n  #overlay {\n    position: fixed;\n    /* display: none; */\n    width: 100%;\n    height: 42% !important;\n    top: 0;\n    left: 0;\n    right: 0;\n    bottom: 0;\n    background-color: #000000a8;\n    z-index: 0;\n    cursor: pointer;\n  }\n}\n\n@media (min-width: 411px) {\n  .backText {\n    line-height: 30px;\n    text-align: left;\n    margin-left: 5%;\n    padding-top: 55%;\n    position: relative;\n  }\n\n  .smallText {\n    font-size: 25px;\n    font-weight: 100;\n    letter-spacing: -0.1px;\n    line-height: 45px;\n    margin-bottom: 2px;\n    margin-top: 0.5rem;\n    color: white;\n  }\n}\n\n@media (min-height: 736px) and (min-width: 414px) {\n  #overlay {\n    position: fixed;\n    /* display: none; */\n    width: 100%;\n    height: 49% !important;\n    top: 0;\n    left: 0;\n    right: 0;\n    bottom: 0;\n    background-color: #000000a8;\n    z-index: 0;\n    cursor: pointer;\n  }\n}\n\n@media (min-height: 896px) and (min-width: 414px) {\n  #overlay {\n    position: fixed;\n    /* display: none; */\n    width: 100%;\n    height: 40% !important;\n    top: 0;\n    left: 0;\n    right: 0;\n    bottom: 0;\n    background-color: #000000a8;\n    z-index: 0;\n    cursor: pointer;\n  }\n}\n\n@media (min-width: 540px) {\n  .backText {\n    line-height: 30px;\n    text-align: left;\n    margin-left: 5%;\n    padding-top: 45%;\n    position: relative;\n  }\n\n  .smallText {\n    font-size: 25px;\n    font-weight: 100;\n    letter-spacing: -0.1px;\n    line-height: 45px;\n    margin-bottom: 2px;\n    margin-top: 0.5rem;\n    color: white;\n  }\n}\n\n@media (min-width: 600px) {\n  .backText {\n    line-height: 30px;\n    text-align: left;\n    margin-left: 5%;\n    padding-top: 60%;\n    position: relative;\n  }\n\n  .smallText {\n    font-size: 35px;\n    font-weight: 100;\n    letter-spacing: -0.1px;\n    line-height: 45px;\n    margin-bottom: 2px;\n    margin-top: 0.5rem;\n    color: white;\n  }\n\n  .centerLogin {\n    margin-top: 8% !important;\n    background-color: white;\n    z-index: 999999;\n  }\n}\n\n@media (min-width: 768px) {\n  .backText {\n    line-height: 30px;\n    text-align: left;\n    margin-left: 5%;\n    padding-top: 50%;\n    position: relative;\n  }\n\n  .smallText {\n    font-size: 35px;\n    font-weight: 100;\n    letter-spacing: -0.1px;\n    line-height: 45px;\n    margin-bottom: 2px;\n    margin-top: 0.5rem;\n    color: white;\n  }\n}\n\n@media (min-width: 800px) {\n  .backText {\n    line-height: 30px;\n    text-align: left;\n    margin-left: 5%;\n    padding-top: 50%;\n    position: relative;\n  }\n\n  .smallText {\n    font-size: 50px;\n    font-weight: 100;\n    letter-spacing: -0.1px;\n    line-height: 45px;\n    margin-bottom: 2px;\n    margin-top: 0.5rem;\n    color: white;\n  }\n\n  .BigText {\n    font-size: 80px;\n    font-weight: 900;\n    letter-spacing: -0.1px;\n    line-height: 50px;\n    margin-bottom: 2px;\n    margin-top: 0.5rem;\n    color: white;\n  }\n}\n\nion-item {\n  --border-color: var(--ion-color-danger, #ffffff);\n  --border-color: white;\n  --highlight-color-invalid: rgb(255, 255, 255);\n  --highlight-color-valid: rgb(255, 255, 255);\n}\n\n.flag {\n  width: 20px;\n  /* height: 10px; */\n  vertical-align: middle;\n  margin-left: 8px;\n}\n\n.centerLogin {\n  margin-top: 8%;\n  background-color: white;\n  z-index: 999999;\n}\n\n.headerText {\n  text-align: center;\n  font-size: 20px !important;\n  margin-bottom: 5%;\n}\n\n.button-native {\n  --background: #fbb91d !important;\n}\n\n.alignCen {\n  position: relative !important;\n  text-align: center !important;\n}\n\n.background_whole {\n  z-index: 0 !important;\n  --background: linear-gradient(360deg, #fff 80%, #000000 50%) !important;\n}\n\n.move_segment {\n  margin-top: 10%;\n}\n\n.logoSize {\n  text-align: center;\n  width: 30%;\n  align-content: center;\n  margin-left: 0%;\n  margin-top: 5%;\n  margin-bottom: 5%;\n}\n\n.centerText {\n  text-align: center;\n  font-size: 15px !important;\n  margin-top: 30%;\n  position: fixed;\n  color: #000000;\n  margin-left: 5%;\n  margin-right: 5%;\n}\n\n#overlay {\n  position: fixed;\n  /* display: none; */\n  width: 100%;\n  height: 50%;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  background-color: #000000a8;\n  z-index: 0;\n  cursor: pointer;\n}\n\nion-content[_ngcontent-dog-c1] .phoneCountry[_ngcontent-dog-c1] {\n  border: 1px solid gainsboro;\n  border-radius: 10px;\n  margin-left: 2px;\n  background-color: #000000 !important;\n  color: #fbb91d;\n}\n\n.footer-md:before {\n  left: 0;\n  top: -2px;\n  bottom: auto;\n  background-position: left 0 top 0;\n  position: absolute;\n  width: 100%;\n  height: 2px;\n  background-image: url();\n  background-repeat: repeat-x;\n  content: \"\" !important;\n}\n\nion-content {\n  /** RTL **/\n}\n\n@-webkit-keyframes wiki {\n  from {\n    opacity: 0;\n  }\n  to {\n    opacity: 1;\n  }\n}\n\n@keyframes wiki {\n  from {\n    opacity: 0;\n  }\n  to {\n    opacity: 1;\n  }\n}\n\nion-content form {\n  margin-bottom: 16px;\n  height: 300px;\n  background: white;\n  padding: 10px;\n  border-radius: 12px;\n  margin-top: 5%;\n  -webkit-animation-direction: alternate;\n          animation-direction: alternate;\n  -webkit-animation-name: wiki;\n          animation-name: wiki;\n  -webkit-animation-duration: 0.3s;\n          animation-duration: 0.3s;\n  -webkit-animation-iteration-count: 1;\n          animation-iteration-count: 1;\n  border: 1px solid #d8d8d8;\n  box-shadow: 0px 1.5px 0px 0px rgba(3, 3, 3, 0.253);\n}\n\nion-content form ion-button {\n  margin-top: 10px !important;\n  border-radius: 12px;\n}\n\nion-content .new-background-color {\n  margin-top: 10px;\n}\n\nion-content p {\n  font-size: 0.8em;\n}\n\nion-content ion-input {\n  padding: 30px;\n}\n\nion-content ion-item {\n  --inner-padding-start: 0px;\n  --padding-start: 0px;\n  --inner-padding-end: 0px;\n  padding: 2px;\n}\n\nion-content ion-button {\n  --background: #fbb91d;\n  height: 50px;\n  color: #000000;\n  -webkit-animation-direction: alternate;\n          animation-direction: alternate;\n  -webkit-animation-name: wiki;\n          animation-name: wiki;\n  -webkit-animation-duration: 0.3s;\n          animation-duration: 0.3s;\n  -webkit-animation-iteration-count: 1;\n          animation-iteration-count: 1;\n  border-radius: 10px;\n}\n\nion-content .terms {\n  margin-top: 40px;\n  color: #fbb91d;\n  text-align: center;\n}\n\nion-content .logo-text {\n  color: #fbb91d;\n  text-align: center;\n  font-size: 1.2em;\n}\n\nion-content .logo-text1 {\n  color: #fbb91d;\n  text-align: center;\n  font-size: 1.4em;\n  font-weight: 200;\n  text-transform: uppercase;\n}\n\nion-content .o_section {\n  -webkit-animation-direction: alternate;\n          animation-direction: alternate;\n  -webkit-animation-name: wiki;\n          animation-name: wiki;\n  -webkit-animation-duration: 0.3s;\n          animation-duration: 0.3s;\n  -webkit-animation-iteration-count: 1;\n          animation-iteration-count: 1;\n}\n\nion-content .main {\n  height: 100vh;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n          flex-direction: column;\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n\n@media screen and (min-width: 767px) {\n  ion-content ion-card {\n    width: 600px;\n    margin: auto;\n  }\n}\n\nion-content ion-card {\n  --background: #fff;\n  box-shadow: none;\n  -webkit-box-shadow: none;\n  overflow: scroll;\n}\n\nion-content .input {\n  background-color: #f0f0f0;\n  border: 1px solid #d2d2d2;\n  border-radius: 10px;\n  font-size: 0.9em !important;\n}\n\nion-content .input {\n  height: 52px !important;\n}\n\nion-content .otpinput {\n  letter-spacing: 30px;\n  -webkit-padding-end: 0;\n  --padding-end: 0;\n  font-size: 30px;\n  border: none;\n  background: white;\n}\n\nion-content .white {\n  color: white;\n}\n\nion-content .OTP-border ion-col div {\n  border-bottom: 1px solid;\n}\n\nion-content .small {\n  font-size: 13px;\n}\n\nion-content .small a {\n  text-decoration: unset !important;\n}\n\nion-content .button-color {\n  background-color: var(--ion-color-mytheme);\n}\n\nion-content .logo {\n  width: 1.25em !important;\n}\n\nion-content .grid {\n  height: 100vh;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n          flex-direction: column;\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n\nion-content .row {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n          flex-direction: row;\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n\nion-content .img-logo {\n  height: 120px;\n  width: 120px;\n}\n\nion-content .fire-logo {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n          flex-direction: column;\n  -webkit-box-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n          align-items: center;\n  padding-bottom: 30px;\n}\n\nion-content .bold {\n  font-weight: bold;\n}\n\nion-content .block {\n  display: block;\n}\n\nion-content .transition {\n  background: -webkit-gradient(linear, left top, right top, color-stop(14%, #fbb91d), color-stop(96%, #fbb91d));\n  background: linear-gradient(to right, #fbb91d 14%, #fbb91d 96%);\n  color: black;\n}\n\nion-content .btn-color {\n  color: #fbb91d;\n}\n\nion-content .error {\n  color: red;\n  text-align: center;\n  display: block;\n  font-weight: bold;\n}\n\nion-content .invoice-box {\n  max-width: 800px;\n  margin: auto;\n  padding: 30px;\n  border: 1px solid #eee;\n  box-shadow: 0 0 10px rgba(0, 0, 0, 0.15);\n  font-size: 16px;\n  line-height: 24px;\n  font-family: \"Helvetica Neue\", \"Helvetica\", Helvetica, Arial, sans-serif;\n  color: #555;\n}\n\nion-content .invoice-box table {\n  width: 100%;\n  line-height: inherit;\n  text-align: left;\n}\n\nion-content .invoice-box table td {\n  padding: 5px;\n  vertical-align: top;\n}\n\nion-content .invoice-box table tr td:nth-child(2) {\n  text-align: right;\n}\n\nion-content .invoice-box table tr.top table td {\n  padding-bottom: 20px;\n}\n\nion-content .btn-transition {\n  background: -webkit-gradient(linear, left top, right top, color-stop(14%, #fbb91d), color-stop(96%, #fbb91d));\n  background: linear-gradient(to right, #fbb91d 14%, #fbb91d 96%);\n  color: #000000;\n}\n\nion-content .emailbtn {\n  background-color: #fbb91d;\n  color: #000000;\n}\n\nion-content .input {\n  background-color: #f0f0f0;\n  border: 1px solid #d2d2d2;\n  border-radius: 10px;\n  font-size: 0.9em !important;\n}\n\nion-content .phoneCountry {\n  border: 1px solid gainsboro;\n  border-radius: 3px;\n  margin-left: 2px;\n  background-color: #000000 !important;\n  color: #fbb91d;\n}\n\nion-content .phoneAuthGrid {\n  border: 1px solid #ccc;\n  margin: 0 7px;\n  border-radius: 3px;\n  background-color: #ffffff;\n  z-index: 0 !important;\n}\n\nion-content .invoice-box table tr.top table td.title {\n  font-size: 45px;\n  line-height: 45px;\n  color: #333;\n}\n\nion-content .invoice-box table tr.information table td {\n  padding-bottom: 40px;\n}\n\nion-content .invoice-box table tr.heading td {\n  background: #eee;\n  border-bottom: 1px solid #ddd;\n  font-weight: bold;\n}\n\nion-content .invoice-box table tr.details td {\n  padding-bottom: 20px;\n}\n\nion-content .invoice-box table tr.item td {\n  border-bottom: 1px solid #eee;\n}\n\nion-content .invoice-box table tr.item.last td {\n  border-bottom: none;\n}\n\nion-content .invoice-box table tr.total td:nth-child(2) {\n  border-top: 2px solid #eee;\n  font-weight: bold;\n}\n\n@media only screen and (max-width: 600px) {\n  ion-content .invoice-box table tr.top table td {\n    width: 100%;\n    display: block;\n    text-align: center;\n  }\n  ion-content .invoice-box table tr.information table td {\n    width: 100%;\n    display: block;\n    text-align: center;\n  }\n}\n\nion-content .rtl {\n  direction: rtl;\n  font-family: Tahoma, \"Helvetica Neue\", \"Helvetica\", Helvetica, Arial, sans-serif;\n}\n\nion-content .rtl table {\n  text-align: right;\n}\n\nion-content .rtl table tr td:nth-child(2) {\n  text-align: left;\n}\n\n.ionic-selectable:not(.ionic-selectable-has-label) {\n  background: #000000 !important;\n  color: #fbb91d !important;\n  border-radius: 10px;\n}\n\n.ionic-selectable-icon-inner {\n  color: var(--icon-color, #fbb91d) !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYWhhL0lvbmljQXBwcy9DNFJpZGUtSU9TLTIwMjQvY2FsbDRyaWRlLXBhc3Nlbmdlci9zcmMvYXBwL3BhZ2VzL2xvZ2luL2xvZ2luLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvbG9naW4vbG9naW4ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBR0Usa0JBQUE7RUFFQSxrQkFBQTtBQ0ZGOztBREtBO0VBR0ssMkJBQUE7RUFDSCxtQkFBQTtBQ0pGOztBRFdBO0VBQ0UsNkNBQUE7RUFHQSxXQUFBO0VBQ0EsMkJBQUE7RUFDQSw0QkFBQTtFQUNBLHNCQUFBO0VBQ0EscUJBQUE7RUFDQSxrQkFBQTtBQ1ZGOztBRGFBO0VBQ0UsZUFBQTtFQUNBLGdCQUFBO0VBQ0Esc0JBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0FDVkY7O0FEYUE7RUFDRSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxzQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7QUNWRjs7QURhQTtFQUNFLGVBQUE7RUFDQSxnQkFBQTtFQUNBLHNCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtBQ1ZGOztBRGFBO0VBQ0UsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FDVkY7O0FEYUE7RUFDRTtJQUNFLGlCQUFBO0lBQ0EsZ0JBQUE7SUFDQSxlQUFBO0lBQ0EsZ0JBQUE7SUFDQSxrQkFBQTtFQ1ZGO0FBQ0Y7O0FEYUE7RUFDRTtJQUNFLGlCQUFBO0lBQ0EsZ0JBQUE7SUFDQSxlQUFBO0lBQ0EsZ0JBQUE7SUFDQSxrQkFBQTtFQ1hGOztFRGFBO0lBQ0UsZUFBQTtJQUNBLGdCQUFBO0lBQ0Esc0JBQUE7SUFDQSxpQkFBQTtJQUNBLGtCQUFBO0lBQ0Esa0JBQUE7SUFDQSxZQUFBO0VDVkY7QUFDRjs7QURhQTtFQUNFO0lBQ0UsaUJBQUE7SUFDQSxnQkFBQTtJQUNBLGVBQUE7SUFDQSxnQkFBQTtJQUNBLGtCQUFBO0VDWEY7O0VEYUE7SUFDRSxlQUFBO0lBQ0EsZ0JBQUE7SUFDQSxzQkFBQTtJQUNBLGlCQUFBO0lBQ0Esa0JBQUE7SUFDQSxrQkFBQTtJQUNBLFlBQUE7RUNWRjs7RURZQTtJQUNFLGVBQUE7SUFDQSxtQkFBQTtJQUNBLFdBQUE7SUFDQSxzQkFBQTtJQUNBLE1BQUE7SUFDQSxPQUFBO0lBQ0EsUUFBQTtJQUNBLFNBQUE7SUFDQSwyQkFBQTtJQUNBLFVBQUE7SUFDQSxlQUFBO0VDVEY7QUFDRjs7QURZQTtFQUNFO0lBQ0UsaUJBQUE7SUFDQSxnQkFBQTtJQUNBLGVBQUE7SUFDQSxnQkFBQTtJQUNBLGtCQUFBO0VDVkY7O0VEWUE7SUFDRSxlQUFBO0lBQ0EsZ0JBQUE7SUFDQSxzQkFBQTtJQUNBLGlCQUFBO0lBQ0Esa0JBQUE7SUFDQSxrQkFBQTtJQUNBLFlBQUE7RUNURjs7RURXQTtJQUNFLGVBQUE7SUFDQSxXQUFBO0lBQ0Esc0JBQUE7SUFDQSxNQUFBO0lBQ0EsT0FBQTtJQUNBLFFBQUE7SUFDQSxTQUFBO0lBQ0EsMkJBQUE7SUFDQSxVQUFBO0lBQ0EsZUFBQTtFQ1JGO0FBQ0Y7O0FENkNBO0VBQ0c7SUFDQyx5QkFBQTtJQUNBLHVCQUFBO0lBQ0EsZUFBQTtFQzNDRjs7RUQ4Q0Y7SUFDRSxlQUFBO0lBQ0EsbUJBQUE7SUFDQSxXQUFBO0lBQ0Esc0JBQUE7SUFDQSxNQUFBO0lBQ0EsT0FBQTtJQUNBLFFBQUE7SUFDQSxTQUFBO0lBQ0EsMkJBQUE7SUFDQSxVQUFBO0lBQ0EsZUFBQTtFQzNDQTtBQUNGOztBRDhDQTtFQUNFO0lBQ0UsaUJBQUE7SUFDQSxnQkFBQTtJQUNBLGVBQUE7SUFDQSxnQkFBQTtJQUNBLGtCQUFBO0VDNUNGOztFRDhDQTtJQUNFLGVBQUE7SUFDQSxnQkFBQTtJQUNBLHNCQUFBO0lBQ0EsaUJBQUE7SUFDQSxrQkFBQTtJQUNBLGtCQUFBO0lBQ0EsWUFBQTtFQzNDRjtBQUNGOztBRCtDRTtFQUNBO0lBQ0UsZUFBQTtJQUNBLG1CQUFBO0lBQ0EsV0FBQTtJQUNBLHNCQUFBO0lBQ0EsTUFBQTtJQUNBLE9BQUE7SUFDQSxRQUFBO0lBQ0EsU0FBQTtJQUNBLDJCQUFBO0lBQ0EsVUFBQTtJQUNBLGVBQUE7RUM3Q0Y7QUFDRjs7QURpREE7RUFDRTtJQUNFLGVBQUE7SUFDQSxtQkFBQTtJQUNBLFdBQUE7SUFDQSxzQkFBQTtJQUNBLE1BQUE7SUFDQSxPQUFBO0lBQ0EsUUFBQTtJQUNBLFNBQUE7SUFDQSwyQkFBQTtJQUNBLFVBQUE7SUFDQSxlQUFBO0VDL0NGO0FBQ0Y7O0FEb0RBO0VBQ0U7SUFDRSxpQkFBQTtJQUNBLGdCQUFBO0lBQ0EsZUFBQTtJQUNBLGdCQUFBO0lBQ0Esa0JBQUE7RUNsREY7O0VEb0RBO0lBQ0UsZUFBQTtJQUNBLGdCQUFBO0lBQ0Esc0JBQUE7SUFDQSxpQkFBQTtJQUNBLGtCQUFBO0lBQ0Esa0JBQUE7SUFDQSxZQUFBO0VDakRGO0FBQ0Y7O0FEb0RBO0VBQ0U7SUFDRSxpQkFBQTtJQUNBLGdCQUFBO0lBQ0EsZUFBQTtJQUNBLGdCQUFBO0lBQ0Esa0JBQUE7RUNsREY7O0VEcURBO0lBQ0UsZUFBQTtJQUNBLGdCQUFBO0lBQ0Esc0JBQUE7SUFDQSxpQkFBQTtJQUNBLGtCQUFBO0lBQ0Esa0JBQUE7SUFDQSxZQUFBO0VDbERGOztFRG9ERztJQUNELHlCQUFBO0lBQ0EsdUJBQUE7SUFDQSxlQUFBO0VDakRGO0FBQ0Y7O0FEb0RBO0VBQ0U7SUFDRSxpQkFBQTtJQUNBLGdCQUFBO0lBQ0EsZUFBQTtJQUNBLGdCQUFBO0lBQ0Esa0JBQUE7RUNsREY7O0VEcURBO0lBQ0UsZUFBQTtJQUNBLGdCQUFBO0lBQ0Esc0JBQUE7SUFDQSxpQkFBQTtJQUNBLGtCQUFBO0lBQ0Esa0JBQUE7SUFDQSxZQUFBO0VDbERGO0FBQ0Y7O0FEcURBO0VBQ0U7SUFDRSxpQkFBQTtJQUNBLGdCQUFBO0lBQ0EsZUFBQTtJQUNBLGdCQUFBO0lBQ0Esa0JBQUE7RUNuREY7O0VEc0RBO0lBQ0UsZUFBQTtJQUNBLGdCQUFBO0lBQ0Esc0JBQUE7SUFDQSxpQkFBQTtJQUNBLGtCQUFBO0lBQ0Esa0JBQUE7SUFDQSxZQUFBO0VDbkRGOztFRHNEQTtJQUNFLGVBQUE7SUFDQSxnQkFBQTtJQUNBLHNCQUFBO0lBQ0EsaUJBQUE7SUFDQSxrQkFBQTtJQUNBLGtCQUFBO0lBQ0EsWUFBQTtFQ25ERjtBQUNGOztBRHNERTtFQUNFLGdEQUFBO0VBQ0EscUJBQUE7RUFDQSw2Q0FBQTtFQUNBLDJDQUFBO0FDcERKOztBRHVEQTtFQUNFLFdBQUE7RUFDQSxrQkFBQTtFQUNBLHNCQUFBO0VBQ0EsZ0JBQUE7QUNwREY7O0FEdURBO0VBQ0UsY0FBQTtFQUNBLHVCQUFBO0VBQ0EsZUFBQTtBQ3BERjs7QUR1REE7RUFDRSxrQkFBQTtFQUNBLDBCQUFBO0VBQ0EsaUJBQUE7QUNwREY7O0FEdURBO0VBQ0UsZ0NBQUE7QUNwREY7O0FEd0RBO0VBQ0UsNkJBQUE7RUFDQSw2QkFBQTtBQ3JERjs7QUR1REE7RUFDRSxxQkFBQTtFQUNBLHVFQUFBO0FDcERGOztBRHNEQTtFQUNFLGVBQUE7QUNuREY7O0FEc0RBO0VBQ0Usa0JBQUE7RUFDQSxVQUFBO0VBQ0EscUJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0FDbkRGOztBRHVEQTtFQUNFLGtCQUFBO0VBQ0EsMEJBQUE7RUFFQSxlQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUNyREY7O0FEd0RBO0VBQ0UsZUFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7RUFDQSxNQUFBO0VBQ0EsT0FBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0VBQ0EsMkJBQUE7RUFDQSxVQUFBO0VBQ0EsZUFBQTtBQ3JERjs7QUR3REE7RUFDRSwyQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxvQ0FBQTtFQUNBLGNBQUE7QUNyREY7O0FEd0RBO0VBQ0UsT0FBQTtFQUNBLFNBQUE7RUFDQSxZQUFBO0VBQ0EsaUNBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0EsdUJBQUE7RUFDQSwyQkFBQTtFQUNBLHNCQUFBO0FDckRGOztBRDZFQTtFQStSRSxVQUFBO0FDeFdGOztBRDJFRTtFQUNFO0lBQ0UsVUFBQTtFQ3pFSjtFRDJFRTtJQUNFLFVBQUE7RUN6RUo7QUFDRjs7QURtRUU7RUFDRTtJQUNFLFVBQUE7RUN6RUo7RUQyRUU7SUFDRSxVQUFBO0VDekVKO0FBQ0Y7O0FEMkVFO0VBQ0UsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsaUJBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFFQSxjQUFBO0VBQ0Esc0NBQUE7VUFBQSw4QkFBQTtFQUNBLDRCQUFBO1VBQUEsb0JBQUE7RUFDQSxnQ0FBQTtVQUFBLHdCQUFBO0VBQ0Esb0NBQUE7VUFBQSw0QkFBQTtFQUNBLHlCQUFBO0VBQ0Esa0RBQUE7QUMxRUo7O0FEMkVJO0VBQ0UsMkJBQUE7RUFDQSxtQkFBQTtBQ3pFTjs7QUQ0RUU7RUFDRSxnQkFBQTtBQzFFSjs7QUQ2RUU7RUFDRSxnQkFBQTtBQzNFSjs7QUQ4RUU7RUFDRSxhQUFBO0FDNUVKOztBRDhFRTtFQUNFLDBCQUFBO0VBQ0Esb0JBQUE7RUFDQSx3QkFBQTtFQUNBLFlBQUE7QUM1RUo7O0FEOEVFO0VBQ0UscUJBQUE7RUFDQSxZQUFBO0VBQ0EsY0FBQTtFQUNBLHNDQUFBO1VBQUEsOEJBQUE7RUFDQSw0QkFBQTtVQUFBLG9CQUFBO0VBQ0EsZ0NBQUE7VUFBQSx3QkFBQTtFQUNBLG9DQUFBO1VBQUEsNEJBQUE7RUFDQSxtQkFBQTtBQzVFSjs7QUQ4RUU7RUFDRSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtBQzVFSjs7QUQ4RUU7RUFDRSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQzVFSjs7QUQ4RUU7RUFDRSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EseUJBQUE7QUM1RUo7O0FEOEVFO0VBQ0Usc0NBQUE7VUFBQSw4QkFBQTtFQUNBLDRCQUFBO1VBQUEsb0JBQUE7RUFDQSxnQ0FBQTtVQUFBLHdCQUFBO0VBQ0Esb0NBQUE7VUFBQSw0QkFBQTtBQzVFSjs7QUQrRUU7RUFDRSxhQUFBO0VBQ0Esb0JBQUE7RUFBQSxhQUFBO0VBQ0EsNEJBQUE7RUFBQSw2QkFBQTtVQUFBLHNCQUFBO0VBQ0Esd0JBQUE7VUFBQSx1QkFBQTtBQzdFSjs7QUQrRUU7RUFDRTtJQUNFLFlBQUE7SUFDQSxZQUFBO0VDN0VKO0FBQ0Y7O0FEK0VFO0VBQ0Usa0JBQUE7RUFDQSxnQkFBQTtFQUNBLHdCQUFBO0VBQ0EsZ0JBQUE7QUM3RUo7O0FEK0VFO0VBQ0UseUJBQUE7RUFDQSx5QkFBQTtFQUNBLG1CQUFBO0VBQ0EsMkJBQUE7QUM3RUo7O0FEK0VFO0VBQ0UsdUJBQUE7QUM3RUo7O0FEK0VFO0VBQ0Usb0JBQUE7RUFDQSxzQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtBQzdFSjs7QUQrRUU7RUFDRSxZQUFBO0FDN0VKOztBRGlGTTtFQUNFLHdCQUFBO0FDL0VSOztBRG9GRTtFQUNFLGVBQUE7QUNsRko7O0FEbUZJO0VBQ0UsaUNBQUE7QUNqRk47O0FEb0ZFO0VBQ0UsMENBQUE7QUNsRko7O0FEb0ZFO0VBQ0Usd0JBQUE7QUNsRko7O0FEb0ZFO0VBQ0UsYUFBQTtFQUNBLG9CQUFBO0VBQUEsYUFBQTtFQUNBLDRCQUFBO0VBQUEsNkJBQUE7VUFBQSxzQkFBQTtFQUNBLHdCQUFBO1VBQUEsdUJBQUE7QUNsRko7O0FEb0ZFO0VBQ0Usb0JBQUE7RUFBQSxhQUFBO0VBQ0EsOEJBQUE7RUFBQSw2QkFBQTtVQUFBLG1CQUFBO0VBQ0Esd0JBQUE7VUFBQSx1QkFBQTtBQ2xGSjs7QURvRkU7RUFDRSxhQUFBO0VBQ0EsWUFBQTtBQ2xGSjs7QURvRkU7RUFDRSxvQkFBQTtFQUFBLGFBQUE7RUFDQSw0QkFBQTtFQUFBLDZCQUFBO1VBQUEsc0JBQUE7RUFDQSx3QkFBQTtVQUFBLHVCQUFBO0VBQ0EseUJBQUE7VUFBQSxtQkFBQTtFQUNBLG9CQUFBO0FDbEZKOztBRG9GRTtFQUNFLGlCQUFBO0FDbEZKOztBRG9GRTtFQUNFLGNBQUE7QUNsRko7O0FEb0ZFO0VBQ0UsNkdBQUE7RUFBQSwrREFBQTtFQUNBLFlBQUE7QUNsRko7O0FEb0ZFO0VBQ0UsY0FBQTtBQ2xGSjs7QURvRkU7RUFDRSxVQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7QUNsRko7O0FEcUZFO0VBQ0UsZ0JBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0Esd0NBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSx3RUFBQTtFQUNBLFdBQUE7QUNuRko7O0FEc0ZFO0VBQ0UsV0FBQTtFQUNBLG9CQUFBO0VBQ0EsZ0JBQUE7QUNwRko7O0FEdUZFO0VBQ0UsWUFBQTtFQUNBLG1CQUFBO0FDckZKOztBRHdGRTtFQUNFLGlCQUFBO0FDdEZKOztBRHlGRTtFQUNFLG9CQUFBO0FDdkZKOztBRHlGRTtFQUNFLDZHQUFBO0VBQUEsK0RBQUE7RUFDQSxjQUFBO0FDdkZKOztBRDBGRTtFQUNFLHlCQUFBO0VBQ0EsY0FBQTtBQ3hGSjs7QUQyRkU7RUFDRSx5QkFBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7RUFDQSwyQkFBQTtBQ3pGSjs7QUQyRkU7RUFDRSwyQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxvQ0FBQTtFQUNBLGNBQUE7QUN6Rko7O0FEMkZFO0VBQ0Usc0JBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSx5QkFBQTtFQUNBLHFCQUFBO0FDekZKOztBRDJGRTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7QUN6Rko7O0FENEZFO0VBQ0Usb0JBQUE7QUMxRko7O0FENkZFO0VBQ0UsZ0JBQUE7RUFDQSw2QkFBQTtFQUNBLGlCQUFBO0FDM0ZKOztBRDhGRTtFQUNFLG9CQUFBO0FDNUZKOztBRCtGRTtFQUNFLDZCQUFBO0FDN0ZKOztBRGdHRTtFQUNFLG1CQUFBO0FDOUZKOztBRGlHRTtFQUNFLDBCQUFBO0VBQ0EsaUJBQUE7QUMvRko7O0FEa0dFO0VBQ0U7SUFDRSxXQUFBO0lBQ0EsY0FBQTtJQUNBLGtCQUFBO0VDaEdKO0VEbUdFO0lBQ0UsV0FBQTtJQUNBLGNBQUE7SUFDQSxrQkFBQTtFQ2pHSjtBQUNGOztBRHFHRTtFQUNFLGNBQUE7RUFDQSxnRkFBQTtBQ25HSjs7QUR1R0U7RUFDRSxpQkFBQTtBQ3JHSjs7QUR3R0U7RUFDRSxnQkFBQTtBQ3RHSjs7QUQySUE7RUFDRSw4QkFBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7QUN4SUY7O0FEMklBO0VBQ0UsNENBQUE7QUN4SUYiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9sb2dpbi9sb2dpbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYmstY29sb3Ige1xyXG4gIC8vIGJhY2tncm91bmQ6ICNmYmI5MWQ7XHJcbiAgLy8gaGVpZ2h0OiAxNSU7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIC8vIHotaW5kZXg6IDk5OTk5OTk5OTk5OTk7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG4uY291bnRyeUNvZGV7XHJcbiAgIC8vIGJhY2tncm91bmQtY29sb3I6ICNmYmI5MWQ7XHJcblxyXG4gICAgIGJvcmRlcjogMXB4IHNvbGlkIGdhaW5zYm9ybztcclxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gIC8vIG1hcmdpbi1sZWZ0OiAycHg7XHJcbiAgLy8gYmFja2dyb3VuZC1jb2xvcjogI2ZiYjkxZCAhaW1wb3J0YW50OyAgXHJcbn1cclxuXHJcblxyXG5cclxuLmltZ0JhY2sge1xyXG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybCgvYXNzZXRzL2ltZy9sb2dpbjEuanBnKTtcclxuICAvL2JhY2tncm91bmQtaW1hZ2U6IHVybChcImh0dHBzOi8vcmVzLmNsb3VkaW5hcnkuY29tL3NhaGFuYW5hL2ltYWdlL3VwbG9hZC92MTY0MDA4ODAxOS9kcml2aW5nX3l0ZWdmaS5naWZcIik7XHJcblxyXG4gIGhlaWdodDogNTAlO1xyXG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcclxuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgYWxpZ24tY29udGVudDogY2VudGVyO1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuLkJpZ1RleHQge1xyXG4gIGZvbnQtc2l6ZTogNTBweDtcclxuICBmb250LXdlaWdodDogOTAwO1xyXG4gIGxldHRlci1zcGFjaW5nOiAtMC4xcHg7XHJcbiAgbGluZS1oZWlnaHQ6IDMwcHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogMnB4O1xyXG4gIG1hcmdpbi10b3A6IDAuNXJlbTtcclxuICBjb2xvcjogd2hpdGU7XHJcbn1cclxuXHJcbiN2ZXJ5U21hbGwge1xyXG4gIGZvbnQtc2l6ZTogMjRweDtcclxuICBmb250LXdlaWdodDogOTAwO1xyXG4gIGxldHRlci1zcGFjaW5nOiAtMC4xcHg7XHJcbiAgbGluZS1oZWlnaHQ6IDMwcHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogMnB4O1xyXG4gIG1hcmdpbi10b3A6IDAuNXJlbTtcclxuICBjb2xvcjogd2hpdGU7XHJcbn1cclxuXHJcbi5zbWFsbFRleHQge1xyXG4gIGZvbnQtc2l6ZTogMjRweDtcclxuICBmb250LXdlaWdodDogMTAwO1xyXG4gIGxldHRlci1zcGFjaW5nOiAtMC4xcHg7XHJcbiAgbGluZS1oZWlnaHQ6IDMwcHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogMnB4O1xyXG4gIG1hcmdpbi10b3A6IDAuNXJlbTtcclxuICBjb2xvcjogd2hpdGU7XHJcbn1cclxuXHJcbi5iYWNrVGV4dCB7XHJcbiAgbGluZS1oZWlnaHQ6IDMwcHg7XHJcbiAgdGV4dC1hbGlnbjogbGVmdDtcclxuICBtYXJnaW4tbGVmdDogNSU7XHJcbiAgcGFkZGluZy10b3A6IDY1JTtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbn1cclxuXHJcbkBtZWRpYSAobWluLXdpZHRoOiAzMjBweCkge1xyXG4gIC5iYWNrVGV4dCB7XHJcbiAgICBsaW5lLWhlaWdodDogMzBweDtcclxuICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICBtYXJnaW4tbGVmdDogNSU7XHJcbiAgICBwYWRkaW5nLXRvcDogNTAlO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIH1cclxufVxyXG5cclxuQG1lZGlhIChtaW4td2lkdGg6IDM2MHB4KSB7XHJcbiAgLmJhY2tUZXh0IHtcclxuICAgIGxpbmUtaGVpZ2h0OiAzMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICAgIG1hcmdpbi1sZWZ0OiA1JTtcclxuICAgIHBhZGRpbmctdG9wOiA1MCU7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgfVxyXG4gIC5zbWFsbFRleHQge1xyXG4gICAgZm9udC1zaXplOiAyNXB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDEwMDtcclxuICAgIGxldHRlci1zcGFjaW5nOiAtMC4xcHg7XHJcbiAgICBsaW5lLWhlaWdodDogNDVweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDJweDtcclxuICAgIG1hcmdpbi10b3A6IDAuNXJlbTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICB9XHJcbn1cclxuXHJcbkBtZWRpYSAobWluLWhlaWdodDogODEycHgpIGFuZCAobWluLXdpZHRoOiAzNzVweCl7XHJcbiAgLmJhY2tUZXh0IHtcclxuICAgIGxpbmUtaGVpZ2h0OiAzMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICAgIG1hcmdpbi1sZWZ0OiA1JTtcclxuICAgIHBhZGRpbmctdG9wOiA1OCU7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgfVxyXG4gIC5zbWFsbFRleHQge1xyXG4gICAgZm9udC1zaXplOiAyNXB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDEwMDtcclxuICAgIGxldHRlci1zcGFjaW5nOiAtMC4xcHg7XHJcbiAgICBsaW5lLWhlaWdodDogNDVweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDJweDtcclxuICAgIG1hcmdpbi10b3A6IDAuNXJlbTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICB9XHJcbiAgI292ZXJsYXkge1xyXG4gICAgcG9zaXRpb246IGZpeGVkO1xyXG4gICAgLyogZGlzcGxheTogbm9uZTsgKi9cclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiA0MyUgIWltcG9ydGFudDtcclxuICAgIHRvcDogMDtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICByaWdodDogMDtcclxuICAgIGJvdHRvbTogMDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMwMDAwMDBhODtcclxuICAgIHotaW5kZXg6IDA7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgfVxyXG59XHJcblxyXG5AbWVkaWEgKG1heC1oZWlnaHQ6IDY2N3B4KSBhbmQgKG1pbi13aWR0aDogMzc1cHgpe1xyXG4gIC5iYWNrVGV4dCB7XHJcbiAgICBsaW5lLWhlaWdodDogMzBweDtcclxuICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICBtYXJnaW4tbGVmdDogNSU7XHJcbiAgICBwYWRkaW5nLXRvcDogNTglO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIH1cclxuICAuc21hbGxUZXh0IHtcclxuICAgIGZvbnQtc2l6ZTogMjVweDtcclxuICAgIGZvbnQtd2VpZ2h0OiAxMDA7XHJcbiAgICBsZXR0ZXItc3BhY2luZzogLTAuMXB4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDQ1cHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAycHg7XHJcbiAgICBtYXJnaW4tdG9wOiAwLjVyZW07XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgfVxyXG4gICNvdmVybGF5IHtcclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiA1MCUgIWltcG9ydGFudDtcclxuICAgIHRvcDogMDtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICByaWdodDogMDtcclxuICAgIGJvdHRvbTogMDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMwMDAwMDBhODtcclxuICAgIHotaW5kZXg6IDA7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgfVxyXG59XHJcbi8vIEBtZWRpYSAobWluLXdpZHRoOiAzNzVweCkge1xyXG4vLyAgIC5iYWNrVGV4dCB7XHJcbi8vICAgICBsaW5lLWhlaWdodDogMzBweDtcclxuLy8gICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbi8vICAgICBtYXJnaW4tbGVmdDogNSU7XHJcbi8vICAgICBwYWRkaW5nLXRvcDogNTglO1xyXG4vLyAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4vLyAgIH1cclxuLy8gICAuc21hbGxUZXh0IHtcclxuLy8gICAgIGZvbnQtc2l6ZTogMjVweDtcclxuLy8gICAgIGZvbnQtd2VpZ2h0OiAxMDA7XHJcbi8vICAgICBsZXR0ZXItc3BhY2luZzogLTAuMXB4O1xyXG4vLyAgICAgbGluZS1oZWlnaHQ6IDQ1cHg7XHJcbi8vICAgICBtYXJnaW4tYm90dG9tOiAycHg7XHJcbi8vICAgICBtYXJnaW4tdG9wOiAwLjVyZW07XHJcbi8vICAgICBjb2xvcjogd2hpdGU7XHJcbi8vICAgfVxyXG5cclxuLy8gICAjb3ZlcmxheSB7XHJcbi8vICAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbi8vICAgICAvKiBkaXNwbGF5OiBub25lOyAqL1xyXG4vLyAgICAgd2lkdGg6IDEwMCU7XHJcbi8vICAgICBoZWlnaHQ6IDUwJSAhaW1wb3J0YW50O1xyXG4vLyAgICAgdG9wOiAwO1xyXG4vLyAgICAgbGVmdDogMDtcclxuLy8gICAgIHJpZ2h0OiAwO1xyXG4vLyAgICAgYm90dG9tOiAwO1xyXG4vLyAgICAgYmFja2dyb3VuZC1jb2xvcjogIzAwMDAwMGE4O1xyXG4vLyAgICAgei1pbmRleDogMDtcclxuLy8gICAgIGN1cnNvcjogcG9pbnRlcjtcclxuLy8gICB9XHJcblxyXG4gXHJcbi8vIH1cclxuXHJcbkBtZWRpYSAobWluLXdpZHRoOiAzOTBweCkge1xyXG4gICAuY2VudGVyTG9naW4ge1xyXG4gICAgbWFyZ2luLXRvcDogOCUgIWltcG9ydGFudDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG4gICAgei1pbmRleDogOTk5OTk5O1xyXG59XHJcblxyXG4jb3ZlcmxheSB7XHJcbiAgcG9zaXRpb246IGZpeGVkO1xyXG4gIC8qIGRpc3BsYXk6IG5vbmU7ICovXHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiA0MiUgIWltcG9ydGFudDtcclxuICB0b3A6IDA7XHJcbiAgbGVmdDogMDtcclxuICByaWdodDogMDtcclxuICBib3R0b206IDA7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwMDAwMGE4O1xyXG4gIHotaW5kZXg6IDA7XHJcbiAgY3Vyc29yOiBwb2ludGVyO1xyXG59XHJcbn1cclxuXHJcbkBtZWRpYSAobWluLXdpZHRoOiA0MTFweCkge1xyXG4gIC5iYWNrVGV4dCB7XHJcbiAgICBsaW5lLWhlaWdodDogMzBweDtcclxuICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICBtYXJnaW4tbGVmdDogNSU7XHJcbiAgICBwYWRkaW5nLXRvcDogNTUlO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIH1cclxuICAuc21hbGxUZXh0IHtcclxuICAgIGZvbnQtc2l6ZTogMjVweDtcclxuICAgIGZvbnQtd2VpZ2h0OiAxMDA7XHJcbiAgICBsZXR0ZXItc3BhY2luZzogLTAuMXB4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDQ1cHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAycHg7XHJcbiAgICBtYXJnaW4tdG9wOiAwLjVyZW07XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgfVxyXG59XHJcblxyXG5cclxuICBAbWVkaWEgKG1pbi1oZWlnaHQ6IDczNnB4KSBhbmQgKG1pbi13aWR0aDogNDE0cHgpe1xyXG4gICNvdmVybGF5IHtcclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgIC8qIGRpc3BsYXk6IG5vbmU7ICovXHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogNDklICFpbXBvcnRhbnQ7XHJcbiAgICB0b3A6IDA7XHJcbiAgICBsZWZ0OiAwO1xyXG4gICAgcmlnaHQ6IDA7XHJcbiAgICBib3R0b206IDA7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwMDAwYTg7XHJcbiAgICB6LWluZGV4OiAwO1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gIH1cclxufVxyXG5cclxuXHJcbkBtZWRpYSAobWluLWhlaWdodDogODk2cHgpIGFuZCAobWluLXdpZHRoOiA0MTRweCl7XHJcbiAgI292ZXJsYXkge1xyXG4gICAgcG9zaXRpb246IGZpeGVkO1xyXG4gICAgLyogZGlzcGxheTogbm9uZTsgKi9cclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiA0MCUgIWltcG9ydGFudDtcclxuICAgIHRvcDogMDtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICByaWdodDogMDtcclxuICAgIGJvdHRvbTogMDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMwMDAwMDBhODtcclxuICAgIHotaW5kZXg6IDA7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgfVxyXG59XHJcblxyXG5cclxuXHJcbkBtZWRpYSAobWluLXdpZHRoOiA1NDBweCkge1xyXG4gIC5iYWNrVGV4dCB7XHJcbiAgICBsaW5lLWhlaWdodDogMzBweDtcclxuICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICBtYXJnaW4tbGVmdDogNSU7XHJcbiAgICBwYWRkaW5nLXRvcDogNDUlO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIH1cclxuICAuc21hbGxUZXh0IHtcclxuICAgIGZvbnQtc2l6ZTogMjVweDtcclxuICAgIGZvbnQtd2VpZ2h0OiAxMDA7XHJcbiAgICBsZXR0ZXItc3BhY2luZzogLTAuMXB4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDQ1cHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAycHg7XHJcbiAgICBtYXJnaW4tdG9wOiAwLjVyZW07XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgfVxyXG59XHJcblxyXG5AbWVkaWEgKG1pbi13aWR0aDogNjAwcHgpIHtcclxuICAuYmFja1RleHQge1xyXG4gICAgbGluZS1oZWlnaHQ6IDMwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDUlO1xyXG4gICAgcGFkZGluZy10b3A6IDYwJTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICB9XHJcblxyXG4gIC5zbWFsbFRleHQge1xyXG4gICAgZm9udC1zaXplOiAzNXB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDEwMDtcclxuICAgIGxldHRlci1zcGFjaW5nOiAtMC4xcHg7XHJcbiAgICBsaW5lLWhlaWdodDogNDVweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDJweDtcclxuICAgIG1hcmdpbi10b3A6IDAuNXJlbTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICB9XHJcbiAgICAgLmNlbnRlckxvZ2luIHtcclxuICAgIG1hcmdpbi10b3A6IDglICFpbXBvcnRhbnQ7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICAgIHotaW5kZXg6IDk5OTk5OTtcclxufVxyXG59XHJcblxyXG5AbWVkaWEgKG1pbi13aWR0aDogNzY4cHgpIHtcclxuICAuYmFja1RleHQge1xyXG4gICAgbGluZS1oZWlnaHQ6IDMwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDUlO1xyXG4gICAgcGFkZGluZy10b3A6IDUwJTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICB9XHJcblxyXG4gIC5zbWFsbFRleHQge1xyXG4gICAgZm9udC1zaXplOiAzNXB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDEwMDtcclxuICAgIGxldHRlci1zcGFjaW5nOiAtMC4xcHg7XHJcbiAgICBsaW5lLWhlaWdodDogNDVweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDJweDtcclxuICAgIG1hcmdpbi10b3A6IDAuNXJlbTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICB9XHJcbn1cclxuXHJcbkBtZWRpYSAobWluLXdpZHRoOiA4MDBweCkge1xyXG4gIC5iYWNrVGV4dCB7XHJcbiAgICBsaW5lLWhlaWdodDogMzBweDtcclxuICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICBtYXJnaW4tbGVmdDogNSU7XHJcbiAgICBwYWRkaW5nLXRvcDogNTAlO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIH1cclxuXHJcbiAgLnNtYWxsVGV4dCB7XHJcbiAgICBmb250LXNpemU6IDUwcHg7XHJcbiAgICBmb250LXdlaWdodDogMTAwO1xyXG4gICAgbGV0dGVyLXNwYWNpbmc6IC0wLjFweDtcclxuICAgIGxpbmUtaGVpZ2h0OiA0NXB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMnB4O1xyXG4gICAgbWFyZ2luLXRvcDogMC41cmVtO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gIH1cclxuXHJcbiAgLkJpZ1RleHQge1xyXG4gICAgZm9udC1zaXplOiA4MHB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDkwMDtcclxuICAgIGxldHRlci1zcGFjaW5nOiAtMC4xcHg7XHJcbiAgICBsaW5lLWhlaWdodDogNTBweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDJweDtcclxuICAgIG1hcmdpbi10b3A6IDAuNXJlbTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICB9XHJcbn1cclxuXHJcbiAgaW9uLWl0ZW0ge1xyXG4gICAgLS1ib3JkZXItY29sb3I6IHZhcigtLWlvbi1jb2xvci1kYW5nZXIsICNmZmZmZmYpO1xyXG4gICAgLS1ib3JkZXItY29sb3I6IHdoaXRlOyAvLyBkZWZhdWx0IHVuZGVybGluZSBjb2xvclxyXG4gICAgLS1oaWdobGlnaHQtY29sb3ItaW52YWxpZDogcmdiKDI1NSwgMjU1LCAyNTUpOyAvLyBpbnZhbGlkIHVuZGVybGluZSBjb2xvclxyXG4gICAgLS1oaWdobGlnaHQtY29sb3ItdmFsaWQ6IHJnYigyNTUsIDI1NSwgMjU1KTsgLy8gdmFsaWQgdW5kZXJsaW5lIGNvbG9yXHJcbiAgfVxyXG5cclxuLmZsYWcge1xyXG4gIHdpZHRoOiAyMHB4O1xyXG4gIC8qIGhlaWdodDogMTBweDsgKi9cclxuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xyXG4gIG1hcmdpbi1sZWZ0OiA4cHg7XHJcbn1cclxuXHJcbi5jZW50ZXJMb2dpbiB7XHJcbiAgbWFyZ2luLXRvcDogOCU7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgei1pbmRleDogOTk5OTk5O1xyXG59XHJcblxyXG4uaGVhZGVyVGV4dCB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGZvbnQtc2l6ZTogMjBweCAhaW1wb3J0YW50O1xyXG4gIG1hcmdpbi1ib3R0b206IDUlO1xyXG59XHJcblxyXG4uYnV0dG9uLW5hdGl2ZSB7XHJcbiAgLS1iYWNrZ3JvdW5kOiAjZmJiOTFkICFpbXBvcnRhbnQ7XHJcbiAgLy8gLS1iYWNrZ3JvdW5kLWhvdmVyOiBmYmI5MWQgIWltcG9ydGFudDtcclxufVxyXG5cclxuLmFsaWduQ2VuIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmUgIWltcG9ydGFudDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXIgIWltcG9ydGFudDtcclxufVxyXG4uYmFja2dyb3VuZF93aG9sZSB7XHJcbiAgei1pbmRleDogMCAhaW1wb3J0YW50O1xyXG4gIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDM2MGRlZywgI2ZmZiA4MCUsICMwMDAwMDAgNTAlKSAhaW1wb3J0YW50O1xyXG59XHJcbi5tb3ZlX3NlZ21lbnQge1xyXG4gIG1hcmdpbi10b3A6IDEwJTtcclxufVxyXG5cclxuLmxvZ29TaXplIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgd2lkdGg6IDMwJTtcclxuICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XHJcbiAgbWFyZ2luLWxlZnQ6IDAlO1xyXG4gIG1hcmdpbi10b3A6IDUlO1xyXG4gIG1hcmdpbi1ib3R0b206IDUlO1xyXG4gIC8vIHBvc2l0aW9uOiBmaXhlZDtcclxufVxyXG5cclxuLmNlbnRlclRleHQge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBmb250LXNpemU6IDE1cHggIWltcG9ydGFudDtcclxuXHJcbiAgbWFyZ2luLXRvcDogMzAlO1xyXG4gIHBvc2l0aW9uOiBmaXhlZDtcclxuICBjb2xvcjogIzAwMDAwMDtcclxuICBtYXJnaW4tbGVmdDogNSU7XHJcbiAgbWFyZ2luLXJpZ2h0OiA1JTtcclxufVxyXG5cclxuI292ZXJsYXkge1xyXG4gIHBvc2l0aW9uOiBmaXhlZDtcclxuICAvKiBkaXNwbGF5OiBub25lOyAqL1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogNTAlO1xyXG4gIHRvcDogMDtcclxuICBsZWZ0OiAwO1xyXG4gIHJpZ2h0OiAwO1xyXG4gIGJvdHRvbTogMDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwMDAwYTg7XHJcbiAgei1pbmRleDogMDtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuXHJcbmlvbi1jb250ZW50W19uZ2NvbnRlbnQtZG9nLWMxXSAucGhvbmVDb3VudHJ5W19uZ2NvbnRlbnQtZG9nLWMxXSB7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgZ2FpbnNib3JvO1xyXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgbWFyZ2luLWxlZnQ6IDJweDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwMDAwICFpbXBvcnRhbnQ7XHJcbiAgY29sb3I6ICNmYmI5MWQ7XHJcbn1cclxuXHJcbi5mb290ZXItbWQ6YmVmb3JlIHtcclxuICBsZWZ0OiAwO1xyXG4gIHRvcDogLTJweDtcclxuICBib3R0b206IGF1dG87XHJcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogbGVmdCAwIHRvcCAwO1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDJweDtcclxuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoKTtcclxuICBiYWNrZ3JvdW5kLXJlcGVhdDogcmVwZWF0LXg7XHJcbiAgY29udGVudDogXCJcIiAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4vLyAuY2FyIHtcclxuLy8gICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbi8vICAgLy8gdG9wOiA0MCU7XHJcbi8vICAgLy8gd2lkdGg6IDUwJTtcclxuLy8gICBsZWZ0OiAyNSU7XHJcbi8vIH1cclxuXHJcbi8vIC5yb2FkMSB7XHJcbi8vICAgLy8gbGVmdDogLTUxMzlweDtcclxuLy8gICBhbmltYXRpb246IHJvYWQxIDEwcyBpbmZpbml0ZSBsaW5lYXIgcmV2ZXJzZTtcclxuLy8gfVxyXG5cclxuLy8gQGtleWZyYW1lcyByb2FkMSB7XHJcbi8vICAgZnJvbSB7XHJcbi8vICAgICBsZWZ0OiAtNTEzOXB4O1xyXG4vLyAgIH1cclxuLy8gICB0byB7XHJcbi8vICAgICBsZWZ0OiAwcHg7XHJcbi8vICAgfVxyXG4vLyB9XHJcblxyXG5pb24tY29udGVudCB7XHJcbiAgLy8gLS1iYWNrZ3JvdW5kOiB1cmwoXCJzcmMvYXNzZXRzL2ltZy9hLXNwbGFzaC0xLnBuZ1wiKSBuby1yZXBlYXQgY2VudGVyIC8gY292ZXI7XHJcbiAgQGtleWZyYW1lcyB3aWtpIHtcclxuICAgIGZyb20ge1xyXG4gICAgICBvcGFjaXR5OiAwO1xyXG4gICAgfVxyXG4gICAgdG8ge1xyXG4gICAgICBvcGFjaXR5OiAxO1xyXG4gICAgfVxyXG4gIH1cclxuICBmb3JtIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDE2cHg7XHJcbiAgICBoZWlnaHQ6IDMwMHB4O1xyXG4gICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTJweDtcclxuICAgIC8vIG1hcmdpbi10b3A6IDg1JTtcclxuICAgIG1hcmdpbi10b3A6IDUlO1xyXG4gICAgYW5pbWF0aW9uLWRpcmVjdGlvbjogYWx0ZXJuYXRlO1xyXG4gICAgYW5pbWF0aW9uLW5hbWU6IHdpa2k7XHJcbiAgICBhbmltYXRpb24tZHVyYXRpb246IDAuM3M7XHJcbiAgICBhbmltYXRpb24taXRlcmF0aW9uLWNvdW50OiAxO1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xyXG4gICAgYm94LXNoYWRvdzogMHB4IDEuNXB4IDBweCAwcHggcmdiYSgzLCAzLCAzLCAwLjI1Myk7XHJcbiAgICBpb24tYnV0dG9uIHtcclxuICAgICAgbWFyZ2luLXRvcDogMTBweCAhaW1wb3J0YW50O1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG4gICAgfVxyXG4gIH1cclxuICAubmV3LWJhY2tncm91bmQtY29sb3Ige1xyXG4gICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgIC8vYmFja2dyb3VuZDogdXJsKFwic3JjL2Fzc2V0cy9pbWcvYS1zcGxhc2gtMS5wbmdcIikgbm8tcmVwZWF0IGNlbnRlciAvIGNvdmVyO1xyXG4gIH1cclxuICBwIHtcclxuICAgIGZvbnQtc2l6ZTogMC44ZW07XHJcbiAgICAvL2NvbG9yOiAjZDJkMmQyO1xyXG4gIH1cclxuICBpb24taW5wdXQge1xyXG4gICAgcGFkZGluZzogMzBweDtcclxuICB9XHJcbiAgaW9uLWl0ZW0ge1xyXG4gICAgLS1pbm5lci1wYWRkaW5nLXN0YXJ0OiAwcHg7XHJcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDBweDtcclxuICAgIC0taW5uZXItcGFkZGluZy1lbmQ6IDBweDtcclxuICAgIHBhZGRpbmc6IDJweDtcclxuICB9XHJcbiAgaW9uLWJ1dHRvbiB7XHJcbiAgICAtLWJhY2tncm91bmQ6ICNmYmI5MWQ7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICBjb2xvcjogIzAwMDAwMDtcclxuICAgIGFuaW1hdGlvbi1kaXJlY3Rpb246IGFsdGVybmF0ZTtcclxuICAgIGFuaW1hdGlvbi1uYW1lOiB3aWtpO1xyXG4gICAgYW5pbWF0aW9uLWR1cmF0aW9uOiAwLjNzO1xyXG4gICAgYW5pbWF0aW9uLWl0ZXJhdGlvbi1jb3VudDogMTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgfVxyXG4gIC50ZXJtcyB7XHJcbiAgICBtYXJnaW4tdG9wOiA0MHB4O1xyXG4gICAgY29sb3I6ICNmYmI5MWQ7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgfVxyXG4gIC5sb2dvLXRleHQge1xyXG4gICAgY29sb3I6ICNmYmI5MWQ7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6IDEuMmVtO1xyXG4gIH1cclxuICAubG9nby10ZXh0MSB7XHJcbiAgICBjb2xvcjogI2ZiYjkxZDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGZvbnQtc2l6ZTogMS40ZW07XHJcbiAgICBmb250LXdlaWdodDogMjAwO1xyXG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICB9XHJcbiAgLm9fc2VjdGlvbiB7XHJcbiAgICBhbmltYXRpb24tZGlyZWN0aW9uOiBhbHRlcm5hdGU7XHJcbiAgICBhbmltYXRpb24tbmFtZTogd2lraTtcclxuICAgIGFuaW1hdGlvbi1kdXJhdGlvbjogMC4zcztcclxuICAgIGFuaW1hdGlvbi1pdGVyYXRpb24tY291bnQ6IDE7XHJcbiAgfVxyXG5cclxuICAubWFpbiB7XHJcbiAgICBoZWlnaHQ6IDEwMHZoO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICB9XHJcbiAgQG1lZGlhIHNjcmVlbiBhbmQgKG1pbi13aWR0aDogNzY3cHgpIHtcclxuICAgIGlvbi1jYXJkIHtcclxuICAgICAgd2lkdGg6IDYwMHB4O1xyXG4gICAgICBtYXJnaW46IGF1dG87XHJcbiAgICB9XHJcbiAgfVxyXG4gIGlvbi1jYXJkIHtcclxuICAgIC0tYmFja2dyb3VuZDogI2ZmZjtcclxuICAgIGJveC1zaGFkb3c6IG5vbmU7XHJcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IG5vbmU7XHJcbiAgICBvdmVyZmxvdzogc2Nyb2xsO1xyXG4gIH1cclxuICAuaW5wdXQge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDI0MCwgMjQwLCAyNDApO1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgcmdiKDIxMCwgMjEwLCAyMTApO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgIGZvbnQtc2l6ZTogMC45ZW0gIWltcG9ydGFudDtcclxuICB9XHJcbiAgLmlucHV0IHtcclxuICAgIGhlaWdodDogNTJweCAhaW1wb3J0YW50O1xyXG4gIH1cclxuICAub3RwaW5wdXQge1xyXG4gICAgbGV0dGVyLXNwYWNpbmc6IDMwcHg7XHJcbiAgICAtd2Via2l0LXBhZGRpbmctZW5kOiAwO1xyXG4gICAgLS1wYWRkaW5nLWVuZDogMDtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gIH1cclxuICAud2hpdGUge1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gIH1cclxuICAuT1RQLWJvcmRlciB7XHJcbiAgICBpb24tY29sIHtcclxuICAgICAgZGl2IHtcclxuICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQ7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIC5zbWFsbCB7XHJcbiAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICBhIHtcclxuICAgICAgdGV4dC1kZWNvcmF0aW9uOiB1bnNldCAhaW1wb3J0YW50O1xyXG4gICAgfVxyXG4gIH1cclxuICAuYnV0dG9uLWNvbG9yIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1teXRoZW1lKTtcclxuICB9XHJcbiAgLmxvZ28ge1xyXG4gICAgd2lkdGg6IDEuMjVlbSAhaW1wb3J0YW50O1xyXG4gIH1cclxuICAuZ3JpZCB7XHJcbiAgICBoZWlnaHQ6IDEwMHZoO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICB9XHJcbiAgLnJvdyB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIH1cclxuICAuaW1nLWxvZ28ge1xyXG4gICAgaGVpZ2h0OiAxMjBweDtcclxuICAgIHdpZHRoOiAxMjBweDtcclxuICB9XHJcbiAgLmZpcmUtbG9nbyB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHBhZGRpbmctYm90dG9tOiAzMHB4O1xyXG4gIH1cclxuICAuYm9sZCB7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICB9XHJcbiAgLmJsb2NrIHtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gIH1cclxuICAudHJhbnNpdGlvbiB7XHJcbiAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICNmYmI5MWQgMTQlLCAjZmJiOTFkIDk2JSk7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbiAgfVxyXG4gIC5idG4tY29sb3Ige1xyXG4gICAgY29sb3I6ICNmYmI5MWQ7XHJcbiAgfVxyXG4gIC5lcnJvciB7XHJcbiAgICBjb2xvcjogcmVkO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICB9XHJcblxyXG4gIC5pbnZvaWNlLWJveCB7XHJcbiAgICBtYXgtd2lkdGg6IDgwMHB4O1xyXG4gICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgcGFkZGluZzogMzBweDtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICNlZWU7XHJcbiAgICBib3gtc2hhZG93OiAwIDAgMTBweCByZ2JhKDAsIDAsIDAsIDAuMTUpO1xyXG4gICAgZm9udC1zaXplOiAxNnB4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDI0cHg7XHJcbiAgICBmb250LWZhbWlseTogXCJIZWx2ZXRpY2EgTmV1ZVwiLCBcIkhlbHZldGljYVwiLCBIZWx2ZXRpY2EsIEFyaWFsLCBzYW5zLXNlcmlmO1xyXG4gICAgY29sb3I6ICM1NTU7XHJcbiAgfVxyXG5cclxuICAuaW52b2ljZS1ib3ggdGFibGUge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBsaW5lLWhlaWdodDogaW5oZXJpdDtcclxuICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgfVxyXG5cclxuICAuaW52b2ljZS1ib3ggdGFibGUgdGQge1xyXG4gICAgcGFkZGluZzogNXB4O1xyXG4gICAgdmVydGljYWwtYWxpZ246IHRvcDtcclxuICB9XHJcblxyXG4gIC5pbnZvaWNlLWJveCB0YWJsZSB0ciB0ZDpudGgtY2hpbGQoMikge1xyXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgfVxyXG5cclxuICAuaW52b2ljZS1ib3ggdGFibGUgdHIudG9wIHRhYmxlIHRkIHtcclxuICAgIHBhZGRpbmctYm90dG9tOiAyMHB4O1xyXG4gIH1cclxuICAuYnRuLXRyYW5zaXRpb24ge1xyXG4gICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjZmJiOTFkIDE0JSwgI2ZiYjkxZCA5NiUpO1xyXG4gICAgY29sb3I6ICMwMDAwMDA7XHJcbiAgfVxyXG5cclxuICAuZW1haWxidG4ge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZiYjkxZDtcclxuICAgIGNvbG9yOiAjMDAwMDAwO1xyXG4gIH1cclxuXHJcbiAgLmlucHV0IHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYigyNDAsIDI0MCwgMjQwKTtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkIHJnYigyMTAsIDIxMCwgMjEwKTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICBmb250LXNpemU6IDAuOWVtICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG4gIC5waG9uZUNvdW50cnkge1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgZ2FpbnNib3JvO1xyXG4gICAgYm9yZGVyLXJhZGl1czogM3B4O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDJweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMwMDAwMDAgIWltcG9ydGFudDtcclxuICAgIGNvbG9yOiAjZmJiOTFkO1xyXG4gIH1cclxuICAucGhvbmVBdXRoR3JpZCB7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgbWFyZ2luOiAwIDdweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDNweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XHJcbiAgICB6LWluZGV4OiAwICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG4gIC5pbnZvaWNlLWJveCB0YWJsZSB0ci50b3AgdGFibGUgdGQudGl0bGUge1xyXG4gICAgZm9udC1zaXplOiA0NXB4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDQ1cHg7XHJcbiAgICBjb2xvcjogIzMzMztcclxuICB9XHJcblxyXG4gIC5pbnZvaWNlLWJveCB0YWJsZSB0ci5pbmZvcm1hdGlvbiB0YWJsZSB0ZCB7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogNDBweDtcclxuICB9XHJcblxyXG4gIC5pbnZvaWNlLWJveCB0YWJsZSB0ci5oZWFkaW5nIHRkIHtcclxuICAgIGJhY2tncm91bmQ6ICNlZWU7XHJcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2RkZDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gIH1cclxuXHJcbiAgLmludm9pY2UtYm94IHRhYmxlIHRyLmRldGFpbHMgdGQge1xyXG4gICAgcGFkZGluZy1ib3R0b206IDIwcHg7XHJcbiAgfVxyXG5cclxuICAuaW52b2ljZS1ib3ggdGFibGUgdHIuaXRlbSB0ZCB7XHJcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2VlZTtcclxuICB9XHJcblxyXG4gIC5pbnZvaWNlLWJveCB0YWJsZSB0ci5pdGVtLmxhc3QgdGQge1xyXG4gICAgYm9yZGVyLWJvdHRvbTogbm9uZTtcclxuICB9XHJcblxyXG4gIC5pbnZvaWNlLWJveCB0YWJsZSB0ci50b3RhbCB0ZDpudGgtY2hpbGQoMikge1xyXG4gICAgYm9yZGVyLXRvcDogMnB4IHNvbGlkICNlZWU7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICB9XHJcblxyXG4gIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogNjAwcHgpIHtcclxuICAgIC5pbnZvaWNlLWJveCB0YWJsZSB0ci50b3AgdGFibGUgdGQge1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIH1cclxuXHJcbiAgICAuaW52b2ljZS1ib3ggdGFibGUgdHIuaW5mb3JtYXRpb24gdGFibGUgdGQge1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8qKiBSVEwgKiovXHJcbiAgLnJ0bCB7XHJcbiAgICBkaXJlY3Rpb246IHJ0bDtcclxuICAgIGZvbnQtZmFtaWx5OiBUYWhvbWEsIFwiSGVsdmV0aWNhIE5ldWVcIiwgXCJIZWx2ZXRpY2FcIiwgSGVsdmV0aWNhLCBBcmlhbCxcclxuICAgICAgc2Fucy1zZXJpZjtcclxuICB9XHJcblxyXG4gIC5ydGwgdGFibGUge1xyXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgfVxyXG5cclxuICAucnRsIHRhYmxlIHRyIHRkOm50aC1jaGlsZCgyKSB7XHJcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gIH1cclxufVxyXG5cclxuLy8gLmNhciB7XHJcbi8vICAgbWFyZ2luOiAwIGF1dG87XHJcbi8vICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4vLyAgIHdpZHRoOiA0MDBweDtcclxuLy8gICBhbmltYXRpb246IG1vdmluZyAxMHMgbGluZWFyIC0ycyBpbmZpbml0ZTtcclxuLy8gfVxyXG4vLyAuY2FyOmJlZm9yZSB7XHJcbi8vICAgY29udGVudDogXCJcIjtcclxuLy8gICBkaXNwbGF5OiBibG9jaztcclxuLy8gICBhbmltYXRpb246IGNhcm1vdmUgMy4xcyBpbmZpbml0ZSBsaW5lYXI7XHJcbi8vICAgYmFja2dyb3VuZDogdXJsKFwiaHR0cHM6Ly9pLnN0YWNrLmltZ3VyLmNvbS94V05PRy5wbmdcIikgY2VudGVyL2NvdmVyO1xyXG4vLyAgIHBhZGRpbmctdG9wOiA0NS4yNSU7XHJcbi8vIH1cclxuXHJcbi8vIC53ZWVsIHtcclxuLy8gICBhbmltYXRpb246IHdoZWVsIDEwcyBpbmZpbml0ZSAtMnMgbGluZWFyO1xyXG4vLyAgIGJhY2tncm91bmQ6IHVybChcImh0dHBzOi8vaS5zdGFjay5pbWd1ci5jb20vME9zangucG5nXCIpIGNlbnRlci9jb3ZlcjtcclxuLy8gICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbi8vICAgYm90dG9tOiAwLjglO1xyXG4vLyAgIHdpZHRoOiAxNC4xNSU7XHJcbi8vIH1cclxuLy8gLndlZWw6YmVmb3JlIHtcclxuLy8gICBjb250ZW50OiBcIlwiO1xyXG4vLyAgIGRpc3BsYXk6IGJsb2NrO1xyXG4vLyAgIHBhZGRpbmctdG9wOiAxMDAlO1xyXG4vLyB9XHJcbi8vIC53ZWVsMSB7XHJcbi8vICAgbGVmdDogMTQuNSU7XHJcbi8vIH1cclxuLy8gLndlZWwyIHtcclxuLy8gICByaWdodDogMTAlO1xyXG4vLyB9XHJcblxyXG4uaW9uaWMtc2VsZWN0YWJsZTpub3QoLmlvbmljLXNlbGVjdGFibGUtaGFzLWxhYmVsKSB7XHJcbiAgYmFja2dyb3VuZDogIzAwMDAwMCAhaW1wb3J0YW50O1xyXG4gIGNvbG9yOiAjZmJiOTFkICFpbXBvcnRhbnQ7XHJcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcclxufVxyXG5cclxuLmlvbmljLXNlbGVjdGFibGUtaWNvbi1pbm5lciB7XHJcbiAgY29sb3I6IHZhcigtLWljb24tY29sb3IsICNmYmI5MWQpICFpbXBvcnRhbnQ7XHJcbn1cclxuIiwiLmJrLWNvbG9yIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5jb3VudHJ5Q29kZSB7XG4gIGJvcmRlcjogMXB4IHNvbGlkIGdhaW5zYm9ybztcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbn1cblxuLmltZ0JhY2sge1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoL2Fzc2V0cy9pbWcvbG9naW4xLmpwZyk7XG4gIGhlaWdodDogNTAlO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uQmlnVGV4dCB7XG4gIGZvbnQtc2l6ZTogNTBweDtcbiAgZm9udC13ZWlnaHQ6IDkwMDtcbiAgbGV0dGVyLXNwYWNpbmc6IC0wLjFweDtcbiAgbGluZS1oZWlnaHQ6IDMwcHg7XG4gIG1hcmdpbi1ib3R0b206IDJweDtcbiAgbWFyZ2luLXRvcDogMC41cmVtO1xuICBjb2xvcjogd2hpdGU7XG59XG5cbiN2ZXJ5U21hbGwge1xuICBmb250LXNpemU6IDI0cHg7XG4gIGZvbnQtd2VpZ2h0OiA5MDA7XG4gIGxldHRlci1zcGFjaW5nOiAtMC4xcHg7XG4gIGxpbmUtaGVpZ2h0OiAzMHB4O1xuICBtYXJnaW4tYm90dG9tOiAycHg7XG4gIG1hcmdpbi10b3A6IDAuNXJlbTtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG4uc21hbGxUZXh0IHtcbiAgZm9udC1zaXplOiAyNHB4O1xuICBmb250LXdlaWdodDogMTAwO1xuICBsZXR0ZXItc3BhY2luZzogLTAuMXB4O1xuICBsaW5lLWhlaWdodDogMzBweDtcbiAgbWFyZ2luLWJvdHRvbTogMnB4O1xuICBtYXJnaW4tdG9wOiAwLjVyZW07XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLmJhY2tUZXh0IHtcbiAgbGluZS1oZWlnaHQ6IDMwcHg7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIG1hcmdpbi1sZWZ0OiA1JTtcbiAgcGFkZGluZy10b3A6IDY1JTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG5AbWVkaWEgKG1pbi13aWR0aDogMzIwcHgpIHtcbiAgLmJhY2tUZXh0IHtcbiAgICBsaW5lLWhlaWdodDogMzBweDtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgIG1hcmdpbi1sZWZ0OiA1JTtcbiAgICBwYWRkaW5nLXRvcDogNTAlO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDM2MHB4KSB7XG4gIC5iYWNrVGV4dCB7XG4gICAgbGluZS1oZWlnaHQ6IDMwcHg7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICBtYXJnaW4tbGVmdDogNSU7XG4gICAgcGFkZGluZy10b3A6IDUwJTtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIH1cblxuICAuc21hbGxUZXh0IHtcbiAgICBmb250LXNpemU6IDI1cHg7XG4gICAgZm9udC13ZWlnaHQ6IDEwMDtcbiAgICBsZXR0ZXItc3BhY2luZzogLTAuMXB4O1xuICAgIGxpbmUtaGVpZ2h0OiA0NXB4O1xuICAgIG1hcmdpbi1ib3R0b206IDJweDtcbiAgICBtYXJnaW4tdG9wOiAwLjVyZW07XG4gICAgY29sb3I6IHdoaXRlO1xuICB9XG59XG5AbWVkaWEgKG1pbi1oZWlnaHQ6IDgxMnB4KSBhbmQgKG1pbi13aWR0aDogMzc1cHgpIHtcbiAgLmJhY2tUZXh0IHtcbiAgICBsaW5lLWhlaWdodDogMzBweDtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgIG1hcmdpbi1sZWZ0OiA1JTtcbiAgICBwYWRkaW5nLXRvcDogNTglO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgfVxuXG4gIC5zbWFsbFRleHQge1xuICAgIGZvbnQtc2l6ZTogMjVweDtcbiAgICBmb250LXdlaWdodDogMTAwO1xuICAgIGxldHRlci1zcGFjaW5nOiAtMC4xcHg7XG4gICAgbGluZS1oZWlnaHQ6IDQ1cHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMnB4O1xuICAgIG1hcmdpbi10b3A6IDAuNXJlbTtcbiAgICBjb2xvcjogd2hpdGU7XG4gIH1cblxuICAjb3ZlcmxheSB7XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICAgIC8qIGRpc3BsYXk6IG5vbmU7ICovXG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiA0MyUgIWltcG9ydGFudDtcbiAgICB0b3A6IDA7XG4gICAgbGVmdDogMDtcbiAgICByaWdodDogMDtcbiAgICBib3R0b206IDA7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzAwMDAwMGE4O1xuICAgIHotaW5kZXg6IDA7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICB9XG59XG5AbWVkaWEgKG1heC1oZWlnaHQ6IDY2N3B4KSBhbmQgKG1pbi13aWR0aDogMzc1cHgpIHtcbiAgLmJhY2tUZXh0IHtcbiAgICBsaW5lLWhlaWdodDogMzBweDtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgIG1hcmdpbi1sZWZ0OiA1JTtcbiAgICBwYWRkaW5nLXRvcDogNTglO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgfVxuXG4gIC5zbWFsbFRleHQge1xuICAgIGZvbnQtc2l6ZTogMjVweDtcbiAgICBmb250LXdlaWdodDogMTAwO1xuICAgIGxldHRlci1zcGFjaW5nOiAtMC4xcHg7XG4gICAgbGluZS1oZWlnaHQ6IDQ1cHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMnB4O1xuICAgIG1hcmdpbi10b3A6IDAuNXJlbTtcbiAgICBjb2xvcjogd2hpdGU7XG4gIH1cblxuICAjb3ZlcmxheSB7XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogNTAlICFpbXBvcnRhbnQ7XG4gICAgdG9wOiAwO1xuICAgIGxlZnQ6IDA7XG4gICAgcmlnaHQ6IDA7XG4gICAgYm90dG9tOiAwO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMwMDAwMDBhODtcbiAgICB6LWluZGV4OiAwO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDM5MHB4KSB7XG4gIC5jZW50ZXJMb2dpbiB7XG4gICAgbWFyZ2luLXRvcDogOCUgIWltcG9ydGFudDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgICB6LWluZGV4OiA5OTk5OTk7XG4gIH1cblxuICAjb3ZlcmxheSB7XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICAgIC8qIGRpc3BsYXk6IG5vbmU7ICovXG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiA0MiUgIWltcG9ydGFudDtcbiAgICB0b3A6IDA7XG4gICAgbGVmdDogMDtcbiAgICByaWdodDogMDtcbiAgICBib3R0b206IDA7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzAwMDAwMGE4O1xuICAgIHotaW5kZXg6IDA7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogNDExcHgpIHtcbiAgLmJhY2tUZXh0IHtcbiAgICBsaW5lLWhlaWdodDogMzBweDtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgIG1hcmdpbi1sZWZ0OiA1JTtcbiAgICBwYWRkaW5nLXRvcDogNTUlO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgfVxuXG4gIC5zbWFsbFRleHQge1xuICAgIGZvbnQtc2l6ZTogMjVweDtcbiAgICBmb250LXdlaWdodDogMTAwO1xuICAgIGxldHRlci1zcGFjaW5nOiAtMC4xcHg7XG4gICAgbGluZS1oZWlnaHQ6IDQ1cHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMnB4O1xuICAgIG1hcmdpbi10b3A6IDAuNXJlbTtcbiAgICBjb2xvcjogd2hpdGU7XG4gIH1cbn1cbkBtZWRpYSAobWluLWhlaWdodDogNzM2cHgpIGFuZCAobWluLXdpZHRoOiA0MTRweCkge1xuICAjb3ZlcmxheSB7XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICAgIC8qIGRpc3BsYXk6IG5vbmU7ICovXG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiA0OSUgIWltcG9ydGFudDtcbiAgICB0b3A6IDA7XG4gICAgbGVmdDogMDtcbiAgICByaWdodDogMDtcbiAgICBib3R0b206IDA7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzAwMDAwMGE4O1xuICAgIHotaW5kZXg6IDA7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICB9XG59XG5AbWVkaWEgKG1pbi1oZWlnaHQ6IDg5NnB4KSBhbmQgKG1pbi13aWR0aDogNDE0cHgpIHtcbiAgI292ZXJsYXkge1xuICAgIHBvc2l0aW9uOiBmaXhlZDtcbiAgICAvKiBkaXNwbGF5OiBub25lOyAqL1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogNDAlICFpbXBvcnRhbnQ7XG4gICAgdG9wOiAwO1xuICAgIGxlZnQ6IDA7XG4gICAgcmlnaHQ6IDA7XG4gICAgYm90dG9tOiAwO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMwMDAwMDBhODtcbiAgICB6LWluZGV4OiAwO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDU0MHB4KSB7XG4gIC5iYWNrVGV4dCB7XG4gICAgbGluZS1oZWlnaHQ6IDMwcHg7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICBtYXJnaW4tbGVmdDogNSU7XG4gICAgcGFkZGluZy10b3A6IDQ1JTtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIH1cblxuICAuc21hbGxUZXh0IHtcbiAgICBmb250LXNpemU6IDI1cHg7XG4gICAgZm9udC13ZWlnaHQ6IDEwMDtcbiAgICBsZXR0ZXItc3BhY2luZzogLTAuMXB4O1xuICAgIGxpbmUtaGVpZ2h0OiA0NXB4O1xuICAgIG1hcmdpbi1ib3R0b206IDJweDtcbiAgICBtYXJnaW4tdG9wOiAwLjVyZW07XG4gICAgY29sb3I6IHdoaXRlO1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogNjAwcHgpIHtcbiAgLmJhY2tUZXh0IHtcbiAgICBsaW5lLWhlaWdodDogMzBweDtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgIG1hcmdpbi1sZWZ0OiA1JTtcbiAgICBwYWRkaW5nLXRvcDogNjAlO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgfVxuXG4gIC5zbWFsbFRleHQge1xuICAgIGZvbnQtc2l6ZTogMzVweDtcbiAgICBmb250LXdlaWdodDogMTAwO1xuICAgIGxldHRlci1zcGFjaW5nOiAtMC4xcHg7XG4gICAgbGluZS1oZWlnaHQ6IDQ1cHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMnB4O1xuICAgIG1hcmdpbi10b3A6IDAuNXJlbTtcbiAgICBjb2xvcjogd2hpdGU7XG4gIH1cblxuICAuY2VudGVyTG9naW4ge1xuICAgIG1hcmdpbi10b3A6IDglICFpbXBvcnRhbnQ7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gICAgei1pbmRleDogOTk5OTk5O1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogNzY4cHgpIHtcbiAgLmJhY2tUZXh0IHtcbiAgICBsaW5lLWhlaWdodDogMzBweDtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgIG1hcmdpbi1sZWZ0OiA1JTtcbiAgICBwYWRkaW5nLXRvcDogNTAlO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgfVxuXG4gIC5zbWFsbFRleHQge1xuICAgIGZvbnQtc2l6ZTogMzVweDtcbiAgICBmb250LXdlaWdodDogMTAwO1xuICAgIGxldHRlci1zcGFjaW5nOiAtMC4xcHg7XG4gICAgbGluZS1oZWlnaHQ6IDQ1cHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMnB4O1xuICAgIG1hcmdpbi10b3A6IDAuNXJlbTtcbiAgICBjb2xvcjogd2hpdGU7XG4gIH1cbn1cbkBtZWRpYSAobWluLXdpZHRoOiA4MDBweCkge1xuICAuYmFja1RleHQge1xuICAgIGxpbmUtaGVpZ2h0OiAzMHB4O1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgbWFyZ2luLWxlZnQ6IDUlO1xuICAgIHBhZGRpbmctdG9wOiA1MCU7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB9XG5cbiAgLnNtYWxsVGV4dCB7XG4gICAgZm9udC1zaXplOiA1MHB4O1xuICAgIGZvbnQtd2VpZ2h0OiAxMDA7XG4gICAgbGV0dGVyLXNwYWNpbmc6IC0wLjFweDtcbiAgICBsaW5lLWhlaWdodDogNDVweDtcbiAgICBtYXJnaW4tYm90dG9tOiAycHg7XG4gICAgbWFyZ2luLXRvcDogMC41cmVtO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgfVxuXG4gIC5CaWdUZXh0IHtcbiAgICBmb250LXNpemU6IDgwcHg7XG4gICAgZm9udC13ZWlnaHQ6IDkwMDtcbiAgICBsZXR0ZXItc3BhY2luZzogLTAuMXB4O1xuICAgIGxpbmUtaGVpZ2h0OiA1MHB4O1xuICAgIG1hcmdpbi1ib3R0b206IDJweDtcbiAgICBtYXJnaW4tdG9wOiAwLjVyZW07XG4gICAgY29sb3I6IHdoaXRlO1xuICB9XG59XG5pb24taXRlbSB7XG4gIC0tYm9yZGVyLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItZGFuZ2VyLCAjZmZmZmZmKTtcbiAgLS1ib3JkZXItY29sb3I6IHdoaXRlO1xuICAtLWhpZ2hsaWdodC1jb2xvci1pbnZhbGlkOiByZ2IoMjU1LCAyNTUsIDI1NSk7XG4gIC0taGlnaGxpZ2h0LWNvbG9yLXZhbGlkOiByZ2IoMjU1LCAyNTUsIDI1NSk7XG59XG5cbi5mbGFnIHtcbiAgd2lkdGg6IDIwcHg7XG4gIC8qIGhlaWdodDogMTBweDsgKi9cbiAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbiAgbWFyZ2luLWxlZnQ6IDhweDtcbn1cblxuLmNlbnRlckxvZ2luIHtcbiAgbWFyZ2luLXRvcDogOCU7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICB6LWluZGV4OiA5OTk5OTk7XG59XG5cbi5oZWFkZXJUZXh0IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDIwcHggIWltcG9ydGFudDtcbiAgbWFyZ2luLWJvdHRvbTogNSU7XG59XG5cbi5idXR0b24tbmF0aXZlIHtcbiAgLS1iYWNrZ3JvdW5kOiAjZmJiOTFkICFpbXBvcnRhbnQ7XG59XG5cbi5hbGlnbkNlbiB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZSAhaW1wb3J0YW50O1xuICB0ZXh0LWFsaWduOiBjZW50ZXIgIWltcG9ydGFudDtcbn1cblxuLmJhY2tncm91bmRfd2hvbGUge1xuICB6LWluZGV4OiAwICFpbXBvcnRhbnQ7XG4gIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDM2MGRlZywgI2ZmZiA4MCUsICMwMDAwMDAgNTAlKSAhaW1wb3J0YW50O1xufVxuXG4ubW92ZV9zZWdtZW50IHtcbiAgbWFyZ2luLXRvcDogMTAlO1xufVxuXG4ubG9nb1NpemUge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHdpZHRoOiAzMCU7XG4gIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcbiAgbWFyZ2luLWxlZnQ6IDAlO1xuICBtYXJnaW4tdG9wOiA1JTtcbiAgbWFyZ2luLWJvdHRvbTogNSU7XG59XG5cbi5jZW50ZXJUZXh0IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDE1cHggIWltcG9ydGFudDtcbiAgbWFyZ2luLXRvcDogMzAlO1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIGNvbG9yOiAjMDAwMDAwO1xuICBtYXJnaW4tbGVmdDogNSU7XG4gIG1hcmdpbi1yaWdodDogNSU7XG59XG5cbiNvdmVybGF5IHtcbiAgcG9zaXRpb246IGZpeGVkO1xuICAvKiBkaXNwbGF5OiBub25lOyAqL1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiA1MCU7XG4gIHRvcDogMDtcbiAgbGVmdDogMDtcbiAgcmlnaHQ6IDA7XG4gIGJvdHRvbTogMDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwMDAwMGE4O1xuICB6LWluZGV4OiAwO1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG5cbmlvbi1jb250ZW50W19uZ2NvbnRlbnQtZG9nLWMxXSAucGhvbmVDb3VudHJ5W19uZ2NvbnRlbnQtZG9nLWMxXSB7XG4gIGJvcmRlcjogMXB4IHNvbGlkIGdhaW5zYm9ybztcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgbWFyZ2luLWxlZnQ6IDJweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwMDAwMCAhaW1wb3J0YW50O1xuICBjb2xvcjogI2ZiYjkxZDtcbn1cblxuLmZvb3Rlci1tZDpiZWZvcmUge1xuICBsZWZ0OiAwO1xuICB0b3A6IC0ycHg7XG4gIGJvdHRvbTogYXV0bztcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogbGVmdCAwIHRvcCAwO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDJweDtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCk7XG4gIGJhY2tncm91bmQtcmVwZWF0OiByZXBlYXQteDtcbiAgY29udGVudDogXCJcIiAhaW1wb3J0YW50O1xufVxuXG5pb24tY29udGVudCB7XG4gIC8qKiBSVEwgKiovXG59XG5Aa2V5ZnJhbWVzIHdpa2kge1xuICBmcm9tIHtcbiAgICBvcGFjaXR5OiAwO1xuICB9XG4gIHRvIHtcbiAgICBvcGFjaXR5OiAxO1xuICB9XG59XG5pb24tY29udGVudCBmb3JtIHtcbiAgbWFyZ2luLWJvdHRvbTogMTZweDtcbiAgaGVpZ2h0OiAzMDBweDtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDEycHg7XG4gIG1hcmdpbi10b3A6IDUlO1xuICBhbmltYXRpb24tZGlyZWN0aW9uOiBhbHRlcm5hdGU7XG4gIGFuaW1hdGlvbi1uYW1lOiB3aWtpO1xuICBhbmltYXRpb24tZHVyYXRpb246IDAuM3M7XG4gIGFuaW1hdGlvbi1pdGVyYXRpb24tY291bnQ6IDE7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNkOGQ4ZDg7XG4gIGJveC1zaGFkb3c6IDBweCAxLjVweCAwcHggMHB4IHJnYmEoMywgMywgMywgMC4yNTMpO1xufVxuaW9uLWNvbnRlbnQgZm9ybSBpb24tYnV0dG9uIHtcbiAgbWFyZ2luLXRvcDogMTBweCAhaW1wb3J0YW50O1xuICBib3JkZXItcmFkaXVzOiAxMnB4O1xufVxuaW9uLWNvbnRlbnQgLm5ldy1iYWNrZ3JvdW5kLWNvbG9yIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbn1cbmlvbi1jb250ZW50IHAge1xuICBmb250LXNpemU6IDAuOGVtO1xufVxuaW9uLWNvbnRlbnQgaW9uLWlucHV0IHtcbiAgcGFkZGluZzogMzBweDtcbn1cbmlvbi1jb250ZW50IGlvbi1pdGVtIHtcbiAgLS1pbm5lci1wYWRkaW5nLXN0YXJ0OiAwcHg7XG4gIC0tcGFkZGluZy1zdGFydDogMHB4O1xuICAtLWlubmVyLXBhZGRpbmctZW5kOiAwcHg7XG4gIHBhZGRpbmc6IDJweDtcbn1cbmlvbi1jb250ZW50IGlvbi1idXR0b24ge1xuICAtLWJhY2tncm91bmQ6ICNmYmI5MWQ7XG4gIGhlaWdodDogNTBweDtcbiAgY29sb3I6ICMwMDAwMDA7XG4gIGFuaW1hdGlvbi1kaXJlY3Rpb246IGFsdGVybmF0ZTtcbiAgYW5pbWF0aW9uLW5hbWU6IHdpa2k7XG4gIGFuaW1hdGlvbi1kdXJhdGlvbjogMC4zcztcbiAgYW5pbWF0aW9uLWl0ZXJhdGlvbi1jb3VudDogMTtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbn1cbmlvbi1jb250ZW50IC50ZXJtcyB7XG4gIG1hcmdpbi10b3A6IDQwcHg7XG4gIGNvbG9yOiAjZmJiOTFkO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5pb24tY29udGVudCAubG9nby10ZXh0IHtcbiAgY29sb3I6ICNmYmI5MWQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAxLjJlbTtcbn1cbmlvbi1jb250ZW50IC5sb2dvLXRleHQxIHtcbiAgY29sb3I6ICNmYmI5MWQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAxLjRlbTtcbiAgZm9udC13ZWlnaHQ6IDIwMDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cbmlvbi1jb250ZW50IC5vX3NlY3Rpb24ge1xuICBhbmltYXRpb24tZGlyZWN0aW9uOiBhbHRlcm5hdGU7XG4gIGFuaW1hdGlvbi1uYW1lOiB3aWtpO1xuICBhbmltYXRpb24tZHVyYXRpb246IDAuM3M7XG4gIGFuaW1hdGlvbi1pdGVyYXRpb24tY291bnQ6IDE7XG59XG5pb24tY29udGVudCAubWFpbiB7XG4gIGhlaWdodDogMTAwdmg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuQG1lZGlhIHNjcmVlbiBhbmQgKG1pbi13aWR0aDogNzY3cHgpIHtcbiAgaW9uLWNvbnRlbnQgaW9uLWNhcmQge1xuICAgIHdpZHRoOiA2MDBweDtcbiAgICBtYXJnaW46IGF1dG87XG4gIH1cbn1cbmlvbi1jb250ZW50IGlvbi1jYXJkIHtcbiAgLS1iYWNrZ3JvdW5kOiAjZmZmO1xuICBib3gtc2hhZG93OiBub25lO1xuICAtd2Via2l0LWJveC1zaGFkb3c6IG5vbmU7XG4gIG92ZXJmbG93OiBzY3JvbGw7XG59XG5pb24tY29udGVudCAuaW5wdXQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjBmMGYwO1xuICBib3JkZXI6IDFweCBzb2xpZCAjZDJkMmQyO1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBmb250LXNpemU6IDAuOWVtICFpbXBvcnRhbnQ7XG59XG5pb24tY29udGVudCAuaW5wdXQge1xuICBoZWlnaHQ6IDUycHggIWltcG9ydGFudDtcbn1cbmlvbi1jb250ZW50IC5vdHBpbnB1dCB7XG4gIGxldHRlci1zcGFjaW5nOiAzMHB4O1xuICAtd2Via2l0LXBhZGRpbmctZW5kOiAwO1xuICAtLXBhZGRpbmctZW5kOiAwO1xuICBmb250LXNpemU6IDMwcHg7XG4gIGJvcmRlcjogbm9uZTtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG59XG5pb24tY29udGVudCAud2hpdGUge1xuICBjb2xvcjogd2hpdGU7XG59XG5pb24tY29udGVudCAuT1RQLWJvcmRlciBpb24tY29sIGRpdiB7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZDtcbn1cbmlvbi1jb250ZW50IC5zbWFsbCB7XG4gIGZvbnQtc2l6ZTogMTNweDtcbn1cbmlvbi1jb250ZW50IC5zbWFsbCBhIHtcbiAgdGV4dC1kZWNvcmF0aW9uOiB1bnNldCAhaW1wb3J0YW50O1xufVxuaW9uLWNvbnRlbnQgLmJ1dHRvbi1jb2xvciB7XG4gIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1teXRoZW1lKTtcbn1cbmlvbi1jb250ZW50IC5sb2dvIHtcbiAgd2lkdGg6IDEuMjVlbSAhaW1wb3J0YW50O1xufVxuaW9uLWNvbnRlbnQgLmdyaWQge1xuICBoZWlnaHQ6IDEwMHZoO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cbmlvbi1jb250ZW50IC5yb3cge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cbmlvbi1jb250ZW50IC5pbWctbG9nbyB7XG4gIGhlaWdodDogMTIwcHg7XG4gIHdpZHRoOiAxMjBweDtcbn1cbmlvbi1jb250ZW50IC5maXJlLWxvZ28ge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgcGFkZGluZy1ib3R0b206IDMwcHg7XG59XG5pb24tY29udGVudCAuYm9sZCB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuaW9uLWNvbnRlbnQgLmJsb2NrIHtcbiAgZGlzcGxheTogYmxvY2s7XG59XG5pb24tY29udGVudCAudHJhbnNpdGlvbiB7XG4gIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgI2ZiYjkxZCAxNCUsICNmYmI5MWQgOTYlKTtcbiAgY29sb3I6IGJsYWNrO1xufVxuaW9uLWNvbnRlbnQgLmJ0bi1jb2xvciB7XG4gIGNvbG9yOiAjZmJiOTFkO1xufVxuaW9uLWNvbnRlbnQgLmVycm9yIHtcbiAgY29sb3I6IHJlZDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBkaXNwbGF5OiBibG9jaztcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5pb24tY29udGVudCAuaW52b2ljZS1ib3gge1xuICBtYXgtd2lkdGg6IDgwMHB4O1xuICBtYXJnaW46IGF1dG87XG4gIHBhZGRpbmc6IDMwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNlZWU7XG4gIGJveC1zaGFkb3c6IDAgMCAxMHB4IHJnYmEoMCwgMCwgMCwgMC4xNSk7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgbGluZS1oZWlnaHQ6IDI0cHg7XG4gIGZvbnQtZmFtaWx5OiBcIkhlbHZldGljYSBOZXVlXCIsIFwiSGVsdmV0aWNhXCIsIEhlbHZldGljYSwgQXJpYWwsIHNhbnMtc2VyaWY7XG4gIGNvbG9yOiAjNTU1O1xufVxuaW9uLWNvbnRlbnQgLmludm9pY2UtYm94IHRhYmxlIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGxpbmUtaGVpZ2h0OiBpbmhlcml0O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xufVxuaW9uLWNvbnRlbnQgLmludm9pY2UtYm94IHRhYmxlIHRkIHtcbiAgcGFkZGluZzogNXB4O1xuICB2ZXJ0aWNhbC1hbGlnbjogdG9wO1xufVxuaW9uLWNvbnRlbnQgLmludm9pY2UtYm94IHRhYmxlIHRyIHRkOm50aC1jaGlsZCgyKSB7XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xufVxuaW9uLWNvbnRlbnQgLmludm9pY2UtYm94IHRhYmxlIHRyLnRvcCB0YWJsZSB0ZCB7XG4gIHBhZGRpbmctYm90dG9tOiAyMHB4O1xufVxuaW9uLWNvbnRlbnQgLmJ0bi10cmFuc2l0aW9uIHtcbiAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjZmJiOTFkIDE0JSwgI2ZiYjkxZCA5NiUpO1xuICBjb2xvcjogIzAwMDAwMDtcbn1cbmlvbi1jb250ZW50IC5lbWFpbGJ0biB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmYmI5MWQ7XG4gIGNvbG9yOiAjMDAwMDAwO1xufVxuaW9uLWNvbnRlbnQgLmlucHV0IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YwZjBmMDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2QyZDJkMjtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgZm9udC1zaXplOiAwLjllbSAhaW1wb3J0YW50O1xufVxuaW9uLWNvbnRlbnQgLnBob25lQ291bnRyeSB7XG4gIGJvcmRlcjogMXB4IHNvbGlkIGdhaW5zYm9ybztcbiAgYm9yZGVyLXJhZGl1czogM3B4O1xuICBtYXJnaW4tbGVmdDogMnB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwMDAwICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiAjZmJiOTFkO1xufVxuaW9uLWNvbnRlbnQgLnBob25lQXV0aEdyaWQge1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICBtYXJnaW46IDAgN3B4O1xuICBib3JkZXItcmFkaXVzOiAzcHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XG4gIHotaW5kZXg6IDAgIWltcG9ydGFudDtcbn1cbmlvbi1jb250ZW50IC5pbnZvaWNlLWJveCB0YWJsZSB0ci50b3AgdGFibGUgdGQudGl0bGUge1xuICBmb250LXNpemU6IDQ1cHg7XG4gIGxpbmUtaGVpZ2h0OiA0NXB4O1xuICBjb2xvcjogIzMzMztcbn1cbmlvbi1jb250ZW50IC5pbnZvaWNlLWJveCB0YWJsZSB0ci5pbmZvcm1hdGlvbiB0YWJsZSB0ZCB7XG4gIHBhZGRpbmctYm90dG9tOiA0MHB4O1xufVxuaW9uLWNvbnRlbnQgLmludm9pY2UtYm94IHRhYmxlIHRyLmhlYWRpbmcgdGQge1xuICBiYWNrZ3JvdW5kOiAjZWVlO1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2RkZDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5pb24tY29udGVudCAuaW52b2ljZS1ib3ggdGFibGUgdHIuZGV0YWlscyB0ZCB7XG4gIHBhZGRpbmctYm90dG9tOiAyMHB4O1xufVxuaW9uLWNvbnRlbnQgLmludm9pY2UtYm94IHRhYmxlIHRyLml0ZW0gdGQge1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2VlZTtcbn1cbmlvbi1jb250ZW50IC5pbnZvaWNlLWJveCB0YWJsZSB0ci5pdGVtLmxhc3QgdGQge1xuICBib3JkZXItYm90dG9tOiBub25lO1xufVxuaW9uLWNvbnRlbnQgLmludm9pY2UtYm94IHRhYmxlIHRyLnRvdGFsIHRkOm50aC1jaGlsZCgyKSB7XG4gIGJvcmRlci10b3A6IDJweCBzb2xpZCAjZWVlO1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogNjAwcHgpIHtcbiAgaW9uLWNvbnRlbnQgLmludm9pY2UtYm94IHRhYmxlIHRyLnRvcCB0YWJsZSB0ZCB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB9XG4gIGlvbi1jb250ZW50IC5pbnZvaWNlLWJveCB0YWJsZSB0ci5pbmZvcm1hdGlvbiB0YWJsZSB0ZCB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB9XG59XG5pb24tY29udGVudCAucnRsIHtcbiAgZGlyZWN0aW9uOiBydGw7XG4gIGZvbnQtZmFtaWx5OiBUYWhvbWEsIFwiSGVsdmV0aWNhIE5ldWVcIiwgXCJIZWx2ZXRpY2FcIiwgSGVsdmV0aWNhLCBBcmlhbCwgc2Fucy1zZXJpZjtcbn1cbmlvbi1jb250ZW50IC5ydGwgdGFibGUge1xuICB0ZXh0LWFsaWduOiByaWdodDtcbn1cbmlvbi1jb250ZW50IC5ydGwgdGFibGUgdHIgdGQ6bnRoLWNoaWxkKDIpIHtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbn1cblxuLmlvbmljLXNlbGVjdGFibGU6bm90KC5pb25pYy1zZWxlY3RhYmxlLWhhcy1sYWJlbCkge1xuICBiYWNrZ3JvdW5kOiAjMDAwMDAwICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiAjZmJiOTFkICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG59XG5cbi5pb25pYy1zZWxlY3RhYmxlLWljb24taW5uZXIge1xuICBjb2xvcjogdmFyKC0taWNvbi1jb2xvciwgI2ZiYjkxZCkgIWltcG9ydGFudDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/login/login.page.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/login/login.page.ts ***!
  \*******************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var src_validators_email__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/validators/email */ "./src/validators/email.ts");
/* harmony import */ var src_app_services_language_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/language.service */ "./src/app/services/language.service.ts");
/* harmony import */ var src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/settings.service */ "./src/app/services/settings.service.ts");
/* harmony import */ var src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/profile.service */ "./src/app/services/profile.service.ts");
/* harmony import */ var src_app_services_native_map_container_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/native-map-container.service */ "./src/app/services/native-map-container.service.ts");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! src/app/services/pop-up.service */ "./src/app/services/pop-up.service.ts");












class CountryJson2 {
}
let LoginPage = class LoginPage {
    constructor(pop, navCtrl, lp, settings, ntP, platform, menu, loadingCtrl, alertCtrl, authProvider, ph, formBuilder, ngZone) {
        // console.log("JSON COMING", this.CountryJson);
        // console.log("DIAL_CODE", this.CountryJson[72].dial_code);
        this.pop = pop;
        this.navCtrl = navCtrl;
        this.lp = lp;
        this.settings = settings;
        this.ntP = ntP;
        this.platform = platform;
        this.menu = menu;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.authProvider = authProvider;
        this.ph = ph;
        this.formBuilder = formBuilder;
        this.ngZone = ngZone;
        this.overall_list = "signin";
        this.initState = false;
        this.CountryJson = src_environments_environment__WEBPACK_IMPORTED_MODULE_10__["environment"].CountryJson;
        this.OTP = "";
        this.showOTPInput = false;
        this.showPhone = true;
        this.showEmail = false;
        this.showButton = false;
        this.showSignup = false;
        this.showLogin = true;
        this.showOtpComponent = false;
        this.config = {
            allowNumbersOnly: true,
            length: 6,
            isPasswordInput: false,
            disableAutoFocus: false,
            placeholder: "*",
            inputStyles: {
                width: "50px",
                height: "50px",
            },
        };
        // this.port2 = "+233";
        // console.log("PORT is", this.port2);
        //this.port2 = this.CountryJson[72].dial_code;
        //console.log("this.port2", this.port2);
        // tslint:disable-next-line: deprecation
        this.menu.enable(false);
        this.loginForm = formBuilder.group({
            email: [
                "",
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, src_validators_email__WEBPACK_IMPORTED_MODULE_4__["EmailValidator"].isValid]),
            ],
            password: [
                "",
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(6), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]),
            ],
        });
        this.signupForm = formBuilder.group({
            email: [
                "",
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, src_validators_email__WEBPACK_IMPORTED_MODULE_4__["EmailValidator"].isValid]),
            ],
            password: [
                "",
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(6), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]),
            ],
        });
        if (this.initState) {
            this.platform.backButton.subscribe(() => {
                navigator["app"].exitApp();
            });
        }
        else {
            this.platform.backButton.subscribe(() => {
                this.initState = false;
            });
        }
    }
    segmentChanged(ev) {
        console.log("Segment changed", ev);
    }
    showPhoneLogin() {
        this.showPhone = true;
        this.showEmail = false;
    }
    showEmailLogin() {
        this.showEmail = true;
        this.showPhone = false;
    }
    // async ionViewDidEnter() {
    //   this.recaptchaVerifier = new firebase.auth.RecaptchaVerifier(
    //     "sign-in-button",
    //     {
    //       size: "invisible",
    //       callback: (response) => { },
    //       "expired-callback": () => { },
    //     }
    //   );
    // }
    // ionViewDidLoad() {
    //   this.recaptchaVerifier = new firebase.auth.RecaptchaVerifier(
    //     "sign-in-button",
    //     {
    //       size: "invisible",
    //       callback: (response) => { },
    //       "expired-callback": () => { },
    //     }
    //   );
    // }
    // countryCodeChange($event) {
    //   console.log("EVENT", $event);
    //   this.CountryCode = $event.detail.value;
    // }
    countryCodeChange(event) {
        this.CountryCode = event.value;
        console.log("CountryCode", this.CountryCode.dial_code);
    }
    // Button event after the nmber is entered and button is clicked
    // signinWithPhoneNumber($event) {
    //   // this.navCtrl.navigateRoot("home");
    //   this.CountryCode = "+233"
    //   console.log("country", $event, this.recaptchaVerifier);
    //   console.log("PhoneNo", this.PhoneNo, this.CountryCode);
    //   this.showLoadRefresh().then(() => {
    //     if (this.PhoneNo && this.CountryCode) {
    //       this.authProvider
    //         .signInWithPhoneNumber(
    //           this.recaptchaVerifier,
    //           this.CountryCode + this.PhoneNo
    //         )
    //         .then((success) => {
    //           this.showOtpComponent = true;
    //           this.showPhone = false;
    //           this.showEmail = false;
    //           this.showButton = true;
    //         });
    //     } else {
    //       this.pop.presentToast("Error! SMS couldnt be sent. Please try again");
    //     }
    //   });
    // }
    // async showSuccess() {
    //   const alert = await this.alertCtrl.create({
    //     header: "OTP Successful",
    //     buttons: [
    //       {
    //         text: "Ok",
    //         handler: (res) => {
    //           alert.dismiss();
    //         },
    //       },
    //     ],
    //   });
    //   alert.present();
    // }
    // async OtpVerification() {
    //   const alert = await this.alertCtrl.create({
    //     header: "Enter OTP",
    //     backdropDismiss: false,
    //     inputs: [
    //       {
    //         name: "otp",
    //         type: "text",
    //         placeholder: "Enter your otp",
    //       },
    //     ],
    //     buttons: [
    //       {
    //         text: "Enter",
    //         handler: (res) => {
    //           this.authProvider
    //             .enterVerificationCode(res.otp)
    //             .then((userData) => {
    //               // this.showSuccess();
    //               console.log(userData);
    //             });
    //         },
    //       },
    //     ],
    //   });
    //   await alert.present();
    // }
    // // OTP Code
    // onOtpChange(otp) {
    //   this.otp = otp;
    //   // When all 6 digits are filled, trigger OTP validation method
    //   if (otp.length == 6) {
    //     this.validateOtp(otp);
    //   }
    // }
    // async validateOtp(otp) {
    //   // await this.loadingCtrl.dismiss().then(() => {
    //   this.authProvider.enterVerificationCode(otp).then(async (userData) => {
    //     console.log(userData);
    //     // await this.loadingCtrl.dismiss().then(() => {
    //     this.ph.getUserProfile().on("value", (userProfileSnapshot) => {
    //       console.log("USER PROFILE:: ", userProfileSnapshot.val());
    //       const user_details = userProfileSnapshot.val();
    //       console.log("USER PROFILE user_details:: ", user_details);
    //       if (user_details == null) {
    //         let navigationExtras: NavigationExtras = {
    //           queryParams: {
    //             from_phone: "yes",
    //           },
    //         };
    //         // this.navCtrl.navigateRoot("update-users-info");
    //         this.navCtrl.navigateRoot(["update-users-info"], navigationExtras);
    //       } else {
    //         console.log("Im home");
    //         this.ngZone.run(() => {
    //           this.navCtrl.navigateRoot("home");
    //         });
    //       }
    //     });
    //     // this.showSuccess();
    //     //});
    //     // const loading = await this.loadingCtrl.create();
    //     // await loading.present();
    //   });
    //   // const loading = await this.loadingCtrl.create();
    //   // await loading.present();
    // }
    // checkForGPS() { }
    // async loginUser() {
    //   if (!this.loginForm.valid) {
    //     console.log(this.loginForm.value);
    //   } else {
    //     this.authProvider
    //       .loginUser(this.loginForm.value.email, this.loginForm.value.password)
    //       .then(
    //         async (authData) => {
    //           await this.loadingCtrl.dismiss().then(() => {
    //             this.ph.getUserProfile().on("value", (userProfileSnapshot) => {
    //               console.log("USER PROFILE:: ", userProfileSnapshot.val());
    //               const phone = userProfileSnapshot.val().phone;
    //               console.log("USER PROFILE phone:: ", phone);
    //               if (phone == null) {
    //                 this.navCtrl.navigateRoot("update-users-info");
    //               } else {
    //                 console.log("Im home");
    //                 this.ngZone.run(() => {
    //                   this.navCtrl.navigateRoot("home");
    //                 });
    //               }
    //             });
    //           });
    //         },
    //         async (error) => {
    //           this.loadingCtrl.dismiss().then(async () => {
    //             const alert = await this.alertCtrl.create({
    //               message: error.message,
    //               buttons: [
    //                 {
    //                   text: this.lp.translate()[0].accept,
    //                   role: "cancel",
    //                 },
    //               ],
    //             });
    //             alert.present();
    //           });
    //         }
    //       );
    //     const loading = await this.loadingCtrl.create();
    //     await loading.present();
    //   }
    // }
    // async signupUser() {
    //   if (!this.signupForm.valid) {
    //     console.log(this.signupForm.value);
    //   } else {
    //     this.authProvider
    //       .signupUser(this.signupForm.value.email, this.signupForm.value.password)
    //       .then(
    //         async () => {
    //           await this.loadingCtrl.dismiss().then(() => {
    //             if (this.ph.phone == null) {
    //               // this.navCtrl.navigateRoot("phone");
    //               this.navCtrl.navigateRoot("update-users-info");
    //             } else {
    //               this.navCtrl.navigateRoot("");
    //             }
    //           });
    //         },
    //         async (error) => {
    //           this.loadingCtrl.dismiss().then(async () => {
    //             const alert = await this.alertCtrl.create({
    //               message: error.message,
    //               buttons: [
    //                 {
    //                   text: this.lp.translate()[0].accept,
    //                   role: "cancel",
    //                 },
    //               ],
    //             });
    //             alert.present();
    //           });
    //         }
    //       );
    //     const loading = await this.loadingCtrl.create();
    //     await loading.present();
    //   }
    // }
    onSubmit() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            if (!this.loginForm.valid) {
                console.log(this.loginForm.value);
                const alert = yield this.alertCtrl.create({
                    message: "Please enter valid email or password",
                    buttons: [
                        {
                            text: "Cancel",
                            role: "cancel",
                        },
                    ],
                });
                alert.present();
            }
            else {
                this.authProvider
                    .loginUser(this.loginForm.value.email, this.loginForm.value.password)
                    .then(() => {
                    this.loadingCtrl.dismiss().then(() => {
                        console.log("ABOUT TO TRUN HOOMMMe--->>");
                    });
                }, (error) => {
                    this.loadingCtrl.dismiss().then(() => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                        const alert = yield this.alertCtrl.create({
                            message: error.message,
                            buttons: [
                                {
                                    text: "Cancel",
                                    role: "cancel",
                                },
                            ],
                        });
                        alert.present();
                    }));
                });
                const loading = yield this.loadingCtrl.create();
                loading.present();
            }
        });
    }
    signupUser() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            if (!this.signupForm.valid) {
                console.log(this.signupForm.value);
                const alert = yield this.alertCtrl.create({
                    message: "Please enter valid email or password",
                    buttons: [
                        {
                            text: "Cancel",
                            role: "cancel",
                        },
                    ],
                });
                alert.present();
            }
            else {
                this.authProvider
                    .signupUser(this.signupForm.value.email, this.signupForm.value.password)
                    .then(() => {
                    this.loadingCtrl.dismiss().then(() => {
                        console.log("ABOUT TO TRUN SIGNUP MORE--->>");
                        // this.navCtrl.navigateForward("more-info");
                    });
                }, (error) => {
                    this.loadingCtrl.dismiss().then(() => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                        const alert = yield this.alertCtrl.create({
                            message: error.message,
                            buttons: [
                                {
                                    text: "Cancel",
                                    role: "cancel",
                                },
                            ],
                        });
                        alert.present();
                    }));
                });
                const loading = yield this.loadingCtrl.create();
                loading.present();
            }
        });
    }
    signuphere() {
        this.showSignup = true;
        this.showLogin = false;
    }
    loginhere() {
        this.showSignup = false;
        this.showLogin = true;
    }
    goToBack() {
        this.navCtrl.navigateRoot("login-entrance");
    }
    goToSignup() {
        this.navCtrl.navigateRoot("entrance");
    }
    goToResetPassword() {
        this.navCtrl.navigateRoot("reset-password");
    }
    showLoadRefresh() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingCtrl.create({});
            yield loading.present().then(() => {
                let myTimeout = setTimeout(() => {
                    clearTimeout(myTimeout);
                    loading.dismiss();
                }, 400);
            });
        });
    }
    ngOnInit() { }
};
LoginPage.ctorParameters = () => [
    { type: src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_11__["PopUpService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: src_app_services_language_service__WEBPACK_IMPORTED_MODULE_5__["LanguageService"] },
    { type: src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_6__["SettingsService"] },
    { type: src_app_services_native_map_container_service__WEBPACK_IMPORTED_MODULE_8__["NativeMapContainerService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_9__["AuthService"] },
    { type: src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_7__["ProfileService"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("ngOtpInput", { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], LoginPage.prototype, "ngOtpInput", void 0);
LoginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-login",
        template: __webpack_require__(/*! raw-loader!./login.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/login/login.page.html"),
        styles: [__webpack_require__(/*! ./login.page.scss */ "./src/app/pages/login/login.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_11__["PopUpService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
        src_app_services_language_service__WEBPACK_IMPORTED_MODULE_5__["LanguageService"],
        src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_6__["SettingsService"],
        src_app_services_native_map_container_service__WEBPACK_IMPORTED_MODULE_8__["NativeMapContainerService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
        src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_9__["AuthService"],
        src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_7__["ProfileService"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"]])
], LoginPage);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
const environment = {
    production: false,
    CountryJson: [
        {
            name: "Ghana",
            dial_code: "+233",
            code: "GH",
            flag: "/assets/flags/gha.svg",
        },
    ],
};


/***/ })

}]);
//# sourceMappingURL=pages-login-login-module-es2015.js.map