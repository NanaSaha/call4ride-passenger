(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-entrance-entrance-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/entrance/entrance.page.html":
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/entrance/entrance.page.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content class='ion-padding'>\n\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/entrance/entrance.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/entrance/entrance.module.ts ***!
  \***************************************************/
/*! exports provided: EntrancePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EntrancePageModule", function() { return EntrancePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _entrance_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./entrance.page */ "./src/app/pages/entrance/entrance.page.ts");







const routes = [
    {
        path: '',
        component: _entrance_page__WEBPACK_IMPORTED_MODULE_6__["EntrancePage"]
    }
];
let EntrancePageModule = class EntrancePageModule {
};
EntrancePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_entrance_page__WEBPACK_IMPORTED_MODULE_6__["EntrancePage"]]
    })
], EntrancePageModule);



/***/ }),

/***/ "./src/app/pages/entrance/entrance.page.scss":
/*!***************************************************!*\
  !*** ./src/app/pages/entrance/entrance.page.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2VudHJhbmNlL2VudHJhbmNlLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/pages/entrance/entrance.page.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/entrance/entrance.page.ts ***!
  \*************************************************/
/*! exports provided: EntrancePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EntrancePage", function() { return EntrancePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



let EntrancePage = class EntrancePage {
    constructor(navCtrl, load) {
        this.navCtrl = navCtrl;
        this.load = load;
    }
    ngOnInit() {
        this.presentRouteLoader('');
    }
    presentRouteLoader(message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.load.create({
                // tslint:disable-next-line: object-literal-shorthand
                message: message
            });
            loading.present();
            const myTimeout = setTimeout(() => {
                loading.dismiss();
                this.navCtrl.navigateRoot('signup');
                clearTimeout(myTimeout);
            }, 1000);
        });
    }
};
EntrancePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] }
];
EntrancePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-entrance',
        template: __webpack_require__(/*! raw-loader!./entrance.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/entrance/entrance.page.html"),
        styles: [__webpack_require__(/*! ./entrance.page.scss */ "./src/app/pages/entrance/entrance.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]])
], EntrancePage);



/***/ })

}]);
//# sourceMappingURL=pages-entrance-entrance-module-es2015.js.map