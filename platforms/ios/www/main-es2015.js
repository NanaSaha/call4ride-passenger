(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./pages/about/about.module": [
		"./src/app/pages/about/about.module.ts",
		"pages-about-about-module"
	],
	"./pages/accountkit/accountkit.module": [
		"./src/app/pages/accountkit/accountkit.module.ts",
		"pages-accountkit-accountkit-module"
	],
	"./pages/booklater/booklater.module": [
		"./src/app/pages/booklater/booklater.module.ts",
		"default~pages-booklater-booklater-module~pages-estimate-estimate-module~pages-home-home-module~pages~7d935c43",
		"default~pages-booklater-booklater-module~pages-estimate-estimate-module~pages-home-home-module~pages~46d11c0c",
		"pages-booklater-booklater-module"
	],
	"./pages/cancelled/cancelled.module": [
		"./src/app/pages/cancelled/cancelled.module.ts",
		"pages-cancelled-cancelled-module"
	],
	"./pages/card/card.module": [
		"./src/app/pages/card/card.module.ts",
		"pages-card-card-module"
	],
	"./pages/documentdetail/documentdetail.module": [
		"./src/app/pages/documentdetail/documentdetail.module.ts",
		"common",
		"pages-documentdetail-documentdetail-module"
	],
	"./pages/documents/documents.module": [
		"./src/app/pages/documents/documents.module.ts",
		"pages-documents-documents-module"
	],
	"./pages/entrance/entrance.module": [
		"./src/app/pages/entrance/entrance.module.ts",
		"pages-entrance-entrance-module"
	],
	"./pages/estimate/estimate.module": [
		"./src/app/pages/estimate/estimate.module.ts",
		"default~pages-booklater-booklater-module~pages-estimate-estimate-module~pages-home-home-module~pages~7d935c43",
		"default~pages-booklater-booklater-module~pages-estimate-estimate-module~pages-home-home-module~pages~46d11c0c",
		"pages-estimate-estimate-module"
	],
	"./pages/history-details/history-details.module": [
		"./src/app/pages/history-details/history-details.module.ts",
		"pages-history-details-history-details-module"
	],
	"./pages/history/history.module": [
		"./src/app/pages/history/history.module.ts",
		"pages-history-history-module"
	],
	"./pages/home/home.module": [
		"./src/app/pages/home/home.module.ts",
		"default~pages-booklater-booklater-module~pages-estimate-estimate-module~pages-home-home-module~pages~7d935c43",
		"default~pages-booklater-booklater-module~pages-estimate-estimate-module~pages-home-home-module~pages~46d11c0c",
		"default~pages-home-home-module~someone-someone-module",
		"common",
		"pages-home-home-module"
	],
	"./pages/intro/intro.module": [
		"./src/app/pages/intro/intro.module.ts",
		"pages-intro-intro-module"
	],
	"./pages/login-entrance/login-entrance.module": [
		"./src/app/pages/login-entrance/login-entrance.module.ts",
		"pages-login-entrance-login-entrance-module"
	],
	"./pages/login/login.module": [
		"./src/app/pages/login/login.module.ts",
		"common",
		"pages-login-login-module"
	],
	"./pages/news/news.module": [
		"./src/app/pages/news/news.module.ts",
		"pages-news-news-module"
	],
	"./pages/payment-approval/payment-approval.module": [
		"./src/app/pages/payment-approval/payment-approval.module.ts",
		"pages-payment-approval-payment-approval-module"
	],
	"./pages/payment/payment.module": [
		"./src/app/pages/payment/payment.module.ts",
		"pages-payment-payment-module"
	],
	"./pages/phone/phone.module": [
		"./src/app/pages/phone/phone.module.ts",
		"pages-phone-phone-module"
	],
	"./pages/profile/profile.module": [
		"./src/app/pages/profile/profile.module.ts",
		"default~pages-booklater-booklater-module~pages-estimate-estimate-module~pages-home-home-module~pages~46d11c0c",
		"common",
		"pages-profile-profile-module"
	],
	"./pages/promo/promo.module": [
		"./src/app/pages/promo/promo.module.ts",
		"pages-promo-promo-module"
	],
	"./pages/referalcode/referalcode.module": [
		"./src/app/pages/referalcode/referalcode.module.ts",
		"pages-referalcode-referalcode-module"
	],
	"./pages/referride/referride.module": [
		"./src/app/pages/referride/referride.module.ts",
		"pages-referride-referride-module"
	],
	"./pages/reset-password/reset-password.module": [
		"./src/app/pages/reset-password/reset-password.module.ts",
		"common",
		"pages-reset-password-reset-password-module"
	],
	"./pages/route/route.module": [
		"./src/app/pages/route/route.module.ts",
		"default~pages-booklater-booklater-module~pages-estimate-estimate-module~pages-home-home-module~pages~7d935c43",
		"common",
		"pages-route-route-module"
	],
	"./pages/schedule/schedule.module": [
		"./src/app/pages/schedule/schedule.module.ts",
		"default~pages-booklater-booklater-module~pages-estimate-estimate-module~pages-home-home-module~pages~7d935c43",
		"default~pages-booklater-booklater-module~pages-estimate-estimate-module~pages-home-home-module~pages~46d11c0c",
		"pages-schedule-schedule-module"
	],
	"./pages/settings/settings.module": [
		"./src/app/pages/settings/settings.module.ts",
		"pages-settings-settings-module"
	],
	"./pages/signup/signup.module": [
		"./src/app/pages/signup/signup.module.ts",
		"common",
		"pages-signup-signup-module"
	],
	"./pages/support/support.module": [
		"./src/app/pages/support/support.module.ts",
		"pages-support-support-module"
	],
	"./someone/someone.module": [
		"./src/app/someone/someone.module.ts",
		"default~pages-home-home-module~someone-someone-module",
		"someone-someone-module"
	],
	"./splash/splash.module": [
		"./src/app/splash/splash.module.ts",
		"splash-splash-module"
	],
	"./update-users-info/update-users-info.module": [
		"./src/app/update-users-info/update-users-info.module.ts",
		"update-users-info-update-users-info-module"
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./node_modules/@ionic/core/dist/esm lazy recursive ^\\.\\/.*\\.entry\\.js$ include: \\.entry\\.js$ exclude: \\.system\\.entry\\.js$":
/*!*****************************************************************************************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm lazy ^\.\/.*\.entry\.js$ include: \.entry\.js$ exclude: \.system\.entry\.js$ namespace object ***!
  \*****************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./ion-action-sheet-controller_8.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-action-sheet-controller_8.entry.js",
		"common",
		0
	],
	"./ion-action-sheet-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-action-sheet-ios.entry.js",
		"common",
		1
	],
	"./ion-action-sheet-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-action-sheet-md.entry.js",
		"common",
		2
	],
	"./ion-alert-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-alert-ios.entry.js",
		"common",
		3
	],
	"./ion-alert-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-alert-md.entry.js",
		"common",
		4
	],
	"./ion-app_8-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-app_8-ios.entry.js",
		"common",
		5
	],
	"./ion-app_8-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-app_8-md.entry.js",
		"common",
		6
	],
	"./ion-avatar_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-avatar_3-ios.entry.js",
		"common",
		7
	],
	"./ion-avatar_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-avatar_3-md.entry.js",
		"common",
		8
	],
	"./ion-back-button-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-back-button-ios.entry.js",
		"common",
		9
	],
	"./ion-back-button-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-back-button-md.entry.js",
		"common",
		10
	],
	"./ion-backdrop-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-backdrop-ios.entry.js",
		11
	],
	"./ion-backdrop-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-backdrop-md.entry.js",
		12
	],
	"./ion-button_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-button_2-ios.entry.js",
		"common",
		13
	],
	"./ion-button_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-button_2-md.entry.js",
		"common",
		14
	],
	"./ion-card_5-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-card_5-ios.entry.js",
		"common",
		15
	],
	"./ion-card_5-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-card_5-md.entry.js",
		"common",
		16
	],
	"./ion-checkbox-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-checkbox-ios.entry.js",
		"common",
		17
	],
	"./ion-checkbox-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-checkbox-md.entry.js",
		"common",
		18
	],
	"./ion-chip-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-chip-ios.entry.js",
		"common",
		19
	],
	"./ion-chip-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-chip-md.entry.js",
		"common",
		20
	],
	"./ion-col_3.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-col_3.entry.js",
		21
	],
	"./ion-datetime_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-datetime_3-ios.entry.js",
		"common",
		22
	],
	"./ion-datetime_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-datetime_3-md.entry.js",
		"common",
		23
	],
	"./ion-fab_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-fab_3-ios.entry.js",
		"common",
		24
	],
	"./ion-fab_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-fab_3-md.entry.js",
		"common",
		25
	],
	"./ion-img.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-img.entry.js",
		26
	],
	"./ion-infinite-scroll_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-infinite-scroll_2-ios.entry.js",
		"common",
		27
	],
	"./ion-infinite-scroll_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-infinite-scroll_2-md.entry.js",
		"common",
		28
	],
	"./ion-input-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-input-ios.entry.js",
		"common",
		29
	],
	"./ion-input-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-input-md.entry.js",
		"common",
		30
	],
	"./ion-item-option_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-item-option_3-ios.entry.js",
		"common",
		31
	],
	"./ion-item-option_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-item-option_3-md.entry.js",
		"common",
		32
	],
	"./ion-item_8-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-item_8-ios.entry.js",
		"common",
		33
	],
	"./ion-item_8-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-item_8-md.entry.js",
		"common",
		34
	],
	"./ion-loading-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-loading-ios.entry.js",
		"common",
		35
	],
	"./ion-loading-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-loading-md.entry.js",
		"common",
		36
	],
	"./ion-menu_4-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-menu_4-ios.entry.js",
		"common",
		37
	],
	"./ion-menu_4-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-menu_4-md.entry.js",
		"common",
		38
	],
	"./ion-modal-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-modal-ios.entry.js",
		"common",
		39
	],
	"./ion-modal-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-modal-md.entry.js",
		"common",
		40
	],
	"./ion-nav_5.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-nav_5.entry.js",
		"common",
		41
	],
	"./ion-popover-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-popover-ios.entry.js",
		"common",
		42
	],
	"./ion-popover-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-popover-md.entry.js",
		"common",
		43
	],
	"./ion-progress-bar-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-progress-bar-ios.entry.js",
		"common",
		44
	],
	"./ion-progress-bar-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-progress-bar-md.entry.js",
		"common",
		45
	],
	"./ion-radio_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-radio_2-ios.entry.js",
		"common",
		46
	],
	"./ion-radio_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-radio_2-md.entry.js",
		"common",
		47
	],
	"./ion-range-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-range-ios.entry.js",
		"common",
		48
	],
	"./ion-range-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-range-md.entry.js",
		"common",
		49
	],
	"./ion-refresher_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-refresher_2-ios.entry.js",
		"common",
		50
	],
	"./ion-refresher_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-refresher_2-md.entry.js",
		"common",
		51
	],
	"./ion-reorder_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-reorder_2-ios.entry.js",
		"common",
		52
	],
	"./ion-reorder_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-reorder_2-md.entry.js",
		"common",
		53
	],
	"./ion-ripple-effect.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-ripple-effect.entry.js",
		54
	],
	"./ion-route_4.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-route_4.entry.js",
		"common",
		55
	],
	"./ion-searchbar-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-searchbar-ios.entry.js",
		"common",
		56
	],
	"./ion-searchbar-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-searchbar-md.entry.js",
		"common",
		57
	],
	"./ion-segment_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-segment_2-ios.entry.js",
		"common",
		58
	],
	"./ion-segment_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-segment_2-md.entry.js",
		"common",
		59
	],
	"./ion-select_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-select_3-ios.entry.js",
		"common",
		60
	],
	"./ion-select_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-select_3-md.entry.js",
		"common",
		61
	],
	"./ion-slide_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-slide_2-ios.entry.js",
		62
	],
	"./ion-slide_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-slide_2-md.entry.js",
		63
	],
	"./ion-spinner.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-spinner.entry.js",
		"common",
		64
	],
	"./ion-split-pane-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-split-pane-ios.entry.js",
		65
	],
	"./ion-split-pane-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-split-pane-md.entry.js",
		66
	],
	"./ion-tab-bar_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-tab-bar_2-ios.entry.js",
		"common",
		67
	],
	"./ion-tab-bar_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-tab-bar_2-md.entry.js",
		"common",
		68
	],
	"./ion-tab_2.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-tab_2.entry.js",
		"common",
		69
	],
	"./ion-text.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-text.entry.js",
		"common",
		70
	],
	"./ion-textarea-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-textarea-ios.entry.js",
		"common",
		71
	],
	"./ion-textarea-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-textarea-md.entry.js",
		"common",
		72
	],
	"./ion-toast-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-toast-ios.entry.js",
		"common",
		73
	],
	"./ion-toast-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-toast-md.entry.js",
		"common",
		74
	],
	"./ion-toggle-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-toggle-ios.entry.js",
		"common",
		75
	],
	"./ion-toggle-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-toggle-md.entry.js",
		"common",
		76
	],
	"./ion-virtual-scroll.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-virtual-scroll.entry.js",
		77
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./node_modules/@ionic/core/dist/esm lazy recursive ^\\.\\/.*\\.entry\\.js$ include: \\.entry\\.js$ exclude: \\.system\\.entry\\.js$";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-app>\n  <ion-split-pane contentId=\"main-content\">\n    <ion-menu\n      type=\"overlay\"\n      contentId=\"main-content\"\n      (ionWillOpen)=\"menuOpened()\"\n      (ionDrag)=\"menuOpened()\"\n      (ionDidClose)=\"menuClosed()\"\n    >\n      <ion-menu-toggle auto-hide=\"false\">\n        <ion-header no-border routerLink=\"/profile\" style=\"margin-top: 40px\">\n          <!-- <ion-toolbar> -->\n          <!-- <ion-item *ngIf=\"profileServ.userID\" lines=\"none\">\n            <div class=\"resultContainer\">\n              <div style=\"display: inline\">\n                <div class=\"img-wrapper\" *ngIf=\"!picture\">\n                  <ion-icon name=\"contact\" id=\"drivericonSize\"></ion-icon>\n                </div>\n                <div class=\"img-wrapper\" *ngIf=\"picture\">\n                  <ion-avatar>\n                    <img [src]=\"picture\" id=\"drivericonSize\" />\n                  </ion-avatar>\n                </div>\n                <div class=\"content-wrap\" *ngIf=\"first_name\">\n                  <span class=\"bookTitle\">\n                    {{ first_name }} {{ last_name }}</span\n                  >\n                  <br />\n                  <span class=\"bookTitle\" id=\"editPro\" (click)=\"goProfile()\">{{\n                    unique\n                  }}</span>\n                </div>\n\n                <div class=\"content-wrap\" *ngIf=\"!first_name\">\n                  <span class=\"bookTitle\"> Sign In </span>\n                </div>\n              </div>\n            </div>\n          </ion-item> -->\n\n          <ion-item *ngIf=\"profileServ.userID\" lines=\"none\">\n            <ion-grid>\n              <ion-row>\n                <ion-col size=\"12\" routerLink=\"/profile\">\n                  <ion-avatar style=\"\n    position: absolute;\">\n                    <ion-icon\n                      name=\"contact\"\n                      id=\"drivericonSize\"\n                      *ngIf=\"!picture\" style=\"text-align: center;\n    position: absolute;\"\n                    ></ion-icon>\n                    <img *ngIf=\"picture\" [src]=\"picture\" />\n                  </ion-avatar><br>\n                  <ion-label *ngIf=\"first_name\" style=\"text-align: center;\">\n                    <ion-label *ngIf=\"first_name\" style=\"text-align: center;\" >\n                      <h4>{{ first_name }}</h4>\n                  \n                      <span class=\"bookTitle\" id=\"editPro\" style=\"text-align: center;\">{{ unique }}</span> \n                    </ion-label>\n                  </ion-label>\n                  \n                </ion-col>\n                <!-- <ion-col size=\"8\" routerLink=\"/profile\">\n                  <ion-label *ngIf=\"first_name\" class=\"ion-text-right\">\n                    <ion-label *ngIf=\"first_name\" class=\"ion-text-right\">\n                      <h4>{{ first_name }}</h4>\n\n                    </ion-label>\n                  </ion-label>\n                </ion-col> -->\n              </ion-row>\n            </ion-grid>\n          </ion-item>\n\n          <!-- </ion-toolbar> -->\n        </ion-header>\n      </ion-menu-toggle>\n      <ion-content>\n        <ion-item-divider></ion-item-divider>\n        <ion-list lines=\"none\" style=\"margin: 30px\">\n          <ion-menu-toggle auto-hide=\"false\">\n            <ion-item [routerLink]=\"[appPages[0].url]\">\n              <ion-icon slot=\"start\" [name]=\"appPages[0].icon\"></ion-icon>\n              <ion-label>\n                <h1>{{ appPages[0].title }}</h1>\n              </ion-label>\n            </ion-item>\n\n            <ion-item [routerLink]=\"[appPages[1].url]\">\n              <ion-icon slot=\"start\" [name]=\"appPages[1].icon\"></ion-icon>\n              <ion-label>\n                <h1>{{ appPages[1].title }}</h1>\n              </ion-label>\n            </ion-item>\n            <ion-item [routerLink]=\"[appPages[2].url]\">\n              <ion-icon slot=\"start\" [name]=\"appPages[2].icon\"></ion-icon>\n              <ion-label>\n                <h1>{{ appPages[2].title }}</h1>\n              </ion-label>\n            </ion-item>\n            <ion-item [routerLink]=\"[appPages[3].url]\">\n              <ion-icon slot=\"start\" [name]=\"appPages[3].icon\"></ion-icon>\n              <ion-label>\n                <h1>{{ appPages[3].title }}</h1>\n              </ion-label>\n            </ion-item>\n            <ion-item [routerLink]=\"[appPages[4].url]\">\n              <ion-icon slot=\"start\" [name]=\"appPages[4].icon\"></ion-icon>\n              <ion-label>\n                <h1>{{ appPages[4].title }}</h1>\n              </ion-label>\n            </ion-item>\n\n            <ion-item [routerLink]=\"[appPages[5].url]\">\n              <ion-icon slot=\"start\" [name]=\"appPages[5].icon\"></ion-icon>\n              <ion-label>\n                <h1>{{ appPages[5].title }}</h1>\n              </ion-label>\n            </ion-item>\n\n            <!-- <ion-item [routerLink]=\"[appPages[6].url]\">\n              <ion-icon slot=\"start\" [name]=\"appPages[6].icon\"></ion-icon>\n              <ion-label>\n                <h1>{{ appPages[6].title }}</h1>\n              </ion-label>\n            </ion-item> -->\n\n            <ion-footer style=\"margin-top: 100%\">\n              <ion-list>\n                <ion-item routerLink=\"/about\">\n                  <!-- <ion-icon name=\"information-circle-outline\"></ion-icon> -->\n                  <ion-label style=\"padding-left: 20px\">\n                    <p>Version 1.0.0</p>\n                  </ion-label>\n                </ion-item>\n              </ion-list>\n            </ion-footer>\n          </ion-menu-toggle>\n        </ion-list>\n      </ion-content>\n    </ion-menu>\n    <ion-router-outlet main id=\"main-content\"></ion-router-outlet>\n  </ion-split-pane>\n</ion-app>\n"

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



const routes = [
    {
        path: "home",
        redirectTo: "home",
        pathMatch: "full",
    },
    { path: "about", loadChildren: "./pages/about/about.module#AboutPageModule" },
    {
        path: "accountkit",
        loadChildren: "./pages/accountkit/accountkit.module#AccountkitPageModule",
    },
    // {
    //   path: "autocomplete",
    //   loadChildren:
    //     "./pages/autocomplete/autocomplete.module#AutocompletePageModule",
    // },
    {
        path: "booklater",
        loadChildren: "./pages/booklater/booklater.module#BooklaterPageModule",
    },
    { path: "card", loadChildren: "./pages/card/card.module#CardPageModule" },
    // { path: "chat", loadChildren: "./pages/chat/chat.module#ChatPageModule" },
    {
        path: "documentdetail",
        loadChildren: "./pages/documentdetail/documentdetail.module#DocumentdetailPageModule",
    },
    {
        path: "documents",
        loadChildren: "./pages/documents/documents.module#DocumentsPageModule",
    },
    {
        path: "entrance",
        loadChildren: "./pages/entrance/entrance.module#EntrancePageModule",
    },
    {
        path: "estimate",
        loadChildren: "./pages/estimate/estimate.module#EstimatePageModule",
    },
    {
        path: "history",
        loadChildren: "./pages/history/history.module#HistoryPageModule",
    },
    {
        path: "history-details",
        loadChildren: "./pages/history-details/history-details.module#HistoryDetailsPageModule",
    },
    { path: "intro", loadChildren: "./pages/intro/intro.module#IntroPageModule" },
    { path: "login", loadChildren: "./pages/login/login.module#LoginPageModule" },
    {
        path: "login-entrance",
        loadChildren: "./pages/login-entrance/login-entrance.module#LoginEntrancePageModule",
    },
    { path: "news", loadChildren: "./pages/news/news.module#NewsPageModule" },
    {
        path: "payment",
        loadChildren: "./pages/payment/payment.module#PaymentPageModule",
    },
    {
        path: "payment-approval",
        loadChildren: "./pages/payment-approval/payment-approval.module#PaymentApprovalPageModule",
    },
    { path: "phone", loadChildren: "./pages/phone/phone.module#PhonePageModule" },
    { path: "promo", loadChildren: "./pages/promo/promo.module#PromoPageModule" },
    // { path: "rate", loadChildren: "./pages/rate/rate.module#RatePageModule" },
    {
        path: "referalcode",
        loadChildren: "./pages/referalcode/referalcode.module#ReferalcodePageModule",
    },
    {
        path: "referride",
        loadChildren: "./pages/referride/referride.module#ReferridePageModule",
    },
    {
        path: "reset-password",
        loadChildren: "./pages/reset-password/reset-password.module#ResetPasswordPageModule",
    },
    {
        path: "schedule",
        loadChildren: "./pages/schedule/schedule.module#SchedulePageModule",
    },
    {
        path: "settings",
        loadChildren: "./pages/settings/settings.module#SettingsPageModule",
    },
    {
        path: "signup",
        loadChildren: "./pages/signup/signup.module#SignupPageModule",
    },
    { path: "home", loadChildren: "./pages/home/home.module#HomePageModule" },
    {
        path: "support",
        loadChildren: "./pages/support/support.module#SupportPageModule",
    },
    {
        path: "profile",
        loadChildren: "./pages/profile/profile.module#ProfilePageModule",
    },
    {
        path: "cancelled",
        loadChildren: "./pages/cancelled/cancelled.module#CancelledPageModule",
    },
    { path: "route", loadChildren: "./pages/route/route.module#RoutePageModule" },
    // {
    //   path: "driver-info",
    //   loadChildren: "./pages/driver-info/driver-info.module#DriverInfoPageModule",
    // },
    // {
    //   path: "trip-info",
    //   loadChildren: "./pages/trip-info/trip-info.module#TripInfoPageModule",
    // },
    {
        path: "update-users-info",
        loadChildren: "./update-users-info/update-users-info.module#UpdateUsersInfoPageModule",
    },
    { path: 'splash', loadChildren: './splash/splash.module#SplashPageModule' },
    { path: 'someone', loadChildren: './someone/someone.module#SomeonePageModule' },
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, { preloadingStrategy: _angular_router__WEBPACK_IMPORTED_MODULE_2__["PreloadAllModules"] }),
        ],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-label {\n  font-size: 40px;\n}\n\nh4,\nh1,\nion-icon {\n  color: black;\n}\n\nion-item {\n  padding-top: 0 px;\n}\n\n#iconSize {\n  font-size: 60px !important;\n}\n\n.resultContainer {\n  width: 100%;\n  position: relative;\n  left: 0;\n  margin-top: 30px;\n  margin-bottom: 20px;\n  padding: 10px;\n}\n\n.bookImage,\n.bookTitle,\n.bookPrice {\n  margin-left: 20px;\n}\n\n#editPro {\n  color: #fbb91d;\n  font-size: 13px;\n}\n\n.content-wrap,\n.img-wrapper {\n  display: inline-block;\n}\n\n.sc-ion-label-md-s h1 {\n  font-size: 16px !important;\n}\n\n#drivericonSize {\n  font-size: 70px !important;\n}\n\n@font-face {\n  font-family: \"Montserrat\";\n  font-style: normal;\n  font-weight: normal;\n  src: url('Montserrat-Regular.ttf');\n}\n\n@font-face {\n  font-family: \"Montserrat\";\n  font-style: normal;\n  font-weight: bold;\n  src: url('Montserrat-Bold.ttf');\n}\n\n@font-face {\n  font-family: \"Montserrat\";\n  font-style: italic;\n  font-weight: normal;\n  src: url('Montserrat-Italic.ttf');\n}\n\n@font-face {\n  font-family: \"Montserratt\";\n  font-style: italic;\n  font-weight: bold;\n  src: url('Montserrat-Light.ttf');\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYWhhL0lvbmljQXBwcy9DNFJpZGUtSU9TLTIwMjQvY2FsbDRyaWRlLXBhc3Nlbmdlci9zcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvYXBwLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsZUFBQTtBQ0NGOztBREVBOzs7RUFHRSxZQUFBO0FDQ0Y7O0FERUE7RUFFRSxpQkFBQTtBQ0FGOztBREdBO0VBQ0UsMEJBQUE7QUNBRjs7QURHQTtFQUNFLFdBQUE7RUFDQSxrQkFBQTtFQUNBLE9BQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtBQ0FGOztBREdBOzs7RUFHRSxpQkFBQTtBQ0FGOztBREdBO0VBQ0UsY0FBQTtFQUNBLGVBQUE7QUNBRjs7QURHQTs7RUFFRSxxQkFBQTtBQ0FGOztBREdBO0VBQ0UsMEJBQUE7QUNBRjs7QURHQTtFQUdFLDBCQUFBO0FDRkY7O0FEUUU7RUFDRSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQ0FBQTtBQ0xKOztBRFFFO0VBQ0UseUJBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsK0JBQUE7QUNOSjs7QURTRTtFQUNFLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLGlDQUFBO0FDUEo7O0FEVUU7RUFDRSwwQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQ0FBQTtBQ1JKIiwiZmlsZSI6InNyYy9hcHAvYXBwLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWxhYmVsIHtcclxuICBmb250LXNpemU6IDQwcHg7XHJcbn1cclxuXHJcbmg0LFxyXG5oMSxcclxuaW9uLWljb24ge1xyXG4gIGNvbG9yOiByZ2IoMCwgMCwgMCk7XHJcbn1cclxuXHJcbmlvbi1pdGVtIHtcclxuICAvLyBwYWRkaW5nLXRvcDogMTVweDtcclxuICBwYWRkaW5nLXRvcDogMCBweDtcclxufVxyXG5cclxuI2ljb25TaXplIHtcclxuICBmb250LXNpemU6IDYwcHggIWltcG9ydGFudDtcclxufVxyXG5cclxuLnJlc3VsdENvbnRhaW5lciB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIGxlZnQ6IDA7XHJcbiAgbWFyZ2luLXRvcDogMzBweDtcclxuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG4gIHBhZGRpbmc6IDEwcHg7XHJcbn1cclxuXHJcbi5ib29rSW1hZ2UsXHJcbi5ib29rVGl0bGUsXHJcbi5ib29rUHJpY2Uge1xyXG4gIG1hcmdpbi1sZWZ0OiAyMHB4O1xyXG59XHJcblxyXG4jZWRpdFBybyB7XHJcbiAgY29sb3I6IHJnYmEoMjUxLCAxODUsIDI5LCAxKTtcclxuICBmb250LXNpemU6IDEzcHhcclxufVxyXG5cclxuLmNvbnRlbnQtd3JhcCxcclxuLmltZy13cmFwcGVyIHtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbn1cclxuXHJcbi5zYy1pb24tbGFiZWwtbWQtcyBoMSB7XHJcbiAgZm9udC1zaXplOiAxNnB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbiNkcml2ZXJpY29uU2l6ZSB7XHJcbiAgLy8gZm9udC1zaXplOiA3MHB4ICFpbXBvcnRhbnQ7XHJcbiAgLy8gd2lkdGg6IDEwMHB4ICFpbXBvcnRhbnQ7XHJcbiAgZm9udC1zaXplOiA3MHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcblxyXG5cclxuXHJcbiAgQGZvbnQtZmFjZSB7XHJcbiAgICBmb250LWZhbWlseTogXCJNb250c2VycmF0XCI7XHJcbiAgICBmb250LXN0eWxlOiBub3JtYWw7XHJcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xyXG4gICAgc3JjOiB1cmwoXCIuLi9hc3NldHMvZm9udHMvTW9udHNlcnJhdC1SZWd1bGFyLnR0ZlwiKTtcclxuICB9XHJcblxyXG4gIEBmb250LWZhY2Uge1xyXG4gICAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdFwiO1xyXG4gICAgZm9udC1zdHlsZTogbm9ybWFsO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBzcmM6IHVybChcIi4uL2Fzc2V0cy9mb250cy9Nb250c2VycmF0LUJvbGQudHRmXCIpO1xyXG4gIH1cclxuXHJcbiAgQGZvbnQtZmFjZSB7XHJcbiAgICBmb250LWZhbWlseTogXCJNb250c2VycmF0XCI7XHJcbiAgICBmb250LXN0eWxlOiBpdGFsaWM7XHJcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xyXG4gICAgc3JjOiB1cmwoXCIuLi9hc3NldHMvZm9udHMvTW9udHNlcnJhdC1JdGFsaWMudHRmXCIpO1xyXG4gIH1cclxuXHJcbiAgQGZvbnQtZmFjZSB7XHJcbiAgICBmb250LWZhbWlseTogXCJNb250c2VycmF0dFwiO1xyXG4gICAgZm9udC1zdHlsZTogaXRhbGljO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBzcmM6IHVybChcIi4uL2Fzc2V0cy9mb250cy9Nb250c2VycmF0LUxpZ2h0LnR0ZlwiKTtcclxuICB9XHJcblxyXG4iLCJpb24tbGFiZWwge1xuICBmb250LXNpemU6IDQwcHg7XG59XG5cbmg0LFxuaDEsXG5pb24taWNvbiB7XG4gIGNvbG9yOiBibGFjaztcbn1cblxuaW9uLWl0ZW0ge1xuICBwYWRkaW5nLXRvcDogMCBweDtcbn1cblxuI2ljb25TaXplIHtcbiAgZm9udC1zaXplOiA2MHB4ICFpbXBvcnRhbnQ7XG59XG5cbi5yZXN1bHRDb250YWluZXIge1xuICB3aWR0aDogMTAwJTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBsZWZ0OiAwO1xuICBtYXJnaW4tdG9wOiAzMHB4O1xuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xuICBwYWRkaW5nOiAxMHB4O1xufVxuXG4uYm9va0ltYWdlLFxuLmJvb2tUaXRsZSxcbi5ib29rUHJpY2Uge1xuICBtYXJnaW4tbGVmdDogMjBweDtcbn1cblxuI2VkaXRQcm8ge1xuICBjb2xvcjogI2ZiYjkxZDtcbiAgZm9udC1zaXplOiAxM3B4O1xufVxuXG4uY29udGVudC13cmFwLFxuLmltZy13cmFwcGVyIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xufVxuXG4uc2MtaW9uLWxhYmVsLW1kLXMgaDEge1xuICBmb250LXNpemU6IDE2cHggIWltcG9ydGFudDtcbn1cblxuI2RyaXZlcmljb25TaXplIHtcbiAgZm9udC1zaXplOiA3MHB4ICFpbXBvcnRhbnQ7XG59XG5cbkBmb250LWZhY2Uge1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0XCI7XG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgc3JjOiB1cmwoXCIuLi9hc3NldHMvZm9udHMvTW9udHNlcnJhdC1SZWd1bGFyLnR0ZlwiKTtcbn1cbkBmb250LWZhY2Uge1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0XCI7XG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIHNyYzogdXJsKFwiLi4vYXNzZXRzL2ZvbnRzL01vbnRzZXJyYXQtQm9sZC50dGZcIik7XG59XG5AZm9udC1mYWNlIHtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdFwiO1xuICBmb250LXN0eWxlOiBpdGFsaWM7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gIHNyYzogdXJsKFwiLi4vYXNzZXRzL2ZvbnRzL01vbnRzZXJyYXQtSXRhbGljLnR0ZlwiKTtcbn1cbkBmb250LWZhY2Uge1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0dFwiO1xuICBmb250LXN0eWxlOiBpdGFsaWM7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBzcmM6IHVybChcIi4uL2Fzc2V0cy9mb250cy9Nb250c2VycmF0LUxpZ2h0LnR0ZlwiKTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/splash-screen/ngx */ "./node_modules/@ionic-native/splash-screen/ngx/index.js");
/* harmony import */ var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/status-bar/ngx */ "./node_modules/@ionic-native/status-bar/ngx/index.js");
/* harmony import */ var _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/onesignal/ngx */ "./node_modules/@ionic-native/onesignal/ngx/index.js");
/* harmony import */ var _services_settings_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./services/settings.service */ "./src/app/services/settings.service.ts");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _services_profile_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./services/profile.service */ "./src/app/services/profile.service.ts");
/* harmony import */ var _services_language_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./services/language.service */ "./src/app/services/language.service.ts");
/* harmony import */ var _services_native_map_container_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./services/native-map-container.service */ "./src/app/services/native-map-container.service.ts");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _services_pop_up_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./services/pop-up.service */ "./src/app/services/pop-up.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");















let AppComponent = class AppComponent {
    constructor(platform, splashScreen, pop, navCtrl, statusBar, zone, set, cMap, lp, modalCtrl, loadingCtrl, oneSignal, profileServ, alertCtrl, auth, router, toastCtrl, geo) {
        this.platform = platform;
        this.splashScreen = splashScreen;
        this.pop = pop;
        this.navCtrl = navCtrl;
        this.statusBar = statusBar;
        this.zone = zone;
        this.set = set;
        this.cMap = cMap;
        this.lp = lp;
        this.modalCtrl = modalCtrl;
        this.loadingCtrl = loadingCtrl;
        this.oneSignal = oneSignal;
        this.profileServ = profileServ;
        this.alertCtrl = alertCtrl;
        this.auth = auth;
        this.router = router;
        this.toastCtrl = toastCtrl;
        this.geo = geo;
        this.showSplash = true;
        this.appPages = [
            {
                title: "Home",
                url: "/home",
                icon: "ios-home",
            },
            {
                title: "Trip History",
                url: "/history",
                icon: "ios-clock",
            },
            {
                title: "Promo Code",
                url: "/referalcode",
                icon: "pricetags",
            },
            {
                title: "Referal Code",
                url: "/promo",
                icon: "ribbon",
            },
            // {
            //   title: "Payment",
            //   url: "/payment",
            //   icon: "ios-card",
            // },
            {
                title: "Support",
                url: "/support",
                icon: "ios-chatbubbles",
            },
            // {
            //   title: "Language",
            //   url: "/settings",
            //   icon: "ios-settings",
            // },
            {
                title: "Call4Ride Info",
                url: "/about",
                icon: "information-circle-outline",
            },
        ];
        this.initializeApp();
    }
    initializeApp() {
        ///initialize onesignal notification here
        this.platform.ready().then(() => {
            // this.foregroundService.start('Call4Ride Rider', 'Requesting A Ride', 'ic_launcher');
            // this.powerManagement.dim().then(
            //   res => console.log('Wakelock acquired'),
            //   () => {
            //     console.log('Failed to acquire wakelock');
            //   }
            // );
            // this.powerManagement.acquire().then(
            //   res => console.log('Wakelock acquired'),
            //   () => {
            //     console.log('Failed to acquire wakelock');
            //   }
            // );
            // this.powerManagement.setReleaseOnPause(false).then(
            //   res => console.log('setReleaseOnPause successfully'),
            //   () => {
            //     console.log('Failed to set');
            //   }
            // ); 
            this.statusBar.styleDefault();
            this.splashScreen.hide();
            this.allowLocation();
            this.startUp();
            if (this.platform.is("cordova")) {
                this.setupPush();
                this.getNotificationPlayerIds()
                    .then((ids) => {
                })
                    .catch((e) => {
                });
            }
            // this.keepScreenAlive();
            // this.backgroundMode.enable();
        });
    }
    setupPush() {
        // I recommend to put these into your environment.ts
        this.oneSignal.startInit("8113f114-14de-4ee6-8ab2-4ec3cfb5a90a", "516821551729");
        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.None);
        // Notifcation was received in general
        this.oneSignal.handleNotificationReceived().subscribe((data) => {
            let msg = data.payload.body;
            let title = data.payload.title;
            let additionalData = data.payload.additionalData;
            this.showAlert(title, msg, additionalData.task);
        });
        // Notification was really clicked/opened
        this.oneSignal.handleNotificationOpened().subscribe((data) => {
            // Just a note that the data is a different place here!
            let additionalData = data.notification.payload.additionalData;
            this.showAlert("Notification opened", "You already read this before", additionalData.task);
        });
        this.oneSignal.endInit();
    }
    // keepScreenAlive() {
    //   this.insomnia.keepAwake()
    //     .then(
    //       () => console.log('KEEPING AWAKE success'),
    //       () => console.log('KEEPING AWAKE error')
    //     );
    // }
    getNotificationPlayerIds() {
        return new Promise((resolve, reject) => {
            if (this.platform.is("cordova")) {
                this.oneSignal
                    .getIds()
                    .then((ids) => {
                    resolve(ids);
                })
                    .catch((e) => {
                    reject(e);
                });
            }
        });
    }
    startUp() {
        const unsubscribe = firebase__WEBPACK_IMPORTED_MODULE_11___default.a.auth().onAuthStateChanged((user) => {
            if (!user) {
                // if this is not a user then show entrance scene and hide status bar
                this.navCtrl.navigateRoot("splash");
                this.profileServ.login = true;
                unsubscribe();
            }
            else {
                unsubscribe();
                // Check If the connection is okay or bad.
                this.profileServ
                    .getUserProfil()
                    .child(this.profileServ.id)
                    .on("value", (userProfileSnapshot) => {
                    if (userProfileSnapshot.val() == null) {
                        this.auth.logoutUser().then(() => {
                            this.navCtrl.navigateRoot("login");
                        });
                    }
                    this.profileServ
                        .getUserProfil()
                        .child(this.profileServ.id)
                        .on("child_added", (userProfileSnapshot) => {
                        this.first_name = userProfileSnapshot.val().first_name;
                        this.email = userProfileSnapshot.val().email;
                        this.phone = userProfileSnapshot.val().phonenumber;
                        if (this.phone) {
                            if (this.first_name != null || this.first_name != undefined) {
                                this.navCtrl.navigateForward("home");
                            }
                            else {
                                this.navCtrl.navigateRoot("update-users-info");
                            }
                        }
                        else {
                            this.navCtrl.navigateRoot("update-users-info");
                        }
                    });
                    this.profileServ
                        .getUserProfil()
                        .child(this.profileServ.id)
                        .off("value");
                });
            }
        });
    }
    menuOpened() {
        this.profileServ
            .getUserProfil()
            .child(this.profileServ.id)
            .on("child_added", (userProfileSnapshot) => {
            this.first_name = userProfileSnapshot.val().first_name;
            this.email = userProfileSnapshot.val().email;
            this.unique = userProfileSnapshot.val().unique_number;
        });
    }
    menuClosed() { }
    showAlert(title, msg, task) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertCtrl.create({
                header: title,
                subHeader: msg,
                buttons: [
                    {
                        text: `Action: ${task}`,
                        handler: () => {
                            // E.g: Navigate to a specific screen
                        },
                    },
                ],
            });
            alert.present();
        });
    }
    goProfile() {
        this.navCtrl.navigateRoot("profile");
    }
    allowLocation() {
        this.watch = this.geo.watchPosition({ enableHighAccuracy: true, timeout: 10000 }).subscribe(position => {
            if (position.coords != undefined) {
                var geoposition = position;
            }
            else {
                var positionError = position;
            }
        });
    }
    presentToast(message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message,
                duration: 3000,
                position: "bottom",
            });
            toast.onDidDismiss();
            toast.present();
        });
    }
};
AppComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
    { type: _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_3__["SplashScreen"] },
    { type: _services_pop_up_service__WEBPACK_IMPORTED_MODULE_12__["PopUpService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_4__["StatusBar"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"] },
    { type: _services_settings_service__WEBPACK_IMPORTED_MODULE_6__["SettingsService"] },
    { type: _services_native_map_container_service__WEBPACK_IMPORTED_MODULE_10__["NativeMapContainerService"] },
    { type: _services_language_service__WEBPACK_IMPORTED_MODULE_9__["LanguageService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_5__["OneSignal"] },
    { type: _services_profile_service__WEBPACK_IMPORTED_MODULE_8__["ProfileService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: _services_auth_service__WEBPACK_IMPORTED_MODULE_7__["AuthService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_13__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] },
    { type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_14__["Geolocation"] }
];
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-root",
        template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
        styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"],
        _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_3__["SplashScreen"],
        _services_pop_up_service__WEBPACK_IMPORTED_MODULE_12__["PopUpService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
        _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_4__["StatusBar"],
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"],
        _services_settings_service__WEBPACK_IMPORTED_MODULE_6__["SettingsService"],
        _services_native_map_container_service__WEBPACK_IMPORTED_MODULE_10__["NativeMapContainerService"],
        _services_language_service__WEBPACK_IMPORTED_MODULE_9__["LanguageService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
        _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_5__["OneSignal"],
        _services_profile_service__WEBPACK_IMPORTED_MODULE_8__["ProfileService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
        _services_auth_service__WEBPACK_IMPORTED_MODULE_7__["AuthService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_13__["Router"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"],
        _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_14__["Geolocation"]])
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: firebaseConfig, AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "firebaseConfig", function() { return firebaseConfig; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/in-app-browser/ngx */ "./node_modules/@ionic-native/in-app-browser/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/splash-screen/ngx */ "./node_modules/@ionic-native/splash-screen/ngx/index.js");
/* harmony import */ var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/status-bar/ngx */ "./node_modules/@ionic-native/status-bar/ngx/index.js");
/* harmony import */ var _ionic_native_call_number_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/call-number/ngx */ "./node_modules/@ionic-native/call-number/ngx/index.js");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic-native/onesignal/ngx */ "./node_modules/@ionic-native/onesignal/ngx/index.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _ionic_native_vibration_ngx__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ionic-native/vibration/ngx */ "./node_modules/@ionic-native/vibration/ngx/index.js");
/* harmony import */ var _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @ionic-native/http/ngx */ "./node_modules/@ionic-native/http/ngx/index.js");
/* harmony import */ var _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @ionic-native/social-sharing/ngx */ "./node_modules/@ionic-native/social-sharing/ngx/index.js");
/* harmony import */ var ionic4_rating__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ionic4-rating */ "./node_modules/ionic4-rating/dist/index.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_native_firebase_ngx__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @ionic-native/firebase/ngx */ "./node_modules/@ionic-native/firebase/ngx/index.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_20___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_20__);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var ng_otp_input__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ng-otp-input */ "./node_modules/ng-otp-input/fesm2015/ng-otp-input.js");
/* harmony import */ var ionic_selectable__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ionic-selectable */ "./node_modules/ionic-selectable/esm2015/ionic-selectable.min.js");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");

























// Import ionic-rating module
const firebaseConfig = {
    // apiKey: "AIzaSyApjpwWIXFsAn6WWgauNG93bHdAY126eVw",
    // authDomain: "ghana-c4r.firebaseapp.com",
    // projectId: "ghana-c4r",
    // storageBucket: "ghana-c4r.appspot.com",
    // databaseURL: "https://ghana-c4r-default-rtdb.firebaseio.com",
    // messagingSenderId: "516821551729",
    // appId: "1:516821551729:web:04f1a30ca01b8acc9d29d0",
    // measurementId: "G-2Y04YP8P7Q"
    apiKey: "AIzaSyDtQGdYGuIQ7f-r9JqWBBj7q6RJJ9595nI",
    authDomain: "call4ride-2fe35.firebaseapp.com",
    projectId: "call4ride-2fe35",
    storageBucket: "call4ride-2fe35.appspot.com",
    databaseURL: "https://call4ride-2fe35-default-rtdb.firebaseio.com",
    messagingSenderId: "1092796986980",
    appId: "1:1092796986980:web:24d071186167016803d171",
    measurementId: "G-EPD48CQC9J"
};
firebase__WEBPACK_IMPORTED_MODULE_20__["initializeApp"](firebaseConfig);
let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_app_component__WEBPACK_IMPORTED_MODULE_12__["AppComponent"]],
        entryComponents: [
        // AutocompletePage,
        // DriverInfoPage,
        // TripInfoPage,
        // ChatPage,
        // RatePage,
        ],
        imports: [
            ionic4_rating__WEBPACK_IMPORTED_MODULE_17__["IonicRatingModule"],
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["BrowserModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_18__["ReactiveFormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_18__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonicModule"].forRoot(),
            _app_routing_module__WEBPACK_IMPORTED_MODULE_13__["AppRoutingModule"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_21__["IonicStorageModule"].forRoot(),
            _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
            // AutocompletePageModule,
            // DriverInfoPageModule,
            // TripInfoPageModule,
            // ChatPageModule,
            // RatePageModule,
            ng_otp_input__WEBPACK_IMPORTED_MODULE_22__["NgOtpInputModule"],
            ionic_selectable__WEBPACK_IMPORTED_MODULE_23__["IonicSelectableModule"],
        ],
        providers: [
            _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_8__["StatusBar"],
            _ionic_native_vibration_ngx__WEBPACK_IMPORTED_MODULE_14__["Vibration"],
            _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_15__["HTTP"],
            _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_7__["SplashScreen"],
            _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_16__["SocialSharing"],
            _ionic_native_firebase_ngx__WEBPACK_IMPORTED_MODULE_19__["Firebase"],
            _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_5__["InAppBrowser"],
            _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_10__["Camera"],
            _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_11__["OneSignal"],
            _ionic_native_call_number_ngx__WEBPACK_IMPORTED_MODULE_9__["CallNumber"],
            _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_24__["Geolocation"],
            { provide: _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouteReuseStrategy"], useClass: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonicRouteStrategy"] },
        ],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_12__["AppComponent"]],
    })
], AppModule);



/***/ }),

/***/ "./src/app/services/activity.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/activity.service.ts ***!
  \**********************************************/
/*! exports provided: ActivityService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActivityService", function() { return ActivityService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_2__);



let ActivityService = class ActivityService {
    constructor() {
        firebase__WEBPACK_IMPORTED_MODULE_2___default.a.auth().onAuthStateChanged((user) => {
            if (user) {
                this.user = user;
                this.currentDriverRef = firebase__WEBPACK_IMPORTED_MODULE_2___default.a.database().ref(`Drivers/AllDrivers`);
                this.CustomerOwnPropertyRef = firebase__WEBPACK_IMPORTED_MODULE_2___default.a.database().ref(`Customer`);
                this.PoolRef = firebase__WEBPACK_IMPORTED_MODULE_2___default.a.database().ref(`Pools/${user.uid}`);
            }
        });
    }
    getCurrentDriver(id) {
        return this.currentDriverRef.child(id);
    }
    getPoolActivityProfile(id) {
        return this.CustomerOwnPropertyRef.child(`${id}/client`);
    }
    getActivityProfile(id) {
        return this.CustomerOwnPropertyRef.child(`${id}/client`);
    }
    getActiveProfile(id) {
        return firebase__WEBPACK_IMPORTED_MODULE_2___default.a.database().ref(`Customer/${id}`);
    }
    getPoolProfile() {
        return this.PoolRef;
    }
    getActivProfile() {
        return firebase__WEBPACK_IMPORTED_MODULE_2___default.a.database().ref(`Customer/`);
    }
    getConnectionState() {
        return this.connectedRef;
    }
    RequestPool(lat, lng, driverid) {
        return this.PoolRef.set({
            pool_details: [lat, lng, driverid],
        });
    }
};
ActivityService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: "root",
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], ActivityService);



/***/ }),

/***/ "./src/app/services/auth.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/auth.service.ts ***!
  \******************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _language_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./language.service */ "./src/app/services/language.service.ts");
/* harmony import */ var _pop_up_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./pop-up.service */ "./src/app/services/pop-up.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/profile.service */ "./src/app/services/profile.service.ts");








let AuthService = class AuthService {
    constructor(lp, injector, pop, platform, router, alertCtrl, ph) {
        this.lp = lp;
        this.injector = injector;
        this.pop = pop;
        this.platform = platform;
        this.router = router;
        this.alertCtrl = alertCtrl;
        this.ph = ph;
        this.fireAuth = firebase__WEBPACK_IMPORTED_MODULE_5___default.a.auth();
        this.userProfileRef = firebase__WEBPACK_IMPORTED_MODULE_5___default.a.database().ref("/userProfile");
        firebase__WEBPACK_IMPORTED_MODULE_5___default.a
            .auth()
            .onAuthStateChanged((user) => (this.currentUser = user));
    }
    get navCtrl() {
        // tslint:disable-next-line: deprecation
        return this.injector.get(_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"]);
    }
    loginUser(email, password) {
        return this.fireAuth.signInWithEmailAndPassword(email, password).then((result) => {
            console.log("SIGIN RESULT:::" + JSON.stringify(result));
            console.log("UID:::" + result.user.uid);
            const unsubscribe2 = firebase__WEBPACK_IMPORTED_MODULE_5___default.a.auth().onAuthStateChanged((user) => {
                this.ph
                    .getUserProfil()
                    .child(this.ph.id)
                    .on("value", (userProfileSnapshot) => {
                    console.log("USER::", user);
                    console.log("USER PROFILE SNAPSHOT::", userProfileSnapshot.val());
                    console.log("FIRST NAME USER PROFILE SNAPSHOT::", userProfileSnapshot.val().first_name);
                    console.log("PROFILE ID::", this.ph.id);
                    if (user != null) {
                        if (userProfileSnapshot.val() == null && userProfileSnapshot.val().first_name == undefined) {
                            this.navCtrl.navigateRoot("update-users-info");
                        }
                        else {
                            console.log("Im home");
                            this.navCtrl.navigateRoot("home");
                        }
                    }
                    else {
                        this.navCtrl.navigateRoot("login");
                    }
                });
            });
            //   const unsubscribe =  firebase.auth()
            //   .onAuthStateChanged((user) =>{
            //     this.ph.getUserProfile().child(this.ph.id).on("value", async (userProfileSnapshot) => {
            //   // this.ph.getUserProfile().on("value", (userProfileSnapshot) => {
            //     console.log("USER PROFILE:: ", userProfileSnapshot.val());
            //     const user_details = userProfileSnapshot.val();
            //     console.log("USER PROFILE user_details:: ", user_details);
            //     if (user_details == null) {
            //       this.navCtrl.navigateRoot("update-users-info");
            //             } else {
            //       console.log("Im home");
            //         this.navCtrl.navigateRoot("home");
            //     }
            //   });
            // });
        })
            .catch((error) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            console.log("ERROR SIGNING IN" + error.message);
            let alert = yield this.alertCtrl.create({
                message: "User account cannot be found. Kindly register first",
                buttons: [
                    {
                        text: "Cancel",
                        role: "cancel",
                    },
                ],
            });
            alert.present();
        }));
        ;
    }
    signupUser(email, password) {
        return this.fireAuth
            .createUserWithEmailAndPassword(email, password)
            .then((newUser) => {
            this.userProfileRef.child(newUser.user.uid).child("userInfo").set({
                email,
            });
            this.navCtrl.navigateRoot("update-users-info");
            // this.router.navigate(["more-info"]);
        });
    }
    // signupUser(email: string, password: string): Promise<any> {
    //   return firebase
    //     .auth()
    //     .createUserWithEmailAndPassword(email, password)
    //     .then((newUser) => {
    //       console.log("NEW USER CREATED:: --", newUser);
    //       this.userProfileRef.child(newUser.user.uid).child("userInfo").set({
    //         email: email,
    //       });
    //     });
    // }
    signInWithPhoneNumber(recaptchaVerifier, phoneNumber) {
        console.log("PHONE::", phoneNumber);
        console.log("recaptchaVerifier::", recaptchaVerifier);
        return new Promise((resolve, reject) => {
            return firebase__WEBPACK_IMPORTED_MODULE_5___default.a
                .auth()
                .signInWithPhoneNumber(phoneNumber, recaptchaVerifier)
                .then((confirmationResult) => {
                console.log("confirmationResult", confirmationResult);
                this.confirmationResult = confirmationResult;
                resolve(confirmationResult);
            })
                .catch((error) => {
                console.log(error);
                reject("SMS not sent");
            });
        });
    }
    enterVerificationCode(code) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                this.confirmationResult
                    .confirm(code)
                    .then((result) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                    console.log("RESULTS", result);
                    const user = result.user;
                    resolve(user);
                }))
                    .catch((error) => {
                    console.log("ERROR IN VERIFICATION", error);
                    reject(error.message);
                });
            });
        });
    }
    resetPassword(email) {
        return firebase__WEBPACK_IMPORTED_MODULE_5___default.a.auth().sendPasswordResetEmail(email);
    }
    logoutUser() {
        this.userProfileRef
            .child(firebase__WEBPACK_IMPORTED_MODULE_5___default.a.auth().currentUser.uid)
            .child("userInfo")
            .off();
        return firebase__WEBPACK_IMPORTED_MODULE_5___default.a.auth().signOut();
    }
    get authenticated() {
        return this.currentUser !== null;
    }
    signOut() {
        firebase__WEBPACK_IMPORTED_MODULE_5___default.a.auth().signOut();
    }
    displayName() {
        if (this.currentUser !== null) {
            return this.currentUser.displayName;
        }
        else {
            return "";
        }
    }
};
AuthService.ctorParameters = () => [
    { type: _language_service__WEBPACK_IMPORTED_MODULE_2__["LanguageService"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Injector"] },
    { type: _pop_up_service__WEBPACK_IMPORTED_MODULE_3__["PopUpService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] },
    { type: src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_7__["ProfileService"] }
];
AuthService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: "root",
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_language_service__WEBPACK_IMPORTED_MODULE_2__["LanguageService"],
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["Injector"],
        _pop_up_service__WEBPACK_IMPORTED_MODULE_3__["PopUpService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"],
        _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"],
        src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_7__["ProfileService"]])
], AuthService);



/***/ }),

/***/ "./src/app/services/event.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/event.service.ts ***!
  \*******************************************/
/*! exports provided: EventService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventService", function() { return EventService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _language_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./language.service */ "./src/app/services/language.service.ts");
/* harmony import */ var _profile_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./profile.service */ "./src/app/services/profile.service.ts");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_4__);





let EventService = class EventService {
    constructor(ph, lp) {
        this.ph = ph;
        this.lp = lp;
        firebase__WEBPACK_IMPORTED_MODULE_4___default.a.auth().onAuthStateChanged(user => {
            if (user) {
                this.id = user.uid;
                this.user = user;
                this.userProfileRef = firebase__WEBPACK_IMPORTED_MODULE_4___default.a.database().ref(`userProfile/${user.uid}`);
                this.scheduleRef = firebase__WEBPACK_IMPORTED_MODULE_4___default.a.database().ref(`ScheduledRides`);
                this.londonRef = firebase__WEBPACK_IMPORTED_MODULE_4___default.a.database().ref(`LondonRides`);
                this.CustomerOwnPropertyRef = firebase__WEBPACK_IMPORTED_MODULE_4___default.a.database().ref(`Customer/${user.uid}/client`);
                this.ratingText = this.lp.translate()[0].notrate;
                this.ratingValue = 0;
            }
        });
    }
    getUserProfile() {
        return this.userProfileRef;
    }
    getEventList() {
        return this.userProfileRef.child('userInfo').child('/eventList');
    }
    getScheduledList() {
        return this.userProfileRef.child('userInfo').child('/scheduled');
    }
    getDetailOfInfo() {
        return this.appPrice;
    }
    getEventDetail(eventId) {
        return this.userProfileRef.child('userInfo').child('/eventList').child(eventId);
    }
    getCancelledList() {
        return this.userProfileRef.child('userInfo').child('/cancelled');
    }
    getChatList(id) {
        return firebase__WEBPACK_IMPORTED_MODULE_4___default.a.database().ref(`Customer/${id}/client/Chat`);
    }
    getSupportChatList(id) {
        return firebase__WEBPACK_IMPORTED_MODULE_4___default.a.database().ref(`DashboardSettings/user/complains/${id}`);
    }
    Complain(value, id) {
        return firebase__WEBPACK_IMPORTED_MODULE_4___default.a.database().ref(`DashboardSettings/user/complains/${id}`).push({
            Client_Message: value,
            email: this.user.email,
            chat_key: this.id
        });
    }
    PushUserDetails(name, picture, lat, lng, locationName, payWith) {
        return this.scheduleRef.child('/client').update({
            Client_username: name,
            Client_location: [lat, lng],
            Client_locationName: locationName,
            Client_paymentForm: payWith,
            Client_picture: picture,
            Client_ID: this.id,
            Client_PickedUp: false,
            Client_Dropped: false,
            Client_HasRated: false,
        });
    }
    CreateSchedule(price, name, picture, lat, lng, locationName, payWith, destination, phone, date, id, ratetext, ratevalue, time) {
        return this.scheduleRef.child(id).update({
            Client_username: name,
            Client_Deleted: false,
            Client_location: [lat, lng],
            Client_locationName: locationName,
            Client_paymentForm: payWith,
            Client_picture: picture,
            Client_phoneNumber: phone,
            Client_destinationName: destination,
            Client_CanChargeCard: false,
            Client_PickedUp: false,
            Client_Dropped: false,
            Client_HasRated: false,
            Client_ended: false,
            Client_price: price,
            Client_Date: date,
            Client_Time: time,
            Client_ID: id,
            Client_hasPaid: false,
            Client_paidCash: false,
            Client_Rating: ratevalue,
            Client_RatingText: ratetext
        });
    }
    CreateLondonBook(name, picture, lat, lng, locationName, payWith, destination, phone, date, id, ratetext, ratevalue, time) {
        return this.londonRef.child(id).update({
            Client_username: name,
            Client_Deleted: false,
            Client_locationName: locationName,
            Client_paymentForm: payWith,
            Client_picture: picture,
            Client_phoneNumber: phone,
            Client_destinationName: destination,
            Client_ended: false,
            Client_Date: date,
            Client_Time: time,
            Client_ID: id,
            Client_hasPaid: false,
            Client_paidCash: false,
            Client_Rating: ratevalue,
            Client_RatingText: ratetext
        });
    }
    UpdateDestination(destinationName, price, id) {
        return firebase__WEBPACK_IMPORTED_MODULE_4___default.a.database().ref(`Customer/${id}/client`).update({
            Client_destinationName: destinationName,
            Client_price: price,
        });
    }
    UpdateNetworkSate(state, id) {
        return firebase__WEBPACK_IMPORTED_MODULE_4___default.a.database().ref(`Customer/${id}/client`).update({
            Network_state: state,
        });
    }
    CreateNewSchedule(date) {
        return this.userProfileRef.child('/scheduled').push({
            TimeandDate: date,
        });
    }
    UpdateSate(state, id) {
        return firebase__WEBPACK_IMPORTED_MODULE_4___default.a.database().ref(`Customer/${id}/client`).update({
            Left_and_Returned: state,
        });
    }
    UpdateCard(card, month, year, cvc, amount, email, driverPay) {
        return this.userProfileRef.update({
            Card_Number: card,
            Card_month: month,
            Card_Year: year,
            Card_Cvc: cvc,
            Card_Amount: amount,
            Card_email: email,
            Card_driverPay: driverPay
        });
    }
};
EventService.ctorParameters = () => [
    { type: _profile_service__WEBPACK_IMPORTED_MODULE_3__["ProfileService"] },
    { type: _language_service__WEBPACK_IMPORTED_MODULE_2__["LanguageService"] }
];
EventService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_profile_service__WEBPACK_IMPORTED_MODULE_3__["ProfileService"], _language_service__WEBPACK_IMPORTED_MODULE_2__["LanguageService"]])
], EventService);



/***/ }),

/***/ "./src/app/services/geocoder.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/geocoder.service.ts ***!
  \**********************************************/
/*! exports provided: GeocoderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GeocoderService", function() { return GeocoderService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



let GeocoderService = class GeocoderService {
    constructor(platform) {
        this.platform = platform;
        this.locationChange = true;
        // tslint:disable-next-line: new-parens
        this.geocoder = new google.maps.Geocoder;
    }
    Geocode(address) {
        // tslint:disable-next-line: object-literal-key-quotes
        this.geocoder.geocode({ 'address': address }, (results, status) => {
            if (status === 'OK') {
                const position = results[0].geometry.location;
                this.lat = position.lat();
                this.lng = position.lng();
                console.log(this.lat);
            }
            else {
            }
        });
    }
    Reverse_Geocode(lat, lng, driverMode) {
        let latlng = { lat: parseFloat(lat), lng: parseFloat(lng) };
        this.geocoder.geocode({ 'location': latlng }, (results, status) => {
            if (status === 'OK') {
                if (results[0]) {
                    if (!driverMode) {
                        if (this.locationChange) {
                            if (document.getElementById("location"))
                                document.getElementById("location").innerText = results[0].formatted_address;
                            this.locationName = results[0].formatted_address;
                            console.log('location was chnaged');
                        }
                        if (this.destinationChange) {
                            if (document.getElementById("destination"))
                                document.getElementById("destination").innerText = results[0].formatted_address;
                            this.destinationSetName = results[0].formatted_address;
                            console.log('destination was chnaged');
                        }
                    }
                    else {
                        console.log('This is not the driver mode');
                        /// var driver_location = results[0].formatted_address;
                    }
                }
                else {
                    // window.alert('No results found');
                }
            }
            else {
                // window.alert('Geocoder failed due to: ' + status);
            }
        });
    }
    Simple_Geocode(lat, lng) {
        const latlng = { lat: parseFloat(lat), lng: parseFloat(lng) };
        let result;
        // tslint:disable-next-line: object-literal-key-quotes
        this.geocoder.geocode({ 'location': latlng }, (results, status) => {
            if (status === 'OK') {
                if (results[0]) {
                    result = results[0].formatted_address;
                }
            }
        });
        return result;
    }
};
GeocoderService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] }
];
GeocoderService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"]])
], GeocoderService);



/***/ }),

/***/ "./src/app/services/language.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/language.service.ts ***!
  \**********************************************/
/*! exports provided: LanguageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LanguageService", function() { return LanguageService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");





let LanguageService = class LanguageService {
    constructor(http, storage, Alert) {
        this.http = http;
        this.storage = storage;
        this.Alert = Alert;
        this.targetLanguageCode = 'en';
        this.show = false;
        console.log('Hello LanguageProvider Provider');
        this.http = http;
        this.storage.get(`Language`).then((value) => {
            if (value != null) {
                this.targetLanguageCode = value;
                this.show = true;
            }
            else {
                this.targetLanguageCode = 'en';
            }
            console.log(value);
        });
    }
    translate() {
        if (this.targetLanguageCode === 'en') {
            return [
                {
                    whereTo: 'Where To ?',
                    set: 'Set Pin On Location',
                    dest: 'Your Destination',
                    done: 'Done',
                    economy: 'STANDARD',
                    pool: 'POOL',
                    cash: 'Cash',
                    wrongdest: 'Wrong Destination',
                    arrival: 'Driver Arrives In ',
                    grut: 'Network Error. Check your internet connection',
                    card: 'Card',
                    addcard: 'Add A Card',
                    request: 'RIDE NOW',
                    later: 'RIDE LATER',
                    someone: 'Ride With Someone',
                    getcharged: 'Get Charged :',
                    join: 'JOIN POOL',
                    start: 'START POOL',
                    proccessingcharge: 'Processing Charge..',
                    picknote: 'New Passenger To Pickup. You Have Less Than 60 Seconds To accept the Job.',
                    passenger_add_trip: 'New Stop has been added by Passenger. Please ensure that is right',
                    unfortunate: 'Unfortunately,',
                    notavailable: 'is not available in your area.',
                    just: 'Just A Second',
                    fbacc: 'Your Facebook Account Has No Email.',
                    retry: 'Failed Internet. Retry',
                    home: 'Please Add Home And Work in Settings',
                    notrate: 'Not Rated',
                    nodriver: 'No Drivers Found',
                    whycancel: 'Why Do You Want To Cancel',
                    chose: 'Please Choose An Option',
                    longpick: 'Long Pickup ?',
                    incorrect: 'Incorrect Request ?',
                    reject: 'Reject',
                    accept: 'Accept',
                    cantexit: 'Sorry Cant Exit At this Time',
                    lost: 'Connection lost, trying to connect to the internet. Retrying..',
                    recover: 'Please wait while we recover lost information',
                    driverarive: 'Driver Arrives In ',
                    drive: 'Driving To Your Destination',
                    drivercancel: 'Driver Cancelled',
                    end: 'Your Journey Has Ended',
                    startride: 'Your Journey Has Started',
                    paidcash: 'Confirm You Have Paid Cash',
                    poolcancel: 'Pool Cancelled. No Other Rider',
                    waiting: 'Please Wait...',
                    driverfound: 'Driver Found',
                    check: 'Checkout',
                    website: 'Website',
                    career: 'Careers',
                    faq: 'FAQ',
                    address: 'Enter Search',
                    add: 'Add',
                    homesearch: 'Home',
                    worksearch: 'Work',
                    rideSch: 'Schedule A Ride',
                    rideSchu: 'You Scheduled A Ride',
                    from: 'FROM',
                    to: 'TO',
                    date: 'Choose A Date',
                    clear: 'Cancel',
                    addDest: 'Please Add Your Destination and Location',
                    addTime: 'Wrong Time',
                    sucsch: 'Succefully Scheduled A Ride',
                    sucschw: 'Succefully Cancelled Ride',
                    error: 'Are You Sure ?',
                    report: 'Your complain has been submitted, we will get back to you. In a few minutes.',
                    support: 'Support',
                    complaint: 'Write A Complaint',
                    submit: 'SUBMIT',
                    customercare: 'CustomerCare',
                    email: 'Email',
                    password: 'Password',
                    newacc: 'Create A New Account',
                    login: 'LOGIN',
                    validemail: 'Enter A Valid Email',
                    resetpasss: 'Reset Password',
                    emailsent: 'Your Email Has Been Sent. To Recover your Password',
                    review: 'Write A Review',
                    rate: 'Rate',
                    rateexp: 'Rate Your Experience',
                    alertcar: 'Select at least one star',
                    promo: 'Promo',
                    enteruid: 'Enter Sharing UID',
                    uid: 'sharing UID',
                    cardf: 'Please Add A Card',
                    shareid: 'This is a Sharing UID',
                    shareid2: 'This is not a Sharing UID',
                    shareid3: 'There is no Sharing UID',
                    name: 'Name',
                    phone: 'Phone',
                    profile: 'Profile',
                    choosefrom: 'Choose From',
                    camera: 'Camera',
                    file: 'File',
                    cancel: 'Cancel',
                    pictureset: 'Profile Picture Set Successfully',
                    failed: 'Failed. Please Try Again',
                    resend: 'RESEND',
                    details: 'Add Missing Details',
                    countrycode: 'Country Code',
                    payment: 'Reciept',
                    pay: 'PAY',
                    safety: 'Please make sure you are at your requested destination before checking out. To avoid fraud',
                    regards: 'Thank you for using',
                    payment2: 'Payment',
                    entercode: 'Enter A Promo Code',
                    retryi: 'Retry In',
                    secs: 'Seconds',
                    fastandconv: 'Fast and Convenient',
                    signup: 'SIGNUP',
                    facebook: 'FACEBOOK',
                    price: 'PRICE',
                    location: 'LOCATION',
                    destination: 'DESTINATION',
                    detailsp: 'Details',
                    triphistory: 'Trip History',
                    freeride: 'Free Rides',
                    facebookl: 'Facebook',
                    whatsapp: 'Whatsapp',
                    instagram: 'Instagram',
                    half1: 'Share your promo code with friends. Once they have tried',
                    half2: 'you will also get a free ride.',
                    adapt: 'This is my share ID for ',
                    calc: 'Trip Calculator',
                    pvc: 'Prices may vary due to unforseen circumstances. Such as weather changes etc',
                    priceappear: 'Price Will Appear Here',
                    scan: 'Scan Card',
                    message: 'Message',
                    cvc: 'Cvc',
                    year: 'Year',
                    month: 'Month',
                    confirm: 'We will make a payment to confirm card.',
                    cardsuc: 'Card Successfully Activated',
                    cardunc: 'Card Activation Failed',
                    drop: 'You Have Been Dropped. Please Checkout.',
                    retrynew: 'Retry',
                    about: 'About',
                    lang: 'Language'
                },
            ];
        }
        if (this.targetLanguageCode === 'tk') {
            return [
                {
                    whereTo: 'Nereye',
                    set: 'Put pin in position',
                    dest: 'Hedefiniz',
                    done: 'Tamamlandı',
                    economy: 'STANDARD',
                    pool: 'POOL',
                    cash: 'nakit',
                    wrongdest: 'Yanlış hedef',
                    arrival: 'Sürücü geldi ',
                    grut: 'Şebeke hatası. İnternet bağlantınızı kontrol edin',
                    card: 'kredi kartı',
                    addcard: 'Kredi kartı ekle',
                    request: 'ŞİMDİ',
                    later: 'Sonra Yolculuk',
                    someone: 'bir yolculuk',
                    getcharged: 'tahsil edilecektir:',
                    join: 'HAVUZ KATILIN',
                    start: 'BAŞLATMA HAVUZU',
                    proccessingcharge: 'işlem ücreti',
                    picknote: 'Almak için yeni yolcu. Siparişi kabul etmek için 60 saniyeden az zamanınız var.',
                    passenger_add_trip: 'New Stop has been added by Passenger. Please ensure that is right',
                    unfortunate: 'Ne yazık ki,',
                    notavailable: 'bölgenizde mevcut değil.',
                    just: 'Bir saniye',
                    fbacc: 'Facebook hesabınızın e-posta adresi yok.',
                    retry: 'İnternet başarısız oldu. tekrar',
                    notrate: 'Derecelendirilmedi',
                    nodriver: 'Sürücü bulunamadı',
                    home: 'Lütfen Ayarlara Ev ve İş ekleyin',
                    whycancel: 'İptal etmek istiyor musun',
                    chose: 'Lütfen bir seçenek seçin',
                    longpick: 'sürücü alma?',
                    incorrect: 'Yanlış istek?',
                    reject: 'red',
                    accept: 'kabul etmek',
                    cantexit: 'Üzgünüm, şimdi bitiremiyorum',
                    lost: 'Bağlantı koptu ve İnternete bağlanmaya çalışıyor. Tekrar et ..',
                    recover: 'Kayıp verileri kurtarırken lütfen bekleyin',
                    driverarive: 'Sürücü geldi ',
                    drive: 'Hedefinize sürünl',
                    drivercancel: 'Sürücü iptal edildi',
                    end: 'Yolculuğunuz bitti',
                    startride: 'Yolculuğunuz başladı',
                    paidcash: 'Nakit ödediğinizi onaylayın',
                    poolcancel: 'Havuz iptal edildi. Başka sürücü yok',
                    waiting: 'Lütfen bekleyin ...',
                    driverfound: 'Sürücü bulundu',
                    check: 'Cıkış',
                    website: 'web sitesi',
                    career: 'kariyer',
                    faq: 'Sorular ve cevaplar',
                    address: 'Aramayı gir',
                    add: 'ekle',
                    homesearch: 'ev',
                    worksearch: 'İş',
                    rideSch: 'Bir yolculuk planlayın',
                    rideSchu: 'Bir gezi planladınız',
                    from: 'Nerden',
                    to: 'Nereye',
                    date: 'Bir Tarih Seçin, bir tarih seçin',
                    clear: 'iptal etmek',
                    addDest: 'Lütfen varış yerinizi ve konumunuzu ekleyin',
                    addTime: 'Yanlış zaman',
                    sucsch: 'Bir yolculuk başarıyla planlandı',
                    sucschw: 'Yolculuk başarıyla iptal edildi',
                    error: 'Eminmisin ?',
                    report: 'Şikayetiniz gönderildi ve en kısa sürede sizinle iletişim kuracağız. Birkaç dakika içinde',
                    support: 'destek',
                    complaint: 'Şikayetinizi yazın',
                    submit: 'GÖNDER',
                    customercare: 'müşteri hizmetleri',
                    email: 'Email',
                    password: 'şifre',
                    newacc: 'Yeni hesap oluştur',
                    login: 'Login',
                    validemail: 'Geçerli bir e-posta adresi girin',
                    resetpasss: 'şifreyi sıfırla',
                    emailsent: 'E-postan gönderildi. Şifrenizi nasıl kurtarırsınız?',
                    review: 'Yorum yaz',
                    rate: 'değerlendirme',
                    rateexp: 'Deneyiminizi değerlendirin',
                    alertcar: 'En az bir yıldız seçin',
                    promo: 'Promo',
                    enteruid: 'Yayın UID',
                    uid: 'UID',
                    cardf: 'Lütfen bir kart ekleyin',
                    shareid: 'Bu ortak bir UID',
                    shareid2: 'Bu bir yayın UID',
                    shareid3: 'Ortak bir UID yok',
                    name: 'isim',
                    phone: 'telefon numarası',
                    profile: 'Profil',
                    choosefrom: 'seçim',
                    camera: 'kamera',
                    file: 'dosya',
                    cancel: 'iptal etmek',
                    pictureset: 'Profil resmi başarıyla ayarlandı',
                    failed: 'Başarısız. Lütfen tekrar dene',
                    resend: 'TEKRAR GÖNDER',
                    details: 'Eksik ayrıntı ekle',
                    countrycode: 'ülke kodu',
                    payment: 'makbuz',
                    pay: 'ödeme',
                    safety: 'Lütfen kontrol etmeden önce istediğiniz varış noktasında bulunduğunuzdan emin olun. Dolandırıcılığı önlemek için',
                    regards: 'Taxi 7x24 Koop Kullandığınız için teşekkürler',
                    payment2: 'ödeme',
                    entercode: 'Bir promosyon kodu girin',
                    retryi: 'Tekrar dene',
                    secs: 'saniye',
                    fastandconv: 'Hızlı ve kullanışlı',
                    signup: 'KAYIT',
                    facebook: 'FACEBOOK',
                    price: 'FİYAT',
                    location: 'konum',
                    destination: 'HEDEF',
                    detailsp: 'ayrıntılar',
                    triphistory: 'seyahat bilgisi',
                    freeride: 'ücretsiz sürmek',
                    facebookl: 'Facebook',
                    whatsapp: 'Whatsapp',
                    instagram: 'Instagram',
                    half1: 'Promosyon kodunuzu arkadaşlarınızla paylaşın. Denemedikleri anda',
                    half2: 'Ayrıca ücretsiz bir yolculuk alacaksınız.',
                    adapt: 'Bu benim yayın kimliğim',
                    calc: 'seyahat Hesaplama',
                    pvc: 'Fiyatlar öngörülemeyen koşullar nedeniyle değişebilir. Hava durumu değişiklikleri gibi',
                    priceappear: 'İşte fiyat görüntülenir',
                    scan: 'Haritayı tara',
                    message: 'mesaj',
                    cvc: 'Cvc',
                    year: 'Yıl',
                    month: 'Ay',
                    confirm: 'Kartı onaylamak için bir ödeme yapacağız.',
                    cardsuc: 'Kart başarıyla etkinleştirildi ',
                    cardunc: 'Kart aktivasyonu başarısız oldu',
                    drop: 'düştün. Lütfen kontrol et.',
                    retrynew: 'tekrar',
                    about: 'yaklaşık',
                    lang: 'Dil'
                },
            ];
        }
        if (this.targetLanguageCode === 'fr') {
            return [
                {
                    whereTo: 'Où aller?',
                    set: 'Définir l`emplacement de la broche',
                    dest: 'Votre destination',
                    done: 'fait',
                    economy: 'ÉCONOMIE',
                    pool: 'PISCINE',
                    cash: '<<Argent>>',
                    wrongdest: 'Mauvaise destination',
                    arrival: 'Driver arrive ',
                    grut: 'Erreur réseau. Vérifiez votre connection internet',
                    card: 'Carte',
                    addcard: 'Ajouter une carte',
                    request: 'DEMANDE DE RIDE',
                    someone: 'Roulez avec quelqu`un',
                    getcharged: 'Soyez facturé seulement',
                    join: 'JOIN PISCINE',
                    start: 'START POOL',
                    proccessingcharge: '«Frais de traitement ..»',
                    picknote: 'Nouveau passager à ramasser. Vous avez moins de 60 secondes pour accepter le travail. ',
                    passenger_add_trip: 'New Stop has been added by Passenger. Please ensure that is right',
                    unfortunate: 'Malheureusement,',
                    notavailable: 'n`est pas disponible dans votre région.',
                    just: '«Juste une seconde»',
                    fbacc: 'Votre compte Facebook n`a pas d`email.',
                    retry: 'Échec d`Internet. Recommencez',
                    home: 'S`il vous plaît ajouter Home And Work dans les paramètres',
                    notrate: 'Non classé',
                    nodriver: 'Aucun pilote trouvé',
                    whycancel: ' «Pourquoi voulez-vous annuler?',
                    chose: 'S`il vous plaît choisir une option',
                    longpick: 'Long Pickup ?',
                    incorrect: 'Demande incorrecte?',
                    reject: 'Rejeter',
                    accept: 'Accepter',
                    cantexit: 'Désolé, je ne peux pas quitter pour le moment',
                    lost: 'Connexion perdue, essayant de se connecter à Internet. Réessayer .. ',
                    recover: 'Veuillez patienter pendant que nous récupérons les informations perdues',
                    driverarive: 'Driver Arrives In ',
                    drive: '«Conduire à votre destination»',
                    drivercancel: 'Pilote annulé',
                    end: 'Votre voyage s`est terminé',
                    startride: 'Votre voyage a commencé',
                    paidcash: 'Confirmez que vous avez payé de l`argent',
                    poolcancel: 'Pool annulé. No Other Rider',
                    waiting: 'S`il vous plaît attendre ...',
                    driverfound: 'Pilote trouvé',
                    check: 'Commander',
                    website: 'Site Web',
                    career: ' «Carrières»',
                    faq: 'FAQ',
                    address: '',
                    add: 'Ajouter',
                    homesearch: 'Accueil',
                    worksearch: 'Travail',
                    rideSch: 'Programme Un tour',
                    rideSchu: '«Vous avez prévu un tour»',
                    from: 'DE',
                    to: 'À',
                    date: 'Choisissez une date',
                    clear: 'Annuler',
                    addDest: 'Veuillez ajouter votre destination et votre emplacement',
                    addTime: 'Mauvais moment',
                    sucsch: 'a programmé avec succès un tour',
                    sucschw: '«Succès annulé avec succès»',
                    error: 'Êtes-vous sûr?',
                    report: 'Votre plainte a été soumise, nous vous répondrons. Dans quelques minutes.',
                    support: 'Soutien',
                    complaint: 'Écrivez une plainte',
                    submit: 'SOUMETTRE',
                    customercare: 'CustomerCare',
                    email: 'Email',
                    password: 'Mot de passe',
                    newacc: 'Créer un nouveau compte',
                    login: 'Connexion',
                    validemail: 'Entrez un email valide',
                    resetpasss: 'Réinitialiser le mot de passe',
                    emailsent: 'Votre email a été envoyé. Pour récupérer votre mot de passe ',
                    review: 'Ecrire un commentaire',
                    rate: 'Taux',
                    rateexp: 'Évaluer votre expérience',
                    alertcar: 'Sélectionnez au moins une étoile',
                    promo: 'Promo',
                    enteruid: 'Entrer l`UID de partage',
                    uid: 'partage UID',
                    cardf: 'S`il vous plaît Ajouter une carte',
                    shareid: 'Ceci est un partage UID',
                    shareid2: 'Ceci n`est pas un UID de partage',
                    shareid3: 'Il n`y a pas d`UID de partage',
                    name: 'nom',
                    phone: 'Téléphone',
                    profile: 'Profil',
                    choosefrom: 'Choisir De',
                    camera: 'Caméra',
                    file: 'Fichier',
                    cancel: 'Annuler',
                    pictureset: 'L`image du profil s`est bien passée',
                    failed: 'Échec. Veuillez réessayer',
                    resend: 'RENVOYER',
                    details: 'Ajouter des détails manquants',
                    countrycode: 'Code du pays',
                    payment: 'Reciept',
                    pay: 'PAYER',
                    safety: 'S`il vous plaît assurez-vous que vous êtes à votre destination demandée avant de partir. Pour éviter la fraude',
                    regards: 'Merci d`avoir utilisé',
                    payment2: 'Paiement',
                    entercode: 'Entrez un code promo',
                    retryi: 'Réessayer dans',
                    secs: 'Secondes',
                    fastandconv: 'Rapide et pratique',
                    signup: 'S`INSCRIRE',
                    facebook: 'FACEBOOK',
                    price: 'PRIX',
                    location: 'EMPLACEMENT',
                    destination: 'DESTINATION',
                    detailsp: 'Détails',
                    triphistory: 'histoire de voyage',
                    freeride: 'des tours gratuits',
                    facebookl: 'Facebook',
                    whatsapp: 'Whatsapp',
                    instagram: 'Instagram',
                    half1: 'Partagez votre code promo avec vos amis. Une fois qu`ils ont essayé ',
                    half2: 'vous aurez aussi un tour gratuit.',
                    adapt: 'Ceci est mon identifiant de partage pour ',
                    calc: 'calculateur de voyage',
                    pvc: 'Les prix peuvent varier en raison de circonstances imprévues. Tels que les changements de temps etc',
                    priceappear: 'Le prix apparaîtra ici',
                    scan: 'Carte d`analyse',
                    message: 'Message',
                    cvc: 'Cvc',
                    year: 'Année',
                    month: 'Mois',
                    confirm: 'Nous effectuerons un paiement pour confirmer la carte.',
                    cardsuc: 'Carte activée avec succès',
                    cardunc: 'L`activation de la carte a échoué',
                    drop: 'Vous avez été droppé. Veuillez vérifier s`il vous plait.',
                    retrynew: 'Réessayer',
                    about: 'à propos',
                    lang: 'Langue'
                }
            ];
        }
        if (this.targetLanguageCode === 'gm') {
            return [
                {
                    whereTo: 'Wohin ?',
                    set: 'Pin auf Position setzen',
                    dest: 'Dein Ziel',
                    done: 'Erledigt',
                    economy: 'STANDARD',
                    pool: 'POOL',
                    cash: 'bar',
                    wrongdest: 'Falsches Ziel',
                    arrival: 'Fahrer kommt an ',
                    grut: 'Netzwerkfehler. Prüfe deine Internetverbindung',
                    card: 'Kredit Karte',
                    addcard: 'Füge eine Kredit Karte hinzu',
                    request: 'JETZT FAHREN',
                    later: 'SPÄTER Fahren',
                    someone: 'Fahrt',
                    getcharged: 'Aufgeladen werden :',
                    join: 'POOL BEITRETEN',
                    start: 'START POOL',
                    proccessingcharge: 'Bearbeitungsgebühr',
                    picknote: 'Neuer Passagier zur Abholung. Sie haben weniger als 60 Sekunden Zeit, um den Auftrag anzunehmen.',
                    passenger_add_trip: 'New Stop has been added by Passenger. Please ensure that is right',
                    unfortunate: 'Unglücklicherweise,',
                    notavailable: 'ist in Ihrer Region nicht verfügbar.',
                    just: 'Eine Sekunde',
                    fbacc: 'Ihr Facebook-Konto hat keine E-Mail.',
                    retry: 'Internet fehlgeschlagen. Wiederholen',
                    notrate: 'Nicht bewertet',
                    nodriver: 'Keine Fahrer gefunden',
                    home: 'Bitte fügen Sie Home And Work in Settings hinzu',
                    whycancel: 'wollen Sie Abbrechen',
                    chose: 'Bitte wähle eine Option',
                    longpick: 'Lange Abholung?',
                    incorrect: 'Falsche Anfrage ?',
                    reject: 'Ablehnen',
                    accept: 'Akzeptieren',
                    cantexit: 'Es tut uns leid, ich kann jetzt nicht beenden',
                    lost: 'Die Verbindung wurde unterbrochen und versucht, eine Verbindung zum Internet herzustellen. Wiederholen ..',
                    recover: 'Bitte warten Sie, während wir verlorene Daten wiederherstellen',
                    driverarive: 'Fahrer kommt an ',
                    drive: 'Fahren zu Ihrem Ziel',
                    drivercancel: 'Fahrer abgebrochen',
                    end: 'Deine Reise ist zu Ende',
                    startride: 'Ihre Reise hat begonnen',
                    paidcash: 'Bestätigen Sie, dass Sie Bargeld bezahlt haben',
                    poolcancel: 'Pool abgebrochen. Kein anderer Fahrer',
                    waiting: 'Bitte warten ...',
                    driverfound: 'Fahrer gefunden',
                    check: 'Auschecken',
                    website: 'Webseite',
                    career: 'Karriere',
                    faq: 'Fragen und Antworten',
                    address: 'Suche eingeben',
                    add: 'Hinzufügen',
                    homesearch: 'Zuhause',
                    worksearch: 'Arbeit',
                    rideSch: 'Planen Sie eine Fahrt',
                    rideSchu: 'Sie haben eine Fahrt geplant',
                    from: 'VON',
                    to: 'NACH',
                    date: 'Choose A Date',
                    clear: 'Abbrechen',
                    addDest: 'Bitte fügen Sie Ihr Ziel und Ihren Standort hinzu',
                    addTime: 'Falsche Zeit',
                    sucsch: 'Eine Fahrt erfolgreich geplant',
                    sucschw: 'Fahrt erfolgreich abgebrochen',
                    error: 'Bist du sicher ?',
                    report: 'Ihre Beschwerde wurde eingereicht, wir werden uns umgehend bei Ihnen melden. In ein paar Minuten.',
                    support: 'Unterstützung',
                    complaint: 'Schreiben Sie ihre Beschwerde',
                    submit: 'ABSCHICKEN',
                    customercare: 'Kundendienst',
                    email: 'Email',
                    password: 'Passwort',
                    newacc: 'Ein neues Konto erstellen',
                    login: 'ANMELDUNG',
                    validemail: 'Geben Sie eine gültige E-Mail-Adresse einl',
                    resetpasss: 'Passwort zurücksetzen',
                    emailsent: 'Ihre E-Mail wurde gesendet. So stellen Sie Ihr Passwort wieder her',
                    review: 'Schreiben Sie eine Bewertung',
                    rate: 'Bewertung',
                    rateexp: 'Bewerten Sie Ihre Erfahrung',
                    alertcar: 'Wählen Sie mindestens einen Stern aus',
                    promo: 'Promo',
                    enteruid: 'Geben Sie die Freigabe-UID ein',
                    uid: 'UID teilen',
                    cardf: 'Bitte fügen Sie eine Karte hinzu',
                    shareid: 'Dies ist eine gemeinsame UID',
                    shareid2: 'Dies ist keine Freigabe-UID',
                    shareid3: 'Es gibt keine gemeinsame UID',
                    name: 'Name',
                    phone: 'Telefonnummer',
                    profile: 'Profil',
                    choosefrom: 'Wähle aus',
                    camera: 'Kamera',
                    file: 'Datei',
                    cancel: 'Abbrechen',
                    pictureset: 'Profilbild erfolgreich eingestellt',
                    failed: 'Fehlgeschlagen. Bitte versuche es erneut',
                    resend: 'ERNEUT SENDEN',
                    details: 'Fehlende Details hinzufügen',
                    countrycode: 'Landesvorwahl',
                    payment: 'Quittung',
                    pay: 'ZAHLEN',
                    safety: 'Bitte vergewissern Sie sich, dass Sie sich an Ihrem gewünschten Zielort befinden, bevor Sie auschecken. Um Betrug zu vermeiden',
                    regards: 'Danke für das benutzen',
                    payment2: 'Zahlung',
                    entercode: 'Geben Sie einen Promo-Code ein',
                    retryi: 'Erneut versuchen in',
                    secs: 'Sekunden',
                    fastandconv: 'Schnell und bequem',
                    signup: 'REGISTRIEREN',
                    facebook: 'FACEBOOK',
                    price: 'PREIS',
                    location: 'STANDORT',
                    destination: 'ZIEL',
                    detailsp: 'Einzelheiten',
                    triphistory: 'Reiseverlauf',
                    freeride: 'Freifahrten',
                    facebookl: 'Facebook',
                    whatsapp: 'Whatsapp',
                    instagram: 'Instagram',
                    half1: 'Teilen Sie Ihren Promo-Code mit Freunden. Sobald sie es versucht haben',
                    half2: 'Sie werden auch eine kostenlose Fahrt bekommen.',
                    adapt: 'Dies ist meine Freigabe-ID für',
                    calc: 'Reiserechnerr',
                    pvc: 'Die Preise können aufgrund unvorhergesehener Umstände variieren. Wie Wetteränderungen usw',
                    priceappear: 'Hier wird der Preis angezeigt',
                    scan: 'Karte scannen',
                    message: 'Botschaft',
                    cvc: 'Cvc',
                    year: 'Jahr',
                    month: 'Monat',
                    confirm: 'Wir werden eine Zahlung vornehmen, um die Karte zu bestätigen.',
                    cardsuc: 'Karte erfolgreich aktiviert ',
                    cardunc: 'Kartenaktivierung fehlgeschlagen',
                    drop: 'Du wurdest fallen gelassen. Bitte checken sie auss.',
                    retrynew: 'Wiederholen',
                    about: 'ungefähr',
                    lang: 'Sprache'
                }
            ];
        }
        if (this.targetLanguageCode === 'pr') {
            return [
                {
                    whereTo: 'Para onde',
                    set: 'Coloque o Pin no Local',
                    dest: 'Seu Destino',
                    done: 'Feito',
                    economy: 'Econômico',
                    pool: 'GRUPO',
                    cash: 'Dinheiro',
                    wrongdest: 'Destino Errado',
                    arrival: 'O Condutor chegará em ',
                    grut: 'Erro de Rede. Verifique a sua ligação à internet',
                    card: 'Cartão',
                    addcard: 'Adicione um Cartão',
                    request: 'RESERVE UMA CORRIDA',
                    someone: 'Viage em grupo',
                    getcharged: 'Seja debitado',
                    join: 'Junte-se ao grupo',
                    start: 'Começe o grupo',
                    proccessingcharge: 'Procesando o pagamento',
                    picknote: 'Novo Passageiro a recuperar. Você tem menos de 60 Segundos para aceitar este trabalho.',
                    passenger_add_trip: 'New Stop has been added by Passenger. Please ensure that is right',
                    unfortunate: 'Infelismente,',
                    notavailable: 'Não está disponível na sua área.',
                    just: 'Um momento',
                    fbacc: 'A sua conta de Facebook não possui email.',
                    retry: 'Falha de Internet. Retroceder',
                    home: 'Por favor Adicione o endereço da Casa e Trabalho em Propriedades',
                    notrate: 'Não Avaliado',
                    nodriver: 'Nenhum Condutor Encontrado',
                    whycancel: 'Porquê quer Cancelar ?',
                    chose: 'Por Favor Escolha Uma Opção',
                    longpick: 'Demora na Recolha ?',
                    incorrect: 'Endereço Incoreto ?',
                    reject: 'Rejeitar',
                    accept: 'Aceitar',
                    cantexit: 'Desculpe não pode Sair Agora',
                    lost: 'Conexão perdida, tentativa de conexão à internet. Reconectando..',
                    recover: 'Por favor aguarde enquanto recuperamos a informação perdida',
                    driverarive: 'O condutor chegará em',
                    drive: 'A Conduzir para o seu Destino',
                    drivercancel: 'Cancelado pelo Condutor',
                    end: 'O seu percurso chegou ao fim',
                    startride: 'Começo do seu percurso',
                    paidcash: 'Confirme o Pagamento em dinheiro',
                    poolcancel: 'Viagem em grupo Cancelado. Nenhum outro passageiro',
                    waiting: 'Por favor aguarde…',
                    driverfound: 'Condutor Encontrado',
                    check: 'Pagamento',
                    website: 'Website',
                    career: 'Careira',
                    faq: 'FAQ',
                    address: 'Inserir endereço',
                    add: 'Adicionar',
                    homesearch: 'Casa',
                    worksearch: 'Trabalho',
                    rideSch: 'Reserve uma corrida',
                    rideSchu: 'Voce Reservou uma Corrida',
                    from: 'DE',
                    to: 'PARA',
                    date: 'Escolha uma Data',
                    clear: 'Cancelar',
                    addDest: 'Por favor Adicione o seu Destino e Localidade',
                    addTime: 'Hora errada',
                    sucsch: 'Corrida agendada com Sucesso',
                    sucschw: 'Corrida Cancelada com Sucesso',
                    error: 'Tem certeza ?',
                    report: 'A sua reclamação foi submetida, iremos-lhe contactar. em alguns minutos.',
                    support: 'Atendimento',
                    complaint: 'Escreva Uma Reclamação',
                    submit: 'CONFIRMAR',
                    customercare: 'Serviço de apoio ao cliente',
                    email: 'Email',
                    password: 'Palavra-passe',
                    newacc: 'Criar Uma  Nova Conta',
                    login: 'ENTRAR',
                    validemail: 'Insira um  email válido',
                    resetpasss: 'Redefinir a palavra-passe',
                    emailsent: 'O seu email foi enviado. Para redefinir a sua palavra-passe',
                    review: 'Dê um testemunho',
                    rate: 'Avaliar',
                    rateexp: 'Avalie a sua experiência',
                    alertcar: 'Selecione pelo menos uma estrela',
                    promo: 'Promoção',
                    enteruid: 'Insira a sua UID de partilha',
                    uid: 'UID de Partilha',
                    cardf: 'Por favor adicione um cartão de pagamento',
                    shareid: 'Isso é uma UID de Partilha',
                    shareid2: 'Isso não é uma UID de Partilha',
                    shareid3: 'Não possui UID de Partilha',
                    name: 'Nome',
                    phone: 'Phone',
                    profile: 'Perfil',
                    choosefrom: 'Escolha De',
                    camera: 'Camera',
                    file: 'Ficheiro',
                    cancel: 'Cancelar',
                    pictureset: 'Foto de perfil definido com Sucesso',
                    failed: 'Insucesso. Por favor tente outra vez',
                    resend: 'REENVIAR',
                    details: 'Adicione os detalhes em falta',
                    countrycode: 'Indicativo do país',
                    payment: 'Recibo',
                    pay: 'PAGAR',
                    safety: 'Por favor certifique-se que se encontra no seu local de destino antes de pagar. Para evitar fraudes',
                    regards: 'Muito Obrigado por usar',
                    payment2: 'Pagamento',
                    entercode: 'Insira o código promocional',
                    retryi: 'Voltar Em',
                    secs: 'Segundos',
                    fastandconv: 'Rápido e conveniente',
                    signup: 'CRIAR CONTA',
                    facebook: 'FACEBOOK',
                    price: 'PREÇO',
                    location: 'LUGAR',
                    destination: 'DESTINO',
                    detailsp: 'Detalhes',
                    triphistory: 'Minhas viagens',
                    freeride: 'Viagens grátis',
                    facebookl: 'Facebook',
                    whatsapp: 'Whatsapp',
                    instagram: 'Instagram',
                    half1: 'Partilhe o seu código promocional com os seus amigos. Basta eles usarem',
                    half2: 'voce ganhará também viagens gratuitas.',
                    adapt: 'Esse  é o meu ID de partilha para',
                    calc: 'Cálculo da corrida',
                    pvc: 'Os preços podem variar segundo diversos factores e circunstâncias. Tais como o estado do tempo etc',
                    priceappear: 'O preço aparecerá aqui',
                    scan: 'Escaneie o cartão de pagamento',
                    message: 'Mensagem',
                    cvc: 'Cvc',
                    year: 'Ano',
                    month: 'Mês',
                    confirm: 'Faremos um débito para confirmar o cartão.',
                    cardsuc: 'Cartão Ativado com Sucesso',
                    cardunc: 'Erro ao Ativar o Cartão',
                    drop: 'Chegou ao Seu Destino. Por favor realize o pagamento.',
                    retrynew: 'Voltar',
                    about: 'Acerca de',
                    lang: 'Língua'
                }
            ];
        }
        if (this.targetLanguageCode === 'rs') {
            return [
                {
                    whereTo: 'куда?',
                    set: 'набор PIN-кода на локации',
                    dest: 'пункт назначения',
                    done: 'Готово',
                    economy: 'эконом',
                    pool: 'POOL',
                    cash: 'наличными',
                    wrongdest: 'неправильная направления',
                    arrival: 'водитель прибывает ',
                    grut: 'сетевая ошибка. Проверьте подключение к интернету',
                    card: 'карта',
                    addcard: 'Добавить карту',
                    request: 'запросит поездок',
                    someone: 'ездить с кем-то',
                    getcharged: 'платить :',
                    join: 'присоединяйтесь',
                    start: 'пуск',
                    proccessingcharge: 'обработка..',
                    picknote: 'Новый пассажир Пикап. У вас меньше 60 секунд, чтобы принять задание.',
                    passenger_add_trip: 'New Stop has been added by Passenger. Please ensure that is right',
                    unfortunate: 'к сожалению,',
                    notavailable: 'недоступна в вашем регионе.',
                    just: 'одну секунду',
                    fbacc: 'в Вашем аккаунте Facebook нет электронной почты.',
                    retry: 'ошибка сети. Повторить',
                    home: 'пожалуйста, добавьте дом и работу в настройках',
                    notrate: 'не оценили',
                    nodriver: 'драйверы не найден',
                    whycancel: 'почему Вы хотите отменить',
                    chose: 'Пожалуйста, выберите вариант',
                    longpick: 'длинный Пикап ?',
                    incorrect: 'неправильный запрос  ?',
                    reject: 'отклонить',
                    accept: 'принять',
                    cantexit: 'извините, не могу выйти в это время',
                    lost: 'соединение потеряно, попытка подключения к интернету. Повторение..',
                    recover: 'пожалуйста, подождите, пока мы восстановить потерянную информацию',
                    driverarive: 'водитель прибывает ',
                    drive: 'вождение к месту назначения',
                    drivercancel: 'водитель отменен',
                    end: 'Ваше путешествие закончилось',
                    startride: 'Ваше путешествие началось',
                    paidcash: 'подтвердите, что Вы заплатили наличными',
                    poolcancel: 'отменен',
                    waiting: 'пожалуйста, подождите...',
                    driverfound: 'водитель найден',
                    check: 'оформить',
                    website: 'Website',
                    career: 'Careers',
                    faq: 'FAQ',
                    address: 'вопросы',
                    add: 'введите',
                    homesearch: 'дом',
                    worksearch: 'работа',
                    rideSch: 'Планоровать езду',
                    rideSchu: 'Вы запланировали поездку',
                    from: 'от',
                    to: 'К',
                    date: 'выберите дату',
                    clear: 'Отмена',
                    addDest: 'пожалуйста, добавьте Пункт назначения',
                    addTime: 'Wrong Time',
                    sucsch: 'удачно спланированная поездка',
                    sucschw: 'успешно отменен езда',
                    error: 'Вы уверены ?',
                    report: 'ваша жалоба была подана, мы свяжемся с вами. Через несколько минут.',
                    support: 'Поддержка',
                    complaint: 'написать жалобу',
                    submit: 'отправить',
                    customercare: 'CustomerCare',
                    email: 'Электронная почта',
                    password: 'пароль',
                    newacc: 'создать новую учетную запись',
                    login: 'логин',
                    validemail: 'введите допустимый адрес электронной почты',
                    resetpasss: 'сброс пароля',
                    emailsent: 'ваше письмо Отправлено. Для восстановления пароля',
                    review: 'написать отзыв',
                    rate: 'ставка',
                    rateexp: 'оцените свой опыт',
                    alertcar: 'выберите хотя бы одну звезду',
                    promo: 'промо',
                    enteruid: 'введите Общий UID',
                    uid: 'sharing UID',
                    cardf: 'пожалуйста, добавьте карту',
                    shareid: 'это общий UID',
                    shareid2: 'это не Общий UID',
                    shareid3: 'нет общего UID',
                    name: 'имя',
                    phone: 'Телефон',
                    profile: 'профиль',
                    choosefrom: 'выбор',
                    camera: 'камера',
                    file: 'файл',
                    cancel: 'Отмена',
                    pictureset: 'успешно',
                    failed: 'не удалось. Пожалуйста, Попробуйте Еще Раз',
                    resend: 'повторить',
                    details: 'Добавить недостающие детали',
                    countrycode: 'код страны',
                    payment: 'чек',
                    pay: 'оплатить',
                    // tslint:disable-next-line: max-line-length
                    safety: 'пожалуйста, убедитесь, что вы находитесь в нужном месте перед отъездом. Чтобы избежать мошенничества',
                    regards: 'спасибо за использование',
                    payment2: 'платеж',
                    entercode: 'введите промо-код',
                    retryi: 'повтор',
                    secs: 'секунд',
                    fastandconv: 'быстро и удобно',
                    signup: 'Регистрация',
                    facebook: 'FACEBOOK',
                    price: 'цена',
                    location: 'место',
                    destination: 'пункт назначения',
                    detailsp: 'детали',
                    triphistory: 'История путешествия',
                    freeride: 'бесплатные поездки',
                    facebookl: 'Facebook',
                    whatsapp: 'Whatsapp',
                    instagram: 'Instagram',
                    half1: 'поделитесь своим промо-кодом с друзьями. Как только они попробовали',
                    half2: 'Вы также получите бесплатную поездку.',
                    adapt: 'это мой ID акцию ',
                    calc: 'калькулятор поездки',
                    // tslint:disable-next-line: max-line-length
                    pvc: 'цены могут меняться в зависимости от непредвиденных обстоятельств. Такие как изменения погоды и т. д.',
                    priceappear: 'появится',
                    scan: 'карта сканирования',
                    message: 'сообщение',
                    cvc: 'Cvc',
                    year: 'год"',
                    month: 'месяц',
                    confirm: 'мы сделаем оплату для того чтобы подтвердить карточку..',
                    cardsuc: 'карта успешно активирована',
                    cardunc: 'карта неуспешно активирована',
                    drop: 'Вы доехали.',
                    retrynew: 'повторить',
                    about: 'Инфо',
                    lang: 'Língua'
                }
            ];
        }
        if (this.targetLanguageCode === 'az') {
            return [
                {
                    whereTo: 'Haraya?',
                    set: 'Xəritədə nöqtəni qeyd edin',
                    dest: 'Gedəcəyiniz yer',
                    done: 'Tamam',
                    economy: 'Ekonom',
                    pool: 'Ümumi',
                    cash: 'Nağd',
                    wrongdest: 'Yalnış təyinat nöqtəsi',
                    arrival: 'Sürücünün gəlmə vaxtı ',
                    grut: 'Şəbəkə xətası.İnternet bağlantınızı yoxlayın',
                    card: 'Kart',
                    addcard: 'Kart əlavə edin',
                    request: 'Səyahət sorğula',
                    someone: 'Başqa birisi ilə get',
                    getcharged: 'Çıxıldı :',
                    join: 'Ümumi gedişə qoşul',
                    start: 'Ümumi gedişi başla',
                    proccessingcharge: 'Balansdan çıxılma..',
                    picknote: 'Yeni sifariş var. Sifarişi qəbul etməyiniz üçün 60 saniyəniz var.',
                    passenger_add_trip: 'New Stop has been added by Passenger. Please ensure that is right',
                    unfortunate: 'Təəssüf,',
                    notavailable: 'sizin ərazidə mövcud deyil.',
                    just: 'Gözləyin',
                    fbacc: 'Sizin facebook hesabınızda email tapilmadı.',
                    retry: 'İnternet kəsildi, yenidən cəhd edin',
                    home: 'Ev və iş ünvanınızı ayarlarda əlavə edin',
                    notrate: 'Not Rated',
                    nodriver: 'Sürücülər tapılmadı',
                    whycancel: 'İmtinanın səbəbi nədir',
                    chose: 'Seçin',
                    longpick: 'Uzun müddət gözləmə?',
                    incorrect: 'Yalnış sorğu?',
                    reject: 'İmtina',
                    accept: 'Qəbul et',
                    cantexit: 'Bağışlayın, hal hazıda çıxmaq mümkün deyil',
                    lost: 'Əlaqə kəsildi, yeni əlaqəyə cəhd olunur..',
                    recover: 'Zəhmət olmasa informasiyanın bərpasını gözləyin',
                    driverarive: 'Sürücünun gəlmə vaxtı',
                    drive: 'Sizin təyinat nöqtənizə gəlinir',
                    drivercancel: 'Sürücü imtina etdi',
                    end: 'Səyahət başa çatdı',
                    startride: 'Səyahətini başladı',
                    paidcash: 'Nəğd ödədiyinizi təsdiqləyin',
                    poolcancel: 'Pool Cancelled. No Other Rider',
                    waiting: 'Gözləyin...',
                    driverfound: 'Sürücü tapıldı',
                    check: 'Ödəmə',
                    website: 'Veb sayt',
                    career: 'Daşınmalar',
                    faq: 'FAQ',
                    address: 'Axtarış',
                    add: 'Əlavə et',
                    homesearch: 'Ana səhifə',
                    worksearch: 'İş',
                    rideSch: 'Səyahət planlaşdır',
                    rideSchu: 'Siz səyahət planlaşdırdınız',
                    from: 'Dan',
                    to: 'Dək',
                    date: 'Tarixi seçin',
                    clear: 'İmtina et',
                    addDest: 'Zəhmət olmasa təyinat və çıxış məntəqənizi əlavə edin',
                    addTime: 'Yalnış vaxt',
                    sucsch: 'Succefully Scheduled A Ride',
                    sucschw: 'Succefully Cancelled Ride',
                    error: 'Əminsiniz?',
                    report: 'Sizin şikayətiniz göndərildi. Tezliklə sizə cavab veriləcək',
                    support: 'Dəstək',
                    complaint: 'Şikayət et',
                    submit: 'QƏBUL',
                    customercare: 'CustomerCare',
                    email: 'EMail',
                    password: 'Şifrə',
                    newacc: 'Yeni hesab yarat',
                    login: 'DAXİL OL',
                    validemail: 'Düzgün email daxil edin',
                    resetpasss: 'Şifrəni yenilə',
                    emailsent: 'Şifrəni yeniləmək üçün emailinizə qaydalar göndərildi',
                    review: 'Şərh yaz',
                    rate: 'Qiymətləndir',
                    rateexp: 'Təcrübəni qiymətləndir',
                    alertcar: 'Ən azı bir ulduz seçin',
                    promo: 'Promo',
                    enteruid: 'Sharing UID daxil edin',
                    uid: 'sharing UID',
                    cardf: 'Kart əlavə edin',
                    shareid: 'Bu Sharing UID',
                    shareid2: 'Bu  Sharing UID deyil',
                    shareid3: 'Sharing UID yoxdur',
                    name: 'Ad',
                    phone: 'Telefon',
                    profile: 'Profil',
                    choosefrom: 'Başlanğıcı seçin',
                    camera: 'Kamera',
                    file: 'Fayl',
                    cancel: 'İmtina',
                    pictureset: 'Profil şəkli uğurla yüklənildi',
                    failed: 'Uğursuz. Yenidən cəhd edin',
                    resend: 'YENİDƏN GÖNDƏR',
                    details: 'Qalan məlumatları əlavə edin',
                    countrycode: 'Ölkə kodu',
                    payment: 'Qəbz',
                    pay: 'Ödə',
                    safety: 'Əmin olun ki, lazım olan ünvandasınız',
                    regards: 'İstifadə etdiyiniz üçün təşəkkür edirik',
                    payment2: 'Ödəmə',
                    entercode: 'Promo kodu əlavə edin',
                    retryi: 'Cəhd edin',
                    secs: 'Saniyə',
                    fastandconv: 'Tez və rahat',
                    signup: 'BİZƏ QOŞUL',
                    facebook: 'FACEBOOK',
                    price: 'QİYMƏT',
                    location: 'ÜNVAN',
                    destination: 'TƏYİNAT',
                    detailsp: 'Detallar',
                    triphistory: 'Səhayət tarixi',
                    freeride: 'Pulsuz gedişlər',
                    facebookl: 'Facebook',
                    whatsapp: 'Whatsapp',
                    instagram: 'Instagram',
                    half1: 'Promo kodunuzu dostlarla bölüş',
                    half2: 'siz eyni zamanda pulsuz gedişlər əldə edəcəksiniz.',
                    adapt: 'Bu mənim share id mdir ',
                    calc: 'Səyahət hesablaylcısı',
                    pvc: 'Qiymət müxtəlif səbəblərə görə dəyişə bilər',
                    priceappear: 'Qiymət',
                    scan: 'Karti skan edin',
                    message: 'Mesaj',
                    cvc: 'Cvc',
                    year: 'İl',
                    month: 'Ay',
                    confirm: 'Kartin təsdiqi üçün ödəniş olunacaq.',
                    cardsuc: 'Kart aktivləşdirildi',
                    cardunc: 'Kartın aktivləşdirilməsi uğursuz başa çatdı',
                    drop: 'Səyahətiniz başa çatdı. Zəhmət olmasa ödəniş edin',
                    retrynew: 'Yenidən cəhd',
                    about: 'Haqqında',
                    lang: 'Língua'
                }
            ];
        }
    }
};
LanguageService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] }
];
LanguageService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"]])
], LanguageService);



/***/ }),

/***/ "./src/app/services/native-map-container.service.ts":
/*!**********************************************************!*\
  !*** ./src/app/services/native-map-container.service.ts ***!
  \**********************************************************/
/*! exports provided: NativeMapContainerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NativeMapContainerService", function() { return NativeMapContainerService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/google-maps */ "./node_modules/@ionic-native/google-maps/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _profile_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./profile.service */ "./src/app/services/profile.service.ts");
/* harmony import */ var _settings_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./settings.service */ "./src/app/services/settings.service.ts");
/* harmony import */ var _geocoder_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./geocoder.service */ "./src/app/services/geocoder.service.ts");
/* harmony import */ var _event_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./event.service */ "./src/app/services/event.service.ts");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");










let NativeMapContainerService = class NativeMapContainerService {
    // this.marker: Marker;
    constructor(eventProvider, toastCtrl, alert, settings, zone, myProf, gcode, platform, geo, storage) {
        this.eventProvider = eventProvider;
        this.toastCtrl = toastCtrl;
        this.alert = alert;
        this.settings = settings;
        this.zone = zone;
        this.myProf = myProf;
        this.gcode = gcode;
        this.platform = platform;
        this.geo = geo;
        this.storage = storage;
        this.onLocationbarHide = true;
        this.onDestinatiobarHide = true;
        this.CARS = [];
        this.pause = true;
        this.shove = true;
        this.speed = 50; // km/h
        this.canMess = true;
        this.cars = [];
        this.car_location = [];
        this.delay = 100;
        this.hasRequested = false;
        this.isCarAvailable = false;
        this.norideavailable = false;
        this.canShowchoiceTab = false;
        this.noGps = false;
        this.isLocationChange = false;
        this.onGpsEnabled = false;
        this.isNavigate = false;
        this.executiveStance = "none";
        this.tricycleStance = "none";
        this.standardStance = "none";
        this.closeDrivers = [];
        this.mapLoadComplete = false;
        this.driverCarType = 0;
        this.choseCar = false;
        this.toggleNav = true;
        this.isClear = false;
        this.onbar = false;
        this.classic = false;
        this.smallcar = false;
        this.pool = false;
        this.onbar1 = false;
        this.onbar2 = false;
        this.onbar3 = false;
        this.canShow = true;
        this.toggleBtn = false;
        this.onPointerHide = false;
        this.stopMovingUserDestination = false;
        this.stopMovingUsertoDriver = false;
        this.selected_destination_bar = false;
        this.pan = 0;
        this.NotifyTimes = -1;
        this.canCheck = true;
        this.isDriverAvailable = false;
        this.does = true;
        this.carMarker = [];
        this.ClearDetection = false;
        this.hasDone = false;
        this.hasStart = false;
        this.hasShown = true;
        this.correctLocationShown = false;
        this.hasShow = true;
        this.started = false;
        this.showDone = false;
        this.hasbooked = false;
        this.locationChange = true;
        this.hasAdded = false;
        this.hasCompleted = false;
        this.isDestination = false;
        this.CloseCars = [];
        this.hidelocator = true;
        this.ready = false;
        this.nodriver = true;
        this.isBooking = false;
        this.radius = 3000;
        this.isComingToUser = true;
        this.directionsService = new google.maps.DirectionsService();
        this.directionsDisplay = new google.maps.DirectionsRenderer();
        this.canDo = true;
        this.isDropped = false;
        this.frankdejong = false;
        this.driverRejected = true;
        this.hasPushed = false;
        this.isDone = true;
        this.isWalk = false;
        this.routeNumber = 0;
        this.should = true;
        this.route22 = [];
        this.actualPrice = 0;
        this.myTolls = [];
        this.surge = 0;
        this.yellow_markersArray = [];
        this.driver_markersArray = [];
        this.client_markersArray = [];
        this.flag_markersArray = [];
        this.car_markersArray = [];
        this.rider_markersArray = [];
        if (!this.platform.is("cordova")) {
            this.lat = 5.4966964;
            this.lng = 7.5297323;
        }
    }
    ///Start the cordova map
    loadMap() {
        let lat;
        let lng;
        let zoom;
        lat = 5.6027352;
        lng = -0.2095918;
        zoom = 19;
        let mapOptions = {
            camera: {
                target: {
                    lat: 5.614818,
                    lng: -0.205874,
                },
                zoom: zoom,
                tilt: 0,
            },
        };
        if (!this.platform.is("cordova")) {
            this.Geofiring();
            this.hasShown = true;
            this.lat = 40.65563;
            this.lng = -73.95025;
            this.gcode.locationName = "Devdex Software";
        }
        this.map = _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_2__["GoogleMaps"].create("map", mapOptions);
        this.map.one(_ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_2__["GoogleMapsEvent"].MAP_READY).then(() => {
            this.map.setMyLocationEnabled(true);
            this.map.setAllGesturesEnabled(true);
            this.map.setClickable(true);
            this.map.setCompassEnabled(true);
            this.hasStart = true;
            // this.directionsDisplay.setMap(this.map);
            this.watch2 = this.geo.watchPosition({ enableHighAccuracy: true, timeout: 10000 }).subscribe(position => {
                if (position.coords != undefined) {
                    var geoposition = position;
                    this.location = geoposition;
                    this.correctLocationShown = true;
                    this.lat = geoposition.coords.latitude,
                        this.lng = geoposition.coords.longitude;
                    this.storage.set("cMap.lat", this.lat);
                    this.storage.set("cMap.lng", this.lng);
                    this.gcode.Reverse_Geocode(this.lat, this.lng, false);
                    this.userPos = new google.maps.LatLng(this.lat, this.lng);
                    this.AnimateToLoc(this.lat, this.lng);
                }
                else {
                    var positionError = position;
                    // console.log('Error ' + positionError.code + ': ' + positionError.message);
                    // this.presentToast("Finding it difficult to get your current location. Please wait..");
                }
            });
        });
    }
    //check if gps is available by trying to getlocation info which automatically handles everything
    Restart() {
        this.isDropped = false;
        if (this.platform.is("cordova")) {
            this.map.getMyLocation().then(location => {
                this.lat = location.latLng.lat;
                this.lng = location.latLng.lng;
                this.storage.set("cMap.lat", this.lat);
                this.storage.set("cMap.lng", this.lng);
                this.AnimateToLoc(this.lat, this.lng);
                this.hasbooked = true;
                this.gcode.Reverse_Geocode(this.lat, this.lng, false);
                this.userPos = new google.maps.LatLng(this.lat, this.lng);
                this.location = location;
                this.canShow = false;
                this.hasShown = true;
                this.hasRequested = false;
                this.isDriverAvailable = false;
            });
        }
        else {
            this.gcode.Reverse_Geocode(this.lat, this.lng, false);
            this.userPos = new google.maps.LatLng(this.lat, this.lng);
            this.location = location;
            this.canShow = false;
            this.hasShown = true;
            this.hasRequested = false;
            this.isDriverAvailable = false;
        }
    }
    //Start the map touch detection
    PumpControls() {
        this.map.on(_ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_2__["GoogleMapsEvent"].CAMERA_MOVE_START).subscribe((start) => {
            this.hidelocator = false;
            this.pause = false;
            this.hasMoved = true;
        });
        this.map.on(_ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_2__["GoogleMapsEvent"].CAMERA_MOVE_END).subscribe((start) => {
            this.pause = true;
            //Check if the user has already booked a ride
            if (!this.hasRequested) {
                if (this.canCheck && this.ready) {
                    this.canCheck = false;
                }
                if (!this.platform.is("cordova")) {
                    this.lat = 4.883364;
                    this.lng = 7.025034;
                }
                else {
                    if (this.isLocationChange) {
                        let center = this.map.getCameraPosition();
                        if (!this.StopLocUpdate) {
                            this.lat = center.target.lat;
                            this.lng = center.target.lng;
                            this.storage.set("cMap.lat", this.lat);
                            this.storage.set("cMap.lng", this.lng);
                        }
                        this.gcode.Reverse_Geocode(this.lat, this.lng, false);
                    }
                    if (this.isDestinationChange) {
                        let center = this.map.getCameraPosition();
                        let lat = center.target.lat;
                        let lng = center.target.lng;
                        this.gcode.Reverse_Geocode(lat, lng, false);
                    }
                }
            }
        });
    }
    ///Animate to user location
    AnimateToLoc(lat, lng) {
        if (!this.hasbooked) {
            this.map
                .moveCamera({
                target: {
                    lat,
                    lng,
                },
                zoom: 19,
                tilt: 0,
                bearing: 0,
            })
                .then((distanceApart) => {
                this.lat = lat;
                this.lng = lng;
                this.storage.set("cMap.lat", this.lat);
                this.storage.set("cMap.lng", this.lng);
                this.canDo = true;
                this.PumpControls();
                this.Geofiring();
                this.statistic();
                this.canShow = false;
                this.map.setClickable(true);
                this.hasRequested = false;
                this.isDriverAvailable = false;
                const image_icon = {
                    url: "assets/icon/pin.png",
                    size: new google.maps.Size(40, 40),
                };
                if (this.yellow_markersArray.length >= 1) {
                    this.marker.setPosition({
                        lat: lat,
                        lng: lng
                    });
                    this.hasShown = true;
                    this.mapLoadComplete = true;
                }
                else if (this.yellow_markersArray.length < 1) {
                    this.map
                        .addMarker({
                        icon: image_icon,
                        position: {
                            lat,
                            lng,
                        },
                        rotation: 0,
                    })
                        .then((marker) => {
                        this.marker = marker;
                        this.yellow_markersArray.push(this.marker);
                        this.hasShown = true;
                        this.mapLoadComplete = true;
                    });
                }
            });
        }
        else {
            this.map
                .moveCamera({
                target: {
                    lat,
                    lng,
                },
                zoom: 19,
                tilt: 0,
                bearing: 0,
            })
                .then((distanceApart) => {
                this.lat = lat;
                this.lng = lng;
                this.storage.set("cMap.lat", this.lat);
                this.storage.set("cMap.lng", this.lng);
                this.canShow = false;
                this.map.setClickable(true);
                this.hasRequested = false;
                this.isDriverAvailable = false;
                this.Geofiring();
                const image_icon2 = {
                    url: "assets/icon/pin.png",
                    size: new google.maps.Size(40, 40),
                };
                //CHeck if yellow marker exist
                if (this.yellow_markersArray.length >= 1) {
                    this.marker.setPosition({
                        lat: lat,
                        lng: lng
                    });
                    let latlng = new google.maps.LatLng(lat, lng);
                }
                else if (this.yellow_markersArray.length < 1) {
                    this.map
                        .addMarker({
                        icon: image_icon2,
                        position: {
                            lat,
                            lng,
                        },
                    })
                        .then((marker) => {
                        this.marker = marker;
                        this.yellow_markersArray.push(this.marker);
                        this.hasShown = true;
                        this.canDo = true;
                    });
                }
            });
        }
        // });
    }
    statistic() {
        let myKey;
        this.nodriver = true;
        this.myProf.getAllDrivers().on("child_added", (driverSnapshot) => {
            this.Geofiring();
            if (!this.isBooking) {
                myKey = driverSnapshot.key;
                const driv_latitude = Number(driverSnapshot.val().driver_details[0]);
                const driv_longitude = Number(driverSnapshot.val().driver_details[1]);
                this.locations = [
                    [
                        driv_latitude,
                        driv_longitude,
                    ],
                ];
                var promises = this.locations.map((location, index) => {
                    return this.geoFireInstance.set(myKey, location).then(() => { });
                });
            }
        });
        this.myProf.getAllDrivers().on("child_changed", (driverSnapshot) => {
            if (!this.isBooking) {
                myKey = driverSnapshot.key;
                const driv_latitude2 = Number(driverSnapshot.val().driver_details[0]);
                const driv_longitude2 = Number(driverSnapshot.val().driver_details[1]);
                // Specify the locations for each fish
                this.locations = [
                    [
                        driv_latitude2,
                        driv_longitude2,
                    ],
                ];
                var promises = this.locations.map((location, index) => {
                    return this.geoFireInstance.set(myKey, location).then(() => { });
                });
            }
        });
        this.myProf.getAllDrivers().on("child_removed", (driverSnapshot) => {
            if (!this.isBooking) {
                if (driverSnapshot.val()) {
                    this.nodriver = false;
                }
                else {
                    this.nodriver = true;
                }
                this.myProf.geofireDrivers.child(driverSnapshot.key).remove();
                this.myProf.getAllDrivers().off("value");
            }
        });
    }
    Geofiring() {
        this.markers = [];
        this.car_notificationIds = [];
        this.car_notificationIds.length = 0;
        this.driverHere = false;
        this.allcar_array = [];
        this.locations = null;
        this.locations = [];
        var i;
        var a;
        this.geoFireInstance = new geofire.GeoFire(this.myProf.geofireDrivers);
        // Create a GeoQuery centered at fish2
        var geoQuery = this.geoFireInstance.query({
            center: [this.lat, this.lng],
            radius: this.settings.apart,
        });
        let p_Arry = [];
        let v_Arry = [];
        this.keyEntered = geoQuery.on("key_entered", (key, location, distance) => {
            this.isDone = true;
            if (!this.isBooking) {
                this.myProf
                    .getAllDrivers()
                    .child(key)
                    .once("value", (myShot) => {
                    this.nodriver = false;
                    if (this.platform.is("cordova")) {
                        if (myShot.val()) {
                            this.AddCar(myShot);
                        }
                    }
                    if (myShot.val() != null) {
                        if (myShot.val().driver_details[0])
                            this.car_notificationIds.push([
                                myShot.val().driver_details[0],
                                myShot.val().driver_details[1],
                                myShot.val().driver_details[2],
                                myShot.key,
                                myShot.val().driver_details[3],
                                myShot.val().driver_details[4],
                                myShot.val().driver_details[5],
                                myShot.val().driver_details[6],
                                myShot.val().driver_details[7],
                                myShot.val().driver_details[8],
                            ]);
                        this.hasPushed = true;
                        this.car_notificationIds.reduce((acc, current) => {
                            const x = acc.find((item) => item[4] === current[4]);
                            if (!x) {
                                this.allcar_array = acc.concat([current]);
                                return acc.concat([current]);
                            }
                            else {
                                return acc;
                            }
                        }, []);
                    }
                    else {
                        this.myProf.geofireDrivers.child(key).remove();
                    }
                });
            }
        });
        this.exited = geoQuery.on("key_exited", (key, location, distance) => {
            if (!this.isBooking) {
                if (this.platform.is("cordova")) {
                    if (this.markers[key])
                        this.markers[key].remove();
                }
                for (i = 0; i < this.car_notificationIds.length; i++) {
                    if (this.car_notificationIds[i][3] == key) {
                        this.car_notificationIds.splice(i, 1);
                        return true;
                    }
                    else {
                    }
                }
            }
        });
        this.moved = geoQuery.on("key_moved", (key, location, distance) => {
            if (!this.isBooking) {
                this.myProf
                    .getAllDrivers()
                    .child(key)
                    .once("value", (myShot) => {
                    if (this.platform.is("cordova")) {
                        if (this.markers[myShot.key]) {
                            this.markers[myShot.key].remove();
                            this.AddCar(myShot);
                        }
                    }
                    this.myProf.getAllDrivers().child(key).off("value");
                });
            }
        });
    }
    presentToast(message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: message,
                duration: 3000,
                position: "top",
            });
            toast.onDidDismiss().then(() => {
            });
            yield toast.present();
        });
    }
    AddCar(data) {
        var uluru = {
            lat: data.val().driver_details[0],
            lng: data.val().driver_details[1],
        };
        if (this.car_markersArray.length >= 1) {
            this.car.setPosition({
                lat: data.val().driver_details[0],
                lng: data.val().driver_details[1]
            });
        }
        else if (this.car_markersArray.length < 1) {
            this.map
                .addMarker({
                title: "",
                icon: {
                    url: "assets/icon/car.png",
                    size: {
                        width: 70,
                        height: 45,
                    },
                },
                position: uluru,
                rotation: data.val().driver_details[8],
            })
                .then((f) => {
                this.markers[data.key] = f;
                this.car = f;
                this.car_markersArray.push(this.car);
            });
        }
    }
    //Show distance between driver and User in the map
    setMarkers(lat, lng, uid) {
        if (this.watch2) {
            this.watch2.unsubscribe();
        }
        this.driver_lat = lat;
        this.driver_lng = lng;
        this.uid = uid;
        this.destination = new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_2__["LatLng"](lat, lng);
        this.isPickedUp = true;
        let location = new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_2__["LatLng"](this.driver_lat, this.driver_lng);
        this.MyLat = this.lat;
        this.MyLng = this.lng;
        this.myProf
            .getUserAsClientInfo()
            .child(uid)
            .on("child_changed", (driverSnap) => {
            if (driverSnap.val()) {
                //DRIVER LOCATION COORDINATES
                this.driver_lat = driverSnap.val().Driver_location[0];
                this.driver_lng = driverSnap.val().Driver_location[1];
                this.driver_bearing = driverSnap.val().Driver_bearing;
                let location = new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_2__["LatLng"](this.driver_lat, this.driver_lng);
                this.destination = new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_2__["LatLng"](driverSnap.val().Driver_location[0], driverSnap.val().Driver_location[1]);
            }
            //this.myProf.getUserAsClientInfo().child(uid).off("value");
            // });
        });
        this.WatchToPick();
    }
    WatchToPick() {
        if (this.watch2) {
            this.watch2.unsubscribe();
        }
        if (this.passenger) {
            this.passenger.remove();
            this.rider_markersArray = [];
        }
        if (this.client) {
            this.client.remove();
            this.client_markersArray = [];
        }
        if (this.driver) {
            this.driver.remove();
            this.driver_markersArray = [];
        }
        if (this.flag) {
            this.flag.remove();
            this.flag_markersArray = [];
        }
        let pope = true;
        this.watch_to_pick = this.geo.watchPosition({ enableHighAccuracy: true, timeout: 10000 }).subscribe(position => {
            if (position.coords != undefined) {
                var geoposition = position;
                this.location = geoposition;
                this.rider_lat = geoposition.coords.latitude,
                    this.rider_lng = geoposition.coords.longitude;
                this.rider_bearing = geoposition.coords.heading;
                this.route = [];
                if (this.isPickedUp) {
                    this.directionsService.route({
                        origin: new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_2__["LatLng"](this.rider_lat, this.rider_lng),
                        destination: this.destination,
                        travelMode: google.maps.TravelMode.DRIVING,
                        provideRouteAlternatives: true,
                    }, (res, status) => {
                        if (this.isPickedUp) {
                            if (status == google.maps.DirectionsStatus.OK) {
                                let gole;
                                if (res.routes.length > 1) {
                                    gole = this.routeNumber;
                                }
                                else {
                                    gole = 0;
                                }
                                let point;
                                if (res.routes[gole].overview_path) {
                                    for (let i = 0, len = res.routes[gole].overview_path.length; i < len; i++) {
                                        point = res.routes[gole].overview_path[i];
                                        this.route[i] = {
                                            lat: Number(point.lat()),
                                            lng: Number(point.lng()),
                                        };
                                    }
                                }
                                if (this.Line) {
                                    this.Line.remove();
                                }
                                if (this.Line2) {
                                    this.Line2.remove();
                                }
                                this.AddPolyLine();
                                this.MoveDriver(this.driver_lat, this.driver_lng, this.driver_bearing);
                                this.showPassenger(this.rider_lat, this.rider_lng);
                                this.myProf.UpdateUserLocation(this.rider_lat, this.rider_lng, this.uid, this.rider_bearing);
                                // setTimeout(() => {
                                // if (pope) {
                                this.Measure(this.rider_lat, this.rider_lng, this.driver_lat, this.driver_lng);
                                // if (point)
                                // this.map.animateCamera({
                                //   target: point,
                                //   duration: 500,
                                // });
                                //     pope = false;
                                // }
                                // }, 1000);
                                // let path: ILatLng[] = [{ "lat": this.driver_lat, "lng": this.driver_lng }, { "lat": this.rider_lat, "lng": this.rider_lng }];
                                // let latLngBounds2 = new LatLngBounds(path);
                                // this.map.animateCamera({
                                //   target: latLngBounds2,
                                //   duration: 500,
                                // });
                                // var bounds = [{ "lat": this.driver_lat, "lng": this.driver_lng },{ "lat": this.rider_lat, "lng": this.rider_lng },];
                                // this.map.moveCamera({
                                //   'target': bounds,
                                // });
                            }
                            else {
                                if (this.Line) {
                                    this.Line.remove();
                                }
                                if (this.Line2) {
                                    this.Line2.remove();
                                }
                            }
                        }
                        else {
                        }
                    });
                }
                else {
                }
            }
            else {
            }
        });
        // });
    }
    MoveDriver(lat, lng, bearing) {
        if (this.driver_markersArray.length >= 1) {
            this.driver.setPosition({
                lat: lat,
                lng: lng
            });
            this.driver.setRotation(bearing);
        }
        else if (this.driver_markersArray.length < 1) {
            const myIcon = {
                url: "assets/icon/map-taxi.png",
                size: new google.maps.Size(40, 40),
            };
            this.map
                .addMarker({
                title: "",
                icon: myIcon,
                position: {
                    lat: lat,
                    lng: lng,
                },
            })
                .then((marker) => {
                this.driver = marker;
                this.driver_markersArray.push(this.driver);
            });
        }
    }
    MoveClient(lat, lng) {
        // if (this.client) {
        //   this.client.remove();
        // }
        if (this.client_markersArray.length >= 1) {
            this.client.setPosition({
                lat: lat,
                lng: lng
            });
        }
        else if (this.client_markersArray.length < 1) {
            const riderIcon = {
                url: "assets/icon/rider.png",
                size: new google.maps.Size(40, 40),
            };
            this.map
                .addMarker({
                title: "",
                icon: riderIcon,
                position: {
                    lat: lat,
                    lng: lng,
                },
            })
                .then((markdfder) => {
                this.client = markdfder;
                this.client_markersArray.push(this.client);
            });
        }
    }
    showPassenger(lat, lng) {
        if (this.rider_markersArray.length >= 1) {
            this.passenger.setPosition({
                lat: lat,
                lng: lng
            });
        }
        else if (this.rider_markersArray.length < 1) {
            const riderIcon = {
                url: "assets/icon/rider.png",
                size: new google.maps.Size(40, 40),
            };
            this.map
                .addMarker({
                title: "",
                icon: riderIcon,
                position: {
                    lat: lat,
                    lng: lng,
                },
            })
                .then((markdfder) => {
                this.passenger = markdfder;
                this.rider_markersArray.push(this.passenger);
            });
        }
    }
    MoveFlag(lat, lng) {
        if (this.flag_markersArray.length >= 1) {
            this.flag.setPosition({
                lat: lat,
                lng: lng
            });
        }
        else if (this.flag_markersArray.length < 1) {
            // if (this.flag) {
            //   this.flag.remove();
            // }
            const rflagIcon = {
                url: "assets/icon/flag2.png",
                size: new google.maps.Size(40, 40),
            };
            this.map
                .addMarker({
                title: "",
                icon: rflagIcon,
                position: {
                    lat: lat,
                    lng: lng,
                },
            })
                .then((markdfder) => {
                this.flag = markdfder;
                this.flag_markersArray.push(this.flag);
            });
        }
    }
    //Show distance between driver and User in the map
    setMarkersDestination(lat, lng, uid) {
        this.destination = new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_2__["LatLng"](lat, lng);
        this.isPickedUp = false;
        this.isDropped = true;
        this.flag_Lat = lat;
        this.flag_Lng = lng;
        this.uid = uid;
        // this.route = [];
        this.myProf
            .getUserAsClientInfo()
            .child(uid)
            .on("child_changed", (driverSnap) => {
            if (driverSnap.val()) {
                this.driver_lat = driverSnap.val().Driver_location[0];
                this.driver_lng = driverSnap.val().Driver_location[1];
                this.driver_bearing = driverSnap.val().Driver_bearing;
                let location = new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_2__["LatLng"](this.driver_lat, this.driver_lng);
                this.destination = new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_2__["LatLng"](driverSnap.val().Driver_location[0], driverSnap.val().Driver_location[1]);
            }
            //this.myProf.getUserAsClientInfo().child(uid).off("value");
            // });
        });
        this.WatchToDrop();
    }
    WatchToDrop() {
        if (this.watch_to_pick) {
            this.watch_to_pick.unsubscribe();
        }
        if (this.watch2) {
            this.watch2.unsubscribe();
        }
        if (this.passenger) {
            this.passenger.remove();
            this.rider_markersArray = [];
        }
        if (this.client) {
            this.client.remove();
            this.client_markersArray = [];
        }
        if (this.driver) {
            this.driver.remove();
            this.driver_markersArray = [];
        }
        if (this.flag) {
            this.flag.remove();
            this.flag_markersArray = [];
        }
        let pope = true;
        this.watch_to_drop = this.geo.watchPosition({ enableHighAccuracy: true, timeout: 10000 }).subscribe(position => {
            if (position.coords != undefined) {
                var geoposition = position;
                this.location = geoposition;
                this.rider_lat = geoposition.coords.latitude,
                    this.rider_lng = geoposition.coords.longitude;
                this.rider_bearing = geoposition.coords.heading;
                this.route22 = [];
                this.destination_route = [];
                // this.route = [];
                this.directionsService.route({
                    origin: new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_2__["LatLng"](this.rider_lat, this.rider_lng),
                    destination: this.destination,
                    travelMode: google.maps.TravelMode.DRIVING,
                    provideRouteAlternatives: true,
                }, (res, status) => {
                    if (this.hasCompleted == false) {
                        if (status == google.maps.DirectionsStatus.OK) {
                            //  this.directionsDisplay.setDirections(res);
                            let gole;
                            if (res.routes.length > 1) {
                                gole = this.routeNumber;
                            }
                            else {
                                gole = 0;
                            }
                            let point;
                            if (res.routes[gole].overview_path) {
                                for (let i = 0, len = res.routes[gole].overview_path.length; i < len; i++) {
                                    point = res.routes[gole].overview_path[i];
                                    this.route22[i] = {
                                        lat: Number(point.lat()),
                                        lng: Number(point.lng()),
                                    };
                                    this.destination_route[i] = {
                                        lat: Number(point.lat()),
                                        lng: Number(point.lng()),
                                    };
                                }
                            }
                            if (this.Line) {
                                this.Line.remove();
                            }
                            if (this.Line2) {
                                this.Line2.remove();
                            }
                            this.AddPolyLineDestination();
                            this.MoveDriver(this.driver_lat, this.driver_lng, this.rider_bearing);
                            this.MoveFlag(this.flag_Lat, this.flag_Lng);
                            // setTimeout(() => {
                            //   if (pope) {
                            this.Measure(this.flag_Lat, this.flag_Lng, this.driver_lat, this.driver_lng);
                        }
                    }
                    else {
                        if (this.Line) {
                            this.Line.remove();
                        }
                        if (this.Line2) {
                            this.Line2.remove();
                        }
                    }
                });
            }
            else {
            }
        });
    }
    AddPolyLine() {
        this.map
            .addPolyline({
            color: "#fbb91d",
            visible: true,
            width: 7,
            points: this.route,
        })
            .then((ff) => {
            this.Line2 = ff;
        });
    }
    AddPolyLineDestination() {
        this.map
            .addPolyline({
            color: "#fbb91d",
            visible: true,
            width: 7,
            points: this.destination_route,
        })
            .then((ff) => {
            this.Line = ff;
        });
    }
    Measure(lat, lng, lat2, lng2) {
        let arrayOfLatLng = [new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_2__["LatLng"](lat, lng), new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_2__["LatLng"](lat2, lng2)];
        let bounds = new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_2__["LatLngBounds"](arrayOfLatLng);
        let center = bounds.getCenter();
        var mapElement = document.getElementById("map");
        var mapDimensions = {
            height: mapElement.offsetHeight,
            width: mapElement.offsetWidth,
        };
        // var zoom = this.getBoundsZoomLevel(bounds, mapDimensions);
        // console.log("ZOOM VAR:", zoom);
        // this.map
        //   .animateCamera({
        //     target: center,
        //     zoom: zoom,
        //     duration: 200,
        //   })
        //   .then(() => { });
        this.map
            .animateCamera({
            target: bounds,
            duration: 500,
        })
            .then(() => {
            this.isNavigate = true;
        });
        // this.map.moveCamera({
        //   'target': center,
        // });
    }
    getBoundsZoomLevel(bounds, mapDim) {
        // var WORLD_DIM = { height: 180 / 2.05, width: 160 / 2.05 };
        var WORLD_DIM = { height: 96 / 2.05, width: 96 / 2.05 };
        var ZOOM_MAX = 13;
        var ne = bounds.northeast;
        var sw = bounds.southwest;
        var latFraction = (this.latRad(ne.lat) - this.latRad(sw.lat)) / Math.PI;
        var lngDiff = ne.lng - sw.lng;
        var lngFraction = ((lngDiff < 0 ? lngDiff + 360 * 1.16 : lngDiff) / 360) * 1.16;
        var latZoom = this.zoom(mapDim.height, WORLD_DIM.height, latFraction);
        var lngZoom = this.zoom(mapDim.width, WORLD_DIM.width, lngFraction);
        return Math.min(latZoom, lngZoom, ZOOM_MAX);
    }
    latRad(lat) {
        var sin = Math.sin((lat * Math.PI) / 180);
        var radX2 = Math.log((1 + sin) / (1 - sin)) / 2;
        return Math.max(Math.min(radX2, Math.PI), -Math.PI) / 2;
    }
    zoom(mapPx, worldPx, fraction) {
        return Math.floor(Math.log(mapPx / worldPx / fraction) / Math.LN2);
    }
    ShowDestination(loc1, loc2) {
        if (this.watch_to_pick) {
            this.watch_to_pick.unsubscribe();
        }
        if (this.watch2) {
            this.watch2.unsubscribe();
        }
        let t_Mode = google.maps.TravelMode.DRIVING;
        this.origin = new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_2__["LatLng"](loc1[0], loc1[1]);
        this.new_destination = new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_2__["LatLng"](loc2[0], loc2[1]);
        this.destination_route = [];
        this.directionsService.route({
            origin: new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_2__["LatLng"](loc1[0], loc1[1]),
            destination: new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_2__["LatLng"](loc2[0], loc2[1]),
            travelMode: t_Mode,
            provideRouteAlternatives: true,
        }, (res, status) => {
            if (status == google.maps.DirectionsStatus.OK) {
                let gole;
                if (res.routes.length > 1) {
                    gole = this.routeNumber;
                }
                else {
                    gole = 0;
                }
                let point;
                if (res.routes[gole].overview_path) {
                    for (let i = 0, len = res.routes[gole].overview_path.length; i < len; i++) {
                        point = res.routes[gole].overview_path[i];
                        this.destination_route[i] = {
                            lat: Number(point.lat()),
                            lng: Number(point.lng()),
                        };
                    }
                }
                //Show Rider At Location
                this.MoveClient(loc1[0], loc1[1]);
                //Display flag at Destination
                this.MoveFlag(loc2[0], loc2[1]);
                this.AddPolyLineDestination();
                this.Measure(loc1[0], loc1[1], loc2[0], loc2[1]);
                // let path: ILatLng[] = [{ "lat": loc1[0], "lng": loc1[1] }, { "lat": loc2[0], "lng": loc2[1] }];
                // let latLngBounds = new LatLngBounds(path);
                // this.map.animateCamera({
                //   target: latLngBounds,
                //   duration: 500,
                // });
                // var bounds = [{ "lat": loc1[0], "lng": loc1[1] }, { "lat": loc2[0], "lng": loc2[1] }];
                // this.map.moveCamera({
                //   'target': bounds,
                // });
                // this.map.setCameraZoom(19);
                //this.map.setCameraTarget(point);
            }
        });
        // this.directionsDisplay.setMap(this.map);
    }
};
NativeMapContainerService.ctorParameters = () => [
    { type: _event_service__WEBPACK_IMPORTED_MODULE_7__["EventService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
    { type: _settings_service__WEBPACK_IMPORTED_MODULE_5__["SettingsService"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"] },
    { type: _profile_service__WEBPACK_IMPORTED_MODULE_4__["ProfileService"] },
    { type: _geocoder_service__WEBPACK_IMPORTED_MODULE_6__["GeocoderService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"] },
    { type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_8__["Geolocation"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_9__["Storage"] }
];
NativeMapContainerService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: "root",
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_event_service__WEBPACK_IMPORTED_MODULE_7__["EventService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
        _settings_service__WEBPACK_IMPORTED_MODULE_5__["SettingsService"],
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"],
        _profile_service__WEBPACK_IMPORTED_MODULE_4__["ProfileService"],
        _geocoder_service__WEBPACK_IMPORTED_MODULE_6__["GeocoderService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"],
        _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_8__["Geolocation"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_9__["Storage"]])
], NativeMapContainerService);



/***/ }),

/***/ "./src/app/services/pop-up.service.ts":
/*!********************************************!*\
  !*** ./src/app/services/pop-up.service.ts ***!
  \********************************************/
/*! exports provided: PopUpService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PopUpService", function() { return PopUpService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _activity_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./activity.service */ "./src/app/services/activity.service.ts");
/* harmony import */ var _language_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./language.service */ "./src/app/services/language.service.ts");
/* harmony import */ var _native_map_container_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./native-map-container.service */ "./src/app/services/native-map-container.service.ts");
/* harmony import */ var _profile_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./profile.service */ "./src/app/services/profile.service.ts");










let PopUpService = class PopUpService {
    constructor(act, platform, lp, toastCtrl, storage, cMap, alert, ph, load) {
        this.act = act;
        this.platform = platform;
        this.lp = lp;
        this.toastCtrl = toastCtrl;
        this.storage = storage;
        this.cMap = cMap;
        this.alert = alert;
        this.ph = ph;
        this.load = load;
        this.onRequest = false;
        this.canDismiss = false;
        this.calculateBtn = false;
        this.allowed = true;
        this.hasCleared = false;
        this.driverEnded = true;
    }
    showAlertNormal(title, subtitle, network) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alert.create({
                header: title,
                subHeader: subtitle,
                buttons: [
                    {
                        text: this.lp.translate()[0].retrynew,
                        role: "cancel",
                        handler: () => {
                            if (network) {
                                this.clearAll(this.uid, true);
                            }
                        },
                    },
                ],
                backdropDismiss: false,
            });
            yield alert.present();
        });
    }
    showAlert(title, subtitle) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alert.create({
                header: title,
                subHeader: subtitle,
                buttons: [
                    {
                        text: this.lp.translate()[0].accept,
                        role: "cancel",
                        handler: () => {
                            this.cMap.map.setClickable(true);
                        },
                    },
                ],
                backdropDismiss: false,
            });
            yield alert.present();
        });
    }
    presentSimpleLoader(message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.load.create({
                // tslint:disable-next-line: object-literal-shorthand
                message: message,
            });
            yield loading.present();
            console.log("RUnnin SImple Loader");
            const myInterval = setTimeout(() => {
                loading.dismiss();
                clearTimeout(myInterval);
            }, 500);
        });
    }
    SmartLoader(message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.load.create({
                message: message,
            });
            yield loading.present();
            console.log("Running Loader");
            const myInterval = setInterval(() => {
                loading.dismiss();
                clearInterval(myInterval);
            }, 500);
        });
    }
    presentToast(message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                // tslint:disable-next-line: object-literal-shorthand
                message: message,
                duration: 3000,
                position: "top",
            });
            toast.onDidDismiss();
            toast.present();
        });
    }
    showToast(message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.toast = yield this.toastCtrl.create({
                // tslint:disable-next-line: object-literal-shorthand
                message: message,
                position: "top",
            });
            this.toast.present();
        });
    }
    hideToast() {
        this.toast.dismiss();
    }
    showPomp(title, message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alert.create({
                // tslint:disable-next-line: object-literal-shorthand
                header: title,
                subHeader: message,
                buttons: [
                    {
                        text: this.lp.translate()[0].accept,
                        role: "cancel",
                        handler: () => {
                            this.clearAll(this.uid, true);
                        },
                    },
                ],
                backdropDismiss: false,
            });
            yield alert.present();
        });
    }
    showPimp(title) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alert.create({
                // tslint:disable-next-line: object-literal-shorthand
                header: title,
                buttons: [
                    {
                        text: "OK",
                        role: "cancel",
                        handler: () => { },
                    },
                ],
                backdropDismiss: false,
            });
            yield alert.present();
        });
    }
    refactor() {
        this.cMap.onDestinatiobarHide = false;
        this.calculateBtn = false;
        document.getElementById("destination").innerHTML = "Set Destination";
    }
    alertClosure(message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alert.create({
                header: message,
                buttons: [
                    {
                        text: "Exit",
                        role: "cancel",
                        handler: () => { },
                    },
                ],
                backdropDismiss: true,
            });
            yield alert.present();
        });
    }
    clearAll(uid, can) {
        this.driverEnded = false;
        console.log(uid);
        // tslint:disable-next-line: prefer-const
        let customer = firebase__WEBPACK_IMPORTED_MODULE_3___default.a.database().ref(`Customer/${uid}`);
        customer
            .remove()
            .then((success) => { })
            .catch((error) => { });
    }
    locatePosition(lat, lng) { }
    presentRouteLoader(message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.load.create({
                // tslint:disable-next-line: object-literal-shorthand
                message: message,
            });
            yield loading.present();
            console.log("sdfggfsfsfsfs");
            const myInterval = setInterval(() => {
                if (this.canDismiss) {
                    loading.dismiss();
                    clearInterval(myInterval);
                }
            }, 1000);
        });
    }
    presentLoader(message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.dismissLoader = yield this.load.create({
                message: message,
            });
            this.dismissLoader.present();
            console.log("Running Loader");
        });
    }
    hideLoader() {
        this.dismissLoader.dismiss();
        console.log("LOADER DISMISSED");
    }
    showLoader2(message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.dismissLoader22 = yield this.load.create({
                message: message,
            });
            yield this.dismissLoader22.present();
            console.log("SHOW  LOADER");
        });
    }
    removeLoader2() {
        if (this.dismissLoader22)
            this.dismissLoader22.dismiss();
        //this.presentLoader = false;
        console.log("REMOVIN LOADER");
    }
    showAlertComplex(title, message, accept, reject, iscancel) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alert.create({
                header: title,
                // tslint:disable-next-line: object-literal-shorthand
                message: message,
                inputs: [
                    {
                        name: "long",
                        label: this.lp.translate()[0].longpick,
                        type: "checkbox",
                        value: "true",
                        checked: false,
                    },
                    {
                        name: "incorrect",
                        label: this.lp.translate()[0].incorrect,
                        type: "checkbox",
                        value: "false",
                        checked: false,
                    },
                ],
                buttons: [
                    {
                        text: this.lp.translate()[0].reject,
                        role: "cancel",
                        handler: () => {
                            // tslint:disable-next-line: no-trailing-whitespace
                        },
                    },
                    {
                        text: this.lp.translate()[0].accept,
                        handler: () => {
                            if (iscancel) {
                                this.clearAll(this.uid, true);
                            }
                        },
                    },
                ],
                backdropDismiss: false,
            });
            yield alert.present();
        });
    }
};
PopUpService.ctorParameters = () => [
    { type: _activity_service__WEBPACK_IMPORTED_MODULE_5__["ActivityService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
    { type: _language_service__WEBPACK_IMPORTED_MODULE_6__["LanguageService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"] },
    { type: _native_map_container_service__WEBPACK_IMPORTED_MODULE_7__["NativeMapContainerService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: _profile_service__WEBPACK_IMPORTED_MODULE_8__["ProfileService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] }
];
PopUpService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: "root",
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_activity_service__WEBPACK_IMPORTED_MODULE_5__["ActivityService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"],
        _language_service__WEBPACK_IMPORTED_MODULE_6__["LanguageService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"],
        _native_map_container_service__WEBPACK_IMPORTED_MODULE_7__["NativeMapContainerService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
        _profile_service__WEBPACK_IMPORTED_MODULE_8__["ProfileService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]])
], PopUpService);



/***/ }),

/***/ "./src/app/services/profile.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/profile.service.ts ***!
  \*********************************************/
/*! exports provided: ProfileService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileService", function() { return ProfileService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_2__);



let ProfileService = class ProfileService {
    constructor() {
        this.isHome = true;
        this.login = false;
        this.kit = false;
        this.ratingText = "";
        this.ratingValue = 0;
        this.loadingState = false;
        this.hasLoaded = false;
        this.referal = "rty";
        this.refEarning = 0;
        this.status = "Unverified";
        this.credits = 0;
        firebase__WEBPACK_IMPORTED_MODULE_2___default.a.auth().onAuthStateChanged((user) => {
            if (user) {
                // console.log(user)
                this.user = user;
                //console.log(this.user)
                this.id = this.user.uid;
                this.userProfil = firebase__WEBPACK_IMPORTED_MODULE_2___default.a.database().ref(`userProfile/`);
                this.customer = firebase__WEBPACK_IMPORTED_MODULE_2___default.a.database().ref(`Customer`);
                this.userProfile = firebase__WEBPACK_IMPORTED_MODULE_2___default.a
                    .database()
                    .ref(`userProfile/${user.uid}/userInfo`);
                this.IDProfile = firebase__WEBPACK_IMPORTED_MODULE_2___default.a.database().ref(`userProfile/`);
                this.WebAdminProfile = firebase__WEBPACK_IMPORTED_MODULE_2___default.a.database().ref(`DashboardSettings`);
                this.CancelledProfile = firebase__WEBPACK_IMPORTED_MODULE_2___default.a.database().ref(`Company`);
                this.DriverProfile = firebase__WEBPACK_IMPORTED_MODULE_2___default.a
                    .database()
                    .ref(`driverProfile/${user.uid}`);
                this.ScheduledProfile = firebase__WEBPACK_IMPORTED_MODULE_2___default.a.database().ref(`ScheduledRides`);
                this.uidProfile = firebase__WEBPACK_IMPORTED_MODULE_2___default.a.database().ref(`SharingID/`);
                this.companyProfile = firebase__WEBPACK_IMPORTED_MODULE_2___default.a.database().ref(`Company`);
                this.promoProfile = firebase__WEBPACK_IMPORTED_MODULE_2___default.a.database().ref("SharingIDPromo");
                this.usedPromo = firebase__WEBPACK_IMPORTED_MODULE_2___default.a.database().ref(`UsedPromo/${user.uid}`);
                this.pricing = firebase__WEBPACK_IMPORTED_MODULE_2___default.a.database().ref(`pricing`);
                //  this.getCode = firebase.database().ref(`SharingIDPromo/`).orderByChild("code").equalTo("54ca2c11d1afc1612871624a");
                //       ref.child("studentList")
                //  .orderByChild("name")
                //  .equalTo("54ca2c11d1afc1612871624a")
                //  .on("child_added", function(snapshot) {
                //     console.log(snapshot.val());
                //   });
                this.referalProfile = firebase__WEBPACK_IMPORTED_MODULE_2___default.a.database().ref(`userProfile/`);
                this.referalProfile2 = firebase__WEBPACK_IMPORTED_MODULE_2___default.a.database().ref(`driverProfile/`);
                this.companyNews = firebase__WEBPACK_IMPORTED_MODULE_2___default.a.database().ref(`News/User`);
                this.getDriverProfile().on("value", (userProfileSnapshot) => {
                    this.driver = userProfileSnapshot.val();
                });
                this.users = firebase__WEBPACK_IMPORTED_MODULE_2___default.a.database().ref(`userProfile`);
                this.drivers = firebase__WEBPACK_IMPORTED_MODULE_2___default.a.database().ref(`Drivers/AllDrivers`);
                this.geofireDrivers = firebase__WEBPACK_IMPORTED_MODULE_2___default.a.database().ref(`Drivers/Geofired`);
                this.pools = firebase__WEBPACK_IMPORTED_MODULE_2___default.a.database().ref(`PoolRides`);
                this.CustomerOwnPropertyRef = firebase__WEBPACK_IMPORTED_MODULE_2___default.a
                    .database()
                    .ref(`Customer/${user.uid}/client`);
                this.getUserProfil()
                    .child(this.id)
                    .on("child_added", (userProfileSnapshot) => {
                    console.log("SNAPSHOT ON GETUSERPROFIL::", userProfileSnapshot.val());
                    this.userID = userProfileSnapshot.val();
                    this.home = userProfileSnapshot.val().Home;
                    this.work = userProfileSnapshot.val().Work;
                    this.favoriteDriver = userProfileSnapshot.val().favorite;
                    this.phone = userProfileSnapshot.val().phonenumber;
                    this.pic = userProfileSnapshot.val().picture;
                    this.verificationID = userProfileSnapshot.val().random;
                    this.name = userProfileSnapshot.val().first_name;
                    this.credits = userProfileSnapshot.val().credits;
                    this.paymentType = userProfileSnapshot.val().payWith;
                    this.paymentToken = userProfileSnapshot.val().payment_token;
                    this.customerID = userProfileSnapshot.val().customerID;
                    this.card = userProfileSnapshot.val().Card_Number;
                    this.email = userProfileSnapshot.val().Card_email;
                    this.cvc = userProfileSnapshot.val().Card_Cvc;
                    this.year = userProfileSnapshot.val().Card_Year;
                    this.month = userProfileSnapshot.val().Card_month;
                    this.ratingText = userProfileSnapshot.val().ratingtext;
                    this.ratingValue = userProfileSnapshot.val().rating;
                    this.rating_positive = userProfileSnapshot.val().rating_positive;
                    this.rating_negative = userProfileSnapshot.val().rating_negative;
                    this.earnings = userProfileSnapshot.val().earnings;
                    this.status = userProfileSnapshot.val().verified;
                    if (userProfileSnapshot.val().referal) {
                        this.referal = userProfileSnapshot.val().referal;
                        let ref;
                        if (userProfileSnapshot.val().referalID) {
                            ref = userProfileSnapshot.val().referalID;
                        }
                        else {
                            ref = "etter";
                        }
                        console.log(ref);
                        this.convrefEarning = userProfileSnapshot.val().refEarning;
                        if (ref.replace(/[^A-Z,0-9]/gi, "").length == 4) {
                            this.getReferalProfile().on("value", (userProfileSnapshot) => {
                                if (userProfileSnapshot.val().refEarning)
                                    this.refEarning = userProfileSnapshot.val().refEarning;
                            });
                        }
                        else {
                            this.getReferalProfile2().on("value", (userProfileSnapshot) => {
                                if (userProfileSnapshot.val().refEarning)
                                    this.refEarning = userProfileSnapshot.val().refEarning;
                            });
                        }
                    }
                    console.log(this.referal);
                });
            }
        });
    }
    // createMyDocList(data, id): Promise<void> {
    //   return this.companyProfile
    //     .child("Rider")
    //     .child("documents")
    //     .child(`${id}`)
    //     .child(`${this.id}/`)
    //     .update({
    //       name: this.name,
    //       client: "Rider",
    //       data: data,
    //       comment: "none",
    //       expired: [2019, 6, 5],
    //       denied: "false",
    //       approved: "false",
    //     });
    // }
    // this.userProfileRef.child(newUser.user.uid).child("userInfo").set({
    //         email: email,
    //       });
    getUserProfile() {
        return this.userProfile;
    }
    getUserProfil() {
        return this.userProfil;
    }
    getyProfile() {
        return this.IDProfile;
    }
    getUserIDProfile(r) {
        return this.IDProfile.child(r);
    }
    getReferalProfile() {
        return this.referalProfile.child(this.referal);
    }
    getReferalProfile2() {
        return this.referalProfile2.child(this.referal);
    }
    getDriverProfile() {
        return this.referalProfile2;
    }
    getNewsProfile() {
        return this.companyNews;
    }
    getWebAdminProfile() {
        return this.WebAdminProfile;
    }
    getCancelledProfile() {
        return this.CancelledProfile;
    }
    getScheduledProfile(id) {
        return this.ScheduledProfile.child(id);
    }
    getUserAsClientInfo() {
        return this.customer;
    }
    getAllDrivers() {
        return this.drivers;
    }
    getAllUser() {
        return this.users;
    }
    getAllPool() {
        return this.pools;
    }
    getAllSharingID(code) {
        return this.uidProfile.child(code);
    }
    getAllSharingPromoID() {
        return this.promoProfile;
    }
    getAllUsedCodes() {
        return this.usedPromo;
    }
    getPricing() {
        return this.pricing;
    }
    getSharingID() {
        return this.uidProfile;
    }
    getCompanyProfile(id) {
        return this.companyProfile.child(`${id}/`);
    }
    getCompanies() {
        return this.companyProfile;
    }
    updateName(first_name) {
        return this.userProfile.update({
            name: first_name,
        });
    }
    updateRouteNumber(first_name) {
        return this.userProfile.update({
            routeNumber: first_name,
        });
    }
    RiderPromoSaved(rider_id, code, percentage, status) {
        return this.usedPromo.push({
            rider_id: rider_id,
            code: code,
            percentage: percentage,
            status: status,
        });
    }
    createDocList(data, id) {
        return this.companyProfile
            .child("Rider")
            .child("documents")
            .child(`${id}/`)
            .push({
            data: data,
        });
    }
    createMyDocList(data, id) {
        return this.companyProfile
            .child("Rider")
            .child("documents")
            .child(`${id}`)
            .child(`${this.id}/`)
            .update({
            name: this.name,
            client: "Rider",
            data: data,
            comment: "none",
            expired: [2019, 6, 5],
            denied: "false",
            approved: "false",
        });
    }
    uploadDocFile(data, id) {
        return this.companyProfile
            .child("Rider")
            .child("documents")
            .child(`${id}`)
            .child(`${this.id}/`)
            .update({
            name: this.name,
            client: "Rider",
            data: data,
            comment: "none",
            expired: [2019, 6, 5],
            denied: "false",
            approved: "false",
        });
    }
    uploadDocFile2(data, id) {
        return this.companyProfile
            .child("Rider")
            .child("documents")
            .child(`${id}`)
            .child(`${this.id}/`)
            .update({
            data: data,
        });
    }
    ///Forr Refer for ride
    updatePromoID(id) {
        return this.promoProfile.child(id).update({
            id: [this.id],
        });
    }
    createPromo(code, percentage) {
        return this.promoProfile.push({
            code: code,
            percentage: percentage,
        });
    }
    updateRideID(id) {
        return this.userProfile
            .update({
            idForRide: id,
        })
            .then(() => {
            this.updatePromoID(id);
        });
    }
    ///Forr Refer for cash
    updateID(first_name) {
        return this.userProfile.update({
            id: first_name,
        });
    }
    M_updateID(id, name) {
        console.log(id, name);
        return this.userProfile.child("favorite").child(id).update({
            name: name,
        });
    }
    M_updateIDP(id, name) {
        console.log(id, name);
        return this.userProfile.child("rejected").child(id).update({
            name: name,
        });
    }
    updateGUID(first_name) {
        return this.uidProfile
            .child(first_name)
            .update({
            id: [this.id],
        })
            .then(() => {
            this.updateID(first_name);
        });
    }
    updateEmail(first_name) {
        return this.userProfile.update({
            email: first_name,
        });
    }
    UpdateNumber(first_name, number) {
        return this.userProfile.update({
            name: first_name,
            phone: number,
            rating: 0,
            ratingtext: "Not Yet Rated",
        });
    }
    UpdateNumbers(number) {
        return this.userProfile.update({
            phone: number,
        });
    }
    UpdateEmergencyNumbers(number) {
        return this.userProfile.update({
            emergencyNumber: number,
        });
    }
    UpdateUserLocation(lat, lng, id, bearing) {
        console.log("upadating in database");
        return firebase__WEBPACK_IMPORTED_MODULE_2___default.a
            .database()
            .ref(`Customer/${id}/client`)
            .update({
            Client_location: [lat, lng],
            Client_Bearing: bearing
        });
    }
    UpdateEarnings(sum) {
        return this.userProfile.update({
            earnings: this.earnings + sum,
        });
    }
    UpdateRefEarnings(sum) {
        console.log(this.refEarning);
        return this.referalProfile.child(this.referal).update({
            refEarning: this.refEarning + sum,
        });
    }
    UpdateRefEarnings2(sum) {
        return this.referalProfile2.child(this.referal[0]).update({
            refEarning: this.refEarning + sum,
        });
    }
    UpdateHome(number) {
        return this.userProfile.update({
            Home: number,
        });
    }
    UpdateToll(number) {
        return this.userProfile.update({
            DeTol: number,
        });
    }
    UpdateBank(bank, account) {
        return this.userProfile.update({
            bank: bank,
            accountNumber: account,
        });
    }
    UpdateWork(number) {
        return this.userProfile.update({
            Work: number,
        });
    }
    UpdateCredits(number) {
        return this.userProfile.update({
            credits: number,
        });
    }
    UpdateDriverCredits(number, id) {
        return this.referalProfile2.child(id).child("userInfo").update({
            credits: number,
        });
    }
    UpdateReferal(number, id) {
        return this.userProfile.set({
            referal: number,
            referalID: id,
        });
    }
    updateDestination(number) {
        return this.userProfile.update({
            Work: number,
        });
    }
    createHistory(name, price, date, location, destination, realP, waitTimeCost) {
        return this.userProfile
            .child("/eventList")
            .push({
            user_name: this.name,
            name: name,
            price: price,
            date: date,
            location: location,
            destination: destination,
            realPrice: realP,
            waitTimeCost: waitTimeCost
        })
            .then((id) => {
            this.historyID = id;
            this.getUserProfile().on("value", (us) => {
                console.log(us.val(), id);
                if (us.val().Driver_id) {
                    this.pushToHistory(us.val().Driver_id, us.val().Driver_key, id.key);
                }
            });
        });
    }
    updateHistory(id, upvote, downvote, tip) {
        return this.userProfile
            .child("/eventList")
            .child(id)
            .update({
            tip: tip,
            upvote: upvote,
            downvote: downvote,
        })
            .then((id) => {
            this.historyID = id;
        });
    }
    pushToHistory(id, key, histID) {
        return this.userProfile
            .child("/eventList")
            .child(histID)
            .update({
            driver_id: id,
            driver_key: key,
        })
            .then((id) => {
            this.historyID = id;
        });
    }
    updateHistoryTip(price, id) {
        return this.userProfile.child("/eventList").child(id).update({
            tip: price,
        });
    }
    updateDriiverTip(price, id, key) {
        return this.referalProfile2
            .child(id)
            .child("userInfo")
            .child("/eventList")
            .child(key)
            .update({
            tip: price,
        });
    }
    updateHistoryVoteUp(id, up) {
        return this.userProfile.child("/eventList").child(id).update({
            upvote: up,
        });
    }
    updateHistoryVoteUpSingle(id, up) {
        return this.userProfile.child("/eventList").child(id).update({
            upvote: up,
        });
    }
    updateHistoryVoteDown(id, down) {
        return this.userProfile.child("/eventList").child(id).update({
            downvote: down,
        });
    }
    updateHistoryVoteDownSingle(id, down) {
        return this.userProfile.child("/eventList").child(id).update({
            downvote: down,
        });
    }
    UpdatePhoto(pic) {
        return this.userProfile.update({
            picture: pic,
        });
    }
    UpdateGuid(pic) {
        return this.userProfile.update({
            GUID: [pic, this.id],
        });
    }
    UpdateEarning(price) {
        return this.userProfile.update({
            earnings: price,
        });
    }
    UpdateUserRating(rate, text) {
        return this.userProfile.update({
            rating: rate,
            ratingtext: text,
        });
    }
    PushRandomNumber(number) {
        return this.userProfile.update({
            random: number,
        });
    }
    Complain(value) {
        return firebase__WEBPACK_IMPORTED_MODULE_2___default.a.database().ref(`DashboardSettings/user/complains`).push({
            complain: value,
            email: this.user.email,
        });
    }
    RateDriver(id, rScore, text, positive) {
        return firebase__WEBPACK_IMPORTED_MODULE_2___default.a.database().ref(`driverProfile/${id}/userInfo`).update({
            rating: rScore,
            review: text,
        });
    }
    Tip(id, val) {
        return firebase__WEBPACK_IMPORTED_MODULE_2___default.a.database().ref(`driverProfile/${id}/userInfo`).update({
            tip: val,
        });
    }
    PositiveRateDriver(id, value) {
        return firebase__WEBPACK_IMPORTED_MODULE_2___default.a.database().ref(`driverProfile/${id}/userInfo`).update({
            rating_positive: value,
        });
    }
    NegativeRateDriver(id, value) {
        return firebase__WEBPACK_IMPORTED_MODULE_2___default.a.database().ref(`driverProfile/${id}/userInfo`).update({
            rating_negative: value,
        });
    }
    Request(id) {
        return firebase__WEBPACK_IMPORTED_MODULE_2___default.a
            .database()
            .ref(`driverProfile/${id}/userInfo/favorite`)
            .update({
            favoriteSeek: true,
            id: [this.id, this.name],
        })
            .then(() => { });
    }
    Rater(id, rate) {
        return firebase__WEBPACK_IMPORTED_MODULE_2___default.a
            .database()
            .ref(`driverProfile/${id}/userInfo/rater`)
            .update({
            rating: true,
            rate: rate,
        })
            .then(() => { });
    }
    Tippen(id, est) {
        return firebase__WEBPACK_IMPORTED_MODULE_2___default.a
            .database()
            .ref(`driverProfile/${id}/userInfo/tipper`)
            .update({
            tippen: true,
            tipped: est,
        })
            .then(() => { });
    }
    ApprovePickup(value, id) {
        return firebase__WEBPACK_IMPORTED_MODULE_2___default.a.database().ref(`Customer/${id}/client`).update({
            Client_PickedUp: value,
        });
    }
    Cancelled(chat, name, date, location, destination, price, reason, charge, surcharge, realP, osc) {
        return firebase__WEBPACK_IMPORTED_MODULE_2___default.a.database().ref(`Cancelled/`).push({
            user_name: this.name,
            chat: chat,
            name: name,
            date: date,
            location: location,
            destination: destination,
            price: price,
            reason: reason,
            charge: charge,
            surcharge: surcharge,
            realPrice: realP,
            osc: osc,
        });
    }
    CancelledMe(name, date, location, destination, price, reason, charge, surcharge, realP, osc) {
        return this.userProfile.child(`cancelled/`).push({
            user_name: this.name,
            name: name,
            date: date,
            location: location,
            destination: destination,
            price: price,
            reason: reason,
            charge: charge,
            surcharge: surcharge,
            realPrice: realP,
            osc: osc,
        });
    }
    CancelledMe2(id, name, date, location, destination, price, reason, charge, surcharge, realP, osc) {
        return this.referalProfile2.child(id).child(`userInfo/cancelled/`).push({
            name: name,
            user_name: this.name,
            date: date,
            location: location,
            destination: destination,
            price: price,
            reason: reason,
            charge: charge,
            surcharge: surcharge,
            realPrice: realP,
            osc: osc,
        });
    }
    Completed(chat, id, name, date, location, destination, price, toll, date1, date2, date3, location2, destination2, surcharge, realP, osc, waitTimeCost) {
        return firebase__WEBPACK_IMPORTED_MODULE_2___default.a.database().ref(`Completed`).push({
            chat: chat,
            name: name,
            user_name: name,
            date: date,
            location: location,
            destination: destination,
            price: price,
            toll: toll,
            date1: date1,
            date2: date2,
            date3: date3,
            location2: location2,
            destination2: destination2,
            surcharge: surcharge,
            realPrice: realP,
            osc: osc,
            waitTimeCost: waitTimeCost
        });
    }
    ApproveDrop(value, id) {
        return firebase__WEBPACK_IMPORTED_MODULE_2___default.a.database().ref(`Customer/${id}/client`).update({
            Client_Dropped: value,
        });
    }
    SendMessage(value, id) {
        return firebase__WEBPACK_IMPORTED_MODULE_2___default.a.database().ref(`Customer/${id}/client/Chat`).push({
            Client_Message: value,
        });
    }
    SendMessage2(value, id) {
        return firebase__WEBPACK_IMPORTED_MODULE_2___default.a.database().ref(`Customer/${id}/client/Chat`).push({
            Client_Audio: value,
        });
    }
    CanCharge(value, id) {
        return firebase__WEBPACK_IMPORTED_MODULE_2___default.a.database().ref(`Customer/${id}/client`).update({
            Client_CanChargeCard: value,
        });
    }
    UpdatePaymentType(number) {
        return this.userProfile.update({
            payWith: number,
        });
    }
    UseCard(id, sd) {
        return this.userProfile.update({
            customerID: sd,
            source: id,
        });
    }
    AddPaymentCard(card, cust) {
        return this.userProfile.child("CreditCards").push({
            card: card,
            checked: false,
            customerID: cust,
        });
    }
    AddDestination(card) {
        return this.userProfile.child("Places").push({
            place: card,
        });
    }
    CheckCard(id) {
        return this.userProfile.child("CreditCards").child(id).update({
            checked: true,
        });
    }
    DitchCard(id) {
        return this.userProfile.child("CreditCards").child(id).update({
            checked: false,
        });
    }
    send_sms(recipient_number, message_body) {
        console.log("SMS HITT");
        const data = {
            recipient_number: recipient_number,
            message_body: message_body
        };
        const options = {
            method: 'POST',
            // headers: {
            //   'Content-Type': 'application/json',
            //   //'Access- Control-Allow-Origin': "*",
            // },
            body: JSON.stringify(data)
        };
        fetch(`https://api.ghingerhealth.com/sendsms`, options)
            .then((response) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            console.log("RESPONSE FORM SMS::", response);
        }))
            .catch(e => {
            console.log("ERRO RESPONSE FORM SMS::", e);
        });
    }
};
ProfileService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: "root",
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], ProfileService);



/***/ }),

/***/ "./src/app/services/settings.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/settings.service.ts ***!
  \**********************************************/
/*! exports provided: SettingsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsService", function() { return SettingsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _language_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./language.service */ "./src/app/services/language.service.ts");




let SettingsService = class SettingsService {
    constructor(lang) {
        this.lang = lang;
        this.appName = "Rider";
        this.car1 = "Standard";
        this.car2 = "Taxi";
        this.car3 = "Executive";
        this.language = "en";
        this.appCareer = "https://callforride.com";
        this.appFaq = "https://callforride.com";
        this.appLink = "https://callforride.com";
        this.appinsta = "https://www.instagram.com/callforride/";
        this.appFB = "https://www.facebook.com/call4rideghana/";
        this.appTikTok = "http://tiktok.com/@call4ridegh";
        this.appYoutube = "https://youtube.com/channel/UCXdQQx_eSKKV9xjDs7NO3Nw";
        this.twitter = "https://twitter.com/callforride";
        this.appTerms = "https://Call4Ride.net";
        //public apart = 3000; // 500 metres apart from driver
        this.apart = 150; // 500 metres apart from driver
        this.appCountryCode = "GH";
        this.appStripeKey = "sk_live_Emol5oC7Z42JqsYTzC05y85e";
        this.isStripe = false;
        this.appcurrency = "₵";
        this.appDashboard = "https://ajetaxi.firebaseapp.com//";
        this.appPhone = "+233502903382";
        this.companyMail = "call4ridegh@gmail.com";
        // tslint:disable-next-line: variable-name
        this.current_ID = false;
        this.mailGUrl = "sandbox28ca01fb2b374bb1b8aceb9d0a86895a.mailgun.org";
        this.mailGKey = window.btoa("api:key-60b9b5a8e7097e2fdcada552e4820db4");
        this.refer = false;
        this.wallet = false;
        this.company = false;
        this.pool = false;
        this.schedule = false;
        this.OnesignalAppID = "f6345b2b-3777-4631-8415-a3a178826e8d";
        this.CloudID = "1003487763108";
        // tslint:disable-next-line: variable-name
        this.support_email = "chndth@gmail.com";
        this.distanceDriving = 3000;
        this.WebAdminProfile = firebase__WEBPACK_IMPORTED_MODULE_2___default.a.database().ref(`DashboardSettings`);
        // this.getWebAdminProfile().on("value", (sShot) => {
        //   // tslint:disable-next-line: curly
        //   if (sShot.val().name) this.appName = sShot.val().name;
        //   // tslint:disable-next-line: curly
        //   if (sShot.val().website) this.appLink = sShot.val().website;
        //   // tslint:disable-next-line: curly
        //   if (sShot.val().faq) this.appFaq = sShot.val().faq;
        //   if (sShot.val().instagram) {
        //     this.appinsta = sShot.val().instagram;
        //   }
        //   if (sShot.val().distanceDriving) {
        //     this.distanceDriving = sShot.val().distanceDriving;
        //   }
        //   if (sShot.val().careers) {
        //     this.appCareer = sShot.val().careers;
        //   }
        //   if (sShot.val().facebook) {
        //     this.appFB = sShot.val().facebook;
        //   }
        //   if (sShot.val().apart) {
        //     this.apart = sShot.val().apart;
        //   }
        //   if (sShot.val().countrycode) {
        //     this.appCountryCode = sShot.val().countrycode;
        //   }
        //   if (sShot.val().currency) {
        //     this.appcurrency = sShot.val().currency;
        //   }
        //   if (sShot.val().phone) {
        //     this.appPhone = sShot.val().phone;
        //   }
        //   if (sShot.val().car_0) {
        //     this.car1 = sShot.val().car_0;
        //   }
        //   if (sShot.val().car_1) {
        //     this.car2 = sShot.val().car_1;
        //   }
        //   if (sShot.val().car_2) {
        //     this.car3 = sShot.val().car_2;
        //   }
        //   if (sShot.val().stripe) {
        //     this.isStripe = true;
        //   }
        //   if (sShot.val().apart) {
        //     this.apart = sShot.val().apart;
        //   }
        //   if (sShot.val().langauge) {
        //     console.log(this.langauge);
        //     this.langauge = sShot.val().langauge;
        //   }
        //   if (sShot.val().company) {
        //     this.company = sShot.val().company;
        //   }
        //   if (sShot.val().refer) {
        //     this.refer = sShot.val().refer;
        //   }
        //   if (sShot.val().wallet) {
        //     this.wallet = sShot.val().wallet;
        //   }
        //   if (sShot.val().pool) {
        //     this.pool = sShot.val().pool;
        //   }
        //   if (sShot.val().schedule) {
        //     this.schedule = sShot.val().schedule;
        //   }
        //   if (sShot.val().appID) {
        //     this.OnesignalAppID = sShot.val().appID;
        //   }
        //   if (sShot.val().CloudID) {
        //     this.CloudID = sShot.val().CloudID;
        //   }
        //   if (sShot.val().email) {
        //     this.companyMail = sShot.val().email;
        //   }
        //   if (sShot.val().mailgunUrl) {
        //     this.mailGUrl = sShot.val().mailgunUrl;
        //   }
        //   if (sShot.val().mailgunKey) {
        //     this.mailGKey = sShot.val().mailgunKey;
        //   }
        //   if (sShot.val().support_email) {
        //     this.support_email = sShot.val().support_email;
        //   }
        // });
    }
    getWebAdminProfile() {
        return this.WebAdminProfile;
    }
};
SettingsService.ctorParameters = () => [
    { type: _language_service__WEBPACK_IMPORTED_MODULE_3__["LanguageService"] }
];
SettingsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: "root",
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_language_service__WEBPACK_IMPORTED_MODULE_3__["LanguageService"]])
], SettingsService);



/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");



// if (environment.production) {
Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
//}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])()
    .bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch((err) => console.log(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/saha/IonicApps/C4Ride-IOS-2024/call4ride-passenger/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map