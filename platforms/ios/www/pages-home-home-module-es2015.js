(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-home-home-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/chat/chat.page.html":
/*!*********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/chat/chat.page.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header no-border>\n  <ion-toolbar style=\"margin-top: 10px\">\n    <ion-title>Chat</ion-title>\n       <ion-button icon-only (click)=\"closeChat()\" slot=\"end\">\n        <ion-icon name=\"close\"></ion-icon>\n      </ion-button>\n\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding text-center class=\"yes-scroll\">\n  <div class=\"followed-items\">\n    <ion-list >\n      <div *ngFor=\"let event of eventList\">\n        <div\n          text-wrap\n          detail=\"false\"\n          lines=\"none\"\n          class=\"driver\"\n          *ngIf=\"!event.user\"\n        >\n          <p class=\"chatTxt\">{{event.driver}}</p>\n      </div>\n        <div\n          text-wrap\n          detail=\"false\"\n          lines=\"none\"\n          class=\"user\"\n          *ngIf=\"!event.driver\"\n        >\n          <p class=\"chatTxt\">{{event.user}}</p>\n      </div>\n      </div>\n    </ion-list>\n  </div>\n</ion-content>\n\n<ion-footer>\n  <ion-grid>\n    <ion-row>\n      <ion-col size=\"10\">\n        <ion-input\n          type=\"text\"\n          placeholder=\"Type a message\"\n          [(ngModel)]=\"data.message\"\n          name=\"message\"\n        ></ion-input>\n      </ion-col>\n      <ion-col size=\"2\" (click)=\"Send()\" >\n        <ion-icon name=\"paper-plane\" style=\"font-size: 34px;\"></ion-icon>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-footer>\n\n<!-- <ion-footer>\n  <ion-grid>\n    <ion-row>\n      <ion-col>\n        <ion-button\n          id=\"container_1\"\n          class=\"button\"\n          size=\"large\"\n          color=\"primary\"\n          (click)=\"closeChat()\"\n        >\n          Close\n        </ion-button>\n      </ion-col>\n      <ion-col>\n        <ion-button\n          id=\"container_1\"\n          class=\"button\"\n          size=\"large\"\n          color=\"green\"\n          (click)=\"Send()\"\n        >\n          New Message\n        </ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-footer> -->\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/driver-info/driver-info.page.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/driver-info/driver-info.page.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--\n  Generated template for the AboutPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header no-border>\n  <ion-toolbar>\n    <ion-button (click)=\"closeModal()\" ion-button color=\"primary\" fill=\"clear\">\n      <ion-icon style=\"font-size: 1.5em\" name=\"arrow-back\"></ion-icon>\n      <span style=\"margin-left: 30px; font-size: 1.4em\"\n        >DRIVER AND CAR INFO</span\n      >\n    </ion-button>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"scroll\" padding>\n  <div text-center class=\"whiteFlap\">\n    <ion-title>\n      <img\n        class=\"profile-pic\"\n        [src]=\"info.Driver_picture\"\n        width=\"40%\"\n        height=\"40%\"\n      />\n    </ion-title>\n\n    <ion-title>Driver Name: {{info.Driver_name}} </ion-title>\n    <!-- <h4 text-center>License No # : {{info.Driver_license}}</h4> -->\n  </div>\n\n  <div text-center class=\"whiteFlap\">\n    <!--   <ion-item>\n      <h4 text-center>\n        Rating: +{{info.Driver_Positive_rating}}/{{info.Driver_Negative_rating}}\n      </h4></ion-item\n    > -->\n    <ion-item>Car Type: {{info.Driver_carType}}</ion-item>\n    <ion-item><h4 text-center>Plate No: {{info.Driver_plate}}</h4> </ion-item>\n    <ion-item> <h4 text-center>SEATS: {{info.Driver_seats}}</h4></ion-item>\n  </div>\n  <ion-grid>\n    <ion-row>\n      <!-- <ion-button class=\"buttonNew\" (click)=\"onChange($event)\">\n    Cancel Ride\n  </ion-button> -->\n\n      <ion-col size=\"12\" class=\"centerBtn\">\n        <ion-button (click)=\"onChange($event)\"> Cancel Ride </ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/home/home.page.html":
/*!*********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/home/home.page.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content class=\"no-scroll\">\n  <!-- This is the cordova google map -->\n  <div id=\"map\" [ngClass]=\"{'mess2_short': shortMap == true, 'mess_defualt': defaultMap == true}\">\n    <div class=\"topBar\">\n      <ion-button (click)=\"toggle()\" color=\"primary\" style=\"\n          --border-radius: 100%;\n          width: 60px;\n          height: 60px;\n          --vertical-align: middle;\n          --padding-start: -5px;\n          --padding-end: -5px;\n        \">\n        <ion-icon name=\"ios-menu\" slot=\"icon-only\"></ion-icon>\n      </ion-button>\n\n      <ion-button color=\"dark\" *ngIf=\"\" (click)=\"ReturnHome()\" class=\"Menubutton\" style=\"\n          --border-radius: 100%;\n          width: 50px;\n          height: 50px;\n          --vertical-align: middle;\n          --padding-start: -5px;\n          --padding-end: -5px;\n        \">\n        <ion-icon color=\"light\" name=\"arrow-back\" slot=\"icon-only\"></ion-icon>\n      </ion-button>\n    </div>\n\n    <div class=\"mid-right\">\n      <ion-fab slot=\"fixed\">\n        <ion-fab-button color=\"primary\"> SOS </ion-fab-button>\n        <ion-fab-list side=\"top\">\n          <ion-fab-button (click)=\"call_phone()\"><ion-icon name=\"call\"> </ion-icon></ion-fab-button>\n        </ion-fab-list>\n\n        <ion-fab-list side=\"start\">\n          <ion-fab-button (click)=\"call_phone_other()\">\n            <ion-icon name=\"body\"> </ion-icon>\n          </ion-fab-button>\n        </ion-fab-list>\n\n        <ion-fab-list side=\"bottom\">\n          <ion-fab-button (click)=\"call_phone_other()\">\n            <ion-icon name=\"bonfire\"> </ion-icon>\n          </ion-fab-button>\n        </ion-fab-list>\n      </ion-fab>\n\n    </div>\n    <div *ngIf=\"dProvider.destination_time || dProvider.destination_time != null\">\n      <div class=\"mid-right2\">\n        <ion-fab slot=\"fixed\">\n          <ion-button (click)=\"presentActionSheet()\" color=\"dark\">\n            <ion-icon name=\"share\"></ion-icon>\n            Share Trip\n          </ion-button>\n\n        </ion-fab>\n      </div>\n    </div>\n\n\n    <div *ngIf=\"driver_arrived == true && !dProvider.destination_time\">\n      <div class=\"mid-right3 \">\n       \n        <ion-fab slot=\"fixed\">\n        \n          <ion-button color=\"primary\" >\n            <ion-icon name=\"alarm\" > </ion-icon> \n          <span style=\"text-transform: none\">Driver Waiting for:: </span> <br>\n          <span id=\"demo\"> </span>\n          </ion-button>\n      \n        </ion-fab>\n      </div>\n    </div>\n\n      <div *ngIf=\"hasPaused == true\">\n        <div class=\"mid-right3 \">\n      \n          <ion-fab slot=\"fixed\">\n      \n            <ion-button color=\"primary\">\n              <ion-icon name=\"alarm\"> </ion-icon>\n              <span style=\"text-transform: none\">Trip On Hold for:: </span> <br>\n              <span id=\"pause_time\"> </span>\n            </ion-button>\n      \n          </ion-fab>\n        </div>\n      </div>\n   \n    <br>\n\n  </div>\n\n\n\n\n\n  <div *ngIf=\"!pop.onRequest\" class=\"bars\">\n\n    <div *ngIf=\"cMap.hasShown\">\n      <div class=\"bookingSect2\" *ngIf=\"cMap.selected_destination_bar == false\">\n        <h3 color=\"light\" style=\"font-size: 1.3em\" class=\"ion-padding\" class=\"ion-text-center\"\n          *ngIf=\"ph.name != undefined\">\n          Hi, {{ph.name}}\n        </h3>\n        <h3 color=\"light\" style=\"font-size: 1.3em\" class=\"ion-padding\" class=\"ion-text-center\"\n          *ngIf=\"ph.name == undefined\">\n          Welcome!\n        </h3>\n\n        <ion-grid>\n          <ion-row>\n            <ion-col id=\"iCol\" (click)=\"toggleMoreSection()\">\n              <ion-button class=\"bars-locate\" color=\"dark\">\n                <ion-icon slot=\"start\" name=\"search\"></ion-icon>\n                Where are you going ?\n              </ion-button>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </div>\n    </div>\n\n    <div class=\"bookingSect\" *ngIf=\"cMap.selected_destination_bar == true\">\n\n      <ion-grid>\n        <ion-row>\n          <ion-col id=\"iCol\" (click)=\"showAddressModal(1)\" size=\"10\">\n            <ion-button [hidden]=\"!cMap.onLocationbarHide\" class=\"bars-locate\" color=\"dark\">\n              <!-- <div id=\"location\"  *ngIf=\"!pickUpLocation || pickUpLocation == undefined\">{{myGcode.locationName}}</div>\n\n              <div id=\"location\" *ngIf=\"pickUpLocation\">{{pickUpLocation}}</div>\n             -->\n\n              <div id=\"location\" *ngIf=\"!pickUpLocation || pickUpLocation == undefined || pickUpLocation == ''\">{{myGcode.locationName}}</div>\n              \n              <div  *ngIf=\"pickUpLocation\">{{pickUpLocation}}</div>\n            </ion-button>\n          </ion-col>\n\n          <ion-col size=\"2\">\n            <ion-button color=\"dark\" [hidden]=\"!cMap.onLocationbarHide\" [disabled]=\"cMap.showDone\" class=\"Menubutton\"\n              (click)=\"ToggleChange_1()\" style=\"\n                  --border-radius: 100%;\n                  width: 50px;\n                  height: 50px;\n                  --vertical-align: middle;\n                  --padding-start: -5px;\n                  --padding-end: -5px;\n                \">\n              <ion-icon color=\"primary\" name=\"pin\" slot=\"icon-only\"></ion-icon>\n            </ion-button>\n            <div class=\"verticalLine\"></div>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n\n\n      <ion-grid>\n        <ion-row>\n          <ion-col id=\"iCol\" (click)=\"showAddressModal(2)\" size=\"10\">\n            <ion-button [hidden]=\"!cMap.onDestinatiobarHide\" class=\"bars-destinate\" color=\"dark\">\n              <div id=\"destination\">Your Destination</div>\n            </ion-button>\n          </ion-col>\n\n          <ion-col size=\"2\">\n            <ion-button color=\"dark\" [hidden]=\"!cMap.onDestinatiobarHide\" [disabled]=\"cMap.showDone\" class=\"Menubutton\"\n              (click)=\"ToggleChange_2()\" style=\"\n                  --border-radius: 100%;\n                  width: 50px;\n                  height: 50px;\n                  --vertical-align: middle;\n                  --padding-start: -5px;\n                  --padding-end: -5px;\n                \">\n              <ion-icon color=\"danger\" name=\"pin\" slot=\"icon-only\"></ion-icon>\n            </ion-button>\n          </ion-col>\n\n        </ion-row>\n      </ion-grid>\n    </div>\n  </div>\n\n  <!-- marker/pin situated in the center -->\n  <div [hidden]=\"!cMap.showDone\">\n    <div class=\"centerMarker\">\n      <span class=\"middy\" [hidden]=\"cMap.onPointerHide\">\n        <div id=\"nugget\" [hidden]=\"cMap.onPointerHide\"></div>\n      </span>\n    </div>\n  </div>\n\n  <div class=\"listRides\"\n    *ngIf=\"cMap.canShowchoiceTab == true && cMap.car_notificationIds != [] && rideSelected == false\">\n\n\n    <ion-button (click)=\"dismiss_noride()\" color=\"primary\" style=\"\n          --border-radius: 100%;\n          width: 40px;\n          height: 40px;\n          --vertical-align: middle;\n          --padding-start: -5px;\n          --padding-end: -5px;\n        \" class=\"widIcon\">\n      <ion-icon name=\"close\" slot=\"icon-only\"></ion-icon>\n    </ion-button>\n \n\n\n    <div *ngIf=\"dProvider.timeToReach == 'calculating...' && dProvider.price == null || dProvider.price == null \">\n      <ion-list class=\"fakeItem\">\n        <ion-row tappable>\n          <ion-item>\n            <ion-col size=\"4\" id=\"row1\">\n              <div class=\"TaxiICon2\"></div>\n            </ion-col>\n            <ion-col size=\"5\">\n              <div class=\"align-col\">\n                <h4 id=\"h44\"></h4>\n                <p id=\"p4\"></p>\n              </div>\n            </ion-col>\n            <ion-col size=\"3\">\n              <div class=\"amtStyle\">\n                <h4></h4>\n              </div>\n            </ion-col>\n          </ion-item>\n        </ion-row>\n        <ion-row tappable>\n          <ion-item id=\"red\">\n            <ion-col size=\"4\" id=\"row1\">\n              <div class=\"TaxiICon2\"></div>\n            </ion-col>\n            <ion-col size=\"5\">\n              <div class=\"align-col\">\n                <h4 id=\"h44\"></h4>\n                <p id=\"p4\"></p>\n              </div>\n            </ion-col>\n            <ion-col size=\"3\">\n              <div class=\"amtStyle\">\n                <h4></h4>\n              </div>\n            </ion-col>\n          </ion-item>\n        </ion-row>\n      </ion-list>\n    </div>\n\n    <div *ngIf=\"dProvider.timeToReach != 'calculating...' && dProvider.price != null\" >\n      <ion-list>\n        <ion-radio-group value=\"taxi\" (ionChange)=\"radioGroupChange($event)\" [(ngModel)]=\"yesorno\">\n          <ion-row tappable>\n            <ion-item>\n              <ion-radio slot=\"start\" value=\"private\"> </ion-radio>\n\n              <ion-col size=\"4\" id=\"row1\">\n                <div class=\"imgStyle\">\n                  <img src=\"assets/svgs/Taxi.svg\" class=\"TaxiICon2\" />\n                </div>\n              </ion-col>\n              <ion-col size=\"5\">\n                <div class=\"align-col\">\n                  <h4 id=\"h44\">Private</h4>\n                  <p id=\"p4\">Arrives in {{dProvider.timeToReach}}</p>\n                </div>\n              </ion-col>\n              <ion-col size=\"3\">\n                <div class=\"amtStyle\">\n                  <h4>₵{{(dProvider.price)}}</h4>\n                </div>\n              </ion-col>\n            </ion-item>\n          </ion-row>\n         \n        </ion-radio-group>\n      </ion-list>\n      <ion-col size=\"12\" class=\"centerBtn\">\n        <ion-button (click)=\"checkVal()\">\n          <span> Confirm Trip</span>\n        </ion-button>\n      </ion-col>\n    </div>\n  </div>\n\n  <!-- RIDE TYPE SELECTED-->\n  <div id=\"norideBar\" class=\"bottom-bar2\" *ngIf=\"rideSelected == true && pop.hasCleared == false && !cMap.onbar2\">\n    <div class=\"ion-padding\" class=\"request-for-ride2\">\n      <h4 class=\"butt\" class=\"ion-text-center\">Are you ready ?</h4>\n      <p class=\"centerText\">\n        <img src=\"/assets/svgs/Taxi.svg\" class=\"TaxiIComfirm\" />\n      </p>\n\n      <div *ngIf=\"yesorno == 'private'\">\n        <div class=\"lineHeight\">\n          <h4 class=\"butt\" class=\"ion-text-center\">\n             Private\n            <span style=\"font-size: 40px; font-weight: 800\">\n              <strong>₵{{(dProvider.price)}}</strong></span>\n          </h4>\n\n          <hr />\n          <p class=\"loca\">\n            <span> <img src=\"assets/icon/dott.png\" class=\"dott\" /></span>\n            <b> {{detination_words}}</b>\n          </p>\n        </div>\n      </div>\n\n      <ion-col size=\"12\" class=\"centerBtn\">\n        <ion-button (click)=\"Start()\">\n          <span> Confirm Pickup</span>\n        </ion-button>\n      </ion-col>\n    </div>\n  </div>\n  <!-- END RIDE TYPE SELECTED-->\n\n  <!-- Booking section  -->\n\n  <div id=\"norideBar\" *ngIf=\"cMap.norideavailable == true\" class=\"bottom-bar\">\n    <div class=\"request-for-ride\">\n      <img id=\"envelope\" src=\"assets/icon/world.png\" />\n\n      <p class=\"butt\" class=\"ion-text-center\">\n        No Driver Available In This Area\n      </p>\n\n      <ion-col size=\"12\" class=\"centerBtn\">\n        <ion-button (click)=\"dismiss_noride()\"> Dismiss </ion-button>\n      </ion-col>\n    </div>\n  </div>\n\n  <!-- CONNECTING TO DRIVER-->\n  <div id=\"norideBar\" *ngIf=\"cMap.onbar2\" class=\"carbottom-bar2\">\n    <div class=\"ion-padding\" class=\"request-for-ride2\">\n      <p class=\"centerText\">\n        <img src=\"/assets/svgs/Taxi.svg\" class=\"TaxiICon\" />\n      </p>\n\n      <h4 class=\"butt\" class=\"ion-text-center\">\n        <strong>Connecting to the nearest driver ......</strong>\n      </h4>\n      <!-- <p class=\"centerText\" (click)=\"onChange($event)\">\n          <img src=\"/assets/svgs/times-solid.svg\" class=\"TaxiICon\" />\n        </p> -->\n\n      <div class=\"loader\">\n        <div class=\"inner one\"></div>\n        <div class=\"inner two\"></div>\n        <div class=\"inner three\"></div>\n      </div>\n\n      <ion-button color=\"primary\" class=\"button_style\" (click)=\"onChange($event)\">\n        Cancel Request\n        <ion-icon color=\"light\" name=\"close\" slot=\"end\"></ion-icon>\n      </ion-button>\n    </div>\n  </div>\n\n  <!-- ENDED CONNECTING TO DRIVER-->\n\n  <!-- DRIVER FOUND-->\n\n\n\n  <label *ngIf=\"cMap.onbar3\">\n\n    <input type=\"checkbox\" name=\"run\" value=\"click\" (click)=\"completeItem()\" />\n    <div id=\"button1\" class=\"button\" style=\"z-index: 999999999999\">\n      <ion-icon name=\"ios-more\" style=\"font-size: 34px; margin-top: 13px\"></ion-icon>\n\n      <button class=\"main-con\">\n        <div class=\"driverFoundNew\" *ngIf=\"cMap.onbar3\">\n          <div class=\"ion-padding\" class=\"request-for-ride2\">\n            <!-- WHEN DRIVER IS FOUND-->\n            <div class=\"headSection\" *ngIf=\"dProvider.destination_time == null && driver_arrived == null\">\n              <div class=\"moveHeader\" (click)=\"OpenDriveInfo()\">\n                <span style=\"font-size: 25px; font-weight: 800; margin-left: 10%\">\n                  <strong>Driver Found!</strong></span>\n                <br />\n                <span class=\"centerText\">Arriving in {{dProvider.arriving_time}}, get ready!</span>\n              </div>\n            </div>\n\n            <!-- WHEN DRIVER ARRIVES-->\n            <div class=\"headSection_driver_arrive\" *ngIf=\"driver_arrived == true && !dProvider.destination_time\"\n              (click)=\"OpenDriveInfo()\">\n              <div class=\"moveHeader\">\n                <span style=\"font-size: 25px; font-weight: 800; margin-left: 10%\">\n                  <strong>Driver has Arrived!</strong></span>\n                <br />\n                <span class=\"centerText\">Please confirm the car number to be sure !</span>\n              </div>\n            </div>\n\n            <!-- WHEN DRIVER IS ON THE WAY TO DESTINATION-->\n            <div class=\"headSection\" *ngIf=\"dProvider.destination_time\">\n              <div class=\"moveHeader\">\n                <span style=\"font-size: 25px; font-weight: 800; margin-left: 10%\">\n                  <strong>Trip Ongoing</strong></span>\n                <br />\n                <span class=\"centerText\">Estimated Arrival {{dProvider.destination_time}}</span>\n              </div>\n            </div>\n\n            <div class=\"resultContainer\">\n              <ion-grid>\n                <ion-row>\n                  <ion-col size=\"3\">\n                    <div class=\"img-wrapper\">\n                      <ion-avatar>\n                        <ion-icon name=\"contact\" id=\"drivericonSize\" (click)=\"OpenDriveInfo()\" *ngIf=\"!picture\">\n                        </ion-icon>\n\n                        <img [src]=\"picture\" id=\"drivericonSize\" *ngIf=\"picture\" (click)=\"OpenDriveInfo()\" />\n                      </ion-avatar>\n                    </div>\n                  </ion-col>\n                  <ion-col size=\"7\">\n                    <div class=\"content-wrap\">\n                      <span class=\"bookTitle\">{{carType}}</span>\n                      <br />\n                      <span style=\"font-size: 30px; font-weight: 800\" class=\"bookPrice\">{{plate}}</span>\n                      <br />\n                      <span class=\"bookTitle\">Your driver is {{name}}</span>\n                    </div>\n                  </ion-col>\n\n                  <ion-col size=\"2\">\n                    <div class=\"img-wrapper\">\n                      <img src=\"assets/icon/chat.svg\" class=\"chatIcon\" (click)=\"Send()\" />\n                    </div>\n                  </ion-col>\n                </ion-row>\n              </ion-grid>\n            </div>\n\n                    <ion-item> <ion-icon name=\"pin\" style=\"color: #fbb91d\"></ion-icon> &nbsp;{{detination_words}}</ion-item>\n                    \n                    <ion-item *ngIf=\"!dProvider.new_price_for_add_stop\"> <ion-icon name=\"cash\" style=\"color: #acacac\"></ion-icon>&nbsp;₵{{(dProvider.price)}}</ion-item>\n\n                    <ion-item *ngIf=\"dProvider.new_price_for_add_stop\"> <ion-icon name=\"cash\"\n                        style=\"color: #acacac\"></ion-icon>&nbsp;{{(priceAfterAddStop)}}</ion-item>\n\n                    <ion-item (click)=\"showAddressModal(3)\" *ngIf=\"!newDestinationStop\"> <ion-icon name=\"add\"></ion-icon> Add\n                      Stop</ion-item>\n\n                      <ion-item *ngIf=\"newDestinationStop\"> <ion-icon name=\"pin\"></ion-icon> {{newDestinationStop}}&nbsp;\n                      <ion-badge color=\"success\">\n                        New Stop</ion-badge>\n                      </ion-item>\n                    \n                  \n\n            <ion-col size=\"12\" class=\"centerBtn\">\n              <ion-button (click)=\"callDriver()\">\n                <ion-icon name=\"call\"></ion-icon> <span> Call Driver</span>\n              </ion-button>\n            </ion-col>\n          </div>\n        </div>\n      </button>\n    </div>\n  </label>\n  <!-- END DRIVER FOUND-->\n\n  <!-- TEST-->\n <!-- <label>\n  \n    <input type=\"checkbox\" name=\"run\" value=\"click\" (click)=\"completeItem()\" />\n    <div id=\"button1\" class=\"button\" style=\"z-index: 999999999999\">\n      <ion-icon name=\"ios-more\" style=\"font-size: 34px; margin-top: 13px\"></ion-icon>\n  \n      <button class=\"main-con\">\n        <div class=\"driverFoundNew\">\n          <div class=\"ion-padding\" class=\"request-for-ride2\">\n       \n  \n          \n\n            <div class=\"headSection\">\n              <div class=\"moveHeader\">\n                <span style=\"font-size: 25px; font-weight: 800; margin-left: 10%\">\n                  <strong>Trip Ongoing</strong></span>\n                <br />\n                <span class=\"centerText\">Estimated Arrival 2mins</span>\n              </div>\n            </div>\n  \n            <div class=\"resultContainer\">\n              <ion-grid>\n                <ion-row>\n                  <ion-col size=\"3\">\n                    <div class=\"img-wrapper\">\n                      <ion-avatar>\n                        <ion-icon name=\"contact\" id=\"drivericonSize\" (click)=\"OpenDriveInfo()\" *ngIf=\"!picture\">\n                        </ion-icon>\n  \n            \n                      </ion-avatar>\n                    </div>\n                  </ion-col>\n                  <ion-col size=\"7\">\n                    <div class=\"content-wrap\">\n                      <span class=\"bookTitle\">Honda</span>\n                      <br />\n                      <span style=\"font-size: 30px; font-weight: 800\" class=\"bookPrice\">GT-22334</span>\n                      <br />\n                      <span class=\"bookTitle\">Your driver is Kwame App</span>\n                    </div>\n                  </ion-col>\n  \n                  <ion-col size=\"2\">\n                    <div class=\"img-wrapper\">\n                      <img src=\"assets/icon/chat.svg\" class=\"chatIcon\" (click)=\"Send()\" />\n                    </div>\n                  </ion-col>\n                </ion-row>\n              </ion-grid>\n            </div>\n\n                <ion-item> <ion-icon name=\"pin\" style=\"color: #fbb91d\"></ion-icon> &nbsp;Kokomlemle</ion-item>\n\n                <ion-item> <ion-icon name=\"cash\" style=\"color: #acacac\"></ion-icon>&nbsp; ₵20</ion-item>\n\n                 \n                <ion-item >\n                   <ion-icon name=\"pin\"></ion-icon> Kaneshie Quarters&nbsp;<ion-badge color=\"success\">\n                    New Stop</ion-badge>\n                </ion-item>\n\n            \n           \n            <ion-col size=\"12\" class=\"centerBtn\">\n              <ion-button >\n                <ion-icon name=\"call\"></ion-icon> <span> Call Driver</span>\n              </ion-button>\n            </ion-col>\n          </div>\n        </div>\n      </button>\n    </div>\n  </label> -->\n  <!-- END TEST-->\n\n</ion-content>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/rate/rate.page.html":
/*!*********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/rate/rate.page.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <ion-header no-border>\n  <ion-title style=\"padding-top: 30px\">{{lp.translate()[0].rate}}</ion-title>\n</ion-header>\n\n<ion-content text-center padding>\n  <ion-title>{{lp.translate()[0].rateexp}}</ion-title>\n \n\n  <rating\n    [(ngModel)]=\"rate\"\n    readonly=\"false\"\n    size=\"default\"\n    (ngModelChange)=\"onRateChange($event)\"\n  >\n  </rating>\n\n  <div class=\"reviewspace\">\n    <ion-title>{{lp.translate()[0].review}}</ion-title>\n    <form (ngSubmit)=\"logForm()\">\n      <ion-textarea\n        #myInput\n        id=\"myInput\"\n        rows=\"1\"\n        maxLength=\"500\"\n        [(ngModel)]=\"todo.description\"\n        name=\"description\"\n      >\n      </ion-textarea>\n\n      <ion-button class=\"button\" padding type=\"submit\" epand=\"block\"\n        >{{lp.translate()[0].submit}}</ion-button\n      >\n    </form>\n  </div>\n</ion-content> -->\n\n<ion-content class=\"backgroundColor\">\n  <div class=\"driverFound\">\n    <div class=\"ion-padding\" class=\"request-for-ride2\">\n      <div class=\"headSection\">\n        <div class=\"moveHeader\">\n          <span style=\"font-size: 25px; font-weight: 800\">\n            <strong>Pay & Rate Driver</strong></span\n          >\n        </div>\n      </div>\n\n      <h4 style=\"text-align: center;\">Pay {{name}}, <span style=\"font-size: 60px; font-weight: 800; text-align: center;color: green\">₵{{total}} </span> </h4>\n      <p style=\"text-align: center;\">Main Fare: ₵{{price}} </p>\n        <p style=\"text-align: center;\">Pause/ Waiting Cost: ₵{{waitTimeCost}} </p>\n\n      <div class=\"resultContainer\">\n        <rating\n          [(ngModel)]=\"rate\"\n          readonly=\"false\"\n          size=\"default\"\n          (ngModelChange)=\"onRateChange($event)\"\n        >\n        </rating>\n\n        <div style=\"display: inline\">\n          <div class=\"content-wrap\">\n            <div class=\"reviewspace\">\n              <span\n                style=\"\n                  font-size: 15px;\n                  font-weight: 200;\n                  text-align: center;\n                  margin-left: 20%;\n                \"\n              >\n                Write a review</span\n              >\n              <form (ngSubmit)=\"logForm()\">\n                <ion-textarea\n                  #myInput\n                  id=\"myInput\"\n                  rows=\"1\"\n                  maxLength=\"500\"\n                  [(ngModel)]=\"todo.description\"\n                  name=\"description\"\n                >\n                </ion-textarea>\n\n                <ion-grid>\n                  <ion-row>\n                    <ion-col>\n                      <ion-button\n                        [ngStyle]=\"{'margin-top': 20 + 'px', 'font-size': 1.1 + 'em'}\"\n                        color=\"dark\"\n                        expand=\"block\"\n                        shape=\"round\"\n                        type=\"submit\"\n                      >\n                        SUBMIT\n                      </ion-button>\n                    </ion-col>\n                  </ion-row>\n                </ion-grid>\n              </form>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</ion-content>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/trip-info/trip-info.page.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/trip-info/trip-info.page.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--\n  Generated template for the AboutPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header no-border>\n  <ion-toolbar>\n    <ion-button (click)=\"closeModal()\" color=\"primary\" fill=\"clear\">\n      <ion-icon style=\"font-size: 1.5em\" name=\"arrow-back\"></ion-icon>\n      <span style=\"margin-left: 30px; font-size: 1.4em\">CLOSE</span>\n    </ion-button>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"scroll\" padding>\n  <ion-title text-center>FARE BREAKDOWN</ion-title>\n\n  <ion-title padding text-center>RIDER PAY</ion-title>\n  <ion-title padding text-center\n    ><strong>{{settings.appcurrency}}{{(riderpaid)}}</strong></ion-title\n  >\n  <ion-item no-lines padding>\n    <div text-center>\n      <ion-title>Ride Cost</ion-title>\n      <ion-title padding text-center no-lines\n        ><strong>{{settings.appcurrency}}{{(riderpaid)}}</strong></ion-title\n      >\n    </div>\n  </ion-item>\n\n  <ion-title padding text-center>DRIVER MADE</ion-title>\n  <ion-title text-center\n    ><strong>{{settings.appcurrency}}{{(driverMade)}}</strong></ion-title\n  >\n\n  <ion-item no-lines padding>\n    <div text-center>\n      <ion-title>Ride Cost</ion-title>\n      <ion-title padding text-center no-lines\n        ><strong>{{settings.appcurrency}}{{(riderpaid)}}</strong></ion-title\n      >\n    </div>\n  </ion-item>\n\n  <div text-center class=\"whiteFlap\">\n    <ion-title>\n      <img class=\"profile-pic\" [src]=\"info.Driver_picture\" />\n    </ion-title>\n\n    <ion-title>Driver Name: {{info.Driver_name}} </ion-title>\n    <!-- <h4 text-center>License No # : {{info.Driver_license}}</h4> -->\n  </div>\n\n  <div text-center class=\"whiteFlap\">\n    <ion-item>\n      <h4 text-center>\n        Rating: +{{info.Driver_Positive_rating}}/{{info.Driver_Negative_rating}}\n      </h4></ion-item\n    >\n    <ion-item>Car Type: {{info.Driver_carType}}</ion-item>\n    <ion-item><h4 text-center>Plate No: {{info.Driver_plate}}</h4> </ion-item>\n    <ion-item> <h4 text-center>SEATS: {{info.Driver_seats}}</h4></ion-item>\n  </div>\n\n  <div padding text-center id=\"buttonContainer2\">\n    <ion-item no-lines detail-none ion-item class=\"guttonz\">\n      <ion-label class=\"stack\" (click)=\"onChange($event)\">\n        <h1>CANCEL THIS TRIP</h1>\n      </ion-label>\n      <!-- <ion-select\n        [(ngModel)]=\"currentCar\"\n        (ionChange)=\"onChange($event)\"\n        multiple=\"false\"\n      >\n        <div *ngFor=\"let item of items\">\n          <ion-select-option value=\"{{item.text}}\"\n            >{{item.text}}</ion-select-option\n          >\n        </div>\n      </ion-select> -->\n    </ion-item>\n  </div>\n</ion-content>\n"

/***/ }),

/***/ "./node_modules/sweetalert2/dist/sweetalert2.all.js":
/*!**********************************************************!*\
  !*** ./node_modules/sweetalert2/dist/sweetalert2.all.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/*!
* sweetalert2 v11.7.22
* Released under the MIT License.
*/
(function (global, factory) {
   true ? module.exports = factory() :
  undefined;
})(this, (function () { 'use strict';

  const RESTORE_FOCUS_TIMEOUT = 100;

  /** @type {GlobalState} */
  const globalState = {};
  const focusPreviousActiveElement = () => {
    if (globalState.previousActiveElement instanceof HTMLElement) {
      globalState.previousActiveElement.focus();
      globalState.previousActiveElement = null;
    } else if (document.body) {
      document.body.focus();
    }
  };

  /**
   * Restore previous active (focused) element
   *
   * @param {boolean} returnFocus
   * @returns {Promise<void>}
   */
  const restoreActiveElement = returnFocus => {
    return new Promise(resolve => {
      if (!returnFocus) {
        return resolve();
      }
      const x = window.scrollX;
      const y = window.scrollY;
      globalState.restoreFocusTimeout = setTimeout(() => {
        focusPreviousActiveElement();
        resolve();
      }, RESTORE_FOCUS_TIMEOUT); // issues/900

      window.scrollTo(x, y);
    });
  };

  /**
   * This module contains `WeakMap`s for each effectively-"private  property" that a `Swal` has.
   * For example, to set the private property "foo" of `this` to "bar", you can `privateProps.foo.set(this, 'bar')`
   * This is the approach that Babel will probably take to implement private methods/fields
   *   https://github.com/tc39/proposal-private-methods
   *   https://github.com/babel/babel/pull/7555
   * Once we have the changes from that PR in Babel, and our core class fits reasonable in *one module*
   *   then we can use that language feature.
   */

  var privateProps = {
    promise: new WeakMap(),
    innerParams: new WeakMap(),
    domCache: new WeakMap()
  };

  const swalPrefix = 'swal2-';

  /**
   * @typedef
   * { | 'container'
   *   | 'shown'
   *   | 'height-auto'
   *   | 'iosfix'
   *   | 'popup'
   *   | 'modal'
   *   | 'no-backdrop'
   *   | 'no-transition'
   *   | 'toast'
   *   | 'toast-shown'
   *   | 'show'
   *   | 'hide'
   *   | 'close'
   *   | 'title'
   *   | 'html-container'
   *   | 'actions'
   *   | 'confirm'
   *   | 'deny'
   *   | 'cancel'
   *   | 'default-outline'
   *   | 'footer'
   *   | 'icon'
   *   | 'icon-content'
   *   | 'image'
   *   | 'input'
   *   | 'file'
   *   | 'range'
   *   | 'select'
   *   | 'radio'
   *   | 'checkbox'
   *   | 'label'
   *   | 'textarea'
   *   | 'inputerror'
   *   | 'input-label'
   *   | 'validation-message'
   *   | 'progress-steps'
   *   | 'active-progress-step'
   *   | 'progress-step'
   *   | 'progress-step-line'
   *   | 'loader'
   *   | 'loading'
   *   | 'styled'
   *   | 'top'
   *   | 'top-start'
   *   | 'top-end'
   *   | 'top-left'
   *   | 'top-right'
   *   | 'center'
   *   | 'center-start'
   *   | 'center-end'
   *   | 'center-left'
   *   | 'center-right'
   *   | 'bottom'
   *   | 'bottom-start'
   *   | 'bottom-end'
   *   | 'bottom-left'
   *   | 'bottom-right'
   *   | 'grow-row'
   *   | 'grow-column'
   *   | 'grow-fullscreen'
   *   | 'rtl'
   *   | 'timer-progress-bar'
   *   | 'timer-progress-bar-container'
   *   | 'scrollbar-measure'
   *   | 'icon-success'
   *   | 'icon-warning'
   *   | 'icon-info'
   *   | 'icon-question'
   *   | 'icon-error'
   * } SwalClass
   * @typedef {Record<SwalClass, string>} SwalClasses
   */

  /**
   * @typedef {'success' | 'warning' | 'info' | 'question' | 'error'} SwalIcon
   * @typedef {Record<SwalIcon, string>} SwalIcons
   */

  /** @type {SwalClass[]} */
  const classNames = ['container', 'shown', 'height-auto', 'iosfix', 'popup', 'modal', 'no-backdrop', 'no-transition', 'toast', 'toast-shown', 'show', 'hide', 'close', 'title', 'html-container', 'actions', 'confirm', 'deny', 'cancel', 'default-outline', 'footer', 'icon', 'icon-content', 'image', 'input', 'file', 'range', 'select', 'radio', 'checkbox', 'label', 'textarea', 'inputerror', 'input-label', 'validation-message', 'progress-steps', 'active-progress-step', 'progress-step', 'progress-step-line', 'loader', 'loading', 'styled', 'top', 'top-start', 'top-end', 'top-left', 'top-right', 'center', 'center-start', 'center-end', 'center-left', 'center-right', 'bottom', 'bottom-start', 'bottom-end', 'bottom-left', 'bottom-right', 'grow-row', 'grow-column', 'grow-fullscreen', 'rtl', 'timer-progress-bar', 'timer-progress-bar-container', 'scrollbar-measure', 'icon-success', 'icon-warning', 'icon-info', 'icon-question', 'icon-error'];
  const swalClasses = classNames.reduce((acc, className) => {
    acc[className] = swalPrefix + className;
    return acc;
  }, /** @type {SwalClasses} */{});

  /** @type {SwalIcon[]} */
  const icons = ['success', 'warning', 'info', 'question', 'error'];
  const iconTypes = icons.reduce((acc, icon) => {
    acc[icon] = swalPrefix + icon;
    return acc;
  }, /** @type {SwalIcons} */{});

  const consolePrefix = 'SweetAlert2:';

  /**
   * Capitalize the first letter of a string
   *
   * @param {string} str
   * @returns {string}
   */
  const capitalizeFirstLetter = str => str.charAt(0).toUpperCase() + str.slice(1);

  /**
   * Standardize console warnings
   *
   * @param {string | string[]} message
   */
  const warn = message => {
    console.warn(`${consolePrefix} ${typeof message === 'object' ? message.join(' ') : message}`);
  };

  /**
   * Standardize console errors
   *
   * @param {string} message
   */
  const error = message => {
    console.error(`${consolePrefix} ${message}`);
  };

  /**
   * Private global state for `warnOnce`
   *
   * @type {string[]}
   * @private
   */
  const previousWarnOnceMessages = [];

  /**
   * Show a console warning, but only if it hasn't already been shown
   *
   * @param {string} message
   */
  const warnOnce = message => {
    if (!previousWarnOnceMessages.includes(message)) {
      previousWarnOnceMessages.push(message);
      warn(message);
    }
  };

  /**
   * Show a one-time console warning about deprecated params/methods
   *
   * @param {string} deprecatedParam
   * @param {string} useInstead
   */
  const warnAboutDeprecation = (deprecatedParam, useInstead) => {
    warnOnce(`"${deprecatedParam}" is deprecated and will be removed in the next major release. Please use "${useInstead}" instead.`);
  };

  /**
   * If `arg` is a function, call it (with no arguments or context) and return the result.
   * Otherwise, just pass the value through
   *
   * @param {Function | any} arg
   * @returns {any}
   */
  const callIfFunction = arg => typeof arg === 'function' ? arg() : arg;

  /**
   * @param {any} arg
   * @returns {boolean}
   */
  const hasToPromiseFn = arg => arg && typeof arg.toPromise === 'function';

  /**
   * @param {any} arg
   * @returns {Promise<any>}
   */
  const asPromise = arg => hasToPromiseFn(arg) ? arg.toPromise() : Promise.resolve(arg);

  /**
   * @param {any} arg
   * @returns {boolean}
   */
  const isPromise = arg => arg && Promise.resolve(arg) === arg;

  /**
   * Gets the popup container which contains the backdrop and the popup itself.
   *
   * @returns {HTMLElement | null}
   */
  const getContainer = () => document.body.querySelector(`.${swalClasses.container}`);

  /**
   * @param {string} selectorString
   * @returns {HTMLElement | null}
   */
  const elementBySelector = selectorString => {
    const container = getContainer();
    return container ? container.querySelector(selectorString) : null;
  };

  /**
   * @param {string} className
   * @returns {HTMLElement | null}
   */
  const elementByClass = className => {
    return elementBySelector(`.${className}`);
  };

  /**
   * @returns {HTMLElement | null}
   */
  const getPopup = () => elementByClass(swalClasses.popup);

  /**
   * @returns {HTMLElement | null}
   */
  const getIcon = () => elementByClass(swalClasses.icon);

  /**
   * @returns {HTMLElement | null}
   */
  const getIconContent = () => elementByClass(swalClasses['icon-content']);

  /**
   * @returns {HTMLElement | null}
   */
  const getTitle = () => elementByClass(swalClasses.title);

  /**
   * @returns {HTMLElement | null}
   */
  const getHtmlContainer = () => elementByClass(swalClasses['html-container']);

  /**
   * @returns {HTMLElement | null}
   */
  const getImage = () => elementByClass(swalClasses.image);

  /**
   * @returns {HTMLElement | null}
   */
  const getProgressSteps = () => elementByClass(swalClasses['progress-steps']);

  /**
   * @returns {HTMLElement | null}
   */
  const getValidationMessage = () => elementByClass(swalClasses['validation-message']);

  /**
   * @returns {HTMLButtonElement | null}
   */
  const getConfirmButton = () => /** @type {HTMLButtonElement} */elementBySelector(`.${swalClasses.actions} .${swalClasses.confirm}`);

  /**
   * @returns {HTMLButtonElement | null}
   */
  const getCancelButton = () => /** @type {HTMLButtonElement} */elementBySelector(`.${swalClasses.actions} .${swalClasses.cancel}`);

  /**
   * @returns {HTMLButtonElement | null}
   */
  const getDenyButton = () => /** @type {HTMLButtonElement} */elementBySelector(`.${swalClasses.actions} .${swalClasses.deny}`);

  /**
   * @returns {HTMLElement | null}
   */
  const getInputLabel = () => elementByClass(swalClasses['input-label']);

  /**
   * @returns {HTMLElement | null}
   */
  const getLoader = () => elementBySelector(`.${swalClasses.loader}`);

  /**
   * @returns {HTMLElement | null}
   */
  const getActions = () => elementByClass(swalClasses.actions);

  /**
   * @returns {HTMLElement | null}
   */
  const getFooter = () => elementByClass(swalClasses.footer);

  /**
   * @returns {HTMLElement | null}
   */
  const getTimerProgressBar = () => elementByClass(swalClasses['timer-progress-bar']);

  /**
   * @returns {HTMLElement | null}
   */
  const getCloseButton = () => elementByClass(swalClasses.close);

  // https://github.com/jkup/focusable/blob/master/index.js
  const focusable = `
  a[href],
  area[href],
  input:not([disabled]),
  select:not([disabled]),
  textarea:not([disabled]),
  button:not([disabled]),
  iframe,
  object,
  embed,
  [tabindex="0"],
  [contenteditable],
  audio[controls],
  video[controls],
  summary
`;
  /**
   * @returns {HTMLElement[]}
   */
  const getFocusableElements = () => {
    const popup = getPopup();
    if (!popup) {
      return [];
    }
    /** @type {NodeListOf<HTMLElement>} */
    const focusableElementsWithTabindex = popup.querySelectorAll('[tabindex]:not([tabindex="-1"]):not([tabindex="0"])');
    const focusableElementsWithTabindexSorted = Array.from(focusableElementsWithTabindex)
    // sort according to tabindex
    .sort((a, b) => {
      const tabindexA = parseInt(a.getAttribute('tabindex') || '0');
      const tabindexB = parseInt(b.getAttribute('tabindex') || '0');
      if (tabindexA > tabindexB) {
        return 1;
      } else if (tabindexA < tabindexB) {
        return -1;
      }
      return 0;
    });

    /** @type {NodeListOf<HTMLElement>} */
    const otherFocusableElements = popup.querySelectorAll(focusable);
    const otherFocusableElementsFiltered = Array.from(otherFocusableElements).filter(el => el.getAttribute('tabindex') !== '-1');
    return [...new Set(focusableElementsWithTabindexSorted.concat(otherFocusableElementsFiltered))].filter(el => isVisible$1(el));
  };

  /**
   * @returns {boolean}
   */
  const isModal = () => {
    return hasClass(document.body, swalClasses.shown) && !hasClass(document.body, swalClasses['toast-shown']) && !hasClass(document.body, swalClasses['no-backdrop']);
  };

  /**
   * @returns {boolean}
   */
  const isToast = () => {
    const popup = getPopup();
    if (!popup) {
      return false;
    }
    return hasClass(popup, swalClasses.toast);
  };

  /**
   * @returns {boolean}
   */
  const isLoading = () => {
    const popup = getPopup();
    if (!popup) {
      return false;
    }
    return popup.hasAttribute('data-loading');
  };

  /**
   * Securely set innerHTML of an element
   * https://github.com/sweetalert2/sweetalert2/issues/1926
   *
   * @param {HTMLElement} elem
   * @param {string} html
   */
  const setInnerHtml = (elem, html) => {
    elem.textContent = '';
    if (html) {
      const parser = new DOMParser();
      const parsed = parser.parseFromString(html, `text/html`);
      const head = parsed.querySelector('head');
      head && Array.from(head.childNodes).forEach(child => {
        elem.appendChild(child);
      });
      const body = parsed.querySelector('body');
      body && Array.from(body.childNodes).forEach(child => {
        if (child instanceof HTMLVideoElement || child instanceof HTMLAudioElement) {
          elem.appendChild(child.cloneNode(true)); // https://github.com/sweetalert2/sweetalert2/issues/2507
        } else {
          elem.appendChild(child);
        }
      });
    }
  };

  /**
   * @param {HTMLElement} elem
   * @param {string} className
   * @returns {boolean}
   */
  const hasClass = (elem, className) => {
    if (!className) {
      return false;
    }
    const classList = className.split(/\s+/);
    for (let i = 0; i < classList.length; i++) {
      if (!elem.classList.contains(classList[i])) {
        return false;
      }
    }
    return true;
  };

  /**
   * @param {HTMLElement} elem
   * @param {SweetAlertOptions} params
   */
  const removeCustomClasses = (elem, params) => {
    Array.from(elem.classList).forEach(className => {
      if (!Object.values(swalClasses).includes(className) && !Object.values(iconTypes).includes(className) && !Object.values(params.showClass || {}).includes(className)) {
        elem.classList.remove(className);
      }
    });
  };

  /**
   * @param {HTMLElement} elem
   * @param {SweetAlertOptions} params
   * @param {string} className
   */
  const applyCustomClass = (elem, params, className) => {
    removeCustomClasses(elem, params);
    if (params.customClass && params.customClass[className]) {
      if (typeof params.customClass[className] !== 'string' && !params.customClass[className].forEach) {
        warn(`Invalid type of customClass.${className}! Expected string or iterable object, got "${typeof params.customClass[className]}"`);
        return;
      }
      addClass(elem, params.customClass[className]);
    }
  };

  /**
   * @param {HTMLElement} popup
   * @param {import('./renderers/renderInput').InputClass} inputClass
   * @returns {HTMLInputElement | null}
   */
  const getInput$1 = (popup, inputClass) => {
    if (!inputClass) {
      return null;
    }
    switch (inputClass) {
      case 'select':
      case 'textarea':
      case 'file':
        return popup.querySelector(`.${swalClasses.popup} > .${swalClasses[inputClass]}`);
      case 'checkbox':
        return popup.querySelector(`.${swalClasses.popup} > .${swalClasses.checkbox} input`);
      case 'radio':
        return popup.querySelector(`.${swalClasses.popup} > .${swalClasses.radio} input:checked`) || popup.querySelector(`.${swalClasses.popup} > .${swalClasses.radio} input:first-child`);
      case 'range':
        return popup.querySelector(`.${swalClasses.popup} > .${swalClasses.range} input`);
      default:
        return popup.querySelector(`.${swalClasses.popup} > .${swalClasses.input}`);
    }
  };

  /**
   * @param {HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement} input
   */
  const focusInput = input => {
    input.focus();

    // place cursor at end of text in text input
    if (input.type !== 'file') {
      // http://stackoverflow.com/a/2345915
      const val = input.value;
      input.value = '';
      input.value = val;
    }
  };

  /**
   * @param {HTMLElement | HTMLElement[] | null} target
   * @param {string | string[] | readonly string[] | undefined} classList
   * @param {boolean} condition
   */
  const toggleClass = (target, classList, condition) => {
    if (!target || !classList) {
      return;
    }
    if (typeof classList === 'string') {
      classList = classList.split(/\s+/).filter(Boolean);
    }
    classList.forEach(className => {
      if (Array.isArray(target)) {
        target.forEach(elem => {
          condition ? elem.classList.add(className) : elem.classList.remove(className);
        });
      } else {
        condition ? target.classList.add(className) : target.classList.remove(className);
      }
    });
  };

  /**
   * @param {HTMLElement | HTMLElement[] | null} target
   * @param {string | string[] | readonly string[] | undefined} classList
   */
  const addClass = (target, classList) => {
    toggleClass(target, classList, true);
  };

  /**
   * @param {HTMLElement | HTMLElement[] | null} target
   * @param {string | string[] | readonly string[] | undefined} classList
   */
  const removeClass = (target, classList) => {
    toggleClass(target, classList, false);
  };

  /**
   * Get direct child of an element by class name
   *
   * @param {HTMLElement} elem
   * @param {string} className
   * @returns {HTMLElement | undefined}
   */
  const getDirectChildByClass = (elem, className) => {
    const children = Array.from(elem.children);
    for (let i = 0; i < children.length; i++) {
      const child = children[i];
      if (child instanceof HTMLElement && hasClass(child, className)) {
        return child;
      }
    }
  };

  /**
   * @param {HTMLElement} elem
   * @param {string} property
   * @param {*} value
   */
  const applyNumericalStyle = (elem, property, value) => {
    if (value === `${parseInt(value)}`) {
      value = parseInt(value);
    }
    if (value || parseInt(value) === 0) {
      elem.style[property] = typeof value === 'number' ? `${value}px` : value;
    } else {
      elem.style.removeProperty(property);
    }
  };

  /**
   * @param {HTMLElement | null} elem
   * @param {string} display
   */
  const show = function (elem) {
    let display = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'flex';
    elem && (elem.style.display = display);
  };

  /**
   * @param {HTMLElement | null} elem
   */
  const hide = elem => {
    elem && (elem.style.display = 'none');
  };

  /**
   * @param {HTMLElement} parent
   * @param {string} selector
   * @param {string} property
   * @param {string} value
   */
  const setStyle = (parent, selector, property, value) => {
    /** @type {HTMLElement} */
    const el = parent.querySelector(selector);
    if (el) {
      el.style[property] = value;
    }
  };

  /**
   * @param {HTMLElement} elem
   * @param {any} condition
   * @param {string} display
   */
  const toggle = function (elem, condition) {
    let display = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'flex';
    condition ? show(elem, display) : hide(elem);
  };

  /**
   * borrowed from jquery $(elem).is(':visible') implementation
   *
   * @param {HTMLElement | null} elem
   * @returns {boolean}
   */
  const isVisible$1 = elem => !!(elem && (elem.offsetWidth || elem.offsetHeight || elem.getClientRects().length));

  /**
   * @returns {boolean}
   */
  const allButtonsAreHidden = () => !isVisible$1(getConfirmButton()) && !isVisible$1(getDenyButton()) && !isVisible$1(getCancelButton());

  /**
   * @param {HTMLElement} elem
   * @returns {boolean}
   */
  const isScrollable = elem => !!(elem.scrollHeight > elem.clientHeight);

  /**
   * borrowed from https://stackoverflow.com/a/46352119
   *
   * @param {HTMLElement} elem
   * @returns {boolean}
   */
  const hasCssAnimation = elem => {
    const style = window.getComputedStyle(elem);
    const animDuration = parseFloat(style.getPropertyValue('animation-duration') || '0');
    const transDuration = parseFloat(style.getPropertyValue('transition-duration') || '0');
    return animDuration > 0 || transDuration > 0;
  };

  /**
   * @param {number} timer
   * @param {boolean} reset
   */
  const animateTimerProgressBar = function (timer) {
    let reset = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
    const timerProgressBar = getTimerProgressBar();
    if (!timerProgressBar) {
      return;
    }
    if (isVisible$1(timerProgressBar)) {
      if (reset) {
        timerProgressBar.style.transition = 'none';
        timerProgressBar.style.width = '100%';
      }
      setTimeout(() => {
        timerProgressBar.style.transition = `width ${timer / 1000}s linear`;
        timerProgressBar.style.width = '0%';
      }, 10);
    }
  };
  const stopTimerProgressBar = () => {
    const timerProgressBar = getTimerProgressBar();
    if (!timerProgressBar) {
      return;
    }
    const timerProgressBarWidth = parseInt(window.getComputedStyle(timerProgressBar).width);
    timerProgressBar.style.removeProperty('transition');
    timerProgressBar.style.width = '100%';
    const timerProgressBarFullWidth = parseInt(window.getComputedStyle(timerProgressBar).width);
    const timerProgressBarPercent = timerProgressBarWidth / timerProgressBarFullWidth * 100;
    timerProgressBar.style.width = `${timerProgressBarPercent}%`;
  };

  /**
   * Detect Node env
   *
   * @returns {boolean}
   */
  const isNodeEnv = () => typeof window === 'undefined' || typeof document === 'undefined';

  const sweetHTML = `
 <div aria-labelledby="${swalClasses.title}" aria-describedby="${swalClasses['html-container']}" class="${swalClasses.popup}" tabindex="-1">
   <button type="button" class="${swalClasses.close}"></button>
   <ul class="${swalClasses['progress-steps']}"></ul>
   <div class="${swalClasses.icon}"></div>
   <img class="${swalClasses.image}" />
   <h2 class="${swalClasses.title}" id="${swalClasses.title}"></h2>
   <div class="${swalClasses['html-container']}" id="${swalClasses['html-container']}"></div>
   <input class="${swalClasses.input}" id="${swalClasses.input}" />
   <input type="file" class="${swalClasses.file}" />
   <div class="${swalClasses.range}">
     <input type="range" />
     <output></output>
   </div>
   <select class="${swalClasses.select}" id="${swalClasses.select}"></select>
   <div class="${swalClasses.radio}"></div>
   <label class="${swalClasses.checkbox}">
     <input type="checkbox" id="${swalClasses.checkbox}" />
     <span class="${swalClasses.label}"></span>
   </label>
   <textarea class="${swalClasses.textarea}" id="${swalClasses.textarea}"></textarea>
   <div class="${swalClasses['validation-message']}" id="${swalClasses['validation-message']}"></div>
   <div class="${swalClasses.actions}">
     <div class="${swalClasses.loader}"></div>
     <button type="button" class="${swalClasses.confirm}"></button>
     <button type="button" class="${swalClasses.deny}"></button>
     <button type="button" class="${swalClasses.cancel}"></button>
   </div>
   <div class="${swalClasses.footer}"></div>
   <div class="${swalClasses['timer-progress-bar-container']}">
     <div class="${swalClasses['timer-progress-bar']}"></div>
   </div>
 </div>
`.replace(/(^|\n)\s*/g, '');

  /**
   * @returns {boolean}
   */
  const resetOldContainer = () => {
    const oldContainer = getContainer();
    if (!oldContainer) {
      return false;
    }
    oldContainer.remove();
    removeClass([document.documentElement, document.body], [swalClasses['no-backdrop'], swalClasses['toast-shown'], swalClasses['has-column']]);
    return true;
  };
  const resetValidationMessage$1 = () => {
    globalState.currentInstance.resetValidationMessage();
  };
  const addInputChangeListeners = () => {
    const popup = getPopup();
    const input = getDirectChildByClass(popup, swalClasses.input);
    const file = getDirectChildByClass(popup, swalClasses.file);
    /** @type {HTMLInputElement} */
    const range = popup.querySelector(`.${swalClasses.range} input`);
    /** @type {HTMLOutputElement} */
    const rangeOutput = popup.querySelector(`.${swalClasses.range} output`);
    const select = getDirectChildByClass(popup, swalClasses.select);
    /** @type {HTMLInputElement} */
    const checkbox = popup.querySelector(`.${swalClasses.checkbox} input`);
    const textarea = getDirectChildByClass(popup, swalClasses.textarea);
    input.oninput = resetValidationMessage$1;
    file.onchange = resetValidationMessage$1;
    select.onchange = resetValidationMessage$1;
    checkbox.onchange = resetValidationMessage$1;
    textarea.oninput = resetValidationMessage$1;
    range.oninput = () => {
      resetValidationMessage$1();
      rangeOutput.value = range.value;
    };
    range.onchange = () => {
      resetValidationMessage$1();
      rangeOutput.value = range.value;
    };
  };

  /**
   * @param {string | HTMLElement} target
   * @returns {HTMLElement}
   */
  const getTarget = target => typeof target === 'string' ? document.querySelector(target) : target;

  /**
   * @param {SweetAlertOptions} params
   */
  const setupAccessibility = params => {
    const popup = getPopup();
    popup.setAttribute('role', params.toast ? 'alert' : 'dialog');
    popup.setAttribute('aria-live', params.toast ? 'polite' : 'assertive');
    if (!params.toast) {
      popup.setAttribute('aria-modal', 'true');
    }
  };

  /**
   * @param {HTMLElement} targetElement
   */
  const setupRTL = targetElement => {
    if (window.getComputedStyle(targetElement).direction === 'rtl') {
      addClass(getContainer(), swalClasses.rtl);
    }
  };

  /**
   * Add modal + backdrop + no-war message for Russians to DOM
   *
   * @param {SweetAlertOptions} params
   */
  const init = params => {
    // Clean up the old popup container if it exists
    const oldContainerExisted = resetOldContainer();

    /* istanbul ignore if */
    if (isNodeEnv()) {
      error('SweetAlert2 requires document to initialize');
      return;
    }
    const container = document.createElement('div');
    container.className = swalClasses.container;
    if (oldContainerExisted) {
      addClass(container, swalClasses['no-transition']);
    }
    setInnerHtml(container, sweetHTML);
    const targetElement = getTarget(params.target);
    targetElement.appendChild(container);
    setupAccessibility(params);
    setupRTL(targetElement);
    addInputChangeListeners();
  };

  /**
   * @param {HTMLElement | object | string} param
   * @param {HTMLElement} target
   */
  const parseHtmlToContainer = (param, target) => {
    // DOM element
    if (param instanceof HTMLElement) {
      target.appendChild(param);
    }

    // Object
    else if (typeof param === 'object') {
      handleObject(param, target);
    }

    // Plain string
    else if (param) {
      setInnerHtml(target, param);
    }
  };

  /**
   * @param {any} param
   * @param {HTMLElement} target
   */
  const handleObject = (param, target) => {
    // JQuery element(s)
    if (param.jquery) {
      handleJqueryElem(target, param);
    }

    // For other objects use their string representation
    else {
      setInnerHtml(target, param.toString());
    }
  };

  /**
   * @param {HTMLElement} target
   * @param {any} elem
   */
  const handleJqueryElem = (target, elem) => {
    target.textContent = '';
    if (0 in elem) {
      for (let i = 0; (i in elem); i++) {
        target.appendChild(elem[i].cloneNode(true));
      }
    } else {
      target.appendChild(elem.cloneNode(true));
    }
  };

  /**
   * @returns {'webkitAnimationEnd' | 'animationend' | false}
   */
  const animationEndEvent = (() => {
    // Prevent run in Node env
    /* istanbul ignore if */
    if (isNodeEnv()) {
      return false;
    }
    const testEl = document.createElement('div');
    const transEndEventNames = {
      WebkitAnimation: 'webkitAnimationEnd',
      // Chrome, Safari and Opera
      animation: 'animationend' // Standard syntax
    };

    for (const i in transEndEventNames) {
      if (Object.prototype.hasOwnProperty.call(transEndEventNames, i) && typeof testEl.style[i] !== 'undefined') {
        return transEndEventNames[i];
      }
    }
    return false;
  })();

  /**
   * @param {SweetAlert} instance
   * @param {SweetAlertOptions} params
   */
  const renderActions = (instance, params) => {
    const actions = getActions();
    const loader = getLoader();
    if (!actions || !loader) {
      return;
    }

    // Actions (buttons) wrapper
    if (!params.showConfirmButton && !params.showDenyButton && !params.showCancelButton) {
      hide(actions);
    } else {
      show(actions);
    }

    // Custom class
    applyCustomClass(actions, params, 'actions');

    // Render all the buttons
    renderButtons(actions, loader, params);

    // Loader
    setInnerHtml(loader, params.loaderHtml || '');
    applyCustomClass(loader, params, 'loader');
  };

  /**
   * @param {HTMLElement} actions
   * @param {HTMLElement} loader
   * @param {SweetAlertOptions} params
   */
  function renderButtons(actions, loader, params) {
    const confirmButton = getConfirmButton();
    const denyButton = getDenyButton();
    const cancelButton = getCancelButton();
    if (!confirmButton || !denyButton || !cancelButton) {
      return;
    }

    // Render buttons
    renderButton(confirmButton, 'confirm', params);
    renderButton(denyButton, 'deny', params);
    renderButton(cancelButton, 'cancel', params);
    handleButtonsStyling(confirmButton, denyButton, cancelButton, params);
    if (params.reverseButtons) {
      if (params.toast) {
        actions.insertBefore(cancelButton, confirmButton);
        actions.insertBefore(denyButton, confirmButton);
      } else {
        actions.insertBefore(cancelButton, loader);
        actions.insertBefore(denyButton, loader);
        actions.insertBefore(confirmButton, loader);
      }
    }
  }

  /**
   * @param {HTMLElement} confirmButton
   * @param {HTMLElement} denyButton
   * @param {HTMLElement} cancelButton
   * @param {SweetAlertOptions} params
   */
  function handleButtonsStyling(confirmButton, denyButton, cancelButton, params) {
    if (!params.buttonsStyling) {
      removeClass([confirmButton, denyButton, cancelButton], swalClasses.styled);
      return;
    }
    addClass([confirmButton, denyButton, cancelButton], swalClasses.styled);

    // Buttons background colors
    if (params.confirmButtonColor) {
      confirmButton.style.backgroundColor = params.confirmButtonColor;
      addClass(confirmButton, swalClasses['default-outline']);
    }
    if (params.denyButtonColor) {
      denyButton.style.backgroundColor = params.denyButtonColor;
      addClass(denyButton, swalClasses['default-outline']);
    }
    if (params.cancelButtonColor) {
      cancelButton.style.backgroundColor = params.cancelButtonColor;
      addClass(cancelButton, swalClasses['default-outline']);
    }
  }

  /**
   * @param {HTMLElement} button
   * @param {'confirm' | 'deny' | 'cancel'} buttonType
   * @param {SweetAlertOptions} params
   */
  function renderButton(button, buttonType, params) {
    const buttonName = /** @type {'Confirm' | 'Deny' | 'Cancel'} */capitalizeFirstLetter(buttonType);
    toggle(button, params[`show${buttonName}Button`], 'inline-block');
    setInnerHtml(button, params[`${buttonType}ButtonText`] || ''); // Set caption text
    button.setAttribute('aria-label', params[`${buttonType}ButtonAriaLabel`] || ''); // ARIA label

    // Add buttons custom classes
    button.className = swalClasses[buttonType];
    applyCustomClass(button, params, `${buttonType}Button`);
  }

  /**
   * @param {SweetAlert} instance
   * @param {SweetAlertOptions} params
   */
  const renderCloseButton = (instance, params) => {
    const closeButton = getCloseButton();
    if (!closeButton) {
      return;
    }
    setInnerHtml(closeButton, params.closeButtonHtml || '');

    // Custom class
    applyCustomClass(closeButton, params, 'closeButton');
    toggle(closeButton, params.showCloseButton);
    closeButton.setAttribute('aria-label', params.closeButtonAriaLabel || '');
  };

  /**
   * @param {SweetAlert} instance
   * @param {SweetAlertOptions} params
   */
  const renderContainer = (instance, params) => {
    const container = getContainer();
    if (!container) {
      return;
    }
    handleBackdropParam(container, params.backdrop);
    handlePositionParam(container, params.position);
    handleGrowParam(container, params.grow);

    // Custom class
    applyCustomClass(container, params, 'container');
  };

  /**
   * @param {HTMLElement} container
   * @param {SweetAlertOptions['backdrop']} backdrop
   */
  function handleBackdropParam(container, backdrop) {
    if (typeof backdrop === 'string') {
      container.style.background = backdrop;
    } else if (!backdrop) {
      addClass([document.documentElement, document.body], swalClasses['no-backdrop']);
    }
  }

  /**
   * @param {HTMLElement} container
   * @param {SweetAlertOptions['position']} position
   */
  function handlePositionParam(container, position) {
    if (!position) {
      return;
    }
    if (position in swalClasses) {
      addClass(container, swalClasses[position]);
    } else {
      warn('The "position" parameter is not valid, defaulting to "center"');
      addClass(container, swalClasses.center);
    }
  }

  /**
   * @param {HTMLElement} container
   * @param {SweetAlertOptions['grow']} grow
   */
  function handleGrowParam(container, grow) {
    if (!grow) {
      return;
    }
    addClass(container, swalClasses[`grow-${grow}`]);
  }

  /// <reference path="../../../../sweetalert2.d.ts"/>


  /** @type {InputClass[]} */
  const inputClasses = ['input', 'file', 'range', 'select', 'radio', 'checkbox', 'textarea'];

  /**
   * @param {SweetAlert} instance
   * @param {SweetAlertOptions} params
   */
  const renderInput = (instance, params) => {
    const popup = getPopup();
    if (!popup) {
      return;
    }
    const innerParams = privateProps.innerParams.get(instance);
    const rerender = !innerParams || params.input !== innerParams.input;
    inputClasses.forEach(inputClass => {
      const inputContainer = getDirectChildByClass(popup, swalClasses[inputClass]);
      if (!inputContainer) {
        return;
      }

      // set attributes
      setAttributes(inputClass, params.inputAttributes);

      // set class
      inputContainer.className = swalClasses[inputClass];
      if (rerender) {
        hide(inputContainer);
      }
    });
    if (params.input) {
      if (rerender) {
        showInput(params);
      }
      // set custom class
      setCustomClass(params);
    }
  };

  /**
   * @param {SweetAlertOptions} params
   */
  const showInput = params => {
    if (!params.input) {
      return;
    }
    if (!renderInputType[params.input]) {
      error(`Unexpected type of input! Expected "text", "email", "password", "number", "tel", "select", "radio", "checkbox", "textarea", "file" or "url", got "${params.input}"`);
      return;
    }
    const inputContainer = getInputContainer(params.input);
    const input = renderInputType[params.input](inputContainer, params);
    show(inputContainer);

    // input autofocus
    if (params.inputAutoFocus) {
      setTimeout(() => {
        focusInput(input);
      });
    }
  };

  /**
   * @param {HTMLInputElement} input
   */
  const removeAttributes = input => {
    for (let i = 0; i < input.attributes.length; i++) {
      const attrName = input.attributes[i].name;
      if (!['id', 'type', 'value', 'style'].includes(attrName)) {
        input.removeAttribute(attrName);
      }
    }
  };

  /**
   * @param {InputClass} inputClass
   * @param {SweetAlertOptions['inputAttributes']} inputAttributes
   */
  const setAttributes = (inputClass, inputAttributes) => {
    const input = getInput$1(getPopup(), inputClass);
    if (!input) {
      return;
    }
    removeAttributes(input);
    for (const attr in inputAttributes) {
      input.setAttribute(attr, inputAttributes[attr]);
    }
  };

  /**
   * @param {SweetAlertOptions} params
   */
  const setCustomClass = params => {
    const inputContainer = getInputContainer(params.input);
    if (typeof params.customClass === 'object') {
      addClass(inputContainer, params.customClass.input);
    }
  };

  /**
   * @param {HTMLInputElement | HTMLTextAreaElement} input
   * @param {SweetAlertOptions} params
   */
  const setInputPlaceholder = (input, params) => {
    if (!input.placeholder || params.inputPlaceholder) {
      input.placeholder = params.inputPlaceholder;
    }
  };

  /**
   * @param {Input} input
   * @param {Input} prependTo
   * @param {SweetAlertOptions} params
   */
  const setInputLabel = (input, prependTo, params) => {
    if (params.inputLabel) {
      const label = document.createElement('label');
      const labelClass = swalClasses['input-label'];
      label.setAttribute('for', input.id);
      label.className = labelClass;
      if (typeof params.customClass === 'object') {
        addClass(label, params.customClass.inputLabel);
      }
      label.innerText = params.inputLabel;
      prependTo.insertAdjacentElement('beforebegin', label);
    }
  };

  /**
   * @param {SweetAlertOptions['input']} inputType
   * @returns {HTMLElement}
   */
  const getInputContainer = inputType => {
    return getDirectChildByClass(getPopup(), swalClasses[inputType] || swalClasses.input);
  };

  /**
   * @param {HTMLInputElement | HTMLOutputElement | HTMLTextAreaElement} input
   * @param {SweetAlertOptions['inputValue']} inputValue
   */
  const checkAndSetInputValue = (input, inputValue) => {
    if (['string', 'number'].includes(typeof inputValue)) {
      input.value = `${inputValue}`;
    } else if (!isPromise(inputValue)) {
      warn(`Unexpected type of inputValue! Expected "string", "number" or "Promise", got "${typeof inputValue}"`);
    }
  };

  /** @type {Record<SweetAlertInput, (input: Input | HTMLElement, params: SweetAlertOptions) => Input>} */
  const renderInputType = {};

  /**
   * @param {HTMLInputElement} input
   * @param {SweetAlertOptions} params
   * @returns {HTMLInputElement}
   */
  renderInputType.text = renderInputType.email = renderInputType.password = renderInputType.number = renderInputType.tel = renderInputType.url = (input, params) => {
    checkAndSetInputValue(input, params.inputValue);
    setInputLabel(input, input, params);
    setInputPlaceholder(input, params);
    input.type = params.input;
    return input;
  };

  /**
   * @param {HTMLInputElement} input
   * @param {SweetAlertOptions} params
   * @returns {HTMLInputElement}
   */
  renderInputType.file = (input, params) => {
    setInputLabel(input, input, params);
    setInputPlaceholder(input, params);
    return input;
  };

  /**
   * @param {HTMLInputElement} range
   * @param {SweetAlertOptions} params
   * @returns {HTMLInputElement}
   */
  renderInputType.range = (range, params) => {
    const rangeInput = range.querySelector('input');
    const rangeOutput = range.querySelector('output');
    checkAndSetInputValue(rangeInput, params.inputValue);
    rangeInput.type = params.input;
    checkAndSetInputValue(rangeOutput, params.inputValue);
    setInputLabel(rangeInput, range, params);
    return range;
  };

  /**
   * @param {HTMLSelectElement} select
   * @param {SweetAlertOptions} params
   * @returns {HTMLSelectElement}
   */
  renderInputType.select = (select, params) => {
    select.textContent = '';
    if (params.inputPlaceholder) {
      const placeholder = document.createElement('option');
      setInnerHtml(placeholder, params.inputPlaceholder);
      placeholder.value = '';
      placeholder.disabled = true;
      placeholder.selected = true;
      select.appendChild(placeholder);
    }
    setInputLabel(select, select, params);
    return select;
  };

  /**
   * @param {HTMLInputElement} radio
   * @returns {HTMLInputElement}
   */
  renderInputType.radio = radio => {
    radio.textContent = '';
    return radio;
  };

  /**
   * @param {HTMLLabelElement} checkboxContainer
   * @param {SweetAlertOptions} params
   * @returns {HTMLInputElement}
   */
  renderInputType.checkbox = (checkboxContainer, params) => {
    const checkbox = getInput$1(getPopup(), 'checkbox');
    checkbox.value = '1';
    checkbox.checked = Boolean(params.inputValue);
    const label = checkboxContainer.querySelector('span');
    setInnerHtml(label, params.inputPlaceholder);
    return checkbox;
  };

  /**
   * @param {HTMLTextAreaElement} textarea
   * @param {SweetAlertOptions} params
   * @returns {HTMLTextAreaElement}
   */
  renderInputType.textarea = (textarea, params) => {
    checkAndSetInputValue(textarea, params.inputValue);
    setInputPlaceholder(textarea, params);
    setInputLabel(textarea, textarea, params);

    /**
     * @param {HTMLElement} el
     * @returns {number}
     */
    const getMargin = el => parseInt(window.getComputedStyle(el).marginLeft) + parseInt(window.getComputedStyle(el).marginRight);

    // https://github.com/sweetalert2/sweetalert2/issues/2291
    setTimeout(() => {
      // https://github.com/sweetalert2/sweetalert2/issues/1699
      if ('MutationObserver' in window) {
        const initialPopupWidth = parseInt(window.getComputedStyle(getPopup()).width);
        const textareaResizeHandler = () => {
          // check if texarea is still in document (i.e. popup wasn't closed in the meantime)
          if (!document.body.contains(textarea)) {
            return;
          }
          const textareaWidth = textarea.offsetWidth + getMargin(textarea);
          if (textareaWidth > initialPopupWidth) {
            getPopup().style.width = `${textareaWidth}px`;
          } else {
            applyNumericalStyle(getPopup(), 'width', params.width);
          }
        };
        new MutationObserver(textareaResizeHandler).observe(textarea, {
          attributes: true,
          attributeFilter: ['style']
        });
      }
    });
    return textarea;
  };

  /**
   * @param {SweetAlert} instance
   * @param {SweetAlertOptions} params
   */
  const renderContent = (instance, params) => {
    const htmlContainer = getHtmlContainer();
    if (!htmlContainer) {
      return;
    }
    applyCustomClass(htmlContainer, params, 'htmlContainer');

    // Content as HTML
    if (params.html) {
      parseHtmlToContainer(params.html, htmlContainer);
      show(htmlContainer, 'block');
    }

    // Content as plain text
    else if (params.text) {
      htmlContainer.textContent = params.text;
      show(htmlContainer, 'block');
    }

    // No content
    else {
      hide(htmlContainer);
    }
    renderInput(instance, params);
  };

  /**
   * @param {SweetAlert} instance
   * @param {SweetAlertOptions} params
   */
  const renderFooter = (instance, params) => {
    const footer = getFooter();
    if (!footer) {
      return;
    }
    toggle(footer, params.footer);
    if (params.footer) {
      parseHtmlToContainer(params.footer, footer);
    }

    // Custom class
    applyCustomClass(footer, params, 'footer');
  };

  /**
   * @param {SweetAlert} instance
   * @param {SweetAlertOptions} params
   */
  const renderIcon = (instance, params) => {
    const innerParams = privateProps.innerParams.get(instance);
    const icon = getIcon();
    if (!icon) {
      return;
    }

    // if the given icon already rendered, apply the styling without re-rendering the icon
    if (innerParams && params.icon === innerParams.icon) {
      // Custom or default content
      setContent(icon, params);
      applyStyles(icon, params);
      return;
    }
    if (!params.icon && !params.iconHtml) {
      hide(icon);
      return;
    }
    if (params.icon && Object.keys(iconTypes).indexOf(params.icon) === -1) {
      error(`Unknown icon! Expected "success", "error", "warning", "info" or "question", got "${params.icon}"`);
      hide(icon);
      return;
    }
    show(icon);

    // Custom or default content
    setContent(icon, params);
    applyStyles(icon, params);

    // Animate icon
    addClass(icon, params.showClass && params.showClass.icon);
  };

  /**
   * @param {HTMLElement} icon
   * @param {SweetAlertOptions} params
   */
  const applyStyles = (icon, params) => {
    for (const [iconType, iconClassName] of Object.entries(iconTypes)) {
      if (params.icon !== iconType) {
        removeClass(icon, iconClassName);
      }
    }
    addClass(icon, params.icon && iconTypes[params.icon]);

    // Icon color
    setColor(icon, params);

    // Success icon background color
    adjustSuccessIconBackgroundColor();

    // Custom class
    applyCustomClass(icon, params, 'icon');
  };

  // Adjust success icon background color to match the popup background color
  const adjustSuccessIconBackgroundColor = () => {
    const popup = getPopup();
    if (!popup) {
      return;
    }
    const popupBackgroundColor = window.getComputedStyle(popup).getPropertyValue('background-color');
    /** @type {NodeListOf<HTMLElement>} */
    const successIconParts = popup.querySelectorAll('[class^=swal2-success-circular-line], .swal2-success-fix');
    for (let i = 0; i < successIconParts.length; i++) {
      successIconParts[i].style.backgroundColor = popupBackgroundColor;
    }
  };
  const successIconHtml = `
  <div class="swal2-success-circular-line-left"></div>
  <span class="swal2-success-line-tip"></span> <span class="swal2-success-line-long"></span>
  <div class="swal2-success-ring"></div> <div class="swal2-success-fix"></div>
  <div class="swal2-success-circular-line-right"></div>
`;
  const errorIconHtml = `
  <span class="swal2-x-mark">
    <span class="swal2-x-mark-line-left"></span>
    <span class="swal2-x-mark-line-right"></span>
  </span>
`;

  /**
   * @param {HTMLElement} icon
   * @param {SweetAlertOptions} params
   */
  const setContent = (icon, params) => {
    if (!params.icon && !params.iconHtml) {
      return;
    }
    let oldContent = icon.innerHTML;
    let newContent = '';
    if (params.iconHtml) {
      newContent = iconContent(params.iconHtml);
    } else if (params.icon === 'success') {
      newContent = successIconHtml;
      oldContent = oldContent.replace(/ style=".*?"/g, ''); // undo adjustSuccessIconBackgroundColor()
    } else if (params.icon === 'error') {
      newContent = errorIconHtml;
    } else if (params.icon) {
      const defaultIconHtml = {
        question: '?',
        warning: '!',
        info: 'i'
      };
      newContent = iconContent(defaultIconHtml[params.icon]);
    }
    if (oldContent.trim() !== newContent.trim()) {
      setInnerHtml(icon, newContent);
    }
  };

  /**
   * @param {HTMLElement} icon
   * @param {SweetAlertOptions} params
   */
  const setColor = (icon, params) => {
    if (!params.iconColor) {
      return;
    }
    icon.style.color = params.iconColor;
    icon.style.borderColor = params.iconColor;
    for (const sel of ['.swal2-success-line-tip', '.swal2-success-line-long', '.swal2-x-mark-line-left', '.swal2-x-mark-line-right']) {
      setStyle(icon, sel, 'backgroundColor', params.iconColor);
    }
    setStyle(icon, '.swal2-success-ring', 'borderColor', params.iconColor);
  };

  /**
   * @param {string} content
   * @returns {string}
   */
  const iconContent = content => `<div class="${swalClasses['icon-content']}">${content}</div>`;

  /**
   * @param {SweetAlert} instance
   * @param {SweetAlertOptions} params
   */
  const renderImage = (instance, params) => {
    const image = getImage();
    if (!image) {
      return;
    }
    if (!params.imageUrl) {
      hide(image);
      return;
    }
    show(image, '');

    // Src, alt
    image.setAttribute('src', params.imageUrl);
    image.setAttribute('alt', params.imageAlt || '');

    // Width, height
    applyNumericalStyle(image, 'width', params.imageWidth);
    applyNumericalStyle(image, 'height', params.imageHeight);

    // Class
    image.className = swalClasses.image;
    applyCustomClass(image, params, 'image');
  };

  /**
   * @param {SweetAlert} instance
   * @param {SweetAlertOptions} params
   */
  const renderPopup = (instance, params) => {
    const container = getContainer();
    const popup = getPopup();
    if (!container || !popup) {
      return;
    }

    // Width
    // https://github.com/sweetalert2/sweetalert2/issues/2170
    if (params.toast) {
      applyNumericalStyle(container, 'width', params.width);
      popup.style.width = '100%';
      const loader = getLoader();
      loader && popup.insertBefore(loader, getIcon());
    } else {
      applyNumericalStyle(popup, 'width', params.width);
    }

    // Padding
    applyNumericalStyle(popup, 'padding', params.padding);

    // Color
    if (params.color) {
      popup.style.color = params.color;
    }

    // Background
    if (params.background) {
      popup.style.background = params.background;
    }
    hide(getValidationMessage());

    // Classes
    addClasses$1(popup, params);
  };

  /**
   * @param {HTMLElement} popup
   * @param {SweetAlertOptions} params
   */
  const addClasses$1 = (popup, params) => {
    const showClass = params.showClass || {};
    // Default Class + showClass when updating Swal.update({})
    popup.className = `${swalClasses.popup} ${isVisible$1(popup) ? showClass.popup : ''}`;
    if (params.toast) {
      addClass([document.documentElement, document.body], swalClasses['toast-shown']);
      addClass(popup, swalClasses.toast);
    } else {
      addClass(popup, swalClasses.modal);
    }

    // Custom class
    applyCustomClass(popup, params, 'popup');
    if (typeof params.customClass === 'string') {
      addClass(popup, params.customClass);
    }

    // Icon class (#1842)
    if (params.icon) {
      addClass(popup, swalClasses[`icon-${params.icon}`]);
    }
  };

  /**
   * @param {SweetAlert} instance
   * @param {SweetAlertOptions} params
   */
  const renderProgressSteps = (instance, params) => {
    const progressStepsContainer = getProgressSteps();
    if (!progressStepsContainer) {
      return;
    }
    const {
      progressSteps,
      currentProgressStep
    } = params;
    if (!progressSteps || progressSteps.length === 0 || currentProgressStep === undefined) {
      hide(progressStepsContainer);
      return;
    }
    show(progressStepsContainer);
    progressStepsContainer.textContent = '';
    if (currentProgressStep >= progressSteps.length) {
      warn('Invalid currentProgressStep parameter, it should be less than progressSteps.length ' + '(currentProgressStep like JS arrays starts from 0)');
    }
    progressSteps.forEach((step, index) => {
      const stepEl = createStepElement(step);
      progressStepsContainer.appendChild(stepEl);
      if (index === currentProgressStep) {
        addClass(stepEl, swalClasses['active-progress-step']);
      }
      if (index !== progressSteps.length - 1) {
        const lineEl = createLineElement(params);
        progressStepsContainer.appendChild(lineEl);
      }
    });
  };

  /**
   * @param {string} step
   * @returns {HTMLLIElement}
   */
  const createStepElement = step => {
    const stepEl = document.createElement('li');
    addClass(stepEl, swalClasses['progress-step']);
    setInnerHtml(stepEl, step);
    return stepEl;
  };

  /**
   * @param {SweetAlertOptions} params
   * @returns {HTMLLIElement}
   */
  const createLineElement = params => {
    const lineEl = document.createElement('li');
    addClass(lineEl, swalClasses['progress-step-line']);
    if (params.progressStepsDistance) {
      applyNumericalStyle(lineEl, 'width', params.progressStepsDistance);
    }
    return lineEl;
  };

  /**
   * @param {SweetAlert} instance
   * @param {SweetAlertOptions} params
   */
  const renderTitle = (instance, params) => {
    const title = getTitle();
    if (!title) {
      return;
    }
    toggle(title, params.title || params.titleText, 'block');
    if (params.title) {
      parseHtmlToContainer(params.title, title);
    }
    if (params.titleText) {
      title.innerText = params.titleText;
    }

    // Custom class
    applyCustomClass(title, params, 'title');
  };

  /**
   * @param {SweetAlert} instance
   * @param {SweetAlertOptions} params
   */
  const render = (instance, params) => {
    renderPopup(instance, params);
    renderContainer(instance, params);
    renderProgressSteps(instance, params);
    renderIcon(instance, params);
    renderImage(instance, params);
    renderTitle(instance, params);
    renderCloseButton(instance, params);
    renderContent(instance, params);
    renderActions(instance, params);
    renderFooter(instance, params);
    const popup = getPopup();
    if (typeof params.didRender === 'function' && popup) {
      params.didRender(popup);
    }
  };

  /*
   * Global function to determine if SweetAlert2 popup is shown
   */
  const isVisible = () => {
    return isVisible$1(getPopup());
  };

  /*
   * Global function to click 'Confirm' button
   */
  const clickConfirm = () => getConfirmButton() && getConfirmButton().click();

  /*
   * Global function to click 'Deny' button
   */
  const clickDeny = () => getDenyButton() && getDenyButton().click();

  /*
   * Global function to click 'Cancel' button
   */
  const clickCancel = () => getCancelButton() && getCancelButton().click();

  /** @typedef {'cancel' | 'backdrop' | 'close' | 'esc' | 'timer'} DismissReason */

  /** @type {Record<DismissReason, DismissReason>} */
  const DismissReason = Object.freeze({
    cancel: 'cancel',
    backdrop: 'backdrop',
    close: 'close',
    esc: 'esc',
    timer: 'timer'
  });

  /**
   * @param {GlobalState} globalState
   */
  const removeKeydownHandler = globalState => {
    if (globalState.keydownTarget && globalState.keydownHandlerAdded) {
      globalState.keydownTarget.removeEventListener('keydown', globalState.keydownHandler, {
        capture: globalState.keydownListenerCapture
      });
      globalState.keydownHandlerAdded = false;
    }
  };

  /**
   * @param {SweetAlert} instance
   * @param {GlobalState} globalState
   * @param {SweetAlertOptions} innerParams
   * @param {*} dismissWith
   */
  const addKeydownHandler = (instance, globalState, innerParams, dismissWith) => {
    removeKeydownHandler(globalState);
    if (!innerParams.toast) {
      globalState.keydownHandler = e => keydownHandler(instance, e, dismissWith);
      globalState.keydownTarget = innerParams.keydownListenerCapture ? window : getPopup();
      globalState.keydownListenerCapture = innerParams.keydownListenerCapture;
      globalState.keydownTarget.addEventListener('keydown', globalState.keydownHandler, {
        capture: globalState.keydownListenerCapture
      });
      globalState.keydownHandlerAdded = true;
    }
  };

  /**
   * @param {number} index
   * @param {number} increment
   */
  const setFocus = (index, increment) => {
    const focusableElements = getFocusableElements();
    // search for visible elements and select the next possible match
    if (focusableElements.length) {
      index = index + increment;

      // rollover to first item
      if (index === focusableElements.length) {
        index = 0;

        // go to last item
      } else if (index === -1) {
        index = focusableElements.length - 1;
      }
      focusableElements[index].focus();
      return;
    }
    // no visible focusable elements, focus the popup
    getPopup().focus();
  };
  const arrowKeysNextButton = ['ArrowRight', 'ArrowDown'];
  const arrowKeysPreviousButton = ['ArrowLeft', 'ArrowUp'];

  /**
   * @param {SweetAlert} instance
   * @param {KeyboardEvent} event
   * @param {Function} dismissWith
   */
  const keydownHandler = (instance, event, dismissWith) => {
    const innerParams = privateProps.innerParams.get(instance);
    if (!innerParams) {
      return; // This instance has already been destroyed
    }

    // Ignore keydown during IME composition
    // https://developer.mozilla.org/en-US/docs/Web/API/Document/keydown_event#ignoring_keydown_during_ime_composition
    // https://github.com/sweetalert2/sweetalert2/issues/720
    // https://github.com/sweetalert2/sweetalert2/issues/2406
    if (event.isComposing || event.keyCode === 229) {
      return;
    }
    if (innerParams.stopKeydownPropagation) {
      event.stopPropagation();
    }

    // ENTER
    if (event.key === 'Enter') {
      handleEnter(instance, event, innerParams);
    }

    // TAB
    else if (event.key === 'Tab') {
      handleTab(event);
    }

    // ARROWS - switch focus between buttons
    else if ([...arrowKeysNextButton, ...arrowKeysPreviousButton].includes(event.key)) {
      handleArrows(event.key);
    }

    // ESC
    else if (event.key === 'Escape') {
      handleEsc(event, innerParams, dismissWith);
    }
  };

  /**
   * @param {SweetAlert} instance
   * @param {KeyboardEvent} event
   * @param {SweetAlertOptions} innerParams
   */
  const handleEnter = (instance, event, innerParams) => {
    // https://github.com/sweetalert2/sweetalert2/issues/2386
    if (!callIfFunction(innerParams.allowEnterKey)) {
      return;
    }
    if (event.target && instance.getInput() && event.target instanceof HTMLElement && event.target.outerHTML === instance.getInput().outerHTML) {
      if (['textarea', 'file'].includes(innerParams.input)) {
        return; // do not submit
      }

      clickConfirm();
      event.preventDefault();
    }
  };

  /**
   * @param {KeyboardEvent} event
   */
  const handleTab = event => {
    const targetElement = event.target;
    const focusableElements = getFocusableElements();
    let btnIndex = -1;
    for (let i = 0; i < focusableElements.length; i++) {
      if (targetElement === focusableElements[i]) {
        btnIndex = i;
        break;
      }
    }

    // Cycle to the next button
    if (!event.shiftKey) {
      setFocus(btnIndex, 1);
    }

    // Cycle to the prev button
    else {
      setFocus(btnIndex, -1);
    }
    event.stopPropagation();
    event.preventDefault();
  };

  /**
   * @param {string} key
   */
  const handleArrows = key => {
    const confirmButton = getConfirmButton();
    const denyButton = getDenyButton();
    const cancelButton = getCancelButton();
    /** @type HTMLElement[] */
    const buttons = [confirmButton, denyButton, cancelButton];
    if (document.activeElement instanceof HTMLElement && !buttons.includes(document.activeElement)) {
      return;
    }
    const sibling = arrowKeysNextButton.includes(key) ? 'nextElementSibling' : 'previousElementSibling';
    let buttonToFocus = document.activeElement;
    for (let i = 0; i < getActions().children.length; i++) {
      buttonToFocus = buttonToFocus[sibling];
      if (!buttonToFocus) {
        return;
      }
      if (buttonToFocus instanceof HTMLButtonElement && isVisible$1(buttonToFocus)) {
        break;
      }
    }
    if (buttonToFocus instanceof HTMLButtonElement) {
      buttonToFocus.focus();
    }
  };

  /**
   * @param {KeyboardEvent} event
   * @param {SweetAlertOptions} innerParams
   * @param {Function} dismissWith
   */
  const handleEsc = (event, innerParams, dismissWith) => {
    if (callIfFunction(innerParams.allowEscapeKey)) {
      event.preventDefault();
      dismissWith(DismissReason.esc);
    }
  };

  /**
   * This module contains `WeakMap`s for each effectively-"private  property" that a `Swal` has.
   * For example, to set the private property "foo" of `this` to "bar", you can `privateProps.foo.set(this, 'bar')`
   * This is the approach that Babel will probably take to implement private methods/fields
   *   https://github.com/tc39/proposal-private-methods
   *   https://github.com/babel/babel/pull/7555
   * Once we have the changes from that PR in Babel, and our core class fits reasonable in *one module*
   *   then we can use that language feature.
   */

  var privateMethods = {
    swalPromiseResolve: new WeakMap(),
    swalPromiseReject: new WeakMap()
  };

  // From https://developer.paciellogroup.com/blog/2018/06/the-current-state-of-modal-dialog-accessibility/
  // Adding aria-hidden="true" to elements outside of the active modal dialog ensures that
  // elements not within the active modal dialog will not be surfaced if a user opens a screen
  // reader’s list of elements (headings, form controls, landmarks, etc.) in the document.

  const setAriaHidden = () => {
    const bodyChildren = Array.from(document.body.children);
    bodyChildren.forEach(el => {
      if (el === getContainer() || el.contains(getContainer())) {
        return;
      }
      if (el.hasAttribute('aria-hidden')) {
        el.setAttribute('data-previous-aria-hidden', el.getAttribute('aria-hidden') || '');
      }
      el.setAttribute('aria-hidden', 'true');
    });
  };
  const unsetAriaHidden = () => {
    const bodyChildren = Array.from(document.body.children);
    bodyChildren.forEach(el => {
      if (el.hasAttribute('data-previous-aria-hidden')) {
        el.setAttribute('aria-hidden', el.getAttribute('data-previous-aria-hidden') || '');
        el.removeAttribute('data-previous-aria-hidden');
      } else {
        el.removeAttribute('aria-hidden');
      }
    });
  };

  /* istanbul ignore file */

  // @ts-ignore
  const isSafariOrIOS = typeof window !== 'undefined' && !!window.GestureEvent; // true for Safari desktop + all iOS browsers https://stackoverflow.com/a/70585394

  // Fix iOS scrolling http://stackoverflow.com/q/39626302

  const iOSfix = () => {
    if (isSafariOrIOS && !hasClass(document.body, swalClasses.iosfix)) {
      const offset = document.body.scrollTop;
      document.body.style.top = `${offset * -1}px`;
      addClass(document.body, swalClasses.iosfix);
      lockBodyScroll();
    }
  };

  /**
   * https://github.com/sweetalert2/sweetalert2/issues/1246
   */
  const lockBodyScroll = () => {
    const container = getContainer();
    let preventTouchMove;
    /**
     * @param {TouchEvent} event
     */
    container.ontouchstart = event => {
      preventTouchMove = shouldPreventTouchMove(event);
    };
    /**
     * @param {TouchEvent} event
     */
    container.ontouchmove = event => {
      if (preventTouchMove) {
        event.preventDefault();
        event.stopPropagation();
      }
    };
  };

  /**
   * @param {TouchEvent} event
   * @returns {boolean}
   */
  const shouldPreventTouchMove = event => {
    const target = event.target;
    const container = getContainer();
    if (isStylus(event) || isZoom(event)) {
      return false;
    }
    if (target === container) {
      return true;
    }
    if (!isScrollable(container) && target instanceof HTMLElement && target.tagName !== 'INPUT' &&
    // #1603
    target.tagName !== 'TEXTAREA' &&
    // #2266
    !(isScrollable(getHtmlContainer()) &&
    // #1944
    getHtmlContainer().contains(target))) {
      return true;
    }
    return false;
  };

  /**
   * https://github.com/sweetalert2/sweetalert2/issues/1786
   *
   * @param {*} event
   * @returns {boolean}
   */
  const isStylus = event => {
    return event.touches && event.touches.length && event.touches[0].touchType === 'stylus';
  };

  /**
   * https://github.com/sweetalert2/sweetalert2/issues/1891
   *
   * @param {TouchEvent} event
   * @returns {boolean}
   */
  const isZoom = event => {
    return event.touches && event.touches.length > 1;
  };
  const undoIOSfix = () => {
    if (hasClass(document.body, swalClasses.iosfix)) {
      const offset = parseInt(document.body.style.top, 10);
      removeClass(document.body, swalClasses.iosfix);
      document.body.style.top = '';
      document.body.scrollTop = offset * -1;
    }
  };

  /**
   * Measure scrollbar width for padding body during modal show/hide
   * https://github.com/twbs/bootstrap/blob/master/js/src/modal.js
   *
   * @returns {number}
   */
  const measureScrollbar = () => {
    const scrollDiv = document.createElement('div');
    scrollDiv.className = swalClasses['scrollbar-measure'];
    document.body.appendChild(scrollDiv);
    const scrollbarWidth = scrollDiv.getBoundingClientRect().width - scrollDiv.clientWidth;
    document.body.removeChild(scrollDiv);
    return scrollbarWidth;
  };

  /**
   * Remember state in cases where opening and handling a modal will fiddle with it.
   * @type {number | null}
   */
  let previousBodyPadding = null;
  const fixScrollbar = () => {
    // for queues, do not do this more than once
    if (previousBodyPadding !== null) {
      return;
    }
    // if the body has overflow
    if (document.body.scrollHeight > window.innerHeight) {
      // add padding so the content doesn't shift after removal of scrollbar
      previousBodyPadding = parseInt(window.getComputedStyle(document.body).getPropertyValue('padding-right'));
      document.body.style.paddingRight = `${previousBodyPadding + measureScrollbar()}px`;
    }
  };
  const undoScrollbar = () => {
    if (previousBodyPadding !== null) {
      document.body.style.paddingRight = `${previousBodyPadding}px`;
      previousBodyPadding = null;
    }
  };

  /**
   * @param {SweetAlert} instance
   * @param {HTMLElement} container
   * @param {boolean} returnFocus
   * @param {Function} didClose
   */
  function removePopupAndResetState(instance, container, returnFocus, didClose) {
    if (isToast()) {
      triggerDidCloseAndDispose(instance, didClose);
    } else {
      restoreActiveElement(returnFocus).then(() => triggerDidCloseAndDispose(instance, didClose));
      removeKeydownHandler(globalState);
    }

    // workaround for https://github.com/sweetalert2/sweetalert2/issues/2088
    // for some reason removing the container in Safari will scroll the document to bottom
    if (isSafariOrIOS) {
      container.setAttribute('style', 'display:none !important');
      container.removeAttribute('class');
      container.innerHTML = '';
    } else {
      container.remove();
    }
    if (isModal()) {
      undoScrollbar();
      undoIOSfix();
      unsetAriaHidden();
    }
    removeBodyClasses();
  }

  /**
   * Remove SweetAlert2 classes from body
   */
  function removeBodyClasses() {
    removeClass([document.documentElement, document.body], [swalClasses.shown, swalClasses['height-auto'], swalClasses['no-backdrop'], swalClasses['toast-shown']]);
  }

  /**
   * Instance method to close sweetAlert
   *
   * @param {any} resolveValue
   */
  function close(resolveValue) {
    resolveValue = prepareResolveValue(resolveValue);
    const swalPromiseResolve = privateMethods.swalPromiseResolve.get(this);
    const didClose = triggerClosePopup(this);
    if (this.isAwaitingPromise) {
      // A swal awaiting for a promise (after a click on Confirm or Deny) cannot be dismissed anymore #2335
      if (!resolveValue.isDismissed) {
        handleAwaitingPromise(this);
        swalPromiseResolve(resolveValue);
      }
    } else if (didClose) {
      // Resolve Swal promise
      swalPromiseResolve(resolveValue);
    }
  }
  const triggerClosePopup = instance => {
    const popup = getPopup();
    if (!popup) {
      return false;
    }
    const innerParams = privateProps.innerParams.get(instance);
    if (!innerParams || hasClass(popup, innerParams.hideClass.popup)) {
      return false;
    }
    removeClass(popup, innerParams.showClass.popup);
    addClass(popup, innerParams.hideClass.popup);
    const backdrop = getContainer();
    removeClass(backdrop, innerParams.showClass.backdrop);
    addClass(backdrop, innerParams.hideClass.backdrop);
    handlePopupAnimation(instance, popup, innerParams);
    return true;
  };

  /**
   * @param {any} error
   */
  function rejectPromise(error) {
    const rejectPromise = privateMethods.swalPromiseReject.get(this);
    handleAwaitingPromise(this);
    if (rejectPromise) {
      // Reject Swal promise
      rejectPromise(error);
    }
  }

  /**
   * @param {SweetAlert} instance
   */
  const handleAwaitingPromise = instance => {
    if (instance.isAwaitingPromise) {
      delete instance.isAwaitingPromise;
      // The instance might have been previously partly destroyed, we must resume the destroy process in this case #2335
      if (!privateProps.innerParams.get(instance)) {
        instance._destroy();
      }
    }
  };

  /**
   * @param {any} resolveValue
   * @returns {SweetAlertResult}
   */
  const prepareResolveValue = resolveValue => {
    // When user calls Swal.close()
    if (typeof resolveValue === 'undefined') {
      return {
        isConfirmed: false,
        isDenied: false,
        isDismissed: true
      };
    }
    return Object.assign({
      isConfirmed: false,
      isDenied: false,
      isDismissed: false
    }, resolveValue);
  };

  /**
   * @param {SweetAlert} instance
   * @param {HTMLElement} popup
   * @param {SweetAlertOptions} innerParams
   */
  const handlePopupAnimation = (instance, popup, innerParams) => {
    const container = getContainer();
    // If animation is supported, animate
    const animationIsSupported = animationEndEvent && hasCssAnimation(popup);
    if (typeof innerParams.willClose === 'function') {
      innerParams.willClose(popup);
    }
    if (animationIsSupported) {
      animatePopup(instance, popup, container, innerParams.returnFocus, innerParams.didClose);
    } else {
      // Otherwise, remove immediately
      removePopupAndResetState(instance, container, innerParams.returnFocus, innerParams.didClose);
    }
  };

  /**
   * @param {SweetAlert} instance
   * @param {HTMLElement} popup
   * @param {HTMLElement} container
   * @param {boolean} returnFocus
   * @param {Function} didClose
   */
  const animatePopup = (instance, popup, container, returnFocus, didClose) => {
    globalState.swalCloseEventFinishedCallback = removePopupAndResetState.bind(null, instance, container, returnFocus, didClose);
    popup.addEventListener(animationEndEvent, function (e) {
      if (e.target === popup) {
        globalState.swalCloseEventFinishedCallback();
        delete globalState.swalCloseEventFinishedCallback;
      }
    });
  };

  /**
   * @param {SweetAlert} instance
   * @param {Function} didClose
   */
  const triggerDidCloseAndDispose = (instance, didClose) => {
    setTimeout(() => {
      if (typeof didClose === 'function') {
        didClose.bind(instance.params)();
      }
      // instance might have been destroyed already
      if (instance._destroy) {
        instance._destroy();
      }
    });
  };

  /**
   * Shows loader (spinner), this is useful with AJAX requests.
   * By default the loader be shown instead of the "Confirm" button.
   *
   * @param {HTMLButtonElement | null} [buttonToReplace]
   */
  const showLoading = buttonToReplace => {
    let popup = getPopup();
    if (!popup) {
      new Swal(); // eslint-disable-line no-new
    }

    popup = getPopup();
    if (!popup) {
      return;
    }
    const loader = getLoader();
    if (isToast()) {
      hide(getIcon());
    } else {
      replaceButton(popup, buttonToReplace);
    }
    show(loader);
    popup.setAttribute('data-loading', 'true');
    popup.setAttribute('aria-busy', 'true');
    popup.focus();
  };

  /**
   * @param {HTMLElement} popup
   * @param {HTMLButtonElement | null} [buttonToReplace]
   */
  const replaceButton = (popup, buttonToReplace) => {
    const actions = getActions();
    const loader = getLoader();
    if (!actions || !loader) {
      return;
    }
    if (!buttonToReplace && isVisible$1(getConfirmButton())) {
      buttonToReplace = getConfirmButton();
    }
    show(actions);
    if (buttonToReplace) {
      hide(buttonToReplace);
      loader.setAttribute('data-button-to-replace', buttonToReplace.className);
      actions.insertBefore(loader, buttonToReplace);
    }
    addClass([popup, actions], swalClasses.loading);
  };

  /**
   * @typedef { string | number | boolean | undefined } InputValue
   */

  /**
   * @param {SweetAlert} instance
   * @param {SweetAlertOptions} params
   */
  const handleInputOptionsAndValue = (instance, params) => {
    if (params.input === 'select' || params.input === 'radio') {
      handleInputOptions(instance, params);
    } else if (['text', 'email', 'number', 'tel', 'textarea'].some(i => i === params.input) && (hasToPromiseFn(params.inputValue) || isPromise(params.inputValue))) {
      showLoading(getConfirmButton());
      handleInputValue(instance, params);
    }
  };

  /**
   * @param {SweetAlert} instance
   * @param {SweetAlertOptions} innerParams
   * @returns {string | number | File | FileList | null}
   */
  const getInputValue = (instance, innerParams) => {
    const input = instance.getInput();
    if (!input) {
      return null;
    }
    switch (innerParams.input) {
      case 'checkbox':
        return getCheckboxValue(input);
      case 'radio':
        return getRadioValue(input);
      case 'file':
        return getFileValue(input);
      default:
        return innerParams.inputAutoTrim ? input.value.trim() : input.value;
    }
  };

  /**
   * @param {HTMLInputElement} input
   * @returns {number}
   */
  const getCheckboxValue = input => input.checked ? 1 : 0;

  /**
   * @param {HTMLInputElement} input
   * @returns {string | null}
   */
  const getRadioValue = input => input.checked ? input.value : null;

  /**
   * @param {HTMLInputElement} input
   * @returns {FileList | File | null}
   */
  const getFileValue = input => input.files && input.files.length ? input.getAttribute('multiple') !== null ? input.files : input.files[0] : null;

  /**
   * @param {SweetAlert} instance
   * @param {SweetAlertOptions} params
   */
  const handleInputOptions = (instance, params) => {
    const popup = getPopup();
    if (!popup) {
      return;
    }
    /**
     * @param {Record<string, any>} inputOptions
     */
    const processInputOptions = inputOptions => {
      if (params.input === 'select') {
        populateSelectOptions(popup, formatInputOptions(inputOptions), params);
      } else if (params.input === 'radio') {
        populateRadioOptions(popup, formatInputOptions(inputOptions), params);
      }
    };
    if (hasToPromiseFn(params.inputOptions) || isPromise(params.inputOptions)) {
      showLoading(getConfirmButton());
      asPromise(params.inputOptions).then(inputOptions => {
        instance.hideLoading();
        processInputOptions(inputOptions);
      });
    } else if (typeof params.inputOptions === 'object') {
      processInputOptions(params.inputOptions);
    } else {
      error(`Unexpected type of inputOptions! Expected object, Map or Promise, got ${typeof params.inputOptions}`);
    }
  };

  /**
   * @param {SweetAlert} instance
   * @param {SweetAlertOptions} params
   */
  const handleInputValue = (instance, params) => {
    const input = instance.getInput();
    if (!input) {
      return;
    }
    hide(input);
    asPromise(params.inputValue).then(inputValue => {
      input.value = params.input === 'number' ? `${parseFloat(inputValue) || 0}` : `${inputValue}`;
      show(input);
      input.focus();
      instance.hideLoading();
    }).catch(err => {
      error(`Error in inputValue promise: ${err}`);
      input.value = '';
      show(input);
      input.focus();
      instance.hideLoading();
    });
  };

  /**
   * @param {HTMLElement} popup
   * @param {InputOptionFlattened[]} inputOptions
   * @param {SweetAlertOptions} params
   */
  function populateSelectOptions(popup, inputOptions, params) {
    const select = getDirectChildByClass(popup, swalClasses.select);
    if (!select) {
      return;
    }
    /**
     * @param {HTMLElement} parent
     * @param {string} optionLabel
     * @param {string} optionValue
     */
    const renderOption = (parent, optionLabel, optionValue) => {
      const option = document.createElement('option');
      option.value = optionValue;
      setInnerHtml(option, optionLabel);
      option.selected = isSelected(optionValue, params.inputValue);
      parent.appendChild(option);
    };
    inputOptions.forEach(inputOption => {
      const optionValue = inputOption[0];
      const optionLabel = inputOption[1];
      // <optgroup> spec:
      // https://www.w3.org/TR/html401/interact/forms.html#h-17.6
      // "...all OPTGROUP elements must be specified directly within a SELECT element (i.e., groups may not be nested)..."
      // check whether this is a <optgroup>
      if (Array.isArray(optionLabel)) {
        // if it is an array, then it is an <optgroup>
        const optgroup = document.createElement('optgroup');
        optgroup.label = optionValue;
        optgroup.disabled = false; // not configurable for now
        select.appendChild(optgroup);
        optionLabel.forEach(o => renderOption(optgroup, o[1], o[0]));
      } else {
        // case of <option>
        renderOption(select, optionLabel, optionValue);
      }
    });
    select.focus();
  }

  /**
   * @param {HTMLElement} popup
   * @param {InputOptionFlattened[]} inputOptions
   * @param {SweetAlertOptions} params
   */
  function populateRadioOptions(popup, inputOptions, params) {
    const radio = getDirectChildByClass(popup, swalClasses.radio);
    if (!radio) {
      return;
    }
    inputOptions.forEach(inputOption => {
      const radioValue = inputOption[0];
      const radioLabel = inputOption[1];
      const radioInput = document.createElement('input');
      const radioLabelElement = document.createElement('label');
      radioInput.type = 'radio';
      radioInput.name = swalClasses.radio;
      radioInput.value = radioValue;
      if (isSelected(radioValue, params.inputValue)) {
        radioInput.checked = true;
      }
      const label = document.createElement('span');
      setInnerHtml(label, radioLabel);
      label.className = swalClasses.label;
      radioLabelElement.appendChild(radioInput);
      radioLabelElement.appendChild(label);
      radio.appendChild(radioLabelElement);
    });
    const radios = radio.querySelectorAll('input');
    if (radios.length) {
      radios[0].focus();
    }
  }

  /**
   * Converts `inputOptions` into an array of `[value, label]`s
   *
   * @param {Record<string, any>} inputOptions
   * @typedef {string[]} InputOptionFlattened
   * @returns {InputOptionFlattened[]}
   */
  const formatInputOptions = inputOptions => {
    /** @type {InputOptionFlattened[]} */
    const result = [];
    if (inputOptions instanceof Map) {
      inputOptions.forEach((value, key) => {
        let valueFormatted = value;
        if (typeof valueFormatted === 'object') {
          // case of <optgroup>
          valueFormatted = formatInputOptions(valueFormatted);
        }
        result.push([key, valueFormatted]);
      });
    } else {
      Object.keys(inputOptions).forEach(key => {
        let valueFormatted = inputOptions[key];
        if (typeof valueFormatted === 'object') {
          // case of <optgroup>
          valueFormatted = formatInputOptions(valueFormatted);
        }
        result.push([key, valueFormatted]);
      });
    }
    return result;
  };

  /**
   * @param {string} optionValue
   * @param {InputValue | Promise<InputValue> | { toPromise: () => InputValue }} inputValue
   * @returns {boolean}
   */
  const isSelected = (optionValue, inputValue) => {
    return !!inputValue && inputValue.toString() === optionValue.toString();
  };

  /**
   * @param {SweetAlert} instance
   */
  const handleConfirmButtonClick = instance => {
    const innerParams = privateProps.innerParams.get(instance);
    instance.disableButtons();
    if (innerParams.input) {
      handleConfirmOrDenyWithInput(instance, 'confirm');
    } else {
      confirm(instance, true);
    }
  };

  /**
   * @param {SweetAlert} instance
   */
  const handleDenyButtonClick = instance => {
    const innerParams = privateProps.innerParams.get(instance);
    instance.disableButtons();
    if (innerParams.returnInputValueOnDeny) {
      handleConfirmOrDenyWithInput(instance, 'deny');
    } else {
      deny(instance, false);
    }
  };

  /**
   * @param {SweetAlert} instance
   * @param {Function} dismissWith
   */
  const handleCancelButtonClick = (instance, dismissWith) => {
    instance.disableButtons();
    dismissWith(DismissReason.cancel);
  };

  /**
   * @param {SweetAlert} instance
   * @param {'confirm' | 'deny'} type
   */
  const handleConfirmOrDenyWithInput = (instance, type) => {
    const innerParams = privateProps.innerParams.get(instance);
    if (!innerParams.input) {
      error(`The "input" parameter is needed to be set when using returnInputValueOn${capitalizeFirstLetter(type)}`);
      return;
    }
    const input = instance.getInput();
    const inputValue = getInputValue(instance, innerParams);
    if (innerParams.inputValidator) {
      handleInputValidator(instance, inputValue, type);
    } else if (input && !input.checkValidity()) {
      instance.enableButtons();
      instance.showValidationMessage(innerParams.validationMessage);
    } else if (type === 'deny') {
      deny(instance, inputValue);
    } else {
      confirm(instance, inputValue);
    }
  };

  /**
   * @param {SweetAlert} instance
   * @param {string | number | File | FileList | null} inputValue
   * @param {'confirm' | 'deny'} type
   */
  const handleInputValidator = (instance, inputValue, type) => {
    const innerParams = privateProps.innerParams.get(instance);
    instance.disableInput();
    const validationPromise = Promise.resolve().then(() => asPromise(innerParams.inputValidator(inputValue, innerParams.validationMessage)));
    validationPromise.then(validationMessage => {
      instance.enableButtons();
      instance.enableInput();
      if (validationMessage) {
        instance.showValidationMessage(validationMessage);
      } else if (type === 'deny') {
        deny(instance, inputValue);
      } else {
        confirm(instance, inputValue);
      }
    });
  };

  /**
   * @param {SweetAlert} instance
   * @param {any} value
   */
  const deny = (instance, value) => {
    const innerParams = privateProps.innerParams.get(instance || undefined);
    if (innerParams.showLoaderOnDeny) {
      showLoading(getDenyButton());
    }
    if (innerParams.preDeny) {
      instance.isAwaitingPromise = true; // Flagging the instance as awaiting a promise so it's own promise's reject/resolve methods doesn't get destroyed until the result from this preDeny's promise is received
      const preDenyPromise = Promise.resolve().then(() => asPromise(innerParams.preDeny(value, innerParams.validationMessage)));
      preDenyPromise.then(preDenyValue => {
        if (preDenyValue === false) {
          instance.hideLoading();
          handleAwaitingPromise(instance);
        } else {
          instance.close({
            isDenied: true,
            value: typeof preDenyValue === 'undefined' ? value : preDenyValue
          });
        }
      }).catch(error => rejectWith(instance || undefined, error));
    } else {
      instance.close({
        isDenied: true,
        value
      });
    }
  };

  /**
   * @param {SweetAlert} instance
   * @param {any} value
   */
  const succeedWith = (instance, value) => {
    instance.close({
      isConfirmed: true,
      value
    });
  };

  /**
   *
   * @param {SweetAlert} instance
   * @param {string} error
   */
  const rejectWith = (instance, error) => {
    instance.rejectPromise(error);
  };

  /**
   *
   * @param {SweetAlert} instance
   * @param {any} value
   */
  const confirm = (instance, value) => {
    const innerParams = privateProps.innerParams.get(instance || undefined);
    if (innerParams.showLoaderOnConfirm) {
      showLoading();
    }
    if (innerParams.preConfirm) {
      instance.resetValidationMessage();
      instance.isAwaitingPromise = true; // Flagging the instance as awaiting a promise so it's own promise's reject/resolve methods doesn't get destroyed until the result from this preConfirm's promise is received
      const preConfirmPromise = Promise.resolve().then(() => asPromise(innerParams.preConfirm(value, innerParams.validationMessage)));
      preConfirmPromise.then(preConfirmValue => {
        if (isVisible$1(getValidationMessage()) || preConfirmValue === false) {
          instance.hideLoading();
          handleAwaitingPromise(instance);
        } else {
          succeedWith(instance, typeof preConfirmValue === 'undefined' ? value : preConfirmValue);
        }
      }).catch(error => rejectWith(instance || undefined, error));
    } else {
      succeedWith(instance, value);
    }
  };

  /**
   * Hides loader and shows back the button which was hidden by .showLoading()
   */
  function hideLoading() {
    // do nothing if popup is closed
    const innerParams = privateProps.innerParams.get(this);
    if (!innerParams) {
      return;
    }
    const domCache = privateProps.domCache.get(this);
    hide(domCache.loader);
    if (isToast()) {
      if (innerParams.icon) {
        show(getIcon());
      }
    } else {
      showRelatedButton(domCache);
    }
    removeClass([domCache.popup, domCache.actions], swalClasses.loading);
    domCache.popup.removeAttribute('aria-busy');
    domCache.popup.removeAttribute('data-loading');
    domCache.confirmButton.disabled = false;
    domCache.denyButton.disabled = false;
    domCache.cancelButton.disabled = false;
  }
  const showRelatedButton = domCache => {
    const buttonToReplace = domCache.popup.getElementsByClassName(domCache.loader.getAttribute('data-button-to-replace'));
    if (buttonToReplace.length) {
      show(buttonToReplace[0], 'inline-block');
    } else if (allButtonsAreHidden()) {
      hide(domCache.actions);
    }
  };

  /**
   * Gets the input DOM node, this method works with input parameter.
   *
   * @returns {HTMLInputElement | null}
   */
  function getInput() {
    const innerParams = privateProps.innerParams.get(this);
    const domCache = privateProps.domCache.get(this);
    if (!domCache) {
      return null;
    }
    return getInput$1(domCache.popup, innerParams.input);
  }

  /**
   * @param {SweetAlert} instance
   * @param {string[]} buttons
   * @param {boolean} disabled
   */
  function setButtonsDisabled(instance, buttons, disabled) {
    const domCache = privateProps.domCache.get(instance);
    buttons.forEach(button => {
      domCache[button].disabled = disabled;
    });
  }

  /**
   * @param {HTMLInputElement | null} input
   * @param {boolean} disabled
   */
  function setInputDisabled(input, disabled) {
    const popup = getPopup();
    if (!popup || !input) {
      return;
    }
    if (input.type === 'radio') {
      /** @type {NodeListOf<HTMLInputElement>} */
      const radios = popup.querySelectorAll(`[name="${swalClasses.radio}"]`);
      for (let i = 0; i < radios.length; i++) {
        radios[i].disabled = disabled;
      }
    } else {
      input.disabled = disabled;
    }
  }

  /**
   * Enable all the buttons
   * @this {SweetAlert}
   */
  function enableButtons() {
    setButtonsDisabled(this, ['confirmButton', 'denyButton', 'cancelButton'], false);
  }

  /**
   * Disable all the buttons
   * @this {SweetAlert}
   */
  function disableButtons() {
    setButtonsDisabled(this, ['confirmButton', 'denyButton', 'cancelButton'], true);
  }

  /**
   * Enable the input field
   * @this {SweetAlert}
   */
  function enableInput() {
    setInputDisabled(this.getInput(), false);
  }

  /**
   * Disable the input field
   * @this {SweetAlert}
   */
  function disableInput() {
    setInputDisabled(this.getInput(), true);
  }

  /**
   * Show block with validation message
   *
   * @param {string} error
   */
  function showValidationMessage(error) {
    const domCache = privateProps.domCache.get(this);
    const params = privateProps.innerParams.get(this);
    setInnerHtml(domCache.validationMessage, error);
    domCache.validationMessage.className = swalClasses['validation-message'];
    if (params.customClass && params.customClass.validationMessage) {
      addClass(domCache.validationMessage, params.customClass.validationMessage);
    }
    show(domCache.validationMessage);
    const input = this.getInput();
    if (input) {
      input.setAttribute('aria-invalid', true);
      input.setAttribute('aria-describedby', swalClasses['validation-message']);
      focusInput(input);
      addClass(input, swalClasses.inputerror);
    }
  }

  /**
   * Hide block with validation message
   */
  function resetValidationMessage() {
    const domCache = privateProps.domCache.get(this);
    if (domCache.validationMessage) {
      hide(domCache.validationMessage);
    }
    const input = this.getInput();
    if (input) {
      input.removeAttribute('aria-invalid');
      input.removeAttribute('aria-describedby');
      removeClass(input, swalClasses.inputerror);
    }
  }

  const defaultParams = {
    title: '',
    titleText: '',
    text: '',
    html: '',
    footer: '',
    icon: undefined,
    iconColor: undefined,
    iconHtml: undefined,
    template: undefined,
    toast: false,
    showClass: {
      popup: 'swal2-show',
      backdrop: 'swal2-backdrop-show',
      icon: 'swal2-icon-show'
    },
    hideClass: {
      popup: 'swal2-hide',
      backdrop: 'swal2-backdrop-hide',
      icon: 'swal2-icon-hide'
    },
    customClass: {},
    target: 'body',
    color: undefined,
    backdrop: true,
    heightAuto: true,
    allowOutsideClick: true,
    allowEscapeKey: true,
    allowEnterKey: true,
    stopKeydownPropagation: true,
    keydownListenerCapture: false,
    showConfirmButton: true,
    showDenyButton: false,
    showCancelButton: false,
    preConfirm: undefined,
    preDeny: undefined,
    confirmButtonText: 'OK',
    confirmButtonAriaLabel: '',
    confirmButtonColor: undefined,
    denyButtonText: 'No',
    denyButtonAriaLabel: '',
    denyButtonColor: undefined,
    cancelButtonText: 'Cancel',
    cancelButtonAriaLabel: '',
    cancelButtonColor: undefined,
    buttonsStyling: true,
    reverseButtons: false,
    focusConfirm: true,
    focusDeny: false,
    focusCancel: false,
    returnFocus: true,
    showCloseButton: false,
    closeButtonHtml: '&times;',
    closeButtonAriaLabel: 'Close this dialog',
    loaderHtml: '',
    showLoaderOnConfirm: false,
    showLoaderOnDeny: false,
    imageUrl: undefined,
    imageWidth: undefined,
    imageHeight: undefined,
    imageAlt: '',
    timer: undefined,
    timerProgressBar: false,
    width: undefined,
    padding: undefined,
    background: undefined,
    input: undefined,
    inputPlaceholder: '',
    inputLabel: '',
    inputValue: '',
    inputOptions: {},
    inputAutoFocus: true,
    inputAutoTrim: true,
    inputAttributes: {},
    inputValidator: undefined,
    returnInputValueOnDeny: false,
    validationMessage: undefined,
    grow: false,
    position: 'center',
    progressSteps: [],
    currentProgressStep: undefined,
    progressStepsDistance: undefined,
    willOpen: undefined,
    didOpen: undefined,
    didRender: undefined,
    willClose: undefined,
    didClose: undefined,
    didDestroy: undefined,
    scrollbarPadding: true
  };
  const updatableParams = ['allowEscapeKey', 'allowOutsideClick', 'background', 'buttonsStyling', 'cancelButtonAriaLabel', 'cancelButtonColor', 'cancelButtonText', 'closeButtonAriaLabel', 'closeButtonHtml', 'color', 'confirmButtonAriaLabel', 'confirmButtonColor', 'confirmButtonText', 'currentProgressStep', 'customClass', 'denyButtonAriaLabel', 'denyButtonColor', 'denyButtonText', 'didClose', 'didDestroy', 'footer', 'hideClass', 'html', 'icon', 'iconColor', 'iconHtml', 'imageAlt', 'imageHeight', 'imageUrl', 'imageWidth', 'preConfirm', 'preDeny', 'progressSteps', 'returnFocus', 'reverseButtons', 'showCancelButton', 'showCloseButton', 'showConfirmButton', 'showDenyButton', 'text', 'title', 'titleText', 'willClose'];

  /** @type {Record<string, string>} */
  const deprecatedParams = {};
  const toastIncompatibleParams = ['allowOutsideClick', 'allowEnterKey', 'backdrop', 'focusConfirm', 'focusDeny', 'focusCancel', 'returnFocus', 'heightAuto', 'keydownListenerCapture'];

  /**
   * Is valid parameter
   *
   * @param {string} paramName
   * @returns {boolean}
   */
  const isValidParameter = paramName => {
    return Object.prototype.hasOwnProperty.call(defaultParams, paramName);
  };

  /**
   * Is valid parameter for Swal.update() method
   *
   * @param {string} paramName
   * @returns {boolean}
   */
  const isUpdatableParameter = paramName => {
    return updatableParams.indexOf(paramName) !== -1;
  };

  /**
   * Is deprecated parameter
   *
   * @param {string} paramName
   * @returns {string | undefined}
   */
  const isDeprecatedParameter = paramName => {
    return deprecatedParams[paramName];
  };

  /**
   * @param {string} param
   */
  const checkIfParamIsValid = param => {
    if (!isValidParameter(param)) {
      warn(`Unknown parameter "${param}"`);
    }
  };

  /**
   * @param {string} param
   */
  const checkIfToastParamIsValid = param => {
    if (toastIncompatibleParams.includes(param)) {
      warn(`The parameter "${param}" is incompatible with toasts`);
    }
  };

  /**
   * @param {string} param
   */
  const checkIfParamIsDeprecated = param => {
    const isDeprecated = isDeprecatedParameter(param);
    if (isDeprecated) {
      warnAboutDeprecation(param, isDeprecated);
    }
  };

  /**
   * Show relevant warnings for given params
   *
   * @param {SweetAlertOptions} params
   */
  const showWarningsForParams = params => {
    if (params.backdrop === false && params.allowOutsideClick) {
      warn('"allowOutsideClick" parameter requires `backdrop` parameter to be set to `true`');
    }
    for (const param in params) {
      checkIfParamIsValid(param);
      if (params.toast) {
        checkIfToastParamIsValid(param);
      }
      checkIfParamIsDeprecated(param);
    }
  };

  /**
   * Updates popup parameters.
   *
   * @param {SweetAlertOptions} params
   */
  function update(params) {
    const popup = getPopup();
    const innerParams = privateProps.innerParams.get(this);
    if (!popup || hasClass(popup, innerParams.hideClass.popup)) {
      warn(`You're trying to update the closed or closing popup, that won't work. Use the update() method in preConfirm parameter or show a new popup.`);
      return;
    }
    const validUpdatableParams = filterValidParams(params);
    const updatedParams = Object.assign({}, innerParams, validUpdatableParams);
    render(this, updatedParams);
    privateProps.innerParams.set(this, updatedParams);
    Object.defineProperties(this, {
      params: {
        value: Object.assign({}, this.params, params),
        writable: false,
        enumerable: true
      }
    });
  }

  /**
   * @param {SweetAlertOptions} params
   * @returns {SweetAlertOptions}
   */
  const filterValidParams = params => {
    const validUpdatableParams = {};
    Object.keys(params).forEach(param => {
      if (isUpdatableParameter(param)) {
        validUpdatableParams[param] = params[param];
      } else {
        warn(`Invalid parameter to update: ${param}`);
      }
    });
    return validUpdatableParams;
  };

  /**
   * Dispose the current SweetAlert2 instance
   */
  function _destroy() {
    const domCache = privateProps.domCache.get(this);
    const innerParams = privateProps.innerParams.get(this);
    if (!innerParams) {
      disposeWeakMaps(this); // The WeakMaps might have been partly destroyed, we must recall it to dispose any remaining WeakMaps #2335
      return; // This instance has already been destroyed
    }

    // Check if there is another Swal closing
    if (domCache.popup && globalState.swalCloseEventFinishedCallback) {
      globalState.swalCloseEventFinishedCallback();
      delete globalState.swalCloseEventFinishedCallback;
    }
    if (typeof innerParams.didDestroy === 'function') {
      innerParams.didDestroy();
    }
    disposeSwal(this);
  }

  /**
   * @param {SweetAlert} instance
   */
  const disposeSwal = instance => {
    disposeWeakMaps(instance);
    // Unset this.params so GC will dispose it (#1569)
    delete instance.params;
    // Unset globalState props so GC will dispose globalState (#1569)
    delete globalState.keydownHandler;
    delete globalState.keydownTarget;
    // Unset currentInstance
    delete globalState.currentInstance;
  };

  /**
   * @param {SweetAlert} instance
   */
  const disposeWeakMaps = instance => {
    // If the current instance is awaiting a promise result, we keep the privateMethods to call them once the promise result is retrieved #2335
    if (instance.isAwaitingPromise) {
      unsetWeakMaps(privateProps, instance);
      instance.isAwaitingPromise = true;
    } else {
      unsetWeakMaps(privateMethods, instance);
      unsetWeakMaps(privateProps, instance);
      delete instance.isAwaitingPromise;
      // Unset instance methods
      delete instance.disableButtons;
      delete instance.enableButtons;
      delete instance.getInput;
      delete instance.disableInput;
      delete instance.enableInput;
      delete instance.hideLoading;
      delete instance.disableLoading;
      delete instance.showValidationMessage;
      delete instance.resetValidationMessage;
      delete instance.close;
      delete instance.closePopup;
      delete instance.closeModal;
      delete instance.closeToast;
      delete instance.rejectPromise;
      delete instance.update;
      delete instance._destroy;
    }
  };

  /**
   * @param {object} obj
   * @param {SweetAlert} instance
   */
  const unsetWeakMaps = (obj, instance) => {
    for (const i in obj) {
      obj[i].delete(instance);
    }
  };

  var instanceMethods = /*#__PURE__*/Object.freeze({
    __proto__: null,
    _destroy: _destroy,
    close: close,
    closeModal: close,
    closePopup: close,
    closeToast: close,
    disableButtons: disableButtons,
    disableInput: disableInput,
    disableLoading: hideLoading,
    enableButtons: enableButtons,
    enableInput: enableInput,
    getInput: getInput,
    handleAwaitingPromise: handleAwaitingPromise,
    hideLoading: hideLoading,
    rejectPromise: rejectPromise,
    resetValidationMessage: resetValidationMessage,
    showValidationMessage: showValidationMessage,
    update: update
  });

  const handlePopupClick = (instance, domCache, dismissWith) => {
    const innerParams = privateProps.innerParams.get(instance);
    if (innerParams.toast) {
      handleToastClick(instance, domCache, dismissWith);
    } else {
      // Ignore click events that had mousedown on the popup but mouseup on the container
      // This can happen when the user drags a slider
      handleModalMousedown(domCache);

      // Ignore click events that had mousedown on the container but mouseup on the popup
      handleContainerMousedown(domCache);
      handleModalClick(instance, domCache, dismissWith);
    }
  };
  const handleToastClick = (instance, domCache, dismissWith) => {
    // Closing toast by internal click
    domCache.popup.onclick = () => {
      const innerParams = privateProps.innerParams.get(instance);
      if (innerParams && (isAnyButtonShown(innerParams) || innerParams.timer || innerParams.input)) {
        return;
      }
      dismissWith(DismissReason.close);
    };
  };

  /**
   * @param {*} innerParams
   * @returns {boolean}
   */
  const isAnyButtonShown = innerParams => {
    return innerParams.showConfirmButton || innerParams.showDenyButton || innerParams.showCancelButton || innerParams.showCloseButton;
  };
  let ignoreOutsideClick = false;
  const handleModalMousedown = domCache => {
    domCache.popup.onmousedown = () => {
      domCache.container.onmouseup = function (e) {
        domCache.container.onmouseup = undefined;
        // We only check if the mouseup target is the container because usually it doesn't
        // have any other direct children aside of the popup
        if (e.target === domCache.container) {
          ignoreOutsideClick = true;
        }
      };
    };
  };
  const handleContainerMousedown = domCache => {
    domCache.container.onmousedown = () => {
      domCache.popup.onmouseup = function (e) {
        domCache.popup.onmouseup = undefined;
        // We also need to check if the mouseup target is a child of the popup
        if (e.target === domCache.popup || domCache.popup.contains(e.target)) {
          ignoreOutsideClick = true;
        }
      };
    };
  };
  const handleModalClick = (instance, domCache, dismissWith) => {
    domCache.container.onclick = e => {
      const innerParams = privateProps.innerParams.get(instance);
      if (ignoreOutsideClick) {
        ignoreOutsideClick = false;
        return;
      }
      if (e.target === domCache.container && callIfFunction(innerParams.allowOutsideClick)) {
        dismissWith(DismissReason.backdrop);
      }
    };
  };

  const isJqueryElement = elem => typeof elem === 'object' && elem.jquery;
  const isElement = elem => elem instanceof Element || isJqueryElement(elem);
  const argsToParams = args => {
    const params = {};
    if (typeof args[0] === 'object' && !isElement(args[0])) {
      Object.assign(params, args[0]);
    } else {
      ['title', 'html', 'icon'].forEach((name, index) => {
        const arg = args[index];
        if (typeof arg === 'string' || isElement(arg)) {
          params[name] = arg;
        } else if (arg !== undefined) {
          error(`Unexpected type of ${name}! Expected "string" or "Element", got ${typeof arg}`);
        }
      });
    }
    return params;
  };

  /**
   * Main method to create a new SweetAlert2 popup
   *
   * @param  {...SweetAlertOptions} args
   * @returns {Promise<SweetAlertResult>}
   */
  function fire() {
    const Swal = this; // eslint-disable-line @typescript-eslint/no-this-alias
    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }
    return new Swal(...args);
  }

  /**
   * Returns an extended version of `Swal` containing `params` as defaults.
   * Useful for reusing Swal configuration.
   *
   * For example:
   *
   * Before:
   * const textPromptOptions = { input: 'text', showCancelButton: true }
   * const {value: firstName} = await Swal.fire({ ...textPromptOptions, title: 'What is your first name?' })
   * const {value: lastName} = await Swal.fire({ ...textPromptOptions, title: 'What is your last name?' })
   *
   * After:
   * const TextPrompt = Swal.mixin({ input: 'text', showCancelButton: true })
   * const {value: firstName} = await TextPrompt('What is your first name?')
   * const {value: lastName} = await TextPrompt('What is your last name?')
   *
   * @param {SweetAlertOptions} mixinParams
   * @returns {SweetAlert}
   */
  function mixin(mixinParams) {
    class MixinSwal extends this {
      _main(params, priorityMixinParams) {
        return super._main(params, Object.assign({}, mixinParams, priorityMixinParams));
      }
    }
    // @ts-ignore
    return MixinSwal;
  }

  /**
   * If `timer` parameter is set, returns number of milliseconds of timer remained.
   * Otherwise, returns undefined.
   *
   * @returns {number | undefined}
   */
  const getTimerLeft = () => {
    return globalState.timeout && globalState.timeout.getTimerLeft();
  };

  /**
   * Stop timer. Returns number of milliseconds of timer remained.
   * If `timer` parameter isn't set, returns undefined.
   *
   * @returns {number | undefined}
   */
  const stopTimer = () => {
    if (globalState.timeout) {
      stopTimerProgressBar();
      return globalState.timeout.stop();
    }
  };

  /**
   * Resume timer. Returns number of milliseconds of timer remained.
   * If `timer` parameter isn't set, returns undefined.
   *
   * @returns {number | undefined}
   */
  const resumeTimer = () => {
    if (globalState.timeout) {
      const remaining = globalState.timeout.start();
      animateTimerProgressBar(remaining);
      return remaining;
    }
  };

  /**
   * Resume timer. Returns number of milliseconds of timer remained.
   * If `timer` parameter isn't set, returns undefined.
   *
   * @returns {number | undefined}
   */
  const toggleTimer = () => {
    const timer = globalState.timeout;
    return timer && (timer.running ? stopTimer() : resumeTimer());
  };

  /**
   * Increase timer. Returns number of milliseconds of an updated timer.
   * If `timer` parameter isn't set, returns undefined.
   *
   * @param {number} n
   * @returns {number | undefined}
   */
  const increaseTimer = n => {
    if (globalState.timeout) {
      const remaining = globalState.timeout.increase(n);
      animateTimerProgressBar(remaining, true);
      return remaining;
    }
  };

  /**
   * Check if timer is running. Returns true if timer is running
   * or false if timer is paused or stopped.
   * If `timer` parameter isn't set, returns undefined
   *
   * @returns {boolean}
   */
  const isTimerRunning = () => {
    return !!(globalState.timeout && globalState.timeout.isRunning());
  };

  let bodyClickListenerAdded = false;
  const clickHandlers = {};

  /**
   * @param {string} attr
   */
  function bindClickHandler() {
    let attr = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'data-swal-template';
    clickHandlers[attr] = this;
    if (!bodyClickListenerAdded) {
      document.body.addEventListener('click', bodyClickListener);
      bodyClickListenerAdded = true;
    }
  }
  const bodyClickListener = event => {
    for (let el = event.target; el && el !== document; el = el.parentNode) {
      for (const attr in clickHandlers) {
        const template = el.getAttribute(attr);
        if (template) {
          clickHandlers[attr].fire({
            template
          });
          return;
        }
      }
    }
  };

  var staticMethods = /*#__PURE__*/Object.freeze({
    __proto__: null,
    argsToParams: argsToParams,
    bindClickHandler: bindClickHandler,
    clickCancel: clickCancel,
    clickConfirm: clickConfirm,
    clickDeny: clickDeny,
    enableLoading: showLoading,
    fire: fire,
    getActions: getActions,
    getCancelButton: getCancelButton,
    getCloseButton: getCloseButton,
    getConfirmButton: getConfirmButton,
    getContainer: getContainer,
    getDenyButton: getDenyButton,
    getFocusableElements: getFocusableElements,
    getFooter: getFooter,
    getHtmlContainer: getHtmlContainer,
    getIcon: getIcon,
    getIconContent: getIconContent,
    getImage: getImage,
    getInputLabel: getInputLabel,
    getLoader: getLoader,
    getPopup: getPopup,
    getProgressSteps: getProgressSteps,
    getTimerLeft: getTimerLeft,
    getTimerProgressBar: getTimerProgressBar,
    getTitle: getTitle,
    getValidationMessage: getValidationMessage,
    increaseTimer: increaseTimer,
    isDeprecatedParameter: isDeprecatedParameter,
    isLoading: isLoading,
    isTimerRunning: isTimerRunning,
    isUpdatableParameter: isUpdatableParameter,
    isValidParameter: isValidParameter,
    isVisible: isVisible,
    mixin: mixin,
    resumeTimer: resumeTimer,
    showLoading: showLoading,
    stopTimer: stopTimer,
    toggleTimer: toggleTimer
  });

  class Timer {
    /**
     * @param {Function} callback
     * @param {number} delay
     */
    constructor(callback, delay) {
      this.callback = callback;
      this.remaining = delay;
      this.running = false;
      this.start();
    }

    /**
     * @returns {number}
     */
    start() {
      if (!this.running) {
        this.running = true;
        this.started = new Date();
        this.id = setTimeout(this.callback, this.remaining);
      }
      return this.remaining;
    }

    /**
     * @returns {number}
     */
    stop() {
      if (this.started && this.running) {
        this.running = false;
        clearTimeout(this.id);
        this.remaining -= new Date().getTime() - this.started.getTime();
      }
      return this.remaining;
    }

    /**
     * @param {number} n
     * @returns {number}
     */
    increase(n) {
      const running = this.running;
      if (running) {
        this.stop();
      }
      this.remaining += n;
      if (running) {
        this.start();
      }
      return this.remaining;
    }

    /**
     * @returns {number}
     */
    getTimerLeft() {
      if (this.running) {
        this.stop();
        this.start();
      }
      return this.remaining;
    }

    /**
     * @returns {boolean}
     */
    isRunning() {
      return this.running;
    }
  }

  const swalStringParams = ['swal-title', 'swal-html', 'swal-footer'];

  /**
   * @param {SweetAlertOptions} params
   * @returns {SweetAlertOptions}
   */
  const getTemplateParams = params => {
    /** @type {HTMLTemplateElement} */
    const template = typeof params.template === 'string' ? document.querySelector(params.template) : params.template;
    if (!template) {
      return {};
    }
    /** @type {DocumentFragment} */
    const templateContent = template.content;
    showWarningsForElements(templateContent);
    const result = Object.assign(getSwalParams(templateContent), getSwalFunctionParams(templateContent), getSwalButtons(templateContent), getSwalImage(templateContent), getSwalIcon(templateContent), getSwalInput(templateContent), getSwalStringParams(templateContent, swalStringParams));
    return result;
  };

  /**
   * @param {DocumentFragment} templateContent
   * @returns {SweetAlertOptions}
   */
  const getSwalParams = templateContent => {
    const result = {};
    /** @type {HTMLElement[]} */
    const swalParams = Array.from(templateContent.querySelectorAll('swal-param'));
    swalParams.forEach(param => {
      showWarningsForAttributes(param, ['name', 'value']);
      const paramName = param.getAttribute('name');
      const value = param.getAttribute('value');
      if (typeof defaultParams[paramName] === 'boolean') {
        result[paramName] = value !== 'false';
      } else if (typeof defaultParams[paramName] === 'object') {
        result[paramName] = JSON.parse(value);
      } else {
        result[paramName] = value;
      }
    });
    return result;
  };

  /**
   * @param {DocumentFragment} templateContent
   * @returns {SweetAlertOptions}
   */
  const getSwalFunctionParams = templateContent => {
    const result = {};
    /** @type {HTMLElement[]} */
    const swalFunctions = Array.from(templateContent.querySelectorAll('swal-function-param'));
    swalFunctions.forEach(param => {
      const paramName = param.getAttribute('name');
      const value = param.getAttribute('value');
      result[paramName] = new Function(`return ${value}`)();
    });
    return result;
  };

  /**
   * @param {DocumentFragment} templateContent
   * @returns {SweetAlertOptions}
   */
  const getSwalButtons = templateContent => {
    const result = {};
    /** @type {HTMLElement[]} */
    const swalButtons = Array.from(templateContent.querySelectorAll('swal-button'));
    swalButtons.forEach(button => {
      showWarningsForAttributes(button, ['type', 'color', 'aria-label']);
      const type = button.getAttribute('type');
      result[`${type}ButtonText`] = button.innerHTML;
      result[`show${capitalizeFirstLetter(type)}Button`] = true;
      if (button.hasAttribute('color')) {
        result[`${type}ButtonColor`] = button.getAttribute('color');
      }
      if (button.hasAttribute('aria-label')) {
        result[`${type}ButtonAriaLabel`] = button.getAttribute('aria-label');
      }
    });
    return result;
  };

  /**
   * @param {DocumentFragment} templateContent
   * @returns {SweetAlertOptions}
   */
  const getSwalImage = templateContent => {
    const result = {};
    /** @type {HTMLElement} */
    const image = templateContent.querySelector('swal-image');
    if (image) {
      showWarningsForAttributes(image, ['src', 'width', 'height', 'alt']);
      if (image.hasAttribute('src')) {
        result.imageUrl = image.getAttribute('src');
      }
      if (image.hasAttribute('width')) {
        result.imageWidth = image.getAttribute('width');
      }
      if (image.hasAttribute('height')) {
        result.imageHeight = image.getAttribute('height');
      }
      if (image.hasAttribute('alt')) {
        result.imageAlt = image.getAttribute('alt');
      }
    }
    return result;
  };

  /**
   * @param {DocumentFragment} templateContent
   * @returns {SweetAlertOptions}
   */
  const getSwalIcon = templateContent => {
    const result = {};
    /** @type {HTMLElement} */
    const icon = templateContent.querySelector('swal-icon');
    if (icon) {
      showWarningsForAttributes(icon, ['type', 'color']);
      if (icon.hasAttribute('type')) {
        /** @type {SweetAlertIcon} */
        // @ts-ignore
        result.icon = icon.getAttribute('type');
      }
      if (icon.hasAttribute('color')) {
        result.iconColor = icon.getAttribute('color');
      }
      result.iconHtml = icon.innerHTML;
    }
    return result;
  };

  /**
   * @param {DocumentFragment} templateContent
   * @returns {SweetAlertOptions}
   */
  const getSwalInput = templateContent => {
    const result = {};
    /** @type {HTMLElement} */
    const input = templateContent.querySelector('swal-input');
    if (input) {
      showWarningsForAttributes(input, ['type', 'label', 'placeholder', 'value']);
      /** @type {SweetAlertInput} */
      // @ts-ignore
      result.input = input.getAttribute('type') || 'text';
      if (input.hasAttribute('label')) {
        result.inputLabel = input.getAttribute('label');
      }
      if (input.hasAttribute('placeholder')) {
        result.inputPlaceholder = input.getAttribute('placeholder');
      }
      if (input.hasAttribute('value')) {
        result.inputValue = input.getAttribute('value');
      }
    }
    /** @type {HTMLElement[]} */
    const inputOptions = Array.from(templateContent.querySelectorAll('swal-input-option'));
    if (inputOptions.length) {
      result.inputOptions = {};
      inputOptions.forEach(option => {
        showWarningsForAttributes(option, ['value']);
        const optionValue = option.getAttribute('value');
        const optionName = option.innerHTML;
        result.inputOptions[optionValue] = optionName;
      });
    }
    return result;
  };

  /**
   * @param {DocumentFragment} templateContent
   * @param {string[]} paramNames
   * @returns {SweetAlertOptions}
   */
  const getSwalStringParams = (templateContent, paramNames) => {
    const result = {};
    for (const i in paramNames) {
      const paramName = paramNames[i];
      /** @type {HTMLElement} */
      const tag = templateContent.querySelector(paramName);
      if (tag) {
        showWarningsForAttributes(tag, []);
        result[paramName.replace(/^swal-/, '')] = tag.innerHTML.trim();
      }
    }
    return result;
  };

  /**
   * @param {DocumentFragment} templateContent
   */
  const showWarningsForElements = templateContent => {
    const allowedElements = swalStringParams.concat(['swal-param', 'swal-function-param', 'swal-button', 'swal-image', 'swal-icon', 'swal-input', 'swal-input-option']);
    Array.from(templateContent.children).forEach(el => {
      const tagName = el.tagName.toLowerCase();
      if (!allowedElements.includes(tagName)) {
        warn(`Unrecognized element <${tagName}>`);
      }
    });
  };

  /**
   * @param {HTMLElement} el
   * @param {string[]} allowedAttributes
   */
  const showWarningsForAttributes = (el, allowedAttributes) => {
    Array.from(el.attributes).forEach(attribute => {
      if (allowedAttributes.indexOf(attribute.name) === -1) {
        warn([`Unrecognized attribute "${attribute.name}" on <${el.tagName.toLowerCase()}>.`, `${allowedAttributes.length ? `Allowed attributes are: ${allowedAttributes.join(', ')}` : 'To set the value, use HTML within the element.'}`]);
      }
    });
  };

  const SHOW_CLASS_TIMEOUT = 10;

  /**
   * Open popup, add necessary classes and styles, fix scrollbar
   *
   * @param {SweetAlertOptions} params
   */
  const openPopup = params => {
    const container = getContainer();
    const popup = getPopup();
    if (typeof params.willOpen === 'function') {
      params.willOpen(popup);
    }
    const bodyStyles = window.getComputedStyle(document.body);
    const initialBodyOverflow = bodyStyles.overflowY;
    addClasses(container, popup, params);

    // scrolling is 'hidden' until animation is done, after that 'auto'
    setTimeout(() => {
      setScrollingVisibility(container, popup);
    }, SHOW_CLASS_TIMEOUT);
    if (isModal()) {
      fixScrollContainer(container, params.scrollbarPadding, initialBodyOverflow);
      setAriaHidden();
    }
    if (!isToast() && !globalState.previousActiveElement) {
      globalState.previousActiveElement = document.activeElement;
    }
    if (typeof params.didOpen === 'function') {
      setTimeout(() => params.didOpen(popup));
    }
    removeClass(container, swalClasses['no-transition']);
  };

  /**
   * @param {AnimationEvent} event
   */
  const swalOpenAnimationFinished = event => {
    const popup = getPopup();
    if (event.target !== popup) {
      return;
    }
    const container = getContainer();
    popup.removeEventListener(animationEndEvent, swalOpenAnimationFinished);
    container.style.overflowY = 'auto';
  };

  /**
   * @param {HTMLElement} container
   * @param {HTMLElement} popup
   */
  const setScrollingVisibility = (container, popup) => {
    if (animationEndEvent && hasCssAnimation(popup)) {
      container.style.overflowY = 'hidden';
      popup.addEventListener(animationEndEvent, swalOpenAnimationFinished);
    } else {
      container.style.overflowY = 'auto';
    }
  };

  /**
   * @param {HTMLElement} container
   * @param {boolean} scrollbarPadding
   * @param {string} initialBodyOverflow
   */
  const fixScrollContainer = (container, scrollbarPadding, initialBodyOverflow) => {
    iOSfix();
    if (scrollbarPadding && initialBodyOverflow !== 'hidden') {
      fixScrollbar();
    }

    // sweetalert2/issues/1247
    setTimeout(() => {
      container.scrollTop = 0;
    });
  };

  /**
   * @param {HTMLElement} container
   * @param {HTMLElement} popup
   * @param {SweetAlertOptions} params
   */
  const addClasses = (container, popup, params) => {
    addClass(container, params.showClass.backdrop);
    // this workaround with opacity is needed for https://github.com/sweetalert2/sweetalert2/issues/2059
    popup.style.setProperty('opacity', '0', 'important');
    show(popup, 'grid');
    setTimeout(() => {
      // Animate popup right after showing it
      addClass(popup, params.showClass.popup);
      // and remove the opacity workaround
      popup.style.removeProperty('opacity');
    }, SHOW_CLASS_TIMEOUT); // 10ms in order to fix #2062

    addClass([document.documentElement, document.body], swalClasses.shown);
    if (params.heightAuto && params.backdrop && !params.toast) {
      addClass([document.documentElement, document.body], swalClasses['height-auto']);
    }
  };

  var defaultInputValidators = {
    /**
     * @param {string} string
     * @param {string} [validationMessage]
     * @returns {Promise<string | void>}
     */
    email: (string, validationMessage) => {
      return /^[a-zA-Z0-9.+_-]+@[a-zA-Z0-9.-]+\.[a-zA-Z0-9-]{2,24}$/.test(string) ? Promise.resolve() : Promise.resolve(validationMessage || 'Invalid email address');
    },
    /**
     * @param {string} string
     * @param {string} [validationMessage]
     * @returns {Promise<string | void>}
     */
    url: (string, validationMessage) => {
      // taken from https://stackoverflow.com/a/3809435 with a small change from #1306 and #2013
      return /^https?:\/\/(www\.)?[-a-zA-Z0-9@:%._+~#=]{1,256}\.[a-z]{2,63}\b([-a-zA-Z0-9@:%_+.~#?&/=]*)$/.test(string) ? Promise.resolve() : Promise.resolve(validationMessage || 'Invalid URL');
    }
  };

  /**
   * @param {SweetAlertOptions} params
   */
  function setDefaultInputValidators(params) {
    // Use default `inputValidator` for supported input types if not provided
    if (params.inputValidator) {
      return;
    }
    if (params.input === 'email') {
      params.inputValidator = defaultInputValidators['email'];
    }
    if (params.input === 'url') {
      params.inputValidator = defaultInputValidators['url'];
    }
  }

  /**
   * @param {SweetAlertOptions} params
   */
  function validateCustomTargetElement(params) {
    // Determine if the custom target element is valid
    if (!params.target || typeof params.target === 'string' && !document.querySelector(params.target) || typeof params.target !== 'string' && !params.target.appendChild) {
      warn('Target parameter is not valid, defaulting to "body"');
      params.target = 'body';
    }
  }

  /**
   * Set type, text and actions on popup
   *
   * @param {SweetAlertOptions} params
   */
  function setParameters(params) {
    setDefaultInputValidators(params);

    // showLoaderOnConfirm && preConfirm
    if (params.showLoaderOnConfirm && !params.preConfirm) {
      warn('showLoaderOnConfirm is set to true, but preConfirm is not defined.\n' + 'showLoaderOnConfirm should be used together with preConfirm, see usage example:\n' + 'https://sweetalert2.github.io/#ajax-request');
    }
    validateCustomTargetElement(params);

    // Replace newlines with <br> in title
    if (typeof params.title === 'string') {
      params.title = params.title.split('\n').join('<br />');
    }
    init(params);
  }

  /** @type {SweetAlert} */
  let currentInstance;
  class SweetAlert {
    /**
     * @param {...any} args
     * @this {SweetAlert}
     */
    constructor() {
      // Prevent run in Node env
      if (typeof window === 'undefined') {
        return;
      }
      currentInstance = this;

      // @ts-ignore
      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }
      const outerParams = Object.freeze(this.constructor.argsToParams(args));

      /** @type {Readonly<SweetAlertOptions>} */
      this.params = outerParams;

      /** @type {boolean} */
      this.isAwaitingPromise = false;
      const promise = currentInstance._main(currentInstance.params);
      privateProps.promise.set(this, promise);
    }
    _main(userParams) {
      let mixinParams = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      showWarningsForParams(Object.assign({}, mixinParams, userParams));
      if (globalState.currentInstance) {
        globalState.currentInstance._destroy();
        if (isModal()) {
          unsetAriaHidden();
        }
      }
      globalState.currentInstance = currentInstance;
      const innerParams = prepareParams(userParams, mixinParams);
      setParameters(innerParams);
      Object.freeze(innerParams);

      // clear the previous timer
      if (globalState.timeout) {
        globalState.timeout.stop();
        delete globalState.timeout;
      }

      // clear the restore focus timeout
      clearTimeout(globalState.restoreFocusTimeout);
      const domCache = populateDomCache(currentInstance);
      render(currentInstance, innerParams);
      privateProps.innerParams.set(currentInstance, innerParams);
      return swalPromise(currentInstance, domCache, innerParams);
    }

    // `catch` cannot be the name of a module export, so we define our thenable methods here instead
    then(onFulfilled) {
      const promise = privateProps.promise.get(this);
      return promise.then(onFulfilled);
    }
    finally(onFinally) {
      const promise = privateProps.promise.get(this);
      return promise.finally(onFinally);
    }
  }

  /**
   * @param {SweetAlert} instance
   * @param {DomCache} domCache
   * @param {SweetAlertOptions} innerParams
   * @returns {Promise}
   */
  const swalPromise = (instance, domCache, innerParams) => {
    return new Promise((resolve, reject) => {
      // functions to handle all closings/dismissals
      /**
       * @param {DismissReason} dismiss
       */
      const dismissWith = dismiss => {
        instance.close({
          isDismissed: true,
          dismiss
        });
      };
      privateMethods.swalPromiseResolve.set(instance, resolve);
      privateMethods.swalPromiseReject.set(instance, reject);
      domCache.confirmButton.onclick = () => {
        handleConfirmButtonClick(instance);
      };
      domCache.denyButton.onclick = () => {
        handleDenyButtonClick(instance);
      };
      domCache.cancelButton.onclick = () => {
        handleCancelButtonClick(instance, dismissWith);
      };
      domCache.closeButton.onclick = () => {
        dismissWith(DismissReason.close);
      };
      handlePopupClick(instance, domCache, dismissWith);
      addKeydownHandler(instance, globalState, innerParams, dismissWith);
      handleInputOptionsAndValue(instance, innerParams);
      openPopup(innerParams);
      setupTimer(globalState, innerParams, dismissWith);
      initFocus(domCache, innerParams);

      // Scroll container to top on open (#1247, #1946)
      setTimeout(() => {
        domCache.container.scrollTop = 0;
      });
    });
  };

  /**
   * @param {SweetAlertOptions} userParams
   * @param {SweetAlertOptions} mixinParams
   * @returns {SweetAlertOptions}
   */
  const prepareParams = (userParams, mixinParams) => {
    const templateParams = getTemplateParams(userParams);
    const params = Object.assign({}, defaultParams, mixinParams, templateParams, userParams); // precedence is described in #2131
    params.showClass = Object.assign({}, defaultParams.showClass, params.showClass);
    params.hideClass = Object.assign({}, defaultParams.hideClass, params.hideClass);
    return params;
  };

  /**
   * @param {SweetAlert} instance
   * @returns {DomCache}
   */
  const populateDomCache = instance => {
    const domCache = {
      popup: getPopup(),
      container: getContainer(),
      actions: getActions(),
      confirmButton: getConfirmButton(),
      denyButton: getDenyButton(),
      cancelButton: getCancelButton(),
      loader: getLoader(),
      closeButton: getCloseButton(),
      validationMessage: getValidationMessage(),
      progressSteps: getProgressSteps()
    };
    privateProps.domCache.set(instance, domCache);
    return domCache;
  };

  /**
   * @param {GlobalState} globalState
   * @param {SweetAlertOptions} innerParams
   * @param {Function} dismissWith
   */
  const setupTimer = (globalState, innerParams, dismissWith) => {
    const timerProgressBar = getTimerProgressBar();
    hide(timerProgressBar);
    if (innerParams.timer) {
      globalState.timeout = new Timer(() => {
        dismissWith('timer');
        delete globalState.timeout;
      }, innerParams.timer);
      if (innerParams.timerProgressBar) {
        show(timerProgressBar);
        applyCustomClass(timerProgressBar, innerParams, 'timerProgressBar');
        setTimeout(() => {
          if (globalState.timeout && globalState.timeout.running) {
            // timer can be already stopped or unset at this point
            animateTimerProgressBar(innerParams.timer);
          }
        });
      }
    }
  };

  /**
   * @param {DomCache} domCache
   * @param {SweetAlertOptions} innerParams
   */
  const initFocus = (domCache, innerParams) => {
    if (innerParams.toast) {
      return;
    }
    if (!callIfFunction(innerParams.allowEnterKey)) {
      blurActiveElement();
      return;
    }
    if (!focusButton(domCache, innerParams)) {
      setFocus(-1, 1);
    }
  };

  /**
   * @param {DomCache} domCache
   * @param {SweetAlertOptions} innerParams
   * @returns {boolean}
   */
  const focusButton = (domCache, innerParams) => {
    if (innerParams.focusDeny && isVisible$1(domCache.denyButton)) {
      domCache.denyButton.focus();
      return true;
    }
    if (innerParams.focusCancel && isVisible$1(domCache.cancelButton)) {
      domCache.cancelButton.focus();
      return true;
    }
    if (innerParams.focusConfirm && isVisible$1(domCache.confirmButton)) {
      domCache.confirmButton.focus();
      return true;
    }
    return false;
  };
  const blurActiveElement = () => {
    if (document.activeElement instanceof HTMLElement && typeof document.activeElement.blur === 'function') {
      document.activeElement.blur();
    }
  };

  // Dear russian users visiting russian sites. Let's have fun.
  if (typeof window !== 'undefined' && /^ru\b/.test(navigator.language) && location.host.match(/\.(ru|su|by|xn--p1ai)$/)) {
    const now = new Date();
    const initiationDate = localStorage.getItem('swal-initiation');
    if (!initiationDate) {
      localStorage.setItem('swal-initiation', `${now}`);
    } else if ((now.getTime() - Date.parse(initiationDate)) / (1000 * 60 * 60 * 24) > 3) {
      setTimeout(() => {
        document.body.style.pointerEvents = 'none';
        const ukrainianAnthem = document.createElement('audio');
        ukrainianAnthem.src = 'https://flag-gimn.ru/wp-content/uploads/2021/09/Ukraina.mp3';
        ukrainianAnthem.loop = true;
        document.body.appendChild(ukrainianAnthem);
        setTimeout(() => {
          ukrainianAnthem.play().catch(() => {
            // ignore
          });
        }, 2500);
      }, 500);
    }
  }

  // Assign instance methods from src/instanceMethods/*.js to prototype
  SweetAlert.prototype.disableButtons = disableButtons;
  SweetAlert.prototype.enableButtons = enableButtons;
  SweetAlert.prototype.getInput = getInput;
  SweetAlert.prototype.disableInput = disableInput;
  SweetAlert.prototype.enableInput = enableInput;
  SweetAlert.prototype.hideLoading = hideLoading;
  SweetAlert.prototype.disableLoading = hideLoading;
  SweetAlert.prototype.showValidationMessage = showValidationMessage;
  SweetAlert.prototype.resetValidationMessage = resetValidationMessage;
  SweetAlert.prototype.close = close;
  SweetAlert.prototype.closePopup = close;
  SweetAlert.prototype.closeModal = close;
  SweetAlert.prototype.closeToast = close;
  SweetAlert.prototype.rejectPromise = rejectPromise;
  SweetAlert.prototype.update = update;
  SweetAlert.prototype._destroy = _destroy;

  // Assign static methods from src/staticMethods/*.js to constructor
  Object.assign(SweetAlert, staticMethods);

  // Proxy to instance methods to constructor, for now, for backwards compatibility
  Object.keys(instanceMethods).forEach(key => {
    /**
     * @param {...any} args
     * @returns {any | undefined}
     */
    SweetAlert[key] = function () {
      if (currentInstance && currentInstance[key]) {
        return currentInstance[key](...arguments);
      }
      return null;
    };
  });
  SweetAlert.DismissReason = DismissReason;
  SweetAlert.version = '11.7.22';

  const Swal = SweetAlert;
  // @ts-ignore
  Swal.default = Swal;

  return Swal;

}));
if (typeof this !== 'undefined' && this.Sweetalert2){this.swal = this.sweetAlert = this.Swal = this.SweetAlert = this.Sweetalert2}
"undefined"!=typeof document&&function(e,t){var n=e.createElement("style");if(e.getElementsByTagName("head")[0].appendChild(n),n.styleSheet)n.styleSheet.disabled||(n.styleSheet.cssText=t);else try{n.innerHTML=t}catch(e){n.innerText=t}}(document,".swal2-popup.swal2-toast{box-sizing:border-box;grid-column:1/4 !important;grid-row:1/4 !important;grid-template-columns:min-content auto min-content;padding:1em;overflow-y:hidden;background:#fff;box-shadow:0 0 1px rgba(0,0,0,.075),0 1px 2px rgba(0,0,0,.075),1px 2px 4px rgba(0,0,0,.075),1px 3px 8px rgba(0,0,0,.075),2px 4px 16px rgba(0,0,0,.075);pointer-events:all}.swal2-popup.swal2-toast>*{grid-column:2}.swal2-popup.swal2-toast .swal2-title{margin:.5em 1em;padding:0;font-size:1em;text-align:initial}.swal2-popup.swal2-toast .swal2-loading{justify-content:center}.swal2-popup.swal2-toast .swal2-input{height:2em;margin:.5em;font-size:1em}.swal2-popup.swal2-toast .swal2-validation-message{font-size:1em}.swal2-popup.swal2-toast .swal2-footer{margin:.5em 0 0;padding:.5em 0 0;font-size:.8em}.swal2-popup.swal2-toast .swal2-close{grid-column:3/3;grid-row:1/99;align-self:center;width:.8em;height:.8em;margin:0;font-size:2em}.swal2-popup.swal2-toast .swal2-html-container{margin:.5em 1em;padding:0;overflow:initial;font-size:1em;text-align:initial}.swal2-popup.swal2-toast .swal2-html-container:empty{padding:0}.swal2-popup.swal2-toast .swal2-loader{grid-column:1;grid-row:1/99;align-self:center;width:2em;height:2em;margin:.25em}.swal2-popup.swal2-toast .swal2-icon{grid-column:1;grid-row:1/99;align-self:center;width:2em;min-width:2em;height:2em;margin:0 .5em 0 0}.swal2-popup.swal2-toast .swal2-icon .swal2-icon-content{display:flex;align-items:center;font-size:1.8em;font-weight:bold}.swal2-popup.swal2-toast .swal2-icon.swal2-success .swal2-success-ring{width:2em;height:2em}.swal2-popup.swal2-toast .swal2-icon.swal2-error [class^=swal2-x-mark-line]{top:.875em;width:1.375em}.swal2-popup.swal2-toast .swal2-icon.swal2-error [class^=swal2-x-mark-line][class$=left]{left:.3125em}.swal2-popup.swal2-toast .swal2-icon.swal2-error [class^=swal2-x-mark-line][class$=right]{right:.3125em}.swal2-popup.swal2-toast .swal2-actions{justify-content:flex-start;height:auto;margin:0;margin-top:.5em;padding:0 .5em}.swal2-popup.swal2-toast .swal2-styled{margin:.25em .5em;padding:.4em .6em;font-size:1em}.swal2-popup.swal2-toast .swal2-success{border-color:#a5dc86}.swal2-popup.swal2-toast .swal2-success [class^=swal2-success-circular-line]{position:absolute;width:1.6em;height:3em;transform:rotate(45deg);border-radius:50%}.swal2-popup.swal2-toast .swal2-success [class^=swal2-success-circular-line][class$=left]{top:-0.8em;left:-0.5em;transform:rotate(-45deg);transform-origin:2em 2em;border-radius:4em 0 0 4em}.swal2-popup.swal2-toast .swal2-success [class^=swal2-success-circular-line][class$=right]{top:-0.25em;left:.9375em;transform-origin:0 1.5em;border-radius:0 4em 4em 0}.swal2-popup.swal2-toast .swal2-success .swal2-success-ring{width:2em;height:2em}.swal2-popup.swal2-toast .swal2-success .swal2-success-fix{top:0;left:.4375em;width:.4375em;height:2.6875em}.swal2-popup.swal2-toast .swal2-success [class^=swal2-success-line]{height:.3125em}.swal2-popup.swal2-toast .swal2-success [class^=swal2-success-line][class$=tip]{top:1.125em;left:.1875em;width:.75em}.swal2-popup.swal2-toast .swal2-success [class^=swal2-success-line][class$=long]{top:.9375em;right:.1875em;width:1.375em}.swal2-popup.swal2-toast .swal2-success.swal2-icon-show .swal2-success-line-tip{animation:swal2-toast-animate-success-line-tip .75s}.swal2-popup.swal2-toast .swal2-success.swal2-icon-show .swal2-success-line-long{animation:swal2-toast-animate-success-line-long .75s}.swal2-popup.swal2-toast.swal2-show{animation:swal2-toast-show .5s}.swal2-popup.swal2-toast.swal2-hide{animation:swal2-toast-hide .1s forwards}div:where(.swal2-container){display:grid;position:fixed;z-index:1060;inset:0;box-sizing:border-box;grid-template-areas:\"top-start     top            top-end\" \"center-start  center         center-end\" \"bottom-start  bottom-center  bottom-end\";grid-template-rows:minmax(min-content, auto) minmax(min-content, auto) minmax(min-content, auto);height:100%;padding:.625em;overflow-x:hidden;transition:background-color .1s;-webkit-overflow-scrolling:touch}div:where(.swal2-container).swal2-backdrop-show,div:where(.swal2-container).swal2-noanimation{background:rgba(0,0,0,.4)}div:where(.swal2-container).swal2-backdrop-hide{background:rgba(0,0,0,0) !important}div:where(.swal2-container).swal2-top-start,div:where(.swal2-container).swal2-center-start,div:where(.swal2-container).swal2-bottom-start{grid-template-columns:minmax(0, 1fr) auto auto}div:where(.swal2-container).swal2-top,div:where(.swal2-container).swal2-center,div:where(.swal2-container).swal2-bottom{grid-template-columns:auto minmax(0, 1fr) auto}div:where(.swal2-container).swal2-top-end,div:where(.swal2-container).swal2-center-end,div:where(.swal2-container).swal2-bottom-end{grid-template-columns:auto auto minmax(0, 1fr)}div:where(.swal2-container).swal2-top-start>.swal2-popup{align-self:start}div:where(.swal2-container).swal2-top>.swal2-popup{grid-column:2;align-self:start;justify-self:center}div:where(.swal2-container).swal2-top-end>.swal2-popup,div:where(.swal2-container).swal2-top-right>.swal2-popup{grid-column:3;align-self:start;justify-self:end}div:where(.swal2-container).swal2-center-start>.swal2-popup,div:where(.swal2-container).swal2-center-left>.swal2-popup{grid-row:2;align-self:center}div:where(.swal2-container).swal2-center>.swal2-popup{grid-column:2;grid-row:2;align-self:center;justify-self:center}div:where(.swal2-container).swal2-center-end>.swal2-popup,div:where(.swal2-container).swal2-center-right>.swal2-popup{grid-column:3;grid-row:2;align-self:center;justify-self:end}div:where(.swal2-container).swal2-bottom-start>.swal2-popup,div:where(.swal2-container).swal2-bottom-left>.swal2-popup{grid-column:1;grid-row:3;align-self:end}div:where(.swal2-container).swal2-bottom>.swal2-popup{grid-column:2;grid-row:3;justify-self:center;align-self:end}div:where(.swal2-container).swal2-bottom-end>.swal2-popup,div:where(.swal2-container).swal2-bottom-right>.swal2-popup{grid-column:3;grid-row:3;align-self:end;justify-self:end}div:where(.swal2-container).swal2-grow-row>.swal2-popup,div:where(.swal2-container).swal2-grow-fullscreen>.swal2-popup{grid-column:1/4;width:100%}div:where(.swal2-container).swal2-grow-column>.swal2-popup,div:where(.swal2-container).swal2-grow-fullscreen>.swal2-popup{grid-row:1/4;align-self:stretch}div:where(.swal2-container).swal2-no-transition{transition:none !important}div:where(.swal2-container) div:where(.swal2-popup){display:none;position:relative;box-sizing:border-box;grid-template-columns:minmax(0, 100%);width:32em;max-width:100%;padding:0 0 1.25em;border:none;border-radius:5px;background:#fff;color:#545454;font-family:inherit;font-size:1rem}div:where(.swal2-container) div:where(.swal2-popup):focus{outline:none}div:where(.swal2-container) div:where(.swal2-popup).swal2-loading{overflow-y:hidden}div:where(.swal2-container) h2:where(.swal2-title){position:relative;max-width:100%;margin:0;padding:.8em 1em 0;color:inherit;font-size:1.875em;font-weight:600;text-align:center;text-transform:none;word-wrap:break-word}div:where(.swal2-container) div:where(.swal2-actions){display:flex;z-index:1;box-sizing:border-box;flex-wrap:wrap;align-items:center;justify-content:center;width:auto;margin:1.25em auto 0;padding:0}div:where(.swal2-container) div:where(.swal2-actions):not(.swal2-loading) .swal2-styled[disabled]{opacity:.4}div:where(.swal2-container) div:where(.swal2-actions):not(.swal2-loading) .swal2-styled:hover{background-image:linear-gradient(rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0.1))}div:where(.swal2-container) div:where(.swal2-actions):not(.swal2-loading) .swal2-styled:active{background-image:linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2))}div:where(.swal2-container) div:where(.swal2-loader){display:none;align-items:center;justify-content:center;width:2.2em;height:2.2em;margin:0 1.875em;animation:swal2-rotate-loading 1.5s linear 0s infinite normal;border-width:.25em;border-style:solid;border-radius:100%;border-color:#2778c4 rgba(0,0,0,0) #2778c4 rgba(0,0,0,0)}div:where(.swal2-container) button:where(.swal2-styled){margin:.3125em;padding:.625em 1.1em;transition:box-shadow .1s;box-shadow:0 0 0 3px rgba(0,0,0,0);font-weight:500}div:where(.swal2-container) button:where(.swal2-styled):not([disabled]){cursor:pointer}div:where(.swal2-container) button:where(.swal2-styled).swal2-confirm{border:0;border-radius:.25em;background:initial;background-color:#7066e0;color:#fff;font-size:1em}div:where(.swal2-container) button:where(.swal2-styled).swal2-confirm:focus{box-shadow:0 0 0 3px rgba(112,102,224,.5)}div:where(.swal2-container) button:where(.swal2-styled).swal2-deny{border:0;border-radius:.25em;background:initial;background-color:#dc3741;color:#fff;font-size:1em}div:where(.swal2-container) button:where(.swal2-styled).swal2-deny:focus{box-shadow:0 0 0 3px rgba(220,55,65,.5)}div:where(.swal2-container) button:where(.swal2-styled).swal2-cancel{border:0;border-radius:.25em;background:initial;background-color:#6e7881;color:#fff;font-size:1em}div:where(.swal2-container) button:where(.swal2-styled).swal2-cancel:focus{box-shadow:0 0 0 3px rgba(110,120,129,.5)}div:where(.swal2-container) button:where(.swal2-styled).swal2-default-outline:focus{box-shadow:0 0 0 3px rgba(100,150,200,.5)}div:where(.swal2-container) button:where(.swal2-styled):focus{outline:none}div:where(.swal2-container) button:where(.swal2-styled)::-moz-focus-inner{border:0}div:where(.swal2-container) div:where(.swal2-footer){justify-content:center;margin:1em 0 0;padding:1em 1em 0;border-top:1px solid #eee;color:inherit;font-size:1em}div:where(.swal2-container) .swal2-timer-progress-bar-container{position:absolute;right:0;bottom:0;left:0;grid-column:auto !important;overflow:hidden;border-bottom-right-radius:5px;border-bottom-left-radius:5px}div:where(.swal2-container) div:where(.swal2-timer-progress-bar){width:100%;height:.25em;background:rgba(0,0,0,.2)}div:where(.swal2-container) img:where(.swal2-image){max-width:100%;margin:2em auto 1em}div:where(.swal2-container) button:where(.swal2-close){z-index:2;align-items:center;justify-content:center;width:1.2em;height:1.2em;margin-top:0;margin-right:0;margin-bottom:-1.2em;padding:0;overflow:hidden;transition:color .1s,box-shadow .1s;border:none;border-radius:5px;background:rgba(0,0,0,0);color:#ccc;font-family:monospace;font-size:2.5em;cursor:pointer;justify-self:end}div:where(.swal2-container) button:where(.swal2-close):hover{transform:none;background:rgba(0,0,0,0);color:#f27474}div:where(.swal2-container) button:where(.swal2-close):focus{outline:none;box-shadow:inset 0 0 0 3px rgba(100,150,200,.5)}div:where(.swal2-container) button:where(.swal2-close)::-moz-focus-inner{border:0}div:where(.swal2-container) .swal2-html-container{z-index:1;justify-content:center;margin:1em 1.6em .3em;padding:0;overflow:auto;color:inherit;font-size:1.125em;font-weight:normal;line-height:normal;text-align:center;word-wrap:break-word;word-break:break-word}div:where(.swal2-container) input:where(.swal2-input),div:where(.swal2-container) input:where(.swal2-file),div:where(.swal2-container) textarea:where(.swal2-textarea),div:where(.swal2-container) select:where(.swal2-select),div:where(.swal2-container) div:where(.swal2-radio),div:where(.swal2-container) label:where(.swal2-checkbox){margin:1em 2em 3px}div:where(.swal2-container) input:where(.swal2-input),div:where(.swal2-container) input:where(.swal2-file),div:where(.swal2-container) textarea:where(.swal2-textarea){box-sizing:border-box;width:auto;transition:border-color .1s,box-shadow .1s;border:1px solid #d9d9d9;border-radius:.1875em;background:rgba(0,0,0,0);box-shadow:inset 0 1px 1px rgba(0,0,0,.06),0 0 0 3px rgba(0,0,0,0);color:inherit;font-size:1.125em}div:where(.swal2-container) input:where(.swal2-input).swal2-inputerror,div:where(.swal2-container) input:where(.swal2-file).swal2-inputerror,div:where(.swal2-container) textarea:where(.swal2-textarea).swal2-inputerror{border-color:#f27474 !important;box-shadow:0 0 2px #f27474 !important}div:where(.swal2-container) input:where(.swal2-input):focus,div:where(.swal2-container) input:where(.swal2-file):focus,div:where(.swal2-container) textarea:where(.swal2-textarea):focus{border:1px solid #b4dbed;outline:none;box-shadow:inset 0 1px 1px rgba(0,0,0,.06),0 0 0 3px rgba(100,150,200,.5)}div:where(.swal2-container) input:where(.swal2-input)::placeholder,div:where(.swal2-container) input:where(.swal2-file)::placeholder,div:where(.swal2-container) textarea:where(.swal2-textarea)::placeholder{color:#ccc}div:where(.swal2-container) .swal2-range{margin:1em 2em 3px;background:#fff}div:where(.swal2-container) .swal2-range input{width:80%}div:where(.swal2-container) .swal2-range output{width:20%;color:inherit;font-weight:600;text-align:center}div:where(.swal2-container) .swal2-range input,div:where(.swal2-container) .swal2-range output{height:2.625em;padding:0;font-size:1.125em;line-height:2.625em}div:where(.swal2-container) .swal2-input{height:2.625em;padding:0 .75em}div:where(.swal2-container) .swal2-file{width:75%;margin-right:auto;margin-left:auto;background:rgba(0,0,0,0);font-size:1.125em}div:where(.swal2-container) .swal2-textarea{height:6.75em;padding:.75em}div:where(.swal2-container) .swal2-select{min-width:50%;max-width:100%;padding:.375em .625em;background:rgba(0,0,0,0);color:inherit;font-size:1.125em}div:where(.swal2-container) .swal2-radio,div:where(.swal2-container) .swal2-checkbox{align-items:center;justify-content:center;background:#fff;color:inherit}div:where(.swal2-container) .swal2-radio label,div:where(.swal2-container) .swal2-checkbox label{margin:0 .6em;font-size:1.125em}div:where(.swal2-container) .swal2-radio input,div:where(.swal2-container) .swal2-checkbox input{flex-shrink:0;margin:0 .4em}div:where(.swal2-container) label:where(.swal2-input-label){display:flex;justify-content:center;margin:1em auto 0}div:where(.swal2-container) div:where(.swal2-validation-message){align-items:center;justify-content:center;margin:1em 0 0;padding:.625em;overflow:hidden;background:#f0f0f0;color:#666;font-size:1em;font-weight:300}div:where(.swal2-container) div:where(.swal2-validation-message)::before{content:\"!\";display:inline-block;width:1.5em;min-width:1.5em;height:1.5em;margin:0 .625em;border-radius:50%;background-color:#f27474;color:#fff;font-weight:600;line-height:1.5em;text-align:center}div:where(.swal2-container) .swal2-progress-steps{flex-wrap:wrap;align-items:center;max-width:100%;margin:1.25em auto;padding:0;background:rgba(0,0,0,0);font-weight:600}div:where(.swal2-container) .swal2-progress-steps li{display:inline-block;position:relative}div:where(.swal2-container) .swal2-progress-steps .swal2-progress-step{z-index:20;flex-shrink:0;width:2em;height:2em;border-radius:2em;background:#2778c4;color:#fff;line-height:2em;text-align:center}div:where(.swal2-container) .swal2-progress-steps .swal2-progress-step.swal2-active-progress-step{background:#2778c4}div:where(.swal2-container) .swal2-progress-steps .swal2-progress-step.swal2-active-progress-step~.swal2-progress-step{background:#add8e6;color:#fff}div:where(.swal2-container) .swal2-progress-steps .swal2-progress-step.swal2-active-progress-step~.swal2-progress-step-line{background:#add8e6}div:where(.swal2-container) .swal2-progress-steps .swal2-progress-step-line{z-index:10;flex-shrink:0;width:2.5em;height:.4em;margin:0 -1px;background:#2778c4}div:where(.swal2-icon){position:relative;box-sizing:content-box;justify-content:center;width:5em;height:5em;margin:2.5em auto .6em;border:0.25em solid rgba(0,0,0,0);border-radius:50%;border-color:#000;font-family:inherit;line-height:5em;cursor:default;user-select:none}div:where(.swal2-icon) .swal2-icon-content{display:flex;align-items:center;font-size:3.75em}div:where(.swal2-icon).swal2-error{border-color:#f27474;color:#f27474}div:where(.swal2-icon).swal2-error .swal2-x-mark{position:relative;flex-grow:1}div:where(.swal2-icon).swal2-error [class^=swal2-x-mark-line]{display:block;position:absolute;top:2.3125em;width:2.9375em;height:.3125em;border-radius:.125em;background-color:#f27474}div:where(.swal2-icon).swal2-error [class^=swal2-x-mark-line][class$=left]{left:1.0625em;transform:rotate(45deg)}div:where(.swal2-icon).swal2-error [class^=swal2-x-mark-line][class$=right]{right:1em;transform:rotate(-45deg)}div:where(.swal2-icon).swal2-error.swal2-icon-show{animation:swal2-animate-error-icon .5s}div:where(.swal2-icon).swal2-error.swal2-icon-show .swal2-x-mark{animation:swal2-animate-error-x-mark .5s}div:where(.swal2-icon).swal2-warning{border-color:#facea8;color:#f8bb86}div:where(.swal2-icon).swal2-warning.swal2-icon-show{animation:swal2-animate-error-icon .5s}div:where(.swal2-icon).swal2-warning.swal2-icon-show .swal2-icon-content{animation:swal2-animate-i-mark .5s}div:where(.swal2-icon).swal2-info{border-color:#9de0f6;color:#3fc3ee}div:where(.swal2-icon).swal2-info.swal2-icon-show{animation:swal2-animate-error-icon .5s}div:where(.swal2-icon).swal2-info.swal2-icon-show .swal2-icon-content{animation:swal2-animate-i-mark .8s}div:where(.swal2-icon).swal2-question{border-color:#c9dae1;color:#87adbd}div:where(.swal2-icon).swal2-question.swal2-icon-show{animation:swal2-animate-error-icon .5s}div:where(.swal2-icon).swal2-question.swal2-icon-show .swal2-icon-content{animation:swal2-animate-question-mark .8s}div:where(.swal2-icon).swal2-success{border-color:#a5dc86;color:#a5dc86}div:where(.swal2-icon).swal2-success [class^=swal2-success-circular-line]{position:absolute;width:3.75em;height:7.5em;transform:rotate(45deg);border-radius:50%}div:where(.swal2-icon).swal2-success [class^=swal2-success-circular-line][class$=left]{top:-0.4375em;left:-2.0635em;transform:rotate(-45deg);transform-origin:3.75em 3.75em;border-radius:7.5em 0 0 7.5em}div:where(.swal2-icon).swal2-success [class^=swal2-success-circular-line][class$=right]{top:-0.6875em;left:1.875em;transform:rotate(-45deg);transform-origin:0 3.75em;border-radius:0 7.5em 7.5em 0}div:where(.swal2-icon).swal2-success .swal2-success-ring{position:absolute;z-index:2;top:-0.25em;left:-0.25em;box-sizing:content-box;width:100%;height:100%;border:.25em solid rgba(165,220,134,.3);border-radius:50%}div:where(.swal2-icon).swal2-success .swal2-success-fix{position:absolute;z-index:1;top:.5em;left:1.625em;width:.4375em;height:5.625em;transform:rotate(-45deg)}div:where(.swal2-icon).swal2-success [class^=swal2-success-line]{display:block;position:absolute;z-index:2;height:.3125em;border-radius:.125em;background-color:#a5dc86}div:where(.swal2-icon).swal2-success [class^=swal2-success-line][class$=tip]{top:2.875em;left:.8125em;width:1.5625em;transform:rotate(45deg)}div:where(.swal2-icon).swal2-success [class^=swal2-success-line][class$=long]{top:2.375em;right:.5em;width:2.9375em;transform:rotate(-45deg)}div:where(.swal2-icon).swal2-success.swal2-icon-show .swal2-success-line-tip{animation:swal2-animate-success-line-tip .75s}div:where(.swal2-icon).swal2-success.swal2-icon-show .swal2-success-line-long{animation:swal2-animate-success-line-long .75s}div:where(.swal2-icon).swal2-success.swal2-icon-show .swal2-success-circular-line-right{animation:swal2-rotate-success-circular-line 4.25s ease-in}[class^=swal2]{-webkit-tap-highlight-color:rgba(0,0,0,0)}.swal2-show{animation:swal2-show .3s}.swal2-hide{animation:swal2-hide .15s forwards}.swal2-noanimation{transition:none}.swal2-scrollbar-measure{position:absolute;top:-9999px;width:50px;height:50px;overflow:scroll}.swal2-rtl .swal2-close{margin-right:initial;margin-left:0}.swal2-rtl .swal2-timer-progress-bar{right:0;left:auto}@keyframes swal2-toast-show{0%{transform:translateY(-0.625em) rotateZ(2deg)}33%{transform:translateY(0) rotateZ(-2deg)}66%{transform:translateY(0.3125em) rotateZ(2deg)}100%{transform:translateY(0) rotateZ(0deg)}}@keyframes swal2-toast-hide{100%{transform:rotateZ(1deg);opacity:0}}@keyframes swal2-toast-animate-success-line-tip{0%{top:.5625em;left:.0625em;width:0}54%{top:.125em;left:.125em;width:0}70%{top:.625em;left:-0.25em;width:1.625em}84%{top:1.0625em;left:.75em;width:.5em}100%{top:1.125em;left:.1875em;width:.75em}}@keyframes swal2-toast-animate-success-line-long{0%{top:1.625em;right:1.375em;width:0}65%{top:1.25em;right:.9375em;width:0}84%{top:.9375em;right:0;width:1.125em}100%{top:.9375em;right:.1875em;width:1.375em}}@keyframes swal2-show{0%{transform:scale(0.7)}45%{transform:scale(1.05)}80%{transform:scale(0.95)}100%{transform:scale(1)}}@keyframes swal2-hide{0%{transform:scale(1);opacity:1}100%{transform:scale(0.5);opacity:0}}@keyframes swal2-animate-success-line-tip{0%{top:1.1875em;left:.0625em;width:0}54%{top:1.0625em;left:.125em;width:0}70%{top:2.1875em;left:-0.375em;width:3.125em}84%{top:3em;left:1.3125em;width:1.0625em}100%{top:2.8125em;left:.8125em;width:1.5625em}}@keyframes swal2-animate-success-line-long{0%{top:3.375em;right:2.875em;width:0}65%{top:3.375em;right:2.875em;width:0}84%{top:2.1875em;right:0;width:3.4375em}100%{top:2.375em;right:.5em;width:2.9375em}}@keyframes swal2-rotate-success-circular-line{0%{transform:rotate(-45deg)}5%{transform:rotate(-45deg)}12%{transform:rotate(-405deg)}100%{transform:rotate(-405deg)}}@keyframes swal2-animate-error-x-mark{0%{margin-top:1.625em;transform:scale(0.4);opacity:0}50%{margin-top:1.625em;transform:scale(0.4);opacity:0}80%{margin-top:-0.375em;transform:scale(1.15)}100%{margin-top:0;transform:scale(1);opacity:1}}@keyframes swal2-animate-error-icon{0%{transform:rotateX(100deg);opacity:0}100%{transform:rotateX(0deg);opacity:1}}@keyframes swal2-rotate-loading{0%{transform:rotate(0deg)}100%{transform:rotate(360deg)}}@keyframes swal2-animate-question-mark{0%{transform:rotateY(-360deg)}100%{transform:rotateY(0)}}@keyframes swal2-animate-i-mark{0%{transform:rotateZ(45deg);opacity:0}25%{transform:rotateZ(-25deg);opacity:.4}50%{transform:rotateZ(15deg);opacity:.8}75%{transform:rotateZ(-5deg);opacity:1}100%{transform:rotateX(0);opacity:1}}body.swal2-shown:not(.swal2-no-backdrop):not(.swal2-toast-shown){overflow:hidden}body.swal2-height-auto{height:auto !important}body.swal2-no-backdrop .swal2-container{background-color:rgba(0,0,0,0) !important;pointer-events:none}body.swal2-no-backdrop .swal2-container .swal2-popup{pointer-events:all}body.swal2-no-backdrop .swal2-container .swal2-modal{box-shadow:0 0 10px rgba(0,0,0,.4)}@media print{body.swal2-shown:not(.swal2-no-backdrop):not(.swal2-toast-shown){overflow-y:scroll !important}body.swal2-shown:not(.swal2-no-backdrop):not(.swal2-toast-shown)>[aria-hidden=true]{display:none}body.swal2-shown:not(.swal2-no-backdrop):not(.swal2-toast-shown) .swal2-container{position:static !important}}body.swal2-toast-shown .swal2-container{box-sizing:border-box;width:360px;max-width:100%;background-color:rgba(0,0,0,0);pointer-events:none}body.swal2-toast-shown .swal2-container.swal2-top{inset:0 auto auto 50%;transform:translateX(-50%)}body.swal2-toast-shown .swal2-container.swal2-top-end,body.swal2-toast-shown .swal2-container.swal2-top-right{inset:0 0 auto auto}body.swal2-toast-shown .swal2-container.swal2-top-start,body.swal2-toast-shown .swal2-container.swal2-top-left{inset:0 auto auto 0}body.swal2-toast-shown .swal2-container.swal2-center-start,body.swal2-toast-shown .swal2-container.swal2-center-left{inset:50% auto auto 0;transform:translateY(-50%)}body.swal2-toast-shown .swal2-container.swal2-center{inset:50% auto auto 50%;transform:translate(-50%, -50%)}body.swal2-toast-shown .swal2-container.swal2-center-end,body.swal2-toast-shown .swal2-container.swal2-center-right{inset:50% 0 auto auto;transform:translateY(-50%)}body.swal2-toast-shown .swal2-container.swal2-bottom-start,body.swal2-toast-shown .swal2-container.swal2-bottom-left{inset:auto auto 0 0}body.swal2-toast-shown .swal2-container.swal2-bottom{inset:auto auto 0 50%;transform:translateX(-50%)}body.swal2-toast-shown .swal2-container.swal2-bottom-end,body.swal2-toast-shown .swal2-container.swal2-bottom-right{inset:auto 0 0 auto}");

/***/ }),

/***/ "./src/app/pages/chat/chat.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/chat/chat.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content h1 {\n  font-size: 1.67em;\n  padding-top: 7px;\n  width: auto;\n  padding-bottom: 7px;\n}\nion-content .followed-items .driver {\n  margin-top: 5px;\n  margin-bottom: 5px;\n  border-radius: 30px;\n  background-color: #fbb91d;\n  padding: 5px;\n  float: right;\n  width: 80%;\n  color: black;\n}\nion-content .followed-items .user {\n  margin-top: 5px;\n  margin-bottom: 5px;\n  border-radius: 30px;\n  background-color: #101110;\n  padding: 5px;\n  float: left;\n  width: 80%;\n  color: #fbb91d;\n}\nion-content .chatTxt {\n  text-align: left;\n  padding: 10px;\n}\nion-footer #container_1 {\n  margin-top: 2%;\n  height: 70px;\n  width: 148px;\n  color: white;\n  text-align: center;\n  background-position: left;\n  overflow: hidden;\n}\nion-footer #container_1 ion-icon {\n  color: #ffffff;\n}\nion-footer #container_1 h2 {\n  font-size: 1em;\n  height: auto;\n}\nion-footer #container_1 ion-icon {\n  margin: 5px;\n}\nion-footer #container_1 .profile-pic {\n  width: 15%;\n  height: 18%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYWhhL0lvbmljQXBwcy9DNFJpZGUtSU9TLTIwMjQvY2FsbDRyaWRlLXBhc3Nlbmdlci9zcmMvYXBwL3BhZ2VzL2NoYXQvY2hhdC5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2NoYXQvY2hhdC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0U7RUFFRSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0FDREo7QURLSTtFQUNFLGVBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBRUEseUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0FDSk47QURhSTtFQUNFLGVBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBRUEseUJBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLFVBQUE7RUFDQSxjQUFBO0FDWk47QUR3QkU7RUFDRSxnQkFBQTtFQUNBLGFBQUE7QUN0Qko7QUQ2Q0U7RUFDRSxjQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFFQSxZQUFBO0VBQ0Esa0JBQUE7RUFFQSx5QkFBQTtFQUNBLGdCQUFBO0FDNUNKO0FENkNJO0VBQ0UsY0FBQTtBQzNDTjtBRDZDSTtFQUNFLGNBQUE7RUFDQSxZQUFBO0FDM0NOO0FEOENJO0VBQ0UsV0FBQTtBQzVDTjtBRCtDSTtFQUNFLFVBQUE7RUFDQSxXQUFBO0FDN0NOIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvY2hhdC9jaGF0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50IHtcclxuICBoMSB7XHJcbiAgICAvLyBiYWNrZ3JvdW5kOiByZ2IoMzYsIDE0MCwgMjEwKTtcclxuICAgIGZvbnQtc2l6ZTogMS42N2VtO1xyXG4gICAgcGFkZGluZy10b3A6IDdweDtcclxuICAgIHdpZHRoOiBhdXRvO1xyXG4gICAgcGFkZGluZy1ib3R0b206IDdweDtcclxuICB9XHJcblxyXG4gIC5mb2xsb3dlZC1pdGVtcyB7XHJcbiAgICAuZHJpdmVyIHtcclxuICAgICAgbWFyZ2luLXRvcDogNXB4O1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiA1cHg7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDMwcHg7XHJcbiAgICAgIC8vIGJvcmRlcjogMXB4IHNvbGlkIHJnYmEoMjEyLCAyMTIsIDIxMiwgMC45Myk7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmYmI5MWQ7XHJcbiAgICAgIHBhZGRpbmc6IDVweDtcclxuICAgICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgICB3aWR0aDogODAlO1xyXG4gICAgICBjb2xvcjogcmdiKDAsIDAsIDApO1xyXG5cclxuICAgICAgLy8gcCB7XHJcbiAgICAgIC8vICAgZm9udC1zaXplOiAxNnB4O1xyXG4gICAgICAvLyAgIGZvbnQtc2l6ZTogNHZ3O1xyXG4gICAgICAvLyAgIGNvbG9yOiByZ2IoMCwgMCwgMCk7XHJcbiAgICAgIC8vIH1cclxuICAgIH1cclxuXHJcbiAgICAudXNlciB7XHJcbiAgICAgIG1hcmdpbi10b3A6IDVweDtcclxuICAgICAgbWFyZ2luLWJvdHRvbTogNXB4O1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xyXG4gICAgICAvLyBib3JkZXI6IDFweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpO1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMTAxMTEwO1xyXG4gICAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgICB3aWR0aDogODAlO1xyXG4gICAgICBjb2xvcjogI2ZiYjkxZFxyXG5cclxuICAgICAgLy8gcCB7XHJcbiAgICAgIC8vICAgZm9udC1zaXplOiAxNnB4O1xyXG4gICAgICAvLyAgIGZvbnQtc2l6ZTogNHZ3O1xyXG4gICAgICAvLyAgIGNvbG9yOiByZ2IoMCwgMCwgMCk7XHJcbiAgICAgIC8vIH1cclxuICAgIH1cclxuXHJcblxyXG4gIH1cclxuXHJcbiAgLmNoYXRUeHR7XHJcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICAgcGFkZGluZzogMTBweDtcclxuICB9XHJcblxyXG4gIC8vIC50b3BwZWQtaXRlbXMge1xyXG4gIC8vICAgaW9uLWl0ZW0ge1xyXG4gIC8vICAgICBtYXJnaW4tdG9wOiAwcHg7XHJcbiAgLy8gICAgIG1hcmdpbi1ib3R0b206IDZweDtcclxuICAvLyAgIH1cclxuICAvLyAgIGgyIHtcclxuICAvLyAgICAgY29sb3I6IG9yYW5nZTtcclxuICAvLyAgICAgZm9udC1zaXplOiAxLjI3ZW07XHJcbiAgLy8gICAgIHBhZGRpbmc6IDJweDtcclxuICAvLyAgIH1cclxuXHJcbiAgLy8gICBpb24tbGFiZWwge1xyXG4gIC8vICAgICBjb2xvcjogcmdiKDAsIDE1MywgMjU1KSAhaW1wb3J0YW50O1xyXG4gIC8vICAgICBmb250LXNpemU6IDFlbTtcclxuICAvLyAgICAgcGFkZGluZzogMnB4O1xyXG4gIC8vICAgfVxyXG4gIC8vIH1cclxufVxyXG5cclxuaW9uLWZvb3RlciB7XHJcbiAgI2NvbnRhaW5lcl8xIHtcclxuICAgIG1hcmdpbi10b3A6IDIlO1xyXG4gICAgaGVpZ2h0OiA3MHB4O1xyXG4gICAgd2lkdGg6IDE0OHB4O1xyXG5cclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHJcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBsZWZ0O1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIGlvbi1pY29uIHtcclxuICAgICAgY29sb3I6ICNmZmZmZmY7XHJcbiAgICB9XHJcbiAgICBoMiB7XHJcbiAgICAgIGZvbnQtc2l6ZTogMWVtO1xyXG4gICAgICBoZWlnaHQ6IGF1dG87XHJcbiAgICB9XHJcblxyXG4gICAgaW9uLWljb24ge1xyXG4gICAgICBtYXJnaW46IDVweDtcclxuICAgIH1cclxuXHJcbiAgICAucHJvZmlsZS1waWMge1xyXG4gICAgICB3aWR0aDogMTUlO1xyXG4gICAgICBoZWlnaHQ6IDE4JTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuIiwiaW9uLWNvbnRlbnQgaDEge1xuICBmb250LXNpemU6IDEuNjdlbTtcbiAgcGFkZGluZy10b3A6IDdweDtcbiAgd2lkdGg6IGF1dG87XG4gIHBhZGRpbmctYm90dG9tOiA3cHg7XG59XG5pb24tY29udGVudCAuZm9sbG93ZWQtaXRlbXMgLmRyaXZlciB7XG4gIG1hcmdpbi10b3A6IDVweDtcbiAgbWFyZ2luLWJvdHRvbTogNXB4O1xuICBib3JkZXItcmFkaXVzOiAzMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmJiOTFkO1xuICBwYWRkaW5nOiA1cHg7XG4gIGZsb2F0OiByaWdodDtcbiAgd2lkdGg6IDgwJTtcbiAgY29sb3I6IGJsYWNrO1xufVxuaW9uLWNvbnRlbnQgLmZvbGxvd2VkLWl0ZW1zIC51c2VyIHtcbiAgbWFyZ2luLXRvcDogNXB4O1xuICBtYXJnaW4tYm90dG9tOiA1cHg7XG4gIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICMxMDExMTA7XG4gIHBhZGRpbmc6IDVweDtcbiAgZmxvYXQ6IGxlZnQ7XG4gIHdpZHRoOiA4MCU7XG4gIGNvbG9yOiAjZmJiOTFkO1xufVxuaW9uLWNvbnRlbnQgLmNoYXRUeHQge1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBwYWRkaW5nOiAxMHB4O1xufVxuXG5pb24tZm9vdGVyICNjb250YWluZXJfMSB7XG4gIG1hcmdpbi10b3A6IDIlO1xuICBoZWlnaHQ6IDcwcHg7XG4gIHdpZHRoOiAxNDhweDtcbiAgY29sb3I6IHdoaXRlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGxlZnQ7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG5pb24tZm9vdGVyICNjb250YWluZXJfMSBpb24taWNvbiB7XG4gIGNvbG9yOiAjZmZmZmZmO1xufVxuaW9uLWZvb3RlciAjY29udGFpbmVyXzEgaDIge1xuICBmb250LXNpemU6IDFlbTtcbiAgaGVpZ2h0OiBhdXRvO1xufVxuaW9uLWZvb3RlciAjY29udGFpbmVyXzEgaW9uLWljb24ge1xuICBtYXJnaW46IDVweDtcbn1cbmlvbi1mb290ZXIgI2NvbnRhaW5lcl8xIC5wcm9maWxlLXBpYyB7XG4gIHdpZHRoOiAxNSU7XG4gIGhlaWdodDogMTglO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/chat/chat.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/chat/chat.page.ts ***!
  \*****************************************/
/*! exports provided: ChatPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatPage", function() { return ChatPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_event_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/event.service */ "./src/app/services/event.service.ts");
/* harmony import */ var src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/profile.service */ "./src/app/services/profile.service.ts");
/* harmony import */ var src_app_services_language_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/language.service */ "./src/app/services/language.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/pop-up.service */ "./src/app/services/pop-up.service.ts");








let ChatPage = class ChatPage {
    constructor(navCtrl, alert, eventProvider, ph, modalctrl, lp, actRoute, pop) {
        this.navCtrl = navCtrl;
        this.alert = alert;
        this.eventProvider = eventProvider;
        this.ph = ph;
        this.modalctrl = modalctrl;
        this.lp = lp;
        this.actRoute = actRoute;
        this.pop = pop;
        this.data = { type: '', nickname: '', message: '' };
        this.id = this.actRoute.snapshot.paramMap.get('id');
    }
    ionViewDidEnter() {
        console.log('inregf');
        this.eventProvider.getChatList(this.id).orderByValue().on('value', snapshot => {
            this.eventList = [];
            console.log('sjiy');
            snapshot.forEach(snap => {
                this.eventList.push({
                    id: snap.key,
                    driver: snap.val().Driver_Message,
                    user: snap.val().Client_Message,
                });
                console.log(this.eventList);
                return false;
            });
        });
    }
    closeChat() {
        this.modalctrl.dismiss();
    }
    Send() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            console.log('MESSAGE', this.data.message);
            if (this.data.message == '') {
                this.pop.showPimp("Message is Empty so it cannot send");
            }
            else {
                this.ph.SendMessage(this.data.message, this.id);
                this.data.message = '';
            }
            // const alert = await this.alert.create({
            //   header: 'message',
            //   inputs: [
            //     {
            //       name: 'Message',
            //       placeholder: 'Reply'
            //     },
            //   ],
            //   buttons: [
            //     {
            //       text: 'Cancel',
            //       role: 'cancel',
            //       handler: data => {
            //       }
            //     },
            //     {
            //       text: 'Send',
            //       handler: data => {
            //         this.ph.SendMessage(data.Message, this.id);
            //       }
            //     }
            //   ]
            // });
            // return await alert.present();
        });
    }
    ngOnInit() {
    }
};
ChatPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: src_app_services_event_service__WEBPACK_IMPORTED_MODULE_3__["EventService"] },
    { type: src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_4__["ProfileService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: src_app_services_language_service__WEBPACK_IMPORTED_MODULE_5__["LanguageService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"] },
    { type: src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_7__["PopUpService"] }
];
ChatPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-chat',
        template: __webpack_require__(/*! raw-loader!./chat.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/chat/chat.page.html"),
        styles: [__webpack_require__(/*! ./chat.page.scss */ "./src/app/pages/chat/chat.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
        src_app_services_event_service__WEBPACK_IMPORTED_MODULE_3__["EventService"], src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_4__["ProfileService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
        src_app_services_language_service__WEBPACK_IMPORTED_MODULE_5__["LanguageService"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"], src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_7__["PopUpService"]])
], ChatPage);



/***/ }),

/***/ "./src/app/pages/driver-info/driver-info.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/pages/driver-info/driver-info.page.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".buttonNew {\n  border: 5px solid #ffff00;\n  width: 60%;\n  left: 50%;\n  margin: 0;\n  top: 70%;\n  left: 50%;\n}\n\n.centerBtn {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n          align-items: center;\n  background: white;\n}\n\n.button {\n  height: 50px;\n}\n\n.butt {\n  display: inline-table;\n  height: auto;\n  overflow: hidden;\n}\n\n.price {\n  color: rgba(52, 184, 12, 0.705);\n  font-size: 1.67em;\n  padding-top: 14px;\n  padding-bottom: 14px;\n  border-radius: 12px;\n}\n\n.price ion-icon {\n  font-size: 0.8em;\n  padding: 5px;\n  color: rgba(52, 184, 12, 0.705);\n}\n\n.location {\n  width: auto;\n  padding-top: 8px;\n  padding-bottom: 8px;\n}\n\n.location p {\n  font-size: 1.3em;\n  height: auto;\n}\n\n.location ion-icon {\n  font-size: 0.8em;\n  padding: 5px;\n  color: orange;\n}\n\n.date {\n  color: orange;\n  font-size: 1.47em;\n  padding-top: 14px;\n  padding-bottom: 14px;\n  border-bottom: 1px solid #d8d8d8;\n}\n\n.date ion-icon {\n  font-size: 0.8em;\n  padding: 5px;\n  color: rgba(52, 184, 12, 0.705);\n}\n\n.destination {\n  width: auto;\n  padding-top: 8px;\n  padding-bottom: 8px;\n}\n\n.destination ion-icon {\n  font-size: 0.8em;\n  padding: 5px;\n  color: darkslateblue;\n}\n\n#envelope {\n  height: auto;\n  width: 6em;\n}\n\n.bars {\n  margin-top: 0%;\n  padding: 12px;\n}\n\n.bars .poiter {\n  z-index: 5;\n  margin-left: 1%;\n  background: rgba(240, 240, 240, 0.92);\n  border-left: 1.1px solid #d8d8d8;\n  border-right: 1.1px solid #d8d8d8;\n  border-top: 1px solid #d8d8d8;\n  border-bottom: 1px solid #d8d8d8;\n}\n\n.bars .bars-locate {\n  height: 50px;\n  width: 100%;\n  background: rgba(240, 240, 240, 0.92);\n  border-left: 1.1px solid #d8d8d8;\n  border-right: 1.1px solid #d8d8d8;\n  border-top: 1px solid #d8d8d8;\n  border-bottom: 1px solid #d8d8d8;\n  margin-left: 0%;\n  z-index: 3;\n  border-radius: 12px;\n  overflow: hidden;\n  line-height: 20px;\n  font-size: 1.2em;\n  text-align: center;\n}\n\n.bars .bars-locate ion-icon {\n  position: absolute;\n  font-size: 1em;\n  left: 2%;\n  color: blue;\n  padding: 5px;\n}\n\n.bars .bars-destinate {\n  height: 100px;\n  width: 100%;\n  background: rgba(240, 240, 240, 0.92);\n  margin: 3% 0 0 -50px;\n  margin-left: 0%;\n  z-index: 3;\n  border-left: 1.1px solid #d8d8d8;\n  border-right: 1.1px solid #d8d8d8;\n  border-top: 1px solid #d8d8d8;\n  border-bottom: 1px solid #d8d8d8;\n  overflow: hidden;\n  border-radius: 50px;\n  line-height: 20px;\n  font-size: 1.2em;\n  text-align: center;\n}\n\n.bars .bars-destinate ion-icon {\n  position: absolute;\n  font-size: 1em;\n  left: 2%;\n  padding: 5px;\n  color: rgba(52, 184, 12, 0.705);\n}\n\n.bars .bars-price {\n  border: 5px solid #ff5100;\n  width: 60%;\n  left: 50%;\n  margin: 0;\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  -webkit-transform: translate(-50%, -50%);\n          transform: translate(-50%, -50%);\n}\n\n.bars .bars-price ion-icon {\n  position: absolute;\n  font-size: 1em;\n  left: 2%;\n  color: blue;\n  padding: 5px;\n}\n\n#position {\n  text-align: center;\n  padding-left: 17px;\n}\n\n#whereto {\n  text-align: center;\n  padding-left: 17px;\n}\n\n#stuff {\n  color: rgba(52, 184, 12, 0.705);\n  width: 100%;\n  height: 60% !important;\n  border: 1px solid rgba(52, 184, 12, 0.705);\n}\n\n.no-scroll {\n  background: blue;\n}\n\n#buttonContainer {\n  width: 100%;\n}\n\n#buttonContainer button {\n  background: blue;\n  border-radius: 12px;\n  color: white;\n  margin: 6px;\n  padding: 15px;\n}\n\nh1 {\n  color: white;\n}\n\n.stack {\n  text-align: center;\n  font-size: 0.5em;\n  color: white;\n}\n\n.guttonz {\n  background: red;\n  border-radius: 15px;\n  height: auto;\n  width: auto;\n  margin-left: 4%;\n  color: white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYWhhL0lvbmljQXBwcy9DNFJpZGUtSU9TLTIwMjQvY2FsbDRyaWRlLXBhc3Nlbmdlci9zcmMvYXBwL3BhZ2VzL2RyaXZlci1pbmZvL2RyaXZlci1pbmZvLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvZHJpdmVyLWluZm8vZHJpdmVyLWluZm8ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UseUJBQUE7RUFDQSxVQUFBO0VBQ0EsU0FBQTtFQUNBLFNBQUE7RUFFQSxRQUFBO0VBQ0EsU0FBQTtBQ0FGOztBREtDO0VBQ0csb0JBQUE7RUFBQSxhQUFBO0VBQ0Esd0JBQUE7VUFBQSx1QkFBQTtFQUNBLHlCQUFBO1VBQUEsbUJBQUE7RUFDQSxpQkFBQTtBQ0ZKOztBRE1BO0VBQ0UsWUFBQTtBQ0hGOztBREtBO0VBQ0UscUJBQUE7RUFDQSxZQUFBO0VBRUEsZ0JBQUE7QUNIRjs7QURNQTtFQUNFLCtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0VBRUEsbUJBQUE7QUNKRjs7QURLRTtFQUNFLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLCtCQUFBO0FDSEo7O0FET0E7RUFDRSxXQUFBO0VBRUEsZ0JBQUE7RUFDQSxtQkFBQTtBQ0xGOztBRE1FO0VBQ0UsZ0JBQUE7RUFDQSxZQUFBO0FDSko7O0FET0U7RUFDRSxnQkFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0FDTEo7O0FEU0E7RUFDRSxhQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0VBQ0EsZ0NBQUE7QUNORjs7QURVRTtFQUNFLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLCtCQUFBO0FDUko7O0FEWUE7RUFDRSxXQUFBO0VBRUEsZ0JBQUE7RUFDQSxtQkFBQTtBQ1ZGOztBRFdFO0VBQ0UsZ0JBQUE7RUFDQSxZQUFBO0VBQ0Esb0JBQUE7QUNUSjs7QURhQTtFQUNFLFlBQUE7RUFDQSxVQUFBO0FDVkY7O0FEYUE7RUFDRSxjQUFBO0VBQ0EsYUFBQTtBQ1ZGOztBRFlFO0VBQ0UsVUFBQTtFQUNBLGVBQUE7RUFDQSxxQ0FBQTtFQUNBLGdDQUFBO0VBQ0EsaUNBQUE7RUFDQSw2QkFBQTtFQUNBLGdDQUFBO0FDVko7O0FEYUU7RUFDRSxZQUFBO0VBQ0EsV0FBQTtFQUNBLHFDQUFBO0VBQ0EsZ0NBQUE7RUFDQSxpQ0FBQTtFQUNBLDZCQUFBO0VBQ0EsZ0NBQUE7RUFFQSxlQUFBO0VBRUEsVUFBQTtFQUVBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUNkSjs7QURnQkk7RUFDRSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxRQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUNkTjs7QURrQkU7RUFDRSxhQUFBO0VBQ0EsV0FBQTtFQUNBLHFDQUFBO0VBQ0Esb0JBQUE7RUFDQSxlQUFBO0VBQ0EsVUFBQTtFQUNBLGdDQUFBO0VBQ0EsaUNBQUE7RUFDQSw2QkFBQTtFQUNBLGdDQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBQ2hCSjs7QURrQkk7RUFDRSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxRQUFBO0VBQ0EsWUFBQTtFQUNBLCtCQUFBO0FDaEJOOztBRG9CRTtFQUNFLHlCQUFBO0VBQ0EsVUFBQTtFQUNBLFNBQUE7RUFDQSxTQUFBO0VBQ0Esa0JBQUE7RUFDQSxRQUFBO0VBQ0EsU0FBQTtFQUVBLHdDQUFBO1VBQUEsZ0NBQUE7QUNsQko7O0FEb0JJO0VBQ0Usa0JBQUE7RUFDQSxjQUFBO0VBQ0EsUUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FDbEJOOztBRHVCQTtFQUNFLGtCQUFBO0VBQ0Esa0JBQUE7QUNwQkY7O0FEdUJBO0VBQ0Usa0JBQUE7RUFDQSxrQkFBQTtBQ3BCRjs7QUR1QkE7RUFDRSwrQkFBQTtFQUNBLFdBQUE7RUFDQSxzQkFBQTtFQUNBLDBDQUFBO0FDcEJGOztBRHVCQTtFQUNFLGdCQUFBO0FDcEJGOztBRHVCQTtFQUNFLFdBQUE7QUNwQkY7O0FEcUJFO0VBRUUsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBRUEsYUFBQTtBQ3JCSjs7QUR5QkE7RUFDRSxZQUFBO0FDdEJGOztBRHlCQTtFQUNFLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0FDdEJGOztBRHlCQTtFQUNFLGVBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUVBLFlBQUE7QUN2QkYiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9kcml2ZXItaW5mby9kcml2ZXItaW5mby5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYnV0dG9uTmV3IHtcclxuICBib3JkZXI6IDVweCBzb2xpZCAjZmZmZjAwO1xyXG4gIHdpZHRoOiA2MCU7XHJcbiAgbGVmdDogNTAlO1xyXG4gIG1hcmdpbjogMDtcclxuICAvLyBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgdG9wOiA3MCU7XHJcbiAgbGVmdDogNTAlO1xyXG4gIC8vIC1tcy10cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcclxuICAvLyB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcclxufVxyXG5cclxuIC5jZW50ZXJCdG4ge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG5cclxuICB9XHJcblxyXG4uYnV0dG9uIHtcclxuICBoZWlnaHQ6IDUwcHg7XHJcbn1cclxuLmJ1dHQge1xyXG4gIGRpc3BsYXk6IGlubGluZS10YWJsZTtcclxuICBoZWlnaHQ6IGF1dG87XHJcblxyXG4gIG92ZXJmbG93OiBoaWRkZW47XHJcbn1cclxuXHJcbi5wcmljZSB7XHJcbiAgY29sb3I6IHJnYmEoNTIsIDE4NCwgMTIsIDAuNzA1KTtcclxuICBmb250LXNpemU6IDEuNjdlbTtcclxuICBwYWRkaW5nLXRvcDogMTRweDtcclxuICBwYWRkaW5nLWJvdHRvbTogMTRweDtcclxuXHJcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcclxuICBpb24taWNvbiB7XHJcbiAgICBmb250LXNpemU6IDAuOGVtO1xyXG4gICAgcGFkZGluZzogNXB4O1xyXG4gICAgY29sb3I6IHJnYmEoNTIsIDE4NCwgMTIsIDAuNzA1KTtcclxuICB9XHJcbn1cclxuXHJcbi5sb2NhdGlvbiB7XHJcbiAgd2lkdGg6IGF1dG87XHJcbiAgLy8gYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGNvbG9yKCRjb2xvcnMsIGxpZ2h0LCBiYXNlKTtcclxuICBwYWRkaW5nLXRvcDogOHB4O1xyXG4gIHBhZGRpbmctYm90dG9tOiA4cHg7XHJcbiAgcCB7XHJcbiAgICBmb250LXNpemU6IDEuM2VtO1xyXG4gICAgaGVpZ2h0OiBhdXRvO1xyXG4gIH1cclxuXHJcbiAgaW9uLWljb24ge1xyXG4gICAgZm9udC1zaXplOiAwLjhlbTtcclxuICAgIHBhZGRpbmc6IDVweDtcclxuICAgIGNvbG9yOiBvcmFuZ2U7XHJcbiAgfVxyXG59XHJcblxyXG4uZGF0ZSB7XHJcbiAgY29sb3I6IG9yYW5nZTtcclxuICBmb250LXNpemU6IDEuNDdlbTtcclxuICBwYWRkaW5nLXRvcDogMTRweDtcclxuICBwYWRkaW5nLWJvdHRvbTogMTRweDtcclxuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xyXG4gIC8vIGJvcmRlcjogMXB4IHNvbGlkIGNvbG9yKCRjb2xvcnMsIGxpZ2h0LCBiYXNlKTtcclxuICAvL2JvcmRlci1ib3R0b206IDFweCBzb2xpZCBjb2xvcigkY29sb3JzLCBsaWdodCwgYmFzZSk7XHJcbiAgLy8gYm9yZGVyLXJhZGl1czogMTJweDtcclxuICBpb24taWNvbiB7XHJcbiAgICBmb250LXNpemU6IDAuOGVtO1xyXG4gICAgcGFkZGluZzogNXB4O1xyXG4gICAgY29sb3I6IHJnYmEoNTIsIDE4NCwgMTIsIDAuNzA1KTtcclxuICB9XHJcbn1cclxuXHJcbi5kZXN0aW5hdGlvbiB7XHJcbiAgd2lkdGg6IGF1dG87XHJcbiAgLy8gYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGNvbG9yKCRjb2xvcnMsIGxpZ2h0LCBiYXNlKTtcclxuICBwYWRkaW5nLXRvcDogOHB4O1xyXG4gIHBhZGRpbmctYm90dG9tOiA4cHg7XHJcbiAgaW9uLWljb24ge1xyXG4gICAgZm9udC1zaXplOiAwLjhlbTtcclxuICAgIHBhZGRpbmc6IDVweDtcclxuICAgIGNvbG9yOiBkYXJrc2xhdGVibHVlO1xyXG4gIH1cclxufVxyXG5cclxuI2VudmVsb3BlIHtcclxuICBoZWlnaHQ6IGF1dG87XHJcbiAgd2lkdGg6IDZlbTtcclxufVxyXG5cclxuLmJhcnMge1xyXG4gIG1hcmdpbi10b3A6IDAlO1xyXG4gIHBhZGRpbmc6IDEycHg7XHJcblxyXG4gIC5wb2l0ZXIge1xyXG4gICAgei1pbmRleDogNTtcclxuICAgIG1hcmdpbi1sZWZ0OiAxJTtcclxuICAgIGJhY2tncm91bmQ6IHJnYmEoMjQwLCAyNDAsIDI0MCwgMC45Mik7XHJcbiAgICBib3JkZXItbGVmdDogMS4xcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xyXG4gICAgYm9yZGVyLXJpZ2h0OiAxLjFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNik7XHJcbiAgICBib3JkZXItdG9wOiAxcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHJnYigyMTYsIDIxNiwgMjE2KTtcclxuICB9XHJcblxyXG4gIC5iYXJzLWxvY2F0ZSB7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGJhY2tncm91bmQ6IHJnYmEoMjQwLCAyNDAsIDI0MCwgMC45Mik7XHJcbiAgICBib3JkZXItbGVmdDogMS4xcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xyXG4gICAgYm9yZGVyLXJpZ2h0OiAxLjFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNik7XHJcbiAgICBib3JkZXItdG9wOiAxcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHJnYigyMTYsIDIxNiwgMjE2KTtcclxuICAgIC8vICBtYXJnaW46IC0xMiUgMCAwIC01MHB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDAlO1xyXG5cclxuICAgIHotaW5kZXg6IDM7XHJcbiAgICAvLyBib3JkZXI6IDFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNik7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xyXG4gICAgZm9udC1zaXplOiAxLjJlbTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHJcbiAgICBpb24taWNvbiB7XHJcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgZm9udC1zaXplOiAxZW07XHJcbiAgICAgIGxlZnQ6IDIlO1xyXG4gICAgICBjb2xvcjogYmx1ZTtcclxuICAgICAgcGFkZGluZzogNXB4O1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLmJhcnMtZGVzdGluYXRlIHtcclxuICAgIGhlaWdodDogMTAwcHg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGJhY2tncm91bmQ6IHJnYmEoMjQwLCAyNDAsIDI0MCwgMC45Mik7XHJcbiAgICBtYXJnaW46IDMlIDAgMCAtNTBweDtcclxuICAgIG1hcmdpbi1sZWZ0OiAwJTtcclxuICAgIHotaW5kZXg6IDM7XHJcbiAgICBib3JkZXItbGVmdDogMS4xcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xyXG4gICAgYm9yZGVyLXJpZ2h0OiAxLjFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNik7XHJcbiAgICBib3JkZXItdG9wOiAxcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHJnYigyMTYsIDIxNiwgMjE2KTtcclxuICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MHB4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDIwcHg7XHJcbiAgICBmb250LXNpemU6IDEuMmVtO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG5cclxuICAgIGlvbi1pY29uIHtcclxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICBmb250LXNpemU6IDFlbTtcclxuICAgICAgbGVmdDogMiU7XHJcbiAgICAgIHBhZGRpbmc6IDVweDtcclxuICAgICAgY29sb3I6IHJnYmEoNTIsIDE4NCwgMTIsIDAuNzA1KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC5iYXJzLXByaWNlIHtcclxuICAgIGJvcmRlcjogNXB4IHNvbGlkICNmZjUxMDA7XHJcbiAgICB3aWR0aDogNjAlO1xyXG4gICAgbGVmdDogNTAlO1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiA1MCU7XHJcbiAgICBsZWZ0OiA1MCU7XHJcbiAgICAtbXMtdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcclxuXHJcbiAgICBpb24taWNvbiB7XHJcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgZm9udC1zaXplOiAxZW07XHJcbiAgICAgIGxlZnQ6IDIlO1xyXG4gICAgICBjb2xvcjogYmx1ZTtcclxuICAgICAgcGFkZGluZzogNXB4O1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuI3Bvc2l0aW9uIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgcGFkZGluZy1sZWZ0OiAxN3B4O1xyXG59XHJcblxyXG4jd2hlcmV0byB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIHBhZGRpbmctbGVmdDogMTdweDtcclxufVxyXG5cclxuI3N0dWZmIHtcclxuICBjb2xvcjogcmdiYSg1MiwgMTg0LCAxMiwgMC43MDUpO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogNjAlICFpbXBvcnRhbnQ7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgcmdiYSg1MiwgMTg0LCAxMiwgMC43MDUpO1xyXG59XHJcblxyXG4ubm8tc2Nyb2xsIHtcclxuICBiYWNrZ3JvdW5kOiBibHVlO1xyXG59XHJcblxyXG4jYnV0dG9uQ29udGFpbmVyIHtcclxuICB3aWR0aDogMTAwJTtcclxuICBidXR0b24ge1xyXG4gICAgLy9oZWlnaHQ6IDgwJSAhaW1wb3J0YW50O1xyXG4gICAgYmFja2dyb3VuZDogYmx1ZTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDEycHg7XHJcbiAgICBjb2xvcjogcmdiKDI1NSwgMjU1LCAyNTUpO1xyXG4gICAgbWFyZ2luOiA2cHg7XHJcbiAgICAvLyBib3JkZXI6IDZweCBzb2xpZCByZ2IoMjU1LCAyNTUsIDI1NSk7XHJcbiAgICBwYWRkaW5nOiAxNXB4O1xyXG4gIH1cclxufVxyXG5cclxuaDEge1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuLnN0YWNrIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgZm9udC1zaXplOiAwLjVlbTtcclxuICBjb2xvcjogd2hpdGU7XHJcbn1cclxuXHJcbi5ndXR0b256IHtcclxuICBiYWNrZ3JvdW5kOiByZWQ7XHJcbiAgYm9yZGVyLXJhZGl1czogMTVweDtcclxuICBoZWlnaHQ6IGF1dG87XHJcbiAgd2lkdGg6IGF1dG87XHJcbiAgbWFyZ2luLWxlZnQ6IDQlO1xyXG4gIC8vIGJveC1zaGFkb3c6IDBweCAwcHggMXB4IDBweCBjb2xvcigkY29sb3JzLCBuYXYtc2hhZG93LCBiYXNlKTtcclxuICBjb2xvcjogd2hpdGU7XHJcbn1cclxuIiwiLmJ1dHRvbk5ldyB7XG4gIGJvcmRlcjogNXB4IHNvbGlkICNmZmZmMDA7XG4gIHdpZHRoOiA2MCU7XG4gIGxlZnQ6IDUwJTtcbiAgbWFyZ2luOiAwO1xuICB0b3A6IDcwJTtcbiAgbGVmdDogNTAlO1xufVxuXG4uY2VudGVyQnRuIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xufVxuXG4uYnV0dG9uIHtcbiAgaGVpZ2h0OiA1MHB4O1xufVxuXG4uYnV0dCB7XG4gIGRpc3BsYXk6IGlubGluZS10YWJsZTtcbiAgaGVpZ2h0OiBhdXRvO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuXG4ucHJpY2Uge1xuICBjb2xvcjogcmdiYSg1MiwgMTg0LCAxMiwgMC43MDUpO1xuICBmb250LXNpemU6IDEuNjdlbTtcbiAgcGFkZGluZy10b3A6IDE0cHg7XG4gIHBhZGRpbmctYm90dG9tOiAxNHB4O1xuICBib3JkZXItcmFkaXVzOiAxMnB4O1xufVxuLnByaWNlIGlvbi1pY29uIHtcbiAgZm9udC1zaXplOiAwLjhlbTtcbiAgcGFkZGluZzogNXB4O1xuICBjb2xvcjogcmdiYSg1MiwgMTg0LCAxMiwgMC43MDUpO1xufVxuXG4ubG9jYXRpb24ge1xuICB3aWR0aDogYXV0bztcbiAgcGFkZGluZy10b3A6IDhweDtcbiAgcGFkZGluZy1ib3R0b206IDhweDtcbn1cbi5sb2NhdGlvbiBwIHtcbiAgZm9udC1zaXplOiAxLjNlbTtcbiAgaGVpZ2h0OiBhdXRvO1xufVxuLmxvY2F0aW9uIGlvbi1pY29uIHtcbiAgZm9udC1zaXplOiAwLjhlbTtcbiAgcGFkZGluZzogNXB4O1xuICBjb2xvcjogb3JhbmdlO1xufVxuXG4uZGF0ZSB7XG4gIGNvbG9yOiBvcmFuZ2U7XG4gIGZvbnQtc2l6ZTogMS40N2VtO1xuICBwYWRkaW5nLXRvcDogMTRweDtcbiAgcGFkZGluZy1ib3R0b206IDE0cHg7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZDhkOGQ4O1xufVxuLmRhdGUgaW9uLWljb24ge1xuICBmb250LXNpemU6IDAuOGVtO1xuICBwYWRkaW5nOiA1cHg7XG4gIGNvbG9yOiByZ2JhKDUyLCAxODQsIDEyLCAwLjcwNSk7XG59XG5cbi5kZXN0aW5hdGlvbiB7XG4gIHdpZHRoOiBhdXRvO1xuICBwYWRkaW5nLXRvcDogOHB4O1xuICBwYWRkaW5nLWJvdHRvbTogOHB4O1xufVxuLmRlc3RpbmF0aW9uIGlvbi1pY29uIHtcbiAgZm9udC1zaXplOiAwLjhlbTtcbiAgcGFkZGluZzogNXB4O1xuICBjb2xvcjogZGFya3NsYXRlYmx1ZTtcbn1cblxuI2VudmVsb3BlIHtcbiAgaGVpZ2h0OiBhdXRvO1xuICB3aWR0aDogNmVtO1xufVxuXG4uYmFycyB7XG4gIG1hcmdpbi10b3A6IDAlO1xuICBwYWRkaW5nOiAxMnB4O1xufVxuLmJhcnMgLnBvaXRlciB7XG4gIHotaW5kZXg6IDU7XG4gIG1hcmdpbi1sZWZ0OiAxJTtcbiAgYmFja2dyb3VuZDogcmdiYSgyNDAsIDI0MCwgMjQwLCAwLjkyKTtcbiAgYm9yZGVyLWxlZnQ6IDEuMXB4IHNvbGlkICNkOGQ4ZDg7XG4gIGJvcmRlci1yaWdodDogMS4xcHggc29saWQgI2Q4ZDhkODtcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNkOGQ4ZDg7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZDhkOGQ4O1xufVxuLmJhcnMgLmJhcnMtbG9jYXRlIHtcbiAgaGVpZ2h0OiA1MHB4O1xuICB3aWR0aDogMTAwJTtcbiAgYmFja2dyb3VuZDogcmdiYSgyNDAsIDI0MCwgMjQwLCAwLjkyKTtcbiAgYm9yZGVyLWxlZnQ6IDEuMXB4IHNvbGlkICNkOGQ4ZDg7XG4gIGJvcmRlci1yaWdodDogMS4xcHggc29saWQgI2Q4ZDhkODtcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNkOGQ4ZDg7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZDhkOGQ4O1xuICBtYXJnaW4tbGVmdDogMCU7XG4gIHotaW5kZXg6IDM7XG4gIGJvcmRlci1yYWRpdXM6IDEycHg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICBmb250LXNpemU6IDEuMmVtO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uYmFycyAuYmFycy1sb2NhdGUgaW9uLWljb24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGZvbnQtc2l6ZTogMWVtO1xuICBsZWZ0OiAyJTtcbiAgY29sb3I6IGJsdWU7XG4gIHBhZGRpbmc6IDVweDtcbn1cbi5iYXJzIC5iYXJzLWRlc3RpbmF0ZSB7XG4gIGhlaWdodDogMTAwcHg7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kOiByZ2JhKDI0MCwgMjQwLCAyNDAsIDAuOTIpO1xuICBtYXJnaW46IDMlIDAgMCAtNTBweDtcbiAgbWFyZ2luLWxlZnQ6IDAlO1xuICB6LWluZGV4OiAzO1xuICBib3JkZXItbGVmdDogMS4xcHggc29saWQgI2Q4ZDhkODtcbiAgYm9yZGVyLXJpZ2h0OiAxLjFweCBzb2xpZCAjZDhkOGQ4O1xuICBib3JkZXItdG9wOiAxcHggc29saWQgI2Q4ZDhkODtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNkOGQ4ZDg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIGJvcmRlci1yYWRpdXM6IDUwcHg7XG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICBmb250LXNpemU6IDEuMmVtO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uYmFycyAuYmFycy1kZXN0aW5hdGUgaW9uLWljb24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGZvbnQtc2l6ZTogMWVtO1xuICBsZWZ0OiAyJTtcbiAgcGFkZGluZzogNXB4O1xuICBjb2xvcjogcmdiYSg1MiwgMTg0LCAxMiwgMC43MDUpO1xufVxuLmJhcnMgLmJhcnMtcHJpY2Uge1xuICBib3JkZXI6IDVweCBzb2xpZCAjZmY1MTAwO1xuICB3aWR0aDogNjAlO1xuICBsZWZ0OiA1MCU7XG4gIG1hcmdpbjogMDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDUwJTtcbiAgbGVmdDogNTAlO1xuICAtbXMtdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xufVxuLmJhcnMgLmJhcnMtcHJpY2UgaW9uLWljb24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGZvbnQtc2l6ZTogMWVtO1xuICBsZWZ0OiAyJTtcbiAgY29sb3I6IGJsdWU7XG4gIHBhZGRpbmc6IDVweDtcbn1cblxuI3Bvc2l0aW9uIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwYWRkaW5nLWxlZnQ6IDE3cHg7XG59XG5cbiN3aGVyZXRvIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwYWRkaW5nLWxlZnQ6IDE3cHg7XG59XG5cbiNzdHVmZiB7XG4gIGNvbG9yOiByZ2JhKDUyLCAxODQsIDEyLCAwLjcwNSk7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDYwJSAhaW1wb3J0YW50O1xuICBib3JkZXI6IDFweCBzb2xpZCByZ2JhKDUyLCAxODQsIDEyLCAwLjcwNSk7XG59XG5cbi5uby1zY3JvbGwge1xuICBiYWNrZ3JvdW5kOiBibHVlO1xufVxuXG4jYnV0dG9uQ29udGFpbmVyIHtcbiAgd2lkdGg6IDEwMCU7XG59XG4jYnV0dG9uQ29udGFpbmVyIGJ1dHRvbiB7XG4gIGJhY2tncm91bmQ6IGJsdWU7XG4gIGJvcmRlci1yYWRpdXM6IDEycHg7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgbWFyZ2luOiA2cHg7XG4gIHBhZGRpbmc6IDE1cHg7XG59XG5cbmgxIHtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG4uc3RhY2sge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMC41ZW07XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLmd1dHRvbnoge1xuICBiYWNrZ3JvdW5kOiByZWQ7XG4gIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gIGhlaWdodDogYXV0bztcbiAgd2lkdGg6IGF1dG87XG4gIG1hcmdpbi1sZWZ0OiA0JTtcbiAgY29sb3I6IHdoaXRlO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/driver-info/driver-info.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/driver-info/driver-info.page.ts ***!
  \*******************************************************/
/*! exports provided: DriverInfoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DriverInfoPage", function() { return DriverInfoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/profile.service */ "./src/app/services/profile.service.ts");
/* harmony import */ var src_app_services_language_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/language.service */ "./src/app/services/language.service.ts");
/* harmony import */ var src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/settings.service */ "./src/app/services/settings.service.ts");
/* harmony import */ var src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/pop-up.service */ "./src/app/services/pop-up.service.ts");
/* harmony import */ var _ionic_native_call_number_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/call-number/ngx */ "./node_modules/@ionic-native/call-number/ngx/index.js");








let DriverInfoPage = class DriverInfoPage {
    constructor(navCtrl, ph, lp, settings, pop, modal, callNumber, alertCtl) {
        this.navCtrl = navCtrl;
        this.ph = ph;
        this.lp = lp;
        this.settings = settings;
        this.pop = pop;
        this.modal = modal;
        this.callNumber = callNumber;
        this.alertCtl = alertCtl;
    }
    ionViewDidLoad() {
        console.log("INSIDE DRIVER INFOR PAGE-- CHECK INF0::", this.info);
    }
    closeModal() {
        this.modal.dismiss();
    }
    onChange(e) {
        // this.modal.dismiss(e);
        this.presentAlertCheckbox(e);
    }
    CallDriver() {
        window.open("tel:" + this.info.Driver_number);
        //  window.open(`tel:this.info.Driver_number`, "_system");
        // this.callNumber
        //   .callNumber(this.info.Driver_number, true)
        //   .then((res) => console.log("Launched dialer!", res))
        //   .catch((err) => console.log("Error launching dialer", err));
    }
    ngOnInit() {
        console.log("INSIDE DRIVER INFOR PAGE-- CHECK INF0::", this.info);
    }
    presentAlertCheckbox(e) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertCtl.create({
                header: 'Reason for cancellation ',
                inputs: [
                    {
                        name: 'checkbox1',
                        type: 'radio',
                        label: 'Driver is too far',
                        value: 'pickup',
                        checked: true
                    },
                    {
                        name: 'checkbox2',
                        type: 'radio',
                        label: 'Driver is not coming',
                        value: 'gps'
                    },
                    {
                        name: 'checkbox3',
                        type: 'checkbox',
                        label: 'Wrong car appears',
                        value: 'rider'
                    },
                    {
                        name: 'checkbox4',
                        type: 'radio',
                        label: 'Wrong number plate',
                        value: 'value4'
                    },
                    {
                        name: 'checkbox5',
                        type: 'radio',
                        label: 'Others',
                        value: 'others'
                    },
                ],
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: () => {
                            console.log('Confirm Cancel');
                        }
                    }, {
                        text: 'Ok',
                        handler: () => {
                            console.log('Confirm Ok');
                            this.modal.dismiss(e);
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
};
DriverInfoPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_3__["ProfileService"] },
    { type: src_app_services_language_service__WEBPACK_IMPORTED_MODULE_4__["LanguageService"] },
    { type: src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_5__["SettingsService"] },
    { type: src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_6__["PopUpService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _ionic_native_call_number_ngx__WEBPACK_IMPORTED_MODULE_7__["CallNumber"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], DriverInfoPage.prototype, "info", void 0);
DriverInfoPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-driver-info",
        template: __webpack_require__(/*! raw-loader!./driver-info.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/driver-info/driver-info.page.html"),
        styles: [__webpack_require__(/*! ./driver-info.page.scss */ "./src/app/pages/driver-info/driver-info.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
        src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_3__["ProfileService"],
        src_app_services_language_service__WEBPACK_IMPORTED_MODULE_4__["LanguageService"],
        src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_5__["SettingsService"],
        src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_6__["PopUpService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
        _ionic_native_call_number_ngx__WEBPACK_IMPORTED_MODULE_7__["CallNumber"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]])
], DriverInfoPage);



/***/ }),

/***/ "./src/app/pages/home/home.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/home/home.module.ts ***!
  \*******************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _autocomplete_autocomplete_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../autocomplete/autocomplete.page */ "./src/app/pages/autocomplete/autocomplete.page.ts");
/* harmony import */ var _driver_info_driver_info_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../driver-info/driver-info.page */ "./src/app/pages/driver-info/driver-info.page.ts");
/* harmony import */ var _trip_info_trip_info_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../trip-info/trip-info.page */ "./src/app/pages/trip-info/trip-info.page.ts");
/* harmony import */ var _chat_chat_page__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../chat/chat.page */ "./src/app/pages/chat/chat.page.ts");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./home.page */ "./src/app/pages/home/home.page.ts");
/* harmony import */ var _rate_rate_page__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../rate/rate.page */ "./src/app/pages/rate/rate.page.ts");
/* harmony import */ var _someone_someone_page__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../someone/someone.page */ "./src/app/someone/someone.page.ts");
/* harmony import */ var ionic4_rating__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ionic4-rating */ "./node_modules/ionic4-rating/dist/index.js");















let HomePageModule = class HomePageModule {
};
HomePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            ionic4_rating__WEBPACK_IMPORTED_MODULE_13__["IonicRatingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forChild([
                {
                    path: "",
                    component: _home_page__WEBPACK_IMPORTED_MODULE_10__["HomePage"],
                },
            ]),
        ],
        declarations: [
            _home_page__WEBPACK_IMPORTED_MODULE_10__["HomePage"],
            _autocomplete_autocomplete_page__WEBPACK_IMPORTED_MODULE_6__["AutocompletePage"],
            _driver_info_driver_info_page__WEBPACK_IMPORTED_MODULE_7__["DriverInfoPage"],
            _trip_info_trip_info_page__WEBPACK_IMPORTED_MODULE_8__["TripInfoPage"],
            _chat_chat_page__WEBPACK_IMPORTED_MODULE_9__["ChatPage"],
            _rate_rate_page__WEBPACK_IMPORTED_MODULE_11__["RatePage"],
            _someone_someone_page__WEBPACK_IMPORTED_MODULE_12__["SomeonePage"]
        ],
        entryComponents: [
            _autocomplete_autocomplete_page__WEBPACK_IMPORTED_MODULE_6__["AutocompletePage"],
            _driver_info_driver_info_page__WEBPACK_IMPORTED_MODULE_7__["DriverInfoPage"],
            _trip_info_trip_info_page__WEBPACK_IMPORTED_MODULE_8__["TripInfoPage"],
            _chat_chat_page__WEBPACK_IMPORTED_MODULE_9__["ChatPage"],
            _rate_rate_page__WEBPACK_IMPORTED_MODULE_11__["RatePage"],
            _someone_someone_page__WEBPACK_IMPORTED_MODULE_12__["SomeonePage"]
        ],
    })
], HomePageModule);



/***/ }),

/***/ "./src/app/pages/home/home.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/home/home.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  font-family: \"Montserratt\";\n  margin-top: 0%;\n}\n\nlabel > input {\n  display: none;\n}\n\n#button0 {\n  position: absolute;\n  line-height: 45px;\n  font-weight: bold;\n  text-align: center;\n  display: inline-block;\n  height: 50px;\n  width: 50px;\n  border: 2px dashed black;\n  box-sizing: border-box;\n  left: 0;\n  right: 0;\n  margin: auto;\n  top: 50%;\n  -webkit-transform: translateY(-50%);\n  transform: translateY(-50%);\n  opacity: 0.5;\n}\n\nlabel > input ~ .button {\n  cursor: pointer;\n  position: absolute;\n  line-height: 45px;\n  font-weight: bold;\n  text-align: center;\n  display: inline-block;\n  height: 50px;\n  width: 100%;\n  box-sizing: border-box;\n  -webkit-transition: all 1s ease-in-out;\n  transition: all 1s ease-in-out;\n  -moz-user-select: none;\n   -ms-user-select: none;\n       user-select: none;\n  -webkit-user-select: none;\n}\n\nlabel > input ~ #button1 {\n  left: 0;\n  top: 0;\n  top: 50%;\n  background-color: #f2f2f2;\n  color: black;\n  margin-top: 0%;\n  border-top-left-radius: 27px;\n  border-top-right-radius: 30px;\n  display: inline-block;\n  text-align: center;\n}\n\nlabel > input:checked ~ #button1 {\n  left: calc(0% - 0px);\n  top: calc(70% - 0%);\n  -webkit-transition: all 1s ease-in-out;\n  transition: all 1s ease-in-out;\n}\n\nlabel > input:checked ~ #button1 #map {\n  width: 100%;\n  height: 100%;\n  z-index: 1;\n}\n\n.main-con {\n  background: transparent;\n}\n\n.driverFoundNew {\n  left: 0%;\n  top: 147%;\n  height: 50%;\n  width: 95%;\n  margin-left: 1.5%;\n  background: white;\n  position: absolute;\n  bottom: 0%;\n  z-index: 1;\n  border-top-left-radius: 30px;\n  border-top-right-radius: 30px;\n}\n\n.my-custom-class .modal-wrapper {\n  background: #222;\n}\n\n.TaxiICon2 {\n  width: 70px;\n  height: 70px;\n  vertical-align: middle;\n  text-align: center;\n}\n\n.TaxiICon {\n  width: 100px;\n  height: 35px;\n}\n\n.TaxiIComfirm {\n  width: 100px;\n  height: 35px;\n}\n\n.listRow:hover {\n  background-color: black;\n  color: white;\n}\n\n.align-col {\n  line-height: 1;\n  vertical-align: middle;\n  text-align: center;\n}\n\n.item-radio-checked {\n  --background: black !important;\n  color: white !important;\n}\n\n.active {\n  background-color: black;\n  color: white;\n}\n\n.imgStyle {\n  vertical-align: middle;\n  text-align: center;\n}\n\n.spinners {\n  width: 40px;\n  height: 40px;\n  background-color: white;\n  border-radius: 30px;\n  margin: 14px auto;\n  -webkit-animation: sk-rotateplane 1.2s infinite ease-in-out;\n  animation: sk-rotateplane 1.2s infinite ease-in-out;\n}\n\n@-webkit-keyframes sk-rotateplane {\n  0% {\n    -webkit-transform: perspective(120px);\n  }\n  50% {\n    -webkit-transform: perspective(120px) rotateY(180deg);\n  }\n  100% {\n    -webkit-transform: perspective(120px) rotateY(180deg) rotateX(180deg);\n  }\n}\n\n@keyframes sk-rotateplane {\n  0% {\n    transform: perspective(120px) rotateX(0deg) rotateY(0deg);\n    -webkit-transform: perspective(120px) rotateX(0deg) rotateY(0deg);\n  }\n  50% {\n    transform: perspective(120px) rotateX(-180.1deg) rotateY(0deg);\n    -webkit-transform: perspective(120px) rotateX(-180.1deg) rotateY(0deg);\n  }\n  100% {\n    transform: perspective(120px) rotateX(-180deg) rotateY(-179.9deg);\n    -webkit-transform: perspective(120px) rotateX(-180deg) rotateY(-179.9deg);\n  }\n}\n\n.scrolling-wrapper {\n  overflow: auto !important;\n  width: auto;\n  display: grid;\n  grid-template-columns: auto auto auto auto auto auto auto auto;\n  border-radius: 30px;\n  padding: 0px;\n  margin-top: 22px;\n}\n\n.scrolling-wrapper #stf {\n  margin: 5px;\n  width: 100px;\n  height: 100px;\n  font-size: 1em;\n  background: #d8d8d8;\n  border-radius: 30px;\n}\n\n.scrolling-wrapper #stf img {\n  background: #d8d8d8;\n  padding: 6px;\n  border-radius: 30px;\n  width: 90px;\n  height: 90px;\n}\n\n.scrolling-wrapper #stf h3 {\n  text-align: center;\n  padding: 1px;\n}\n\n.scrolling-wrapper #stf2 {\n  margin: 3px;\n  width: auto;\n  height: auto;\n  font-size: 1em;\n  border-radius: 30px;\n}\n\n.scrolling-wrapper #stf2 h3 {\n  text-align: center;\n  padding: 1px;\n}\n\n.centerText {\n  text-align: center;\n  font-size: 16px !important;\n}\n\n.scrolling-wrapper2 {\n  overflow: auto !important;\n  width: 100%;\n  display: grid;\n  grid-template-columns: auto auto auto auto auto auto auto auto;\n  border-radius: 30px;\n  padding: 0px;\n  margin-top: 22px;\n  -webkit-animation-direction: alternate;\n          animation-direction: alternate;\n  -webkit-animation-name: bump;\n          animation-name: bump;\n  -webkit-animation-duration: 1.1s;\n          animation-duration: 1.1s;\n  -webkit-animation-iteration-count: 1;\n          animation-iteration-count: 1;\n  background: #fdfdfd;\n}\n\n.scrolling-wrapper2 #stf {\n  margin: 7px;\n  width: 100px;\n  height: 100px;\n  font-size: 1em;\n  background: #d8d8d8;\n  border-radius: 30px;\n}\n\n.scrolling-wrapper2 #stf img {\n  padding: 16px;\n  border-radius: 30px;\n}\n\nion-item {\n  width: 100%;\n}\n\nion-footer {\n  font-family: \"Humanst\";\n  margin-top: 0% !important;\n}\n\n.diss {\n  padding-left: 60%;\n  margin-top: 10%;\n  z-index: 3;\n}\n\n.mid-right {\n  padding-left: 80%;\n  margin-top: 20%;\n  z-index: 3;\n}\n\n.mid-right2 {\n  padding-left: 60%;\n  margin-top: 40%;\n  z-index: 3;\n}\n\n.mid-right2 ion-icon {\n  color: #ffffff !important;\n}\n\n.mid-right3 {\n  float: left;\n  margin-top: -13%;\n  margin-left: 11%;\n  z-index: 3;\n}\n\n.mid-right3 ion-icon {\n  color: #000000 !important;\n}\n\n.mid-right4 {\n  float: left;\n  margin-top: 0%;\n  z-index: 3;\n}\n\n.mid-right4 ion-icon {\n  color: #000000 !important;\n}\n\n.topBar {\n  height: 50px;\n  padding: 8px;\n  width: 100%;\n  margin-top: 5%;\n  z-index: 3;\n}\n\n.topBar ion-icon {\n  padding: 0px;\n  color: #000000;\n}\n\n.topBar .locate {\n  width: auto;\n  float: right;\n  margin: 14px;\n  height: 40px;\n  background: var(--ion-color-primary);\n  border-radius: 50px;\n}\n\n.topBar .locate ion-icon {\n  font-size: 1.5em;\n  color: #fdfdfd;\n  padding: 15px;\n}\n\n.hidden {\n  -webkit-animation-direction: alternate;\n          animation-direction: alternate;\n  -webkit-animation-name: wiki;\n          animation-name: wiki;\n  -webkit-animation-duration: 0.3s;\n          animation-duration: 0.3s;\n  -webkit-animation-iteration-count: 1;\n          animation-iteration-count: 1;\n}\n\n@-webkit-keyframes left_right {\n  0% {\n    left: 75%;\n  }\n  50% {\n    left: 80%;\n  }\n  100% {\n    left: 75%;\n  }\n}\n\n@keyframes left_right {\n  0% {\n    left: 75%;\n  }\n  50% {\n    left: 80%;\n  }\n  100% {\n    left: 75%;\n  }\n}\n\n@-webkit-keyframes left_right2 {\n  0% {\n    left: 55%;\n  }\n  50% {\n    left: 50%;\n  }\n  100% {\n    left: 55%;\n  }\n}\n\n@keyframes left_right2 {\n  0% {\n    left: 55%;\n  }\n  50% {\n    left: 50%;\n  }\n  100% {\n    left: 55%;\n  }\n}\n\n@-webkit-keyframes wiki {\n  from {\n    scale: 0;\n  }\n  to {\n    scale: 1;\n  }\n}\n\n@keyframes wiki {\n  from {\n    scale: 0;\n  }\n  to {\n    scale: 1;\n  }\n}\n\n@-webkit-keyframes bump {\n  0% {\n    top: 100%;\n  }\n  100% {\n    top: 60%;\n  }\n}\n\n@keyframes bump {\n  0% {\n    top: 100%;\n  }\n  100% {\n    top: 60%;\n  }\n}\n\n@-webkit-keyframes bump2 {\n  0% {\n    top: 120%;\n  }\n  100% {\n    top: auto;\n  }\n}\n\n@keyframes bump2 {\n  0% {\n    top: 120%;\n  }\n  100% {\n    top: auto;\n  }\n}\n\nion-content [noScroll] {\n  overflow: hidden;\n}\n\nion-content .bars {\n  margin-top: 15%;\n  padding-top: 18px;\n  height: 20%;\n}\n\nion-content .bars .poiter {\n  z-index: 5;\n  margin-left: 1%;\n  background: #bd2121;\n  border-left: 1.1px solid #d8d8d8;\n  border-right: 1.1px solid #d8d8d8;\n  border-top: 1px solid #d8d8d8;\n  border-bottom: 1px solid #d8d8d8;\n}\n\nion-content .bars .bars-whereto {\n  height: auto;\n  width: 90%;\n  background: var(--ion-color-primary);\n  margin-top: -5%;\n  -webkit-animation-direction: alternate;\n          animation-direction: alternate;\n  -webkit-animation-name: bump;\n          animation-name: bump;\n  -webkit-animation-duration: 1s;\n          animation-duration: 1s;\n  margin-left: 5%;\n  padding: 2%;\n  box-shadow: 0px 0px 5px 0px rgba(3, 3, 3, 0.253);\n  z-index: 0;\n  overflow: hidden;\n  font-size: 1.5em;\n  text-align: left;\n}\n\nion-content .bars .bars-whereto .myText {\n  color: #f3f3f3;\n  font-style: bold;\n  font-size: 1em;\n  text-align: center;\n}\n\nion-content .bars .bars-whereto .myText2 {\n  color: #f3f3f3;\n  font-style: bold;\n  font-size: 1.3em;\n  text-align: center;\n}\n\nion-content .bars .bars-whereto ion-icon {\n  position: absolute;\n  font-size: 2.1em;\n  right: 4%;\n  border-radius: 30px;\n  color: #000000;\n}\n\nion-content .bars .bars-whereto2 {\n  height: 60px;\n  width: 90%;\n  background: #bd2121;\n  margin: -12% 0 0 -50px;\n  margin-left: 5%;\n  z-index: 0;\n  -webkit-animation-name: wiki;\n          animation-name: wiki;\n  -webkit-animation-duration: 0.3s;\n          animation-duration: 0.3s;\n  border: 1px solid red;\n  border-radius: 30px;\n  overflow: hidden;\n  line-height: 20px;\n  font-size: 1.5em;\n  text-align: left;\n}\n\nion-content .bars .bars-whereto2 .myText {\n  margin-left: 6%;\n  color: gray;\n}\n\nion-content .bars .bars-whereto2 ion-icon {\n  position: absolute;\n  font-size: 1.8em;\n  left: 2%;\n  color: #000000;\n  padding: 5px;\n}\n\nion-content .bars .bookingSect2 {\n  background: white;\n  padding: 2%;\n  position: fixed;\n  height: auto;\n  width: 100%;\n  border-radius: 0px;\n  margin-left: 0%;\n  top: 80%;\n  z-index: 3;\n  text-transform: none !important;\n}\n\nion-content .bars .bookingSect2 .myText {\n  color: var(--ion-color-primary);\n  font-style: bold;\n  font-size: 1em;\n  text-align: center;\n}\n\nion-content .bars .bookingSect2 .myText2 {\n  color: #f3f3f3;\n  font-style: bold;\n  font-size: 1.3em;\n  text-align: center;\n}\n\nion-content .bars .bookingSect2 ion-icon {\n  font-size: 2.1em;\n  right: 4%;\n  border-radius: 30px;\n  color: #fbb91d;\n}\n\nion-content .bars .bookingSect2 .bars-locate {\n  height: 66px;\n  width: 100%;\n  background: black;\n  color: #fbb91d !important;\n  margin-left: 0%;\n  border-radius: 30px;\n  overflow: hidden;\n  text-transform: none !important;\n  font-size: 1.2em;\n}\n\nion-content .bars .verticalLine::after {\n  width: 1%;\n  height: 50px;\n  background: green;\n  margin: auto;\n  margin-top: 0;\n  position: relative;\n  margin-left: 10%;\n}\n\nion-content .bars .bookingSect {\n  background: white;\n  padding: 2%;\n  position: fixed;\n  height: auto;\n  width: 100%;\n  border-radius: 0px;\n  margin-left: 0%;\n  top: 77%;\n  z-index: 3;\n}\n\nion-content .bars .bookingSect .bars-locate {\n  height: 66px;\n  width: 100%;\n  background: white;\n  margin-left: 0%;\n  border-radius: 30px;\n  overflow: hidden;\n  font-size: 1.4em;\n  float: left;\n}\n\nion-content .bars .bookingSect .bars-locate-pin {\n  width: auto;\n  float: right;\n  margin-top: 1%;\n  background: #fdfdfd;\n  border-radius: 30px;\n}\n\nion-content .bars .bookingSect .bars-locate-pin ion-icon {\n  font-size: 1.3em;\n  margin-left: 2%;\n  color: red;\n  padding: 5px;\n  -webkit-animation-direction: alternate;\n          animation-direction: alternate;\n  -webkit-animation-name: left_right2;\n          animation-name: left_right2;\n  -webkit-animation-duration: 0.8s;\n          animation-duration: 0.8s;\n}\n\nion-content .bars .bookingSect .bars-destinate {\n  height: 66px;\n  width: 100%;\n  background: white;\n  margin-left: 0%;\n  border-radius: 30px;\n  overflow: hidden;\n  font-size: 1.4em;\n  text-align: center;\n  float: left;\n}\n\nion-content .bars .bookingSect .bars-destinate-pin {\n  width: auto;\n  float: right;\n  margin-top: 1%;\n  background: #fdfdfd;\n  border-radius: 30px;\n}\n\nion-content .bars .bookingSect .bars-destinate-pin ion-icon {\n  font-size: 1.4em;\n  margin-left: 2%;\n  color: var(--ion-color-primary);\n  padding: 5px;\n  -webkit-animation-direction: alternate;\n          animation-direction: alternate;\n  -webkit-animation-name: left_right2;\n          animation-name: left_right2;\n  -webkit-animation-duration: 0.8s;\n          animation-duration: 0.8s;\n}\n\nion-content .reset {\n  height: 5%;\n  width: 10%;\n  background: #bd2121;\n  position: absolute;\n  margin-left: 85%;\n  top: 12%;\n  margin-top: 0%;\n  z-index: 1;\n  border: 1px solid #d8d8d8;\n  border-radius: 30px;\n  font-size: 1.5em;\n  text-align: center;\n}\n\nion-content .reset ion-icon {\n  margin-top: 4%;\n}\n\nion-content h3 {\n  color: #000000;\n  font-size: 0.7em;\n  font-weight: 300;\n}\n\nion-content h1 {\n  color: #ffffff;\n  font-size: 0.7em;\n}\n\nion-content .stack {\n  text-align: center;\n  font-size: 0.5em;\n  color: white;\n}\n\nion-content #myCol {\n  width: 0% !important;\n  padding: 0px;\n}\n\nion-content #iCol {\n  width: 50% !important;\n  padding: 0px;\n}\n\nion-content #rides {\n  z-index: 4;\n  font-size: 3em;\n  padding: 6px;\n  width: 100%;\n  background: rgba(240, 240, 240, 0.92);\n}\n\nion-content #rides_bigger {\n  border-radius: 30px;\n  z-index: 4;\n  border: 1px solid #d8d8d8;\n  height: auto;\n  overflow: visible;\n}\n\nion-content .bars-gps {\n  height: 50px;\n  width: 90%;\n  background: rgba(240, 240, 240, 0.92);\n  position: absolute;\n  margin: -50px 0 0 -50px;\n  margin-left: 5%;\n  top: 20%;\n  z-index: 1;\n  border: 1px solid var(--ion-color-primary);\n  overflow: hidden;\n  border-radius: 30px;\n  line-height: 50px;\n  font-size: 1.2em;\n  text-align: center;\n  color: var(--ion-color-primary);\n}\n\nion-content .bars-position {\n  height: 50px;\n  width: 20%;\n  background: rgba(240, 240, 240, 0.92);\n  margin: -50px 0 0 -50px;\n  margin-left: 5%;\n  top: 20%;\n  z-index: 1;\n  border: 1px solid red;\n  overflow: hidden;\n  border-radius: 30px;\n  line-height: 50px;\n  font-size: 1.2em;\n  text-align: center;\n  color: red;\n}\n\nion-content .bar {\n  position: fixed;\n  height: 35px;\n  width: 70%;\n  background: var(--ion-color-primary);\n  margin-left: 15%;\n  z-index: 4;\n  top: 60%;\n  border: 1px solid #d8d8d8;\n  border-radius: 30px;\n  font-size: 1.1em;\n  text-align: center;\n  color: white;\n}\n\nion-content .bar .icon {\n  position: absolute;\n  font-size: 1.6em;\n  right: 2%;\n  padding: 0px;\n  color: #fdfdfd;\n  -webkit-animation-name: left_right;\n          animation-name: left_right;\n  -webkit-animation-duration: 0.5s;\n          animation-duration: 0.5s;\n  -webkit-animation-iteration-count: infinite;\n          animation-iteration-count: infinite;\n  display: inline;\n}\n\nion-content .bar .textleft {\n  float: left;\n  border: 1.4px solid #fdfdfd;\n  padding: 9px;\n  z-index: 3;\n  border-radius: 30px;\n  font-size: 0.9em;\n}\n\nion-content .leftButton {\n  float: left;\n  width: 10%;\n  border-radius: 30px;\n  font-size: 1em;\n  text-align: center;\n  background: white;\n}\n\nion-content .leftButton h1 {\n  color: var(--ion-color-primary);\n}\n\nion-content .previous {\n  margin-top: 40px;\n}\n\nion-content .centerMarker {\n  z-index: 1;\n  margin-top: 41% !important;\n}\n\nion-content .centerMarker .middy {\n  position: absolute;\n  left: 45%;\n  margin-left: 5%;\n  background: #383838;\n  height: 30px;\n  top: 0%;\n  margin-top: 0%;\n  border-bottom-left-radius: 4px;\n  border-bottom-right-radius: 4px;\n  width: 0.65%;\n}\n\nion-content #nugget {\n  position: fixed;\n  z-index: 2;\n  background: var(--ion-color-primary);\n  height: 15px;\n  width: 4%;\n  left: 45%;\n  margin-left: 3.4%;\n  margin-top: -4% !important;\n  border-radius: 50px;\n}\n\n@media only screen and (min-width: 1024px) {\n  ion-content #nugget {\n    position: fixed;\n    z-index: 2;\n    background: var(--ion-color-primary);\n    height: 15px;\n    width: 4%;\n    left: 45%;\n    margin-left: 3.4%;\n    margin-top: -1% !important;\n    border-radius: 50px;\n  }\n}\n\nion-content #hider {\n  margin-left: 73%;\n  top: 3%;\n}\n\nion-content #hider .hutton {\n  padding: auto;\n  border-radius: 40px;\n  width: 57%;\n}\n\nion-content #hider .hutton ion-icon {\n  margin-left: 5% !important;\n}\n\nion-content .scroll {\n  height: 100%;\n}\n\nion-content #map {\n  width: 100%;\n  height: 100%;\n  z-index: 1;\n}\n\nion-content .default_map {\n  width: 100%;\n  height: 100%;\n  z-index: 1;\n}\n\nion-content .map_change {\n  width: 100%;\n  height: 100%;\n  z-index: 1;\n}\n\nion-content .widIcon {\n  margin-left: 85%;\n  margin-top: -10%;\n  position: absolute;\n  font-size: 15px;\n  color: black;\n  z-index: 99999999999;\n}\n\nion-content .mess_defualt {\n  width: 100%;\n  height: 100% !important;\n  z-index: 1;\n}\n\nion-content .mess2_short {\n  width: 100%;\n  height: 50% !important;\n  z-index: 1;\n}\n\nion-content .bottom-bar {\n  height: auto;\n  width: 100%;\n  background: white;\n  position: absolute;\n  margin: -50px 0 0 -50px;\n  margin-left: 0%;\n  bottom: 0%;\n  z-index: 1;\n  border-top-left-radius: 30px;\n  border-top-right-radius: 30px;\n}\n\nion-content .bottom-bar .butt {\n  color: lightgoldenrodyellow;\n  font-size: 1.2em;\n}\n\nion-content .bottom-bar .pricey {\n  border-radius: 30px;\n  background: var(--ion-color-primary);\n  margin: 2%;\n  padding: 1%;\n  font-size: 1.2em;\n  width: 33%;\n  color: white;\n  z-index: 9;\n  border: 1px solid var(--ion-color-primary);\n  margin-left: 30%;\n  margin-top: 0%;\n}\n\nion-content .bottom-bar .topey {\n  border-radius: 30px;\n  margin: 2%;\n  font-size: 0.9em;\n  color: var(--ion-color-primary);\n  margin-top: 8%;\n  margin-bottom: 8%;\n}\n\nion-content .bottom-bar button {\n  margin: auto;\n  height: 35px;\n  text-align: center;\n}\n\nion-content .bottom-bar .booker {\n  border-top: 1px solid rgba(3, 3, 3, 0.253);\n  height: auto;\n}\n\nion-content .bottom-bar .booker .gutton {\n  border-radius: 30px;\n  height: 50px;\n  color: var(--ion-color-primary);\n  font-size: 1.3em;\n}\n\nion-content .bottom-bar .tTha {\n  color: var(--ion-color-primary) !important;\n}\n\nion-content .bottom-bar .guttonq {\n  border-radius: 30px;\n  height: auto;\n  color: var(--ion-color-primary);\n  font-size: 1.3em;\n  padding: 10px;\n}\n\nion-content .bottom-bar .text {\n  padding-bottom: 0px;\n  font-size: 1.16em;\n  color: #fdfdfd;\n}\n\nion-content #myGrid {\n  overflow-x: auto !important;\n  width: auto !important;\n}\n\nion-content #myGrid2 {\n  overflow-x: auto !important;\n  width: auto !important;\n}\n\nion-content #cart-btn {\n  position: relative;\n  background-color: red;\n}\n\nion-content #cart-badge {\n  position: absolute;\n  top: 0px;\n  right: 0px;\n  background-color: red;\n}\n\nion-content .bottom-list {\n  margin-top: 90%;\n  overflow-x: auto;\n}\n\nion-content .bottom-list .my-grid {\n  width: auto;\n}\n\nion-content .bottom-list .my-grid .stuffs {\n  height: 48.5px;\n  margin-left: 34%;\n  padding: 0%;\n  width: 29.5%;\n  top: 0px;\n  vertical-align: middle;\n  border-radius: 12px;\n  font-size: 1em;\n  text-align: center;\n  z-index: 3;\n}\n\nion-content .bottom-list .my-grid .stuffs .itemPics {\n  width: 100%;\n}\n\nion-content .yellowBtn {\n  color: black !important;\n}\n\nion-content .loca {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n          align-items: center;\n  margin-top: -2%;\n}\n\nion-content .dott {\n  width: 20px;\n  height: 20px;\n}\n\nion-content .centerBtn {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n          align-items: center;\n  background: white;\n}\n\nion-content .emailbtn {\n  background-color: #000000;\n  color: #fbb91d;\n}\n\nion-content .chatIcon {\n  width: 80px;\n  height: 40px;\n  position: absolute;\n  margin-left: -20%;\n  margin-top: -27%;\n}\n\nion-content .listRides {\n  margin-right: 0;\n  margin-top: 0px;\n  margin-bottom: 0;\n  padding-left: 0;\n  padding-right: 0;\n  padding-top: 0px;\n  padding-bottom: 8px;\n  background: #fff;\n  border-top-left-radius: 30px;\n  border-top-right-radius: 30px;\n  width: 90%;\n  margin-left: 5%;\n  position: absolute;\n  bottom: 0px;\n  padding-top: 15%;\n  z-index: 333;\n}\n\nion-content .driverFound {\n  height: 56%;\n  width: 95%;\n  margin-left: 1.5%;\n  background: white;\n  position: absolute;\n  bottom: 0%;\n  z-index: 1;\n  border-top-left-radius: 30px;\n  border-top-right-radius: 30px;\n}\n\nion-content .headSection {\n  background-color: #fbb91d;\n  color: black;\n  margin-top: -6%;\n  border-top-left-radius: 27px;\n  border-top-right-radius: 30px;\n  display: inline-block;\n  width: 100%;\n  height: 45%;\n  text-align: center;\n}\n\nion-content .headSection_driver_arrive {\n  background-color: black;\n  color: #fbb91d;\n  margin-top: -6%;\n  border-top-left-radius: 27px;\n  border-top-right-radius: 30px;\n  display: inline-block;\n  width: 100%;\n  height: 45%;\n  text-align: center;\n}\n\nion-content .button_style {\n  text-align: center;\n  border-radius: 60px;\n  height: 40px;\n  width: 60%;\n  color: white;\n  margin-left: 20%;\n}\n\nion-content .moveHeader {\n  -webkit-box-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n          align-items: center;\n  align-items: center;\n  margin-top: 20px;\n  margin-left: 0px;\n  margin-right: 0px;\n}\n\nion-content #iconSize {\n  font-size: 60px !important;\n}\n\nion-content #drivericonSize {\n  font-size: 70px !important;\n}\n\nion-content #h44 {\n  font-size: 16px;\n}\n\nion-content #p4 {\n  font-size: 12px;\n}\n\nion-content #locSize {\n  font-size: 16px !important;\n}\n\nion-content .resultContainer {\n  width: 100%;\n  position: relative;\n  left: 0;\n  margin-top: 30px;\n  margin-bottom: 10px;\n  padding-right: 5%;\n  padding-left: 5%;\n  border-bottom: #bababa solid 1px;\n  background-color: white;\n}\n\nion-content .resultContainer2 {\n  width: 100%;\n  position: relative;\n  left: 0;\n  margin-top: 0px;\n  margin-bottom: 0px;\n  padding: 10px;\n  background-color: white;\n}\n\nion-content .bookImage,\nion-content .bookTitle,\nion-content .bookPrice {\n  margin-left: 20px;\n}\n\nion-content .content-wrap,\nion-content .img-wrapper {\n  display: inline-block;\n  margin-left: 0px;\n}\n\nion-content .content-wrap2 {\n  display: inline-block;\n}\n\nion-content .mapHeight {\n  height: 50%;\n  width: 100%;\n  position: relative;\n}\n\nion-content .carbottom-bar2 {\n  height: 45%;\n  width: 100%;\n  background: white;\n  position: absolute;\n  bottom: 0%;\n  z-index: 1;\n  border-top-left-radius: 30px;\n  border-top-right-radius: 30px;\n}\n\nion-content .carbottom-bar2 .butt {\n  color: lightgoldenrodyellow;\n  font-size: 1.2em;\n}\n\nion-content .carbottom-bar2 .booker {\n  border-top: 1px solid rgba(3, 3, 3, 0.253);\n  height: auto;\n}\n\nion-content .carbottom-bar2 .tTha {\n  color: var(--ion-color-primary) !important;\n}\n\nion-content .carbottom-bar2 .text {\n  padding-bottom: 0px;\n  font-size: 1.16em;\n  color: #fdfdfd;\n}\n\nion-content .bottom-bar2 {\n  height: 50%;\n  width: 95%;\n  margin-left: 1.5%;\n  background: white;\n  position: absolute;\n  bottom: 0%;\n  z-index: 1;\n  border-top-left-radius: 30px;\n  border-top-right-radius: 30px;\n}\n\nion-content .bottom-bar2 .butt {\n  color: lightgoldenrodyellow;\n  font-size: 1.2em;\n}\n\nion-content .bottom-bar2 .booker {\n  border-top: 1px solid rgba(3, 3, 3, 0.253);\n  height: auto;\n}\n\nion-content .bottom-bar2 .tTha {\n  color: var(--ion-color-primary) !important;\n}\n\nion-content .bottom-bar2 .text {\n  padding-bottom: 0px;\n  font-size: 1.16em;\n  color: #fdfdfd;\n}\n\nion-content .bottom3-container {\n  border-top-left-radius: 30px;\n  border-top-right-radius: 30px;\n  text-align: center;\n}\n\nion-content .bottom3-container .bottom-bar3 {\n  height: 100px auto;\n  width: 100%;\n  background: var(--ion-color-primary);\n  border-top-left-radius: 30px;\n  border-top-right-radius: 30px;\n  margin: 0px 0 0 0px;\n  margin-left: 0%;\n  bottom: 15%;\n  z-index: 1;\n}\n\nion-content .bottom3-container .bottom-bar3 .myImage {\n  z-index: 0;\n  position: fixed;\n  left: 0%;\n  width: 100%;\n  height: 100px;\n  background-color: #eb4200;\n}\n\nion-content .bottom3-container .bottom-bar3 .profile-pic {\n  width: 5em;\n  height: 5em;\n  border: 1px solid #3e3c3c;\n  border-radius: 50px;\n}\n\nion-content .bottom3-container .bottom-bar3 .logos {\n  padding: 0px;\n}\n\nion-content .bottom3-container .bottom-bar3 .logos ion-icon {\n  font-size: 1.1em;\n  color: #fbb91d;\n}\n\nion-content .bottom3-container .bottom-bar3 .text {\n  padding: 0px;\n  font-size: 1.4em;\n  margin-top: 6%;\n  color: #fdfdfd;\n}\n\nion-content .bottom3-container .bottom-bar3 ion-icon {\n  font-size: 2.3em;\n  color: #fbb91d;\n}\n\n#butt {\n  color: var(--ion-color-primary);\n  margin-top: 8%;\n  margin-bottom: 8%;\n  font-size: 1.3em;\n  padding: 7px;\n}\n\n#envelope {\n  height: auto;\n  width: 5em;\n  margin-left: 40%;\n  margin-top: 5%;\n}\n\n#envelop {\n  height: 5em;\n  width: 5em;\n}\n\n#buttonContainer {\n  float: right;\n}\n\n#buttonContainer button {\n  background: var(--ion-color-primary);\n  border-radius: 12px;\n  color: white;\n  margin: 6px;\n  padding: 15px;\n}\n\n#buttonContainer2 {\n  float: left;\n}\n\n#buttonContainer2 button {\n  background: var(--ion-color-primary);\n  border-radius: 12px;\n  color: white;\n  margin: auto;\n  padding: 15px;\n}\n\n#pay {\n  width: 100%;\n  height: auto;\n  margin: 1%;\n  margin-left: 2%;\n  font-size: 1.6em;\n  padding: 10px;\n}\n\n#pay ion-badge {\n  height: 70%;\n  font-size: 1.6em;\n}\n\n#pay2 {\n  width: 100%;\n  height: auto;\n  margin: 1%;\n  margin-left: 2%;\n  padding: 4px;\n}\n\n#pay2 ion-badge {\n  height: 70%;\n  font-size: 1.6em;\n}\n\n.myCentre {\n  z-index: 3;\n  margin-top: 65%;\n}\n\n#control-buttons {\n  font-size: 1.8em;\n}\n\n#control-buttons ion-icon {\n  padding: 16px;\n}\n\n#top-buttons {\n  height: auto;\n  width: 100%;\n  background: var(--ion-color-primary);\n  position: absolute;\n  margin: -50px 0 0 -50px;\n  margin-left: 0%;\n  bottom: 0%;\n  z-index: 1;\n  border-top-left-radius: 30px;\n  border-top-right-radius: 30px;\n}\n\n#top-buttons .lowbutton {\n  border-radius: 30px;\n}\n\n#top-buttons ion-icon {\n  font-size: 1em;\n  color: #030303;\n}\n\n#map-canvas {\n  width: 100%;\n  height: 100%;\n  z-index: 0;\n}\n\n#header {\n  font-size: 1.32em;\n  font-weight: 200;\n  text-align: center;\n  background: var(--ion-color-primary);\n  border-bottom-left-radius: 0px !important;\n  border-bottom-right-radius: 0px !important;\n  color: #fdfdfd;\n}\n\n#location {\n  text-align: center;\n  padding-left: 17px;\n  overflow: hidden;\n  text-transform: none !important;\n}\n\n#destination {\n  text-align: center;\n  padding-left: 17px;\n  overflow: hidden;\n  text-transform: none !important;\n}\n\n.locate_button {\n  z-index: 3;\n  float: right;\n  margin-top: 6%;\n  width: auto;\n  height: 40px;\n  margin-right: 4.5%;\n  border-radius: 60px;\n  background: white;\n}\n\n.locate_button ion-icon {\n  margin-top: -2%;\n  border-radius: 60px;\n}\n\n.request-for-ride {\n  margin-top: 0% !important;\n}\n\n.request-for-ride2 {\n  height: 200px;\n  background: white;\n}\n\n.lineHeight {\n  line-height: 1;\n}\n\n.bottum {\n  background: #fdfdfd;\n  border-radius: 60px;\n}\n\n.bottum ion-icon {\n  font-size: 1.7em;\n}\n\n#rides {\n  border-radius: 0px;\n  border: 1px solid #d8d8d8;\n}\n\n#rides ion-badge {\n  font-size: 1em;\n}\n\n.button-right {\n  position: absolute;\n  left: 75%;\n  padding: 16px;\n  top: 60%;\n}\n\n.buttonNew {\n  border: 5px solid #fbb91d;\n  width: 60%;\n  left: 50%;\n  margin: 0;\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  -webkit-transform: translate(-50%, -50%);\n          transform: translate(-50%, -50%);\n}\n\nion-footer #lower_items {\n  height: auto;\n  width: 100%;\n  background: white;\n  z-index: 15;\n}\n\nion-footer .buttons {\n  margin-top: 10% !important;\n  border-top-left-radius: 5px;\n  border-top-right-radius: 5px;\n  text-align: center;\n  margin: auto;\n  padding: auto !important;\n  background: var(--ion-color-primary);\n  z-index: 300 !important;\n  border: 0.7px solid var(--ion-color-primary);\n  color: white;\n  font-size: 17px;\n}\n\nion-footer #rides {\n  border-radius: 0px;\n  border: 1px solid #d8d8d8;\n}\n\nion-footer #rides ion-badge {\n  font-size: 1.6em;\n}\n\n#price {\n  font-size: 1em;\n  font-weight: 500;\n}\n\n#price h2 {\n  font-size: 0.63em;\n  font-weight: 600;\n  color: var(--ion-color-primary);\n}\n\n@-webkit-keyframes animatedBar {\n  0% {\n    background-position: -406px 0;\n  }\n  100% {\n    background-position: 0 0;\n  }\n}\n\n@keyframes animatedBar {\n  0% {\n    background-position: -406px 0;\n  }\n  100% {\n    background-position: 0 0;\n  }\n}\n\n.fakeItem h2, .fakeItem h3, .fakeItem h4, .fakeItem p {\n  background-color: lightgrey;\n  opacity: 0.5;\n  height: 1em;\n  margin-top: 10px;\n  position: relative;\n  background: -webkit-gradient(linear, left top, right top, from(#dddddd), to(#EEEEEE));\n  background: linear-gradient(to right, #dddddd, #EEEEEE);\n  -webkit-animation: animatedBar 700ms linear infinite;\n          animation: animatedBar 700ms linear infinite;\n}\n\n.fakeItem h2:after, .fakeItem h3:after, .fakeItem h4:after, .fakeItem p:after {\n  position: absolute;\n  display: block;\n  content: \"\";\n  right: 0;\n  bottom: 0;\n  top: 0;\n  background-color: white;\n}\n\n.fakeItem .TaxiICon2 {\n  background-color: lightgrey;\n  opacity: 0.5;\n  height: 1em;\n  margin-top: 10px;\n  position: relative;\n  background: -webkit-gradient(linear, left top, right top, from(#dddddd), to(#EEEEEE));\n  background: linear-gradient(to right, #dddddd, #EEEEEE);\n  -webkit-animation: animatedBar 700ms linear infinite;\n          animation: animatedBar 700ms linear infinite;\n}\n\n.fakeItem .TaxiICon2:after {\n  position: absolute;\n  display: block;\n  content: \"\";\n  right: 0;\n  bottom: 0;\n  top: 0;\n  background-color: white;\n}\n\n.fakeItem h2:after {\n  width: 65%;\n}\n\n.fakeItem h3:after {\n  width: 60%;\n}\n\n.fakeItem p:after {\n  width: 40%;\n}\n\n.loader {\n  position: relative;\n  top: calc(0%);\n  left: calc(50% - 32px);\n  width: 64px;\n  height: 64px;\n  border-radius: 50%;\n  -webkit-perspective: 800px;\n          perspective: 800px;\n}\n\n.inner {\n  position: absolute;\n  box-sizing: border-box;\n  width: 100%;\n  height: 100%;\n  border-radius: 50%;\n}\n\n.inner.one {\n  left: 0%;\n  top: 0%;\n  -webkit-animation: rotate-one 1s linear infinite;\n          animation: rotate-one 1s linear infinite;\n  border-bottom: 3px solid #fbb91d;\n}\n\n.inner.two {\n  right: 0%;\n  top: 0%;\n  -webkit-animation: rotate-two 1s linear infinite;\n          animation: rotate-two 1s linear infinite;\n  border-right: 3px solid #d2ae8a;\n}\n\n.inner.three {\n  right: 0%;\n  bottom: 0%;\n  -webkit-animation: rotate-three 1s linear infinite;\n          animation: rotate-three 1s linear infinite;\n  border-top: 3px solid #fbb91d;\n}\n\n@-webkit-keyframes rotate-one {\n  0% {\n    -webkit-transform: rotateX(35deg) rotateY(-45deg) rotateZ(0deg);\n            transform: rotateX(35deg) rotateY(-45deg) rotateZ(0deg);\n  }\n  100% {\n    -webkit-transform: rotateX(35deg) rotateY(-45deg) rotateZ(360deg);\n            transform: rotateX(35deg) rotateY(-45deg) rotateZ(360deg);\n  }\n}\n\n@keyframes rotate-one {\n  0% {\n    -webkit-transform: rotateX(35deg) rotateY(-45deg) rotateZ(0deg);\n            transform: rotateX(35deg) rotateY(-45deg) rotateZ(0deg);\n  }\n  100% {\n    -webkit-transform: rotateX(35deg) rotateY(-45deg) rotateZ(360deg);\n            transform: rotateX(35deg) rotateY(-45deg) rotateZ(360deg);\n  }\n}\n\n@-webkit-keyframes rotate-two {\n  0% {\n    -webkit-transform: rotateX(50deg) rotateY(10deg) rotateZ(0deg);\n            transform: rotateX(50deg) rotateY(10deg) rotateZ(0deg);\n  }\n  100% {\n    -webkit-transform: rotateX(50deg) rotateY(10deg) rotateZ(360deg);\n            transform: rotateX(50deg) rotateY(10deg) rotateZ(360deg);\n  }\n}\n\n@keyframes rotate-two {\n  0% {\n    -webkit-transform: rotateX(50deg) rotateY(10deg) rotateZ(0deg);\n            transform: rotateX(50deg) rotateY(10deg) rotateZ(0deg);\n  }\n  100% {\n    -webkit-transform: rotateX(50deg) rotateY(10deg) rotateZ(360deg);\n            transform: rotateX(50deg) rotateY(10deg) rotateZ(360deg);\n  }\n}\n\n@-webkit-keyframes rotate-three {\n  0% {\n    -webkit-transform: rotateX(35deg) rotateY(55deg) rotateZ(0deg);\n            transform: rotateX(35deg) rotateY(55deg) rotateZ(0deg);\n  }\n  100% {\n    -webkit-transform: rotateX(35deg) rotateY(55deg) rotateZ(360deg);\n            transform: rotateX(35deg) rotateY(55deg) rotateZ(360deg);\n  }\n}\n\n@keyframes rotate-three {\n  0% {\n    -webkit-transform: rotateX(35deg) rotateY(55deg) rotateZ(0deg);\n            transform: rotateX(35deg) rotateY(55deg) rotateZ(0deg);\n  }\n  100% {\n    -webkit-transform: rotateX(35deg) rotateY(55deg) rotateZ(360deg);\n            transform: rotateX(35deg) rotateY(55deg) rotateZ(360deg);\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYWhhL0lvbmljQXBwcy9DNFJpZGUtSU9TLTIwMjQvY2FsbDRyaWRlLXBhc3Nlbmdlci9zcmMvYXBwL3BhZ2VzL2hvbWUvaG9tZS5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2hvbWUvaG9tZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRywwQkFBQTtFQUNELGNBQUE7QUNDRjs7QURJQTtFQUNFLGFBQUE7QUNERjs7QURNQTtFQUNFLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLHdCQUFBO0VBQ0Esc0JBQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtFQUNBLFlBQUE7RUFDQSxRQUFBO0VBQ0EsbUNBQUE7RUFFQSwyQkFBQTtFQUNBLFlBQUE7QUNIRjs7QURNQTtFQUNFLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFFQSxzQkFBQTtFQUNBLHNDQUFBO0VBQ0EsOEJBQUE7RUFDQSxzQkFBQTtHQUFBLHFCQUFBO09BQUEsaUJBQUE7RUFDQSx5QkFBQTtBQ0pGOztBRE9BO0VBRUUsT0FBQTtFQUNFLE1BQUE7RUFFQyxRQUFBO0VBR0QseUJBQUE7RUFDQSxZQUFBO0VBQ0EsY0FBQTtFQUNBLDRCQUFBO0VBQ0EsNkJBQUE7RUFDQSxxQkFBQTtFQUVBLGtCQUFBO0FDVEo7O0FEYUE7RUFDRSxvQkFBQTtFQUNELG1CQUFBO0VBQ0Msc0NBQUE7RUFDQSw4QkFBQTtBQ1ZGOztBRFlFO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFFQSxVQUFBO0FDWEo7O0FEZ0JBO0VBQ0UsdUJBQUE7QUNiRjs7QURnQkU7RUFDQSxRQUFBO0VBRUksU0FBQTtFQUNGLFdBQUE7RUFDQSxVQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFVBQUE7RUFDQSw0QkFBQTtFQUNBLDZCQUFBO0FDZEo7O0FEa0JBO0VBQ0UsZ0JBQUE7QUNmRjs7QURrQkE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLHNCQUFBO0VBQ0Esa0JBQUE7QUNmRjs7QURrQkE7RUFDRSxZQUFBO0VBQ0EsWUFBQTtBQ2ZGOztBRG9CQTtFQUNFLFlBQUE7RUFDQSxZQUFBO0FDakJGOztBRHNCQTtFQUNFLHVCQUFBO0VBQ0EsWUFBQTtBQ25CRjs7QUR1QkE7RUFFRSxjQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQkFBQTtBQ3JCRjs7QUQ2QkE7RUFDRSw4QkFBQTtFQUNBLHVCQUFBO0FDMUJGOztBRDRCQTtFQUNFLHVCQUFBO0VBQ0EsWUFBQTtBQ3pCRjs7QUQ0QkE7RUFDRSxzQkFBQTtFQUNBLGtCQUFBO0FDekJGOztBRDRCQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0EsMkRBQUE7RUFDQSxtREFBQTtBQ3pCRjs7QUQ0QkE7RUFDRTtJQUNFLHFDQUFBO0VDekJGO0VEMkJBO0lBQ0UscURBQUE7RUN6QkY7RUQyQkE7SUFDRSxxRUFBQTtFQ3pCRjtBQUNGOztBRDRCQTtFQUNFO0lBQ0UseURBQUE7SUFDQSxpRUFBQTtFQzFCRjtFRDRCQTtJQUNFLDhEQUFBO0lBQ0Esc0VBQUE7RUMxQkY7RUQ0QkE7SUFDRSxpRUFBQTtJQUNBLHlFQUFBO0VDMUJGO0FBQ0Y7O0FENkJBO0VBQ0UseUJBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtFQUNBLDhEQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QUMzQkY7O0FENEJFO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBRUEsbUJBQUE7QUMzQko7O0FENkJJO0VBQ0UsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQzNCTjs7QUQ2Qkk7RUFDRSxrQkFBQTtFQUNBLFlBQUE7QUMzQk47O0FEOEJFO0VBQ0UsV0FBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsY0FBQTtFQUdBLG1CQUFBO0FDOUJKOztBRGdDSTtFQUNFLGtCQUFBO0VBQ0EsWUFBQTtBQzlCTjs7QURtQ0E7RUFDRSxrQkFBQTtFQUNBLDBCQUFBO0FDaENGOztBRG1DQTtFQUNFLHlCQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7RUFDQSw4REFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0Esc0NBQUE7VUFBQSw4QkFBQTtFQUNBLDRCQUFBO1VBQUEsb0JBQUE7RUFDQSxnQ0FBQTtVQUFBLHdCQUFBO0VBQ0Esb0NBQUE7VUFBQSw0QkFBQTtFQUNBLG1CQUFBO0FDaENGOztBRGlDRTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUVBLG1CQUFBO0FDaENKOztBRGtDSTtFQUVFLGFBQUE7RUFDQSxtQkFBQTtBQ2pDTjs7QURzQ0E7RUFDRSxXQUFBO0FDbkNGOztBRHNDQTtFQUNFLHNCQUFBO0VBQ0EseUJBQUE7QUNuQ0Y7O0FEdUNBO0VBRUUsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsVUFBQTtBQ3JDRjs7QUR1Q0E7RUFFRSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxVQUFBO0FDckNGOztBRHdDQTtFQUVFLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLFVBQUE7QUN0Q0Y7O0FEd0NFO0VBQ0UseUJBQUE7QUN0Q0o7O0FEMENBO0VBSUUsV0FBQTtFQUNFLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDRixVQUFBO0FDMUNGOztBRDRDRTtFQUNFLHlCQUFBO0FDMUNKOztBRDhDQTtFQUNFLFdBQUE7RUFFQSxjQUFBO0VBQ0EsVUFBQTtBQzVDRjs7QUQ4Q0U7RUFDRSx5QkFBQTtBQzVDSjs7QURnREE7RUFDRSxZQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxjQUFBO0VBQ0EsVUFBQTtBQzdDRjs7QUQrQ0U7RUFDRSxZQUFBO0VBQ0EsY0FBQTtBQzdDSjs7QUQrQ0U7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0Esb0NBQUE7RUFHQSxtQkFBQTtBQy9DSjs7QURnREk7RUFDRSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxhQUFBO0FDOUNOOztBRG1EQTtFQUNFLHNDQUFBO1VBQUEsOEJBQUE7RUFDQSw0QkFBQTtVQUFBLG9CQUFBO0VBQ0EsZ0NBQUE7VUFBQSx3QkFBQTtFQUNBLG9DQUFBO1VBQUEsNEJBQUE7QUNoREY7O0FEb0RFO0VBQ0U7SUFDRSxTQUFBO0VDakRKO0VEbURFO0lBQ0UsU0FBQTtFQ2pESjtFRG1ERTtJQUNFLFNBQUE7RUNqREo7QUFDRjs7QUR3Q0U7RUFDRTtJQUNFLFNBQUE7RUNqREo7RURtREU7SUFDRSxTQUFBO0VDakRKO0VEbURFO0lBQ0UsU0FBQTtFQ2pESjtBQUNGOztBRG1ERTtFQUNFO0lBQ0UsU0FBQTtFQ2pESjtFRG1ERTtJQUNFLFNBQUE7RUNqREo7RURtREU7SUFDRSxTQUFBO0VDakRKO0FBQ0Y7O0FEd0NFO0VBQ0U7SUFDRSxTQUFBO0VDakRKO0VEbURFO0lBQ0UsU0FBQTtFQ2pESjtFRG1ERTtJQUNFLFNBQUE7RUNqREo7QUFDRjs7QURtREU7RUFDRTtJQUNFLFFBQUE7RUNqREo7RURtREU7SUFDRSxRQUFBO0VDakRKO0FBQ0Y7O0FEMkNFO0VBQ0U7SUFDRSxRQUFBO0VDakRKO0VEbURFO0lBQ0UsUUFBQTtFQ2pESjtBQUNGOztBRG1ERTtFQUNFO0lBQ0UsU0FBQTtFQ2pESjtFRG9ERTtJQUNFLFFBQUE7RUNsREo7QUFDRjs7QUQyQ0U7RUFDRTtJQUNFLFNBQUE7RUNqREo7RURvREU7SUFDRSxRQUFBO0VDbERKO0FBQ0Y7O0FEb0RFO0VBQ0U7SUFDRSxTQUFBO0VDbERKO0VEcURFO0lBQ0UsU0FBQTtFQ25ESjtBQUNGOztBRDRDRTtFQUNFO0lBQ0UsU0FBQTtFQ2xESjtFRHFERTtJQUNFLFNBQUE7RUNuREo7QUFDRjs7QURxREU7RUFDRSxnQkFBQTtBQ25ESjs7QURxREU7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0FDbkRKOztBRG9ESTtFQUNFLFVBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQ0FBQTtFQUNBLGlDQUFBO0VBQ0EsNkJBQUE7RUFDQSxnQ0FBQTtBQ2xETjs7QURvREk7RUFDRSxZQUFBO0VBQ0EsVUFBQTtFQUNBLG9DQUFBO0VBQ0EsZUFBQTtFQUNBLHNDQUFBO1VBQUEsOEJBQUE7RUFDQSw0QkFBQTtVQUFBLG9CQUFBO0VBQ0EsOEJBQUE7VUFBQSxzQkFBQTtFQUVBLGVBQUE7RUFDQSxXQUFBO0VBQ0EsZ0RBQUE7RUFDQSxVQUFBO0VBR0EsZ0JBQUE7RUFFQSxnQkFBQTtFQUNBLGdCQUFBO0FDdEROOztBRHVETTtFQUVFLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtBQ3REUjs7QUR3RE07RUFFRSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FDdkRSOztBRHlETTtFQUNFLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxTQUFBO0VBRUEsbUJBQUE7RUFDQSxjQUFBO0FDeERSOztBRDJESTtFQUNFLFlBQUE7RUFDQSxVQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtFQUNBLGVBQUE7RUFFQSxVQUFBO0VBQ0EsNEJBQUE7VUFBQSxvQkFBQTtFQUNBLGdDQUFBO1VBQUEsd0JBQUE7RUFDQSxxQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7QUMxRE47O0FEMkRNO0VBQ0UsZUFBQTtFQUNBLFdBQUE7QUN6RFI7O0FEMkRNO0VBQ0Usa0JBQUE7RUFDQSxnQkFBQTtFQUNBLFFBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtBQ3pEUjs7QUQ0REk7RUFHRSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxRQUFBO0VBQ0EsVUFBQTtFQUNBLCtCQUFBO0FDNUROOztBRDJFTTtFQUVFLCtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7QUMxRVI7O0FENEVNO0VBRUUsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBQzNFUjs7QUQ2RU07RUFFRSxnQkFBQTtFQUNBLFNBQUE7RUFFQSxtQkFBQTtFQUNBLGNBQUE7QUM3RVI7O0FEZ0ZNO0VBQ0UsWUFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLHlCQUFBO0VBQ0EsZUFBQTtFQUVBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSwrQkFBQTtFQUNBLGdCQUFBO0FDL0VSOztBRDhGSTtFQUNFLFNBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUM1Rk47O0FEOEZJO0VBWUUsaUJBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsUUFBQTtFQUNBLFVBQUE7QUN2R047O0FEeUdNO0VBQ0UsWUFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUVBLGVBQUE7RUFFQSxtQkFBQTtFQUNBLGdCQUFBO0VBRUEsZ0JBQUE7RUFDQSxXQUFBO0FDMUdSOztBRDRHTTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBRUEsbUJBQUE7QUMzR1I7O0FENEdRO0VBQ0UsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsVUFBQTtFQUNBLFlBQUE7RUFDQSxzQ0FBQTtVQUFBLDhCQUFBO0VBQ0EsbUNBQUE7VUFBQSwyQkFBQTtFQUNBLGdDQUFBO1VBQUEsd0JBQUE7QUMxR1Y7O0FENkdNO0VBQ0UsWUFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUdBLGVBQUE7RUFJQSxtQkFBQTtFQUNBLGdCQUFBO0VBRUEsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7QUNqSFI7O0FEbUhNO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7RUFFQSxtQkFBQTtBQ2xIUjs7QURtSFE7RUFDRSxnQkFBQTtFQUNBLGVBQUE7RUFDQSwrQkFBQTtFQUNBLFlBQUE7RUFDQSxzQ0FBQTtVQUFBLDhCQUFBO0VBQ0EsbUNBQUE7VUFBQSwyQkFBQTtFQUNBLGdDQUFBO1VBQUEsd0JBQUE7QUNqSFY7O0FEc0hFO0VBQ0UsVUFBQTtFQUNBLFVBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxRQUFBO0VBQ0EsY0FBQTtFQUNBLFVBQUE7RUFDQSx5QkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBQ3BISjs7QURxSEk7RUFDRSxjQUFBO0FDbkhOOztBRHNIRTtFQUNFLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0FDcEhKOztBRHNIRTtFQUNFLGNBQUE7RUFDQSxnQkFBQTtBQ3BISjs7QURzSEU7RUFDRSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtBQ3BISjs7QUQrSEU7RUFDRSxvQkFBQTtFQUNBLFlBQUE7QUM3SEo7O0FEK0hFO0VBQ0UscUJBQUE7RUFDQSxZQUFBO0FDN0hKOztBRCtIRTtFQUVFLFVBQUE7RUFJQSxjQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxxQ0FBQTtBQ2pJSjs7QURtSUU7RUFDRSxtQkFBQTtFQUNBLFVBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtBQ2pJSjs7QURtSUU7RUFDRSxZQUFBO0VBQ0EsVUFBQTtFQUNBLHFDQUFBO0VBQ0Esa0JBQUE7RUFDQSx1QkFBQTtFQUNBLGVBQUE7RUFDQSxRQUFBO0VBQ0EsVUFBQTtFQUNBLDBDQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLCtCQUFBO0FDaklKOztBRG1JRTtFQUNFLFlBQUE7RUFDQSxVQUFBO0VBQ0EscUNBQUE7RUFDQSx1QkFBQTtFQUNBLGVBQUE7RUFDQSxRQUFBO0VBQ0EsVUFBQTtFQUNBLHFCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7QUNqSUo7O0FEb0lFO0VBQ0UsZUFBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0VBQ0Esb0NBQUE7RUFDQSxnQkFBQTtFQUNBLFVBQUE7RUFDQSxRQUFBO0VBQ0EseUJBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0FDbElKOztBRG1JSTtFQUNFLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxTQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7RUFDQSxrQ0FBQTtVQUFBLDBCQUFBO0VBQ0EsZ0NBQUE7VUFBQSx3QkFBQTtFQUNBLDJDQUFBO1VBQUEsbUNBQUE7RUFDQSxlQUFBO0FDaklOOztBRG1JSTtFQUNFLFdBQUE7RUFDQSwyQkFBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtBQ2pJTjs7QURvSUU7RUFDRSxXQUFBO0VBQ0EsVUFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7QUNsSUo7O0FEbUlJO0VBQ0UsK0JBQUE7QUNqSU47O0FEb0lFO0VBQ0UsZ0JBQUE7QUNsSUo7O0FEb0lFO0VBQ0UsVUFBQTtFQUNBLDBCQUFBO0FDbElKOztBRG1JSTtFQUNFLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxPQUFBO0VBQ0EsY0FBQTtFQUNBLDhCQUFBO0VBQ0EsK0JBQUE7RUFDQSxZQUFBO0FDaklOOztBRHVJRTtFQUNFLGVBQUE7RUFDQSxVQUFBO0VBQ0Esb0NBQUE7RUFDQSxZQUFBO0VBQ0EsU0FBQTtFQUNBLFNBQUE7RUFDQSxpQkFBQTtFQUNBLDBCQUFBO0VBRUEsbUJBQUE7QUN0SUo7O0FEd0lFO0VBQ0U7SUFDRSxlQUFBO0lBQ0EsVUFBQTtJQUNBLG9DQUFBO0lBQ0EsWUFBQTtJQUNBLFNBQUE7SUFDQSxTQUFBO0lBQ0EsaUJBQUE7SUFDQSwwQkFBQTtJQUVBLG1CQUFBO0VDdklKO0FBQ0Y7O0FEeUlFO0VBQ0UsZ0JBQUE7RUFDQSxPQUFBO0FDdklKOztBRHdJSTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtFQUNBLFVBQUE7QUN0SU47O0FEd0lNO0VBQ0UsMEJBQUE7QUN0SVI7O0FEMElFO0VBQ0UsWUFBQTtBQ3hJSjs7QUQ0SUU7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUVBLFVBQUE7QUMzSUo7O0FEK0lFO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0FDN0lGOztBRGlKQTtFQUNFLFdBQUE7RUFDRSxZQUFBO0VBQ0EsVUFBQTtBQy9JSjs7QURtSkE7RUFDRSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0Ysa0JBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtFQUNBLG9CQUFBO0FDakpBOztBRHNKQTtFQUNDLFdBQUE7RUFDQyx1QkFBQTtFQUNBLFVBQUE7QUNwSkY7O0FEdUpBO0VBRUcsV0FBQTtFQUNELHNCQUFBO0VBRUUsVUFBQTtBQ3ZKSjs7QUQ2SkU7RUFDRSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFFQSx1QkFBQTtFQUNBLGVBQUE7RUFDQSxVQUFBO0VBQ0EsVUFBQTtFQUNBLDRCQUFBO0VBQ0EsNkJBQUE7QUM1Sko7O0FEK0pJO0VBQ0UsMkJBQUE7RUFDQSxnQkFBQTtBQzdKTjs7QURvS0k7RUFDRSxtQkFBQTtFQUNBLG9DQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLDBDQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0FDbEtOOztBRG9LSTtFQUNFLG1CQUFBO0VBRUEsVUFBQTtFQUVBLGdCQUFBO0VBRUEsK0JBQUE7RUFFQSxjQUFBO0VBQ0EsaUJBQUE7QUN0S047O0FEd0tJO0VBQ0UsWUFBQTtFQUVBLFlBQUE7RUFDQSxrQkFBQTtBQ3ZLTjs7QUR5S0k7RUFDRSwwQ0FBQTtFQUVBLFlBQUE7QUN4S047O0FEMEtNO0VBRUUsbUJBQUE7RUFDQSxZQUFBO0VBRUEsK0JBQUE7RUFJQSxnQkFBQTtBQzdLUjs7QURnTEk7RUFDRSwwQ0FBQTtBQzlLTjs7QURnTEk7RUFFRSxtQkFBQTtFQUNBLFlBQUE7RUFNQSwrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsYUFBQTtBQ3BMTjs7QURzTEk7RUFDRSxtQkFBQTtFQUVBLGlCQUFBO0VBQ0EsY0FBQTtBQ3JMTjs7QUR5TEU7RUFDRSwyQkFBQTtFQUNBLHNCQUFBO0FDdkxKOztBRHlMRTtFQUNFLDJCQUFBO0VBQ0Esc0JBQUE7QUN2TEo7O0FEeUxFO0VBQ0Usa0JBQUE7RUFDQSxxQkFBQTtBQ3ZMSjs7QUR5TEU7RUFDRSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxVQUFBO0VBQ0EscUJBQUE7QUN2TEo7O0FEeUxFO0VBQ0UsZUFBQTtFQUNBLGdCQUFBO0FDdkxKOztBRHdMSTtFQUNFLFdBQUE7QUN0TE47O0FEdUxNO0VBQ0UsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxRQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUdBLFVBQUE7QUN2TFI7O0FEd0xRO0VBQ0UsV0FBQTtBQ3RMVjs7QUQ0TEU7RUFDRSx1QkFBQTtBQzFMSjs7QUQ2TEU7RUFDRSxvQkFBQTtFQUFBLGFBQUE7RUFDQSx3QkFBQTtVQUFBLHVCQUFBO0VBQ0EseUJBQUE7VUFBQSxtQkFBQTtFQUNBLGVBQUE7QUMzTEo7O0FEOExFO0VBQ0UsV0FBQTtFQUNBLFlBQUE7QUM1TEo7O0FEK0xFO0VBQ0Usb0JBQUE7RUFBQSxhQUFBO0VBQ0Esd0JBQUE7VUFBQSx1QkFBQTtFQUNBLHlCQUFBO1VBQUEsbUJBQUE7RUFDQSxpQkFBQTtBQzdMSjs7QURpTUU7RUFDRSx5QkFBQTtFQUNBLGNBQUE7QUMvTEo7O0FEa01FO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7QUNoTUo7O0FEbU1FO0VBU0UsZUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLDRCQUFBO0VBQ0EsNkJBQUE7RUFDQSxVQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtBQ3pNSjs7QUQ0TUU7RUFDRSxXQUFBO0VBQ0EsVUFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxVQUFBO0VBQ0EsNEJBQUE7RUFDQSw2QkFBQTtBQzFNSjs7QUQrTUU7RUFDRSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsNEJBQUE7RUFDQSw2QkFBQTtFQUNBLHFCQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtBQzdNSjs7QURpTkc7RUFDQyx1QkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsNEJBQUE7RUFDQSw2QkFBQTtFQUNBLHFCQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtBQy9NSjs7QURtTkE7RUFDRSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QUNqTkY7O0FEcU5FO0VBQ0Usd0JBQUE7VUFBQSx1QkFBQTtFQUNBLHlCQUFBO1VBQUEsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtBQ25OSjs7QURzTkU7RUFDRSwwQkFBQTtBQ3BOSjs7QUR1TkU7RUFDRSwwQkFBQTtBQ3JOSjs7QUR3TkU7RUFDRSxlQUFBO0FDdE5KOztBRHlORTtFQUNFLGVBQUE7QUN2Tko7O0FEeU5FO0VBQ0UsMEJBQUE7QUN2Tko7O0FEME5FO0VBQ0UsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsT0FBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFFQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0NBQUE7RUFDRyx1QkFBQTtBQ3pOUDs7QUQyTkU7RUFDRSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxPQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0FDek5KOztBRDRORTs7O0VBR0UsaUJBQUE7QUMxTko7O0FENk5FOztFQUVFLHFCQUFBO0VBQ0EsZ0JBQUE7QUMzTko7O0FEOE5FO0VBQ0UscUJBQUE7QUM1Tko7O0FEK05FO0VBQ0UsV0FBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtBQzdOSjs7QURnT0U7RUFDRSxXQUFBO0VBQ0EsV0FBQTtFQUVBLGlCQUFBO0VBQ0Esa0JBQUE7RUFFQSxVQUFBO0VBQ0EsVUFBQTtFQUNBLDRCQUFBO0VBQ0EsNkJBQUE7QUNoT0o7O0FEbU9JO0VBQ0UsMkJBQUE7RUFDQSxnQkFBQTtBQ2pPTjs7QURvT0k7RUFDRSwwQ0FBQTtFQUVBLFlBQUE7QUNuT047O0FEcU9JO0VBQ0UsMENBQUE7QUNuT047O0FEc09JO0VBQ0UsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7QUNwT047O0FEd09FO0VBQ0UsV0FBQTtFQUNBLFVBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFFQSxVQUFBO0VBQ0EsVUFBQTtFQUNBLDRCQUFBO0VBQ0EsNkJBQUE7QUN2T0o7O0FEME9JO0VBQ0UsMkJBQUE7RUFDQSxnQkFBQTtBQ3hPTjs7QUQyT0k7RUFDRSwwQ0FBQTtFQUVBLFlBQUE7QUMxT047O0FENE9JO0VBQ0UsMENBQUE7QUMxT047O0FENk9JO0VBQ0UsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7QUMzT047O0FEOE9FO0VBQ0UsNEJBQUE7RUFDQSw2QkFBQTtFQUNBLGtCQUFBO0FDNU9KOztBRDZPSTtFQUNFLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLG9DQUFBO0VBQ0EsNEJBQUE7RUFDQSw2QkFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtFQUdBLFdBQUE7RUFDQSxVQUFBO0FDN09OOztBRCtPTTtFQUNFLFVBQUE7RUFDQSxlQUFBO0VBQ0EsUUFBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0VBQ0EseUJBQUE7QUM3T1I7O0FEK09NO0VBQ0UsVUFBQTtFQUNBLFdBQUE7RUFDQSx5QkFBQTtFQUNBLG1CQUFBO0FDN09SOztBRCtPTTtFQUNFLFlBQUE7QUM3T1I7O0FEOE9RO0VBQ0UsZ0JBQUE7RUFDQSxjQUFBO0FDNU9WOztBRCtPTTtFQUNFLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxjQUFBO0FDN09SOztBRCtPTTtFQUNFLGdCQUFBO0VBQ0EsY0FBQTtBQzdPUjs7QURtUEE7RUFDRSwrQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtBQ2hQRjs7QURtUEE7RUFDRSxZQUFBO0VBQ0EsVUFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtBQ2hQRjs7QURtUEE7RUFDRSxXQUFBO0VBQ0EsVUFBQTtBQ2hQRjs7QURtUEE7RUFDRSxZQUFBO0FDaFBGOztBRGlQRTtFQUVFLG9DQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUVBLGFBQUE7QUNqUEo7O0FEc1BBO0VBQ0UsV0FBQTtBQ25QRjs7QURvUEU7RUFFRSxvQ0FBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFFQSxhQUFBO0FDcFBKOztBRHlQQTtFQUdFLFdBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUlBLGFBQUE7QUMzUEY7O0FENFBFO0VBQ0UsV0FBQTtFQUNBLGdCQUFBO0FDMVBKOztBRDhQQTtFQUdFLFdBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLGVBQUE7RUFJQSxZQUFBO0FDaFFGOztBRGlRRTtFQUNFLFdBQUE7RUFDQSxnQkFBQTtBQy9QSjs7QURtUUE7RUFDRSxVQUFBO0VBQ0EsZUFBQTtBQ2hRRjs7QURtUUE7RUFHRSxnQkFBQTtBQ2xRRjs7QURtUUU7RUFDRSxhQUFBO0FDalFKOztBRHFRQTtFQUNFLFlBQUE7RUFDQSxXQUFBO0VBQ0Esb0NBQUE7RUFDQSxrQkFBQTtFQUVBLHVCQUFBO0VBQ0EsZUFBQTtFQUNBLFVBQUE7RUFDQSxVQUFBO0VBQ0EsNEJBQUE7RUFDQSw2QkFBQTtBQ25RRjs7QURzUUU7RUFDRSxtQkFBQTtBQ3BRSjs7QUR1UUU7RUFDRSxjQUFBO0VBQ0EsY0FBQTtBQ3JRSjs7QUR5UUE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUVBLFVBQUE7QUN2UUY7O0FEMFFBO0VBQ0UsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0Esb0NBQUE7RUFDQSx5Q0FBQTtFQUNBLDBDQUFBO0VBQ0EsY0FBQTtBQ3ZRRjs7QUQwUUE7RUFDRSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSwrQkFBQTtBQ3ZRRjs7QUQyUUE7RUFDRSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSwrQkFBQTtBQ3hRRjs7QUQ0UUE7RUFDRSxVQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUVBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtBQzFRRjs7QUQ0UUU7RUFFRSxlQUFBO0VBQ0EsbUJBQUE7QUMzUUo7O0FEZ1JBO0VBQ0UseUJBQUE7QUM3UUY7O0FEZ1JBO0VBQ0UsYUFBQTtFQUNBLGlCQUFBO0FDN1FGOztBRGdSQTtFQUNFLGNBQUE7QUM3UUY7O0FEZ1JBO0VBQ0UsbUJBQUE7RUFFQSxtQkFBQTtBQzlRRjs7QUQrUUU7RUFDRSxnQkFBQTtBQzdRSjs7QURpUkE7RUFDRSxrQkFBQTtFQUNBLHlCQUFBO0FDOVFGOztBRCtRRTtFQUNFLGNBQUE7QUM3UUo7O0FEaVJBO0VBQ0Usa0JBQUE7RUFDQSxTQUFBO0VBQ0EsYUFBQTtFQUNBLFFBQUE7QUM5UUY7O0FEaVJBO0VBQ0UseUJBQUE7RUFDQSxVQUFBO0VBQ0EsU0FBQTtFQUNBLFNBQUE7RUFDQSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0VBRUEsd0NBQUE7VUFBQSxnQ0FBQTtBQzlRRjs7QURrUkU7RUFDRSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtBQy9RSjs7QURpUkU7RUFDRSwwQkFBQTtFQUNBLDJCQUFBO0VBQ0EsNEJBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSx3QkFBQTtFQUNBLG9DQUFBO0VBQ0EsdUJBQUE7RUFDQSw0Q0FBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0FDL1FKOztBRGlSRTtFQUNFLGtCQUFBO0VBQ0EseUJBQUE7QUMvUUo7O0FEZ1JJO0VBQ0UsZ0JBQUE7QUM5UU47O0FEbVJBO0VBQ0UsY0FBQTtFQUNBLGdCQUFBO0FDaFJGOztBRGlSRTtFQUNFLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSwrQkFBQTtBQy9RSjs7QUR1Ukk7RUFDSTtJQUNJLDZCQUFBO0VDcFJWO0VEc1JNO0lBQ0ksd0JBQUE7RUNwUlY7QUFDRjs7QUQ4UUk7RUFDSTtJQUNJLDZCQUFBO0VDcFJWO0VEc1JNO0lBQ0ksd0JBQUE7RUNwUlY7QUFDRjs7QUR1Uk07RUFDRSwyQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLHFGQUFBO0VBQUEsdURBQUE7RUFDQSxvREFBQTtVQUFBLDRDQUFBO0FDclJSOztBRHVSUTtFQUNFLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLFdBQUE7RUFDQSxRQUFBO0VBQ0EsU0FBQTtFQUNBLE1BQUE7RUFDQSx1QkFBQTtBQ3JSVjs7QUR5Uk07RUFDSSwyQkFBQTtFQUNGLFlBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLHFGQUFBO0VBQUEsdURBQUE7RUFDQSxvREFBQTtVQUFBLDRDQUFBO0FDdlJSOztBRHlSUTtFQUNFLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLFdBQUE7RUFDQSxRQUFBO0VBQ0EsU0FBQTtFQUNBLE1BQUE7RUFDQSx1QkFBQTtBQ3ZSVjs7QUQyUk07RUFDRSxVQUFBO0FDelJSOztBRDRSTTtFQUNFLFVBQUE7QUMxUlI7O0FENlJNO0VBQ0UsVUFBQTtBQzNSUjs7QURnU0E7RUFDRSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSwwQkFBQTtVQUFBLGtCQUFBO0FDN1JGOztBRGdTQTtFQUNFLGtCQUFBO0VBQ0Esc0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0FDN1JGOztBRGdTQTtFQUNFLFFBQUE7RUFDQSxPQUFBO0VBQ0EsZ0RBQUE7VUFBQSx3Q0FBQTtFQUNBLGdDQUFBO0FDN1JGOztBRGdTQTtFQUNFLFNBQUE7RUFDQSxPQUFBO0VBQ0EsZ0RBQUE7VUFBQSx3Q0FBQTtFQUNBLCtCQUFBO0FDN1JGOztBRGdTQTtFQUNFLFNBQUE7RUFDQSxVQUFBO0VBQ0Esa0RBQUE7VUFBQSwwQ0FBQTtFQUNBLDZCQUFBO0FDN1JGOztBRGtTQTtFQUNFO0lBQ0UsK0RBQUE7WUFBQSx1REFBQTtFQy9SRjtFRGlTQTtJQUNFLGlFQUFBO1lBQUEseURBQUE7RUMvUkY7QUFDRjs7QUR5UkE7RUFDRTtJQUNFLCtEQUFBO1lBQUEsdURBQUE7RUMvUkY7RURpU0E7SUFDRSxpRUFBQTtZQUFBLHlEQUFBO0VDL1JGO0FBQ0Y7O0FEa1NBO0VBQ0U7SUFDRSw4REFBQTtZQUFBLHNEQUFBO0VDaFNGO0VEa1NBO0lBQ0UsZ0VBQUE7WUFBQSx3REFBQTtFQ2hTRjtBQUNGOztBRDBSQTtFQUNFO0lBQ0UsOERBQUE7WUFBQSxzREFBQTtFQ2hTRjtFRGtTQTtJQUNFLGdFQUFBO1lBQUEsd0RBQUE7RUNoU0Y7QUFDRjs7QURtU0E7RUFDRTtJQUNFLDhEQUFBO1lBQUEsc0RBQUE7RUNqU0Y7RURtU0E7SUFDRSxnRUFBQTtZQUFBLHdEQUFBO0VDalNGO0FBQ0Y7O0FEMlJBO0VBQ0U7SUFDRSw4REFBQTtZQUFBLHNEQUFBO0VDalNGO0VEbVNBO0lBQ0UsZ0VBQUE7WUFBQSx3REFBQTtFQ2pTRjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvaG9tZS9ob21lLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50IHtcbiAgIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXR0XCI7XG4gIG1hcmdpbi10b3A6IDAlO1xufVxuXG5cbi8vTkV3IFRyYW5zaXRpb25cbmxhYmVsID4gaW5wdXQgeyBcbiAgZGlzcGxheTogbm9uZTtcbiAgICAvLyBtYXJnaW4tdG9wOiAyNSU7XG4gICAgLy8gbWFyZ2luLWxlZnQ6IDQzJTtcbn1cblxuI2J1dHRvbjAgeyAgXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGluZS1oZWlnaHQ6IDQ1cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgaGVpZ2h0OiA1MHB4O1xuICB3aWR0aDogNTBweDsgIFxuICBib3JkZXI6IDJweCBkYXNoZWQgYmxhY2s7XG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gIGxlZnQ6IDA7XG4gIHJpZ2h0OiAwO1xuICBtYXJnaW46IGF1dG87XG4gIHRvcDogNTAlO1xuICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNTAlKTtcbiAgLW1zLXRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNTAlKTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xuICBvcGFjaXR5OiAwLjU7XG59XG5cbmxhYmVsID4gaW5wdXQgfiAuYnV0dG9uIHsgXG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsaW5lLWhlaWdodDogNDVweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBoZWlnaHQ6IDUwcHg7XG4gIHdpZHRoOiAxMDAlOyAgXG4gIC8vIGJvcmRlcjogMnB4IHNvbGlkIGNyaW1zb247XG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIDFzIGVhc2UtaW4tb3V0O1xuICB0cmFuc2l0aW9uOiBhbGwgMXMgZWFzZS1pbi1vdXQ7XG4gIHVzZXItc2VsZWN0OiBub25lO1xuICAtd2Via2l0LXVzZXItc2VsZWN0OiBub25lO1xufVxuXG5sYWJlbCA+IGlucHV0IH4gI2J1dHRvbjEgeyBcbiBcbiAgbGVmdDogMDtcbiAgICB0b3A6IDA7XG5cbiAgICAgdG9wOiA1MCU7XG4gIFxuXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2YyZjJmMjtcbiAgICBjb2xvcjogYmxhY2s7XG4gICAgbWFyZ2luLXRvcDogMCU7XG4gICAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMjdweDtcbiAgICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMzBweDtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuXG5sYWJlbCA+IGlucHV0OmNoZWNrZWQgfiAjYnV0dG9uMSB7IFxuICBsZWZ0OiBjYWxjKDAlIC0gMHB4KTtcbiB0b3A6IGNhbGMoNzAlIC0gMCUpO1xuICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAxcyBlYXNlLWluLW91dDtcbiAgdHJhbnNpdGlvbjogYWxsIDFzIGVhc2UtaW4tb3V0O1xuXG4gICNtYXAge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICAvL3Bvc2l0aW9uOiBmaXhlZDtcbiAgICB6LWluZGV4OiAxO1xuICB9XG59XG5cblxuLm1haW4tY29ue1xuICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbn1cblxuICAuZHJpdmVyRm91bmROZXd7XG4gIGxlZnQ6IDAlO1xuICAgIC8vIHRvcDogMTAwJTtcbiAgICAgIHRvcDogMTQ3JTtcbiAgICBoZWlnaHQ6IDUwJTtcbiAgICB3aWR0aDogOTUlO1xuICAgIG1hcmdpbi1sZWZ0OiAxLjUlO1xuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBib3R0b206IDAlO1xuICAgIHotaW5kZXg6IDE7XG4gICAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMzBweDtcbiAgICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMzBweDtcbiAgfVxuLy9FTkRFXG5cbi5teS1jdXN0b20tY2xhc3MgLm1vZGFsLXdyYXBwZXIge1xuICBiYWNrZ3JvdW5kOiAjMjIyO1xufVxuXG4uVGF4aUlDb24yIHtcbiAgd2lkdGg6IDcwcHg7XG4gIGhlaWdodDogNzBweDtcbiAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uVGF4aUlDb24ge1xuICB3aWR0aDogMTAwcHg7XG4gIGhlaWdodDogMzVweDtcbiAgLy8gdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbiAgLy8gdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uVGF4aUlDb21maXJtIHtcbiAgd2lkdGg6IDEwMHB4O1xuICBoZWlnaHQ6IDM1cHg7XG4gIC8vIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG4gIC8vIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmxpc3RSb3c6aG92ZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcbiAgY29sb3I6IHdoaXRlO1xuICAvLyAtLWJhY2tncm91bmQtaG92ZXI6IGJsdWU7XG59XG5cbi5hbGlnbi1jb2wge1xuICAvLyB0ZXh0LWFsaWduOiBsZWZ0O1xuICBsaW5lLWhlaWdodDogMTtcbiAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAvLyBtYXJnaW4tbGVmdDogMTZweDtcbiAgLy8gbWFyZ2luLWJvdHRvbTogMTZweDtcbn1cblxuLmxpc3RSb3cge1xuICAvLyBoZWlnaHQ6IDYwcHg7XG59XG4uaXRlbS1yYWRpby1jaGVja2VkIHtcbiAgLS1iYWNrZ3JvdW5kOiBibGFjayAhaW1wb3J0YW50O1xuICBjb2xvcjogd2hpdGUgIWltcG9ydGFudDtcbn1cbi5hY3RpdmUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcbiAgY29sb3I6IHdoaXRlO1xufVxuXG4uaW1nU3R5bGUge1xuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5zcGlubmVycyB7XG4gIHdpZHRoOiA0MHB4O1xuICBoZWlnaHQ6IDQwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHJnYigyNTUsIDI1NSwgMjU1KTtcbiAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgbWFyZ2luOiAxNHB4IGF1dG87XG4gIC13ZWJraXQtYW5pbWF0aW9uOiBzay1yb3RhdGVwbGFuZSAxLjJzIGluZmluaXRlIGVhc2UtaW4tb3V0O1xuICBhbmltYXRpb246IHNrLXJvdGF0ZXBsYW5lIDEuMnMgaW5maW5pdGUgZWFzZS1pbi1vdXQ7XG59XG5cbkAtd2Via2l0LWtleWZyYW1lcyBzay1yb3RhdGVwbGFuZSB7XG4gIDAlIHtcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogcGVyc3BlY3RpdmUoMTIwcHgpO1xuICB9XG4gIDUwJSB7XG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHBlcnNwZWN0aXZlKDEyMHB4KSByb3RhdGVZKDE4MGRlZyk7XG4gIH1cbiAgMTAwJSB7XG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHBlcnNwZWN0aXZlKDEyMHB4KSByb3RhdGVZKDE4MGRlZykgcm90YXRlWCgxODBkZWcpO1xuICB9XG59XG5cbkBrZXlmcmFtZXMgc2stcm90YXRlcGxhbmUge1xuICAwJSB7XG4gICAgdHJhbnNmb3JtOiBwZXJzcGVjdGl2ZSgxMjBweCkgcm90YXRlWCgwZGVnKSByb3RhdGVZKDBkZWcpO1xuICAgIC13ZWJraXQtdHJhbnNmb3JtOiBwZXJzcGVjdGl2ZSgxMjBweCkgcm90YXRlWCgwZGVnKSByb3RhdGVZKDBkZWcpO1xuICB9XG4gIDUwJSB7XG4gICAgdHJhbnNmb3JtOiBwZXJzcGVjdGl2ZSgxMjBweCkgcm90YXRlWCgtMTgwLjFkZWcpIHJvdGF0ZVkoMGRlZyk7XG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHBlcnNwZWN0aXZlKDEyMHB4KSByb3RhdGVYKC0xODAuMWRlZykgcm90YXRlWSgwZGVnKTtcbiAgfVxuICAxMDAlIHtcbiAgICB0cmFuc2Zvcm06IHBlcnNwZWN0aXZlKDEyMHB4KSByb3RhdGVYKC0xODBkZWcpIHJvdGF0ZVkoLTE3OS45ZGVnKTtcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogcGVyc3BlY3RpdmUoMTIwcHgpIHJvdGF0ZVgoLTE4MGRlZykgcm90YXRlWSgtMTc5LjlkZWcpO1xuICB9XG59XG5cbi5zY3JvbGxpbmctd3JhcHBlciB7XG4gIG92ZXJmbG93OiBhdXRvICFpbXBvcnRhbnQ7XG4gIHdpZHRoOiBhdXRvO1xuICBkaXNwbGF5OiBncmlkO1xuICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IGF1dG8gYXV0byBhdXRvIGF1dG8gYXV0byBhdXRvIGF1dG8gYXV0bztcbiAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgcGFkZGluZzogMHB4O1xuICBtYXJnaW4tdG9wOiAyMnB4O1xuICAjc3RmIHtcbiAgICBtYXJnaW46IDVweDtcbiAgICB3aWR0aDogMTAwcHg7XG4gICAgaGVpZ2h0OiAxMDBweDtcbiAgICBmb250LXNpemU6IDFlbTtcbiAgICBiYWNrZ3JvdW5kOiByZ2IoMjE2LCAyMTYsIDIxNik7XG4gICAgLy9tYXJnaW4tbGVmdDogMzBweDtcbiAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xuICAgIC8vIGJveC1zaGFkb3c6IDBweCAwcHggNXB4IDBweCByZ2JhKDMsIDMsIDMsIDAuMjUzKTtcbiAgICBpbWcge1xuICAgICAgYmFja2dyb3VuZDogcmdiKDIxNiwgMjE2LCAyMTYpO1xuICAgICAgcGFkZGluZzogNnB4O1xuICAgICAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgICAgIHdpZHRoOiA5MHB4O1xuICAgICAgaGVpZ2h0OiA5MHB4O1xuICAgIH1cbiAgICBoMyB7XG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICBwYWRkaW5nOiAxcHg7XG4gICAgfVxuICB9XG4gICNzdGYyIHtcbiAgICBtYXJnaW46IDNweDtcbiAgICB3aWR0aDogYXV0bztcbiAgICBoZWlnaHQ6IGF1dG87XG4gICAgZm9udC1zaXplOiAxZW07XG4gICAgLy8gYmFja2dyb3VuZDogcmdiKDIxNiwgMjE2LCAyMTYpO1xuICAgIC8vbWFyZ2luLWxlZnQ6IDMwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgICAvLyBib3gtc2hhZG93OiAwcHggMHB4IDVweCAwcHggcmdiYSgzLCAzLCAzLCAwLjI1Myk7XG4gICAgaDMge1xuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgcGFkZGluZzogMXB4O1xuICAgIH1cbiAgfVxufVxuXG4uY2VudGVyVGV4dCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAxNnB4ICFpbXBvcnRhbnQ7XG59XG5cbi5zY3JvbGxpbmctd3JhcHBlcjIge1xuICBvdmVyZmxvdzogYXV0byAhaW1wb3J0YW50O1xuICB3aWR0aDogMTAwJTtcbiAgZGlzcGxheTogZ3JpZDtcbiAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiBhdXRvIGF1dG8gYXV0byBhdXRvIGF1dG8gYXV0byBhdXRvIGF1dG87XG4gIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gIHBhZGRpbmc6IDBweDtcbiAgbWFyZ2luLXRvcDogMjJweDtcbiAgYW5pbWF0aW9uLWRpcmVjdGlvbjogYWx0ZXJuYXRlO1xuICBhbmltYXRpb24tbmFtZTogYnVtcDtcbiAgYW5pbWF0aW9uLWR1cmF0aW9uOiAxLjFzO1xuICBhbmltYXRpb24taXRlcmF0aW9uLWNvdW50OiAxO1xuICBiYWNrZ3JvdW5kOiAjZmRmZGZkO1xuICAjc3RmIHtcbiAgICBtYXJnaW46IDdweDtcbiAgICB3aWR0aDogMTAwcHg7XG4gICAgaGVpZ2h0OiAxMDBweDtcbiAgICBmb250LXNpemU6IDFlbTtcbiAgICBiYWNrZ3JvdW5kOiByZ2IoMjE2LCAyMTYsIDIxNik7XG4gICAgLy9tYXJnaW4tbGVmdDogMzBweDtcbiAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xuICAgIC8vIGJveC1zaGFkb3c6IDBweCAwcHggNXB4IDBweCByZ2JhKDMsIDMsIDMsIDAuMjUzKTtcbiAgICBpbWcge1xuICAgICAgLy9iYWNrZ3JvdW5kOiByZ2IoMjE2LCAyMTYsIDIxNik7XG4gICAgICBwYWRkaW5nOiAxNnB4O1xuICAgICAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgICB9XG4gIH1cbn1cblxuaW9uLWl0ZW0ge1xuICB3aWR0aDogMTAwJTtcbn1cblxuaW9uLWZvb3RlciB7XG4gIGZvbnQtZmFtaWx5OiBcIkh1bWFuc3RcIjtcbiAgbWFyZ2luLXRvcDogMCUgIWltcG9ydGFudDtcbn1cblxuXG4uZGlzcyB7XG4gIC8vIGZsb2F0OiByaWdodDtcbiAgcGFkZGluZy1sZWZ0OiA2MCU7XG4gIG1hcmdpbi10b3A6IDEwJTtcbiAgei1pbmRleDogMztcbn1cbi5taWQtcmlnaHQge1xuICAvLyBmbG9hdDogcmlnaHQ7XG4gIHBhZGRpbmctbGVmdDogODAlO1xuICBtYXJnaW4tdG9wOiAyMCU7XG4gIHotaW5kZXg6IDM7XG59XG5cbi5taWQtcmlnaHQyIHtcbiAgLy8gZmxvYXQ6IHJpZ2h0O1xuICBwYWRkaW5nLWxlZnQ6IDYwJTtcbiAgbWFyZ2luLXRvcDogNDAlO1xuICB6LWluZGV4OiAzO1xuXG4gIGlvbi1pY29uIHtcbiAgICBjb2xvcjogI2ZmZmZmZiAhaW1wb3J0YW50O1xuICB9XG59XG5cbi5taWQtcmlnaHQzIHtcbiAgLy8gZmxvYXQ6IGxlZnQ7XG5cbiAgLy8gbWFyZ2luLXRvcDogNDAlO1xuICBmbG9hdDogbGVmdDtcbiAgICBtYXJnaW4tdG9wOiAtMTMlO1xuICAgIG1hcmdpbi1sZWZ0OiAxMSU7XG4gIHotaW5kZXg6IDM7XG5cbiAgaW9uLWljb24ge1xuICAgIGNvbG9yOiAjMDAwMDAwICFpbXBvcnRhbnQ7XG4gIH1cbn1cblxuLm1pZC1yaWdodDQge1xuICBmbG9hdDogbGVmdDtcblxuICBtYXJnaW4tdG9wOiAwJTtcbiAgei1pbmRleDogMztcblxuICBpb24taWNvbiB7XG4gICAgY29sb3I6ICMwMDAwMDAgIWltcG9ydGFudDtcbiAgfVxufVxuXG4udG9wQmFyIHtcbiAgaGVpZ2h0OiA1MHB4O1xuICBwYWRkaW5nOiA4cHg7XG4gIHdpZHRoOiAxMDAlO1xuICBtYXJnaW4tdG9wOiA1JTsgLy9jaGFuZ2UgdG8gMTAlXG4gIHotaW5kZXg6IDM7XG4gIC8vIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudChyZ2JhKDk1LCA5NCwgOTQsIDAuMDQxKSwgcmdiYSgyLCA5MiwgMTYxLCAwKTM4KTtcbiAgaW9uLWljb24ge1xuICAgIHBhZGRpbmc6IDBweDtcbiAgICBjb2xvcjogIzAwMDAwMDtcbiAgfVxuICAubG9jYXRlIHtcbiAgICB3aWR0aDogYXV0bztcbiAgICBmbG9hdDogcmlnaHQ7XG4gICAgbWFyZ2luOiAxNHB4O1xuICAgIGhlaWdodDogNDBweDtcbiAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gICAgLy8gYm94LXNoYWRvdzogMHB4IDBweCAxMnB4IDBweCByZ2JhKDMsIDMsIDMsIDAuMjUzKTtcbiAgICAvLyBib3JkZXI6IDFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNik7XG4gICAgYm9yZGVyLXJhZGl1czogNTBweDtcbiAgICBpb24taWNvbiB7XG4gICAgICBmb250LXNpemU6IDEuNWVtO1xuICAgICAgY29sb3I6ICNmZGZkZmQ7XG4gICAgICBwYWRkaW5nOiAxNXB4O1xuICAgIH1cbiAgfVxufVxuXG4uaGlkZGVuIHtcbiAgYW5pbWF0aW9uLWRpcmVjdGlvbjogYWx0ZXJuYXRlO1xuICBhbmltYXRpb24tbmFtZTogd2lraTtcbiAgYW5pbWF0aW9uLWR1cmF0aW9uOiAwLjNzO1xuICBhbmltYXRpb24taXRlcmF0aW9uLWNvdW50OiAxO1xufVxuXG5pb24tY29udGVudCB7XG4gIEBrZXlmcmFtZXMgbGVmdF9yaWdodCB7XG4gICAgMCUge1xuICAgICAgbGVmdDogNzUlO1xuICAgIH1cbiAgICA1MCUge1xuICAgICAgbGVmdDogODAlO1xuICAgIH1cbiAgICAxMDAlIHtcbiAgICAgIGxlZnQ6IDc1JTtcbiAgICB9XG4gIH1cbiAgQGtleWZyYW1lcyBsZWZ0X3JpZ2h0MiB7XG4gICAgMCUge1xuICAgICAgbGVmdDogNTUlO1xuICAgIH1cbiAgICA1MCUge1xuICAgICAgbGVmdDogNTAlO1xuICAgIH1cbiAgICAxMDAlIHtcbiAgICAgIGxlZnQ6IDU1JTtcbiAgICB9XG4gIH1cbiAgQGtleWZyYW1lcyB3aWtpIHtcbiAgICBmcm9tIHtcbiAgICAgIHNjYWxlOiAwO1xuICAgIH1cbiAgICB0byB7XG4gICAgICBzY2FsZTogMTtcbiAgICB9XG4gIH1cbiAgQGtleWZyYW1lcyBidW1wIHtcbiAgICAwJSB7XG4gICAgICB0b3A6IDEwMCU7XG4gICAgfVxuICAgIC8vIDUwJSB7dG9wOiA5MCU7fVxuICAgIDEwMCUge1xuICAgICAgdG9wOiA2MCU7XG4gICAgfVxuICB9XG4gIEBrZXlmcmFtZXMgYnVtcDIge1xuICAgIDAlIHtcbiAgICAgIHRvcDogMTIwJTtcbiAgICB9XG4gICAgLy8gNTAlIHt0b3A6IDkwJTt9XG4gICAgMTAwJSB7XG4gICAgICB0b3A6IGF1dG87XG4gICAgfVxuICB9XG4gIFtub1Njcm9sbF0ge1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gIH1cbiAgLmJhcnMge1xuICAgIG1hcmdpbi10b3A6IDE1JTsgLy9jaGFuZ2UgdG8gMTMlXG4gICAgcGFkZGluZy10b3A6IDE4cHg7XG4gICAgaGVpZ2h0OiAyMCU7XG4gICAgLnBvaXRlciB7XG4gICAgICB6LWluZGV4OiA1O1xuICAgICAgbWFyZ2luLWxlZnQ6IDElO1xuICAgICAgYmFja2dyb3VuZDogI2JkMjEyMTtcbiAgICAgIGJvcmRlci1sZWZ0OiAxLjFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNik7XG4gICAgICBib3JkZXItcmlnaHQ6IDEuMXB4IHNvbGlkIHJnYigyMTYsIDIxNiwgMjE2KTtcbiAgICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNik7XG4gICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xuICAgIH1cbiAgICAuYmFycy13aGVyZXRvIHtcbiAgICAgIGhlaWdodDogYXV0bztcbiAgICAgIHdpZHRoOiA5MCU7XG4gICAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gICAgICBtYXJnaW4tdG9wOiAtNSU7IC8vY2hhbmdlIHRvIDEzJVxuICAgICAgYW5pbWF0aW9uLWRpcmVjdGlvbjogYWx0ZXJuYXRlO1xuICAgICAgYW5pbWF0aW9uLW5hbWU6IGJ1bXA7XG4gICAgICBhbmltYXRpb24tZHVyYXRpb246IDFzO1xuICAgICAgLy9tYXJnaW46IC0xMiUgMCAwIC01MHB4O1xuICAgICAgbWFyZ2luLWxlZnQ6IDUlO1xuICAgICAgcGFkZGluZzogMiU7XG4gICAgICBib3gtc2hhZG93OiAwcHggMHB4IDVweCAwcHggcmdiYSgzLCAzLCAzLCAwLjI1Myk7XG4gICAgICB6LWluZGV4OiAwO1xuICAgICAgLy8gYm9yZGVyOiAxcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xuICAgICAgLy9ib3JkZXItcmFkaXVzOiAzMHB4O1xuICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICAgIC8vbGluZS1oZWlnaHQ6MjBweDtcbiAgICAgIGZvbnQtc2l6ZTogMS41ZW07XG4gICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgICAgLm15VGV4dCB7XG4gICAgICAgIC8vbWFyZ2luLWxlZnQ6IDYlO1xuICAgICAgICBjb2xvcjogcmdiKDI0MywgMjQzLCAyNDMpO1xuICAgICAgICBmb250LXN0eWxlOiBib2xkO1xuICAgICAgICBmb250LXNpemU6IDFlbTtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgfVxuICAgICAgLm15VGV4dDIge1xuICAgICAgICAvL21hcmdpbi1sZWZ0OiA2JTtcbiAgICAgICAgY29sb3I6IHJnYigyNDMsIDI0MywgMjQzKTtcbiAgICAgICAgZm9udC1zdHlsZTogYm9sZDtcbiAgICAgICAgZm9udC1zaXplOiAxLjNlbTtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgfVxuICAgICAgaW9uLWljb24ge1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIGZvbnQtc2l6ZTogMi4xZW07XG4gICAgICAgIHJpZ2h0OiA0JTtcbiAgICAgICAgLy8gIGJhY2tncm91bmQ6ICNiZDIxMjE7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gICAgICAgIGNvbG9yOiAjMDAwMDAwO1xuICAgICAgfVxuICAgIH1cbiAgICAuYmFycy13aGVyZXRvMiB7XG4gICAgICBoZWlnaHQ6IDYwcHg7XG4gICAgICB3aWR0aDogOTAlO1xuICAgICAgYmFja2dyb3VuZDogI2JkMjEyMTtcbiAgICAgIG1hcmdpbjogLTEyJSAwIDAgLTUwcHg7XG4gICAgICBtYXJnaW4tbGVmdDogNSU7XG4gICAgICAvLyAgYm94LXNoYWRvdzogMHB4IDBweCA1cHggMHB4ICByZ2JhKDMsIDMsIDMsIDAuMjUzKTtcbiAgICAgIHotaW5kZXg6IDA7XG4gICAgICBhbmltYXRpb24tbmFtZTogd2lraTtcbiAgICAgIGFuaW1hdGlvbi1kdXJhdGlvbjogMC4zcztcbiAgICAgIGJvcmRlcjogMXB4IHNvbGlkIHJlZDtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gICAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgICAgbGluZS1oZWlnaHQ6IDIwcHg7XG4gICAgICBmb250LXNpemU6IDEuNWVtO1xuICAgICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICAgIC5teVRleHQge1xuICAgICAgICBtYXJnaW4tbGVmdDogNiU7XG4gICAgICAgIGNvbG9yOiBncmF5O1xuICAgICAgfVxuICAgICAgaW9uLWljb24ge1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIGZvbnQtc2l6ZTogMS44ZW07XG4gICAgICAgIGxlZnQ6IDIlO1xuICAgICAgICBjb2xvcjogIzAwMDAwMDtcbiAgICAgICAgcGFkZGluZzogNXB4O1xuICAgICAgfVxuICAgIH1cbiAgICAuYm9va2luZ1NlY3QyIHtcblxuXG4gICAgICBiYWNrZ3JvdW5kOiByZ2IoMjU1LCAyNTUsIDI1NSk7XG4gICAgICBwYWRkaW5nOiAyJTtcbiAgICAgIHBvc2l0aW9uOiBmaXhlZDtcbiAgICAgIGhlaWdodDogYXV0bztcbiAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgYm9yZGVyLXJhZGl1czogMHB4O1xuICAgICAgbWFyZ2luLWxlZnQ6IDAlO1xuICAgICAgdG9wOiA4MCU7XG4gICAgICB6LWluZGV4OiAzO1xuICAgICAgdGV4dC10cmFuc2Zvcm06IG5vbmUgIWltcG9ydGFudDtcblxuICAgICAgLy8gcG9zaXRpb246IGZpeGVkO1xuICAgICAgLy8gYmFja2dyb3VuZDogcmdiYSgyNTUsIDI1NSwgMjU1LCAwKTtcbiAgICAgIC8vIHBhZGRpbmc6IDIlO1xuICAgICAgLy8gaGVpZ2h0OiBhdXRvO1xuICAgICAgLy8gdG9wOiA4MCU7XG4gICAgICAvLyB3aWR0aDogOTAlO1xuICAgICAgLy8gYm9yZGVyLXJhZGl1czogMzBweDtcbiAgICAgIC8vIG1hcmdpbi1sZWZ0OiA1JTtcbiAgICAgIC8vICAgei1pbmRleDogMztcblxuXG5cblxuICAgICAgLm15VGV4dCB7XG4gICAgICAgIC8vbWFyZ2luLWxlZnQ6IDYlO1xuICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xuICAgICAgICBmb250LXN0eWxlOiBib2xkO1xuICAgICAgICBmb250LXNpemU6IDFlbTtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgfVxuICAgICAgLm15VGV4dDIge1xuICAgICAgICAvL21hcmdpbi1sZWZ0OiA2JTtcbiAgICAgICAgY29sb3I6IHJnYigyNDMsIDI0MywgMjQzKTtcbiAgICAgICAgZm9udC1zdHlsZTogYm9sZDtcbiAgICAgICAgZm9udC1zaXplOiAxLjNlbTtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgfVxuICAgICAgaW9uLWljb24ge1xuICAgICAgICAvLyBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIGZvbnQtc2l6ZTogMi4xZW07XG4gICAgICAgIHJpZ2h0OiA0JTtcbiAgICAgICAgLy8gIGJhY2tncm91bmQ6ICNiZDIxMjE7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gICAgICAgIGNvbG9yOiAjZmJiOTFkOztcbiAgICAgICAgXG4gICAgICB9XG4gICAgICAuYmFycy1sb2NhdGUge1xuICAgICAgICBoZWlnaHQ6IDY2cHg7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBiYWNrZ3JvdW5kOiByZ2IoMCwgMCwgMCk7XG4gICAgICAgIGNvbG9yOiAjZmJiOTFkICFpbXBvcnRhbnQ7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiAwJTtcblxuICAgICAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xuICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgICAgICB0ZXh0LXRyYW5zZm9ybTogbm9uZSAhaW1wb3J0YW50O1xuICAgICAgICBmb250LXNpemU6IDEuMmVtO1xuICAgICAgfVxuICAgIH1cblxuICAgIC8vIC5NZW51YnV0dG9uOmJlZm9yZSB7XG4gICAgLy8gICBjb250ZW50OiBcIlwiO1xuICAgIC8vICAgLy8gcG9zaXRpb246IGFic29sdXRlO1xuICAgIC8vICAgYm9yZGVyLWxlZnQ6IDFweCBzb2xpZCAhaW1wb3J0YW50O1xuICAgIC8vICAgbGVmdDogMTRweCAhaW1wb3J0YW50O1xuICAgIC8vICAgYmFja2dyb3VuZDogb3JhbmdlICFpbXBvcnRhbnQ7XG4gICAgLy8gICBib3R0b206IDAgIWltcG9ydGFudDtcbiAgICAvLyAgIHotaW5kZXg6IC0xICFpbXBvcnRhbnQ7XG4gICAgLy8gICBoZWlnaHQ6IDEwMCUgIWltcG9ydGFudDtcbiAgICAvLyB9XG5cbiAgICAudmVydGljYWxMaW5lOjphZnRlciB7XG4gICAgICB3aWR0aDogMSU7XG4gICAgICBoZWlnaHQ6IDUwcHg7XG4gICAgICBiYWNrZ3JvdW5kOiBncmVlbjtcbiAgICAgIG1hcmdpbjogYXV0bztcbiAgICAgIG1hcmdpbi10b3A6IDA7XG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICBtYXJnaW4tbGVmdDogMTAlO1xuICAgIH1cbiAgICAuYm9va2luZ1NlY3Qge1xuXG4gICAgICAvLyBiYWNrZ3JvdW5kOiByZ2IoMjU1LCAyNTUsIDI1NSk7XG4gICAgICAvLyBwYWRkaW5nOiAyJTtcbiAgICAgIC8vIHBvc2l0aW9uOiBmaXhlZDtcbiAgICAgIC8vIGhlaWdodDogYXV0bztcbiAgICAgIC8vIHdpZHRoOiA5MCU7XG4gICAgICAvLyBib3JkZXItcmFkaXVzOiAzMHB4O1xuICAgICAgLy8gbWFyZ2luLWxlZnQ6IDUlO1xuICAgICAgLy8gdG9wOiA3MCU7XG4gICAgICAvLyB6LWluZGV4OiAzO1xuXG4gICAgICBiYWNrZ3JvdW5kOiByZ2IoMjU1LCAyNTUsIDI1NSk7XG4gICAgICBwYWRkaW5nOiAyJTtcbiAgICAgIHBvc2l0aW9uOiBmaXhlZDtcbiAgICAgIGhlaWdodDogYXV0bztcbiAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgYm9yZGVyLXJhZGl1czogMHB4O1xuICAgICAgbWFyZ2luLWxlZnQ6IDAlO1xuICAgICAgdG9wOiA3NyU7XG4gICAgICB6LWluZGV4OiAzO1xuXG4gICAgICAuYmFycy1sb2NhdGUge1xuICAgICAgICBoZWlnaHQ6IDY2cHg7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICBcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDAlO1xuICAgIFxuICAgICAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xuICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgICAgICAvLyBsaW5lLWhlaWdodDoyMHB4O1xuICAgICAgICBmb250LXNpemU6IDEuNGVtO1xuICAgICAgICBmbG9hdDogbGVmdDtcbiAgICAgIH1cbiAgICAgIC5iYXJzLWxvY2F0ZS1waW4ge1xuICAgICAgICB3aWR0aDogYXV0bztcbiAgICAgICAgZmxvYXQ6IHJpZ2h0O1xuICAgICAgICBtYXJnaW4tdG9wOiAxJTtcbiAgICAgICAgYmFja2dyb3VuZDogI2ZkZmRmZDtcbiAgICAgICAgLy8gYm9yZGVyOiAxcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xuICAgICAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xuICAgICAgICBpb24taWNvbiB7XG4gICAgICAgICAgZm9udC1zaXplOiAxLjNlbTtcbiAgICAgICAgICBtYXJnaW4tbGVmdDogMiU7XG4gICAgICAgICAgY29sb3I6IHJlZDtcbiAgICAgICAgICBwYWRkaW5nOiA1cHg7XG4gICAgICAgICAgYW5pbWF0aW9uLWRpcmVjdGlvbjogYWx0ZXJuYXRlO1xuICAgICAgICAgIGFuaW1hdGlvbi1uYW1lOiBsZWZ0X3JpZ2h0MjtcbiAgICAgICAgICBhbmltYXRpb24tZHVyYXRpb246IDAuOHM7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIC5iYXJzLWRlc3RpbmF0ZSB7XG4gICAgICAgIGhlaWdodDogNjZweDtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgICAgICAvLyBib3JkZXI6IDAuOHB4IHNvbGlkIGNvbG9yKCRjb2xvcnMsIGRvb20sIGJhc2UpO1xuICAgICAgICAvLyAgbWFyZ2luOiAtMTIlIDAgMCAtNTBweDtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDAlO1xuICAgICAgICAvLyB6LWluZGV4OiAzO1xuICAgICAgICAvLyAgYm94LXNoYWRvdzogMHB4IDBweCA0cHggMHB4ICByZ2JhKDMsIDMsIDMsIDAuMjUzKTtcbiAgICAgICAgLy8gYm9yZGVyOiAxcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xuICAgICAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xuICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgICAgICAvLyBsaW5lLWhlaWdodDoyMHB4O1xuICAgICAgICBmb250LXNpemU6IDEuNGVtO1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIGZsb2F0OiBsZWZ0O1xuICAgICAgfVxuICAgICAgLmJhcnMtZGVzdGluYXRlLXBpbiB7XG4gICAgICAgIHdpZHRoOiBhdXRvO1xuICAgICAgICBmbG9hdDogcmlnaHQ7XG4gICAgICAgIG1hcmdpbi10b3A6IDElO1xuICAgICAgICBiYWNrZ3JvdW5kOiAjZmRmZGZkO1xuICAgICAgICAvLyBib3JkZXI6IDFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNik7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gICAgICAgIGlvbi1pY29uIHtcbiAgICAgICAgICBmb250LXNpemU6IDEuNGVtO1xuICAgICAgICAgIG1hcmdpbi1sZWZ0OiAyJTtcbiAgICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xuICAgICAgICAgIHBhZGRpbmc6IDVweDtcbiAgICAgICAgICBhbmltYXRpb24tZGlyZWN0aW9uOiBhbHRlcm5hdGU7XG4gICAgICAgICAgYW5pbWF0aW9uLW5hbWU6IGxlZnRfcmlnaHQyO1xuICAgICAgICAgIGFuaW1hdGlvbi1kdXJhdGlvbjogMC44cztcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxuICAucmVzZXQge1xuICAgIGhlaWdodDogNSU7XG4gICAgd2lkdGg6IDEwJTtcbiAgICBiYWNrZ3JvdW5kOiAjYmQyMTIxO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBtYXJnaW4tbGVmdDogODUlO1xuICAgIHRvcDogMTIlO1xuICAgIG1hcmdpbi10b3A6IDAlO1xuICAgIHotaW5kZXg6IDE7XG4gICAgYm9yZGVyOiAxcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xuICAgIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gICAgZm9udC1zaXplOiAxLjVlbTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgaW9uLWljb24ge1xuICAgICAgbWFyZ2luLXRvcDogNCU7XG4gICAgfVxuICB9XG4gIGgzIHtcbiAgICBjb2xvcjogIzAwMDAwMDtcbiAgICBmb250LXNpemU6IDAuN2VtO1xuICAgIGZvbnQtd2VpZ2h0OiAzMDA7XG4gIH1cbiAgaDEge1xuICAgIGNvbG9yOiAjZmZmZmZmO1xuICAgIGZvbnQtc2l6ZTogMC43ZW07XG4gIH1cbiAgLnN0YWNrIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgZm9udC1zaXplOiAwLjVlbTtcbiAgICBjb2xvcjogd2hpdGU7XG4gIH1cbiAgLmd1dHRvbnoge1xuICAgIC8vIGJhY2tncm91bmQ6IHJlZDtcbiAgICAvLyBib3JkZXItcmFkaXVzOiAzMHB4O1xuICAgIC8vIGhlaWdodDogYXV0bztcbiAgICAvLyB3aWR0aDogNTAlO1xuICAgIC8vIG1hcmdpbi1sZWZ0OiAyNyU7XG5cbiAgICAvLyBjb2xvcjogd2hpdGU7XG4gIH1cbiAgI215Q29sIHtcbiAgICB3aWR0aDogMCUgIWltcG9ydGFudDtcbiAgICBwYWRkaW5nOiAwcHg7XG4gIH1cbiAgI2lDb2wge1xuICAgIHdpZHRoOiA1MCUgIWltcG9ydGFudDtcbiAgICBwYWRkaW5nOiAwcHg7XG4gIH1cbiAgI3JpZGVzIHtcbiAgICAvLyBib3JkZXItcmFkaXVzOiAzMHB4O1xuICAgIHotaW5kZXg6IDQ7XG4gICAgLy8gYm9yZGVyOiAxcHggc29saWQgcmdiKDIxNiwgMjE2LCAyMTYpO1xuICAgIC8vIGhlaWdodDogMTAwJSAhaW1wb3J0YW50O1xuICAgIC8vcGFkZGluZzogNXB4O1xuICAgIGZvbnQtc2l6ZTogM2VtO1xuICAgIHBhZGRpbmc6IDZweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDI0MCwgMjQwLCAyNDAsIDAuOTIpO1xuICB9XG4gICNyaWRlc19iaWdnZXIge1xuICAgIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gICAgei1pbmRleDogNDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNik7XG4gICAgaGVpZ2h0OiBhdXRvO1xuICAgIG92ZXJmbG93OiB2aXNpYmxlO1xuICB9XG4gIC5iYXJzLWdwcyB7XG4gICAgaGVpZ2h0OiA1MHB4O1xuICAgIHdpZHRoOiA5MCU7XG4gICAgYmFja2dyb3VuZDogcmdiYSgyNDAsIDI0MCwgMjQwLCAwLjkyKTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgbWFyZ2luOiAtNTBweCAwIDAgLTUwcHg7XG4gICAgbWFyZ2luLWxlZnQ6IDUlO1xuICAgIHRvcDogMjAlO1xuICAgIHotaW5kZXg6IDE7XG4gICAgYm9yZGVyOiAxcHggc29saWQgdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgICBsaW5lLWhlaWdodDogNTBweDtcbiAgICBmb250LXNpemU6IDEuMmVtO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xuICB9XG4gIC5iYXJzLXBvc2l0aW9uIHtcbiAgICBoZWlnaHQ6IDUwcHg7XG4gICAgd2lkdGg6IDIwJTtcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDI0MCwgMjQwLCAyNDAsIDAuOTIpO1xuICAgIG1hcmdpbjogLTUwcHggMCAwIC01MHB4O1xuICAgIG1hcmdpbi1sZWZ0OiA1JTtcbiAgICB0b3A6IDIwJTtcbiAgICB6LWluZGV4OiAxO1xuICAgIGJvcmRlcjogMXB4IHNvbGlkIHJlZDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gICAgbGluZS1oZWlnaHQ6IDUwcHg7XG4gICAgZm9udC1zaXplOiAxLjJlbTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgY29sb3I6IHJlZDtcbiAgfVxuICAvLyB9XG4gIC5iYXIge1xuICAgIHBvc2l0aW9uOiBmaXhlZDtcbiAgICBoZWlnaHQ6IDM1cHg7XG4gICAgd2lkdGg6IDcwJTtcbiAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gICAgbWFyZ2luLWxlZnQ6IDE1JTtcbiAgICB6LWluZGV4OiA0O1xuICAgIHRvcDogNjAlO1xuICAgIGJvcmRlcjogMXB4IHNvbGlkIHJnYigyMTYsIDIxNiwgMjE2KTtcbiAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xuICAgIGZvbnQtc2l6ZTogMS4xZW07XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAuaWNvbiB7XG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICBmb250LXNpemU6IDEuNmVtO1xuICAgICAgcmlnaHQ6IDIlO1xuICAgICAgcGFkZGluZzogMHB4O1xuICAgICAgY29sb3I6ICNmZGZkZmQ7XG4gICAgICBhbmltYXRpb24tbmFtZTogbGVmdF9yaWdodDtcbiAgICAgIGFuaW1hdGlvbi1kdXJhdGlvbjogMC41cztcbiAgICAgIGFuaW1hdGlvbi1pdGVyYXRpb24tY291bnQ6IGluZmluaXRlO1xuICAgICAgZGlzcGxheTogaW5saW5lO1xuICAgIH1cbiAgICAudGV4dGxlZnQge1xuICAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgICBib3JkZXI6IDEuNHB4IHNvbGlkICNmZGZkZmQ7XG4gICAgICBwYWRkaW5nOiA5cHg7XG4gICAgICB6LWluZGV4OiAzO1xuICAgICAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgICAgIGZvbnQtc2l6ZTogMC45ZW07XG4gICAgfVxuICB9XG4gIC5sZWZ0QnV0dG9uIHtcbiAgICBmbG9hdDogbGVmdDtcbiAgICB3aWR0aDogMTAlO1xuICAgIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gICAgZm9udC1zaXplOiAxZW07XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgIGgxIHtcbiAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gICAgfVxuICB9XG4gIC5wcmV2aW91cyB7XG4gICAgbWFyZ2luLXRvcDogNDBweDtcbiAgfVxuICAuY2VudGVyTWFya2VyIHtcbiAgICB6LWluZGV4OiAxO1xuICAgIG1hcmdpbi10b3A6IDQxJSAhaW1wb3J0YW50O1xuICAgIC5taWRkeSB7XG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICBsZWZ0OiA0NSU7XG4gICAgICBtYXJnaW4tbGVmdDogNSU7XG4gICAgICBiYWNrZ3JvdW5kOiByZ2IoNTYsIDU2LCA1Nik7XG4gICAgICBoZWlnaHQ6IDMwcHg7XG4gICAgICB0b3A6IDAlO1xuICAgICAgbWFyZ2luLXRvcDogMCU7XG4gICAgICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiA0cHg7XG4gICAgICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogNHB4O1xuICAgICAgd2lkdGg6IDAuNjUlO1xuICAgICAgLy8gQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiAxMDI0cHgpIHtcbiAgICAgIC8vICAgbWFyZ2luLXRvcDogNjAlO1xuICAgICAgLy8gICB9XG4gICAgfVxuICB9XG4gICNudWdnZXQge1xuICAgIHBvc2l0aW9uOiBmaXhlZDtcbiAgICB6LWluZGV4OiAyO1xuICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgICBoZWlnaHQ6IDE1cHg7XG4gICAgd2lkdGg6IDQlO1xuICAgIGxlZnQ6IDQ1JTtcbiAgICBtYXJnaW4tbGVmdDogMy40JTtcbiAgICBtYXJnaW4tdG9wOiAtNCUgIWltcG9ydGFudDtcbiAgICAvLyB0b3A6IDMwJSAhaW1wb3J0YW50O1xuICAgIGJvcmRlci1yYWRpdXM6IDUwcHg7XG4gIH1cbiAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiAxMDI0cHgpIHtcbiAgICAjbnVnZ2V0IHtcbiAgICAgIHBvc2l0aW9uOiBmaXhlZDtcbiAgICAgIHotaW5kZXg6IDI7XG4gICAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gICAgICBoZWlnaHQ6IDE1cHg7XG4gICAgICB3aWR0aDogNCU7XG4gICAgICBsZWZ0OiA0NSU7XG4gICAgICBtYXJnaW4tbGVmdDogMy40JTtcbiAgICAgIG1hcmdpbi10b3A6IC0xJSAhaW1wb3J0YW50O1xuICAgICAgLy8gdG9wOiAzMCUgIWltcG9ydGFudDtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDUwcHg7XG4gICAgfVxuICB9XG4gICNoaWRlciB7XG4gICAgbWFyZ2luLWxlZnQ6IDczJTtcbiAgICB0b3A6IDMlO1xuICAgIC5odXR0b24ge1xuICAgICAgcGFkZGluZzogYXV0bztcbiAgICAgIGJvcmRlci1yYWRpdXM6IDQwcHg7XG4gICAgICB3aWR0aDogNTclO1xuICAgICAgLy8gYm94LXNoYWRvdzogMHB4IDBweCA0cHggMHB4IHJnYmEoMywgMywgMywgMC4yNTMpO1xuICAgICAgaW9uLWljb24ge1xuICAgICAgICBtYXJnaW4tbGVmdDogNSUgIWltcG9ydGFudDtcbiAgICAgIH1cbiAgICB9XG4gIH1cbiAgLnNjcm9sbCB7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICB9XG5cblxuICAjbWFwIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgLy9wb3NpdGlvbjogZml4ZWQ7XG4gICAgei1pbmRleDogMTtcbiAgfVxuXG5cbiAgLmRlZmF1bHRfbWFwIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgei1pbmRleDogMTtcblxufVxuXG4ubWFwX2NoYW5nZSB7XG4gIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICB6LWluZGV4OiAxO1xufVxuXG5cbi53aWRJY29ue1xuICBtYXJnaW4tbGVmdDogODUlO1xuICBtYXJnaW4tdG9wOiAtMTAlO1xucG9zaXRpb246IGFic29sdXRlO1xuZm9udC1zaXplOiAxNXB4O1xuY29sb3I6IGJsYWNrO1xuei1pbmRleDogOTk5OTk5OTk5OTk7XG59XG5cblxuXG4ubWVzc19kZWZ1YWx0IHtcbiB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlICFpbXBvcnRhbnQ7XG4gIHotaW5kZXg6IDE7XG59XG5cbi5tZXNzMl9zaG9ydCB7XG4gIC8vIGJhY2tncm91bmQtY29sb3I6IHJnYigxNjIsIDI1NSwgMCk7XG4gICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiA1MCUgIWltcG9ydGFudDtcbiAgXG4gICAgei1pbmRleDogMTtcbn1cblxuXG5cblxuICAuYm90dG9tLWJhciB7XG4gICAgaGVpZ2h0OiBhdXRvO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAvLyB0b3A6IDcwJTtcbiAgICBtYXJnaW46IC01MHB4IDAgMCAtNTBweDtcbiAgICBtYXJnaW4tbGVmdDogMCU7XG4gICAgYm90dG9tOiAwJTtcbiAgICB6LWluZGV4OiAxO1xuICAgIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDMwcHg7XG4gICAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDMwcHg7XG4gICAgLy9ib3JkZXI6IDFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNik7XG4gICAgLy8gYm94LXNoYWRvdzogMHB4IC0xcHggN3B4IDBweCByZ2JhKDMsIDMsIDMsIDAuMjUzKTtcbiAgICAuYnV0dCB7XG4gICAgICBjb2xvcjogbGlnaHRnb2xkZW5yb2R5ZWxsb3c7XG4gICAgICBmb250LXNpemU6IDEuMmVtO1xuICAgIH1cbiAgICAvLyAuYm90dG9tLWxpc3R7XG4gICAgLy8gICBtYXJnaW4tdG9wOiA5MCU7XG4gICAgLy8gICBvdmVyZmxvdy14OiBhdXRvO1xuICAgIC8vICAgIC5teS1ncmlke1xuICAgIC8vICAgICB3aWR0aDogYXV0bztcbiAgICAucHJpY2V5IHtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gICAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gICAgICBtYXJnaW46IDIlO1xuICAgICAgcGFkZGluZzogMSU7XG4gICAgICBmb250LXNpemU6IDEuMmVtO1xuICAgICAgd2lkdGg6IDMzJTtcbiAgICAgIGNvbG9yOiByZ2IoMjU1LCAyNTUsIDI1NSk7XG4gICAgICB6LWluZGV4OiA5O1xuICAgICAgYm9yZGVyOiAxcHggc29saWQgdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xuICAgICAgbWFyZ2luLWxlZnQ6IDMwJTtcbiAgICAgIG1hcmdpbi10b3A6IDAlO1xuICAgIH1cbiAgICAudG9wZXkge1xuICAgICAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgICAgIC8vIGJhY2tncm91bmQ6IHJnYmEoMjQwLCAyNDAsIDI0MCwgMC45Mik7XG4gICAgICBtYXJnaW46IDIlO1xuICAgICAgLy8gcGFkZGluZzogMSU7XG4gICAgICBmb250LXNpemU6IDAuOWVtO1xuICAgICAgLy8gd2lkdGg6IDUwJTtcbiAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gICAgICAvLyBtYXJnaW4tbGVmdDogMTAlO1xuICAgICAgbWFyZ2luLXRvcDogOCU7XG4gICAgICBtYXJnaW4tYm90dG9tOiA4JTtcbiAgICB9XG4gICAgYnV0dG9uIHtcbiAgICAgIG1hcmdpbjogYXV0bztcbiAgICAgIC8vIGJvdHRvbTogLTEwNnB4O1xuICAgICAgaGVpZ2h0OiAzNXB4O1xuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIH1cbiAgICAuYm9va2VyIHtcbiAgICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCByZ2JhKDMsIDMsIDMsIDAuMjUzKTtcbiAgICAgIC8vIGJveC1zaGFkb3c6IDBweCAtMXB4IDRweCAwcHggcmdiYSgzLCAzLCAzLCAwLjI1Myk7XG4gICAgICBoZWlnaHQ6IGF1dG87XG4gICAgICAvLyBvdmVyZmxvdzogaGlkZGVuICFpbXBvcnRhbnQ7XG4gICAgICAuZ3V0dG9uIHtcbiAgICAgICAgLy8gIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudChyZ2JhKDEsIDE3LCA0MSwgMC44OSksIHJnYmEoOCwgMjUsIDQ0LCAwLjYyMykgcmdiYSgzLCAxNywgMzEsIDAuNDM4KSkgLCB1cmwoJG15VXJsKSBuby1yZXBlYXQ7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gICAgICAgIGhlaWdodDogNTBweDtcbiAgICAgICAgLy8gd2lkdGg6IDEwMCU7XG4gICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gICAgICAgIC8vIGJveC1zaGFkb3c6IDBweCAwcHggNnB4IDBweCByZ2JhKDMsIDMsIDMsIDAuMjUzKTtcbiAgICAgICAgLy9ib3JkZXI6IDFweCBzb2xpZCB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gICAgICAgIC8vYmFja2dyb3VuZC1wb3NpdGlvbjogbGVmdDtcbiAgICAgICAgZm9udC1zaXplOiAxLjNlbTtcbiAgICAgIH1cbiAgICB9XG4gICAgLnRUaGEge1xuICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KSAhaW1wb3J0YW50O1xuICAgIH1cbiAgICAuZ3V0dG9ucSB7XG4gICAgICAvLyAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHJnYmEoMSwgMTcsIDQxLCAwLjg5KSwgcmdiYSg4LCAyNSwgNDQsIDAuNjIzKSByZ2JhKDMsIDE3LCAzMSwgMC40MzgpKSAsIHVybCgkbXlVcmwpIG5vLXJlcGVhdDtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gICAgICBoZWlnaHQ6IGF1dG87XG4gICAgICAvLyB3aWR0aDogODAlO1xuICAgICAgLy8gbWFyZ2luLWxlZnQ6IDQwcHg7XG4gICAgICAvLyBib3gtc2hhZG93OiAwcHggMHB4IDZweCAwcHggcmdiYSgzLCAzLCAzLCAwLjI1Myk7XG4gICAgICAvL2JvcmRlcjogMXB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgICAgIC8vYmFja2dyb3VuZC1wb3NpdGlvbjogbGVmdDtcXFxuICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgICAgIGZvbnQtc2l6ZTogMS4zZW07XG4gICAgICBwYWRkaW5nOiAxMHB4O1xuICAgIH1cbiAgICAudGV4dCB7XG4gICAgICBwYWRkaW5nLWJvdHRvbTogMHB4O1xuICAgICAgLy8gYm9yZGVyOiAxcHggc29saWQgdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xuICAgICAgZm9udC1zaXplOiAxLjE2ZW07XG4gICAgICBjb2xvcjogI2ZkZmRmZDtcbiAgICAgIC8vYm9yZGVyLXJhZGl1czogMzBweDtcbiAgICB9XG4gIH1cbiAgI215R3JpZCB7XG4gICAgb3ZlcmZsb3cteDogYXV0byAhaW1wb3J0YW50O1xuICAgIHdpZHRoOiBhdXRvICFpbXBvcnRhbnQ7XG4gIH1cbiAgI215R3JpZDIge1xuICAgIG92ZXJmbG93LXg6IGF1dG8gIWltcG9ydGFudDtcbiAgICB3aWR0aDogYXV0byAhaW1wb3J0YW50O1xuICB9XG4gICNjYXJ0LWJ0biB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGJhY2tncm91bmQtY29sb3I6IHJlZDtcbiAgfVxuICAjY2FydC1iYWRnZSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogMHB4O1xuICAgIHJpZ2h0OiAwcHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmVkO1xuICB9XG4gIC5ib3R0b20tbGlzdCB7XG4gICAgbWFyZ2luLXRvcDogOTAlO1xuICAgIG92ZXJmbG93LXg6IGF1dG87XG4gICAgLm15LWdyaWQge1xuICAgICAgd2lkdGg6IGF1dG87XG4gICAgICAuc3R1ZmZzIHtcbiAgICAgICAgaGVpZ2h0OiA0OC41cHg7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiAzNCU7XG4gICAgICAgIHBhZGRpbmc6IDAlO1xuICAgICAgICB3aWR0aDogMjkuNSU7XG4gICAgICAgIHRvcDogMHB4O1xuICAgICAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xuICAgICAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xuICAgICAgICBmb250LXNpemU6IDFlbTtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICAvLyBib3JkZXI6IDEuMnB4IHNvbGlkICAjZmRmZGZkLDtcbiAgICAgICAgLy8gYm94LXNoYWRvdzogMHB4IDBweCA1cHggMHB4IHJnYmEoMywgMywgMywgMC4yNTMpO1xuICAgICAgICB6LWluZGV4OiAzO1xuICAgICAgICAuaXRlbVBpY3Mge1xuICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgLnllbGxvd0J0biB7XG4gICAgY29sb3I6IHJnYigwLCAwLCAwKSAhaW1wb3J0YW50O1xuICB9XG5cbiAgLmxvY2Ege1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBtYXJnaW4tdG9wOiAtMiU7XG4gIH1cblxuICAuZG90dCB7XG4gICAgd2lkdGg6IDIwcHg7XG4gICAgaGVpZ2h0OiAyMHB4O1xuICB9XG5cbiAgLmNlbnRlckJ0biB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuXG4gIH1cblxuICAuZW1haWxidG4ge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMwMDAwMDA7XG4gICAgY29sb3I6ICNmYmI5MWQ7XG4gIH1cblxuICAuY2hhdEljb24ge1xuICAgIHdpZHRoOiA4MHB4O1xuICAgIGhlaWdodDogNDBweDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgbWFyZ2luLWxlZnQ6IC0yMCU7XG4gICAgbWFyZ2luLXRvcDogLTI3JTtcbiAgfVxuXG4gIC5saXN0UmlkZXMge1xuICAgIC8vIGhlaWdodDogYXV0bztcbiAgICAvLyB3aWR0aDogMTAwJTtcbiAgICAvLyBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICAvLyBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgLy8gYm90dG9tOiAwJTtcblxuICAgIC8vIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDMwcHg7XG4gICAgLy8gYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDMwcHg7XG4gICAgbWFyZ2luLXJpZ2h0OiAwO1xuICAgIG1hcmdpbi10b3A6IDBweDtcbiAgICBtYXJnaW4tYm90dG9tOiAwO1xuICAgIHBhZGRpbmctbGVmdDogMDtcbiAgICBwYWRkaW5nLXJpZ2h0OiAwO1xuICAgIHBhZGRpbmctdG9wOiAwcHg7XG4gICAgcGFkZGluZy1ib3R0b206IDhweDtcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICAgIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDMwcHg7XG4gICAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDMwcHg7XG4gICAgd2lkdGg6IDkwJTtcbiAgICBtYXJnaW4tbGVmdDogNSU7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJvdHRvbTogMHB4O1xuICAgIHBhZGRpbmctdG9wOiAxNSU7XG4gICAgei1pbmRleDogMzMzO1xuICB9XG5cbiAgLmRyaXZlckZvdW5kIHtcbiAgICBoZWlnaHQ6IDU2JTtcbiAgICB3aWR0aDogOTUlO1xuICAgIG1hcmdpbi1sZWZ0OiAxLjUlO1xuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBib3R0b206IDAlO1xuICAgIHotaW5kZXg6IDE7XG4gICAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMzBweDtcbiAgICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMzBweDtcbiAgfVxuXG5cblxuICAuaGVhZFNlY3Rpb24ge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmYmI5MWQ7XG4gICAgY29sb3I6IGJsYWNrO1xuICAgIG1hcmdpbi10b3A6IC02JTtcbiAgICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAyN3B4O1xuICAgIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAzMHB4O1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDQ1JTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIH1cblxuXG4gICAuaGVhZFNlY3Rpb25fZHJpdmVyX2Fycml2ZSB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XG4gICAgY29sb3I6ICNmYmI5MWQgIDtcbiAgICBtYXJnaW4tdG9wOiAtNiU7XG4gICAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMjdweDtcbiAgICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMzBweDtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiA0NSU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB9XG5cblxuLmJ1dHRvbl9zdHlsZSB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgYm9yZGVyLXJhZGl1czogNjBweDtcbiAgaGVpZ2h0OiA0MHB4O1xuICB3aWR0aDogNjAlO1xuICBjb2xvcjogd2hpdGU7XG4gIG1hcmdpbi1sZWZ0OiAyMCVcbn1cblxuXG4gIC5tb3ZlSGVhZGVyIHtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgbWFyZ2luLXRvcDogMjBweDtcbiAgICBtYXJnaW4tbGVmdDogMHB4O1xuICAgIG1hcmdpbi1yaWdodDogMHB4O1xuICB9XG5cbiAgI2ljb25TaXplIHtcbiAgICBmb250LXNpemU6IDYwcHggIWltcG9ydGFudDtcbiAgfVxuXG4gICNkcml2ZXJpY29uU2l6ZSB7XG4gICAgZm9udC1zaXplOiA3MHB4ICFpbXBvcnRhbnQ7XG4gIH1cblxuICAjaDQ0IHtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gIH1cblxuICAjcDQge1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgfVxuICAjbG9jU2l6ZSB7XG4gICAgZm9udC1zaXplOiAxNnB4ICFpbXBvcnRhbnQ7XG4gIH1cblxuICAucmVzdWx0Q29udGFpbmVyIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgbGVmdDogMDtcbiAgICBtYXJnaW4tdG9wOiAzMHB4O1xuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gICAgLy8gcGFkZGluZzogMTBweDtcbiAgICBwYWRkaW5nLXJpZ2h0OiA1JTtcbiAgICBwYWRkaW5nLWxlZnQ6IDUlO1xuICAgIGJvcmRlci1ib3R0b206ICNiYWJhYmEgc29saWQgMXB4O1xuICAgICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICB9XG4gIC5yZXN1bHRDb250YWluZXIyIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgbGVmdDogMDtcbiAgICBtYXJnaW4tdG9wOiAwcHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gIH1cblxuICAuYm9va0ltYWdlLFxuICAuYm9va1RpdGxlLFxuICAuYm9va1ByaWNlIHtcbiAgICBtYXJnaW4tbGVmdDogMjBweDtcbiAgfVxuXG4gIC5jb250ZW50LXdyYXAsXG4gIC5pbWctd3JhcHBlciB7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIG1hcmdpbi1sZWZ0OiAwcHg7XG4gIH1cblxuICAuY29udGVudC13cmFwMiB7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICB9XG5cbiAgLm1hcEhlaWdodHtcbiAgICBoZWlnaHQ6IDUwJTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIH1cblxuICAuY2FyYm90dG9tLWJhcjIge1xuICAgIGhlaWdodDogNDUlO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIC8vIG1hcmdpbi1sZWZ0OiA1JTtcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG5cbiAgICBib3R0b206IDAlO1xuICAgIHotaW5kZXg6IDE7XG4gICAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMzBweDtcbiAgICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMzBweDtcbiAgICAvL2JvcmRlcjogMXB4IHNvbGlkIHJnYigyMTYsIDIxNiwgMjE2KTtcbiAgICAvLyBib3gtc2hhZG93OiAwcHggLTFweCA3cHggMHB4IHJnYmEoMywgMywgMywgMC4yNTMpO1xuICAgIC5idXR0IHtcbiAgICAgIGNvbG9yOiBsaWdodGdvbGRlbnJvZHllbGxvdztcbiAgICAgIGZvbnQtc2l6ZTogMS4yZW07XG4gICAgfVxuXG4gICAgLmJvb2tlciB7XG4gICAgICBib3JkZXItdG9wOiAxcHggc29saWQgcmdiYSgzLCAzLCAzLCAwLjI1Myk7XG5cbiAgICAgIGhlaWdodDogYXV0bztcbiAgICB9XG4gICAgLnRUaGEge1xuICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KSAhaW1wb3J0YW50O1xuICAgIH1cblxuICAgIC50ZXh0IHtcbiAgICAgIHBhZGRpbmctYm90dG9tOiAwcHg7XG4gICAgICBmb250LXNpemU6IDEuMTZlbTtcbiAgICAgIGNvbG9yOiAjZmRmZGZkO1xuICAgIH1cbiAgfVxuXG4gIC5ib3R0b20tYmFyMiB7XG4gICAgaGVpZ2h0OiA1MCU7XG4gICAgd2lkdGg6IDk1JTtcbiAgICBtYXJnaW4tbGVmdDogMS41JTtcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG5cbiAgICBib3R0b206IDAlO1xuICAgIHotaW5kZXg6IDE7XG4gICAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMzBweDtcbiAgICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMzBweDtcbiAgICAvL2JvcmRlcjogMXB4IHNvbGlkIHJnYigyMTYsIDIxNiwgMjE2KTtcbiAgICAvLyBib3gtc2hhZG93OiAwcHggLTFweCA3cHggMHB4IHJnYmEoMywgMywgMywgMC4yNTMpO1xuICAgIC5idXR0IHtcbiAgICAgIGNvbG9yOiBsaWdodGdvbGRlbnJvZHllbGxvdztcbiAgICAgIGZvbnQtc2l6ZTogMS4yZW07XG4gICAgfVxuXG4gICAgLmJvb2tlciB7XG4gICAgICBib3JkZXItdG9wOiAxcHggc29saWQgcmdiYSgzLCAzLCAzLCAwLjI1Myk7XG5cbiAgICAgIGhlaWdodDogYXV0bztcbiAgICB9XG4gICAgLnRUaGEge1xuICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KSAhaW1wb3J0YW50O1xuICAgIH1cblxuICAgIC50ZXh0IHtcbiAgICAgIHBhZGRpbmctYm90dG9tOiAwcHg7XG4gICAgICBmb250LXNpemU6IDEuMTZlbTtcbiAgICAgIGNvbG9yOiAjZmRmZGZkO1xuICAgIH1cbiAgfVxuICAuYm90dG9tMy1jb250YWluZXIge1xuICAgIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDMwcHg7XG4gICAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDMwcHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIC5ib3R0b20tYmFyMyB7XG4gICAgICBoZWlnaHQ6IDEwMHB4IGF1dG87XG4gICAgICB3aWR0aDogMTAwJTtcbiAgICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgICAgIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDMwcHg7XG4gICAgICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMzBweDtcbiAgICAgIG1hcmdpbjogMHB4IDAgMCAwcHg7XG4gICAgICBtYXJnaW4tbGVmdDogMCU7XG4gICAgICAvLyBib3JkZXItdG9wOiAxLjJweCBzb2xpZCByZ2JhKDMsIDMsIDMsIDAuMjUzKTtcbiAgICAgIC8vIGJveC1zaGFkb3c6IDBweCAtMXB4IDdweCAwcHggcmdiYSgzLCAzLCAzLCAwLjI1Myk7XG4gICAgICBib3R0b206IDE1JTtcbiAgICAgIHotaW5kZXg6IDE7XG4gICAgICAvL2JvcmRlci1yYWRpdXM6IDMwcHg7XG4gICAgICAubXlJbWFnZSB7XG4gICAgICAgIHotaW5kZXg6IDA7XG4gICAgICAgIHBvc2l0aW9uOiBmaXhlZDtcbiAgICAgICAgbGVmdDogMCU7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBoZWlnaHQ6IDEwMHB4O1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMjM1LCA2NiwgMCk7XG4gICAgICB9XG4gICAgICAucHJvZmlsZS1waWMge1xuICAgICAgICB3aWR0aDogNWVtO1xuICAgICAgICBoZWlnaHQ6IDVlbTtcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgIzNlM2MzYztcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNTBweDtcbiAgICAgIH1cbiAgICAgIC5sb2dvcyB7XG4gICAgICAgIHBhZGRpbmc6IDBweDtcbiAgICAgICAgaW9uLWljb24ge1xuICAgICAgICAgIGZvbnQtc2l6ZTogMS4xZW07XG4gICAgICAgICAgY29sb3I6ICNmYmI5MWQ7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIC50ZXh0IHtcbiAgICAgICAgcGFkZGluZzogMHB4O1xuICAgICAgICBmb250LXNpemU6IDEuNGVtO1xuICAgICAgICBtYXJnaW4tdG9wOiA2JTtcbiAgICAgICAgY29sb3I6ICNmZGZkZmQ7XG4gICAgICB9XG4gICAgICBpb24taWNvbiB7XG4gICAgICAgIGZvbnQtc2l6ZTogMi4zZW07XG4gICAgICAgIGNvbG9yOiAgI2ZiYjkxZDtcbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cblxuI2J1dHQge1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xuICBtYXJnaW4tdG9wOiA4JTtcbiAgbWFyZ2luLWJvdHRvbTogOCU7XG4gIGZvbnQtc2l6ZTogMS4zZW07XG4gIHBhZGRpbmc6IDdweDtcbn1cblxuI2VudmVsb3BlIHtcbiAgaGVpZ2h0OiBhdXRvO1xuICB3aWR0aDogNWVtO1xuICBtYXJnaW4tbGVmdDogNDAlO1xuICBtYXJnaW4tdG9wOiA1JTtcbn1cblxuI2VudmVsb3Age1xuICBoZWlnaHQ6IDVlbTtcbiAgd2lkdGg6IDVlbTtcbn1cblxuI2J1dHRvbkNvbnRhaW5lciB7XG4gIGZsb2F0OiByaWdodDtcbiAgYnV0dG9uIHtcbiAgICAvL2hlaWdodDogODAlICFpbXBvcnRhbnQ7XG4gICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xuICAgIGJvcmRlci1yYWRpdXM6IDEycHg7XG4gICAgY29sb3I6IHJnYigyNTUsIDI1NSwgMjU1KTtcbiAgICBtYXJnaW46IDZweDtcbiAgICAvLyBib3JkZXI6IDZweCBzb2xpZCByZ2IoMjU1LCAyNTUsIDI1NSk7XG4gICAgcGFkZGluZzogMTVweDtcbiAgICAvLyBib3gtc2hhZG93OiAwcHggMHB4IDRweCAwcHggcmdiYSgzLCAzLCAzLCAwLjI1Myk7XG4gIH1cbn1cblxuI2J1dHRvbkNvbnRhaW5lcjIge1xuICBmbG9hdDogbGVmdDtcbiAgYnV0dG9uIHtcbiAgICAvL2hlaWdodDogODAlICFpbXBvcnRhbnQ7XG4gICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xuICAgIGJvcmRlci1yYWRpdXM6IDEycHg7XG4gICAgY29sb3I6IHJnYigyNTUsIDI1NSwgMjU1KTtcbiAgICBtYXJnaW46IGF1dG87XG4gICAgLy8gYm9yZGVyOiA2cHggc29saWQgcmdiKDI1NSwgMjU1LCAyNTUpO1xuICAgIHBhZGRpbmc6IDE1cHg7XG4gICAgLy8gYm94LXNoYWRvdzogMHB4IDBweCA0cHggMHB4IHJnYmEoMywgMywgMywgMC4yNTMpO1xuICB9XG59XG5cbiNwYXkge1xuICAvLyAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHJnYmEoMSwgMTcsIDQxLCAwLjg5KSwgcmdiYSg4LCAyNSwgNDQsIDAuNjIzKSByZ2JhKDMsIDE3LCAzMSwgMC40MzgpKSAsIHVybCgkbXlVcmwpIG5vLXJlcGVhdDtcbiAgLy9ib3JkZXItcmFkaXVzOiAzMHB4O1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiBhdXRvO1xuICBtYXJnaW46IDElO1xuICBtYXJnaW4tbGVmdDogMiU7XG4gIGZvbnQtc2l6ZTogMS42ZW07XG4gIC8vYmFja2dyb3VuZDogcmdiKDI1NSwgMjU1LCAyNTUpICFpbXBvcnRhbnQ7XG4gIC8vIGJvcmRlcjogMXB4IHNvbGlkIHJnYigyNTUsIDI1NSwgMjU1KTtcbiAgLy8gYm94LXNoYWRvdzogMHB4IC0xcHggN3B4IDBweCByZ2JhKDMsIDMsIDMsIDAuMjUzKTtcbiAgcGFkZGluZzogMTBweDtcbiAgaW9uLWJhZGdlIHtcbiAgICBoZWlnaHQ6IDcwJTtcbiAgICBmb250LXNpemU6IDEuNmVtO1xuICB9XG59XG5cbiNwYXkyIHtcbiAgLy8gIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudChyZ2JhKDEsIDE3LCA0MSwgMC44OSksIHJnYmEoOCwgMjUsIDQ0LCAwLjYyMykgcmdiYSgzLCAxNywgMzEsIDAuNDM4KSkgLCB1cmwoJG15VXJsKSBuby1yZXBlYXQ7XG4gIC8vYm9yZGVyLXJhZGl1czogMzBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogYXV0bztcbiAgbWFyZ2luOiAxJTtcbiAgbWFyZ2luLWxlZnQ6IDIlO1xuICAvL2JhY2tncm91bmQ6IHJnYigyNTUsIDI1NSwgMjU1KSAhaW1wb3J0YW50O1xuICAvLyBib3JkZXI6IDFweCBzb2xpZCByZ2IoMjU1LCAyNTUsIDI1NSk7XG4gIC8vIGJveC1zaGFkb3c6IDBweCAtMXB4IDdweCAwcHggcmdiYSgzLCAzLCAzLCAwLjI1Myk7XG4gIHBhZGRpbmc6IDRweDtcbiAgaW9uLWJhZGdlIHtcbiAgICBoZWlnaHQ6IDcwJTtcbiAgICBmb250LXNpemU6IDEuNmVtO1xuICB9XG59XG5cbi5teUNlbnRyZSB7XG4gIHotaW5kZXg6IDM7XG4gIG1hcmdpbi10b3A6IDY1JTtcbn1cblxuI2NvbnRyb2wtYnV0dG9ucyB7XG4gIC8vIHBhZGRpbmc6IDM1cHg7XG4gIC8vQGF0LXJvb3QgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDEuOGVtO1xuICBpb24taWNvbiB7XG4gICAgcGFkZGluZzogMTZweDtcbiAgfVxufVxuXG4jdG9wLWJ1dHRvbnMge1xuICBoZWlnaHQ6IGF1dG87XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgLy8gdG9wOiA2NSU7XG4gIG1hcmdpbjogLTUwcHggMCAwIC01MHB4O1xuICBtYXJnaW4tbGVmdDogMCU7XG4gIGJvdHRvbTogMCU7XG4gIHotaW5kZXg6IDE7XG4gIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDMwcHg7XG4gIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAzMHB4O1xuICAvL2JvcmRlcjogMXB4IHNvbGlkIHJnYigyMTYsIDIxNiwgMjE2KTtcbiAgLy8gYm94LXNoYWRvdzogMHB4IC0xcHggN3B4IDBweCByZ2JhKDMsIDMsIDMsIDAuMjUzKTtcbiAgLmxvd2J1dHRvbiB7XG4gICAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgICAvLyBib3gtc2hhZG93OiAwcHggMXB4IDJweCAwcHggcmdiYSgzLCAzLCAzLCAwLjI1Myk7XG4gIH1cbiAgaW9uLWljb24ge1xuICAgIGZvbnQtc2l6ZTogMWVtO1xuICAgIGNvbG9yOiByZ2IoMywgMywgMyk7XG4gIH1cbn1cblxuI21hcC1jYW52YXMge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xuICAvLyBwb3NpdGlvbjogZml4ZWQ7XG4gIHotaW5kZXg6IDA7XG59XG5cbiNoZWFkZXIge1xuICBmb250LXNpemU6IDEuMzJlbTtcbiAgZm9udC13ZWlnaHQ6IDIwMDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDBweCAhaW1wb3J0YW50O1xuICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMHB4ICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiAjZmRmZGZkO1xufVxuXG4jbG9jYXRpb24ge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHBhZGRpbmctbGVmdDogMTdweDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmUgIWltcG9ydGFudFxuICAvL3dpZHRoOiA0MCU7XG59XG5cbiNkZXN0aW5hdGlvbiB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcGFkZGluZy1sZWZ0OiAxN3B4O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICB0ZXh0LXRyYW5zZm9ybTogbm9uZSAhaW1wb3J0YW50XG4gIC8vIHdpZHRoOiA0MCU7XG59XG5cbi5sb2NhdGVfYnV0dG9uIHtcbiAgei1pbmRleDogMztcbiAgZmxvYXQ6IHJpZ2h0O1xuICBtYXJnaW4tdG9wOiA2JTtcbiAgd2lkdGg6IGF1dG87XG4gIGhlaWdodDogNDBweDtcbiAgLy8gcGFkZGluZzogMHB4O1xuICBtYXJnaW4tcmlnaHQ6IDQuNSU7XG4gIGJvcmRlci1yYWRpdXM6IDYwcHg7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICAvLyBib3gtc2hhZG93OiAwcHggMHB4IDRweCAwcHggcmdiYSgzLCAzLCAzLCAwLjI1Myk7XG4gIGlvbi1pY29uIHtcbiAgICAvLyAgbWFyZ2luLWxlZnQ6IGF1dG8gIWltcG9ydGFudDtcbiAgICBtYXJnaW4tdG9wOiAtMiU7XG4gICAgYm9yZGVyLXJhZGl1czogNjBweDtcbiAgICAvL3BhZGRpbmc6IGF1dG87XG4gIH1cbn1cblxuLnJlcXVlc3QtZm9yLXJpZGUge1xuICBtYXJnaW4tdG9wOiAwJSAhaW1wb3J0YW50O1xufVxuXG4ucmVxdWVzdC1mb3ItcmlkZTIge1xuICBoZWlnaHQ6IDIwMHB4O1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbn1cblxuLmxpbmVIZWlnaHQge1xuICBsaW5lLWhlaWdodDogMTtcbn1cblxuLmJvdHR1bSB7XG4gIGJhY2tncm91bmQ6ICNmZGZkZmQ7XG4gIC8vIGJveC1zaGFkb3c6IDBweCAwcHggNHB4IDBweCByZ2JhKDMsIDMsIDMsIDAuMjUzKTtcbiAgYm9yZGVyLXJhZGl1czogNjBweDtcbiAgaW9uLWljb24ge1xuICAgIGZvbnQtc2l6ZTogMS43ZW07XG4gIH1cbn1cblxuI3JpZGVzIHtcbiAgYm9yZGVyLXJhZGl1czogMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNik7XG4gIGlvbi1iYWRnZSB7XG4gICAgZm9udC1zaXplOiAxZW07XG4gIH1cbn1cblxuLmJ1dHRvbi1yaWdodCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogNzUlO1xuICBwYWRkaW5nOiAxNnB4O1xuICB0b3A6IDYwJTtcbn1cblxuLmJ1dHRvbk5ldyB7XG4gIGJvcmRlcjogNXB4IHNvbGlkICNmYmI5MWQ7XG4gIHdpZHRoOiA2MCU7XG4gIGxlZnQ6IDUwJTtcbiAgbWFyZ2luOiAwO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogNTAlO1xuICBsZWZ0OiA1MCU7XG4gIC1tcy10cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG59XG5cbmlvbi1mb290ZXIge1xuICAjbG93ZXJfaXRlbXMge1xuICAgIGhlaWdodDogYXV0bztcbiAgICB3aWR0aDogMTAwJTtcbiAgICBiYWNrZ3JvdW5kOiByZ2IoMjU1LCAyNTUsIDI1NSk7XG4gICAgei1pbmRleDogMTU7XG4gIH1cbiAgLmJ1dHRvbnMge1xuICAgIG1hcmdpbi10b3A6IDEwJSAhaW1wb3J0YW50O1xuICAgIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDVweDtcbiAgICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogNXB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBtYXJnaW46IGF1dG87XG4gICAgcGFkZGluZzogYXV0byAhaW1wb3J0YW50O1xuICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgICB6LWluZGV4OiAzMDAgIWltcG9ydGFudDtcbiAgICBib3JkZXI6IDAuN3B4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgZm9udC1zaXplOiAxN3B4O1xuICB9XG4gICNyaWRlcyB7XG4gICAgYm9yZGVyLXJhZGl1czogMHB4O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkIHJnYigyMTYsIDIxNiwgMjE2KTtcbiAgICBpb24tYmFkZ2Uge1xuICAgICAgZm9udC1zaXplOiAxLjZlbTtcbiAgICB9XG4gIH1cbn1cblxuI3ByaWNlIHtcbiAgZm9udC1zaXplOiAxZW07XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIGgyIHtcbiAgICBmb250LXNpemU6IDAuNjNlbTtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gIH1cbn1cblxuXG4uZmFrZUl0ZW0ge1xuIFxuXG4gICAgQGtleWZyYW1lcyBhbmltYXRlZEJhcntcbiAgICAgICAgMCV7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiAtNDA2cHggMDtcbiAgICAgICAgfVxuICAgICAgICAxMDAle1xuICAgICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogMCAwO1xuICAgICAgICB9XG4gICAgIH1cbiAgICBcbiAgICAgIGgyLCBoMywgaDQsIHAge1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiBsaWdodGdyZXk7XG4gICAgICAgIG9wYWNpdHk6IDAuNTtcbiAgICAgICAgaGVpZ2h0OiAxZW07XG4gICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjZGRkZGRkICwgI0VFRUVFRSk7XG4gICAgICAgIGFuaW1hdGlvbjogYW5pbWF0ZWRCYXIgNzAwbXMgbGluZWFyIGluZmluaXRlO1xuICAgICAgICBcbiAgICAgICAgJjphZnRlciB7XG4gICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgIGNvbnRlbnQ6ICcnO1xuICAgICAgICAgIHJpZ2h0OiAwO1xuICAgICAgICAgIGJvdHRvbTogMDtcbiAgICAgICAgICB0b3A6IDA7XG4gICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgLlRheGlJQ29uMntcbiAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiBsaWdodGdyZXk7XG4gICAgICAgIG9wYWNpdHk6IDAuNTtcbiAgICAgICAgaGVpZ2h0OiAxZW07XG4gICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjZGRkZGRkICwgI0VFRUVFRSk7XG4gICAgICAgIGFuaW1hdGlvbjogYW5pbWF0ZWRCYXIgNzAwbXMgbGluZWFyIGluZmluaXRlO1xuICAgICAgICBcbiAgICAgICAgJjphZnRlciB7XG4gICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgIGNvbnRlbnQ6ICcnO1xuICAgICAgICAgIHJpZ2h0OiAwO1xuICAgICAgICAgIGJvdHRvbTogMDtcbiAgICAgICAgICB0b3A6IDA7XG4gICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICBcbiAgICAgIGgyOmFmdGVyIHtcbiAgICAgICAgd2lkdGg6IDY1JTtcbiAgICAgIH1cbiAgICBcbiAgICAgIGgzOmFmdGVyIHtcbiAgICAgICAgd2lkdGg6IDYwJTtcbiAgICAgIH0gIFxuICAgIFxuICAgICAgcDphZnRlciB7XG4gICAgICAgIHdpZHRoOiA0MCU7XG4gICAgICB9XG4gICB9XG5cblxuLmxvYWRlciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgdG9wOiBjYWxjKDAlKTtcbiAgbGVmdDogY2FsYyg1MCUgLSAzMnB4KTtcbiAgd2lkdGg6IDY0cHg7XG4gIGhlaWdodDogNjRweDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBwZXJzcGVjdGl2ZTogODAwcHg7XG59XG5cbi5pbm5lciB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgYm9yZGVyLXJhZGl1czogNTAlOyAgXG59XG5cbi5pbm5lci5vbmUge1xuICBsZWZ0OiAwJTtcbiAgdG9wOiAwJTtcbiAgYW5pbWF0aW9uOiByb3RhdGUtb25lIDFzIGxpbmVhciBpbmZpbml0ZTtcbiAgYm9yZGVyLWJvdHRvbTogM3B4IHNvbGlkICNmYmI5MWQ7XG59XG5cbi5pbm5lci50d28ge1xuICByaWdodDogMCU7XG4gIHRvcDogMCU7XG4gIGFuaW1hdGlvbjogcm90YXRlLXR3byAxcyBsaW5lYXIgaW5maW5pdGU7XG4gIGJvcmRlci1yaWdodDogM3B4IHNvbGlkICNkMmFlOGE7XG59XG5cbi5pbm5lci50aHJlZSB7XG4gIHJpZ2h0OiAwJTtcbiAgYm90dG9tOiAwJTtcbiAgYW5pbWF0aW9uOiByb3RhdGUtdGhyZWUgMXMgbGluZWFyIGluZmluaXRlO1xuICBib3JkZXItdG9wOiAzcHggc29saWQgI2ZiYjkxZDtcblxuIFxufVxuXG5Aa2V5ZnJhbWVzIHJvdGF0ZS1vbmUge1xuICAwJSB7XG4gICAgdHJhbnNmb3JtOiByb3RhdGVYKDM1ZGVnKSByb3RhdGVZKC00NWRlZykgcm90YXRlWigwZGVnKTtcbiAgfVxuICAxMDAlIHtcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZVgoMzVkZWcpIHJvdGF0ZVkoLTQ1ZGVnKSByb3RhdGVaKDM2MGRlZyk7XG4gIH1cbn1cblxuQGtleWZyYW1lcyByb3RhdGUtdHdvIHtcbiAgMCUge1xuICAgIHRyYW5zZm9ybTogcm90YXRlWCg1MGRlZykgcm90YXRlWSgxMGRlZykgcm90YXRlWigwZGVnKTtcbiAgfVxuICAxMDAlIHtcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZVgoNTBkZWcpIHJvdGF0ZVkoMTBkZWcpIHJvdGF0ZVooMzYwZGVnKTtcbiAgfVxufVxuXG5Aa2V5ZnJhbWVzIHJvdGF0ZS10aHJlZSB7XG4gIDAlIHtcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZVgoMzVkZWcpIHJvdGF0ZVkoNTVkZWcpIHJvdGF0ZVooMGRlZyk7XG4gIH1cbiAgMTAwJSB7XG4gICAgdHJhbnNmb3JtOiByb3RhdGVYKDM1ZGVnKSByb3RhdGVZKDU1ZGVnKSByb3RhdGVaKDM2MGRlZyk7XG4gIH1cbn1cblxuIiwiaW9uLWNvbnRlbnQge1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0dFwiO1xuICBtYXJnaW4tdG9wOiAwJTtcbn1cblxubGFiZWwgPiBpbnB1dCB7XG4gIGRpc3BsYXk6IG5vbmU7XG59XG5cbiNidXR0b24wIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsaW5lLWhlaWdodDogNDVweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBoZWlnaHQ6IDUwcHg7XG4gIHdpZHRoOiA1MHB4O1xuICBib3JkZXI6IDJweCBkYXNoZWQgYmxhY2s7XG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gIGxlZnQ6IDA7XG4gIHJpZ2h0OiAwO1xuICBtYXJnaW46IGF1dG87XG4gIHRvcDogNTAlO1xuICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNTAlKTtcbiAgLW1zLXRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNTAlKTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xuICBvcGFjaXR5OiAwLjU7XG59XG5cbmxhYmVsID4gaW5wdXQgfiAuYnV0dG9uIHtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxpbmUtaGVpZ2h0OiA0NXB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGhlaWdodDogNTBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIDFzIGVhc2UtaW4tb3V0O1xuICB0cmFuc2l0aW9uOiBhbGwgMXMgZWFzZS1pbi1vdXQ7XG4gIHVzZXItc2VsZWN0OiBub25lO1xuICAtd2Via2l0LXVzZXItc2VsZWN0OiBub25lO1xufVxuXG5sYWJlbCA+IGlucHV0IH4gI2J1dHRvbjEge1xuICBsZWZ0OiAwO1xuICB0b3A6IDA7XG4gIHRvcDogNTAlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjJmMmYyO1xuICBjb2xvcjogYmxhY2s7XG4gIG1hcmdpbi10b3A6IDAlO1xuICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAyN3B4O1xuICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMzBweDtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbmxhYmVsID4gaW5wdXQ6Y2hlY2tlZCB+ICNidXR0b24xIHtcbiAgbGVmdDogY2FsYygwJSAtIDBweCk7XG4gIHRvcDogY2FsYyg3MCUgLSAwJSk7XG4gIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIDFzIGVhc2UtaW4tb3V0O1xuICB0cmFuc2l0aW9uOiBhbGwgMXMgZWFzZS1pbi1vdXQ7XG59XG5sYWJlbCA+IGlucHV0OmNoZWNrZWQgfiAjYnV0dG9uMSAjbWFwIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgei1pbmRleDogMTtcbn1cblxuLm1haW4tY29uIHtcbiAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG59XG5cbi5kcml2ZXJGb3VuZE5ldyB7XG4gIGxlZnQ6IDAlO1xuICB0b3A6IDE0NyU7XG4gIGhlaWdodDogNTAlO1xuICB3aWR0aDogOTUlO1xuICBtYXJnaW4tbGVmdDogMS41JTtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYm90dG9tOiAwJTtcbiAgei1pbmRleDogMTtcbiAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMzBweDtcbiAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDMwcHg7XG59XG5cbi5teS1jdXN0b20tY2xhc3MgLm1vZGFsLXdyYXBwZXIge1xuICBiYWNrZ3JvdW5kOiAjMjIyO1xufVxuXG4uVGF4aUlDb24yIHtcbiAgd2lkdGg6IDcwcHg7XG4gIGhlaWdodDogNzBweDtcbiAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uVGF4aUlDb24ge1xuICB3aWR0aDogMTAwcHg7XG4gIGhlaWdodDogMzVweDtcbn1cblxuLlRheGlJQ29tZmlybSB7XG4gIHdpZHRoOiAxMDBweDtcbiAgaGVpZ2h0OiAzNXB4O1xufVxuXG4ubGlzdFJvdzpob3ZlciB7XG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi5hbGlnbi1jb2wge1xuICBsaW5lLWhlaWdodDogMTtcbiAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uaXRlbS1yYWRpby1jaGVja2VkIHtcbiAgLS1iYWNrZ3JvdW5kOiBibGFjayAhaW1wb3J0YW50O1xuICBjb2xvcjogd2hpdGUgIWltcG9ydGFudDtcbn1cblxuLmFjdGl2ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi5pbWdTdHlsZSB7XG4gIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLnNwaW5uZXJzIHtcbiAgd2lkdGg6IDQwcHg7XG4gIGhlaWdodDogNDBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gIG1hcmdpbjogMTRweCBhdXRvO1xuICAtd2Via2l0LWFuaW1hdGlvbjogc2stcm90YXRlcGxhbmUgMS4ycyBpbmZpbml0ZSBlYXNlLWluLW91dDtcbiAgYW5pbWF0aW9uOiBzay1yb3RhdGVwbGFuZSAxLjJzIGluZmluaXRlIGVhc2UtaW4tb3V0O1xufVxuXG5ALXdlYmtpdC1rZXlmcmFtZXMgc2stcm90YXRlcGxhbmUge1xuICAwJSB7XG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHBlcnNwZWN0aXZlKDEyMHB4KTtcbiAgfVxuICA1MCUge1xuICAgIC13ZWJraXQtdHJhbnNmb3JtOiBwZXJzcGVjdGl2ZSgxMjBweCkgcm90YXRlWSgxODBkZWcpO1xuICB9XG4gIDEwMCUge1xuICAgIC13ZWJraXQtdHJhbnNmb3JtOiBwZXJzcGVjdGl2ZSgxMjBweCkgcm90YXRlWSgxODBkZWcpIHJvdGF0ZVgoMTgwZGVnKTtcbiAgfVxufVxuQGtleWZyYW1lcyBzay1yb3RhdGVwbGFuZSB7XG4gIDAlIHtcbiAgICB0cmFuc2Zvcm06IHBlcnNwZWN0aXZlKDEyMHB4KSByb3RhdGVYKDBkZWcpIHJvdGF0ZVkoMGRlZyk7XG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHBlcnNwZWN0aXZlKDEyMHB4KSByb3RhdGVYKDBkZWcpIHJvdGF0ZVkoMGRlZyk7XG4gIH1cbiAgNTAlIHtcbiAgICB0cmFuc2Zvcm06IHBlcnNwZWN0aXZlKDEyMHB4KSByb3RhdGVYKC0xODAuMWRlZykgcm90YXRlWSgwZGVnKTtcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogcGVyc3BlY3RpdmUoMTIwcHgpIHJvdGF0ZVgoLTE4MC4xZGVnKSByb3RhdGVZKDBkZWcpO1xuICB9XG4gIDEwMCUge1xuICAgIHRyYW5zZm9ybTogcGVyc3BlY3RpdmUoMTIwcHgpIHJvdGF0ZVgoLTE4MGRlZykgcm90YXRlWSgtMTc5LjlkZWcpO1xuICAgIC13ZWJraXQtdHJhbnNmb3JtOiBwZXJzcGVjdGl2ZSgxMjBweCkgcm90YXRlWCgtMTgwZGVnKSByb3RhdGVZKC0xNzkuOWRlZyk7XG4gIH1cbn1cbi5zY3JvbGxpbmctd3JhcHBlciB7XG4gIG92ZXJmbG93OiBhdXRvICFpbXBvcnRhbnQ7XG4gIHdpZHRoOiBhdXRvO1xuICBkaXNwbGF5OiBncmlkO1xuICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IGF1dG8gYXV0byBhdXRvIGF1dG8gYXV0byBhdXRvIGF1dG8gYXV0bztcbiAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgcGFkZGluZzogMHB4O1xuICBtYXJnaW4tdG9wOiAyMnB4O1xufVxuLnNjcm9sbGluZy13cmFwcGVyICNzdGYge1xuICBtYXJnaW46IDVweDtcbiAgd2lkdGg6IDEwMHB4O1xuICBoZWlnaHQ6IDEwMHB4O1xuICBmb250LXNpemU6IDFlbTtcbiAgYmFja2dyb3VuZDogI2Q4ZDhkODtcbiAgYm9yZGVyLXJhZGl1czogMzBweDtcbn1cbi5zY3JvbGxpbmctd3JhcHBlciAjc3RmIGltZyB7XG4gIGJhY2tncm91bmQ6ICNkOGQ4ZDg7XG4gIHBhZGRpbmc6IDZweDtcbiAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgd2lkdGg6IDkwcHg7XG4gIGhlaWdodDogOTBweDtcbn1cbi5zY3JvbGxpbmctd3JhcHBlciAjc3RmIGgzIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwYWRkaW5nOiAxcHg7XG59XG4uc2Nyb2xsaW5nLXdyYXBwZXIgI3N0ZjIge1xuICBtYXJnaW46IDNweDtcbiAgd2lkdGg6IGF1dG87XG4gIGhlaWdodDogYXV0bztcbiAgZm9udC1zaXplOiAxZW07XG4gIGJvcmRlci1yYWRpdXM6IDMwcHg7XG59XG4uc2Nyb2xsaW5nLXdyYXBwZXIgI3N0ZjIgaDMge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHBhZGRpbmc6IDFweDtcbn1cblxuLmNlbnRlclRleHQge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMTZweCAhaW1wb3J0YW50O1xufVxuXG4uc2Nyb2xsaW5nLXdyYXBwZXIyIHtcbiAgb3ZlcmZsb3c6IGF1dG8gIWltcG9ydGFudDtcbiAgd2lkdGg6IDEwMCU7XG4gIGRpc3BsYXk6IGdyaWQ7XG4gIGdyaWQtdGVtcGxhdGUtY29sdW1uczogYXV0byBhdXRvIGF1dG8gYXV0byBhdXRvIGF1dG8gYXV0byBhdXRvO1xuICBib3JkZXItcmFkaXVzOiAzMHB4O1xuICBwYWRkaW5nOiAwcHg7XG4gIG1hcmdpbi10b3A6IDIycHg7XG4gIGFuaW1hdGlvbi1kaXJlY3Rpb246IGFsdGVybmF0ZTtcbiAgYW5pbWF0aW9uLW5hbWU6IGJ1bXA7XG4gIGFuaW1hdGlvbi1kdXJhdGlvbjogMS4xcztcbiAgYW5pbWF0aW9uLWl0ZXJhdGlvbi1jb3VudDogMTtcbiAgYmFja2dyb3VuZDogI2ZkZmRmZDtcbn1cbi5zY3JvbGxpbmctd3JhcHBlcjIgI3N0ZiB7XG4gIG1hcmdpbjogN3B4O1xuICB3aWR0aDogMTAwcHg7XG4gIGhlaWdodDogMTAwcHg7XG4gIGZvbnQtc2l6ZTogMWVtO1xuICBiYWNrZ3JvdW5kOiAjZDhkOGQ4O1xuICBib3JkZXItcmFkaXVzOiAzMHB4O1xufVxuLnNjcm9sbGluZy13cmFwcGVyMiAjc3RmIGltZyB7XG4gIHBhZGRpbmc6IDE2cHg7XG4gIGJvcmRlci1yYWRpdXM6IDMwcHg7XG59XG5cbmlvbi1pdGVtIHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbmlvbi1mb290ZXIge1xuICBmb250LWZhbWlseTogXCJIdW1hbnN0XCI7XG4gIG1hcmdpbi10b3A6IDAlICFpbXBvcnRhbnQ7XG59XG5cbi5kaXNzIHtcbiAgcGFkZGluZy1sZWZ0OiA2MCU7XG4gIG1hcmdpbi10b3A6IDEwJTtcbiAgei1pbmRleDogMztcbn1cblxuLm1pZC1yaWdodCB7XG4gIHBhZGRpbmctbGVmdDogODAlO1xuICBtYXJnaW4tdG9wOiAyMCU7XG4gIHotaW5kZXg6IDM7XG59XG5cbi5taWQtcmlnaHQyIHtcbiAgcGFkZGluZy1sZWZ0OiA2MCU7XG4gIG1hcmdpbi10b3A6IDQwJTtcbiAgei1pbmRleDogMztcbn1cbi5taWQtcmlnaHQyIGlvbi1pY29uIHtcbiAgY29sb3I6ICNmZmZmZmYgIWltcG9ydGFudDtcbn1cblxuLm1pZC1yaWdodDMge1xuICBmbG9hdDogbGVmdDtcbiAgbWFyZ2luLXRvcDogLTEzJTtcbiAgbWFyZ2luLWxlZnQ6IDExJTtcbiAgei1pbmRleDogMztcbn1cbi5taWQtcmlnaHQzIGlvbi1pY29uIHtcbiAgY29sb3I6ICMwMDAwMDAgIWltcG9ydGFudDtcbn1cblxuLm1pZC1yaWdodDQge1xuICBmbG9hdDogbGVmdDtcbiAgbWFyZ2luLXRvcDogMCU7XG4gIHotaW5kZXg6IDM7XG59XG4ubWlkLXJpZ2h0NCBpb24taWNvbiB7XG4gIGNvbG9yOiAjMDAwMDAwICFpbXBvcnRhbnQ7XG59XG5cbi50b3BCYXIge1xuICBoZWlnaHQ6IDUwcHg7XG4gIHBhZGRpbmc6IDhweDtcbiAgd2lkdGg6IDEwMCU7XG4gIG1hcmdpbi10b3A6IDUlO1xuICB6LWluZGV4OiAzO1xufVxuLnRvcEJhciBpb24taWNvbiB7XG4gIHBhZGRpbmc6IDBweDtcbiAgY29sb3I6ICMwMDAwMDA7XG59XG4udG9wQmFyIC5sb2NhdGUge1xuICB3aWR0aDogYXV0bztcbiAgZmxvYXQ6IHJpZ2h0O1xuICBtYXJnaW46IDE0cHg7XG4gIGhlaWdodDogNDBweDtcbiAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xuICBib3JkZXItcmFkaXVzOiA1MHB4O1xufVxuLnRvcEJhciAubG9jYXRlIGlvbi1pY29uIHtcbiAgZm9udC1zaXplOiAxLjVlbTtcbiAgY29sb3I6ICNmZGZkZmQ7XG4gIHBhZGRpbmc6IDE1cHg7XG59XG5cbi5oaWRkZW4ge1xuICBhbmltYXRpb24tZGlyZWN0aW9uOiBhbHRlcm5hdGU7XG4gIGFuaW1hdGlvbi1uYW1lOiB3aWtpO1xuICBhbmltYXRpb24tZHVyYXRpb246IDAuM3M7XG4gIGFuaW1hdGlvbi1pdGVyYXRpb24tY291bnQ6IDE7XG59XG5cbkBrZXlmcmFtZXMgbGVmdF9yaWdodCB7XG4gIDAlIHtcbiAgICBsZWZ0OiA3NSU7XG4gIH1cbiAgNTAlIHtcbiAgICBsZWZ0OiA4MCU7XG4gIH1cbiAgMTAwJSB7XG4gICAgbGVmdDogNzUlO1xuICB9XG59XG5Aa2V5ZnJhbWVzIGxlZnRfcmlnaHQyIHtcbiAgMCUge1xuICAgIGxlZnQ6IDU1JTtcbiAgfVxuICA1MCUge1xuICAgIGxlZnQ6IDUwJTtcbiAgfVxuICAxMDAlIHtcbiAgICBsZWZ0OiA1NSU7XG4gIH1cbn1cbkBrZXlmcmFtZXMgd2lraSB7XG4gIGZyb20ge1xuICAgIHNjYWxlOiAwO1xuICB9XG4gIHRvIHtcbiAgICBzY2FsZTogMTtcbiAgfVxufVxuQGtleWZyYW1lcyBidW1wIHtcbiAgMCUge1xuICAgIHRvcDogMTAwJTtcbiAgfVxuICAxMDAlIHtcbiAgICB0b3A6IDYwJTtcbiAgfVxufVxuQGtleWZyYW1lcyBidW1wMiB7XG4gIDAlIHtcbiAgICB0b3A6IDEyMCU7XG4gIH1cbiAgMTAwJSB7XG4gICAgdG9wOiBhdXRvO1xuICB9XG59XG5pb24tY29udGVudCBbbm9TY3JvbGxdIHtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cbmlvbi1jb250ZW50IC5iYXJzIHtcbiAgbWFyZ2luLXRvcDogMTUlO1xuICBwYWRkaW5nLXRvcDogMThweDtcbiAgaGVpZ2h0OiAyMCU7XG59XG5pb24tY29udGVudCAuYmFycyAucG9pdGVyIHtcbiAgei1pbmRleDogNTtcbiAgbWFyZ2luLWxlZnQ6IDElO1xuICBiYWNrZ3JvdW5kOiAjYmQyMTIxO1xuICBib3JkZXItbGVmdDogMS4xcHggc29saWQgI2Q4ZDhkODtcbiAgYm9yZGVyLXJpZ2h0OiAxLjFweCBzb2xpZCAjZDhkOGQ4O1xuICBib3JkZXItdG9wOiAxcHggc29saWQgI2Q4ZDhkODtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNkOGQ4ZDg7XG59XG5pb24tY29udGVudCAuYmFycyAuYmFycy13aGVyZXRvIHtcbiAgaGVpZ2h0OiBhdXRvO1xuICB3aWR0aDogOTAlO1xuICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gIG1hcmdpbi10b3A6IC01JTtcbiAgYW5pbWF0aW9uLWRpcmVjdGlvbjogYWx0ZXJuYXRlO1xuICBhbmltYXRpb24tbmFtZTogYnVtcDtcbiAgYW5pbWF0aW9uLWR1cmF0aW9uOiAxcztcbiAgbWFyZ2luLWxlZnQ6IDUlO1xuICBwYWRkaW5nOiAyJTtcbiAgYm94LXNoYWRvdzogMHB4IDBweCA1cHggMHB4IHJnYmEoMywgMywgMywgMC4yNTMpO1xuICB6LWluZGV4OiAwO1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBmb250LXNpemU6IDEuNWVtO1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xufVxuaW9uLWNvbnRlbnQgLmJhcnMgLmJhcnMtd2hlcmV0byAubXlUZXh0IHtcbiAgY29sb3I6ICNmM2YzZjM7XG4gIGZvbnQtc3R5bGU6IGJvbGQ7XG4gIGZvbnQtc2l6ZTogMWVtO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5pb24tY29udGVudCAuYmFycyAuYmFycy13aGVyZXRvIC5teVRleHQyIHtcbiAgY29sb3I6ICNmM2YzZjM7XG4gIGZvbnQtc3R5bGU6IGJvbGQ7XG4gIGZvbnQtc2l6ZTogMS4zZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbmlvbi1jb250ZW50IC5iYXJzIC5iYXJzLXdoZXJldG8gaW9uLWljb24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGZvbnQtc2l6ZTogMi4xZW07XG4gIHJpZ2h0OiA0JTtcbiAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgY29sb3I6ICMwMDAwMDA7XG59XG5pb24tY29udGVudCAuYmFycyAuYmFycy13aGVyZXRvMiB7XG4gIGhlaWdodDogNjBweDtcbiAgd2lkdGg6IDkwJTtcbiAgYmFja2dyb3VuZDogI2JkMjEyMTtcbiAgbWFyZ2luOiAtMTIlIDAgMCAtNTBweDtcbiAgbWFyZ2luLWxlZnQ6IDUlO1xuICB6LWluZGV4OiAwO1xuICBhbmltYXRpb24tbmFtZTogd2lraTtcbiAgYW5pbWF0aW9uLWR1cmF0aW9uOiAwLjNzO1xuICBib3JkZXI6IDFweCBzb2xpZCByZWQ7XG4gIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICBmb250LXNpemU6IDEuNWVtO1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xufVxuaW9uLWNvbnRlbnQgLmJhcnMgLmJhcnMtd2hlcmV0bzIgLm15VGV4dCB7XG4gIG1hcmdpbi1sZWZ0OiA2JTtcbiAgY29sb3I6IGdyYXk7XG59XG5pb24tY29udGVudCAuYmFycyAuYmFycy13aGVyZXRvMiBpb24taWNvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgZm9udC1zaXplOiAxLjhlbTtcbiAgbGVmdDogMiU7XG4gIGNvbG9yOiAjMDAwMDAwO1xuICBwYWRkaW5nOiA1cHg7XG59XG5pb24tY29udGVudCAuYmFycyAuYm9va2luZ1NlY3QyIHtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHBhZGRpbmc6IDIlO1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIGhlaWdodDogYXV0bztcbiAgd2lkdGg6IDEwMCU7XG4gIGJvcmRlci1yYWRpdXM6IDBweDtcbiAgbWFyZ2luLWxlZnQ6IDAlO1xuICB0b3A6IDgwJTtcbiAgei1pbmRleDogMztcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmUgIWltcG9ydGFudDtcbn1cbmlvbi1jb250ZW50IC5iYXJzIC5ib29raW5nU2VjdDIgLm15VGV4dCB7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gIGZvbnQtc3R5bGU6IGJvbGQ7XG4gIGZvbnQtc2l6ZTogMWVtO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5pb24tY29udGVudCAuYmFycyAuYm9va2luZ1NlY3QyIC5teVRleHQyIHtcbiAgY29sb3I6ICNmM2YzZjM7XG4gIGZvbnQtc3R5bGU6IGJvbGQ7XG4gIGZvbnQtc2l6ZTogMS4zZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbmlvbi1jb250ZW50IC5iYXJzIC5ib29raW5nU2VjdDIgaW9uLWljb24ge1xuICBmb250LXNpemU6IDIuMWVtO1xuICByaWdodDogNCU7XG4gIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gIGNvbG9yOiAjZmJiOTFkO1xufVxuaW9uLWNvbnRlbnQgLmJhcnMgLmJvb2tpbmdTZWN0MiAuYmFycy1sb2NhdGUge1xuICBoZWlnaHQ6IDY2cHg7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kOiBibGFjaztcbiAgY29sb3I6ICNmYmI5MWQgIWltcG9ydGFudDtcbiAgbWFyZ2luLWxlZnQ6IDAlO1xuICBib3JkZXItcmFkaXVzOiAzMHB4O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICB0ZXh0LXRyYW5zZm9ybTogbm9uZSAhaW1wb3J0YW50O1xuICBmb250LXNpemU6IDEuMmVtO1xufVxuaW9uLWNvbnRlbnQgLmJhcnMgLnZlcnRpY2FsTGluZTo6YWZ0ZXIge1xuICB3aWR0aDogMSU7XG4gIGhlaWdodDogNTBweDtcbiAgYmFja2dyb3VuZDogZ3JlZW47XG4gIG1hcmdpbjogYXV0bztcbiAgbWFyZ2luLXRvcDogMDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtYXJnaW4tbGVmdDogMTAlO1xufVxuaW9uLWNvbnRlbnQgLmJhcnMgLmJvb2tpbmdTZWN0IHtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHBhZGRpbmc6IDIlO1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIGhlaWdodDogYXV0bztcbiAgd2lkdGg6IDEwMCU7XG4gIGJvcmRlci1yYWRpdXM6IDBweDtcbiAgbWFyZ2luLWxlZnQ6IDAlO1xuICB0b3A6IDc3JTtcbiAgei1pbmRleDogMztcbn1cbmlvbi1jb250ZW50IC5iYXJzIC5ib29raW5nU2VjdCAuYmFycy1sb2NhdGUge1xuICBoZWlnaHQ6IDY2cHg7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgbWFyZ2luLWxlZnQ6IDAlO1xuICBib3JkZXItcmFkaXVzOiAzMHB4O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBmb250LXNpemU6IDEuNGVtO1xuICBmbG9hdDogbGVmdDtcbn1cbmlvbi1jb250ZW50IC5iYXJzIC5ib29raW5nU2VjdCAuYmFycy1sb2NhdGUtcGluIHtcbiAgd2lkdGg6IGF1dG87XG4gIGZsb2F0OiByaWdodDtcbiAgbWFyZ2luLXRvcDogMSU7XG4gIGJhY2tncm91bmQ6ICNmZGZkZmQ7XG4gIGJvcmRlci1yYWRpdXM6IDMwcHg7XG59XG5pb24tY29udGVudCAuYmFycyAuYm9va2luZ1NlY3QgLmJhcnMtbG9jYXRlLXBpbiBpb24taWNvbiB7XG4gIGZvbnQtc2l6ZTogMS4zZW07XG4gIG1hcmdpbi1sZWZ0OiAyJTtcbiAgY29sb3I6IHJlZDtcbiAgcGFkZGluZzogNXB4O1xuICBhbmltYXRpb24tZGlyZWN0aW9uOiBhbHRlcm5hdGU7XG4gIGFuaW1hdGlvbi1uYW1lOiBsZWZ0X3JpZ2h0MjtcbiAgYW5pbWF0aW9uLWR1cmF0aW9uOiAwLjhzO1xufVxuaW9uLWNvbnRlbnQgLmJhcnMgLmJvb2tpbmdTZWN0IC5iYXJzLWRlc3RpbmF0ZSB7XG4gIGhlaWdodDogNjZweDtcbiAgd2lkdGg6IDEwMCU7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBtYXJnaW4tbGVmdDogMCU7XG4gIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIGZvbnQtc2l6ZTogMS40ZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZmxvYXQ6IGxlZnQ7XG59XG5pb24tY29udGVudCAuYmFycyAuYm9va2luZ1NlY3QgLmJhcnMtZGVzdGluYXRlLXBpbiB7XG4gIHdpZHRoOiBhdXRvO1xuICBmbG9hdDogcmlnaHQ7XG4gIG1hcmdpbi10b3A6IDElO1xuICBiYWNrZ3JvdW5kOiAjZmRmZGZkO1xuICBib3JkZXItcmFkaXVzOiAzMHB4O1xufVxuaW9uLWNvbnRlbnQgLmJhcnMgLmJvb2tpbmdTZWN0IC5iYXJzLWRlc3RpbmF0ZS1waW4gaW9uLWljb24ge1xuICBmb250LXNpemU6IDEuNGVtO1xuICBtYXJnaW4tbGVmdDogMiU7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gIHBhZGRpbmc6IDVweDtcbiAgYW5pbWF0aW9uLWRpcmVjdGlvbjogYWx0ZXJuYXRlO1xuICBhbmltYXRpb24tbmFtZTogbGVmdF9yaWdodDI7XG4gIGFuaW1hdGlvbi1kdXJhdGlvbjogMC44cztcbn1cbmlvbi1jb250ZW50IC5yZXNldCB7XG4gIGhlaWdodDogNSU7XG4gIHdpZHRoOiAxMCU7XG4gIGJhY2tncm91bmQ6ICNiZDIxMjE7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbWFyZ2luLWxlZnQ6IDg1JTtcbiAgdG9wOiAxMiU7XG4gIG1hcmdpbi10b3A6IDAlO1xuICB6LWluZGV4OiAxO1xuICBib3JkZXI6IDFweCBzb2xpZCAjZDhkOGQ4O1xuICBib3JkZXItcmFkaXVzOiAzMHB4O1xuICBmb250LXNpemU6IDEuNWVtO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5pb24tY29udGVudCAucmVzZXQgaW9uLWljb24ge1xuICBtYXJnaW4tdG9wOiA0JTtcbn1cbmlvbi1jb250ZW50IGgzIHtcbiAgY29sb3I6ICMwMDAwMDA7XG4gIGZvbnQtc2l6ZTogMC43ZW07XG4gIGZvbnQtd2VpZ2h0OiAzMDA7XG59XG5pb24tY29udGVudCBoMSB7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICBmb250LXNpemU6IDAuN2VtO1xufVxuaW9uLWNvbnRlbnQgLnN0YWNrIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDAuNWVtO1xuICBjb2xvcjogd2hpdGU7XG59XG5pb24tY29udGVudCAjbXlDb2wge1xuICB3aWR0aDogMCUgIWltcG9ydGFudDtcbiAgcGFkZGluZzogMHB4O1xufVxuaW9uLWNvbnRlbnQgI2lDb2wge1xuICB3aWR0aDogNTAlICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmc6IDBweDtcbn1cbmlvbi1jb250ZW50ICNyaWRlcyB7XG4gIHotaW5kZXg6IDQ7XG4gIGZvbnQtc2l6ZTogM2VtO1xuICBwYWRkaW5nOiA2cHg7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kOiByZ2JhKDI0MCwgMjQwLCAyNDAsIDAuOTIpO1xufVxuaW9uLWNvbnRlbnQgI3JpZGVzX2JpZ2dlciB7XG4gIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gIHotaW5kZXg6IDQ7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNkOGQ4ZDg7XG4gIGhlaWdodDogYXV0bztcbiAgb3ZlcmZsb3c6IHZpc2libGU7XG59XG5pb24tY29udGVudCAuYmFycy1ncHMge1xuICBoZWlnaHQ6IDUwcHg7XG4gIHdpZHRoOiA5MCU7XG4gIGJhY2tncm91bmQ6IHJnYmEoMjQwLCAyNDAsIDI0MCwgMC45Mik7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbWFyZ2luOiAtNTBweCAwIDAgLTUwcHg7XG4gIG1hcmdpbi1sZWZ0OiA1JTtcbiAgdG9wOiAyMCU7XG4gIHotaW5kZXg6IDE7XG4gIGJvcmRlcjogMXB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgbGluZS1oZWlnaHQ6IDUwcHg7XG4gIGZvbnQtc2l6ZTogMS4yZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbn1cbmlvbi1jb250ZW50IC5iYXJzLXBvc2l0aW9uIHtcbiAgaGVpZ2h0OiA1MHB4O1xuICB3aWR0aDogMjAlO1xuICBiYWNrZ3JvdW5kOiByZ2JhKDI0MCwgMjQwLCAyNDAsIDAuOTIpO1xuICBtYXJnaW46IC01MHB4IDAgMCAtNTBweDtcbiAgbWFyZ2luLWxlZnQ6IDUlO1xuICB0b3A6IDIwJTtcbiAgei1pbmRleDogMTtcbiAgYm9yZGVyOiAxcHggc29saWQgcmVkO1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBib3JkZXItcmFkaXVzOiAzMHB4O1xuICBsaW5lLWhlaWdodDogNTBweDtcbiAgZm9udC1zaXplOiAxLjJlbTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBjb2xvcjogcmVkO1xufVxuaW9uLWNvbnRlbnQgLmJhciB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgaGVpZ2h0OiAzNXB4O1xuICB3aWR0aDogNzAlO1xuICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gIG1hcmdpbi1sZWZ0OiAxNSU7XG4gIHotaW5kZXg6IDQ7XG4gIHRvcDogNjAlO1xuICBib3JkZXI6IDFweCBzb2xpZCAjZDhkOGQ4O1xuICBib3JkZXItcmFkaXVzOiAzMHB4O1xuICBmb250LXNpemU6IDEuMWVtO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cbmlvbi1jb250ZW50IC5iYXIgLmljb24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGZvbnQtc2l6ZTogMS42ZW07XG4gIHJpZ2h0OiAyJTtcbiAgcGFkZGluZzogMHB4O1xuICBjb2xvcjogI2ZkZmRmZDtcbiAgYW5pbWF0aW9uLW5hbWU6IGxlZnRfcmlnaHQ7XG4gIGFuaW1hdGlvbi1kdXJhdGlvbjogMC41cztcbiAgYW5pbWF0aW9uLWl0ZXJhdGlvbi1jb3VudDogaW5maW5pdGU7XG4gIGRpc3BsYXk6IGlubGluZTtcbn1cbmlvbi1jb250ZW50IC5iYXIgLnRleHRsZWZ0IHtcbiAgZmxvYXQ6IGxlZnQ7XG4gIGJvcmRlcjogMS40cHggc29saWQgI2ZkZmRmZDtcbiAgcGFkZGluZzogOXB4O1xuICB6LWluZGV4OiAzO1xuICBib3JkZXItcmFkaXVzOiAzMHB4O1xuICBmb250LXNpemU6IDAuOWVtO1xufVxuaW9uLWNvbnRlbnQgLmxlZnRCdXR0b24ge1xuICBmbG9hdDogbGVmdDtcbiAgd2lkdGg6IDEwJTtcbiAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgZm9udC1zaXplOiAxZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG59XG5pb24tY29udGVudCAubGVmdEJ1dHRvbiBoMSB7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG59XG5pb24tY29udGVudCAucHJldmlvdXMge1xuICBtYXJnaW4tdG9wOiA0MHB4O1xufVxuaW9uLWNvbnRlbnQgLmNlbnRlck1hcmtlciB7XG4gIHotaW5kZXg6IDE7XG4gIG1hcmdpbi10b3A6IDQxJSAhaW1wb3J0YW50O1xufVxuaW9uLWNvbnRlbnQgLmNlbnRlck1hcmtlciAubWlkZHkge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDQ1JTtcbiAgbWFyZ2luLWxlZnQ6IDUlO1xuICBiYWNrZ3JvdW5kOiAjMzgzODM4O1xuICBoZWlnaHQ6IDMwcHg7XG4gIHRvcDogMCU7XG4gIG1hcmdpbi10b3A6IDAlO1xuICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiA0cHg7XG4gIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiA0cHg7XG4gIHdpZHRoOiAwLjY1JTtcbn1cbmlvbi1jb250ZW50ICNudWdnZXQge1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIHotaW5kZXg6IDI7XG4gIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgaGVpZ2h0OiAxNXB4O1xuICB3aWR0aDogNCU7XG4gIGxlZnQ6IDQ1JTtcbiAgbWFyZ2luLWxlZnQ6IDMuNCU7XG4gIG1hcmdpbi10b3A6IC00JSAhaW1wb3J0YW50O1xuICBib3JkZXItcmFkaXVzOiA1MHB4O1xufVxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiAxMDI0cHgpIHtcbiAgaW9uLWNvbnRlbnQgI251Z2dldCB7XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICAgIHotaW5kZXg6IDI7XG4gICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xuICAgIGhlaWdodDogMTVweDtcbiAgICB3aWR0aDogNCU7XG4gICAgbGVmdDogNDUlO1xuICAgIG1hcmdpbi1sZWZ0OiAzLjQlO1xuICAgIG1hcmdpbi10b3A6IC0xJSAhaW1wb3J0YW50O1xuICAgIGJvcmRlci1yYWRpdXM6IDUwcHg7XG4gIH1cbn1cbmlvbi1jb250ZW50ICNoaWRlciB7XG4gIG1hcmdpbi1sZWZ0OiA3MyU7XG4gIHRvcDogMyU7XG59XG5pb24tY29udGVudCAjaGlkZXIgLmh1dHRvbiB7XG4gIHBhZGRpbmc6IGF1dG87XG4gIGJvcmRlci1yYWRpdXM6IDQwcHg7XG4gIHdpZHRoOiA1NyU7XG59XG5pb24tY29udGVudCAjaGlkZXIgLmh1dHRvbiBpb24taWNvbiB7XG4gIG1hcmdpbi1sZWZ0OiA1JSAhaW1wb3J0YW50O1xufVxuaW9uLWNvbnRlbnQgLnNjcm9sbCB7XG4gIGhlaWdodDogMTAwJTtcbn1cbmlvbi1jb250ZW50ICNtYXAge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xuICB6LWluZGV4OiAxO1xufVxuaW9uLWNvbnRlbnQgLmRlZmF1bHRfbWFwIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgei1pbmRleDogMTtcbn1cbmlvbi1jb250ZW50IC5tYXBfY2hhbmdlIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgei1pbmRleDogMTtcbn1cbmlvbi1jb250ZW50IC53aWRJY29uIHtcbiAgbWFyZ2luLWxlZnQ6IDg1JTtcbiAgbWFyZ2luLXRvcDogLTEwJTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBmb250LXNpemU6IDE1cHg7XG4gIGNvbG9yOiBibGFjaztcbiAgei1pbmRleDogOTk5OTk5OTk5OTk7XG59XG5pb24tY29udGVudCAubWVzc19kZWZ1YWx0IHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJSAhaW1wb3J0YW50O1xuICB6LWluZGV4OiAxO1xufVxuaW9uLWNvbnRlbnQgLm1lc3MyX3Nob3J0IHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogNTAlICFpbXBvcnRhbnQ7XG4gIHotaW5kZXg6IDE7XG59XG5pb24tY29udGVudCAuYm90dG9tLWJhciB7XG4gIGhlaWdodDogYXV0bztcbiAgd2lkdGg6IDEwMCU7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG1hcmdpbjogLTUwcHggMCAwIC01MHB4O1xuICBtYXJnaW4tbGVmdDogMCU7XG4gIGJvdHRvbTogMCU7XG4gIHotaW5kZXg6IDE7XG4gIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDMwcHg7XG4gIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAzMHB4O1xufVxuaW9uLWNvbnRlbnQgLmJvdHRvbS1iYXIgLmJ1dHQge1xuICBjb2xvcjogbGlnaHRnb2xkZW5yb2R5ZWxsb3c7XG4gIGZvbnQtc2l6ZTogMS4yZW07XG59XG5pb24tY29udGVudCAuYm90dG9tLWJhciAucHJpY2V5IHtcbiAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xuICBtYXJnaW46IDIlO1xuICBwYWRkaW5nOiAxJTtcbiAgZm9udC1zaXplOiAxLjJlbTtcbiAgd2lkdGg6IDMzJTtcbiAgY29sb3I6IHdoaXRlO1xuICB6LWluZGV4OiA5O1xuICBib3JkZXI6IDFweCBzb2xpZCB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gIG1hcmdpbi1sZWZ0OiAzMCU7XG4gIG1hcmdpbi10b3A6IDAlO1xufVxuaW9uLWNvbnRlbnQgLmJvdHRvbS1iYXIgLnRvcGV5IHtcbiAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgbWFyZ2luOiAyJTtcbiAgZm9udC1zaXplOiAwLjllbTtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgbWFyZ2luLXRvcDogOCU7XG4gIG1hcmdpbi1ib3R0b206IDglO1xufVxuaW9uLWNvbnRlbnQgLmJvdHRvbS1iYXIgYnV0dG9uIHtcbiAgbWFyZ2luOiBhdXRvO1xuICBoZWlnaHQ6IDM1cHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbmlvbi1jb250ZW50IC5ib3R0b20tYmFyIC5ib29rZXIge1xuICBib3JkZXItdG9wOiAxcHggc29saWQgcmdiYSgzLCAzLCAzLCAwLjI1Myk7XG4gIGhlaWdodDogYXV0bztcbn1cbmlvbi1jb250ZW50IC5ib3R0b20tYmFyIC5ib29rZXIgLmd1dHRvbiB7XG4gIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gIGhlaWdodDogNTBweDtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgZm9udC1zaXplOiAxLjNlbTtcbn1cbmlvbi1jb250ZW50IC5ib3R0b20tYmFyIC50VGhhIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KSAhaW1wb3J0YW50O1xufVxuaW9uLWNvbnRlbnQgLmJvdHRvbS1iYXIgLmd1dHRvbnEge1xuICBib3JkZXItcmFkaXVzOiAzMHB4O1xuICBoZWlnaHQ6IGF1dG87XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gIGZvbnQtc2l6ZTogMS4zZW07XG4gIHBhZGRpbmc6IDEwcHg7XG59XG5pb24tY29udGVudCAuYm90dG9tLWJhciAudGV4dCB7XG4gIHBhZGRpbmctYm90dG9tOiAwcHg7XG4gIGZvbnQtc2l6ZTogMS4xNmVtO1xuICBjb2xvcjogI2ZkZmRmZDtcbn1cbmlvbi1jb250ZW50ICNteUdyaWQge1xuICBvdmVyZmxvdy14OiBhdXRvICFpbXBvcnRhbnQ7XG4gIHdpZHRoOiBhdXRvICFpbXBvcnRhbnQ7XG59XG5pb24tY29udGVudCAjbXlHcmlkMiB7XG4gIG92ZXJmbG93LXg6IGF1dG8gIWltcG9ydGFudDtcbiAgd2lkdGg6IGF1dG8gIWltcG9ydGFudDtcbn1cbmlvbi1jb250ZW50ICNjYXJ0LWJ0biB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgYmFja2dyb3VuZC1jb2xvcjogcmVkO1xufVxuaW9uLWNvbnRlbnQgI2NhcnQtYmFkZ2Uge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMHB4O1xuICByaWdodDogMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZWQ7XG59XG5pb24tY29udGVudCAuYm90dG9tLWxpc3Qge1xuICBtYXJnaW4tdG9wOiA5MCU7XG4gIG92ZXJmbG93LXg6IGF1dG87XG59XG5pb24tY29udGVudCAuYm90dG9tLWxpc3QgLm15LWdyaWQge1xuICB3aWR0aDogYXV0bztcbn1cbmlvbi1jb250ZW50IC5ib3R0b20tbGlzdCAubXktZ3JpZCAuc3R1ZmZzIHtcbiAgaGVpZ2h0OiA0OC41cHg7XG4gIG1hcmdpbi1sZWZ0OiAzNCU7XG4gIHBhZGRpbmc6IDAlO1xuICB3aWR0aDogMjkuNSU7XG4gIHRvcDogMHB4O1xuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xuICBib3JkZXItcmFkaXVzOiAxMnB4O1xuICBmb250LXNpemU6IDFlbTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB6LWluZGV4OiAzO1xufVxuaW9uLWNvbnRlbnQgLmJvdHRvbS1saXN0IC5teS1ncmlkIC5zdHVmZnMgLml0ZW1QaWNzIHtcbiAgd2lkdGg6IDEwMCU7XG59XG5pb24tY29udGVudCAueWVsbG93QnRuIHtcbiAgY29sb3I6IGJsYWNrICFpbXBvcnRhbnQ7XG59XG5pb24tY29udGVudCAubG9jYSB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBtYXJnaW4tdG9wOiAtMiU7XG59XG5pb24tY29udGVudCAuZG90dCB7XG4gIHdpZHRoOiAyMHB4O1xuICBoZWlnaHQ6IDIwcHg7XG59XG5pb24tY29udGVudCAuY2VudGVyQnRuIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xufVxuaW9uLWNvbnRlbnQgLmVtYWlsYnRuIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwMDAwMDtcbiAgY29sb3I6ICNmYmI5MWQ7XG59XG5pb24tY29udGVudCAuY2hhdEljb24ge1xuICB3aWR0aDogODBweDtcbiAgaGVpZ2h0OiA0MHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG1hcmdpbi1sZWZ0OiAtMjAlO1xuICBtYXJnaW4tdG9wOiAtMjclO1xufVxuaW9uLWNvbnRlbnQgLmxpc3RSaWRlcyB7XG4gIG1hcmdpbi1yaWdodDogMDtcbiAgbWFyZ2luLXRvcDogMHB4O1xuICBtYXJnaW4tYm90dG9tOiAwO1xuICBwYWRkaW5nLWxlZnQ6IDA7XG4gIHBhZGRpbmctcmlnaHQ6IDA7XG4gIHBhZGRpbmctdG9wOiAwcHg7XG4gIHBhZGRpbmctYm90dG9tOiA4cHg7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG4gIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDMwcHg7XG4gIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAzMHB4O1xuICB3aWR0aDogOTAlO1xuICBtYXJnaW4tbGVmdDogNSU7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYm90dG9tOiAwcHg7XG4gIHBhZGRpbmctdG9wOiAxNSU7XG4gIHotaW5kZXg6IDMzMztcbn1cbmlvbi1jb250ZW50IC5kcml2ZXJGb3VuZCB7XG4gIGhlaWdodDogNTYlO1xuICB3aWR0aDogOTUlO1xuICBtYXJnaW4tbGVmdDogMS41JTtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYm90dG9tOiAwJTtcbiAgei1pbmRleDogMTtcbiAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMzBweDtcbiAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDMwcHg7XG59XG5pb24tY29udGVudCAuaGVhZFNlY3Rpb24ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmJiOTFkO1xuICBjb2xvcjogYmxhY2s7XG4gIG1hcmdpbi10b3A6IC02JTtcbiAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMjdweDtcbiAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDMwcHg7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogNDUlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5pb24tY29udGVudCAuaGVhZFNlY3Rpb25fZHJpdmVyX2Fycml2ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xuICBjb2xvcjogI2ZiYjkxZDtcbiAgbWFyZ2luLXRvcDogLTYlO1xuICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAyN3B4O1xuICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMzBweDtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiA0NSU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbmlvbi1jb250ZW50IC5idXR0b25fc3R5bGUge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGJvcmRlci1yYWRpdXM6IDYwcHg7XG4gIGhlaWdodDogNDBweDtcbiAgd2lkdGg6IDYwJTtcbiAgY29sb3I6IHdoaXRlO1xuICBtYXJnaW4tbGVmdDogMjAlO1xufVxuaW9uLWNvbnRlbnQgLm1vdmVIZWFkZXIge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgbWFyZ2luLXRvcDogMjBweDtcbiAgbWFyZ2luLWxlZnQ6IDBweDtcbiAgbWFyZ2luLXJpZ2h0OiAwcHg7XG59XG5pb24tY29udGVudCAjaWNvblNpemUge1xuICBmb250LXNpemU6IDYwcHggIWltcG9ydGFudDtcbn1cbmlvbi1jb250ZW50ICNkcml2ZXJpY29uU2l6ZSB7XG4gIGZvbnQtc2l6ZTogNzBweCAhaW1wb3J0YW50O1xufVxuaW9uLWNvbnRlbnQgI2g0NCB7XG4gIGZvbnQtc2l6ZTogMTZweDtcbn1cbmlvbi1jb250ZW50ICNwNCB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbn1cbmlvbi1jb250ZW50ICNsb2NTaXplIHtcbiAgZm9udC1zaXplOiAxNnB4ICFpbXBvcnRhbnQ7XG59XG5pb24tY29udGVudCAucmVzdWx0Q29udGFpbmVyIHtcbiAgd2lkdGg6IDEwMCU7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbGVmdDogMDtcbiAgbWFyZ2luLXRvcDogMzBweDtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgcGFkZGluZy1yaWdodDogNSU7XG4gIHBhZGRpbmctbGVmdDogNSU7XG4gIGJvcmRlci1ib3R0b206ICNiYWJhYmEgc29saWQgMXB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbn1cbmlvbi1jb250ZW50IC5yZXN1bHRDb250YWluZXIyIHtcbiAgd2lkdGg6IDEwMCU7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbGVmdDogMDtcbiAgbWFyZ2luLXRvcDogMHB4O1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xufVxuaW9uLWNvbnRlbnQgLmJvb2tJbWFnZSxcbmlvbi1jb250ZW50IC5ib29rVGl0bGUsXG5pb24tY29udGVudCAuYm9va1ByaWNlIHtcbiAgbWFyZ2luLWxlZnQ6IDIwcHg7XG59XG5pb24tY29udGVudCAuY29udGVudC13cmFwLFxuaW9uLWNvbnRlbnQgLmltZy13cmFwcGVyIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBtYXJnaW4tbGVmdDogMHB4O1xufVxuaW9uLWNvbnRlbnQgLmNvbnRlbnQtd3JhcDIge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG59XG5pb24tY29udGVudCAubWFwSGVpZ2h0IHtcbiAgaGVpZ2h0OiA1MCU7XG4gIHdpZHRoOiAxMDAlO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5pb24tY29udGVudCAuY2FyYm90dG9tLWJhcjIge1xuICBoZWlnaHQ6IDQ1JTtcbiAgd2lkdGg6IDEwMCU7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJvdHRvbTogMCU7XG4gIHotaW5kZXg6IDE7XG4gIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDMwcHg7XG4gIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAzMHB4O1xufVxuaW9uLWNvbnRlbnQgLmNhcmJvdHRvbS1iYXIyIC5idXR0IHtcbiAgY29sb3I6IGxpZ2h0Z29sZGVucm9keWVsbG93O1xuICBmb250LXNpemU6IDEuMmVtO1xufVxuaW9uLWNvbnRlbnQgLmNhcmJvdHRvbS1iYXIyIC5ib29rZXIge1xuICBib3JkZXItdG9wOiAxcHggc29saWQgcmdiYSgzLCAzLCAzLCAwLjI1Myk7XG4gIGhlaWdodDogYXV0bztcbn1cbmlvbi1jb250ZW50IC5jYXJib3R0b20tYmFyMiAudFRoYSB7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSkgIWltcG9ydGFudDtcbn1cbmlvbi1jb250ZW50IC5jYXJib3R0b20tYmFyMiAudGV4dCB7XG4gIHBhZGRpbmctYm90dG9tOiAwcHg7XG4gIGZvbnQtc2l6ZTogMS4xNmVtO1xuICBjb2xvcjogI2ZkZmRmZDtcbn1cbmlvbi1jb250ZW50IC5ib3R0b20tYmFyMiB7XG4gIGhlaWdodDogNTAlO1xuICB3aWR0aDogOTUlO1xuICBtYXJnaW4tbGVmdDogMS41JTtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYm90dG9tOiAwJTtcbiAgei1pbmRleDogMTtcbiAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMzBweDtcbiAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDMwcHg7XG59XG5pb24tY29udGVudCAuYm90dG9tLWJhcjIgLmJ1dHQge1xuICBjb2xvcjogbGlnaHRnb2xkZW5yb2R5ZWxsb3c7XG4gIGZvbnQtc2l6ZTogMS4yZW07XG59XG5pb24tY29udGVudCAuYm90dG9tLWJhcjIgLmJvb2tlciB7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCByZ2JhKDMsIDMsIDMsIDAuMjUzKTtcbiAgaGVpZ2h0OiBhdXRvO1xufVxuaW9uLWNvbnRlbnQgLmJvdHRvbS1iYXIyIC50VGhhIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KSAhaW1wb3J0YW50O1xufVxuaW9uLWNvbnRlbnQgLmJvdHRvbS1iYXIyIC50ZXh0IHtcbiAgcGFkZGluZy1ib3R0b206IDBweDtcbiAgZm9udC1zaXplOiAxLjE2ZW07XG4gIGNvbG9yOiAjZmRmZGZkO1xufVxuaW9uLWNvbnRlbnQgLmJvdHRvbTMtY29udGFpbmVyIHtcbiAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMzBweDtcbiAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDMwcHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbmlvbi1jb250ZW50IC5ib3R0b20zLWNvbnRhaW5lciAuYm90dG9tLWJhcjMge1xuICBoZWlnaHQ6IDEwMHB4IGF1dG87XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDMwcHg7XG4gIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAzMHB4O1xuICBtYXJnaW46IDBweCAwIDAgMHB4O1xuICBtYXJnaW4tbGVmdDogMCU7XG4gIGJvdHRvbTogMTUlO1xuICB6LWluZGV4OiAxO1xufVxuaW9uLWNvbnRlbnQgLmJvdHRvbTMtY29udGFpbmVyIC5ib3R0b20tYmFyMyAubXlJbWFnZSB7XG4gIHotaW5kZXg6IDA7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgbGVmdDogMCU7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWI0MjAwO1xufVxuaW9uLWNvbnRlbnQgLmJvdHRvbTMtY29udGFpbmVyIC5ib3R0b20tYmFyMyAucHJvZmlsZS1waWMge1xuICB3aWR0aDogNWVtO1xuICBoZWlnaHQ6IDVlbTtcbiAgYm9yZGVyOiAxcHggc29saWQgIzNlM2MzYztcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcbn1cbmlvbi1jb250ZW50IC5ib3R0b20zLWNvbnRhaW5lciAuYm90dG9tLWJhcjMgLmxvZ29zIHtcbiAgcGFkZGluZzogMHB4O1xufVxuaW9uLWNvbnRlbnQgLmJvdHRvbTMtY29udGFpbmVyIC5ib3R0b20tYmFyMyAubG9nb3MgaW9uLWljb24ge1xuICBmb250LXNpemU6IDEuMWVtO1xuICBjb2xvcjogI2ZiYjkxZDtcbn1cbmlvbi1jb250ZW50IC5ib3R0b20zLWNvbnRhaW5lciAuYm90dG9tLWJhcjMgLnRleHQge1xuICBwYWRkaW5nOiAwcHg7XG4gIGZvbnQtc2l6ZTogMS40ZW07XG4gIG1hcmdpbi10b3A6IDYlO1xuICBjb2xvcjogI2ZkZmRmZDtcbn1cbmlvbi1jb250ZW50IC5ib3R0b20zLWNvbnRhaW5lciAuYm90dG9tLWJhcjMgaW9uLWljb24ge1xuICBmb250LXNpemU6IDIuM2VtO1xuICBjb2xvcjogI2ZiYjkxZDtcbn1cblxuI2J1dHQge1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xuICBtYXJnaW4tdG9wOiA4JTtcbiAgbWFyZ2luLWJvdHRvbTogOCU7XG4gIGZvbnQtc2l6ZTogMS4zZW07XG4gIHBhZGRpbmc6IDdweDtcbn1cblxuI2VudmVsb3BlIHtcbiAgaGVpZ2h0OiBhdXRvO1xuICB3aWR0aDogNWVtO1xuICBtYXJnaW4tbGVmdDogNDAlO1xuICBtYXJnaW4tdG9wOiA1JTtcbn1cblxuI2VudmVsb3Age1xuICBoZWlnaHQ6IDVlbTtcbiAgd2lkdGg6IDVlbTtcbn1cblxuI2J1dHRvbkNvbnRhaW5lciB7XG4gIGZsb2F0OiByaWdodDtcbn1cbiNidXR0b25Db250YWluZXIgYnV0dG9uIHtcbiAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xuICBib3JkZXItcmFkaXVzOiAxMnB4O1xuICBjb2xvcjogd2hpdGU7XG4gIG1hcmdpbjogNnB4O1xuICBwYWRkaW5nOiAxNXB4O1xufVxuXG4jYnV0dG9uQ29udGFpbmVyMiB7XG4gIGZsb2F0OiBsZWZ0O1xufVxuI2J1dHRvbkNvbnRhaW5lcjIgYnV0dG9uIHtcbiAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xuICBib3JkZXItcmFkaXVzOiAxMnB4O1xuICBjb2xvcjogd2hpdGU7XG4gIG1hcmdpbjogYXV0bztcbiAgcGFkZGluZzogMTVweDtcbn1cblxuI3BheSB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IGF1dG87XG4gIG1hcmdpbjogMSU7XG4gIG1hcmdpbi1sZWZ0OiAyJTtcbiAgZm9udC1zaXplOiAxLjZlbTtcbiAgcGFkZGluZzogMTBweDtcbn1cbiNwYXkgaW9uLWJhZGdlIHtcbiAgaGVpZ2h0OiA3MCU7XG4gIGZvbnQtc2l6ZTogMS42ZW07XG59XG5cbiNwYXkyIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogYXV0bztcbiAgbWFyZ2luOiAxJTtcbiAgbWFyZ2luLWxlZnQ6IDIlO1xuICBwYWRkaW5nOiA0cHg7XG59XG4jcGF5MiBpb24tYmFkZ2Uge1xuICBoZWlnaHQ6IDcwJTtcbiAgZm9udC1zaXplOiAxLjZlbTtcbn1cblxuLm15Q2VudHJlIHtcbiAgei1pbmRleDogMztcbiAgbWFyZ2luLXRvcDogNjUlO1xufVxuXG4jY29udHJvbC1idXR0b25zIHtcbiAgZm9udC1zaXplOiAxLjhlbTtcbn1cbiNjb250cm9sLWJ1dHRvbnMgaW9uLWljb24ge1xuICBwYWRkaW5nOiAxNnB4O1xufVxuXG4jdG9wLWJ1dHRvbnMge1xuICBoZWlnaHQ6IGF1dG87XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbWFyZ2luOiAtNTBweCAwIDAgLTUwcHg7XG4gIG1hcmdpbi1sZWZ0OiAwJTtcbiAgYm90dG9tOiAwJTtcbiAgei1pbmRleDogMTtcbiAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMzBweDtcbiAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDMwcHg7XG59XG4jdG9wLWJ1dHRvbnMgLmxvd2J1dHRvbiB7XG4gIGJvcmRlci1yYWRpdXM6IDMwcHg7XG59XG4jdG9wLWJ1dHRvbnMgaW9uLWljb24ge1xuICBmb250LXNpemU6IDFlbTtcbiAgY29sb3I6ICMwMzAzMDM7XG59XG5cbiNtYXAtY2FudmFzIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgei1pbmRleDogMDtcbn1cblxuI2hlYWRlciB7XG4gIGZvbnQtc2l6ZTogMS4zMmVtO1xuICBmb250LXdlaWdodDogMjAwO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogMHB4ICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiAwcHggIWltcG9ydGFudDtcbiAgY29sb3I6ICNmZGZkZmQ7XG59XG5cbiNsb2NhdGlvbiB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcGFkZGluZy1sZWZ0OiAxN3B4O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICB0ZXh0LXRyYW5zZm9ybTogbm9uZSAhaW1wb3J0YW50O1xufVxuXG4jZGVzdGluYXRpb24ge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHBhZGRpbmctbGVmdDogMTdweDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmUgIWltcG9ydGFudDtcbn1cblxuLmxvY2F0ZV9idXR0b24ge1xuICB6LWluZGV4OiAzO1xuICBmbG9hdDogcmlnaHQ7XG4gIG1hcmdpbi10b3A6IDYlO1xuICB3aWR0aDogYXV0bztcbiAgaGVpZ2h0OiA0MHB4O1xuICBtYXJnaW4tcmlnaHQ6IDQuNSU7XG4gIGJvcmRlci1yYWRpdXM6IDYwcHg7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xufVxuLmxvY2F0ZV9idXR0b24gaW9uLWljb24ge1xuICBtYXJnaW4tdG9wOiAtMiU7XG4gIGJvcmRlci1yYWRpdXM6IDYwcHg7XG59XG5cbi5yZXF1ZXN0LWZvci1yaWRlIHtcbiAgbWFyZ2luLXRvcDogMCUgIWltcG9ydGFudDtcbn1cblxuLnJlcXVlc3QtZm9yLXJpZGUyIHtcbiAgaGVpZ2h0OiAyMDBweDtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG59XG5cbi5saW5lSGVpZ2h0IHtcbiAgbGluZS1oZWlnaHQ6IDE7XG59XG5cbi5ib3R0dW0ge1xuICBiYWNrZ3JvdW5kOiAjZmRmZGZkO1xuICBib3JkZXItcmFkaXVzOiA2MHB4O1xufVxuLmJvdHR1bSBpb24taWNvbiB7XG4gIGZvbnQtc2l6ZTogMS43ZW07XG59XG5cbiNyaWRlcyB7XG4gIGJvcmRlci1yYWRpdXM6IDBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2Q4ZDhkODtcbn1cbiNyaWRlcyBpb24tYmFkZ2Uge1xuICBmb250LXNpemU6IDFlbTtcbn1cblxuLmJ1dHRvbi1yaWdodCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogNzUlO1xuICBwYWRkaW5nOiAxNnB4O1xuICB0b3A6IDYwJTtcbn1cblxuLmJ1dHRvbk5ldyB7XG4gIGJvcmRlcjogNXB4IHNvbGlkICNmYmI5MWQ7XG4gIHdpZHRoOiA2MCU7XG4gIGxlZnQ6IDUwJTtcbiAgbWFyZ2luOiAwO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogNTAlO1xuICBsZWZ0OiA1MCU7XG4gIC1tcy10cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG59XG5cbmlvbi1mb290ZXIgI2xvd2VyX2l0ZW1zIHtcbiAgaGVpZ2h0OiBhdXRvO1xuICB3aWR0aDogMTAwJTtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHotaW5kZXg6IDE1O1xufVxuaW9uLWZvb3RlciAuYnV0dG9ucyB7XG4gIG1hcmdpbi10b3A6IDEwJSAhaW1wb3J0YW50O1xuICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiA1cHg7XG4gIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiA1cHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luOiBhdXRvO1xuICBwYWRkaW5nOiBhdXRvICFpbXBvcnRhbnQ7XG4gIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgei1pbmRleDogMzAwICFpbXBvcnRhbnQ7XG4gIGJvcmRlcjogMC43cHggc29saWQgdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMTdweDtcbn1cbmlvbi1mb290ZXIgI3JpZGVzIHtcbiAgYm9yZGVyLXJhZGl1czogMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjZDhkOGQ4O1xufVxuaW9uLWZvb3RlciAjcmlkZXMgaW9uLWJhZGdlIHtcbiAgZm9udC1zaXplOiAxLjZlbTtcbn1cblxuI3ByaWNlIHtcbiAgZm9udC1zaXplOiAxZW07XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG59XG4jcHJpY2UgaDIge1xuICBmb250LXNpemU6IDAuNjNlbTtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbn1cblxuQGtleWZyYW1lcyBhbmltYXRlZEJhciB7XG4gIDAlIHtcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiAtNDA2cHggMDtcbiAgfVxuICAxMDAlIHtcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiAwIDA7XG4gIH1cbn1cbi5mYWtlSXRlbSBoMiwgLmZha2VJdGVtIGgzLCAuZmFrZUl0ZW0gaDQsIC5mYWtlSXRlbSBwIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogbGlnaHRncmV5O1xuICBvcGFjaXR5OiAwLjU7XG4gIGhlaWdodDogMWVtO1xuICBtYXJnaW4tdG9wOiAxMHB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgI2RkZGRkZCwgI0VFRUVFRSk7XG4gIGFuaW1hdGlvbjogYW5pbWF0ZWRCYXIgNzAwbXMgbGluZWFyIGluZmluaXRlO1xufVxuLmZha2VJdGVtIGgyOmFmdGVyLCAuZmFrZUl0ZW0gaDM6YWZ0ZXIsIC5mYWtlSXRlbSBoNDphZnRlciwgLmZha2VJdGVtIHA6YWZ0ZXIge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBjb250ZW50OiBcIlwiO1xuICByaWdodDogMDtcbiAgYm90dG9tOiAwO1xuICB0b3A6IDA7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xufVxuLmZha2VJdGVtIC5UYXhpSUNvbjIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBsaWdodGdyZXk7XG4gIG9wYWNpdHk6IDAuNTtcbiAgaGVpZ2h0OiAxZW07XG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjZGRkZGRkLCAjRUVFRUVFKTtcbiAgYW5pbWF0aW9uOiBhbmltYXRlZEJhciA3MDBtcyBsaW5lYXIgaW5maW5pdGU7XG59XG4uZmFrZUl0ZW0gLlRheGlJQ29uMjphZnRlciB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIHJpZ2h0OiAwO1xuICBib3R0b206IDA7XG4gIHRvcDogMDtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG59XG4uZmFrZUl0ZW0gaDI6YWZ0ZXIge1xuICB3aWR0aDogNjUlO1xufVxuLmZha2VJdGVtIGgzOmFmdGVyIHtcbiAgd2lkdGg6IDYwJTtcbn1cbi5mYWtlSXRlbSBwOmFmdGVyIHtcbiAgd2lkdGg6IDQwJTtcbn1cblxuLmxvYWRlciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgdG9wOiBjYWxjKDAlKTtcbiAgbGVmdDogY2FsYyg1MCUgLSAzMnB4KTtcbiAgd2lkdGg6IDY0cHg7XG4gIGhlaWdodDogNjRweDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBwZXJzcGVjdGl2ZTogODAwcHg7XG59XG5cbi5pbm5lciB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xufVxuXG4uaW5uZXIub25lIHtcbiAgbGVmdDogMCU7XG4gIHRvcDogMCU7XG4gIGFuaW1hdGlvbjogcm90YXRlLW9uZSAxcyBsaW5lYXIgaW5maW5pdGU7XG4gIGJvcmRlci1ib3R0b206IDNweCBzb2xpZCAjZmJiOTFkO1xufVxuXG4uaW5uZXIudHdvIHtcbiAgcmlnaHQ6IDAlO1xuICB0b3A6IDAlO1xuICBhbmltYXRpb246IHJvdGF0ZS10d28gMXMgbGluZWFyIGluZmluaXRlO1xuICBib3JkZXItcmlnaHQ6IDNweCBzb2xpZCAjZDJhZThhO1xufVxuXG4uaW5uZXIudGhyZWUge1xuICByaWdodDogMCU7XG4gIGJvdHRvbTogMCU7XG4gIGFuaW1hdGlvbjogcm90YXRlLXRocmVlIDFzIGxpbmVhciBpbmZpbml0ZTtcbiAgYm9yZGVyLXRvcDogM3B4IHNvbGlkICNmYmI5MWQ7XG59XG5cbkBrZXlmcmFtZXMgcm90YXRlLW9uZSB7XG4gIDAlIHtcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZVgoMzVkZWcpIHJvdGF0ZVkoLTQ1ZGVnKSByb3RhdGVaKDBkZWcpO1xuICB9XG4gIDEwMCUge1xuICAgIHRyYW5zZm9ybTogcm90YXRlWCgzNWRlZykgcm90YXRlWSgtNDVkZWcpIHJvdGF0ZVooMzYwZGVnKTtcbiAgfVxufVxuQGtleWZyYW1lcyByb3RhdGUtdHdvIHtcbiAgMCUge1xuICAgIHRyYW5zZm9ybTogcm90YXRlWCg1MGRlZykgcm90YXRlWSgxMGRlZykgcm90YXRlWigwZGVnKTtcbiAgfVxuICAxMDAlIHtcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZVgoNTBkZWcpIHJvdGF0ZVkoMTBkZWcpIHJvdGF0ZVooMzYwZGVnKTtcbiAgfVxufVxuQGtleWZyYW1lcyByb3RhdGUtdGhyZWUge1xuICAwJSB7XG4gICAgdHJhbnNmb3JtOiByb3RhdGVYKDM1ZGVnKSByb3RhdGVZKDU1ZGVnKSByb3RhdGVaKDBkZWcpO1xuICB9XG4gIDEwMCUge1xuICAgIHRyYW5zZm9ybTogcm90YXRlWCgzNWRlZykgcm90YXRlWSg1NWRlZykgcm90YXRlWigzNjBkZWcpO1xuICB9XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/home/home.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/home/home.page.ts ***!
  \*****************************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_call_number_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/call-number/ngx */ "./node_modules/@ionic-native/call-number/ngx/index.js");
/* harmony import */ var _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/google-maps */ "./node_modules/@ionic-native/google-maps/index.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.cjs.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _ionic_native_vibration_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/vibration/ngx */ "./node_modules/@ionic-native/vibration/ngx/index.js");
/* harmony import */ var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/status-bar/ngx */ "./node_modules/@ionic-native/status-bar/ngx/index.js");
/* harmony import */ var _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/onesignal/ngx */ "./node_modules/@ionic-native/onesignal/ngx/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var src_app_services_language_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! src/app/services/language.service */ "./src/app/services/language.service.ts");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var src_app_services_activity_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! src/app/services/activity.service */ "./src/app/services/activity.service.ts");
/* harmony import */ var src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! src/app/services/settings.service */ "./src/app/services/settings.service.ts");
/* harmony import */ var src_app_services_native_map_container_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! src/app/services/native-map-container.service */ "./src/app/services/native-map-container.service.ts");
/* harmony import */ var src_app_services_geocoder_service__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! src/app/services/geocoder.service */ "./src/app/services/geocoder.service.ts");
/* harmony import */ var src_app_services_directionservice_service__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! src/app/services/directionservice.service */ "./src/app/services/directionservice.service.ts");
/* harmony import */ var src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! src/app/services/profile.service */ "./src/app/services/profile.service.ts");
/* harmony import */ var src_app_services_onesignal_service__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! src/app/services/onesignal.service */ "./src/app/services/onesignal.service.ts");
/* harmony import */ var src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! src/app/services/pop-up.service */ "./src/app/services/pop-up.service.ts");
/* harmony import */ var src_app_services_event_service__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! src/app/services/event.service */ "./src/app/services/event.service.ts");
/* harmony import */ var src_app_pages_autocomplete_autocomplete_page__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! src/app/pages/autocomplete/autocomplete.page */ "./src/app/pages/autocomplete/autocomplete.page.ts");
/* harmony import */ var src_app_pages_chat_chat_page__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! src/app/pages/chat/chat.page */ "./src/app/pages/chat/chat.page.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _driver_info_driver_info_page__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ../driver-info/driver-info.page */ "./src/app/pages/driver-info/driver-info.page.ts");
/* harmony import */ var src_app_pages_rate_rate_page__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! src/app/pages/rate/rate.page */ "./src/app/pages/rate/rate.page.ts");
/* harmony import */ var _someone_someone_page__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ../../someone/someone.page */ "./src/app/someone/someone.page.ts");
/* harmony import */ var _trip_info_trip_info_page__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ../trip-info/trip-info.page */ "./src/app/pages/trip-info/trip-info.page.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");
/* harmony import */ var _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! @ionic-native/social-sharing/ngx */ "./node_modules/@ionic-native/social-sharing/ngx/index.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_32___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_32__);


































// import { addSeconds, format } from "date-fns";

let HomePage = class HomePage {
    constructor(share, storage, http, lp, authProvider, One, act, settings, statusBar, loadingCtrl, vibration, alert, cMap, call1, myGcode, dProvider, platform, OneSignal, modalCtrl, menu, pop, ph, navCtrl, eventProvider, router, zone, actionSheetController, geo, cdr) {
        this.share = share;
        this.storage = storage;
        this.http = http;
        this.lp = lp;
        this.authProvider = authProvider;
        this.One = One;
        this.act = act;
        this.settings = settings;
        this.statusBar = statusBar;
        this.loadingCtrl = loadingCtrl;
        this.vibration = vibration;
        this.alert = alert;
        this.cMap = cMap;
        this.call1 = call1;
        this.myGcode = myGcode;
        this.dProvider = dProvider;
        this.platform = platform;
        this.OneSignal = OneSignal;
        this.modalCtrl = modalCtrl;
        this.menu = menu;
        this.pop = pop;
        this.ph = ph;
        this.navCtrl = navCtrl;
        this.eventProvider = eventProvider;
        this.router = router;
        this.zone = zone;
        this.actionSheetController = actionSheetController;
        this.geo = geo;
        this.cdr = cdr;
        this.starter$ = new rxjs__WEBPACK_IMPORTED_MODULE_10__["Subject"]();
        this.notify_ID = "43cd6829-4651-4039-bbc3-aace7fbe7d72";
        this.type = "arrow-up";
        this.math = Math;
        this.startedNavigation = false;
        this.booked_for_someone = false;
        this.refreshedTimes = 0;
        this.canShowBars = true;
        this.rideSelected = false;
        this.hasPaused = false;
        this.hasResumed = false;
        this.geocoder = new google.maps.Geocoder();
        this.connect_change = true;
        this.driverFound = true;
        this.pickedup = true;
        this.droppedoff = true;
        this.hasPaid = true;
        this.isChecking = true;
        this.isDriverEnded = true;
        this.canSwoop = true;
        this.canEnd = true;
        this.dest = "Your Destination";
        this.rating_positive = 0;
        this.rating_negative = 0;
        this.isDone = true;
        this.isRoute = true;
        this.tolls = 0;
        this.surcharges = 0;
        this.myData = [];
        this.hasTimed = true;
        this.canIncur = false;
        //timeLeft: number = 120;
        this.timeLeft = 60;
        this.myTolls = [];
        this.toller = [];
        this.allChat = [];
        this.justHide = true;
        this.driverGlobalPercentSurcharge = 0;
        this.riderGlobalSurcharge = 0;
        this.driverGlobalFlatSurcharge = 0;
        this.riderGlobalPercentSurcharge = 0;
        this.riderGlobalFlatSurcharge = 0;
        this.riderZipcodeFlatSurcharge = 0;
        this.riderZipcodePercentSurcharge = 0;
        this.driverZipcodeFlatSurcharge = 0;
        this.driverZipcodePercentSurcharge = 0;
        this.mySurch = [];
        this.totalRiderSurcharge = 0;
        this.totalDriverSurcharge = 0;
        this.result3 = 0;
        this.tollTems = [];
        this.toll_1_value = 0;
        this.allSurcharges = 0;
        this.myg = [];
        this.secs = 300;
        this.pauseCost = 0;
        this.setBorderColor = false;
        this.hideDriverFound = false;
        this.isComplete = false;
        this.defaultMap = false;
        this.shortMap = false;
        this.test2 = false;
        this.seconds = 1;
        this.minutes = 0;
        this.clock_minutes = 0;
        this.hours = 0;
        this.platform.ready().then(() => {
            // this.keepScreenAlive();
            // this.backgroundMode.enable();
            this.cMap.loadMap();
            this.WaitForGeolocation();
            this.subscribeToOnesignal();
            this.allStorage();
        });
        this.subscribeToOnesignal();
        this.getPrice();
    }
    // keepScreenAlive() {
    //   this.insomnia.keepAwake()
    //     .then(
    //       () => console.log('KEEPING AWAKE success'),
    //       () => console.log('KEEPING AWAKE error')
    //     );
    // }
    completeItem() {
        this.defaultMap = !this.defaultMap;
        this.shortMap = !this.shortMap;
        this.storage.set("shortMap", this.shortMap);
        this.storage.set("defaultMap", this.defaultMap);
        this.cdr.detectChanges();
    }
    ionViewDidEnter() {
        this.audioPlayer = new Audio('/assets/sounds/cancelalert.mp3');
        this.subscribeToOnesignal();
        this.allStorage();
    }
    allStorage() {
        this.storage.get("cMap.onbar3").then((value) => {
            if (value == null) {
            }
            else {
                this.cMap.onbar3 = value;
            }
        });
        this.storage.get("defaultMap").then((value) => {
            if (value == null) {
            }
            else {
                this.defaultMap = value;
            }
        });
        this.storage.get("driver_connected").then((value) => {
            if (value == null) {
            }
            else {
                this.driver_connected = value;
            }
        });
        this.storage.get("hasPaused").then((value) => {
            if (value == null) {
            }
            else {
                this.hasPaused = value;
            }
        });
        this.storage.get("destination_time").then((value) => {
            if (value == null) {
            }
            else {
                this.dProvider.destination_time = value;
            }
        });
        this.storage.get("driver_arrived").then((value) => {
            if (value == null) {
            }
            else {
                this.driver_arrived = value;
                // if (this.driver_arrived = true && !this.dProvider.destination_time) {
                //   this.startCountdown()
                // }
            }
        });
        this.storage.get("arriving_time").then((value) => {
            if (value == null) {
            }
            else {
                this.dProvider.arriving_time = value;
            }
        });
        this.storage.get("carType").then((value) => {
            if (value == null) {
            }
            else {
                this.carType = value;
            }
        });
        this.storage.get("plate").then((value) => {
            if (value == null) {
            }
            else {
                this.plate = value;
            }
        });
        this.storage.get("name").then((value) => {
            if (value == null) {
            }
            else {
                this.name = value;
            }
        });
        this.storage.get("cMap.lat").then((value) => {
            if (value == null) {
            }
            else {
                this.cMap.lat = value;
            }
        });
        this.storage.get("cMap.lng").then((value) => {
            if (value == null) {
            }
            else {
                this.cMap.lng = value;
            }
        });
        this.storage.get("detination_words").then((value) => {
            if (value == null) {
                console.log("DESTINATION NULL:::=>>", value);
            }
            else {
                this.detination_words = value;
                console.log("DESTINATION VALUE:::=>>", this.detination_words);
            }
        });
        this.storage.get("dProvider.price").then((value) => {
            if (value == null) {
                console.log("dProvider.price:::=>>", value);
            }
            else {
                this.dProvider.price = value;
                console.log("dProvider.price:::=>>", this.dProvider.price);
            }
        });
        this.storage.get("price").then((value) => {
            if (value == null) {
                console.log("dProvider.price:::=>>", value);
            }
            else {
                console.log("dProvider.price:::=>>", this.dProvider.price);
                this.dProvider.price = value;
            }
        });
        this.storage.get("timeToReach").then((value) => {
            if (value == null) {
            }
            else {
                this.dProvider.timeToReach = value;
            }
        });
        this.storage.get("duration").then((value) => {
            if (value == null) {
            }
            else {
                this.dProvider.duration = value;
            }
        });
        this.storage.get("cMap.selected_destination_bar").then((value) => {
            if (value == null) {
            }
            else {
                this.cMap.selected_destination_bar = value;
            }
        });
        this.storage.get("bull").then((value) => {
            if (value == null) {
            }
            else {
                this.bull = value;
            }
        });
        this.storage.get("uid").then((value) => {
            if (value == null) {
            }
            else {
                this.uid = value;
            }
        });
        this.storage.get("driver_number").then((value) => {
            if (value == null) {
            }
            else {
                this.number = value;
            }
        });
        this.storage.get("rideSelected").then((value) => {
            if (value == null) {
            }
            else {
                this.rideSelected = value;
            }
        });
        this.storage.get("returningUser").then((value) => {
            if (value == null) {
            }
            else {
                this.returningUser = value;
                if (this.returningUser == true) {
                    this.startListeningForResponse();
                }
            }
        });
        this.storage.get("connect_change").then((value) => {
            if (value == null) {
            }
            else {
                this.connect_change = value;
            }
        });
        this.storage.get("StopLocUpdate").then((value) => {
            if (value == null) {
            }
            else {
                this.cMap.StopLocUpdate = value;
            }
        });
        this.storage.get("cMap.hasCompleted").then((value) => {
            if (value == null) {
            }
            else {
                this.cMap.hasCompleted = value;
            }
        });
        this.storage.get("cMap.isPickedUp").then((value) => {
            if (value == null) {
            }
            else {
                this.cMap.isPickedUp = value;
            }
        });
        this.storage.get("shortMap").then((value) => {
            if (value == null) {
            }
            else {
                this.shortMap = value;
            }
        });
        this.storage.get("demo").then((value) => {
            if (value == null) {
            }
            else {
                var demo;
                demo = value;
            }
        });
        this.storage.get("pickUpLocation").then((value) => {
            if (value == null) {
            }
            else {
                this.pickUpLocation = value;
            }
        });
        this.storage.get("myGcode.locationName").then((value) => {
            if (value == null) {
            }
            else {
                this.myGcode.locationName = value;
            }
        });
        this.storage.get("cMap.canShowchoiceTab").then((value) => {
            if (value == null) {
                console.log("canShowchoiceTab NULL ======>>>>>>>", value);
                console.log("canShowchoiceTab NULL 2 ======>>>>>>>", this.cMap.canShowchoiceTab);
            }
            else {
                this.cMap.canShowchoiceTab = value;
                console.log("canShowchoiceTab VALUE ======>>>>>>>", this.cMap.canShowchoiceTab);
            }
        });
        this.storage.get("pop.onRequest").then((value) => {
            if (value == null) {
                console.log("POP REQUEST 1::", this.pop.onRequest);
            }
            else {
                this.pop.onRequest = value;
                console.log("POP REQUEST 2::", this.pop.onRequest);
            }
        });
        this.storage.get("cMap.hasShown").then((value) => {
            if (value == null) {
            }
            else {
                this.cMap.hasShown = value;
            }
        });
        this.storage.get("cMap.onLocationbarHide").then((value) => {
            if (value == null) {
            }
            else {
                this.cMap.onLocationbarHide = value;
            }
        });
        this.storage.get("pop.hasCleared").then((value) => {
            if (value == null) {
            }
            else {
                this.pop.hasCleared = value;
            }
        });
        this.storage.get("cMap.onbar2").then((value) => {
            if (value == null) {
            }
            else {
                this.cMap.onbar2 = value;
            }
        });
        this.storage.get("picture").then((value) => {
            if (value == null) {
            }
            else {
                this.picture = value;
            }
        });
    }
    // ionViewWillLeave() {
    //   this.storage.clear(); 
    // }
    removeStorage() {
        console.log("REMOVING ALL STORAGE:::");
        this.storage.clear();
        this.storage.remove("cMap.onbar3");
        this.storage.remove("defaultMap");
        this.storage.remove("priceAfterAddStop");
        this.storage.remove("driver_connected");
        this.storage.remove("hasPaused");
        this.storage.remove("destination_time");
        this.storage.remove("driver_arrived");
        this.storage.remove("arriving_time");
        this.storage.remove("carType");
        this.storage.remove("plate");
        this.storage.remove("name");
        this.storage.remove("cMap.lat");
        this.storage.remove("cMap.lng");
        this.storage.remove("detination_words");
        this.storage.remove("price");
        this.storage.remove("duration");
        this.storage.remove("cMap.selected_destination_bar");
        this.storage.remove("bull");
        this.storage.remove("uid");
        this.storage.remove("driver_number");
        this.storage.remove("rideSelected");
        this.storage.remove("returningUser");
        this.storage.remove("connect_change");
        this.storage.remove("StopLocUpdate");
        this.storage.remove("cMap.hasCompleted");
        this.storage.remove("cMap.isPickedUp");
        this.storage.remove("shortMap");
        this.storage.remove("demo");
        this.storage.remove("pickUpLocation");
        this.storage.remove("myGcode.locationName");
        this.storage.remove("cMap.canShowchoiceTab");
        this.storage.remove("pop.onRequest");
        this.storage.remove("cMap.hasShown");
        this.storage.remove("cMap.onLocationbarHide");
        this.storage.remove("pop.hasCleared");
        this.storage.remove("cMap.onbar2");
        this.storage.remove("picture");
    }
    subscribeToOnesignal() {
        if (!this.platform.is("cordova")) {
            this.notify_ID = "43cd6829-4651-4039-bbc3-aace7fbe7d72";
        }
        else if (this.platform.is("desktop")) {
            this.notify_ID = "43cd6829-4651-4039-bbc3-aace7fbe7d72";
        }
        else if (this.platform.is("mobileweb")) {
            this.notify_ID = "43cd6829-4651-4039-bbc3-aace7fbe7d72";
        }
        else {
            this.One.getIds().then((success) => {
                this.notify_ID = success.userId;
            });
        }
    }
    hidePanel() {
        this.hideDriverFound = this.hideDriverFound ? false : true;
    }
    toggle() {
        this.menu.enable(true);
        this.menu.open();
    }
    getIDD(id) {
    }
    dismiss_noride() {
        this.cMap.norideavailable = false;
        this.cMap.selected_destination_bar = false;
        this.cMap.hasShown = true;
        this.pop.onRequest = false;
        this.storage.set("pop.onRequest", this.pop.onRequest);
        this.storage.set("cMap.hasShown", this.cMap.hasShown);
        this.storage.set("cMap.selected_destination_bar", this.cMap.selected_destination_bar);
        this.cMap.canShowchoiceTab = false;
        this.storage.set("cMap.canShowchoiceTab", this.cMap.canShowchoiceTab);
        this.storage.set("cMap.norideavailable", this.cMap.norideavailable);
        if (this.cMap.watch_to_drop) {
            this.cMap.watch_to_drop.unsubscribe();
        }
        if (this.cMap.watch2) {
            this.cMap.watch2.unsubscribe();
        }
        if (this.cMap.watch_to_pick) {
            this.cMap.watch_to_pick.unsubscribe();
        }
        /// this.removeStorage()
    }
    dismiss_widget() {
        this.CloseOnly();
        this.Close();
        this.PerformActionOnCancel();
        if (this.cMap.watch_to_drop) {
            this.cMap.watch_to_drop.unsubscribe();
        }
        if (this.cMap.watch2) {
            this.cMap.watch2.unsubscribe();
        }
        if (this.cMap.watch_to_pick) {
            this.cMap.watch_to_pick.unsubscribe();
        }
        this.removeStorage();
    }
    radioGroupChange(event) {
        this.selectedRadioGroup = event.detail;
        // document.getElementById(id).style.backgroundColor = "black";
        // document.getElementById(id).style.color = "white";
    }
    checkVal() {
        this.showUser();
    }
    call_phone() {
        // let customerPhone = this.emergenc;
        // window.open("tel:" + customerPhone);
        this.call1.callNumber(`${this.emergenc}`, true)
            .then(() => console.log('Phone call initiated'))
            .catch(() => console.error('Error initiating phone call'));
    }
    call_phone_other() {
        // window.open("tel:+233276113371");
        this.call1.callNumber("+233276113371", true)
            .then(() => console.log('Phone call initiated'))
            .catch(() => console.error('Error initiating phone call'));
    }
    ngOnInit() {
        this.audioPlayer = new Audio('/assets/sounds/cancelalert.mp3');
        this.allStorage();
        this.subscribeToOnesignal();
        this.checkPromoExist();
        firebase_app__WEBPACK_IMPORTED_MODULE_5__["auth"]().onAuthStateChanged((user) => {
            if (user) {
                this.lat = 5.7186233;
                this.lng = -0.0240626;
                this.WaitForGeolocation();
                if (this.ph.card != null) {
                    var mainStr = this.ph.card || "2345678765445678", vis = mainStr.slice(-4), countNum = "";
                    for (var i = mainStr.length - 4; i > 0; i--) {
                        countNum += "*";
                    }
                    this.cardnumber = countNum + vis;
                }
            }
        });
        //GET USER PROFILE DETAILS
        this.ph.getUserProfile().on("value", (userProfileSnapshot) => {
            const phone = userProfileSnapshot.val().phonenumber;
            this.first_name = userProfileSnapshot.val().first_name;
            this.emergenc = userProfileSnapshot.val().emergencyNumber;
        });
    }
    remove() {
        this.authProvider.logoutUser().then(() => {
            this.navCtrl.navigateRoot("login-entrance");
        });
    }
    toggleMoreSection() {
        this.unturned = true;
        this.cMap.selected_destination_bar = true;
        this.canSwoop = false;
        this.storage.set("cMap.selected_destination_bar", this.cMap.selected_destination_bar);
        this.doNow();
    }
    doNow() {
        // this.pop.presentLoader("").then(() => {
        //   this.pop.hideLoader();
        // });
        Object(rxjs__WEBPACK_IMPORTED_MODULE_10__["timer"])(1000).subscribe(() => {
            // document.getElementById("destination").innerText =
            //   this.lp.translate()[0].dest;
            if (this.platform.is("cordova") && this.platform.is("mobileweb")) {
                this.cMap.map.setClickable(false);
            }
            this.cMap.onDestinatiobarHide = true;
            this.cMap.onLocationbarHide = true;
            this.cMap.showDone = false;
            this.hidelocator = true;
            this.storage.set("cMap.onLocationbarHide", this.cMap.onLocationbarHide);
        });
    }
    showLoadRefresh() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingCtrl.create({});
            yield loading.present().then(() => {
                let myTimeout = setTimeout(() => {
                    clearTimeout(myTimeout);
                    loading.dismiss();
                }, 400);
            });
        });
    }
    startCountdown() {
        this.manTime = 0;
        this.myVal = Object(rxjs__WEBPACK_IMPORTED_MODULE_10__["interval"])(1000).subscribe(() => {
            var demo = document.getElementById("demo");
            this.storage.set("demo", demo);
            this.trop = new Date().getHours() - new Date().getHours();
            if (demo) {
                if (this.seconds) {
                    this.seconds++;
                    if (this.seconds >= 60) {
                        this.minutes++;
                        this.seconds = 1;
                        console.log("Minutes:::", this.minutes);
                    }
                    if (this.minutes >= 5) {
                        this.myVal.unsubscribe();
                        console.log("Wait time Over:::", this.minutes);
                        this.waitTimeAlert();
                    }
                    demo.innerHTML = this.minutes + "min : " +
                        this.seconds + "secs ";
                }
            }
            this.cdr.detectChanges();
        });
    }
    ReturnHome() {
        this.removeStorage();
        this.cMap.selected_destination_bar = false;
        this.storage.set("cMap.selected_destination_bar", this.cMap.selected_destination_bar);
        if (this.platform.is("cordova") && this.platform.is("mobileweb")) {
            this.cMap.map.setClickable(true);
        }
        this.rideSelected = false;
        this.cMap.showDone = false;
        this.isRoute = true;
        this.cMap.StopLocUpdate = false;
        this.storage.set("StopLocUpdate", this.cMap.StopLocUpdate);
        this.storage.set("rideSelected", this.rideSelected);
        this.dProvider.isClose = true;
        this.allSurcharges = 0;
        this.toll_1_value = 0;
        this.surcharges = 0;
        this.myTolls = [];
        this.mySurch = [];
        this.realPrice = 0;
        this.driverGlobalPercentSurcharge = 0;
        this.riderGlobalSurcharge = 0;
        this.driverGlobalFlatSurcharge = 0;
        this.riderGlobalPercentSurcharge = 0;
        this.riderGlobalFlatSurcharge = 0;
        this.riderZipcodeFlatSurcharge = 0;
        this.riderZipcodePercentSurcharge = 0;
        this.driverZipcodeFlatSurcharge = 0;
        this.driverZipcodePercentSurcharge = 0;
        this.myg = [];
        this.actualPrice = 0;
        this.totalRiderSurcharge = 0;
        this.totalDriverSurcharge = 0;
        this.AllTOLLs = null;
        this.hidelocator = false;
        this.tollTems = [];
        this.canSwoop = true;
        this.cMap.shove = true;
        if (this.platform.is("cordova")) {
            this.cMap.map.clear().then(() => {
                this.cMap.yellow_markersArray = [];
                this.cMap.driver_markersArray = [];
                this.cMap.client_markersArray = [];
                this.cMap.flag_markersArray = [];
                this.cMap.car_markersArray = [];
                this.cMap.rider_markersArray = [];
                if (this.cMap.watch_to_drop) {
                    this.cMap.watch_to_drop.unsubscribe();
                }
                if (this.cMap.watch2) {
                    this.cMap.watch2.unsubscribe();
                }
                if (this.cMap.watch_to_pick) {
                    this.cMap.watch_to_pick.unsubscribe();
                }
                this.cMap.Restart();
            });
        }
        setTimeout(() => {
            this.StartTracker();
        }, 2000);
        this.dProvider.isDriver = true;
        this.cMap.hasRequested = false;
        this.pop.onRequest = false;
        this.hidelocator = false;
        this.cMap.canShowchoiceTab = false;
        this.cMap.norideavailable = false;
        this.cMap.isLocationChange = false;
        this.cMap.isDestinationChange = false;
        this.myGcode.locationChange = false;
        this.myGcode.destinationChange = false;
        this.eventProvider
            .getUserProfile()
            .child("userInfo")
            .child("ActualPrice")
            .remove();
        this.storage.set("pop.onRequest", this.pop.onRequest);
        this.storage.set("cMap.canShowchoiceTab", this.cMap.canShowchoiceTab);
        this.storage.set("cMap.norideavailable", this.cMap.norideavailable);
        this.cdr.detectChanges();
    }
    StartTracker() {
        this.watch = this.geo.watchPosition({ enableHighAccuracy: true, timeout: 10000 }).subscribe(position => {
            if (position.coords != undefined) {
                var geoposition = position;
                this.cMap.Geofiring;
                this.lat = geoposition.coords.latitude,
                    this.lng = geoposition.coords.longitude;
                if (!this.cMap.StopLocUpdate) {
                    this.cMap.lat = this.lat;
                    this.cMap.lng = this.lng;
                    this.cMap.gcode.Reverse_Geocode(this.lat, this.lng, false);
                }
            }
            else {
                var positionError = position;
            }
        });
    }
    WaitForGeolocation() {
        //A timer to detect if the location has been found.
        let location_tracker_loop = Object(rxjs__WEBPACK_IMPORTED_MODULE_10__["interval"])(3000).subscribe(() => {
            if (this.cMap.hasShown) {
                location_tracker_loop.unsubscribe();
                this.StartTracker();
                this.showGps = false;
                this.cMap.mapLoadComplete = true;
            }
        });
    }
    toggleMoreBtn() {
        //show or hide more button on connecting with driver.
        this.toggleMore = this.toggleMore ? false : true;
        if (this.toggleMore) {
            this.type = "arrow-down";
        }
        else {
            this.type = "arrow-up";
        }
    }
    GetRoute(location, destination) {
        if (!this.pickUpLocation) {
            this.navCtrl.navigateRoot([
                "route" +
                    {
                        price: this.actualPrice,
                        surcharge: this.allSurcharges,
                        location: this.myGcode.locationName,
                        destination: this.myGcode.destinationSetName,
                        toll: this.tollTems,
                        osc: this.outofstatecharge,
                        baseFare: this.baseFare,
                        time: this.time,
                        mileage: this.mileage,
                    },
            ]);
        }
        else {
            this.navCtrl.navigateRoot([
                "route" +
                    {
                        price: this.actualPrice,
                        surcharge: this.allSurcharges,
                        location: this.pickUpLocation,
                        destination: this.myGcode.destinationSetName,
                        toll: this.tollTems,
                        osc: this.outofstatecharge,
                        baseFare: this.baseFare,
                        time: this.time,
                        mileage: this.mileage,
                    },
            ]);
        }
    }
    showSearch() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.modal = yield this.modalCtrl.create({
                component: src_app_pages_autocomplete_autocomplete_page__WEBPACK_IMPORTED_MODULE_22__["AutocompletePage"],
                cssClass: "my-custom-class",
                showBackdrop: true,
            });
            yield this.modal.present();
            //   this.modalCtrl
            //     .create({
            //       component: AutocompletePage,
            //       cssClass: "my-custom-class",
            //       showBackdrop: true,
            //     })
            //     .then((element) => {
            //       element.present();
            //     });
        });
    }
    showAddressModal(selectedBar) {
        this.showSearch().then(() => {
            this.modal.onDidDismiss().then((data) => {
                //Open the address modal on location bar click to change location
                if (selectedBar == 1 && data.data != null) {
                    if (!this.startedNavigation) {
                        if (!this.pickUpLocation) {
                            document.getElementById("location").innerText = data.data;
                        }
                        this.pickUpLocation = data.data;
                        this.myGcode.locationName = this.pickUpLocation;
                        let loction_words = data.data;
                        this.storage.set("pickUpLocation", this.pickUpLocation);
                        this.storage.set("myGcode.locationName", this.myGcode.locationName);
                        this.pop.presentSimpleLoader("Searching....");
                        this.cMap.StopLocUpdate = true;
                        this.storage.set("StopLocUpdate", this.cMap.StopLocUpdate);
                        if (!this.pickUpLocation) {
                            this.myGcode.geocoder.geocode({ address: data.data }, (results, status) => {
                                if (status === "OK") {
                                    var position = results[0].geometry.location;
                                    this.cMap.lat = position.lat();
                                    this.cMap.lng = position.lng();
                                    this.myPos = new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_4__["LatLng"](position.lat(), position.lng());
                                }
                            });
                        }
                        else {
                            this.myGcode.geocoder.geocode({ address: this.pickUpLocation }, (results, status) => {
                                if (status === "OK") {
                                    var position = results[0].geometry.location;
                                    this.cMap.lat = position.lat();
                                    this.cMap.lng = position.lng();
                                    this.myPos = new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_4__["LatLng"](position.lat(), position.lng());
                                }
                            });
                        }
                    }
                }
                //Open the address modal on destination bar click to change destination
                if (selectedBar == 2 && data.data != null) {
                    if (!this.pickUpLocation) {
                        this.myPos = new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_4__["LatLng"](this.lat, this.lng);
                    }
                    else {
                    }
                    this.cMap.canShowchoiceTab = true;
                    this.storage.set("cMap.canShowchoiceTab", this.cMap.canShowchoiceTab);
                    this.cMap.selected_destination_bar = false;
                    this.storage.set("cMap.selected_destination_bar", this.cMap.selected_destination_bar);
                    document.getElementById("destination").innerText = data.data;
                    this.myGcode.destinationSetName = data.data;
                    this.detination_words = data.data;
                    this.storage.set("detination_words", this.detination_words);
                    // this.showUser();
                    this.pop.presentSimpleLoader("Searching....");
                    ///After data input, check to see if user selected to add a destination or to calculate distance.
                    this.myGcode.geocoder.geocode({ address: data.data }, (results, status) => {
                        if (status === "OK") {
                            var position = results[0].geometry.location;
                            this.calPos = new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_4__["LatLng"](position.lat(), position.lng());
                            this.destination_lat = position.lat();
                            this.destination_lng = position.lng();
                            //CALCULATE ROUTE
                            this.myGcode.destinationSetName = data.data;
                            this.dProvider.calcRoute(this.myPos, this.calPos, false, true, this.detination_words, true, this.percentage);
                            this.picked = true;
                            let h = Object(rxjs__WEBPACK_IMPORTED_MODULE_10__["interval"])(2000).subscribe(() => {
                                //If the estimated price has been calculated
                                let loc1 = [this.cMap.lat, this.cMap.lng];
                                let loc2 = [this.destination_lat, this.destination_lng];
                                this.loc1 = loc1;
                                this.loc2 = loc2;
                                if (this.dProvider.price != null) {
                                    this.pickLocation(loc1, loc2, data.data);
                                    h.unsubscribe();
                                }
                                else {
                                    this.cMap.norideavailable = true;
                                    h.unsubscribe();
                                    this.pop.onRequest = true;
                                    this.storage.set("pop.onRequest", this.pop.onRequest);
                                    this.storage.set("cMap.norideavailable", this.cMap.norideavailable);
                                }
                            });
                        }
                        else {
                            this.cMap.norideavailable = true;
                            this.storage.set("cMap.norideavailable", this.cMap.norideavailable);
                        }
                    }, (error) => {
                        this.cMap.norideavailable = true;
                        this.storage.set("cMap.norideavailable", this.cMap.norideavailable);
                    });
                }
                /// Endign Addding a stop
                if (selectedBar == 3 && data.data != null) {
                    this.myGcode.geocoder.geocode({ address: this.detination_words }, (results, status) => {
                        if (status === "OK") {
                            var position = results[0].geometry.location;
                            this.cMap.lat = position.lat();
                            this.cMap.lng = position.lng();
                            this.myPos = new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_4__["LatLng"](position.lat(), position.lng());
                        }
                    });
                    this.cMap.canShowchoiceTab = true;
                    this.cMap.selected_destination_bar = false;
                    this.newDestinationStop = data.data;
                    this.myGcode.destinationSetName = data.data;
                    this.storage.set("cMap.selected_destination_bar", this.cMap.selected_destination_bar);
                    this.storage.set("newDestinationStop", this.newDestinationStop);
                    this.storage.set("cMap.canShowchoiceTab", this.cMap.canShowchoiceTab);
                    ///After data input, check to see if user selected to add a destination or to calculate distance.
                    this.myGcode.geocoder.geocode({ address: data.data }, (results, status) => {
                        if (status === "OK") {
                            var position = results[0].geometry.location;
                            this.calPos = new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_4__["LatLng"](position.lat(), position.lng());
                            this.destination_lat = position.lat();
                            this.destination_lng = position.lng();
                            //CALCULATE ROUTE
                            this.myGcode.destinationSetName = data.data;
                            this.dProvider.calcRouteForAddstop(this.myPos, this.calPos, false, true, this.newDestinationStop, true, this.percentage);
                            this.picked = true;
                            let h = Object(rxjs__WEBPACK_IMPORTED_MODULE_10__["interval"])(2000).subscribe(() => {
                                //If the estimated price has been calculated
                                let loc1 = [this.cMap.lat, this.cMap.lng];
                                let loc2 = [this.destination_lat, this.destination_lng];
                                this.loc1 = loc1;
                                this.loc2 = loc2;
                                if (this.dProvider.new_price_for_add_stop != null) {
                                    this.pickLocation(loc1, loc2, data.data);
                                    h.unsubscribe();
                                    this.RequestForRideForAddStop();
                                }
                                else {
                                    this.cMap.norideavailable = true;
                                    h.unsubscribe();
                                    this.pop.onRequest = true;
                                    this.storage.set("pop.onRequest", this.pop.onRequest);
                                    this.storage.set("cMap.norideavailable", this.cMap.norideavailable);
                                }
                            });
                        }
                        else {
                            this.cMap.norideavailable = true;
                            this.storage.set("cMap.norideavailable", this.cMap.norideavailable);
                        }
                    }, (error) => {
                        this.cMap.norideavailable = true;
                        this.storage.set("cMap.norideavailable", this.cMap.norideavailable);
                    });
                }
                /// Endign Addding a stop
            });
        });
    }
    FaceShare() {
        let message = "Hi, i am going to " +
            this.loc2 +
            " from " + this.loc1;
        let subject = "My trip";
        let file = "https://www.google.com/maps/search/?api=1&query=" + this.loc2;
        let link = "https://www.google.com/maps/search/?api=1&query=" + this.loc2;
        this.share
            .share(message, subject, file, link)
            .then(() => { })
            .catch(() => { });
    }
    whatsappShare() {
        let message = "Hi, i am going to " +
            this.loc2 +
            "from " + this.loc1;
        let link = "https://www.google.com/maps/search/?api=1&query=" + this.loc2;
        this.share
            .shareViaWhatsApp(message, null, link)
            .then(() => {
        })
            .catch((err) => {
        });
    }
    showUser() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const actionSheet = yield this.actionSheetController.create({
                header: 'Who will take this ride?',
                // subHeader: 'If you request a ride for someone else, enter their phone number.',
                buttons: [{
                        text: 'Myself',
                        role: 'destructive',
                        icon: 'person',
                        handler: () => {
                            this.bookForSelf();
                        }
                    }, {
                        text: 'Someone Else',
                        icon: 'add',
                        handler: () => {
                            this.show_someone_form();
                        }
                    }]
            });
            yield actionSheet.present();
        });
    }
    presentActionSheet() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const actionSheet = yield this.actionSheetController.create({
                header: 'Share Trip',
                buttons: [{
                        text: 'Share to Whatsapp',
                        role: 'destructive',
                        icon: 'logo-whatsapp',
                        handler: () => {
                            this.whatsappShare();
                        }
                    }, {
                        text: 'Share to Others',
                        icon: 'share',
                        handler: () => {
                            this.FaceShare();
                        }
                    }]
            });
            yield actionSheet.present();
        });
    }
    Surcharge(value) {
        // this.realPrice = (this.exp);
        this.actualPrice = this.exp + parseFloat(value);
    }
    checkPrice(data, data1, data2, car) {
        let currentPrice = this.dProvider.price;
        let g = [];
        this.cMap.canShowchoiceTab = true;
        this.storage.set("cMap.canShowchoiceTab", this.cMap.canShowchoiceTab);
        this.storage.set("dProvider.price", this.dProvider.price);
        this.cMap.selected_destination_bar = false;
        this.storage.set("cMap.selected_destination_bar", this.cMap.selected_destination_bar);
        this.pop.removeLoader2();
        this.cMap.car_notificationIds.forEach((element) => {
            this.cartype = element[4];
            this.driverPhone = element[5];
        });
        if (this.cMap.car_notificationIds.length == 0) {
            this.cMap.isBooking = false;
            this.Close();
            this.pop.showPimp("No Cars In This Region");
        }
        else {
            if (!data) {
                this.baseFare = this.cMap.car_notificationIds[0][9];
                this.time = this.cMap.car_notificationIds[0][9];
                this.mileage = this.cMap.car_notificationIds[0][9];
                this.exp = parseFloat(this.baseFare +
                    this.dProvider.miles +
                    this.dProvider.duration);
                // this.realPrice = parseFloat(this.exp.toFixed(2));
                this.realPrice = this.dProvider.price;
                this.r = this.exp;
                this.g = this.exp;
                this.actualPrice =
                    this.cMap.car_notificationIds[0][9] +
                        this.dProvider.miles +
                        this.dProvider.duration;
            }
            else {
                this.actualPrice = 0;
                for (let index = 0; index < this.cMap.car_notificationIds.length; index++) {
                    const element = this.cMap.car_notificationIds[index];
                    if (element[2] == car[2]) {
                        this.refreshedTimes = index;
                    }
                }
            }
        }
    }
    get_state(address) {
        var parts = address.split(",");
        for (var x = parts.length - 1; x >= 0; x--) {
            var item = parts[x].trim();
            item = item.split(" ");
            if (item[0].length == 2) {
                return item[0].toLowerCase();
            }
        }
    }
    checkStateToStateSurch() {
        if (this.get_state(this.myGcode.locationName) !=
            this.get_state(this.myGcode.destinationSetName)) {
            this.outofstatecharge = 20;
        }
        else {
            this.outofstatecharge = 0;
        }
    }
    pickLocation(loc1, loc2, data) {
        let pints = [];
        let cints = [];
        let fints = [];
        let ponts = [];
        let conts = [];
        let fonts = [];
        let pxnts = [];
        let rust = [];
        this.driverGlobalPercentSurcharge = 0;
        this.riderGlobalSurcharge = 0;
        this.driverGlobalFlatSurcharge = 0;
        this.riderGlobalPercentSurcharge = 0;
        this.riderGlobalFlatSurcharge = 0;
        this.riderZipcodeFlatSurcharge = 0;
        this.riderZipcodePercentSurcharge = 0;
        this.driverZipcodeFlatSurcharge = 0;
        this.driverZipcodePercentSurcharge = 0;
        this.mySurch = [];
        if (this.myGcode.destinationSetName != null) {
            this.pop.onRequest = true;
            this.storage.set("pop.onRequest", this.pop.onRequest);
            if (this.platform.is("cordova") && this.platform.is("mobileweb")) {
                this.cMap.map.setClickable(true);
            }
            let apart = (google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(loc1[0], loc1[1]), new google.maps.LatLng(loc2[0], loc2[1])) / 1000).toFixed(2);
            if (parseFloat(apart) <= this.settings.distanceDriving) {
                if (this.cMap.car_notificationIds != undefined || this.cMap.car_notificationIds != null) {
                    if (this.cMap.car_notificationIds.length != 0) {
                        this.tollName = [];
                        this.toll_1 = [];
                        this.checkPrice(null, null, null, null);
                        this.cMap.choseCar = false;
                        this.cMap.showDone = false;
                        this.hidelocator = true;
                    }
                    else {
                        this.cMap.canShowchoiceTab = false;
                        this.cMap.norideavailable = true;
                        this.cMap.selected_destination_bar = false; //show menu bar
                        this.cMap.choseCar = false;
                        this.cMap.showDone = false;
                        this.hidelocator = true;
                        this.storage.set("cMap.selected_destination_bar", this.cMap.selected_destination_bar);
                        this.storage.set("cMap.canShowchoiceTab", this.cMap.canShowchoiceTab);
                        this.storage.set("cMap.norideavailable", this.cMap.norideavailable);
                    }
                }
                else {
                    this.cMap.canShowchoiceTab = false;
                    this.cMap.norideavailable = true;
                    this.cMap.selected_destination_bar = false; //show menu bar
                    this.cMap.choseCar = false;
                    this.cMap.showDone = false;
                    this.hidelocator = true;
                    this.storage.set("cMap.selected_destination_bar ", this.cMap.selected_destination_bar);
                    this.storage.set("cMap.canShowchoiceTab", this.cMap.canShowchoiceTab);
                    this.storage.set("cMap.norideavailable", this.cMap.norideavailable);
                }
            }
            else {
                this.pop.showPimp("Destination Too Far.");
                this.ReturnToSelect();
            }
        }
        else {
            //There is no destination. Tell user to add location
            this.cMap.onDestinatiobarHide = true;
            this.cMap.onLocationbarHide = true;
            this.cMap.showDone = false;
            this.storage.set("cMap.onLocationbarHide", this.cMap.onLocationbarHide);
        }
    }
    checkPayment() {
        if (this.ph.customerID) {
        }
        else {
            this.showPomp();
        }
    }
    showPomp() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alert.create({
                message: "Add Your Momo",
                subHeader: "For Quicker Checkout.",
                buttons: [
                    {
                        text: "Use Cash",
                        role: "cancel",
                        handler: () => { },
                    },
                    {
                        text: "Use Momo",
                        role: "cancel",
                        handler: () => {
                            this.navCtrl.navigateRoot("card");
                        },
                    },
                ],
                backdropDismiss: false,
            });
            yield alert.present();
        });
    }
    waitTimeAlert() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alert.create({
                message: "Driver is waiting....",
                subHeader: "Hello, Driver has been waiting for 5mins. Kindly call or meet driver",
                buttons: [
                    {
                        text: "Meet Driver",
                        role: "cancel",
                        handler: () => {
                        },
                    },
                    {
                        text: "Call Driver",
                        handler: () => {
                            this.callDriver();
                        },
                    },
                ],
                backdropDismiss: false,
            });
            yield alert.present();
        });
    }
    //This is the fuction for estimate btn.
    estimate() {
        this.navCtrl.navigateRoot([
            "estimate",
            {
                lat: this.cMap.lat,
                lng: this.cMap.lng,
            },
        ]);
        // this.platform.backButton.subscribe(() => this.navCtrl.pop());
    }
    popRoutes() {
        if (!this.pickUpLocation) {
            this.GetRoute(this.myGcode.locationName, this.myGcode.destinationSetName);
        }
        else {
            this.GetRoute(this.pickUpLocation, this.myGcode.destinationSetName);
        }
    }
    //goto payment page on cash or card click
    gotoPayment() {
        if (this.ph.card == null) {
            this.navCtrl.navigateRoot("payment");
            this.platform.backButton.subscribe(() => this.navCtrl.pop());
        }
    }
    ReturnToSelect() {
        this.cMap.selected_destination_bar = false;
        this.cMap.showDone = false;
        this.pop.onRequest = false;
        this.hidelocator = false;
        this.cMap.canShowchoiceTab = false;
        this.cMap.norideavailable = false;
        this.dProvider.price = null;
        this.cMap.hasRequested = false;
        this.cMap.hasShow = true;
        this.storage.set("pop.onRequest", this.pop.onRequest);
        this.storage.set("cMap.selected_destination_bar", this.cMap.selected_destination_bar);
        this.storage.set("cMap.canShowchoiceTab", this.cMap.canShowchoiceTab);
        this.storage.set("cMap.norideavailable", this.cMap.norideavailable);
        this.storage.set("dProvider.price", this.dProvider.price);
    }
    Start() {
        this.RequestForRide();
        this.rideSelected = false;
        this.cMap.canShowchoiceTab = false;
        this.cMap.norideavailable = false;
        this.cMap.hasRequested = true;
        // this.watchPositionSubscription.clearWatch(this.mapTracker);
        this.watch.unsubscribe();
        this.pop.driverEnded = true;
        this.cMap.isBooking = true;
        this.cLearMapActivity();
        this.storage.set("cMap.canShowchoiceTab", this.cMap.canShowchoiceTab);
        this.storage.set("rideSelected", this.rideSelected);
        this.storage.set("cMap.norideavailable", this.cMap.norideavailable);
    }
    cLearMapActivity() {
        if (this.cMap.keyEntered) {
            this.cMap.keyEntered.cancel();
        }
        if (this.cMap.exited) {
            this.cMap.exited.cancel();
        }
        if (this.cMap.moved) {
            this.cMap.moved.cancel();
        }
        this.cMap.map.clear().then(() => {
            this.cMap.yellow_markersArray = [];
            this.cMap.driver_markersArray = [];
            this.cMap.client_markersArray = [];
            this.cMap.flag_markersArray = [];
            this.cMap.car_markersArray = [];
            this.cMap.rider_markersArray = [];
            if (this.watch) {
                this.watch.unsubscribe();
            }
            if (this.cMap.watch_to_drop) {
                this.cMap.watch_to_drop.unsubscribe();
            }
            if (this.cMap.watch2) {
                this.cMap.watch2.unsubscribe();
            }
            if (this.cMap.watch_to_pick) {
                this.cMap.watch_to_pick.unsubscribe();
            }
            // if (this.cMap.watch_restart) {
            //     this.cMap.watch_restart.unsubscribe();
            //     console.log("watch_restart UNSUBSCRIBED")
            // }
        });
    }
    ///The Book Now button onclick=>Create and push the users information to the database.
    RequestForRide() {
        //document.getElementById("bar4").style.display = 'none'
        this.bookStarted = true;
        this.hideFunctions();
        this.returningUser = true;
        this.storage.set("returningUser", this.returningUser);
        this.cMap.hasCompleted = false;
        this.storage.set("cMap.hasCompleted", this.cMap.hasCompleted);
        var image = this.ph.id.photoURL;
        var name = this.first_name;
        var pay = this.ph.paymentType;
        this.pop.calculateBtn = false;
        clearInterval(this.newterval);
        if (image == null) {
            if (this.ph.pic == null) {
                image =
                    "https://cdn1.iconfinder.com/data/icons/instagram-ui-glyph/48/Sed-10-128.png";
            }
            else {
                image = this.ph.pic;
            }
        }
        if (name == null) {
            if (this.ph.name != null) {
                name = this.ph.name;
            }
            else {
                name = "User";
            }
        }
        if (pay == null) {
            pay = 1;
        }
        let ratingText = this.ph.ratingText;
        let ratingValue = this.ph.ratingValue;
        if (ratingText == null && ratingValue == null) {
            ratingText = this.lp.translate()[0].notrate;
            ratingValue = 0;
        }
        this.cMap.isBooking = true;
        if (this.cMap.car_notificationIds != null || this.cMap.car_notificationIds != undefined) {
        }
        else {
        }
        if (this.cMap.driverHere) {
            this.Close();
            this.cMap.isBooking = false;
            this.pop.showPimp("No drivers around.");
        }
        else {
            if (this.cMap.car_notificationIds.length <= this.refreshedTimes) {
                this.cMap.isBooking = false;
                this.Close();
            }
            else {
                if (!this.pickUpLocation) {
                    this.createUserInformation(image, this.myGcode.locationName, pay, ratingValue, ratingText);
                }
                else {
                    this.createUserInformation(image, this.pickUpLocation, pay, ratingValue, ratingText);
                }
            }
        }
        this.startTimer();
        this.startListeningForResponse();
    }
    RequestForRideForAddStop() {
        this.bookStarted = true;
        this.hideFunctions();
        this.returningUser = true;
        this.storage.set("returningUser", this.returningUser);
        this.cMap.hasCompleted = false;
        this.storage.set("cMap.hasCompleted", this.cMap.hasCompleted);
        var image = this.ph.id.photoURL;
        var name = this.first_name;
        var pay = this.ph.paymentType;
        this.pop.calculateBtn = false;
        clearInterval(this.newterval);
        if (image == null) {
            if (this.ph.pic == null) {
                image =
                    "https://cdn1.iconfinder.com/data/icons/instagram-ui-glyph/48/Sed-10-128.png";
            }
            else {
                image = this.ph.pic;
            }
        }
        if (pay == null) {
            pay = 1;
        }
        let ratingText = this.ph.ratingText;
        let ratingValue = this.ph.ratingValue;
        if (ratingText == null && ratingValue == null) {
            ratingText = this.lp.translate()[0].notrate;
            ratingValue = 0;
        }
        this.cMap.isBooking = true;
        this.createUserInformationForAddStop(this.detination_words);
    }
    OpenDriveInfo() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let obj = { info: this.bull };
            this.myAlert2 = yield this.modalCtrl.create({
                component: _driver_info_driver_info_page__WEBPACK_IMPORTED_MODULE_25__["DriverInfoPage"],
                componentProps: obj,
            });
            yield this.myAlert2.present();
            yield this.myAlert2.onDidDismiss().then((data) => {
                if (data.data == null || data.data == undefined) {
                }
                else {
                    this.onChange(data);
                }
            });
        });
    }
    OpenTripInfo() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let obj = {
                info: this.bull,
                accepted: this.date2,
                arrived: this.date,
                distance: this.dProvider.distance2,
                toll: this.myTolls,
            };
            this.myAlert3 = yield this.modalCtrl.create({
                component: _trip_info_trip_info_page__WEBPACK_IMPORTED_MODULE_28__["TripInfoPage"],
                componentProps: obj,
            });
            yield this.myAlert3.present();
            yield this.myAlert3.onDidDismiss((data) => {
                if (data.data == null) {
                }
                else {
                    this.onChange(data);
                }
            });
        });
    }
    toggleTurn() {
        this.unturned = this.unturned ? false : true;
        if (this.unturned) {
            this.type = "arrow-down";
        }
        else {
            this.type = "arrow-up";
        }
    }
    createUserInformation(picture, locationName, payWith, ratingValue, ratingText) {
        this.platform.backButton.subscribe(() => this.pop.presentToast(this.lp.translate()[0].cantexit));
        ////Here we check if there are cars available, if none we return
        if (this.cMap.car_notificationIds != null || this.cMap.car_notificationIds != undefined) {
            //This represents the drivers information for access to the driver node in the database
            let selected_driver = this.cMap.car_notificationIds[this.refreshedTimes][2];
            let selected_drivers_key = this.cMap.car_notificationIds[this.refreshedTimes][3];
            this.selected_driver_uid = selected_driver;
            this.pop.uid = selected_driver;
            this.dProvider.id = selected_driver;
            this.uid = selected_driver;
            this.storage.set("uid", this.uid);
            this.storage.set("dProvider.price", this.dProvider.price);
            let dest = this.myGcode.destinationSetName;
            ////console.log('Started the shit here riht now')
            if (!this.platform.is("cordova")) {
                this.lat = 5.4966964;
                this.lng = 7.5297323;
            }
            else {
                this.lat = this.cMap.lat;
                this.lng = this.cMap.lng;
            }
            //Get access to the drivers database node, remove the current driver from the node and push, to avoid any other request on the same driver
            this.act
                .getCurrentDriver(selected_drivers_key)
                .remove()
                .then(() => {
                // alert(selected_drivers_key)
                // console.log(name, locationName, lat, selected_driver, selected_drivers_key, 'hereerdvdhjhfssfsfds');
                this.eventProvider
                    .getUserProfile()
                    .on("value", (userProfileSnapshot) => {
                    this.cMap.routeNumber = userProfileSnapshot.val().routeNumber;
                });
                this.act
                    .getActivityProfile(selected_driver)
                    .set({
                    Client_username: this.first_name || "unknown",
                    Client_Deleted: false,
                    Client_location: [this.lat, this.lng],
                    Client_locationName: locationName || "Accra",
                    Client_paymentForm: payWith || 1,
                    Client_picture: picture || "no pic",
                    Client_phoneNumber: this.ph.phone || 43543533,
                    Client_ID: this.ph.id,
                    Client_destinationName: dest,
                    Client_price: this.dProvider.price,
                    Client_CanChargeCard: false,
                    Client_PickedUp: false,
                    Client_Dropped: false,
                    Client_HasRated: false,
                    Client_Rating: ratingValue,
                    Client_RatingText: ratingText,
                    Client_Positive_rating: this.ph.rating_positive || 0,
                    Client_Negative_rating: this.ph.rating_negative || 0,
                    Client_hasPaid: false,
                    Client_paidCash: false,
                    Client_returnState: false,
                    Client_ended: false,
                    Client_Notif: this.notify_ID,
                    Client_Arrived: false,
                    Client_pold: this.cMap.routeNumber || 0,
                    Client_status: this.ph.status || "Unverified",
                    Client_toll: this.myTolls || "null",
                    Client_token: this.ph.paymentToken || "0",
                    Client_customer_id: this.ph.customerID || "no ID",
                    Client_DriverSurharge: this.totalDriverSurcharge || 0,
                    Client_Surcharges: this.mySurch || [],
                    Client_realPrice: this.dProvider.price || 0,
                    Client_OutOfStateCharge: this.outofstatecharge || "null",
                })
                    .then(() => {
                    this.CreatePushNotification(selected_driver, "old_stop");
                });
            });
            // }
        }
    }
    createUserInformationForAddStop(locationName) {
        this.platform.backButton.subscribe(() => this.pop.presentToast(this.lp.translate()[0].cantexit));
        ////Here we check if there are cars available, if none we return
        if (this.cMap.car_notificationIds != null || this.cMap.car_notificationIds != undefined) {
            this.pop.uid = this.selected_driver_uid,
                this.dProvider.id = this.selected_driver_uid,
                this.uid = this.selected_driver_uid,
                this.storage.set("uid", this.uid);
            let new_dest = this.myGcode.destinationSetName;
            if (!this.platform.is("cordova")) {
                this.lat = 5.4966964;
                this.lng = 7.5297323;
            }
            else {
                this.lat = this.cMap.lat;
                this.lng = this.cMap.lng;
            }
            this.priceAfterAddStop = this.dProvider.price + this.dProvider.new_price_for_add_stop;
            this.storage.set("priceAfterAddStop", this.priceAfterAddStop);
            this.act
                .getActivityProfile(this.selected_driver_uid)
                .update({
                Client_location: [this.lat, this.lng],
                Client_locationName: locationName || "Accra",
                Client_new_destinationName: new_dest,
                Client_price: this.priceAfterAddStop,
                Client_realPrice: this.priceAfterAddStop || 0,
                Client_AddStopPrice: this.priceAfterAddStop || 0
            })
                .then(() => {
                this.CreatePushNotification(this.selected_driver_uid, "new_stop");
            });
        }
    }
    CloseLooper() {
        this.PerformActionOnCancel();
    }
    Close() {
        this.defaultMap = true; //FOr toggling
        this.shortMap = false; //FOr toggling
        this.rideSelected = false;
        this.currentCar = null;
        this.dProvider.destination_time = null;
        this.dProvider.price = null;
        this.pop.clearAll(this.uid, true);
        this.ReturnHome();
        this.cMap.hasCompleted = true;
        this.storage.set("cMap.hasCompleted", this.cMap.hasCompleted);
        this.storage.set("shortMap", this.shortMap);
        this.storage.set("defaultMap", this.defaultMap);
        this.storage.set("destination_time", this.dProvider.destination_time);
        this.storage.set("rideSelected", this.rideSelected);
        this.storage.set("dProvider.price", this.dProvider.price);
        clearInterval(this.interval);
        this.cMap.StopLocUpdate = false;
        this.storage.set("StopLocUpdate", this.cMap.StopLocUpdate);
        if (this.myTimer) {
            this.myTimer.unsubscribe();
        }
        // this.cMap.car_notificationIds = null;
        this.pop.hasCleared = true;
        this.cMap.onbar2 = false;
        this.refreshedTimes = 0;
        this.PerformActionOnCancel();
        this.storage.set("pop.hasCleared", this.pop.hasCleared);
        this.storage.set("cMap.onbar2", this.cMap.onbar2);
        this.eventProvider.getChatList(this.uid).off("child_added");
        this.cMap.map.clear().then(() => {
            this.cMap.yellow_markersArray = [];
            this.cMap.driver_markersArray = [];
            this.cMap.client_markersArray = [];
            this.cMap.flag_markersArray = [];
            this.cMap.car_markersArray = [];
            this.cMap.rider_markersArray = [];
            if (this.watch) {
                this.watch.unsubscribe();
            }
            if (this.cMap.watch_to_drop) {
                this.cMap.watch_to_drop.unsubscribe();
            }
            if (this.cMap.watch2) {
                this.cMap.watch2.unsubscribe();
            }
            if (this.cMap.watch_to_pick) {
                this.cMap.watch_to_pick.unsubscribe();
            }
            // if (this.cMap.watch_restart) {
            //     this.cMap.watch_restart.unsubscribe();
            //     console.log("watch_restart UNSUBSCRIBED")
            // }
        });
        this.act.getActiveProfile(this.uid).off("child_changed");
        this.act.getActiveProfile(this.uid).off("child_removed");
        this.removeStorage();
        // this.platform.backButton.subscribe(() => {
        //   navigator["app"].exitApp();
        // });
    }
    CreatePushNotification(id, stop) {
        let current_id = id[2];
        if (this.platform.is("cordova")) {
            if (stop == "old_stop") {
                this.OneSignal.sendPush(id);
            }
            else {
                this.OneSignal.sendPushForAddTrip(id);
            }
        }
        else if (this.platform.is("desktop")) {
            this.pop.presentToast("New Passenger To Pickup. You Have Less");
        }
        else if (this.platform.is("mobileweb")) {
            this.pop.presentToast("New Passenger To Pickup. You Have Less");
        }
        this.myTimer = Object(rxjs__WEBPACK_IMPORTED_MODULE_10__["interval"])(1000000).subscribe(() => {
            if (!this.driver_connected) {
                //alert("No Driver Found Yet");
                this.refreshedTimes++;
                this.eventProvider.getChatList(this.uid).off("child_added");
                this.myTimer.unsubscribe();
                this.act.getActiveProfile(this.uid).off("child_changed");
                this.act.getActiveProfile(this.uid).off("child_removed");
                this.act.getActiveProfile(this.uid).off("value");
                this.pop.driverEnded = false;
                this.act
                    .getActivityProfile(id)
                    .remove()
                    .then(() => {
                    this.RequestForRide();
                    // alert("Trying again, no driver found");
                });
            }
            else {
                this.cMap.marker = null;
                this.myTimer.unsubscribe();
                //  alert("Driver Found");
                this.refreshedTimes = -1;
                this.cMap.isBooking = true;
                this.pop.driverEnded = true;
                this.CloseLooper();
            }
        });
        this.cMap.ShowDestination(this.loc1, this.loc2);
        this.canCancel = true;
    }
    //call the driver now
    callDriver() {
        // window.open("tel:" +
        //   this.number);
        console.log('Phone c<><<><><> ISSS', this.number);
        this.call1.callNumber(`${this.number}`, true)
            .then(() => console.log('Phone call initiated'))
            .catch(() => console.error('Error initiating phone call'));
    }
    //Send a message to the driver
    Send() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const modal = yield this.modalCtrl.create({
                component: src_app_pages_chat_chat_page__WEBPACK_IMPORTED_MODULE_23__["ChatPage"],
                componentProps: { id: this.uid },
            });
            yield modal.present();
            modal.onDidDismiss().then(() => {
                this.notification = false;
            });
        });
    }
    pausingCountdown() {
        this.manTime = 0;
        this.myVal = Object(rxjs__WEBPACK_IMPORTED_MODULE_10__["interval"])(1000).subscribe(() => {
            var pause_time = document.getElementById("pause_time");
            if (pause_time) {
                if (this.seconds) {
                    this.seconds++;
                    if (this.seconds >= 60) {
                        this.minutes++;
                        this.clock_minutes++;
                        this.seconds = 1;
                        console.log("Minutes:::", this.minutes);
                    }
                    if (this.minutes >= 1) {
                        this.pauseCost = this.pauseCost + 0.20;
                        console.log("Pause Cost iss:::", this.pauseCost);
                        this.minutes = 0;
                    }
                    pause_time.innerHTML = this.clock_minutes + "min : " +
                        this.seconds + "secs ";
                }
            }
        });
    }
    startListeningForResponse() {
        this.platform.backButton.subscribe(() => this.pop.presentToast(this.lp.translate()[0].cantexit));
        if (this.returningUser) {
            this.act
                .getActiveProfile(this.uid)
                .on("child_added", (customerSnapshot) => {
                //  this.act.getActiveProfile(this.uid).once('value', customerSnapshot => {
                let activity = customerSnapshot.val();
                this.driver_ID = activity.Driver_ID;
                if (activity.Driver_number != null) {
                    this.cMap.D_lat = customerSnapshot.val().Driver_location[0];
                    this.cMap.D_lng = customerSnapshot.val().Driver_location[1];
                    this.allChat = customerSnapshot.val().Chat;
                    this.number = customerSnapshot.val().Driver_number;
                    this.storage.set("driver_number", this.number);
                    console.log('Phone c<><<><><> ISSS', this.number);
                    if (this.myTimer) {
                        this.myTimer.unsubscribe();
                    }
                    this.driver_connected = true;
                    this.storage.set("driver_connected", this.driver_connected);
                    this.PerformActionOnAdd(activity);
                    this.bull = activity;
                    this.storage.set("bull", this.bull);
                }
                this.cdr.detectChanges();
            });
        }
        else {
        }
        this.eventProvider.getChatList(this.uid).on("child_added", (snapshot) => {
            this.eventProvider.getChatList(this.uid).on("child_added", () => {
                if (snapshot.val().Driver_Message || snapshot.val().Driver_Audio) {
                    this.notification = true;
                    this.pop.presentToast("New Message From Driver");
                    this.vibration.vibrate(3000);
                    this.audioPlayer.play();
                    this.eventProvider.getChatList(this.uid).off("child_added");
                }
            });
        });
        if (this.cMap.isWalk)
            this.act.getActivityProfile(this.uid).update({
                Client_pold: true,
            });
        this.act
            .getActiveProfile(this.uid)
            .once("child_changed", (customerSnapshot) => {
            let activity = customerSnapshot.val();
            this.driver_arrived = activity.Driver_Arrived;
            this.storage.set("driver_arrived", this.driver_arrived);
            if (this.driver_arrived =  true && !this.dProvider.destination_time) {
                this.startCountdown();
            }
            this.act.getActiveProfile(this.uid).off("value");
            this.cdr.detectChanges();
        });
        this.act
            .getActiveProfile(this.uid)
            .on("child_changed", (customerSnapshot) => {
            let activity = customerSnapshot.val();
            this.driver_ID = activity.Driver_ID;
            this.driver_arrived = activity.Driver_Arrived;
            this.storage.set("driver_arrived", this.driver_arrived);
            if (activity.PauseTrip == true) {
                if (this.hasPaused == false) {
                    this.hasPaused = true;
                    this.pausingCountdown();
                    this.alerting("Trip Paused", "Driver has paused the ongoing trip\nPlease note there will be additional cost charged per minute whiles the trip is paused ", "warning");
                    this.storage.set("hasPaused", this.hasPaused);
                }
                else {
                }
            }
            else if (activity.PauseTrip == false) {
                this.hasPaused = false;
                this.storage.set("hasPaused", this.hasPaused);
                if (this.hasResumed == false) {
                    this.hasResumed = true;
                    this.alerting("Trip Resumed", "Driver has resumed the trip \nEnjoy the ride", "success");
                    this.myVal.unsubscribe();
                    this.storage.set("hasPaused", this.hasPaused);
                }
                else {
                }
            }
            if (activity.Driver_location != null) {
                this.cMap.D_lat = customerSnapshot.val().Driver_location[0];
                this.cMap.D_lng = customerSnapshot.val().Driver_location[1];
                this.allChat = customerSnapshot.val().Chat;
                this.number = customerSnapshot.val().Driver_number;
                this.storage.set("driver_number", this.number);
                if (this.myTimer)
                    this.myTimer.unsubscribe();
                this.driver_connected = true;
                this.storage.set("driver_connected", this.driver_connected);
                this.PerformActionOnAdd(activity);
                this.bull = activity;
            }
            this.act.getActiveProfile(this.uid).off("value");
            this.cdr.detectChanges();
        });
        this.act.getActiveProfile(this.uid).on("child_removed", () => {
            this.cMap.isPickedUp = false;
            this.cMap.hasCompleted = true;
            this.storage.set("cMap.isPickedUp", this.cMap.isPickedUp);
            this.storage.set("cMap.hasCompleted", this.cMap.hasCompleted);
            if (this.pop.driverEnded) {
                this.pop.presentToast("Driver has ended the trip.Search for another driver...");
                this.pop.hasCleared = true;
                this.storage.set("pop.hasCleared", this.pop.hasCleared);
                this.CloseOnly();
                this.PerformActionOnCancel();
                // this.RequestForRide();
                // this.Start()
                this.canEnd = false;
                this.cdr.detectChanges();
            }
            else {
                // this.pop.showPimp("Ride Cancelled...");
                this.onChange("closed");
            }
            this.act.getActiveProfile(this.uid).off("value");
            this.cdr.detectChanges();
        });
    }
    alerting(title, msg, icon) {
        sweetalert2__WEBPACK_IMPORTED_MODULE_32___default.a.fire({
            title: title,
            text: msg,
            icon: icon,
            heightAuto: false
        });
    }
    PerformActionOnCancel() {
        this.removeStorage();
        this.dProvider.destination_time = null;
        this.dProvider.new_price_for_add_stop = null;
        this.newDestinationStop = null;
        this.storage.set("newDestinationStop", this.newDestinationStop);
        this.hasPaused = false;
        this.rideSelected = false;
        this.startedNavigation = false;
        this.driver_connected = false;
        this.driver_arrived = false;
        this.storage.set("driver_connected", this.driver_connected);
        this.storage.set("driver_arrived", this.driver_arrived);
        this.storage.set("rideSelected", this.rideSelected);
        this.surcharges = 0;
        this.canCancel = false;
        this.canIncur = false;
        this.isDone = true;
        clearInterval(this.interval);
        this.toll_1 = [];
        this.myTolls = [];
        this.toller = 0;
        this.eventProvider
            .getUserProfile()
            .child("userInfo")
            .child("ActualPrice")
            .remove();
        this.cMap.routeNumber = 0;
        //why clear drivers? Commenting it out for now to test
        //this.cMap.car_notificationIds.length = 0;
        this.cMap.norideavailable = false;
        this.storage.set("cMap.norideavailable", this.cMap.norideavailable);
        this.eventProvider.getUserProfile().update({
            routeNumber: 0,
        });
        this.cMap.isWalk = false;
        //  this.ph.getUserAsClientInfo().child(this.uid).child('client').off('child_changed')
        this.dProvider.isDriver = false;
        this.isChecking = false;
        this.cMap.map.clear().then(() => {
            this.cMap.yellow_markersArray = [];
            this.cMap.driver_markersArray = [];
            this.cMap.client_markersArray = [];
            this.cMap.flag_markersArray = [];
            this.cMap.car_markersArray = [];
            this.cMap.rider_markersArray = [];
            if (this.watch) {
                this.watch.unsubscribe();
            }
            if (this.cMap.watch_to_drop) {
                this.cMap.watch_to_drop.unsubscribe();
            }
            if (this.cMap.watch2) {
                this.cMap.watch2.unsubscribe();
            }
            if (this.cMap.watch_to_pick) {
                this.cMap.watch_to_pick.unsubscribe();
            }
            // if (this.cMap.watch_restart) {
            //     this.cMap.watch_restart.unsubscribe();
            //     console.log("watch_restart UNSUBSCRIBED")
            // }
        });
        if (this.platform.is("cordova")) {
            this.BeginTracker();
        }
        else {
            this.cMap.Restart();
            if (this.platform.is("cordova") && this.platform.is("mobileweb")) {
                this.cMap.map.setClickable(true);
            }
        }
        //this.cMap.element = this.mapComponent
        this.cMap.hasRequested = false;
        this.cMap.onbar3 = false;
        this.storage.set("cMap.onbar3", this.cMap.onbar3);
        this.cMap.toggleBtn = false;
        this.cMap.onPointerHide = false;
        this.cMap.isBooking = false;
        // this.cMap.onDestinatiobarHide = false;
        this.pop.allowed = true;
        //this.cMap.canMess = false;
        this.pop.canDismiss = true;
        this.hasTimed = true;
        this.cMap.isDestination = false;
        this.cMap.selected_destination_bar = false;
        this.cMap.isLocationChange = false;
        this.cMap.isDestinationChange = false;
        this.dProvider.price = null;
        this.droppedoff = true;
        this.pickedup = true;
        this.hasPaid = true;
        this.driverFound = true;
        this.cMap.showDone = false;
        this.pop.onRequest = false;
        this.canShowBars = true;
        this.toggleMore = true;
        this.cMap.isNavigate = false;
        this.connect_change = true;
        this.storage.set("connect_change", this.connect_change);
        this.storage.set("cMap.selected_destination_bar", this.cMap.selected_destination_bar);
        this.storage.set("dProvider.price", this.dProvider.price);
        this.hidelocator = false;
        this.bookStarted = false;
        this.storage.set("pop.onRequest", this.pop.onRequest);
        this.cdr.detectChanges();
    }
    BeginTracker() {
        this.cMap.Restart();
        if (this.platform.is("cordova") && this.platform.is("mobileweb")) {
            this.cMap.map.setClickable(true);
        }
    }
    PerformActionOnAdd(activity) {
        if (this.connect_change) {
            this.Onconnect(activity);
            this.connect_change = false;
            this.storage.set("connect_change", this.connect_change);
        }
        this.cMap.driver_lat = activity.Driver_location[0];
        this.cMap.driver_lng = activity.Driver_location[1];
        this.driver_pos = new google.maps.LatLng(this.cMap.driver_lat, this.cMap.driver_lng);
        this.destiny = activity.Client_destinationName;
        if (activity.Driver_location[0] != null && !activity.Client_PickedUp) {
            let latlng = {
                lat: parseFloat(activity.Driver_location[0]),
                lng: parseFloat(activity.Driver_location[1]),
            };
            this.geocoder.geocode({ location: latlng }, (results, status) => {
                if (status === "OK") {
                    if (results[0]) {
                        let driverPos = new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_4__["LatLng"](activity.Driver_location[0], activity.Driver_location[1]);
                        let userPos = new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_4__["LatLng"](activity.Client_location[0], activity.Client_location[1]);
                        this.dProvider.calcRouteAriving(userPos, driverPos, true, false, results[0].formatted_address, false, this.percentage);
                        this.hidelocator = true;
                        this.driverFound = false;
                    }
                }
            });
        }
        if (activity.Driver_location[0] != null && activity.Client_PickedUp) {
            let latlng = {
                lat: parseFloat(this.destination_lat),
                lng: parseFloat(this.destination_lng),
            };
            this.geocoder.geocode({ location: latlng }, (results, status) => {
                if (status === "OK") {
                    if (results[0]) {
                        let flagPos = new google.maps.LatLng(this.destination_lat, this.destination_lng);
                        let userPos = new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_4__["LatLng"](activity.Client_location[0], activity.Client_location[1]);
                        this.dProvider.calcDestRoute(true, userPos, flagPos, results[0].formatted_address, false, false, this.percentage);
                    }
                }
            });
        }
        if (activity.Client_PickedUp && this.pickedup) {
            this.cMap.isDestination = true;
            // this.cancelTimer.unsubscribe();
            this.cMap.ClearDetection = true;
            this.canShowBars = false;
            this.toggleMore = false;
            let mydate = new Date();
            this.pickedup = false;
            var currentdate = new Date();
            var datetime = this.paddNumber(currentdate.getMonth() + 1, "00") +
                "-" +
                this.paddNumber(currentdate.getDate(), "00") +
                "-" +
                currentdate.getFullYear() +
                " @ " +
                this.paddNumber((currentdate.getHours() + 24) % 12 || 12, "00") +
                ":" +
                ("0" + currentdate.getMinutes()).slice(-2) +
                " " +
                (currentdate.getHours() > 11 ? "PM" : "AM");
            this.date2 = datetime;
            this.cMap.map.getMyLocation().then((location) => {
                this.myGcode.geocoder.geocode({ location: location.latLng }, (results, status) => {
                    if (status === "OK") {
                        if (results[0]) {
                            let dfo = Object(rxjs__WEBPACK_IMPORTED_MODULE_10__["interval"])(500).subscribe(() => {
                                if (this.dProvider.duration > 0) {
                                    let r = new Date(mydate.getMinutes() - this.dProvider.duration);
                                    var now = new Date();
                                    now.setMinutes(now.getMinutes() - this.dProvider.duration); // timestamp
                                    now = new Date(now); // Date object
                                    var gh = this.paddNumber(now.getMonth() + 1, "00") +
                                        "-" +
                                        this.paddNumber(now.getDate(), "00") +
                                        "-" +
                                        now.getFullYear() +
                                        " @ " +
                                        this.paddNumber((now.getHours() + 24) % 12 || 12, "00") +
                                        ":" +
                                        ("0" + now.getMinutes()).slice(-2) +
                                        " " +
                                        (now.getHours() > 11 ? "PM" : "AM");
                                    this.date1 = gh;
                                    dfo.unsubscribe();
                                }
                                else {
                                    let now = new Date(); // Date object
                                    var gh = this.paddNumber(now.getMonth(), "00") +
                                        "-" +
                                        this.paddNumber(now.getDate(), "00") +
                                        "-" +
                                        now.getFullYear() +
                                        " @ " +
                                        this.paddNumber((now.getHours() + 24) % 12 || 12, "00") +
                                        ":" +
                                        ("0" + now.getMinutes()).slice(-2) +
                                        " " +
                                        (now.getHours() > 11 ? "PM" : "AM");
                                    this.date1 = gh;
                                }
                            });
                            this.location2 = results[0].formatted_address;
                        }
                    }
                });
            });
            this.cMap.toggleBtn = false;
            let lat;
            let lng;
            this.type = "arrow-up";
            this.myGcode.Reverse_Geocode(this.cMap.lat, this.cMap.lng, false);
            this.pop.presentSimpleLoader("Trip Started....");
            if (this.platform.is("cordova")) {
                this.OneSignal.sendArrived(this.notify_ID);
            }
            else if (this.platform.is("desktop")) {
                this.pop.presentToast("Driver Has Started Trip");
            }
            else if (this.platform.is("mobileweb")) {
                this.pop.presentToast("Driver Has Started Trip");
            }
            this.cMap.isDropped = true;
            this.cMap.isPickedUp = false;
            this.storage.set("cMap.isPickedUp", this.cMap.isPickedUp);
            let setDetin = Object(rxjs__WEBPACK_IMPORTED_MODULE_10__["interval"])(500).subscribe(() => {
                this.geocoder.geocode({ address: activity.Client_destinationName }, (results, status) => {
                    if (status == "OK") {
                        setDetin.unsubscribe();
                        var position = results[0].geometry.location;
                        lat = position.lat();
                        lng = position.lng();
                        if (this.platform.is("cordova")) {
                            //console.log('destination');
                            //document.getElementById("header").innerHTML = 'Estimated Arrival ' + this.dProvider.time2;
                            let urPos = new google.maps.LatLng(lat, lng);
                            //this.pop.presentLoader('')
                            let uLOC = new google.maps.LatLng(this.cMap.lat, this.cMap.lng);
                            this.driverFound = false;
                            this.pop.SmartLoader("");
                            this.cMap.setMarkersDestination(lat, lng, this.uid);
                            this.pop.presentSimpleLoader("Ride Started....");
                            if (this.watch) {
                                this.watch.unsubscribe();
                            }
                            if (this.cMap.watch2) {
                                this.cMap.watch2.unsubscribe();
                            }
                            if (this.cMap.watch_to_pick) {
                                this.cMap.watch_to_pick.unsubscribe();
                            }
                            this.dProvider.calcDestRoute(this.name, uLOC, urPos, activity.Client_destinationName, this.uid, true, this.percentage);
                        }
                        else {
                            if (this.watch) {
                                this.watch.unsubscribe();
                            }
                            if (this.cMap.watch2) {
                                this.cMap.watch2.unsubscribe();
                            }
                            if (this.cMap.watch_to_pick) {
                                this.cMap.watch_to_pick.unsubscribe();
                            }
                            this.cMap.setMarkersDestination(lat, lng, this.uid);
                            this.pop.presentSimpleLoader("Ride Ongoing....");
                            let urPos = new google.maps.LatLng(lat, lng);
                            let uLOC = new google.maps.LatLng(this.cMap.lat, this.cMap.lng);
                            this.dProvider.calcDestRoute(this.name, uLOC, urPos, activity.Client_destinationName, this.uid, true, this.percentage);
                        }
                    }
                    else {
                        this.pop.presentSimpleLoader("eror getting location....");
                    }
                });
            });
        }
        if (activity.Client_Dropped && this.droppedoff) {
            this.cMap.hasCompleted = true;
            this.storage.set("cMap.hasCompleted", this.cMap.hasCompleted);
            this.pop.presentSimpleLoader(this.lp.translate()[0].waiting);
            this.ph.uid = this.uid;
            if (this.platform.is("cordova")) {
                this.OneSignal.sendEnd(this.notify_ID);
            }
            else if (this.platform.is("desktop")) {
                this.pop.presentToast("Your Trip Has Ended.");
            }
            else if (this.platform.is("mobileweb")) {
                this.pop.presentToast("Your Trip Has Ended.");
            }
            this.Charged(activity.Client_realPrice);
            this.droppedoff = false;
            let dfo = Object(rxjs__WEBPACK_IMPORTED_MODULE_10__["interval"])(500).subscribe(() => {
                if (this.dProvider.duration2) {
                    var now = new Date();
                    now.setMinutes(now.getMinutes() + this.dProvider.duration2); // timestamp
                    now = new Date(now); // Date object
                    var gh = this.paddNumber(currentdate.getMonth(), "00") +
                        "-" +
                        this.paddNumber(currentdate.getDate(), "00") +
                        "-" +
                        currentdate.getFullYear() +
                        " @ " +
                        this.paddNumber((currentdate.getHours() + 24) % 12 || 12, "00") +
                        ":" +
                        ("0" + currentdate.getMinutes()).slice(-2) +
                        " " +
                        (currentdate.getHours() > 11 ? "PM" : "AM");
                    this.date = gh;
                    dfo.unsubscribe();
                }
            });
            this.cMap.map.getMyLocation().then((location) => {
                this.geocoder.geocode({ location: location.latLng }, (results, status) => {
                    if (status === "OK") {
                        if (results[0]) {
                            this.destination2 = results[0].formatted_address;
                            if (!this.pickUpLocation) {
                                this.ph.Completed(activity.Chat || "", this.myID, this.name, this.date, this.myGcode.locationName, destiny, this.price, this.totTOLLS || 0, this.date1 || "", this.date2 || "", this.date3 || "", this.location2 || "", this.destination2 || "", this.mySurch, this.dProvider.price, this.outofstatecharge || "", this.pauseCost || 0);
                            }
                            else {
                                this.ph.Completed(activity.Chat || "", this.myID, this.name, this.date, this.pickUpLocation, destiny, this.price, this.totTOLLS || 0, this.date1 || "", this.date2 || "", this.date3 || "", this.location2 || "", this.destination2 || "", this.mySurch, this.dProvider.price, this.outofstatecharge || "", this.pauseCost || 0);
                            }
                        }
                    }
                });
            });
            let destiny;
            destiny = activity.Client_destinationName;
            var currentdate = new Date();
            var datetime = this.paddNumber(currentdate.getMonth() + 1, "00") +
                "-" +
                this.paddNumber(currentdate.getDate(), "00") +
                "-" +
                currentdate.getFullYear() +
                " @ " +
                this.paddNumber((currentdate.getHours() + 24) % 12 || 12, "00") +
                ":" +
                ("0" + currentdate.getMinutes()).slice(-2) +
                " " +
                (currentdate.getHours() > 11 ? "PM" : "AM");
            this.date3 = datetime;
        }
        if ((activity.Client_hasPaid && this.hasPaid) ||
            (activity.Client_paidCash && this.hasPaid)) {
            if (this.myTimer)
                this.myTimer.unsubscribe();
            this.act.getActiveProfile(this.uid).off("child_changed");
            this.act.getActiveProfile(this.uid).off("child_added");
            this.act.getActiveProfile(this.uid).off("child_removed");
            this.act.getActiveProfile(this.uid).off("value");
            this.ph.driverID = activity.Driver_ID;
            var datetime = this.paddNumber(currentdate.getMonth() + 1, "00") +
                "-" +
                this.paddNumber(currentdate.getDate(), "00") +
                "-" +
                currentdate.getFullYear() +
                " @ " +
                this.paddNumber((currentdate.getHours() + 24) % 12 || 12, "00") +
                ":" +
                ("0" + currentdate.getMinutes()).slice(-2) +
                " " +
                (currentdate.getHours() > 11 ? "PM" : "AM");
            let destiny;
            destiny = activity.Client_destinationName;
            this.hasPaid = false;
            if (this.isDone)
                if (!this.pickUpLocation) {
                    this.ph
                        .createHistory(this.name, this.price, datetime, this.myGcode.locationName, destiny, this.price, this.pauseCost || 0)
                        .then((id) => {
                        this.isDone = false;
                        this.show_ratings(activity);
                        if (this.canEnd) {
                            this.CloseOnly();
                            this.Close();
                            this.PerformActionOnCancel();
                            this.cMap.onbar3 = false;
                            this.storage.set("cMap.onbar3", this.cMap.onbar3);
                            this.canEnd = false;
                        }
                    });
                }
                else {
                    this.ph
                        .createHistory(this.name, this.price, datetime, this.pickUpLocation, destiny, this.price, this.pauseCost || 0)
                        .then((id) => {
                        this.isDone = false;
                        this.show_ratings(activity);
                        if (this.canEnd) {
                            this.CloseOnly();
                            this.Close();
                            this.PerformActionOnCancel();
                            this.cMap.onbar3 = false;
                            this.storage.set("cMap.onbar3", this.cMap.onbar3);
                            this.canEnd = false;
                        }
                    });
                }
        }
        this.cdr.detectChanges();
    }
    CloseOnly() {
        this.defaultMap = true; //FOr toggling
        this.shortMap = false; //FOr toggling
        this.rideSelected = false;
        this.pop.hasCleared = true;
        this.cMap.onbar2 = false;
        this.driver_arrived = false;
        this.refreshedTimes = 0;
        this.storage.set("shortMap", this.shortMap);
        this.storage.set("defaultMap", this.defaultMap);
        this.storage.set("driver_arrived", this.driver_arrived);
        this.storage.set("rideSelected", this.rideSelected);
        this.storage.set("pop.hasCleared", this.pop.hasCleared);
        this.storage.set("cMap.onbar2", this.cMap.onbar2);
        // this.platform.backButton.subscribe(() => {
        //   navigator["app"].exitApp();
        // });
        this.removeStorage();
    }
    bookForSelf() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            if (this.cMap.norideavailable == true) {
                this.pop.showPimp("Sorry, No driver available in your location. Please try again");
            }
            else {
                this.rideSelected = true;
                this.pop.hasCleared = false;
                this.cMap.onbar2 = false;
                this.storage.set("rideSelected", this.rideSelected);
                this.storage.set("pop.hasCleared", this.pop.hasCleared);
                this.storage.set("cMap.onbar2", this.cMap.onbar2);
            }
        });
    }
    show_someone_form() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.myAlert = yield this.modalCtrl.create({
                component: _someone_someone_page__WEBPACK_IMPORTED_MODULE_27__["SomeonePage"]
            });
            yield this.myAlert.present();
            yield this.myAlert.onDidDismiss().then((data) => {
                if (data.data == 1) {
                }
                else {
                    if (this.cMap.norideavailable == true) {
                        this.pop.showPimp("Sorry, No driver available in your location. Please try again");
                    }
                    else {
                        this.first_name = data.data.first_name;
                        this.ph.phone = data.data.number;
                        this.booked_for_someone = true;
                        this.rideSelected = true;
                        this.pop.hasCleared = false;
                        this.cMap.onbar2 = false;
                        this.storage.set("rideSelected", this.rideSelected);
                        this.storage.set("pop.hasCleared", this.pop.hasCleared);
                        this.storage.set("cMap.onbar2", this.cMap.onbar2);
                    }
                }
            });
            this.myAlert.present();
        });
    }
    show_ratings(activity) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.removeStorage();
            let obj = {
                eventId: activity.Driver_ID,
                positive_Rating: activity.Driver_Positive_rating,
                negative_Rating: activity.Driver_Negative_rating,
                name: activity.Driver_name,
                time: this.dProvider.time,
                distance: this.dProvider.distance2,
                price: activity.Client_realPrice,
                m_ID: this.ph.historyID.key || "noID",
                custom_ID: this.ph.customerID || "noIDPresent",
                waitTimeCost: this.pauseCost || 0
            };
            this.myAlert = yield this.modalCtrl.create({
                component: src_app_pages_rate_rate_page__WEBPACK_IMPORTED_MODULE_26__["RatePage"],
                componentProps: obj,
            });
            yield this.myAlert.present();
            if (this.platform.is("cordova")) {
                this.vibration.vibrate(3000);
                this.audioPlayer.play();
            }
            yield this.myAlert.onDidDismiss().then((data) => {
                this.dProvider.new_price_for_add_stop = "";
                this.newDestinationStop = "";
                this.storage.set("newDestinationStop", this.newDestinationStop);
                if (data.data == 1) {
                    this.CloseOnly();
                    this.Close();
                    this.PerformActionOnCancel();
                }
                else {
                    this.CloseOnly();
                    this.Close();
                    this.PerformActionOnCancel();
                }
            });
            this.myAlert.present();
        });
    }
    ToggleChange_1() {
        if (!this.pickUpLocation) {
            document.getElementById("location").innerText = this.myGcode.locationName;
        }
        else {
            this.pickUpLocation = '';
            this.storage.set("pickUpLocation", this.pickUpLocation);
            document.getElementById("location").innerText = this.myGcode.locationName;
        }
    }
    paddNumber(number, paddingValue) {
        return String(paddingValue + number).slice(-paddingValue.length);
    }
    ToggleChange_2() {
    }
    DriverFound(location, plate, carType, name, seat, rating, picture, client_lat, client_lng, number) {
        this.location = location;
        this.plate = plate;
        this.carType = carType;
        this.seat = seat;
        this.up = rating;
        this.number = number;
        this.picture = picture;
        this.name = name;
        this.cMap.lat = client_lat;
        this.cMap.lng = client_lng;
        this.storage.set("carType", this.carType);
        this.storage.set("plate", this.plate);
        this.storage.set("name", this.name);
        this.storage.set("driver_number", this.number);
        this.storage.set("picture", this.picture);
        console.log('Phone c<><<><><> ISSS', this.number);
        if (location) {
            this.cMap.D_lat = location[0];
            this.cMap.D_lng = location[1];
        }
        else {
            if (this.bookStarted)
                this.bookStarted = false;
            this.pop.presentToast("Couldnt find a driver at this time.");
        }
        //alert("Driver has been Found");
        if (this.platform.is("cordova")) {
            this.cMap.map.clear().then(() => {
                this.cMap.yellow_markersArray = [];
                this.cMap.driver_markersArray = [];
                this.cMap.client_markersArray = [];
                this.cMap.flag_markersArray = [];
                this.cMap.car_markersArray = [];
                this.cMap.rider_markersArray = [];
                if (this.watch) {
                    this.watch.unsubscribe();
                }
                if (this.cMap.watch2) {
                    this.cMap.watch2.unsubscribe();
                }
                if (this.cMap.watch_to_pick) {
                    this.cMap.watch_to_pick.unsubscribe();
                }
                // if (this.cMap.watch_restart) {
                //     this.cMap.watch_restart.unsubscribe();
                //     console.log("watch_restart UNSUBSCRIBED")
                // }
                this.pop.SmartLoader("");
                this.cMap.setMarkers(location[0], location[1], this.uid);
                this.cMap.isPickedUp = true;
                this.storage.set("cMap.isPickedUp", this.cMap.isPickedUp);
                this.cdr.detectChanges();
            });
        }
        if (this.booked_for_someone == true) {
            this.booked_for_someone = false;
            this.ph.send_sms(this.ph.phone, `A ride has been booked for you. Find Drivers Details:\nName: ${this.name}\nPhone: ${this.number}\nCar Plate: ${this.plate}`);
        }
    }
    startTimer() {
        this.interval = setInterval(() => {
            if (this.timeLeft > 0) {
                this.timeLeft--;
                console.log("TIMELEFT--.>>>" + this.timeLeft);
            }
            else {
                clearInterval(this.interval);
                if (this.cMap.onbar3) {
                }
                else {
                    this.Close();
                }
                this.timeLeft = 60;
                console.log("TIMELEFT--.>>>" + this.timeLeft);
            }
        }, 1000);
    }
    onChange(e) {
        this.rideSelected = false;
        this.storage.set("rideSelected", this.rideSelected);
        this.Close();
        let yu = e;
        let charge = 0;
        this.eventProvider.getChatList(this.uid).off("child_added");
        this.act.getActiveProfile(this.uid).off("child_changed");
        this.act.getActiveProfile(this.uid).off("child_removed");
        var currentdate = new Date();
        var datetime = this.paddNumber(currentdate.getMonth() + 1, "00") +
            "-" +
            this.paddNumber(currentdate.getDate(), "00") +
            "-" +
            currentdate.getFullYear() +
            " @ " +
            this.paddNumber((currentdate.getHours() + 24) % 12 || 12, "00") +
            ":" +
            ("0" + currentdate.getMinutes()).slice(-2) +
            " " +
            (currentdate.getHours() > 11 ? "PM" : "AM");
        if (!this.pickUpLocation) {
            if (this.ph.name &&
                datetime &&
                this.myGcode.locationName &&
                this.myGcode.destinationSetName &&
                this.actualPrice &&
                yu)
                this.ph
                    .Cancelled(this.allChat, this.ph.name, datetime, this.myGcode.locationName, this.myGcode.destinationSetName, this.actualPrice, yu, charge, this.mySurch, this.dProvider.price, this.outofstatecharge)
                    .then(() => {
                    this.ph
                        .CancelledMe2(this.driver_ID, this.ph.name, datetime, this.myGcode.locationName, this.myGcode.destinationSetName, this.actualPrice, yu, charge, this.mySurch, this.dProvider.price, this.outofstatecharge)
                        .then(() => { });
                    this.ph
                        .CancelledMe(this.ph.name, datetime, this.myGcode.locationName, this.myGcode.destinationSetName, this.actualPrice, yu, charge, this.mySurch, this.dProvider.price, this.outofstatecharge)
                        .then(() => { });
                    this.ph
                        .getCancelledProfile()
                        .child("Cancelled/documents")
                        .on("value", (snapshot) => {
                        this.items = [];
                        snapshot.forEach((snap) => {
                            if (snap.val().type == "Rider")
                                this.items.push({
                                    key: snap.key,
                                    text: snap.val().title,
                                    type: snap.val().type,
                                    status: snap.val().status,
                                });
                            return false;
                        });
                    });
                    this.currentCar = null;
                });
            this.cdr.detectChanges();
        }
        else {
            if (this.ph.name &&
                datetime &&
                this.pickUpLocation &&
                this.myGcode.destinationSetName &&
                this.actualPrice &&
                yu)
                this.ph
                    .Cancelled(this.allChat, this.ph.name, datetime, this.pickUpLocation, this.myGcode.destinationSetName, this.actualPrice, yu, charge, this.mySurch, this.dProvider.price, this.outofstatecharge)
                    .then(() => {
                    this.ph
                        .CancelledMe2(this.driver_ID, this.ph.name, datetime, this.pickUpLocation, this.myGcode.destinationSetName, this.actualPrice, yu, charge, this.mySurch, this.dProvider.price, this.outofstatecharge)
                        .then(() => { });
                    this.ph
                        .CancelledMe(this.ph.name, datetime, this.pickUpLocation, this.myGcode.destinationSetName, this.actualPrice, yu, charge, this.mySurch, this.dProvider.price, this.outofstatecharge)
                        .then(() => { });
                    this.ph
                        .getCancelledProfile()
                        .child("Cancelled/documents")
                        .on("value", (snapshot) => {
                        this.items = [];
                        snapshot.forEach((snap) => {
                            if (snap.val().type == "Rider")
                                this.items.push({
                                    key: snap.key,
                                    text: snap.val().title,
                                    type: snap.val().type,
                                    status: snap.val().status,
                                });
                            return false;
                        });
                    });
                    this.currentCar = null;
                });
            this.cdr.detectChanges();
        }
        this.pop.driverEnded = false;
        this.pop.hasCleared = true;
        this.storage.set("pop.hasCleared", this.pop.hasCleared);
        this.removeStorage();
    }
    hideFunctionsOnDriverFound() {
        this.cMap.onbar2 = false;
        this.cMap.onbar3 = true;
        this.pop.onRequest = true;
        this.storage.set("cMap.onbar3", this.cMap.onbar3);
        this.storage.set("pop.onRequest", this.pop.onRequest);
        this.shortMap = true; //this is for the toggle to bring up the home widget
        this.cMap.selected_destination_bar = false; //show menu bar
        this.cMap.toggleBtn = true;
        this.cMap.onPointerHide = true;
        this.storage.set("shortMap", this.shortMap);
        this.storage.set("cMap.selected_destination_bar", this.cMap.selected_destination_bar);
        this.storage.set("cMap.onbar2", this.cMap.onbar2);
        this.watch.unsubscribe();
        // if (this.cMap.watch_to_drop) {
        //     this.cMap.watch_to_drop.unsubscribe();
        // }
        if (this.cMap.watch2) {
            this.cMap.watch2.unsubscribe();
        }
        if (this.cMap.watch) {
            this.cMap.watch.unsubscribe();
        }
        this.cdr.detectChanges();
        // if (this.cMap.watch_restart) {
        //     this.cMap.watch_restart.unsubscribe();
        //     console.log("watch_restart UNSUBSCRIBED")
        // }
    }
    hideFunctions() {
        this.hideNews = true;
        this.cMap.hasbooked = true;
        this.cMap.shove = false;
        ///hide and remove some properties on user request.
        this.cMap.onbar2 = true;
        this.cMap.selected_destination_bar = false;
        this.storage.set("cMap.selected_destination_bar", this.cMap.selected_destination_bar);
        this.storage.set("cMap.onbar2", this.cMap.onbar2);
        this.cMap.norideavailable = false;
        this.cMap.canShowchoiceTab = false;
        this.storage.set("cMap.canShowchoiceTab", this.cMap.canShowchoiceTab);
        this.storage.set("cMap.norideavailable", this.cMap.norideavailable);
        if (this.platform.is("cordova") || this.platform.is("mobileweb")) {
            this.cMap.map.setClickable(true);
        }
        // this.cMap.map.clear().then(() => {
        //   this.cMap.map.setCameraZoom(16);
        // });
        this.startedNavigation = true;
        this.pop.onRequest = true;
        this.storage.set("pop.onRequest", this.pop.onRequest);
        //  this.cMap.isCarAvailable = false;
        this.dProvider.calculateBtn = false;
        this.watch.unsubscribe();
        // if (this.cMap.watch_to_drop) {
        //     this.cMap.watch_to_drop.unsubscribe();
        // }
        if (this.cMap.watch2) {
            this.cMap.watch2.unsubscribe();
        }
        if (this.cMap.watch) {
            this.cMap.watch.unsubscribe();
        }
        // if (this.cMap.watch_restart) {
        //     this.cMap.watch_restart.unsubscribe();
        //     console.log("watch_restart UNSUBSCRIBED")
        // }
        // this.cMap.map.clear();
        this.cdr.detectChanges();
    }
    Onconnect(activity) {
        this.pop.presentSimpleLoader(this.lp.translate()[0].driverfound);
        // this.driver_ID = activity.driver_ID
        if (this.platform.is("cordova")) {
            this.vibration.vibrate(1000);
            this.audioPlayer.play();
        }
        this.hideFunctionsOnDriverFound();
        this.pop.uid = this.uid;
        if (this.platform.is("cordova") && this.platform.is("mobileweb")) {
            this.cMap.map.setClickable(true);
        }
        //  this.eventProvider.UpdateSate(true, this.uid);
        //console.log(this.uid)
        this.DriverFound(activity.Driver_location, activity.Driver_plate, activity.Driver_carType, activity.Driver_name, activity.Driver_seats, activity.Driver_Positive_rating, activity.Driver_picture, activity.Client_location[0], activity.Client_location[1], activity.Driver_number);
        // document.getElementById("destination").innerHTML =
        //   this.lp.translate()[0].dest;
        // this.storage.set(`currentUserId`, this.uid);
    }
    ShowPrevious() { }
    Charged(price) {
        if (this.platform.is("cordova") && this.platform.is("mobileweb")) {
            this.cMap.map.setClickable(false);
        }
        this.price = price; //|| activity.Pool_price;
        if (this.paymentType == 2) {
            this.act.getActivityProfile(this.uid).update({
                Client_hasPaid: true,
            });
        }
        else {
            this.act.getActivityProfile(this.uid).update({
                Client_paidCash: true,
            });
        }
        // })
    }
    getPrice() {
        this.ph.getPricing().limitToLast(1).once("value", (data) => {
            if (data.val()) {
                data.forEach((snap) => {
                    let basefare = snap.val().base;
                    let pricePerKm = snap.val().pricePerKm;
                    let surge = snap.val().surge;
                    this.basefar = basefare;
                    let cal_price = Math.round(((basefare) * pricePerKm));
                });
            }
            else {
            }
        });
    }
    checkPromoExist() {
        let id = this.ph.id;
        this.ph
            .getAllUsedCodes()
            .orderByChild("rider_id")
            .equalTo(id)
            .once("value", (data) => {
            if (data.val()) {
                data.forEach((snap) => {
                    this.status = snap.val().status;
                    let perc = snap.val().percentage;
                    if (this.status == "activated") {
                        this.percentage = perc / 100;
                    }
                    else {
                    }
                });
            }
            else {
            }
        });
    }
};
HomePage.ctorParameters = () => [
    { type: _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_31__["SocialSharing"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_6__["Storage"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_24__["HttpClient"] },
    { type: src_app_services_language_service__WEBPACK_IMPORTED_MODULE_11__["LanguageService"] },
    { type: src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_12__["AuthService"] },
    { type: _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_9__["OneSignal"] },
    { type: src_app_services_activity_service__WEBPACK_IMPORTED_MODULE_13__["ActivityService"] },
    { type: src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_14__["SettingsService"] },
    { type: _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_8__["StatusBar"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _ionic_native_vibration_ngx__WEBPACK_IMPORTED_MODULE_7__["Vibration"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: src_app_services_native_map_container_service__WEBPACK_IMPORTED_MODULE_15__["NativeMapContainerService"] },
    { type: _ionic_native_call_number_ngx__WEBPACK_IMPORTED_MODULE_3__["CallNumber"] },
    { type: src_app_services_geocoder_service__WEBPACK_IMPORTED_MODULE_16__["GeocoderService"] },
    { type: src_app_services_directionservice_service__WEBPACK_IMPORTED_MODULE_17__["DirectionserviceService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
    { type: src_app_services_onesignal_service__WEBPACK_IMPORTED_MODULE_19__["OnesignalService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"] },
    { type: src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_20__["PopUpService"] },
    { type: src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_18__["ProfileService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: src_app_services_event_service__WEBPACK_IMPORTED_MODULE_21__["EventService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_29__["Router"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"] },
    { type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_30__["Geolocation"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] }
];
HomePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-home",
        template: __webpack_require__(/*! raw-loader!./home.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/home/home.page.html"),
        styles: [__webpack_require__(/*! ./home.page.scss */ "./src/app/pages/home/home.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_31__["SocialSharing"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_6__["Storage"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_24__["HttpClient"],
        src_app_services_language_service__WEBPACK_IMPORTED_MODULE_11__["LanguageService"],
        src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_12__["AuthService"],
        _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_9__["OneSignal"],
        src_app_services_activity_service__WEBPACK_IMPORTED_MODULE_13__["ActivityService"],
        src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_14__["SettingsService"],
        _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_8__["StatusBar"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
        _ionic_native_vibration_ngx__WEBPACK_IMPORTED_MODULE_7__["Vibration"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
        src_app_services_native_map_container_service__WEBPACK_IMPORTED_MODULE_15__["NativeMapContainerService"],
        _ionic_native_call_number_ngx__WEBPACK_IMPORTED_MODULE_3__["CallNumber"],
        src_app_services_geocoder_service__WEBPACK_IMPORTED_MODULE_16__["GeocoderService"],
        src_app_services_directionservice_service__WEBPACK_IMPORTED_MODULE_17__["DirectionserviceService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"],
        src_app_services_onesignal_service__WEBPACK_IMPORTED_MODULE_19__["OnesignalService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"],
        src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_20__["PopUpService"],
        src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_18__["ProfileService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
        src_app_services_event_service__WEBPACK_IMPORTED_MODULE_21__["EventService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_29__["Router"],
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"],
        _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_30__["Geolocation"],
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"]])
], HomePage);



/***/ }),

/***/ "./src/app/pages/rate/rate.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/rate/rate.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".sc-ion-modal-md-h {\n  --width: 86%;\n  --min-width: auto;\n  --max-width: auto;\n  --height: 70% !important;\n  --min-height: auto;\n  --max-height: auto;\n  --overflow: hidden;\n  --border-radius: 0;\n  --border-width: 0;\n  --border-style: none;\n  --border-color: transparent;\n  --background: var(--ion-background-color, #fff);\n  --box-shadow: none;\n}\n\n.inner-scroll {\n  left: 0px;\n  right: 0px;\n  top: calc(var(--offset-top) * -1);\n  bottom: calc(var(--offset-bottom) * -1);\n  padding-left: var(--padding-start);\n  padding-right: var(--padding-end);\n  padding-top: calc(var(--padding-top) + var(--offset-top));\n  padding-bottom: calc( var(--padding-bottom) + var(--keyboard-offset) + var(--offset-bottom) );\n  position: absolute;\n  background: #cf0d0dc7 !important;\n  color: var(--color);\n  box-sizing: border-box;\n  overflow: hidden;\n}\n\n.driverFound {\n  height: 47%;\n  width: 90%;\n  margin-left: 5%;\n  background: white;\n  position: absolute;\n  bottom: 30%;\n  z-index: 1;\n  border-top-left-radius: 30px;\n  border-top-right-radius: 30px;\n}\n\n.request-for-ride2 {\n  height: 200px;\n}\n\n.headSection {\n  background-color: #fbb91d;\n  color: #000000;\n  margin-top: -6%;\n  border-top-left-radius: 27px;\n  border-top-right-radius: 30px;\n  display: inline-block;\n  width: 100%;\n  height: 35%;\n  text-align: center;\n}\n\n.moveHeader {\n  -webkit-box-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n          align-items: center;\n  align-items: center;\n  margin-top: 20px;\n  margin-left: 0%;\n  margin-right: 0%;\n  width: 100%;\n}\n\n.centerText {\n  text-align: center;\n  font-size: 16px !important;\n}\n\n.resultContainer {\n  width: 100%;\n  position: relative;\n  left: 0;\n  margin-top: 16px;\n  margin-bottom: 0px;\n  padding: 10px;\n}\n\n.content-wrap,\n.img-wrapper {\n  display: inline-block;\n  margin-left: 50px;\n}\n\n#drivericonSize {\n  font-size: 70px !important;\n}\n\n.bookImage,\n.bookTitle,\n.bookPrice {\n  margin-left: 20px;\n}\n\n.resultContainer2 {\n  width: 100%;\n  position: relative;\n  left: 0;\n  margin-top: 0px;\n  margin-bottom: 0px;\n  padding: 10px;\n}\n\n.content-wrap2 {\n  display: inline-block;\n}\n\n.dott {\n  width: 20px;\n  height: 20px;\n}\n\n.centerBtn {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n          align-items: center;\n}\n\nul {\n  padding: 0px;\n  color: #0a64eb !important;\n}\n\nul.rating li {\n  padding: 5px 10px !important;\n  background: none;\n  color: #0a64eb !important;\n}\n\nul.rating li ion-icon {\n  font-size: 50px;\n}\n\n.reviewspace {\n  margin-top: 30%;\n}\n\nform {\n  height: 300px auto;\n}\n\nform .button {\n  border-radius: 12px;\n}\n\n#myInput {\n  border: 1.5px solid #0a64eb;\n  border-radius: 12px;\n  height: 100px;\n  margin-left: 0%;\n  margin-bottom: 6%;\n  width: 220px;\n  margin-top: 17px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYWhhL0lvbmljQXBwcy9DNFJpZGUtSU9TLTIwMjQvY2FsbDRyaWRlLXBhc3Nlbmdlci9zcmMvYXBwL3BhZ2VzL3JhdGUvcmF0ZS5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL3JhdGUvcmF0ZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtFQUNBLHdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0VBQ0EsMkJBQUE7RUFDQSwrQ0FBQTtFQUNBLGtCQUFBO0FDQ0Y7O0FEZ0JBO0VBQ0UsU0FBQTtFQUNBLFVBQUE7RUFDQSxpQ0FBQTtFQUNBLHVDQUFBO0VBQ0Esa0NBQUE7RUFDQSxpQ0FBQTtFQUNBLHlEQUFBO0VBQ0EsNkZBQUE7RUFHQSxrQkFBQTtFQUNBLGdDQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtFQUNBLGdCQUFBO0FDZkY7O0FEc0JBO0VBQ0UsV0FBQTtFQUNBLFVBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxVQUFBO0VBQ0EsNEJBQUE7RUFDQSw2QkFBQTtBQ25CRjs7QURzQkE7RUFDRSxhQUFBO0FDbkJGOztBRHNCQTtFQUNFLHlCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSw0QkFBQTtFQUNBLDZCQUFBO0VBQ0EscUJBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0FDbkJGOztBRHFCQTtFQUNFLHdCQUFBO1VBQUEsdUJBQUE7RUFDQSx5QkFBQTtVQUFBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7QUNsQkY7O0FEcUJBO0VBQ0Usa0JBQUE7RUFDQSwwQkFBQTtBQ2xCRjs7QURxQkE7RUFDRSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxPQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7QUNsQkY7O0FEc0JBOztFQUVFLHFCQUFBO0VBQ0EsaUJBQUE7QUNuQkY7O0FEc0JBO0VBQ0UsMEJBQUE7QUNuQkY7O0FEc0JBOzs7RUFHRSxpQkFBQTtBQ25CRjs7QURzQkE7RUFDRSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxPQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtBQ25CRjs7QURzQkE7RUFDRSxxQkFBQTtBQ25CRjs7QURzQkE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtBQ25CRjs7QURzQkE7RUFDRSxvQkFBQTtFQUFBLGFBQUE7RUFDQSx3QkFBQTtVQUFBLHVCQUFBO0VBQ0EseUJBQUE7VUFBQSxtQkFBQTtBQ25CRjs7QUR3QkE7RUFDRSxZQUFBO0VBQ0EseUJBQUE7QUNyQkY7O0FEc0JFO0VBQ0UsNEJBQUE7RUFDQSxnQkFBQTtFQUNBLHlCQUFBO0FDcEJKOztBRHNCSTtFQUNFLGVBQUE7QUNwQk47O0FEeUJBO0VBQ0UsZUFBQTtBQ3RCRjs7QUR5QkE7RUFDRSxrQkFBQTtBQ3RCRjs7QUR1QkU7RUFDRSxtQkFBQTtBQ3JCSjs7QUR5QkE7RUFDRSwyQkFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtBQ3RCRiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3JhdGUvcmF0ZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc2MtaW9uLW1vZGFsLW1kLWgge1xyXG4gIC0td2lkdGg6IDg2JTtcclxuICAtLW1pbi13aWR0aDogYXV0bztcclxuICAtLW1heC13aWR0aDogYXV0bztcclxuICAtLWhlaWdodDogNzAlICFpbXBvcnRhbnQ7XHJcbiAgLS1taW4taGVpZ2h0OiBhdXRvO1xyXG4gIC0tbWF4LWhlaWdodDogYXV0bztcclxuICAtLW92ZXJmbG93OiBoaWRkZW47XHJcbiAgLS1ib3JkZXItcmFkaXVzOiAwO1xyXG4gIC0tYm9yZGVyLXdpZHRoOiAwO1xyXG4gIC0tYm9yZGVyLXN0eWxlOiBub25lO1xyXG4gIC0tYm9yZGVyLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yLCAjZmZmKTtcclxuICAtLWJveC1zaGFkb3c6IG5vbmU7XHJcbiAgLy8gbGVmdDogMDtcclxuICAvLyByaWdodDogMDtcclxuICAvLyB0b3A6IDA7XHJcbiAgLy8gYm90dG9tOiAwO1xyXG4gIC8vIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xyXG4gIC8vIGRpc3BsYXk6IGZsZXg7XHJcbiAgLy8gcG9zaXRpb246IGFic29sdXRlO1xyXG4gIC8vIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XHJcbiAgLy8gYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAvLyAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XHJcbiAgLy8ganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgLy8gY29udGFpbjogc3RyaWN0O1xyXG59XHJcblxyXG4vL05FVyBERVNJR04gQ1NTXHJcblxyXG4uaW5uZXItc2Nyb2xsIHtcclxuICBsZWZ0OiAwcHg7XHJcbiAgcmlnaHQ6IDBweDtcclxuICB0b3A6IGNhbGModmFyKC0tb2Zmc2V0LXRvcCkgKiAtMSk7XHJcbiAgYm90dG9tOiBjYWxjKHZhcigtLW9mZnNldC1ib3R0b20pICogLTEpO1xyXG4gIHBhZGRpbmctbGVmdDogdmFyKC0tcGFkZGluZy1zdGFydCk7XHJcbiAgcGFkZGluZy1yaWdodDogdmFyKC0tcGFkZGluZy1lbmQpO1xyXG4gIHBhZGRpbmctdG9wOiBjYWxjKHZhcigtLXBhZGRpbmctdG9wKSArIHZhcigtLW9mZnNldC10b3ApKTtcclxuICBwYWRkaW5nLWJvdHRvbTogY2FsYyhcclxuICAgIHZhcigtLXBhZGRpbmctYm90dG9tKSArIHZhcigtLWtleWJvYXJkLW9mZnNldCkgKyB2YXIoLS1vZmZzZXQtYm90dG9tKVxyXG4gICk7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIGJhY2tncm91bmQ6ICNjZjBkMGRjNyAhaW1wb3J0YW50O1xyXG4gIGNvbG9yOiB2YXIoLS1jb2xvcik7XHJcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICBvdmVyZmxvdzogaGlkZGVuO1xyXG59XHJcblxyXG4vLyAuYmFja2dyb3VuZENvbG9yIHtcclxuLy8gICBiYWNrZ3JvdW5kLWNvbG9yOiBhcXVhO1xyXG4vLyAgIGhlaWdodDogOTBweDtcclxuLy8gfVxyXG4uZHJpdmVyRm91bmQge1xyXG4gIGhlaWdodDogNDclO1xyXG4gIHdpZHRoOiA5MCU7XHJcbiAgbWFyZ2luLWxlZnQ6IDUlO1xyXG4gIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBib3R0b206IDMwJTtcclxuICB6LWluZGV4OiAxO1xyXG4gIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDMwcHg7XHJcbiAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDMwcHg7XHJcbn1cclxuXHJcbi5yZXF1ZXN0LWZvci1yaWRlMiB7XHJcbiAgaGVpZ2h0OiAyMDBweDtcclxufVxyXG5cclxuLmhlYWRTZWN0aW9uIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmJiOTFkO1xyXG4gIGNvbG9yOiAjMDAwMDAwO1xyXG4gIG1hcmdpbi10b3A6IC02JTtcclxuICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAyN3B4O1xyXG4gIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAzMHB4O1xyXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDM1JTtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuLm1vdmVIZWFkZXIge1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gIG1hcmdpbi1sZWZ0OiAwJTtcclxuICBtYXJnaW4tcmlnaHQ6IDAlO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4uY2VudGVyVGV4dCB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGZvbnQtc2l6ZTogMTZweCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4ucmVzdWx0Q29udGFpbmVyIHtcclxuICB3aWR0aDogMTAwJTtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgbGVmdDogMDtcclxuICBtYXJnaW4tdG9wOiAxNnB4O1xyXG4gIG1hcmdpbi1ib3R0b206IDBweDtcclxuICBwYWRkaW5nOiAxMHB4O1xyXG4gIC8vIGJvcmRlci1ib3R0b206ICNiYWJhYmEgc29saWQgMXB4O1xyXG59XHJcblxyXG4uY29udGVudC13cmFwLFxyXG4uaW1nLXdyYXBwZXIge1xyXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICBtYXJnaW4tbGVmdDogNTBweDtcclxufVxyXG5cclxuI2RyaXZlcmljb25TaXplIHtcclxuICBmb250LXNpemU6IDcwcHggIWltcG9ydGFudDtcclxufVxyXG5cclxuLmJvb2tJbWFnZSxcclxuLmJvb2tUaXRsZSxcclxuLmJvb2tQcmljZSB7XHJcbiAgbWFyZ2luLWxlZnQ6IDIwcHg7XHJcbn1cclxuXHJcbi5yZXN1bHRDb250YWluZXIyIHtcclxuICB3aWR0aDogMTAwJTtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgbGVmdDogMDtcclxuICBtYXJnaW4tdG9wOiAwcHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogMDBweDtcclxuICBwYWRkaW5nOiAxMHB4O1xyXG59XHJcblxyXG4uY29udGVudC13cmFwMiB7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG59XHJcblxyXG4uZG90dCB7XHJcbiAgd2lkdGg6IDIwcHg7XHJcbiAgaGVpZ2h0OiAyMHB4O1xyXG59XHJcblxyXG4uY2VudGVyQnRuIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbn1cclxuXHJcbi8vIEVORCBORVcgREVTSUdOIENTU1xyXG5cclxudWwge1xyXG4gIHBhZGRpbmc6IDBweDtcclxuICBjb2xvcjogcmdiKDEwLCAxMDAsIDIzNSkgIWltcG9ydGFudDtcclxuICAmLnJhdGluZyBsaSB7XHJcbiAgICBwYWRkaW5nOiA1cHggMTBweCAhaW1wb3J0YW50O1xyXG4gICAgYmFja2dyb3VuZDogbm9uZTtcclxuICAgIGNvbG9yOiByZ2IoMTAsIDEwMCwgMjM1KSAhaW1wb3J0YW50O1xyXG5cclxuICAgIGlvbi1pY29uIHtcclxuICAgICAgZm9udC1zaXplOiA1MHB4O1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuLnJldmlld3NwYWNlIHtcclxuICBtYXJnaW4tdG9wOiAzMCU7XHJcbn1cclxuXHJcbmZvcm0ge1xyXG4gIGhlaWdodDogMzAwcHggYXV0bztcclxuICAuYnV0dG9uIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDEycHg7XHJcbiAgfVxyXG59XHJcblxyXG4jbXlJbnB1dCB7XHJcbiAgYm9yZGVyOiAxLjVweCBzb2xpZCByZ2IoMTAsIDEwMCwgMjM1KTtcclxuICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG4gIGhlaWdodDogMTAwcHg7XHJcbiAgbWFyZ2luLWxlZnQ6IDAlO1xyXG4gIG1hcmdpbi1ib3R0b206IDYlO1xyXG4gIHdpZHRoOiAyMjBweDtcclxuICBtYXJnaW4tdG9wOiAxN3B4O1xyXG59XHJcbiIsIi5zYy1pb24tbW9kYWwtbWQtaCB7XG4gIC0td2lkdGg6IDg2JTtcbiAgLS1taW4td2lkdGg6IGF1dG87XG4gIC0tbWF4LXdpZHRoOiBhdXRvO1xuICAtLWhlaWdodDogNzAlICFpbXBvcnRhbnQ7XG4gIC0tbWluLWhlaWdodDogYXV0bztcbiAgLS1tYXgtaGVpZ2h0OiBhdXRvO1xuICAtLW92ZXJmbG93OiBoaWRkZW47XG4gIC0tYm9yZGVyLXJhZGl1czogMDtcbiAgLS1ib3JkZXItd2lkdGg6IDA7XG4gIC0tYm9yZGVyLXN0eWxlOiBub25lO1xuICAtLWJvcmRlci1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWJhY2tncm91bmQtY29sb3IsICNmZmYpO1xuICAtLWJveC1zaGFkb3c6IG5vbmU7XG59XG5cbi5pbm5lci1zY3JvbGwge1xuICBsZWZ0OiAwcHg7XG4gIHJpZ2h0OiAwcHg7XG4gIHRvcDogY2FsYyh2YXIoLS1vZmZzZXQtdG9wKSAqIC0xKTtcbiAgYm90dG9tOiBjYWxjKHZhcigtLW9mZnNldC1ib3R0b20pICogLTEpO1xuICBwYWRkaW5nLWxlZnQ6IHZhcigtLXBhZGRpbmctc3RhcnQpO1xuICBwYWRkaW5nLXJpZ2h0OiB2YXIoLS1wYWRkaW5nLWVuZCk7XG4gIHBhZGRpbmctdG9wOiBjYWxjKHZhcigtLXBhZGRpbmctdG9wKSArIHZhcigtLW9mZnNldC10b3ApKTtcbiAgcGFkZGluZy1ib3R0b206IGNhbGMoIHZhcigtLXBhZGRpbmctYm90dG9tKSArIHZhcigtLWtleWJvYXJkLW9mZnNldCkgKyB2YXIoLS1vZmZzZXQtYm90dG9tKSApO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJhY2tncm91bmQ6ICNjZjBkMGRjNyAhaW1wb3J0YW50O1xuICBjb2xvcjogdmFyKC0tY29sb3IpO1xuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuXG4uZHJpdmVyRm91bmQge1xuICBoZWlnaHQ6IDQ3JTtcbiAgd2lkdGg6IDkwJTtcbiAgbWFyZ2luLWxlZnQ6IDUlO1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBib3R0b206IDMwJTtcbiAgei1pbmRleDogMTtcbiAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMzBweDtcbiAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDMwcHg7XG59XG5cbi5yZXF1ZXN0LWZvci1yaWRlMiB7XG4gIGhlaWdodDogMjAwcHg7XG59XG5cbi5oZWFkU2VjdGlvbiB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmYmI5MWQ7XG4gIGNvbG9yOiAjMDAwMDAwO1xuICBtYXJnaW4tdG9wOiAtNiU7XG4gIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDI3cHg7XG4gIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAzMHB4O1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDM1JTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4ubW92ZUhlYWRlciB7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBtYXJnaW4tdG9wOiAyMHB4O1xuICBtYXJnaW4tbGVmdDogMCU7XG4gIG1hcmdpbi1yaWdodDogMCU7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uY2VudGVyVGV4dCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAxNnB4ICFpbXBvcnRhbnQ7XG59XG5cbi5yZXN1bHRDb250YWluZXIge1xuICB3aWR0aDogMTAwJTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBsZWZ0OiAwO1xuICBtYXJnaW4tdG9wOiAxNnB4O1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gIHBhZGRpbmc6IDEwcHg7XG59XG5cbi5jb250ZW50LXdyYXAsXG4uaW1nLXdyYXBwZXIge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIG1hcmdpbi1sZWZ0OiA1MHB4O1xufVxuXG4jZHJpdmVyaWNvblNpemUge1xuICBmb250LXNpemU6IDcwcHggIWltcG9ydGFudDtcbn1cblxuLmJvb2tJbWFnZSxcbi5ib29rVGl0bGUsXG4uYm9va1ByaWNlIHtcbiAgbWFyZ2luLWxlZnQ6IDIwcHg7XG59XG5cbi5yZXN1bHRDb250YWluZXIyIHtcbiAgd2lkdGg6IDEwMCU7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbGVmdDogMDtcbiAgbWFyZ2luLXRvcDogMHB4O1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gIHBhZGRpbmc6IDEwcHg7XG59XG5cbi5jb250ZW50LXdyYXAyIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xufVxuXG4uZG90dCB7XG4gIHdpZHRoOiAyMHB4O1xuICBoZWlnaHQ6IDIwcHg7XG59XG5cbi5jZW50ZXJCdG4ge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cblxudWwge1xuICBwYWRkaW5nOiAwcHg7XG4gIGNvbG9yOiAjMGE2NGViICFpbXBvcnRhbnQ7XG59XG51bC5yYXRpbmcgbGkge1xuICBwYWRkaW5nOiA1cHggMTBweCAhaW1wb3J0YW50O1xuICBiYWNrZ3JvdW5kOiBub25lO1xuICBjb2xvcjogIzBhNjRlYiAhaW1wb3J0YW50O1xufVxudWwucmF0aW5nIGxpIGlvbi1pY29uIHtcbiAgZm9udC1zaXplOiA1MHB4O1xufVxuXG4ucmV2aWV3c3BhY2Uge1xuICBtYXJnaW4tdG9wOiAzMCU7XG59XG5cbmZvcm0ge1xuICBoZWlnaHQ6IDMwMHB4IGF1dG87XG59XG5mb3JtIC5idXR0b24ge1xuICBib3JkZXItcmFkaXVzOiAxMnB4O1xufVxuXG4jbXlJbnB1dCB7XG4gIGJvcmRlcjogMS41cHggc29saWQgIzBhNjRlYjtcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcbiAgaGVpZ2h0OiAxMDBweDtcbiAgbWFyZ2luLWxlZnQ6IDAlO1xuICBtYXJnaW4tYm90dG9tOiA2JTtcbiAgd2lkdGg6IDIyMHB4O1xuICBtYXJnaW4tdG9wOiAxN3B4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/rate/rate.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/rate/rate.page.ts ***!
  \*****************************************/
/*! exports provided: RatePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RatePage", function() { return RatePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_services_language_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/language.service */ "./src/app/services/language.service.ts");
/* harmony import */ var src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/pop-up.service */ "./src/app/services/pop-up.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/profile.service */ "./src/app/services/profile.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");







let RatePage = class RatePage {
    constructor(navCtrl, lp, pop, actRoute, prof, modal, route, navParams) {
        this.navCtrl = navCtrl;
        this.lp = lp;
        this.pop = pop;
        this.actRoute = actRoute;
        this.prof = prof;
        this.modal = modal;
        this.route = route;
        this.navParams = navParams;
        this.name = this.route.snapshot.paramMap.get("name");
        this.todo = {
            description: " ",
        };
        this.price = this.navParams.get('price');
        this.waitTimeCost = this.navParams.get('waitTimeCost');
        console.log("Driver Name:::", this.name);
        console.log("Ride Cost:::", this.price);
        console.log("Wait time Cost:::", this.waitTimeCost);
        this.total = this.waitTimeCost + this.price;
        console.log("Total Cost will be :::", this.total);
        this.actRoute.queryParams.subscribe((params) => {
            if (params && params.special) {
                this.data = JSON.parse(params.special);
                console.log("RATTTTIINN DATA *******", this.data);
            }
        });
    }
    ionViewWillEnter() {
        console.log("INSIDE RATTTTIINN *******");
    }
    onRateChange($event) {
        this.rateNumber = $event;
        console.log("RATING NUMBER__-->>", this.rateNumber);
        this.pop.presentLoader("");
        setTimeout(() => {
            this.pop.hideLoader();
        }, 1000);
    }
    logForm() {
        console.log(this.todo);
        if (this.rateNumber != null) {
            const value = this.data;
            console.log(this.rateNumber);
            this.prof
                .RateDriver(value, this.rateNumber, this.todo.description, true)
                .then((suc) => {
                // this.navCtrl.pop();
            });
            this.modal.dismiss(1);
        }
        else {
            this.pop.showPimp(this.lp.translate()[0].alertcar);
        }
    }
    closeModal() {
        this.modal.dismiss(2);
    }
    acceptModal() {
        console.log("ACCEPTED REQUEST");
        this.modal.dismiss(1);
    }
    ngOnInit() { }
};
RatePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"] },
    { type: src_app_services_language_service__WEBPACK_IMPORTED_MODULE_2__["LanguageService"] },
    { type: src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_3__["PopUpService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"] },
    { type: src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_5__["ProfileService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavParams"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("myInput", { static: true }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
], RatePage.prototype, "myInput", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], RatePage.prototype, "eventId", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], RatePage.prototype, "positive_Rating", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], RatePage.prototype, "negative_Rating", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], RatePage.prototype, "time", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], RatePage.prototype, "m_ID", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], RatePage.prototype, "custom_ID", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], RatePage.prototype, "waitTimeCost", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], RatePage.prototype, "price", void 0);
RatePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-rate",
        template: __webpack_require__(/*! raw-loader!./rate.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/rate/rate.page.html"),
        styles: [__webpack_require__(/*! ./rate.page.scss */ "./src/app/pages/rate/rate.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"],
        src_app_services_language_service__WEBPACK_IMPORTED_MODULE_2__["LanguageService"],
        src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_3__["PopUpService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"],
        src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_5__["ProfileService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavParams"]])
], RatePage);



/***/ }),

/***/ "./src/app/pages/trip-info/trip-info.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/pages/trip-info/trip-info.page.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-card-header {\n  text-align: center;\n  color: black;\n}\n\n.drive {\n  color: blue;\n}\n\n.price {\n  color: blue;\n}\n\n.date {\n  color: red;\n}\n\n.destination {\n  color: green;\n}\n\nh3 {\n  font-size: 1.2em;\n  color: blue;\n  text-align: center;\n}\n\nion-item {\n  color: black;\n  border-bottom: 1px solid #d8d8d8 !important;\n}\n\nh1 {\n  text-align: center;\n  font-size: 1.2em;\n  color: blue;\n}\n\np {\n  font-size: 1.3em;\n  height: auto;\n  color: black;\n}\n\nh3 {\n  font-size: 1em;\n  color: #5a5a5a;\n}\n\n.hists {\n  border: 1px solid #d8d8d8 !important;\n  font-size: 1.1em;\n  width: 100%;\n  border-radius: 12px;\n  background: white;\n  color: black;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYWhhL0lvbmljQXBwcy9DNFJpZGUtSU9TLTIwMjQvY2FsbDRyaWRlLXBhc3Nlbmdlci9zcmMvYXBwL3BhZ2VzL3RyaXAtaW5mby90cmlwLWluZm8ucGFnZS5zY3NzIiwic3JjL2FwcC9wYWdlcy90cmlwLWluZm8vdHJpcC1pbmZvLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFBO0VBQ0EsWUFBQTtBQ0NGOztBREVBO0VBQ0UsV0FBQTtBQ0NGOztBREVBO0VBQ0UsV0FBQTtBQ0NGOztBREVBO0VBQ0UsVUFBQTtBQ0NGOztBREVBO0VBQ0UsWUFBQTtBQ0NGOztBREVBO0VBQ0UsZ0JBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7QUNDRjs7QURFQTtFQUVFLFlBQUE7RUFDQSwyQ0FBQTtBQ0FGOztBREdBO0VBQ0Usa0JBQUE7RUFDQSxnQkFBQTtFQUVBLFdBQUE7QUNERjs7QURJQTtFQUNFLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7QUNERjs7QURJQTtFQUNFLGNBQUE7RUFFQSxjQUFBO0FDRkY7O0FES0E7RUFDRSxvQ0FBQTtFQUVBLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBRUEsaUJBQUE7RUFFQSxZQUFBO0FDTEYiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy90cmlwLWluZm8vdHJpcC1pbmZvLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jYXJkLWhlYWRlciB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGNvbG9yOiByZ2IoMCwgMCwgMCk7XHJcbn1cclxuXHJcbi5kcml2ZSB7XHJcbiAgY29sb3I6IGJsdWU7XHJcbn1cclxuXHJcbi5wcmljZSB7XHJcbiAgY29sb3I6IGJsdWU7XHJcbn1cclxuXHJcbi5kYXRlIHtcclxuICBjb2xvcjogcmVkO1xyXG59XHJcblxyXG4uZGVzdGluYXRpb24ge1xyXG4gIGNvbG9yOiBncmVlbjtcclxufVxyXG5cclxuaDMge1xyXG4gIGZvbnQtc2l6ZTogMS4yZW07XHJcbiAgY29sb3I6IGJsdWU7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG5pb24taXRlbSB7XHJcbiAgLy8gYm9yZGVyLXJhZGl1czogJGJ1dHRvblJhZGl1cztcclxuICBjb2xvcjogcmdiKDAsIDAsIDApO1xyXG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCByZ2IoMjE2LCAyMTYsIDIxNikgIWltcG9ydGFudDtcclxufVxyXG5cclxuaDEge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBmb250LXNpemU6IDEuMmVtO1xyXG4gIC8vd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcclxuICBjb2xvcjogYmx1ZTtcclxufVxyXG5cclxucCB7XHJcbiAgZm9udC1zaXplOiAxLjNlbTtcclxuICBoZWlnaHQ6IGF1dG87XHJcbiAgY29sb3I6IHJnYigwLCAwLCAwKTtcclxufVxyXG5cclxuaDMge1xyXG4gIGZvbnQtc2l6ZTogMWVtO1xyXG5cclxuICBjb2xvcjogcmdiKDkwLCA5MCwgOTApO1xyXG59XHJcblxyXG4uaGlzdHMge1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkIHJnYigyMTYsIDIxNiwgMjE2KSAhaW1wb3J0YW50O1xyXG5cclxuICBmb250LXNpemU6IDEuMWVtO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGJvcmRlci1yYWRpdXM6IDEycHg7XHJcbiAgLy9wYWRkaW5nOiA2cHg7XHJcbiAgYmFja2dyb3VuZDogd2hpdGU7XHJcblxyXG4gIGNvbG9yOiByZ2IoMCwgMCwgMCk7XHJcbn1cclxuIiwiaW9uLWNhcmQtaGVhZGVyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBjb2xvcjogYmxhY2s7XG59XG5cbi5kcml2ZSB7XG4gIGNvbG9yOiBibHVlO1xufVxuXG4ucHJpY2Uge1xuICBjb2xvcjogYmx1ZTtcbn1cblxuLmRhdGUge1xuICBjb2xvcjogcmVkO1xufVxuXG4uZGVzdGluYXRpb24ge1xuICBjb2xvcjogZ3JlZW47XG59XG5cbmgzIHtcbiAgZm9udC1zaXplOiAxLjJlbTtcbiAgY29sb3I6IGJsdWU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuaW9uLWl0ZW0ge1xuICBjb2xvcjogYmxhY2s7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZDhkOGQ4ICFpbXBvcnRhbnQ7XG59XG5cbmgxIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDEuMmVtO1xuICBjb2xvcjogYmx1ZTtcbn1cblxucCB7XG4gIGZvbnQtc2l6ZTogMS4zZW07XG4gIGhlaWdodDogYXV0bztcbiAgY29sb3I6IGJsYWNrO1xufVxuXG5oMyB7XG4gIGZvbnQtc2l6ZTogMWVtO1xuICBjb2xvcjogIzVhNWE1YTtcbn1cblxuLmhpc3RzIHtcbiAgYm9yZGVyOiAxcHggc29saWQgI2Q4ZDhkOCAhaW1wb3J0YW50O1xuICBmb250LXNpemU6IDEuMWVtO1xuICB3aWR0aDogMTAwJTtcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGNvbG9yOiBibGFjaztcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/trip-info/trip-info.page.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/trip-info/trip-info.page.ts ***!
  \***************************************************/
/*! exports provided: TripInfoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TripInfoPage", function() { return TripInfoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/profile.service */ "./src/app/services/profile.service.ts");
/* harmony import */ var src_app_services_language_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/language.service */ "./src/app/services/language.service.ts");
/* harmony import */ var src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/settings.service */ "./src/app/services/settings.service.ts");
/* harmony import */ var src_app_services_event_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/event.service */ "./src/app/services/event.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");








let TripInfoPage = class TripInfoPage {
    constructor(navCtrl, prof, lp, settings, eventProvider, route, modal) {
        this.navCtrl = navCtrl;
        this.prof = prof;
        this.lp = lp;
        this.settings = settings;
        this.eventProvider = eventProvider;
        this.route = route;
        this.modal = modal;
        this.items = [];
        this.info = this.route.snapshot.paramMap.get("info");
        this.toll = this.route.snapshot.paramMap.get("toll");
        this.accepted = this.route.snapshot.paramMap.get("accepted");
        this.arrived = this.route.snapshot.paramMap.get("arrived");
        this.distance = this.route.snapshot.paramMap.get("distance");
        this.float = parseFloat;
        this.currentEvent = {};
        this.total = 0;
        this.allTotals = 0;
        this.driverMade = 0;
        this.surcharges = [];
        this.totalSurge = 0;
        this.actual = 0;
        this.riderPaid = 0;
        this.totalRiderSurge = 0;
        this.totalDriverSurge = 0;
        this.percentRider = 0;
        this.flatRider = 0;
        this.percentDriver = 0;
        this.flatDriver = 0;
        this.riderPercents = [];
        this.driverPercents = [];
        this.math = Math;
        this.riderpaid = 0;
    }
    ionViewDidLoad() {
        console.log("INSIDE TRIP INFO", this.info);
        this.prof
            .getCompanies()
            .child("Cancelled/documents")
            .on("value", (snapshot) => {
            this.items = [];
            snapshot.forEach((snap) => {
                if (snap.val().type == "Rider")
                    this.items.push({
                        status: snap.val().status,
                        text: snap.val().title,
                    });
                this.items.sort();
                this.items.reverse();
                console.log(snap.val().tolls);
                return false;
            });
        });
        let g = [];
        let f = [];
        let b = [];
        let k = [];
        let o = [];
        let c = [];
        let n = [];
        let sa;
        console.log(this.currentEvent);
        this.tolls = this.toll;
        this.info.Client_realPrice;
        this.osc = this.info.Client_OutOfStateCharge;
        this.surcharges = this.info.Client_Surcharges;
        if (this.surcharges)
            for (let index = 0; index < this.surcharges.length; index++) {
                k.push(parseFloat(this.info.Client_Surcharges[index].price));
                const add = (a, b) => a + b;
                const result = k.reduce(add);
                this.totalSurge = result;
                this.actual = this.info.Client_price - this.totalSurge;
                console.log(this.totalSurge);
                this.info.Client_Surcharges[index].price;
            }
        this.surcharges.forEach((element) => {
            //if rider
            if (element.owner == 1) {
                //if percent
                if (element.bone == 1) {
                    let fo = (element.price / 100) * this.info.Client_realPrice;
                    console.log(element.price);
                    o.push(fo);
                    const add1 = (a, b) => a + b;
                    const result1 = o.reduce(add1);
                    this.percentRider = result1;
                    console.log(this.percentRider);
                }
                if (element.bone == 0) {
                    g.push(parseFloat(element.price));
                    const add = (a, b) => a + b;
                    const result = g.reduce(add);
                    this.flatRider = result;
                    console.log(result);
                    element.price;
                }
                this.totalRiderSurge = this.flatRider + this.percentRider;
                console.log(this.totalRiderSurge);
            }
            this.riderpaid = parseFloat(this.info.Client_price).toFixed(2);
            //if driver
            if (element.owner == 0) {
                //if percent
                if (element.bone == 1) {
                    let nb = element.price / 100;
                    console.log(nb * this.riderpaid);
                    let fo = nb * this.riderpaid;
                    n.push(fo);
                    const add2 = (a, b) => a + b;
                    const result2 = n.reduce(add2);
                    this.percentDriver = result2;
                    console.log((Math.floor(element.price) / 100) * this.riderpaid);
                }
                //if flat fee
                if (element.bone == 0) {
                    c.push(parseFloat(element.price));
                    const add4 = (a, b) => a + b;
                    const result4 = c.reduce(add4);
                    this.flatDriver = result4;
                    console.log(result4);
                }
                this.totalDriverSurge = this.flatDriver + this.percentDriver;
                console.log(this.totalDriverSurge, this.flatDriver, this.percentDriver);
            }
        });
        // console.log(this.actual, g, this.currentEvent.surcharge[index].price);
        if (this.info.Client_toll)
            for (let index = 0; index < this.info.Client_toll.length; index++) {
                b.push(this.info.Client_toll[index].tagCost);
                const add = (a, b) => a + b;
                const result = b.reduce(add);
                this.total = result;
                console.log(this.total, g);
            }
        this.driverMade = (this.riderpaid - this.totalDriverSurge).toFixed(2);
    }
    closeModal() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            yield this.modal.dismiss(null);
        });
    }
    onChange(e) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            console.log("CLOSING TRIP INFO");
            yield this.modal.dismiss(e);
        });
    }
    ngOnInit() { }
};
TripInfoPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_3__["ProfileService"] },
    { type: src_app_services_language_service__WEBPACK_IMPORTED_MODULE_4__["LanguageService"] },
    { type: src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_5__["SettingsService"] },
    { type: src_app_services_event_service__WEBPACK_IMPORTED_MODULE_6__["EventService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] }
];
TripInfoPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-trip-info",
        template: __webpack_require__(/*! raw-loader!./trip-info.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/trip-info/trip-info.page.html"),
        styles: [__webpack_require__(/*! ./trip-info.page.scss */ "./src/app/pages/trip-info/trip-info.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
        src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_3__["ProfileService"],
        src_app_services_language_service__WEBPACK_IMPORTED_MODULE_4__["LanguageService"],
        src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_5__["SettingsService"],
        src_app_services_event_service__WEBPACK_IMPORTED_MODULE_6__["EventService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])
], TripInfoPage);



/***/ })

}]);
//# sourceMappingURL=pages-home-home-module-es2015.js.map