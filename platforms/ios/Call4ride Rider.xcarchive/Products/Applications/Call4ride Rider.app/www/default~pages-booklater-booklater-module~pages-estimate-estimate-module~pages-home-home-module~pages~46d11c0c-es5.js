(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-booklater-booklater-module~pages-estimate-estimate-module~pages-home-home-module~pages~46d11c0c"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/autocomplete/autocomplete.page.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/autocomplete/autocomplete.page.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header no-border>\n  <ion-toolbar style=\"margin-top: 20px\">\n    <!-- <ion-button size=\"large\" fill=\"clear\" (click)=\"goBack()\">\n      <ion-icon name=\"arrow-back\"></ion-icon>\n      <span style=\"padding-left: 30px; font-size: 1.1em\">Choose Destination</span>\n    </ion-button> -->\n\n    <ion-button\n      color=\"dark\"\n      (click)=\"goBack()\"\n      style=\"\n        --border-radius: 100%;\n        width: 50px;\n        height: 50px;\n        --vertical-align: middle;\n        --padding-start: -5px;\n        --padding-end: -5px;\n      \"\n    >\n      <ion-icon color=\"light\" name=\"arrow-back\" slot=\"icon-only\"></ion-icon>\n      <!-- <span style=\"padding-left: 30px; font-size: 1.1em; color: black\"\n        >Choose Destination</span\n      > -->\n    </ion-button>\n    <span style=\"padding-left: 30px; font-size: 1.1em; color: black\"\n      >Enter Address</span\n    >\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion-padding\">\n  <ion-toolbar>\n    <!-- <ion-searchbar [(ngModel)]=\"autocomplete.query\" [showCancelButton]=\"focus\" (ionInput)=\"updateSearch()\"\n      (ionCancel)=\"dismiss()\"></ion-searchbar> -->\n    <ion-searchbar\n      [(ngModel)]=\"autocomplete.query\"\n      (ionInput)=\"updateSearch()\"\n      (ionCancel)=\"dismiss()\"\n    ></ion-searchbar>\n  </ion-toolbar>\n  <ion-list class=\"ion-padding\">\n    <ion-item\n      *ngFor=\"let item of autocompleteItems\"\n      tappable\n      (click)=\"chooseItem(item)\"\n    >\n      {{ item }}\n    </ion-item>\n  </ion-list>\n\n  <!-- <ion-footer>\n    <img src=\"/assets/icon/woman.svg\" />\n  </ion-footer> -->\n  <!-- <ion-grid>\n    <ion-row>\n      <ion-col>\n        <ion-item\n          [ngStyle]=\"{'bottom': 0 + 'px'}\"\n          color=\"nav-color\"\n          (click)=\"chooseHome()\"\n        >\n          <ion-label>\n            <ion-icon color=\"primary\" name=\"ios-home\" slot=\"start\"></ion-icon>\n            <span *ngIf=\"home\" style=\"padding-left: 10px\"\n              >{{Lang[0].homesearch}}</span\n            >\n            <span\n              *ngIf=\"!home\"\n              (click)=\"gotoSetting()\"\n              style=\"padding-left: 10px\"\n              >{{Lang[0].add}} {{Lang[0].homesearch}}</span\n            >\n          </ion-label>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col>\n        <ion-item\n          [ngStyle]=\"{'bottom': 0 + 'px'}\"\n          color=\"nav-color\"\n          (click)=\"chooseWork()\"\n        >\n          <ion-label>\n            <ion-icon\n              color=\"primary\"\n              name=\"ios-briefcase\"\n              slot=\"start\"\n            ></ion-icon>\n            <span *ngIf=\"work\" style=\"padding-left: 10px\"\n              >{{Lang[0].worksearch}}</span\n            >\n            <span\n              *ngIf=\"!work\"\n              (click)=\"gotoSetting()\"\n              style=\"padding-left: 10px\"\n              >{{Lang[0].add}} {{Lang[0].worksearch}}</span\n            >\n          </ion-label>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n  </ion-grid> -->\n</ion-content>\n"

/***/ }),

/***/ "./src/app/pages/autocomplete/autocomplete.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/pages/autocomplete/autocomplete.page.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-button {\n  border-radius: 12px;\n}\n\nion-item {\n  --inner-padding-start: 0px;\n  --padding-start: 0px;\n  border-bottom: #0a64eb;\n}\n\n.footer-md:before {\n  left: 0;\n  top: -2px;\n  bottom: auto;\n  background-position: left 0 top 0;\n  position: absolute;\n  width: 100%;\n  height: 2px;\n  background-image: url();\n  background-repeat: repeat-x;\n  content: \"\" !important;\n}\n\n.footer-ios:before {\n  left: 0;\n  top: -2px;\n  bottom: auto;\n  background-position: left 0 top 0;\n  position: absolute;\n  width: 100%;\n  height: 2px;\n  background-image: url();\n  background-repeat: repeat-x;\n  content: \"\" !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYWhhL0lvbmljQXBwcy9DNFJpZGUtSU9TLTIwMjQvY2FsbDRyaWRlLXBhc3Nlbmdlci9zcmMvYXBwL3BhZ2VzL2F1dG9jb21wbGV0ZS9hdXRvY29tcGxldGUucGFnZS5zY3NzIiwic3JjL2FwcC9wYWdlcy9hdXRvY29tcGxldGUvYXV0b2NvbXBsZXRlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLG1CQUFBO0FDQ0Y7O0FERUE7RUFDRSwwQkFBQTtFQUNBLG9CQUFBO0VBQ0Esc0JBQUE7QUNDRjs7QURFQTtFQUNFLE9BQUE7RUFDQSxTQUFBO0VBQ0EsWUFBQTtFQUNBLGlDQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtFQUNBLHVCQUFBO0VBQ0EsMkJBQUE7RUFDQSxzQkFBQTtBQ0NGOztBREVBO0VBQ0UsT0FBQTtFQUNBLFNBQUE7RUFDQSxZQUFBO0VBQ0EsaUNBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0EsdUJBQUE7RUFDQSwyQkFBQTtFQUNBLHNCQUFBO0FDQ0YiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9hdXRvY29tcGxldGUvYXV0b2NvbXBsZXRlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1idXR0b24ge1xyXG4gIGJvcmRlci1yYWRpdXM6IDEycHg7XHJcbn1cclxuXHJcbmlvbi1pdGVtIHtcclxuICAtLWlubmVyLXBhZGRpbmctc3RhcnQ6IDBweDtcclxuICAtLXBhZGRpbmctc3RhcnQ6IDBweDtcclxuICBib3JkZXItYm90dG9tOiByZ2IoMTAsIDEwMCwgMjM1KTtcclxufVxyXG5cclxuLmZvb3Rlci1tZDpiZWZvcmUge1xyXG4gIGxlZnQ6IDA7XHJcbiAgdG9wOiAtMnB4O1xyXG4gIGJvdHRvbTogYXV0bztcclxuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBsZWZ0IDAgdG9wIDA7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogMnB4O1xyXG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybCgpO1xyXG4gIGJhY2tncm91bmQtcmVwZWF0OiByZXBlYXQteDtcclxuICBjb250ZW50OiBcIlwiICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5mb290ZXItaW9zOmJlZm9yZSB7XHJcbiAgbGVmdDogMDtcclxuICB0b3A6IC0ycHg7XHJcbiAgYm90dG9tOiBhdXRvO1xyXG4gIGJhY2tncm91bmQtcG9zaXRpb246IGxlZnQgMCB0b3AgMDtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAycHg7XHJcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCk7XHJcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IHJlcGVhdC14O1xyXG4gIGNvbnRlbnQ6IFwiXCIgIWltcG9ydGFudDtcclxufVxyXG4iLCJpb24tYnV0dG9uIHtcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcbn1cblxuaW9uLWl0ZW0ge1xuICAtLWlubmVyLXBhZGRpbmctc3RhcnQ6IDBweDtcbiAgLS1wYWRkaW5nLXN0YXJ0OiAwcHg7XG4gIGJvcmRlci1ib3R0b206ICMwYTY0ZWI7XG59XG5cbi5mb290ZXItbWQ6YmVmb3JlIHtcbiAgbGVmdDogMDtcbiAgdG9wOiAtMnB4O1xuICBib3R0b206IGF1dG87XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGxlZnQgMCB0b3AgMDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAycHg7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybCgpO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogcmVwZWF0LXg7XG4gIGNvbnRlbnQ6IFwiXCIgIWltcG9ydGFudDtcbn1cblxuLmZvb3Rlci1pb3M6YmVmb3JlIHtcbiAgbGVmdDogMDtcbiAgdG9wOiAtMnB4O1xuICBib3R0b206IGF1dG87XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGxlZnQgMCB0b3AgMDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAycHg7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybCgpO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogcmVwZWF0LXg7XG4gIGNvbnRlbnQ6IFwiXCIgIWltcG9ydGFudDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/autocomplete/autocomplete.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/autocomplete/autocomplete.page.ts ***!
  \*********************************************************/
/*! exports provided: AutocompletePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AutocompletePage", function() { return AutocompletePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/pop-up.service */ "./src/app/services/pop-up.service.ts");
/* harmony import */ var src_app_services_language_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/language.service */ "./src/app/services/language.service.ts");
/* harmony import */ var src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/settings.service */ "./src/app/services/settings.service.ts");
/* harmony import */ var src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/profile.service */ "./src/app/services/profile.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");









var AutocompletePage = /** @class */ (function () {
    function AutocompletePage(pop, lp, settings, navCtrl, ph, zone, modalController, location) {
        this.pop = pop;
        this.lp = lp;
        this.settings = settings;
        this.navCtrl = navCtrl;
        this.ph = ph;
        this.zone = zone;
        this.modalController = modalController;
        this.location = location;
        this.service = new google.maps.places.AutocompleteService();
        this.Lang = this.lp.translate();
        this.autocompleteItems = [];
        // listen for home and work button
        this.autocomplete = {
            query: "",
        };
        console.log("AUTOCOMPETE CALLED -----");
    }
    AutocompletePage.prototype.gotoSetting = function () {
        this.navCtrl.navigateForward("profile");
    };
    AutocompletePage.prototype.chooseHome = function () {
        if (this.home == null) {
            this.dismiss();
            this.pop.presentToast(this.lp.translate()[0].home);
        }
        else {
            this.modalController.dismiss(this.home);
        }
    };
    AutocompletePage.prototype.chooseWork = function () {
        if (this.work == null) {
            this.dismiss();
            this.pop.presentToast(this.lp.translate()[0].home);
        }
        else {
            this.modalController.dismiss(this.work);
        }
    };
    AutocompletePage.prototype.dismiss = function () {
        this.modalController.dismiss();
    };
    AutocompletePage.prototype.chooseItem = function (item) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.dismiss(item)];
                    case 1:
                        _a.sent();
                        this.ph.isHome = true;
                        return [2 /*return*/];
                }
            });
        });
    };
    AutocompletePage.prototype.updateSearch = function () {
        this.ph.isHome = false;
        if (this.autocomplete.query === "") {
            this.autocompleteItems = [];
            return;
        }
        var me = this;
        this.service.getPlacePredictions({
            input: this.autocomplete.query,
            componentRestrictions: { country: ["GH", "US"] },
        }, function (predictions, status) {
            me.autocompleteItems = [];
            console.log(predictions, status);
            me.zone.run(function () {
                if (predictions != null) {
                    predictions.forEach(function (prediction) {
                        me.autocompleteItems.push(prediction.description);
                    });
                }
            });
        });
    };
    AutocompletePage.prototype.goBack = function () {
        this.modalController.dismiss();
    };
    AutocompletePage.prototype.ngOnInit = function () {
        console.log("AUTOCOMPETE CALLED INIT -----");
    };
    AutocompletePage.ctorParameters = function () { return [
        { type: src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_3__["PopUpService"] },
        { type: src_app_services_language_service__WEBPACK_IMPORTED_MODULE_4__["LanguageService"] },
        { type: src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_5__["SettingsService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
        { type: src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_6__["ProfileService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
        { type: _angular_common__WEBPACK_IMPORTED_MODULE_7__["Location"] }
    ]; };
    AutocompletePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-autocomplete",
            template: __webpack_require__(/*! raw-loader!./autocomplete.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/autocomplete/autocomplete.page.html"),
            styles: [__webpack_require__(/*! ./autocomplete.page.scss */ "./src/app/pages/autocomplete/autocomplete.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_3__["PopUpService"],
            src_app_services_language_service__WEBPACK_IMPORTED_MODULE_4__["LanguageService"],
            src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_5__["SettingsService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
            src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_6__["ProfileService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
            _angular_common__WEBPACK_IMPORTED_MODULE_7__["Location"]])
    ], AutocompletePage);
    return AutocompletePage;
}());



/***/ })

}]);
//# sourceMappingURL=default~pages-booklater-booklater-module~pages-estimate-estimate-module~pages-home-home-module~pages~46d11c0c-es5.js.map