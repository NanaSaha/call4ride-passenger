(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-referalcode-referalcode-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/referalcode/referalcode.page.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/referalcode/referalcode.page.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header no-border>\n  <ion-toolbar style=\"margin-top: 20px\">\n    <ion-button size=\"large\" fill=\"clear\" (click)=\"goBack()\">\n      <ion-icon name=\"arrow-back\"></ion-icon>\n      <span style=\"padding-left: 30px; font-size: 1em\">Promo Code</span>\n    </ion-button>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"o-scroll\" padding>\n  <div id=\"recaptcha-container\"></div>\n\n  <h3>Enter your code to activate your discount</h3>\n  <div class=\"form\">\n    <!-- <p padding >Add a Phone Number to continue. </p> -->\n    <!-- <h3>Enter your code to activate your discount</h3> -->\n    <!-- <ion-label class=\"stack\" padding color=\"primary\" position=\"stacked\"\n      >PROMO CODE</ion-label\n    > -->\n    <ion-input\n      class=\"input\"\n      lines=\"none\"\n      type=\"text\"\n      placeholder=\"eg. RIDE4Call4Ride\"\n      [(ngModel)]=\"code\"\n    ></ion-input>\n\n    <ion-button\n      icon-only\n      [ngStyle]=\"{'margin-top': 20 + 'px'}\"\n      color=\"primary\"\n      expand=\"block\"\n      (click)=\"apply_code(code)\"\n    >\n      Apply Code\n    </ion-button>\n  </div>\n\n  <!-- <div class=\"form\">\n    <ion-input\n      class=\"input\"\n      lines=\"none\"\n      type=\"text\"\n      placeholder=\"eg. NEWCODE\"\n      [(ngModel)]=\"code\"\n    ></ion-input>\n\n    <ion-input\n      class=\"input\"\n      lines=\"none\"\n      type=\"number\"\n      placeholder=\"eg. 20%\"\n      [(ngModel)]=\"perc\"\n    ></ion-input>\n\n    <ion-button\n      icon-only\n      [ngStyle]=\"{'margin-top': 20 + 'px'}\"\n      color=\"primary\"\n      expand=\"block\"\n      (click)=\"createDiscount()\"\n    >\n      Create Discount\n    </ion-button>\n  </div> -->\n</ion-content>\n\n<!-- <ion-footer>\n  <img src=\"/assets/img/happy2.png\" />\n</ion-footer> -->\n"

/***/ }),

/***/ "./src/app/pages/referalcode/referalcode.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/referalcode/referalcode.module.ts ***!
  \*********************************************************/
/*! exports provided: ReferalcodePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReferalcodePageModule", function() { return ReferalcodePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _referalcode_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./referalcode.page */ "./src/app/pages/referalcode/referalcode.page.ts");







var routes = [
    {
        path: '',
        component: _referalcode_page__WEBPACK_IMPORTED_MODULE_6__["ReferalcodePage"]
    }
];
var ReferalcodePageModule = /** @class */ (function () {
    function ReferalcodePageModule() {
    }
    ReferalcodePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_referalcode_page__WEBPACK_IMPORTED_MODULE_6__["ReferalcodePage"]]
        })
    ], ReferalcodePageModule);
    return ReferalcodePageModule;
}());



/***/ }),

/***/ "./src/app/pages/referalcode/referalcode.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/pages/referalcode/referalcode.page.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".footer-md:before {\n  left: 0;\n  top: -2px;\n  bottom: auto;\n  background-position: left 0 top 0;\n  position: absolute;\n  width: 100%;\n  height: 2px;\n  background-image: url();\n  background-repeat: repeat-x;\n  content: \"\" !important;\n}\n\nform {\n  margin-top: 5%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYWhhL0lvbmljQXBwcy9DNFJpZGUtSU9TLTIwMjQvY2FsbDRyaWRlLXBhc3Nlbmdlci9zcmMvYXBwL3BhZ2VzL3JlZmVyYWxjb2RlL3JlZmVyYWxjb2RlLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvcmVmZXJhbGNvZGUvcmVmZXJhbGNvZGUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsT0FBQTtFQUNBLFNBQUE7RUFDQSxZQUFBO0VBQ0EsaUNBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0EsdUJBQUE7RUFDQSwyQkFBQTtFQUNBLHNCQUFBO0FDQ0Y7O0FERUE7RUFDRSxjQUFBO0FDQ0YiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9yZWZlcmFsY29kZS9yZWZlcmFsY29kZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZm9vdGVyLW1kOmJlZm9yZSB7XG4gIGxlZnQ6IDA7XG4gIHRvcDogLTJweDtcbiAgYm90dG9tOiBhdXRvO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBsZWZ0IDAgdG9wIDA7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMnB4O1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoKTtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IHJlcGVhdC14O1xuICBjb250ZW50OiBcIlwiICFpbXBvcnRhbnQ7XG59XG5cbmZvcm0ge1xuICBtYXJnaW4tdG9wOiA1JTtcbn1cbiIsIi5mb290ZXItbWQ6YmVmb3JlIHtcbiAgbGVmdDogMDtcbiAgdG9wOiAtMnB4O1xuICBib3R0b206IGF1dG87XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGxlZnQgMCB0b3AgMDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAycHg7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybCgpO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogcmVwZWF0LXg7XG4gIGNvbnRlbnQ6IFwiXCIgIWltcG9ydGFudDtcbn1cblxuZm9ybSB7XG4gIG1hcmdpbi10b3A6IDUlO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/referalcode/referalcode.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/referalcode/referalcode.page.ts ***!
  \*******************************************************/
/*! exports provided: ReferalcodePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReferalcodePage", function() { return ReferalcodePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_language_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/language.service */ "./src/app/services/language.service.ts");
/* harmony import */ var src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/profile.service */ "./src/app/services/profile.service.ts");
/* harmony import */ var src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/pop-up.service */ "./src/app/services/pop-up.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");








var ReferalcodePage = /** @class */ (function () {
    function ReferalcodePage(router, navCtrl, lp, ph, pop, location) {
        this.router = router;
        this.navCtrl = navCtrl;
        this.lp = lp;
        this.ph = ph;
        this.pop = pop;
        this.location = location;
        this.applied = false;
    }
    ReferalcodePage.prototype.ionViewDidLoad = function () {
        console.log("ionViewDidLoad referalcode");
    };
    ReferalcodePage.prototype.apply_code = function (code) {
        // Step 2 - Pass the mobile number for verific
        if (this.code != null) {
            // this.pop.presentLoader("");
            // this.AddCode(code);
            this.SearchForSharingIDs(code);
        }
        else {
            this.pop.presentToast("ENTER VALID DISCOUNT CODE");
        }
    };
    // AddCode(code): void {
    //   this.SearchForSharingIDs(code);
    // }
    ReferalcodePage.prototype.SearchForSharingIDs = function (code) {
        var _this = this;
        console.log("PROMO ID " + code);
        this.ph
            .getAllSharingPromoID()
            .orderByChild("code")
            .equalTo(code)
            .on("value", function (data) {
            _this.all_data = data;
            if (data.val()) {
                _this.all_data.forEach(function (snap) {
                    var rider_id = _this.ph.id;
                    var code = snap.val().code;
                    var percentage = snap.val().percentage;
                    _this.verify_promo(code, rider_id, percentage);
                    //this.navCtrl.navigateRoot("referalcode");
                    // this.router.navigateByUrl("referalcode");
                    //return false;
                });
                console.log("VALID");
            }
            else {
                _this.pop.presentToast("PROMO CODE NOT VALID");
                console.log("NOT VALID");
                _this.router.navigateByUrl("referalcode");
                // this.navCtrl.navigateRoot("referalcode");
                //this.pop.hideLoader();
            }
            //this.pop.hideLoader();
        });
        // var ref = firebase.database().ref("SharingIDPromo");
        // ref
        //   .orderByChild("code")
        //   .equalTo(promoID)
        //   .on("value", function (snapshot) {
        //     // this.all_data = snapshot.val();
        //     console.log("SNAPSHOT DATA::" + snapshot.val());
        //     let all_data = snapshot.val();
        //     console.log("SNAPSHOT DATA 2::" + all_data);
        //     this.all_data = all_data;
        //     console.log("SNAPSHOT DATA 3::" + this.all_data);
        //   });
    };
    ReferalcodePage.prototype.createDiscount = function () {
        console.log("CODE " + this.code, "PErcentage " + this.perc);
        this.ph.createPromo(this.code, this.perc);
    };
    ReferalcodePage.prototype.verify_promo = function (code, id, perc) {
        var _this = this;
        console.log("RUNNINGH VERIFY CODE --->>>>");
        this.verify_code = code;
        this.rider_id = id;
        console.log("VERIFYPROMO CODE IS " + this.verify_code);
        console.log("RIDER ID IS " + this.rider_id);
        this.ph
            .getAllUsedCodes()
            .orderByChild("rider_id")
            .equalTo(id)
            .once("value", function (data) {
            console.log("VALUE FROM VERIFYIN CODE: " + JSON.stringify(data.val()));
            var arr = JSON.stringify(data.val());
            var index = arr.indexOf(code);
            console.log("index is : " + index);
            if (data.val()) {
                data.forEach(function (snap) {
                    _this.user_id = _this.ph.id;
                    var oldcode = snap.val().code;
                    _this.percentage = snap.val().percentage;
                    var status = snap.val().status;
                    console.log(_this.user_id, oldcode, _this.percentage);
                    console.log("Code is " + _this.verify_code);
                    console.log("oldcode is " + oldcode);
                    console.log("Status is " + status);
                    // if (this.verify_code == oldcode && status == "activated") {
                    //   console.log("code used");
                    //   this.pop.presentToast("DISCOUNT CODE APPLIED BUT NOT USED");
                    //   this.router.navigateByUrl("referalcode");
                    // } else if (this.verify_code == oldcode && status == "deactivated") {
                    //   console.log("Same code but deactivated-- APPLY");
                    //   this.pop.presentToast("A DISCOUNT APPLIED TO YOUR NEXT RIDE");
                    //   this.router.navigateByUrl("referalcode");
                    // } else if (this.verify_code != oldcode && status == "activated") {
                    //   console.log(
                    //     "old code not used but it still activated-- DONT APPLY"
                    //   );
                    //   this.pop.presentToast("OLD DISCOUNT CODE NOT USED ");
                    //   this.router.navigateByUrl("referalcode");
                    // } else if (this.verify_code != oldcode && status == "deactivated") {
                    //   console.log("code not used hence save it");
                    //   this.pop.presentToast("A DISCOUNT APPLIED TO YOUR NEXT RIDE");
                    //   this.applied = true;
                    //   console.log("--->>>> APPLIIIEEEDDD--->>>" + this.applied);
                    // this.router.navigateByUrl("referalcode");
                    //}
                });
                if (index == -1) {
                    console.log("INDEX IS -1 CODE DOESNT EXIST");
                    _this.pop.presentToast("PROMO CODE APPLIED SUCCESSFULLY");
                    _this.ph.RiderPromoSaved(_this.user_id, code, perc, "activated");
                }
                else {
                    _this.pop.presentToast("DISCOUNT CODE APPLIED ALREADY");
                }
            }
            else {
                console.log("code doesnt exist hence save it");
                _this.pop.presentToast("PROMO CODE APPLIED SUCCESSFULLY");
                _this.ph.RiderPromoSaved(id, code, perc, "activated");
                _this.router.navigateByUrl("referalcode");
            }
        });
    };
    ReferalcodePage.prototype.goBack = function () {
        this.location.back();
    };
    ReferalcodePage.prototype.checkPromoExist = function () {
        var _this = this;
        var id = this.ph.id;
        this.ph
            .getAllUsedCodes()
            .orderByChild("rider_id")
            .equalTo(id)
            .once("value", function (data) {
            console.log("VALUE FROM VERIFYIN CODE: " + JSON.stringify(data.val()));
            if (data.val()) {
                data.forEach(function (snap) {
                    _this.status = snap.val().status;
                    var percen = snap.val().percentage;
                    console.log("Status is " + _this.status);
                    console.log("percen is " + percen);
                    if (_this.status == "activated") {
                        console.log("The percentage here is " + percen);
                    }
                    else {
                        console.log("Deactivated The percentage here is " + percen);
                    }
                });
            }
            else {
                console.log("NO PROMO CODE ACTIVATED YET");
            }
        });
    };
    ReferalcodePage.prototype.ngOnInit = function () {
        this.checkPromoExist();
    };
    ReferalcodePage.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
        { type: src_app_services_language_service__WEBPACK_IMPORTED_MODULE_3__["LanguageService"] },
        { type: src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_4__["ProfileService"] },
        { type: src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_5__["PopUpService"] },
        { type: _angular_common__WEBPACK_IMPORTED_MODULE_6__["Location"] }
    ]; };
    ReferalcodePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-referalcode",
            template: __webpack_require__(/*! raw-loader!./referalcode.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/referalcode/referalcode.page.html"),
            styles: [__webpack_require__(/*! ./referalcode.page.scss */ "./src/app/pages/referalcode/referalcode.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
            src_app_services_language_service__WEBPACK_IMPORTED_MODULE_3__["LanguageService"],
            src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_4__["ProfileService"],
            src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_5__["PopUpService"],
            _angular_common__WEBPACK_IMPORTED_MODULE_6__["Location"]])
    ], ReferalcodePage);
    return ReferalcodePage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-referalcode-referalcode-module-es5.js.map