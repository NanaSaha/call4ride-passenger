(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-history-details-history-details-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/history-details/history-details.page.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/history-details/history-details.page.html ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header no-border>\n  <ion-toolbar>\n    <ion-button (click)=\"goBack()\" ion-button color=\"primary\" fill=\"clear\">\n      <ion-icon style=\"font-size: 1.5em\" name=\"arrow-back\"></ion-icon>\n    </ion-button>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"yes-scroll\" color=\"light\">\n  <div id=\"dvMap\" style=\"width: auto; height: 180px\"></div>\n\n  <ion-item class=\"hists\" lines=\"none\">\n    <div>\n      <ion-grid>\n        <ion-row>\n          <ion-col>\n            <ion-label class=\"date\">\n              <ion-icon name=\"calendar-outline\"></ion-icon\n              ><strong>{{currentEvent?.date}}</strong>\n            </ion-label>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col>\n            <!-- <h3 class='price'><ion-icon name=\"cash\"></ion-icon> <strong>{{settings.appcurrency}}{{riderpaid}}</strong></h3> -->\n            <ion-label class=\"location\">\n              <ion-icon name=\"locate\"></ion-icon\n              ><strong>{{currentEvent?.location}}</strong>\n            </ion-label>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col>\n            <!-- <h3 class='price'><ion-icon name=\"cash\"></ion-icon> <strong>{{settings.appcurrency}}{{riderpaid}}</strong></h3> -->\n            <ion-label class=\"destination\">\n              <ion-icon name=\"flag\"></ion-icon\n              ><strong>{{currentEvent?.destination}}</strong>\n            </ion-label>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col>\n            <!-- <h3 class='price'><ion-icon name=\"cash\"></ion-icon> <strong>{{settings.appcurrency}}{{riderpaid}}</strong></h3> -->\n            <ion-label class=\"price\">\n              <ion-icon name=\"contact\"></ion-icon><strong>{{prof.name}}</strong>\n            </ion-label>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col>\n          \n            <ion-label class=\"date\">  \n              <ion-icon name=\"car\"></ion-icon\n              ><strong>{{currentEvent?.name}}</strong>\n            </ion-label>\n          </ion-col>\n        </ion-row>\n        \n      </ion-grid>\n    </div>\n  </ion-item>\n  <!-- <ion-title padding text-center>RIDER PAID</ion-title><br>\n  <ion-title padding text-center\n    ><strong>{{settings.appcurrency}}{{(riderpaid)}}</strong></ion-title\n  > -->\n\n  <!-- <ion-item lines=\"none\" padding>\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <ion-label\n            text-center\n            style=\"\n              font-weight: 600;\n              color: rgb(75, 75, 75);\n              margin: 5px;\n              text-align: center;\n            \"\n          >\n            Ride Cost</ion-label\n          >\n        </ion-col>\n        <ion-col>\n          <ion-label text-center style=\"margin: 5px\">\n            <strong\n              >{{settings.appcurrency}}{{(currentPrie)}}</strong\n            ></ion-label\n          >\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-item> -->\n\n<!-- -->\n\n  <ion-item padding>\n    <ion-grid>\n      <ion-row>\n        <ion-col size=\"6\">\n          <ion-label\n            text-center\n            style=\"\n              font-weight: 600;\n              color: rgb(75, 75, 75);\n              margin: 5px;\n              text-align: center;\n            \"\n            >Ride Cost\n          </ion-label>\n        </ion-col>\n        <ion-col size=\"6\">\n          <ion-label text-center style=\"margin: 5px\">\n            <strong\n              >{{settings.appcurrency}}{{(currentPrie)}}</strong\n            ></ion-label\n          >\n        </ion-col>\n\n        <ion-col size=\"6\">\n          <ion-label text-center style=\"\n                                font-weight: 600;\n                                color: rgb(75, 75, 75);\n                                margin: 5px;\n                                text-align: center;\n                              \">\n            Wait Time Cost</ion-label>\n        </ion-col>\n        <ion-col size=\"6\">\n          <ion-label text-center style=\"margin: 5px\">\n            <strong>{{settings.appcurrency}}{{(waitTimeCost)}}</strong></ion-label>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-item>\n\n<ion-title padding text-center>PAYMENT MADE</ion-title>\n<ion-title text-center><strong>{{settings.appcurrency}}{{(driverMade)}}</strong></ion-title>\n\n  <div *ngIf=\"currentEvent?.tip != 0\">\n    <!-- <ion-item>\n      <ion-grid>\n        <ion-row>\n          <ion-col>\n            <ion-title padding text-center>TIPS</ion-title>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col>\n            <ion-title text-center><strong>{{settings.appcurrency}}{{myTip}}</strong></ion-title>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-item> -->\n\n    <ion-item class=\"hists\" no-lines padding text-cente>\n      <ion-grid>\n        <ion-row>\n          <ion-col size=\"3\">\n            <ion-label\n              style=\"\n                margin: 5px;\n                text-overflow: inherit;\n                white-space: normal;\n                font-weight: 600;\n                color: dimgray;\n              \"\n            >\n              Date\n            </ion-label>\n          </ion-col>\n          <ion-col size=\"9\">\n            <ion-label\n              style=\"text-overflow: inherit; white-space: normal; margin: 5px\"\n            >\n              <strong>{{dateOfTip}}</strong></ion-label\n            >\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-item>\n\n    <!-- \n    <div text-center *ngFor=\"let surge of surcharges\">\n      <div *ngIf='surge.owner == 0'>\n        <ion-grid>\n          <ion-row>\n            <ion-col>\n              <ion-title padding style=\"text-overflow: inherit; white-space: normal; margin: 5px;\">{{surge.title}}\n              </ion-title>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col>\n              <ion-label *ngIf='surge.bone == 0' padding> <strong>-${{(surge.price)}}</strong></ion-label>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col>\n              <ion-label *ngIf='surge.bone == 1' padding>\n                <strong>-${{((surge.price/100) * myTip).toFixed(2)}}</strong>\n              </ion-label>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n\n\n\n      </div>\n    </div> -->\n\n    <!-- <ion-grid>\n      <ion-row>\n        <ion-col>\n          <ion-title padding text-center>Final Tip : <strong>{{settings.appcurrency}}{{(totemTIPS)|| 0}}</strong>\n          </ion-title>\n\n        </ion-col>\n      </ion-row>\n    </ion-grid> -->\n  </div>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/pages/history-details/history-details.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/history-details/history-details.module.ts ***!
  \*****************************************************************/
/*! exports provided: HistoryDetailsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistoryDetailsPageModule", function() { return HistoryDetailsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _history_details_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./history-details.page */ "./src/app/pages/history-details/history-details.page.ts");







var routes = [
    {
        path: '',
        component: _history_details_page__WEBPACK_IMPORTED_MODULE_6__["HistoryDetailsPage"]
    }
];
var HistoryDetailsPageModule = /** @class */ (function () {
    function HistoryDetailsPageModule() {
    }
    HistoryDetailsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_history_details_page__WEBPACK_IMPORTED_MODULE_6__["HistoryDetailsPage"]]
        })
    ], HistoryDetailsPageModule);
    return HistoryDetailsPageModule;
}());



/***/ }),

/***/ "./src/app/pages/history-details/history-details.page.scss":
/*!*****************************************************************!*\
  !*** ./src/app/pages/history-details/history-details.page.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content .drive {\n  color: black;\n}\nion-content .drive ion-label {\n  font-size: 0.8em;\n  padding: 12px;\n  color: #248cd2;\n}\nion-content .price {\n  color: #3eab0f;\n  padding-top: 14px;\n  padding-bottom: 14px;\n  border-radius: 12px;\n}\nion-content .price ion-label {\n  font-size: 0.8em;\n  padding: 5px;\n  color: #248cd2;\n}\nion-content .date {\n  color: orange;\n  padding-top: 14px;\n  padding-bottom: 14px;\n  border-bottom: 1px solid rgba(212, 212, 212, 0.93);\n}\nion-content .date ion-label {\n  font-size: 0.8em;\n  padding: 5px;\n  color: #248cd2;\n}\nion-content .location {\n  width: auto;\n  padding-top: 8px;\n  padding-bottom: 8px;\n}\nion-content .location p {\n  height: auto;\n}\nion-content .location ion-label {\n  font-size: 0.8em;\n  padding: 5px;\n  color: orange;\n}\nion-content .destination {\n  width: auto;\n  padding-top: 8px;\n  padding-bottom: 8px;\n}\nion-content .destination ion-label {\n  font-size: 0.8em;\n  padding: 5px;\n  color: darkslateblue;\n}\nion-content p {\n  font-size: 1.3em;\n  height: auto;\n  line-height: 100px;\n  width: auto;\n}\n#topcontent {\n  width: 100%;\n  padding-top: 7px;\n  padding-bottom: 7px;\n  font-size: 1.27em;\n  color: white;\n}\n#topcontent ion-button {\n  border: 1px solid #f7f7f7;\n  vertical-align: middle;\n  width: 100%;\n}\nh1 {\n  background: #248cd2;\n  font-size: 1.67em;\n  padding-top: 7px;\n  width: auto;\n  padding-bottom: 7px;\n}\n.followed-items ion-item {\n  margin-top: 15px;\n  margin-bottom: 15px;\n  border-radius: 12px;\n  border: 1px solid rgba(212, 212, 212, 0.93);\n}\n.topped-items ion-item {\n  margin-top: 0px;\n  margin-bottom: 6px;\n}\n.topped-items h2 {\n  color: orange;\n  font-size: 1.27em;\n  padding: 2px;\n}\n.topped-items ion-label {\n  color: #0099ff !important;\n  font-size: 1em;\n  padding: 2px;\n}\nion-button {\n  border-radius: 12px;\n}\nion-footer {\n  border-top: 1.4px solid rgba(212, 212, 212, 0.93);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYWhhL0lvbmljQXBwcy9DNFJpZGUtSU9TLTIwMjQvY2FsbDRyaWRlLXBhc3Nlbmdlci9zcmMvYXBwL3BhZ2VzL2hpc3RvcnktZGV0YWlscy9oaXN0b3J5LWRldGFpbHMucGFnZS5zY3NzIiwic3JjL2FwcC9wYWdlcy9oaXN0b3J5LWRldGFpbHMvaGlzdG9yeS1kZXRhaWxzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDRTtFQUNFLFlBQUE7QUNBSjtBRENJO0VBQ0UsZ0JBQUE7RUFDQSxhQUFBO0VBQ0EsY0FBQTtBQ0NOO0FER0U7RUFDRSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxvQkFBQTtFQUVBLG1CQUFBO0FDRko7QURHSTtFQUNFLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7QUNETjtBREtFO0VBQ0UsYUFBQTtFQUNBLGlCQUFBO0VBQ0Esb0JBQUE7RUFDQSxrREFBQTtBQ0hKO0FES0k7RUFDRSxnQkFBQTtFQUNBLFlBQUE7RUFDQSxjQUFBO0FDSE47QURPRTtFQUNFLFdBQUE7RUFFQSxnQkFBQTtFQUNBLG1CQUFBO0FDTko7QURPSTtFQUNFLFlBQUE7QUNMTjtBRFFJO0VBQ0UsZ0JBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtBQ05OO0FEVUU7RUFDRSxXQUFBO0VBRUEsZ0JBQUE7RUFDQSxtQkFBQTtBQ1RKO0FEVUk7RUFDRSxnQkFBQTtFQUNBLFlBQUE7RUFDQSxvQkFBQTtBQ1JOO0FEWUU7RUFDRSxnQkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7QUNWSjtBRGNBO0VBQ0UsV0FBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLFlBQUE7QUNYRjtBRFlFO0VBQ0UseUJBQUE7RUFDQSxzQkFBQTtFQUNBLFdBQUE7QUNWSjtBRGNBO0VBQ0UsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0FDWEY7QURlRTtFQUNFLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLDJDQUFBO0FDWko7QURpQkU7RUFDRSxlQUFBO0VBQ0Esa0JBQUE7QUNkSjtBRGdCRTtFQUNFLGFBQUE7RUFDQSxpQkFBQTtFQUNBLFlBQUE7QUNkSjtBRGlCRTtFQUNFLHlCQUFBO0VBQ0EsY0FBQTtFQUNBLFlBQUE7QUNmSjtBRG1CQTtFQUNFLG1CQUFBO0FDaEJGO0FEbUJBO0VBQ0UsaURBQUE7QUNoQkYiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9oaXN0b3J5LWRldGFpbHMvaGlzdG9yeS1kZXRhaWxzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50IHtcclxuICAuZHJpdmUge1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gICAgaW9uLWxhYmVsIHtcclxuICAgICAgZm9udC1zaXplOiAwLjhlbTtcclxuICAgICAgcGFkZGluZzogMTJweDtcclxuICAgICAgY29sb3I6IHJnYigzNiwgMTQwLCAyMTApO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLnByaWNlIHtcclxuICAgIGNvbG9yOiAjM2VhYjBmO1xyXG4gICAgcGFkZGluZy10b3A6IDE0cHg7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMTRweDtcclxuXHJcbiAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG4gICAgaW9uLWxhYmVsIHtcclxuICAgICAgZm9udC1zaXplOiAwLjhlbTtcclxuICAgICAgcGFkZGluZzogNXB4O1xyXG4gICAgICBjb2xvcjogcmdiKDM2LCAxNDAsIDIxMCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAuZGF0ZSB7XHJcbiAgICBjb2xvcjogb3JhbmdlO1xyXG4gICAgcGFkZGluZy10b3A6IDE0cHg7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMTRweDtcclxuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpO1xyXG5cclxuICAgIGlvbi1sYWJlbCB7XHJcbiAgICAgIGZvbnQtc2l6ZTogMC44ZW07XHJcbiAgICAgIHBhZGRpbmc6IDVweDtcclxuICAgICAgY29sb3I6IHJnYigzNiwgMTQwLCAyMTApO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLmxvY2F0aW9uIHtcclxuICAgIHdpZHRoOiBhdXRvO1xyXG5cclxuICAgIHBhZGRpbmctdG9wOiA4cHg7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogOHB4O1xyXG4gICAgcCB7XHJcbiAgICAgIGhlaWdodDogYXV0bztcclxuICAgIH1cclxuXHJcbiAgICBpb24tbGFiZWwge1xyXG4gICAgICBmb250LXNpemU6IDAuOGVtO1xyXG4gICAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICAgIGNvbG9yOiBvcmFuZ2U7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAuZGVzdGluYXRpb24ge1xyXG4gICAgd2lkdGg6IGF1dG87XHJcblxyXG4gICAgcGFkZGluZy10b3A6IDhweDtcclxuICAgIHBhZGRpbmctYm90dG9tOiA4cHg7XHJcbiAgICBpb24tbGFiZWwge1xyXG4gICAgICBmb250LXNpemU6IDAuOGVtO1xyXG4gICAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICAgIGNvbG9yOiBkYXJrc2xhdGVibHVlO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcCB7XHJcbiAgICBmb250LXNpemU6IDEuM2VtO1xyXG4gICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgbGluZS1oZWlnaHQ6IDEwMHB4O1xyXG4gICAgd2lkdGg6IGF1dG87XHJcbiAgfVxyXG59XHJcblxyXG4jdG9wY29udGVudCB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgcGFkZGluZy10b3A6IDdweDtcclxuICBwYWRkaW5nLWJvdHRvbTogN3B4O1xyXG4gIGZvbnQtc2l6ZTogMS4yN2VtO1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBpb24tYnV0dG9uIHtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICNmN2Y3Zjc7XHJcbiAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG59XHJcblxyXG5oMSB7XHJcbiAgYmFja2dyb3VuZDogcmdiKDM2LCAxNDAsIDIxMCk7XHJcbiAgZm9udC1zaXplOiAxLjY3ZW07XHJcbiAgcGFkZGluZy10b3A6IDdweDtcclxuICB3aWR0aDogYXV0bztcclxuICBwYWRkaW5nLWJvdHRvbTogN3B4O1xyXG59XHJcblxyXG4uZm9sbG93ZWQtaXRlbXMge1xyXG4gIGlvbi1pdGVtIHtcclxuICAgIG1hcmdpbi10b3A6IDE1cHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTJweDtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkIHJnYmEoMjEyLCAyMTIsIDIxMiwgMC45Myk7XHJcbiAgfVxyXG59XHJcblxyXG4udG9wcGVkLWl0ZW1zIHtcclxuICBpb24taXRlbSB7XHJcbiAgICBtYXJnaW4tdG9wOiAwcHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiA2cHg7XHJcbiAgfVxyXG4gIGgyIHtcclxuICAgIGNvbG9yOiBvcmFuZ2U7XHJcbiAgICBmb250LXNpemU6IDEuMjdlbTtcclxuICAgIHBhZGRpbmc6IDJweDtcclxuICB9XHJcblxyXG4gIGlvbi1sYWJlbCB7XHJcbiAgICBjb2xvcjogcmdiKDAsIDE1MywgMjU1KSAhaW1wb3J0YW50O1xyXG4gICAgZm9udC1zaXplOiAxZW07XHJcbiAgICBwYWRkaW5nOiAycHg7XHJcbiAgfVxyXG59XHJcblxyXG5pb24tYnV0dG9uIHtcclxuICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG59XHJcblxyXG5pb24tZm9vdGVyIHtcclxuICBib3JkZXItdG9wOiAxLjRweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpO1xyXG59XHJcbiIsImlvbi1jb250ZW50IC5kcml2ZSB7XG4gIGNvbG9yOiBibGFjaztcbn1cbmlvbi1jb250ZW50IC5kcml2ZSBpb24tbGFiZWwge1xuICBmb250LXNpemU6IDAuOGVtO1xuICBwYWRkaW5nOiAxMnB4O1xuICBjb2xvcjogIzI0OGNkMjtcbn1cbmlvbi1jb250ZW50IC5wcmljZSB7XG4gIGNvbG9yOiAjM2VhYjBmO1xuICBwYWRkaW5nLXRvcDogMTRweDtcbiAgcGFkZGluZy1ib3R0b206IDE0cHg7XG4gIGJvcmRlci1yYWRpdXM6IDEycHg7XG59XG5pb24tY29udGVudCAucHJpY2UgaW9uLWxhYmVsIHtcbiAgZm9udC1zaXplOiAwLjhlbTtcbiAgcGFkZGluZzogNXB4O1xuICBjb2xvcjogIzI0OGNkMjtcbn1cbmlvbi1jb250ZW50IC5kYXRlIHtcbiAgY29sb3I6IG9yYW5nZTtcbiAgcGFkZGluZy10b3A6IDE0cHg7XG4gIHBhZGRpbmctYm90dG9tOiAxNHB4O1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgcmdiYSgyMTIsIDIxMiwgMjEyLCAwLjkzKTtcbn1cbmlvbi1jb250ZW50IC5kYXRlIGlvbi1sYWJlbCB7XG4gIGZvbnQtc2l6ZTogMC44ZW07XG4gIHBhZGRpbmc6IDVweDtcbiAgY29sb3I6ICMyNDhjZDI7XG59XG5pb24tY29udGVudCAubG9jYXRpb24ge1xuICB3aWR0aDogYXV0bztcbiAgcGFkZGluZy10b3A6IDhweDtcbiAgcGFkZGluZy1ib3R0b206IDhweDtcbn1cbmlvbi1jb250ZW50IC5sb2NhdGlvbiBwIHtcbiAgaGVpZ2h0OiBhdXRvO1xufVxuaW9uLWNvbnRlbnQgLmxvY2F0aW9uIGlvbi1sYWJlbCB7XG4gIGZvbnQtc2l6ZTogMC44ZW07XG4gIHBhZGRpbmc6IDVweDtcbiAgY29sb3I6IG9yYW5nZTtcbn1cbmlvbi1jb250ZW50IC5kZXN0aW5hdGlvbiB7XG4gIHdpZHRoOiBhdXRvO1xuICBwYWRkaW5nLXRvcDogOHB4O1xuICBwYWRkaW5nLWJvdHRvbTogOHB4O1xufVxuaW9uLWNvbnRlbnQgLmRlc3RpbmF0aW9uIGlvbi1sYWJlbCB7XG4gIGZvbnQtc2l6ZTogMC44ZW07XG4gIHBhZGRpbmc6IDVweDtcbiAgY29sb3I6IGRhcmtzbGF0ZWJsdWU7XG59XG5pb24tY29udGVudCBwIHtcbiAgZm9udC1zaXplOiAxLjNlbTtcbiAgaGVpZ2h0OiBhdXRvO1xuICBsaW5lLWhlaWdodDogMTAwcHg7XG4gIHdpZHRoOiBhdXRvO1xufVxuXG4jdG9wY29udGVudCB7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nLXRvcDogN3B4O1xuICBwYWRkaW5nLWJvdHRvbTogN3B4O1xuICBmb250LXNpemU6IDEuMjdlbTtcbiAgY29sb3I6IHdoaXRlO1xufVxuI3RvcGNvbnRlbnQgaW9uLWJ1dHRvbiB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNmN2Y3Zjc7XG4gIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG5oMSB7XG4gIGJhY2tncm91bmQ6ICMyNDhjZDI7XG4gIGZvbnQtc2l6ZTogMS42N2VtO1xuICBwYWRkaW5nLXRvcDogN3B4O1xuICB3aWR0aDogYXV0bztcbiAgcGFkZGluZy1ib3R0b206IDdweDtcbn1cblxuLmZvbGxvd2VkLWl0ZW1zIGlvbi1pdGVtIHtcbiAgbWFyZ2luLXRvcDogMTVweDtcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcbiAgYm9yZGVyOiAxcHggc29saWQgcmdiYSgyMTIsIDIxMiwgMjEyLCAwLjkzKTtcbn1cblxuLnRvcHBlZC1pdGVtcyBpb24taXRlbSB7XG4gIG1hcmdpbi10b3A6IDBweDtcbiAgbWFyZ2luLWJvdHRvbTogNnB4O1xufVxuLnRvcHBlZC1pdGVtcyBoMiB7XG4gIGNvbG9yOiBvcmFuZ2U7XG4gIGZvbnQtc2l6ZTogMS4yN2VtO1xuICBwYWRkaW5nOiAycHg7XG59XG4udG9wcGVkLWl0ZW1zIGlvbi1sYWJlbCB7XG4gIGNvbG9yOiAjMDA5OWZmICFpbXBvcnRhbnQ7XG4gIGZvbnQtc2l6ZTogMWVtO1xuICBwYWRkaW5nOiAycHg7XG59XG5cbmlvbi1idXR0b24ge1xuICBib3JkZXItcmFkaXVzOiAxMnB4O1xufVxuXG5pb24tZm9vdGVyIHtcbiAgYm9yZGVyLXRvcDogMS40cHggc29saWQgcmdiYSgyMTIsIDIxMiwgMjEyLCAwLjkzKTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/history-details/history-details.page.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/history-details/history-details.page.ts ***!
  \***************************************************************/
/*! exports provided: HistoryDetailsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistoryDetailsPage", function() { return HistoryDetailsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_language_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/language.service */ "./src/app/services/language.service.ts");
/* harmony import */ var src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/settings.service */ "./src/app/services/settings.service.ts");
/* harmony import */ var src_app_services_event_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/event.service */ "./src/app/services/event.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/profile.service */ "./src/app/services/profile.service.ts");








var HistoryDetailsPage = /** @class */ (function () {
    function HistoryDetailsPage(navCtrl, prof, lp, settings, eventProvider, route) {
        this.navCtrl = navCtrl;
        this.prof = prof;
        this.lp = lp;
        this.settings = settings;
        this.eventProvider = eventProvider;
        this.route = route;
        this.currentEvent = {};
        this.total = 0;
        this.tolls = [];
        this.allTotals = 0;
        this.driverMade = 0;
        this.surcharges = [];
        this.totalSurge = 0;
        this.actual = 0;
        this.riderPaid = 0;
        this.totalRiderSurge = 0;
        this.totalDriverSurge = 0;
        this.percentRider = 0;
        this.flatRider = 0;
        this.percentDriver = 0;
        this.flatDriver = 0;
        this.riderPercents = [];
        this.driverPercents = [];
        this.math = Math;
        this.riderpaid = 0;
        this.waitTimeCost = 0;
    }
    HistoryDetailsPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        var g = [];
        var b = [];
        var k = [];
        var o = [];
        var c = [];
        var n = [];
        var j = [];
        var m = [];
        console.log(this.route.snapshot.paramMap.get("eventId"));
        this.eventProvider
            .getEventDetail(this.route.snapshot.paramMap.get("eventId"))
            .on("value", function (eventSnapshot) {
            _this.currentEvent = eventSnapshot.val();
            _this.currentEvent.id = eventSnapshot.key;
            console.log(_this.currentEvent);
            _this.tolls = _this.currentEvent.tolls;
            _this.osc = _this.currentEvent.osc;
            _this.dateOfTip = _this.currentEvent.date;
            console.log(_this.tolls);
            _this.currentEvent.price;
            _this.currentPrie = _this.currentEvent.price;
            _this.waitTimeCost = _this.currentEvent.waitTimeCost;
            //this.riderpaid = parseFloat(this.currentEvent.price).toFixed(2);
            _this.riderpaid = _this.currentEvent.price + _this.currentEvent.waitTimeCost;
            //NO SURCHAGES NOW
            //this.surcharges = this.currentEvent.surcharge;
            // if (this.currentEvent.surcharge)
            //   for (
            //     let index = 0;
            //     index < this.currentEvent.surcharge.length;
            //     index++
            //   ) {
            //     k.push(parseFloat(this.currentEvent.surcharge[index].price));
            //     const add = (a, b) => a + b;
            //     const result = k.reduce(add);
            //     this.totalSurge = result;
            //     this.actual = this.currentEvent.price - this.totalSurge;
            //     console.log(this.totalSurge);
            //     this.currentEvent.surcharge[index].price;
            //   }
            // this.surcharges.forEach((element) => {
            //   //if rider
            //   if (element.owner == 1) {
            //     //if percent
            //     if (element.bone == 1) {
            //       let fo = (element.price / 100) * this.currentEvent.realPrice;
            //       console.log(element.price);
            //       o.push(fo);
            //       const add1 = (a, b) => a + b;
            //       const result1 = o.reduce(add1);
            //       this.percentRider = result1;
            //       console.log(this.percentRider);
            //     }
            //     if (element.bone == 0) {
            //       g.push(parseFloat(element.price));
            //       const add = (a, b) => a + b;
            //       const result = g.reduce(add);
            //       this.flatRider = result;
            //       console.log(result);
            //       element.price;
            //     }
            //     this.totalRiderSurge = this.flatRider + this.percentRider;
            //     console.log(this.totalRiderSurge);
            //   }
            //   this.riderpaid = parseFloat(this.currentEvent.price).toFixed(2);
            //   //if driver
            //   if (element.owner == 0) {
            //     //if percent
            //     if (element.bone == 1) {
            //       let nb = element.price / 100;
            //       console.log(nb * this.currentEvent.tip);
            //       let fo = nb * this.currentEvent.tip;
            //       j.push(fo);
            //       const add2 = (a, b) => a + b;
            //       const result2 = j.reduce(add2);
            //       this.percentDrive = result2;
            //       console.log(
            //         (Math.floor(element.price) / 100) * this.currentEvent.tip
            //       );
            //     }
            //     //if flat fee
            //     if (element.bone == 0) {
            //       m.push(parseFloat(element.price));
            //       const add4 = (a, b) => a + b;
            //       const result4 = m.reduce(add4);
            //       this.flatDrive = result4;
            //       console.log(result4);
            //     }
            //     this.totalDriverSurg = this.flatDrive + this.percentDrive;
            //     //console.log(this.totalDriverSurge, this.flatDriver, this.percentDriver);
            //   }
            //   //if driver
            //   if (element.owner == 0) {
            //     //if percent
            //     if (element.bone == 1) {
            //       let nb = element.price / 100;
            //       console.log(nb * this.riderpaid);
            //       let fo = nb * this.riderpaid;
            //       n.push(fo);
            //       const add2 = (a, b) => a + b;
            //       const result2 = n.reduce(add2);
            //       this.percentDriver = result2;
            //       console.log((Math.floor(element.price) / 100) * this.riderpaid);
            //     }
            //     //if flat fee
            //     if (element.bone == 0) {
            //       c.push(parseFloat(element.price));
            //       const add4 = (a, b) => a + b;
            //       const result4 = c.reduce(add4);
            //       this.flatDriver = result4;
            //       console.log(result4);
            //     }
            //     this.totalDriverSurge = this.flatDriver + this.percentDriver;
            //     console.log(
            //       this.totalDriverSurge,
            //       this.flatDriver,
            //       this.percentDriver
            //     );
            //   }
            // });
            _this.totemTIPS =
                (parseFloat(_this.currentEvent.tip) - _this.totalDriverSurg).toFixed(2) || 0;
            if (!_this.totemTIPS) {
                _this.totemTIPS = 0;
            }
            // console.log(this.actual, g, this.currentEvent.surcharge[index].price);
            console.log(_this.currentEvent.tolls);
            if (_this.currentEvent.tolls) {
                for (var index = 0; index < _this.currentEvent.tolls.length; index++) {
                    b.push(_this.currentEvent.tolls[index].tagCost);
                    var add = function (a, b) { return a + b; };
                    var result = b.reduce(add);
                    _this.total = result;
                    console.log(_this.total, g);
                }
            }
            if (!_this.currentEvent.tip) {
                _this.currentEvent.tip = 0;
            }
            _this.myTip = parseFloat(_this.currentEvent.tip).toFixed(2);
            _this.driverMade = _this.riderpaid;
            _this.GetRoute(_this.currentEvent.location, _this.currentEvent.destination);
        });
    };
    HistoryDetailsPage.prototype.ngOnInit = function () {
        // this.GetRoute(this.currentEvent.location, this.currentEvent.destination);
    };
    HistoryDetailsPage.prototype.GetRoute = function (location, destination) {
        var source, destination;
        var directionsService = new google.maps.DirectionsService();
        var directionsDisplay = new google.maps.DirectionsRenderer({
            draggable: true,
        });
        var mumbai = new google.maps.LatLng(18.975, 72.8258);
        var mapOptions = {
            zoom: 7,
            center: mumbai,
            disableDefaultUI: true,
        };
        var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
        directionsDisplay.setMap(map);
        //directionsDisplay.setPanel(document.getElementById('dvPanel'));
        //*********DIRECTIONS AND ROUTE**********************//
        source = location;
        destination = destination;
        var request = {
            origin: source,
            destination: destination,
            travelMode: google.maps.TravelMode.DRIVING,
        };
        directionsService.route(request, function (response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
            }
        });
        //*********DISTANCE AND DURATION**********************//
        var service = new google.maps.DistanceMatrixService();
        service.getDistanceMatrix({
            origins: [source],
            destinations: [destination],
            travelMode: google.maps.TravelMode.DRIVING,
            unitSystem: google.maps.UnitSystem.METRIC,
            avoidHighways: false,
            avoidTolls: false,
        }, function (response, status) {
            if (status == google.maps.DistanceMatrixStatus.OK &&
                response.rows[0].elements[0].status != "ZERO_RESULTS") {
                // var dvDistance = document.getElementById("dvDistance");
                //  dvDistance.innerHTML = "";
                // dvDistance.innerHTML += "Distance: " + distance + "<br />";
                // dvDistance.innerHTML += "Duration:" + duration;
            }
            else {
                alert("Unable to find the distance via road.");
            }
        });
    };
    HistoryDetailsPage.prototype.goBack = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                this.navCtrl.navigateRoot("home");
                return [2 /*return*/];
            });
        });
    };
    HistoryDetailsPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
        { type: src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_7__["ProfileService"] },
        { type: src_app_services_language_service__WEBPACK_IMPORTED_MODULE_3__["LanguageService"] },
        { type: src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_4__["SettingsService"] },
        { type: src_app_services_event_service__WEBPACK_IMPORTED_MODULE_5__["EventService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"] }
    ]; };
    HistoryDetailsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-history-details",
            template: __webpack_require__(/*! raw-loader!./history-details.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/history-details/history-details.page.html"),
            styles: [__webpack_require__(/*! ./history-details.page.scss */ "./src/app/pages/history-details/history-details.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
            src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_7__["ProfileService"],
            src_app_services_language_service__WEBPACK_IMPORTED_MODULE_3__["LanguageService"],
            src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_4__["SettingsService"],
            src_app_services_event_service__WEBPACK_IMPORTED_MODULE_5__["EventService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"]])
    ], HistoryDetailsPage);
    return HistoryDetailsPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-history-details-history-details-module-es5.js.map